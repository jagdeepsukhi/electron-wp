    var fm_currentDate = new Date();
    var FormCurrency_11 = '$';
    var FormPaypalTax_11 = '0';
    var check_submit11 = 0;
    var check_before_submit11 = {};
    var required_fields11 = ["23","2","9","4","5","6","11","7"];
    var labels_and_ids11 = {"1":"type_text","23":"type_date_new","25":"type_time","2":"type_own_select","9":"type_text","4":"type_text","20":"type_button","5":"type_text","21":"type_button","6":"type_text","11":"type_text","7":"type_own_select","10":"type_submit_reset","12":"type_hidden","13":"type_hidden","14":"type_hidden","15":"type_hidden","16":"type_hidden","8":"type_hidden","22":"type_hidden","17":"type_hidden"};
    var check_regExp_all11 = {"1":["%5E%28%28%28%5Cd%7B4%7D%29%28-%29%280%5B13578%5D%7C10%7C12%29%28-%29%280%5B1-9%5D%7C%5B12%5D%5B0-9%5D%7C3%5B01%5D%29%29%7C%28%28%5Cd%7B4%7D%29%28-%29%280%5B469%5D%7C1%u200C%u200B1%29%28-%29%280%5B1-9%5D%7C%5B12%5D%5B0-9%5D%7C30%29%29%7C%28%28%5Cd%7B4%7D%29%28-%29%2802%29%28-%29%280%5B1-9%5D%7C%5B12%5D%5B0-9%5D%7C2%5B0-8%5D%29%29%7C%28%28%5B02468%u200C%u200B%5D%5B048%5D00%29%28-%29%2802%29%28-%29%2829%29%29%7C%28%28%5B13579%5D%5B26%5D00%29%28-%29%2802%29%28-%29%2829%29%29%7C%28%28%5B0-9%5D%5B0-9%5D%5B0%5D%5B48%5D%29%28-%29%28%u200C%u200B02%29%28-%29%2829%29%29%7C%28%28%5B0-9%5D%5B0-9%5D%5B2468%5D%5B048%5D%29%28-%29%2802%29%28-%29%2829%29%29%7C%28%28%5B0-9%5D%5B0-9%5D%5B13579%5D%5B26%5D%29%28-%29%280%u200C%u200B2%29%28-%29%2829%29%29%29%28%5Cs%29%28%28%5B0-1%5D%5B0-9%5D%7C2%5B0-4%5D%29%3A%28%5B0-5%5D%5B0-9%5D%29%3A%28%5B0-5%5D%5B0-9%5D%29%29%24","","Date and Time must be entered in the following format:    YYYY-MM-DD HH:MM:SS    NOTE: Time is based on 24h clock."],"9":["%5E%5B0-9%5D%5Cd*%24%7CLow","","Gasoline Range must be entered as a positive integer.  "],"4":["%5E%5B0-9%5D%5Cd*%24%7CLow","","Gasoline Range must be entered as a positive integer.  In cases where the gasoline estimator reads 'Low', the word 'Low' can be typed"],"5":["%5E%5Cd%7B1%2C3%7D%28%3F%3A%5C.%5Cd%29%3F%24%7C---","","Electric Range must be entered as a positive number with up to one decimal place. NOTE: In cases where the electric range is too low for the range estimator to read, three dashes should be typed, ie '---'."],"6":["%28%5E100%28%5C.0%7B1%2C2%7D%29%3F%24%29%7C%28%5E%28%5B1-9%5D%28%5B0-9%5D%29%3F%7C0%29%28%5C.%5B0-9%5D%7B1%2C2%7D%29%3F%24%29","","SOC must be entered as an integer from 0-100"],"11":["%5E%5B0-9%5D%5Cd*%24%7C%5E%24","","Charge bar quantity must be a positive integer."]};
    var check_paypal_price_min_max11 = [];
    var file_upload_check11 = [];
    var spinner_check11 = [];
    var scrollbox_trigger_point11 = '20';
    var header_image_animation11 = 'none';
    var scrollbox_loading_delay11 = '0';
    var scrollbox_auto_hide11 = '1';
         function before_load11() {	
}	
 function before_submit11() {
}	
 function before_reset11() {	
}
    function onload_js11() {
  jQuery("#button_calendar_23, #fm-calendar-23").click(function() {
    jQuery("#wdform_23_element11").datepicker("show");
  });
  jQuery("#wdform_23_element11").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_23_element11").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_23_element11").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_23_element11").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_23_element11").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_23_element11").datepicker("setDate", default_date);
  }
    }
    function condition_js11() {
    }
    function check_js11(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js11() {
    
  var disabled_fields = "";	
  jQuery("#form11 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields11\" value =\""+disabled_fields+"\" />").appendTo("#form11");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(11);
    });
    form_view_count11 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(11);
    });
    