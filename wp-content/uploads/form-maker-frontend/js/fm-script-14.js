    var fm_currentDate = new Date();
    var FormCurrency_14 = '$';
    var FormPaypalTax_14 = '0';
    var check_submit14 = 0;
    var check_before_submit14 = {};
    var required_fields14 = ["34","35","7","32","9","11"];
    var labels_and_ids14 = {"22":"type_text","19":"type_text","20":"type_text","21":"type_text","18":"type_text","34":"type_date_new","35":"type_date_new","7":"type_text","32":"type_own_select","9":"type_textarea","14":"type_editor","11":"type_checkbox","13":"type_editor","2":"type_submit_reset","26":"type_hidden","28":"type_hidden","33":"type_hidden"};
    var check_regExp_all14 = {"7":["%5E%28-%29%3F%5B0-9%5D+%24","i","Mileage needs to be a whole number"]};
    var check_paypal_price_min_max14 = [];
    var file_upload_check14 = [];
    var spinner_check14 = [];
    var scrollbox_trigger_point14 = '20';
    var header_image_animation14 = 'none';
    var scrollbox_loading_delay14 = '0';
    var scrollbox_auto_hide14 = '1';
         function before_load14() {
     vlr_before_load();
     
}

 function before_submit14() {
           vlr_before_submit();
}

 function before_reset14() {
     
     
}
    function onload_js14() {
  jQuery("#button_calendar_34, #fm-calendar-34").click(function() {
    jQuery("#wdform_34_element14").datepicker("show");
  });
  jQuery("#wdform_34_element14").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_34_element14").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_34_element14").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_34_element14").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_34_element14").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_34_element14").datepicker("setDate", default_date);
  }
  jQuery("#button_calendar_35, #fm-calendar-35").click(function() {
    jQuery("#wdform_35_element14").datepicker("show");
  });
  jQuery("#wdform_35_element14").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_35_element14").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_35_element14").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_35_element14").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_35_element14").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_35_element14").datepicker("setDate", default_date);
  }
    }
    function condition_js14() {
  if( jQuery("#wdform_32_element14").val()=="" ) {
    jQuery("#form14 div[wdid=9]").removeAttr("style");
  }
  else {
    jQuery("#form14 div[wdid=9]").css("display", "none");
  }
  jQuery("#wdform_32_element14").change(function() {
    if( jQuery("#wdform_32_element14").val()=="" ) {
      jQuery("#form14 div[wdid=9]").removeAttr("style");
    }
    else {
      jQuery("#form14 div[wdid=9]").css("display", "none");
    }
  });
    }
    function check_js14(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js14() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_11_allow_other14\" value=\"no\" />").appendTo("#form14");
  jQuery("<input type=\"hidden\" name=\"wdform_11_allow_other_num14\" value=\"0\" />").appendTo("#form14");
  var disabled_fields = "";	
  jQuery("#form14 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields14\" value =\""+disabled_fields+"\" />").appendTo("#form14");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(14);
    });
    form_view_count14 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(14);
    });
    