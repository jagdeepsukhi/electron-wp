    var fm_currentDate = new Date();
    var FormCurrency_17 = '$';
    var FormPaypalTax_17 = '0';
    var check_submit17 = 0;
    var check_before_submit17 = {};
    var required_fields17 = ["69","71","2","3","4","58","5","7","13","14","15","59","16","17","23","24","25","60","26","27","38","39","40","61","41","42","46","47","48","62","49","50","9","10","11"];
    var labels_and_ids17 = {"66":"type_own_select","69":"type_date_new","71":"type_date_new","68":"type_textarea","29":"type_editor","2":"type_name","3":"type_text","4":"type_checkbox","58":"type_checkbox","5":"type_phone","7":"type_text","12":"type_checkbox","31":"type_editor","13":"type_name","14":"type_text","15":"type_checkbox","59":"type_checkbox","16":"type_phone","17":"type_text","35":"type_checkbox","32":"type_editor","23":"type_name","24":"type_text","25":"type_checkbox","60":"type_checkbox","26":"type_phone","27":"type_text","36":"type_checkbox","44":"type_editor","38":"type_name","39":"type_text","40":"type_checkbox","61":"type_checkbox","41":"type_phone","42":"type_text","37":"type_checkbox","45":"type_editor","46":"type_name","47":"type_text","48":"type_checkbox","62":"type_checkbox","49":"type_phone","50":"type_text","43":"type_editor","9":"type_name","10":"type_phone","11":"type_submitter_mail","8":"type_submit_reset"};
    var check_regExp_all17 = [];
    var check_paypal_price_min_max17 = [];
    var file_upload_check17 = [];
    var spinner_check17 = [];
    var scrollbox_trigger_point17 = '20';
    var header_image_animation17 = 'none';
    var scrollbox_loading_delay17 = '0';
    var scrollbox_auto_hide17 = '1';
         function before_load17() {	
}	
 function before_submit17() {
}	
 function before_reset17() {	
}
    function onload_js17() {
  jQuery("#button_calendar_69, #fm-calendar-69").click(function() {
    jQuery("#wdform_69_element17").datepicker("show");
  });
  jQuery("#wdform_69_element17").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_69_element17").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_69_element17").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_69_element17").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_69_element17").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_69_element17").datepicker("setDate", default_date);
  }
  jQuery("#button_calendar_71, #fm-calendar-71").click(function() {
    jQuery("#wdform_71_element17").datepicker("show");
  });
  jQuery("#wdform_71_element17").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_71_element17").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_71_element17").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_71_element17").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_71_element17").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_71_element17").datepicker("setDate", default_date);
  }
    }
    function condition_js17() {
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=14]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=14]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=14]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=14]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=17]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=17]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=17]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=17]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=16]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=16]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=16]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=16]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=13]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=13]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=13]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=13]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=23]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=23]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=23]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=23]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=24]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=24]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=24]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=24]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=26]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=26]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=26]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=26]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=27]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=27]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=27]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=27]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=31]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=31]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=31]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=31]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=32]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=32]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=32]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=32]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=35]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=35]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=35]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=35]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=36]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=36]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=36]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=36]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=38]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=38]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=38]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=38]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=39]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=39]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=39]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=39]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=41]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=41]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=41]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=41]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=42]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=42]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=42]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=42]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=46]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=46]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=46]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=46]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=47]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=47]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=47]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=47]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=49]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=49]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=49]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=49]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=50]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=50]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=50]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=50]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=44]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=44]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=44]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=44]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=45]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=45]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=45]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=45]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=37]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=37]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=37]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=37]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=57]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=57]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=57]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=57]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=78]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=78]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=78]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=78]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=78]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=78]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=72]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=72]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=72]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=72]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=72]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=72]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=75]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=75]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=75]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=75]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=75]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=75]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=76]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=76]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=76]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=76]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=76]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=76]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=77]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=77]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=77]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=77]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=77]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=77]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=94]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=94]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=94]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=94]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=99]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=99]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=99]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=99]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=98]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=98]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=98]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=98]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=100]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=100]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=100]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=100]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=103]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=103]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=103]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=103]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=103]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=103]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=97]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=97]").removeAttr("style");
  }
  jQuery("div[wdid=12] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=12] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=97]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=97]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=105]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=105]").removeAttr("style");
  }
  jQuery("div[wdid=35] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=35] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=105]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=105]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=106]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=106]").removeAttr("style");
  }
  jQuery("div[wdid=36] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=36] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=106]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=106]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form17 div[wdid=107]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=107]").removeAttr("style");
  }
  jQuery("div[wdid=37] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=37] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form17 div[wdid=107]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=107]").removeAttr("style");
    }
  });
  if( jQuery("#wdform_71_element17").val()=="" ) {
    jQuery("#form17 div[wdid=110]").css("display", "none");
  }
  else {
    jQuery("#form17 div[wdid=110]").removeAttr("style");
  }
  jQuery("#wdform_71_element17").keyup(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=110]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=110]").removeAttr("style");
    }
  });
  jQuery("#wdform_71_element17").change(function() {
    if( jQuery("#wdform_71_element17").val()=="" ) {
      jQuery("#form17 div[wdid=110]").css("display", "none");
    }
    else {
      jQuery("#form17 div[wdid=110]").removeAttr("style");
    }
  });
    }
    function check_js17(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js17() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_4_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_4_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_58_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_58_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_12_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_12_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_15_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_15_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_59_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_59_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_35_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_35_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_25_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_25_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_60_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_60_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_36_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_36_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_40_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_40_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_61_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_61_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_37_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_37_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_48_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_48_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_62_allow_other17\" value=\"no\" />").appendTo("#form17");
  jQuery("<input type=\"hidden\" name=\"wdform_62_allow_other_num17\" value=\"0\" />").appendTo("#form17");
  var disabled_fields = "";	
  jQuery("#form17 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields17\" value =\""+disabled_fields+"\" />").appendTo("#form17");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(17);
    });
    form_view_count17 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(17);
    });
    