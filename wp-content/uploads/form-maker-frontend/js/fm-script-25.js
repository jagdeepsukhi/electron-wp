    var fm_currentDate = new Date();
    var FormCurrency_25 = '';
    var FormPaypalTax_25 = '0';
    var check_submit25 = 0;
    var check_before_submit25 = {};
    var required_fields25 = [];
    var labels_and_ids25 = {"3":"type_text","4":"type_text","5":"type_text","6":"type_text","7":"type_text","8":"type_date_new","18":"type_radio","19":"type_radio","20":"type_text","23":"type_own_select","22":"type_checkbox","21":"type_textarea","1":"type_submit_reset"};
    var check_regExp_all25 = [];
    var check_paypal_price_min_max25 = [];
    var file_upload_check25 = [];
    var spinner_check25 = [];
    var scrollbox_trigger_point25 = '20';
    var header_image_animation25 = 'none';
    var scrollbox_loading_delay25 = '0';
    var scrollbox_auto_hide25 = '1';
         function before_load25() {	
}	
 function before_submit25() {
	 }	
 function before_reset25() {	
}
    function onload_js25() {
  jQuery("#button_calendar_8, #fm-calendar-8").click(function() {
    jQuery("#wdform_8_element25").datepicker("show");
  });
  jQuery("#wdform_8_element25").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_8_element25").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_8_element25").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_8_element25").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_8_element25").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_8_element25").datepicker("setDate", default_date);
  }
    }
    function condition_js25() {
    }
    function check_js25(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js25() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_18_allow_other25\" value=\"no\" />").appendTo("#form25");
  jQuery("<input type=\"hidden\" name=\"wdform_18_allow_other_num25\" value=\"0\" />").appendTo("#form25");
  jQuery("<input type=\"hidden\" name=\"wdform_19_allow_other25\" value=\"no\" />").appendTo("#form25");
  jQuery("<input type=\"hidden\" name=\"wdform_19_allow_other_num25\" value=\"0\" />").appendTo("#form25");
  jQuery("<input type=\"hidden\" name=\"wdform_22_allow_other25\" value=\"no\" />").appendTo("#form25");
  jQuery("<input type=\"hidden\" name=\"wdform_22_allow_other_num25\" value=\"0\" />").appendTo("#form25");
  var disabled_fields = "";	
  jQuery("#form25 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields25\" value =\""+disabled_fields+"\" />").appendTo("#form25");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(25);
    });
    form_view_count25 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(25);
    });
    