    var fm_currentDate = new Date();
    var FormCurrency_26 = '';
    var FormPaypalTax_26 = '0';
    var check_submit26 = 0;
    var check_before_submit26 = {};
    var required_fields26 = [];
    var labels_and_ids26 = {"2":"type_name","3":"type_text","4":"type_text","5":"type_text","6":"type_text","7":"type_text","8":"type_date_new","9":"type_time","10":"type_file_upload","11":"type_file_upload","12":"type_file_upload","13":"type_file_upload","14":"type_file_upload","15":"type_file_upload","16":"type_name","1":"type_submit_reset"};
    var check_regExp_all26 = [];
    var check_paypal_price_min_max26 = [];
    var file_upload_check26 = {"10":"jpg, jpeg, png, gif, doc, docx, xls, xlsx","11":"jpg, jpeg, png, gif, doc, docx, xls, xlsx","12":"jpg, jpeg, png, gif, doc, docx, xls, xlsx","13":"jpg, jpeg, png, gif, doc, docx, xls, xlsx","14":"jpg, jpeg, png, gif, doc, docx, xls, xlsx","15":"jpg, jpeg, png, gif, doc, docx, xls, xlsx"};
    var spinner_check26 = [];
    var scrollbox_trigger_point26 = '20';
    var header_image_animation26 = 'none';
    var scrollbox_loading_delay26 = '0';
    var scrollbox_auto_hide26 = '1';
         function before_load26() {	
}	
 function before_submit26() {
	 }	
 function before_reset26() {	
}
    function onload_js26() {
  jQuery("#button_calendar_8, #fm-calendar-8").click(function() {
    jQuery("#wdform_8_element26").datepicker("show");
  });
  jQuery("#wdform_8_element26").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_8_element26").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_8_element26").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_8_element26").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_8_element26").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_8_element26").datepicker("setDate", default_date);
  }
    }
    function condition_js26() {
    }
    function check_js26(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js26() {
    
  var disabled_fields = "";	
  jQuery("#form26 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields26\" value =\""+disabled_fields+"\" />").appendTo("#form26");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(26);
    });
    form_view_count26 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(26);
    });
    