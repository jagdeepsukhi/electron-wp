    var fm_currentDate = new Date();
    var FormCurrency_10 = '$';
    var FormPaypalTax_10 = '0';
    var check_submit10 = 0;
    var check_before_submit10 = {};
    var required_fields10 = ["20","8","10","11","6"];
    var labels_and_ids10 = {"20":"type_date_new","8":"type_text","10":"type_text","11":"type_text","6":"type_own_select","7":"type_submit_reset","12":"type_hidden","13":"type_hidden","14":"type_hidden","16":"type_hidden","17":"type_hidden","18":"type_hidden","19":"type_hidden","15":"type_hidden"};
    var check_regExp_all10 = {"8":["%5E%5B0-9%5D%7B1%2C3%7D%28%3F%3A%5C.%5B0-9%5D%7B3%2C3%7D%29%24","","Fuel Amount must be documented to the third decimal place (XX.XXX)"],"10":["%5E%5B1-9%5D%5Cd*%24","","Odometer should be a positive integer"],"11":["%5E%5B0-9%5D%7B1%2C3%7D%28%3F%3A%5C.%5B0-9%5D%7B2%2C2%7D%29%24","","Fuel Cost must be entered to second decimal place"]};
    var check_paypal_price_min_max10 = [];
    var file_upload_check10 = [];
    var spinner_check10 = [];
    var scrollbox_trigger_point10 = '20';
    var header_image_animation10 = 'none';
    var scrollbox_loading_delay10 = '0';
    var scrollbox_auto_hide10 = '1';
         function before_load10() {
     
}

 function before_submit10() {
     
     /*     var myodo = jQuery("input[title='Enter Odometer Miles']").val();
var mylastodo = jQuery("input[name='last_odometer_for_mobile_functionality']").val();
var myfuelamount = jQuery("input[title='Enter Gallons (XX.XXX)']").val();
var lowmph = jQuery("input[name='low_mpg']").val();
var highmph = jQuery("input[name='high_mpg']").val();
var miles_traveled = myodo - mylastodo;
var mpg = miles_traveled / myfuelamount;
jQuery("input[name='covered_miles']").val(miles_traveled);
jQuery("input[name='mpg']").val(mpg);


if (((mpg < lowmph)  && (mpg > highmph)) && !(mph <=0) && (first_try)) {
jQuery("input[name='Enter Gallons (XX.XXX)']").css("background-color","#E6635B");
jQuery("input[name='Enter Gallons (XX.XXX)']").effect( "shake" );
jQuery("input[name='Enter Odometer Miles']").css("background-color","#E6635B");
jQuery("input[name='Enter Odometer Miles']").effect( "shake" );
first_try=false;
return false;
}
*/
    
}

 function before_reset10() {
     
}
    function onload_js10() {
  jQuery("#button_calendar_20, #fm-calendar-20").click(function() {
    jQuery("#wdform_20_element10").datepicker("show");
  });
  jQuery("#wdform_20_element10").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_20_element10").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_20_element10").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_20_element10").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_20_element10").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_20_element10").datepicker("setDate", default_date);
  }
    }
    function condition_js10() {
    }
    function check_js10(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js10() {
    
  var disabled_fields = "";	
  jQuery("#form10 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields10\" value =\""+disabled_fields+"\" />").appendTo("#form10");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(10);
    });
    form_view_count10 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(10);
    });
    