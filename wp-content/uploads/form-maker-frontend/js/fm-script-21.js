    var fm_currentDate = new Date();
    var FormCurrency_21 = '$';
    var FormPaypalTax_21 = '0';
    var check_submit21 = 0;
    var check_before_submit21 = {};
    var required_fields21 = [];
    var labels_and_ids21 = {"1":"type_name","2":"type_submitter_mail","5":"type_radio","8":"type_editor","6":"type_editor","4":"type_radio","7":"type_editor","9":"type_editor","10":"type_submit_reset"};
    var check_regExp_all21 = [];
    var check_paypal_price_min_max21 = [];
    var file_upload_check21 = [];
    var spinner_check21 = [];
    var scrollbox_trigger_point21 = '20';
    var header_image_animation21 = 'none';
    var scrollbox_loading_delay21 = '0';
    var scrollbox_auto_hide21 = '1';
         function before_load21() {
     
}

 function before_submit21() {
}

 function before_reset21() {
     
}
    function onload_js21() {
    }
    function condition_js21() {
  if( jQuery("input[name^='wdform_5_element21']:checked").val()=="Counter-clockwise" ) {
    jQuery("#form21 div[wdid=8]").removeAttr("style");
  }
  else {
    jQuery("#form21 div[wdid=8]").css("display", "none");
  }
  jQuery("div[wdid=5] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_5_element21']:checked").val()=="Counter-clockwise" ) {
      jQuery("#form21 div[wdid=8]").removeAttr("style");
    }
    else {
      jQuery("#form21 div[wdid=8]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_5_element21']:checked").val()!="Counter-clockwise" && jQuery("input[name^='wdform_5_element21']:checked").val()) {
    jQuery("#form21 div[wdid=6]").removeAttr("style");
  }
  else {
    jQuery("#form21 div[wdid=6]").css("display", "none");
  }
  jQuery("div[wdid=5] input[type='radio'], div[wdid=5] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_5_element21']:checked").val()!="Counter-clockwise" && jQuery("input[name^='wdform_5_element21']:checked").val()) {
      jQuery("#form21 div[wdid=6]").removeAttr("style");
    }
    else {
      jQuery("#form21 div[wdid=6]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_4_element21']:checked").val()=="20 MPH" ) {
    jQuery("#form21 div[wdid=7]").removeAttr("style");
  }
  else {
    jQuery("#form21 div[wdid=7]").css("display", "none");
  }
  jQuery("div[wdid=4] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_4_element21']:checked").val()=="20 MPH" ) {
      jQuery("#form21 div[wdid=7]").removeAttr("style");
    }
    else {
      jQuery("#form21 div[wdid=7]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_4_element21']:checked").val()!="20 MPH" && jQuery("input[name^='wdform_4_element21']:checked").val()) {
    jQuery("#form21 div[wdid=9]").removeAttr("style");
  }
  else {
    jQuery("#form21 div[wdid=9]").css("display", "none");
  }
  jQuery("div[wdid=4] input[type='radio'], div[wdid=4] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_4_element21']:checked").val()!="20 MPH" && jQuery("input[name^='wdform_4_element21']:checked").val()) {
      jQuery("#form21 div[wdid=9]").removeAttr("style");
    }
    else {
      jQuery("#form21 div[wdid=9]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_4_element21']:checked").val()=="20 MPH" ) {
    jQuery("#form21 div[wdid=10]").removeAttr("style");
  }
  else {
    jQuery("#form21 div[wdid=10]").css("display", "none");
  }
  jQuery("div[wdid=4] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_4_element21']:checked").val()=="20 MPH" ) {
      jQuery("#form21 div[wdid=10]").removeAttr("style");
    }
    else {
      jQuery("#form21 div[wdid=10]").css("display", "none");
    }
  });
    }
    function check_js21(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js21() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_5_allow_other21\" value=\"no\" />").appendTo("#form21");
  jQuery("<input type=\"hidden\" name=\"wdform_5_allow_other_num21\" value=\"0\" />").appendTo("#form21");
  jQuery("<input type=\"hidden\" name=\"wdform_4_allow_other21\" value=\"no\" />").appendTo("#form21");
  jQuery("<input type=\"hidden\" name=\"wdform_4_allow_other_num21\" value=\"0\" />").appendTo("#form21");
  var disabled_fields = "";	
  jQuery("#form21 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields21\" value =\""+disabled_fields+"\" />").appendTo("#form21");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(21);
    });
    form_view_count21 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(21);
    });
    