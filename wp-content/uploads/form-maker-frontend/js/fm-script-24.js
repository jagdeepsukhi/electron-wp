    var fm_currentDate = new Date();
    var FormCurrency_24 = '$';
    var FormPaypalTax_24 = '0';
    var check_submit24 = 0;
    var check_before_submit24 = {};
    var required_fields24 = ["2","3","4","16","26","36","48","58","17","27","37","49","59","18","28","38","50","60","19","29","39","51","61","20","30","40","52","62","21","31","41","53","63","22","32","42","54","64","23","33","44","55","65","24","34","45","56","66","25","35","46","57","67","88","89","90"];
    var labels_and_ids24 = {"2":"type_own_select","3":"type_date_new","4":"type_date_new","5":"type_textarea","6":"type_editor","16":"type_name","26":"type_text","36":"type_radio","48":"type_radio","58":"type_phone_new","68":"type_time","79":"type_checkbox","7":"type_editor","17":"type_name","27":"type_text","37":"type_radio","49":"type_radio","59":"type_phone_new","69":"type_time","80":"type_checkbox","8":"type_editor","18":"type_name","28":"type_text","38":"type_radio","50":"type_radio","60":"type_phone_new","70":"type_time","81":"type_checkbox","9":"type_editor","19":"type_name","29":"type_text","39":"type_radio","51":"type_radio","61":"type_phone_new","71":"type_time","82":"type_checkbox","10":"type_editor","20":"type_name","30":"type_text","40":"type_radio","52":"type_radio","62":"type_phone_new","72":"type_time","83":"type_checkbox","11":"type_editor","21":"type_name","31":"type_text","41":"type_radio","53":"type_radio","63":"type_phone_new","73":"type_time","84":"type_checkbox","12":"type_editor","22":"type_name","32":"type_text","42":"type_radio","54":"type_radio","64":"type_phone_new","74":"type_time","85":"type_checkbox","13":"type_editor","23":"type_name","33":"type_text","44":"type_radio","55":"type_radio","65":"type_phone_new","75":"type_time","86":"type_checkbox","14":"type_editor","24":"type_name","34":"type_text","45":"type_radio","56":"type_radio","66":"type_phone_new","76":"type_time","87":"type_checkbox","15":"type_editor","25":"type_name","35":"type_text","46":"type_radio","57":"type_radio","67":"type_phone_new","77":"type_time","91":"type_editor","88":"type_name","89":"type_phone_new","90":"type_submitter_mail","1":"type_submit_reset"};
    var check_regExp_all24 = [];
    var check_paypal_price_min_max24 = [];
    var file_upload_check24 = [];
    var spinner_check24 = [];
    var scrollbox_trigger_point24 = '20';
    var header_image_animation24 = 'none';
    var scrollbox_loading_delay24 = '0';
    var scrollbox_auto_hide24 = '1';
         function before_load24() {	
}	
 function before_submit24() {
	 }	
 function before_reset24() {	
}
    function onload_js24() {
  jQuery("#button_calendar_3, #fm-calendar-3").click(function() {
    jQuery("#wdform_3_element24").datepicker("show");
  });
  jQuery("#wdform_3_element24").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_3_element24").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_3_element24").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_3_element24").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_3_element24").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_3_element24").datepicker("setDate", default_date);
  }
  jQuery("#button_calendar_4, #fm-calendar-4").click(function() {
    jQuery("#wdform_4_element24").datepicker("show");
  });
  jQuery("#wdform_4_element24").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_4_element24").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_4_element24").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_4_element24").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_4_element24").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_4_element24").datepicker("setDate", default_date);
  }
								jQuery("#wdform_58_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_59_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_60_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_61_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_62_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_63_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_64_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_65_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_66_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_67_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
								jQuery("#wdform_89_element24").intlTelInput({
									nationalMode: false,
									preferredCountries: [ "us" ],
									customPlaceholder: "Phone",
								});
							
    }
    function condition_js24() {
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=7]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=7]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=7]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=7]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=17]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=17]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=17]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=17]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=37]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=37]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=37]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=37]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=49]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=49]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=49]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=49]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=59]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=59]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=59]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=59]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=69]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=69]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=69]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=69]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=80]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=80]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=80]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=80]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=8]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=8]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=8]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=8]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=18]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=18]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=18]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=18]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=28]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=28]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=28]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=28]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=38]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=38]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=38]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=38]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=50]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=50]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=50]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=50]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=60]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=60]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=60]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=60]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=70]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=70]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=70]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=70]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=81]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=81]").removeAttr("style");
  }
  jQuery("div[wdid=80] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=80] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=81]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=81]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=9]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=9]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=9]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=9]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=19]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=19]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=19]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=19]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=29]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=29]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=29]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=29]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=39]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=39]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=39]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=39]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=51]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=51]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=51]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=51]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=61]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=61]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=61]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=61]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=71]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=71]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=71]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=71]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=82]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=82]").removeAttr("style");
  }
  jQuery("div[wdid=81] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=81] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=82]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=82]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=10]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=10]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=10]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=10]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=20]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=20]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=20]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=20]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=30]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=30]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=30]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=30]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=40]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=40]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=40]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=40]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=52]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=52]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=52]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=52]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=62]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=62]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=62]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=62]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=72]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=72]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=72]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=72]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=83]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=83]").removeAttr("style");
  }
  jQuery("div[wdid=82] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=82] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=83]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=83]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=11]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=11]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=11]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=11]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=21]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=21]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=21]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=21]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=31]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=31]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=31]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=31]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=41]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=41]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=41]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=41]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=53]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=53]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=53]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=53]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=63]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=63]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=63]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=63]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=73]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=73]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=73]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=73]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=84]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=84]").removeAttr("style");
  }
  jQuery("div[wdid=83] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=83] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=84]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=84]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=12]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=12]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=12]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=12]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=22]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=22]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=22]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=22]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=32]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=32]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=32]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=32]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=42]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=42]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=42]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=42]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=54]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=54]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=54]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=54]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=64]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=64]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=64]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=64]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=74]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=74]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=74]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=74]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=85]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=85]").removeAttr("style");
  }
  jQuery("div[wdid=84] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=84] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=85]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=85]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=13]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=13]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=13]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=13]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=23]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=23]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=23]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=23]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=33]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=33]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=33]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=33]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=44]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=44]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=44]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=44]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=55]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=55]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=55]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=55]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=65]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=65]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=65]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=65]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=75]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=75]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=75]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=75]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=86]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=86]").removeAttr("style");
  }
  jQuery("div[wdid=85] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=85] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=86]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=86]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=14]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=14]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=14]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=14]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=24]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=24]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=24]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=24]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=34]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=34]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=34]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=34]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=45]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=45]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=45]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=45]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=56]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=56]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=56]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=56]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=66]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=66]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=66]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=66]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=76]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=76]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=76]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=76]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=87]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=87]").removeAttr("style");
  }
  jQuery("div[wdid=86] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=86] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=87]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=87]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=15]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=15]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=15]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=15]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=25]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=25]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=25]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=25]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=35]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=35]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=35]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=35]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=46]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=46]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=46]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=46]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=57]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=57]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=57]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=57]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=67]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=67]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=67]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=67]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=77]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=77]").removeAttr("style");
  }
  jQuery("div[wdid=87] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=87] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=77]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=77]").removeAttr("style");
    }
  });
  if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
    jQuery("#form24 div[wdid=27]").css("display", "none");
  }
  else {
    jQuery("#form24 div[wdid=27]").removeAttr("style");
  }
  jQuery("div[wdid=79] input[type='checkbox']").click(function() {
    if( jQuery("div[wdid=79] input[type=\"checkbox\"]:checked").length==0 ) {
      jQuery("#form24 div[wdid=27]").css("display", "none");
    }
    else {
      jQuery("#form24 div[wdid=27]").removeAttr("style");
    }
  });
    }
    function check_js24(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js24() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_36_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_36_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_48_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_48_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_79_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_79_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_37_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_37_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_49_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_49_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_80_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_80_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_38_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_38_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_50_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_50_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_81_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_81_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_39_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_39_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_51_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_51_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_82_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_82_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_40_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_40_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_52_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_52_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_83_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_83_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_41_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_41_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_53_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_53_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_84_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_84_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_42_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_42_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_54_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_54_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_85_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_85_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_44_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_44_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_55_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_55_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_86_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_86_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_45_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_45_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_56_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_56_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_87_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_87_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_46_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_46_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_57_allow_other24\" value=\"no\" />").appendTo("#form24");
  jQuery("<input type=\"hidden\" name=\"wdform_57_allow_other_num24\" value=\"0\" />").appendTo("#form24");
  var disabled_fields = "";	
  jQuery("#form24 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields24\" value =\""+disabled_fields+"\" />").appendTo("#form24");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(24);
    });
    form_view_count24 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(24);
    });
    