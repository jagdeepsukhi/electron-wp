    var fm_currentDate = new Date();
    var FormCurrency_19 = '$';
    var FormPaypalTax_19 = '0';
    var check_submit19 = 0;
    var check_before_submit19 = {};
    var required_fields19 = ["29","3","4","6","9","11","12","15","16","18","19","20","22"];
    var labels_and_ids19 = {"28":"type_date_new","29":"type_time","3":"type_radio","4":"type_textarea","6":"type_radio","9":"type_textarea","11":"type_radio","12":"type_textarea","15":"type_radio","16":"type_textarea","18":"type_radio","19":"type_textarea","20":"type_radio","22":"type_textarea","24":"type_textarea","25":"type_submit_reset"};
    var check_regExp_all19 = [];
    var check_paypal_price_min_max19 = [];
    var file_upload_check19 = [];
    var spinner_check19 = [];
    var scrollbox_trigger_point19 = '20';
    var header_image_animation19 = 'none';
    var scrollbox_loading_delay19 = '0';
    var scrollbox_auto_hide19 = '1';
         function before_load19() {	
}	
 function before_submit19() {
}	
 function before_reset19() {	
}
    function onload_js19() {
  jQuery("#button_calendar_28, #fm-calendar-28").click(function() {
    jQuery("#wdform_28_element19").datepicker("show");
  });
  jQuery("#wdform_28_element19").datepicker({
    dateFormat: format_date,
    minDate: "",
    maxDate: "",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+50",
    showOtherMonths: true,
    selectOtherMonths: true,
    firstDay: "0",
    beforeShow: function(input, inst) {
      jQuery("#ui-datepicker-div").addClass("fm_datepicker");
    },
    beforeShowDay: function(date) {
      var invalid_dates = "";
      var invalid_dates_finish = [];
      var invalid_dates_start = invalid_dates.split(",");
      var invalid_date_range =[];
      for(var i = 0; i < invalid_dates_start.length; i++ ) {
        invalid_dates_start[i] = invalid_dates_start[i].trim();
        if(invalid_dates_start[i].length < 11 || invalid_dates_start[i].indexOf("-") == -1){
          invalid_dates_finish.push(invalid_dates_start[i]);
        }
        else{
          if(invalid_dates_start[i].indexOf("-") > 4) {
            invalid_date_range.push(invalid_dates_start[i].split("-"));
          }
          else {
            var invalid_date_array = invalid_dates_start[i].split("-");
            var start_invalid_day = invalid_date_array[0] + "-" + invalid_date_array[1] + "-" + invalid_date_array[2];
            var end_invalid_day = invalid_date_array[3] + "-" + invalid_date_array[4] + "-" + invalid_date_array[5];
            invalid_date_range.push([start_invalid_day, end_invalid_day]);
          }
        }
      }
      jQuery.each(invalid_date_range, function( index, value ) {
        for(var d = new Date(value[0]); d <= new Date(value[1]); d.setDate(d.getDate() + 1)) {
          invalid_dates_finish.push(jQuery.datepicker.formatDate(format_date, d));
        }
      });
      var string_days = jQuery.datepicker.formatDate(format_date, date);
      var day = date.getDay();
      return [ invalid_dates_finish.indexOf(string_days) == -1 ];
    }
  });
  var default_date;  
  var date_value = jQuery("#wdform_28_element19").val();  
  (date_value != "") ? default_date = date_value : default_date = "";
  var format_date = "mm/dd/yy";
  jQuery("#wdform_28_element19").datepicker("option", "dateFormat", format_date);
  if(default_date == "today") {
    jQuery("#wdform_28_element19").datepicker("setDate", new Date());
  }
  else if (default_date.indexOf("d") == -1 && default_date.indexOf("m") == -1 && default_date.indexOf("y") == -1 && default_date.indexOf("w") == -1) {
    jQuery("#wdform_28_element19").datepicker("setDate", default_date);
  }
  else {
 
    jQuery("#wdform_28_element19").datepicker("setDate", default_date);
  }
    }
    function condition_js19() {
  if( jQuery("input[name^='wdform_3_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=4]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=4]").css("display", "none");
  }
  jQuery("div[wdid=3] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_3_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=4]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=4]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_6_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=9]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=9]").css("display", "none");
  }
  jQuery("div[wdid=6] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_6_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=9]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=9]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_11_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=12]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=12]").css("display", "none");
  }
  jQuery("div[wdid=11] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_11_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=12]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=12]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_15_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=16]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=16]").css("display", "none");
  }
  jQuery("div[wdid=15] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_15_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=16]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=16]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_18_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=19]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=19]").css("display", "none");
  }
  jQuery("div[wdid=18] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_18_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=19]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=19]").css("display", "none");
    }
  });
  if( jQuery("input[name^='wdform_20_element19']:checked").val()=="No" ) {
    jQuery("#form19 div[wdid=22]").removeAttr("style");
  }
  else {
    jQuery("#form19 div[wdid=22]").css("display", "none");
  }
  jQuery("div[wdid=20] input[type='radio']").click(function() {
    if( jQuery("input[name^='wdform_20_element19']:checked").val()=="No" ) {
      jQuery("#form19 div[wdid=22]").removeAttr("style");
    }
    else {
      jQuery("#form19 div[wdid=22]").css("display", "none");
    }
  });
    }
    function check_js19(id, form_id) {
    if (id != 0) {
    x = jQuery("#" + form_id + "form_view"+id);
    }
    else {
    x = jQuery("#form"+form_id);
    }    }
    function onsubmit_js19() {
    
  jQuery("<input type=\"hidden\" name=\"wdform_3_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_3_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_6_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_6_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_11_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_11_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_15_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_15_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_18_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_18_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_20_allow_other19\" value=\"no\" />").appendTo("#form19");
  jQuery("<input type=\"hidden\" name=\"wdform_20_allow_other_num19\" value=\"0\" />").appendTo("#form19");
  var disabled_fields = "";	
  jQuery("#form19 div[wdid]").each(function() {
    if(jQuery(this).css("display") == "none") {
      disabled_fields += jQuery(this).attr("wdid");
      disabled_fields += ",";
    }
    if(disabled_fields) {
      jQuery("<input type=\"hidden\" name=\"disabled_fields19\" value =\""+disabled_fields+"\" />").appendTo("#form19");
    }
  });    }
    jQuery(window).load(function () {
    formOnload(19);
    });
    form_view_count19 = 0;
    jQuery(document).ready(function () {
    fm_document_ready(19);
    });
    