<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the quiz settings page
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_summary_emails_register_quiz_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_quiz_settings_tabs( "Summary Emails", 'qsm_addon_summary_emails_quiz_settings_tabs_content' );
  }
}

/**
 * Generates the content for your quiz settings tab
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_summary_emails_quiz_settings_tabs_content() {

  global $mlwQuizMasterNext;

  //If nonce is set and correct, save the form
  if ( isset( $_POST["summary_emails_nonce"] ) && wp_verify_nonce( $_POST['summary_emails_nonce'], 'summary_emails') ) {
    $summary_emails_data = array(
      'enabled' => sanitize_text_field( $_POST["summary_emails_enable"] ),
      'frequency' => sanitize_text_field( $_POST["frequency"] ),
      'email_address' => sanitize_email( $_POST["email_address"] ),
      'subject' => sanitize_text_field( $_POST["subject"] ),
      'template' => htmlspecialchars( stripslashes( $_POST["template"] ), ENT_QUOTES )
    );
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting( "summary_emails_settings", $summary_emails_data );
    $mlwQuizMasterNext->alertManager->newAlert( 'Your Summary Emails settings has been saved successfully!', 'success' );
  }

  // Load the settings
  $summary_emails_data = $mlwQuizMasterNext->pluginHelper->get_quiz_setting( "summary_emails_settings" );
  $summary_emails_defaults = array(
    'enabled' => 'no',
    'frequency' => 'weekly',
    'email_address' => '',
    'subject' => 'Quiz Summary Email',
    'template' => '%QUESTIONS_ANSWERS%'
  );
  $summary_emails_data = wp_parse_args( $summary_emails_data, $summary_emails_defaults );

  ?>
  <h2>Summary Emails</h2>
  <form action="" method="post">
    <button class="button-primary">Save Settings</button>
    <table class="form-table" style="width: 100%;">
      <tr valign="top">
        <th scope="row"><label for="summary_emails_enable">Enable summary emails for this quiz/survey?</label></th>
        <td>
            <input type="radio" id="summary_emails_enable_radio1" name="summary_emails_enable" <?php checked( $summary_emails_data["enabled"], 'yes' ); ?> value='yes' /><label for="summary_emails_enable_radio1">Yes</label><br>
            <input type="radio" id="summary_emails_enable_radio2" name="summary_emails_enable" <?php checked( $summary_emails_data["enabled"], 'no' ); ?> value='no' /><label for="summary_emails_enable_radio2">No</label><br>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="frequency">Daily or weekly?</label></th>
        <td>
            <input type="radio" id="frequency_radio1" name="frequency" <?php checked( $summary_emails_data["frequency"], 'weekly' ); ?> value='weekly' /><label for="frequency_radio1">Weekly</label><br>
            <input type="radio" id="frequency_radio2" name="frequency" <?php checked( $summary_emails_data["frequency"], 'daily' ); ?> value='daily' /><label for="frequency_radio2">Daily</label><br>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="email_address">Email address to send summary to?</label></th>
        <td>
            <input type="text" id="email_address" name="email_address" value='<?php echo $summary_emails_data["email_address"]; ?>' />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="subject">Subject for email?</label></th>
        <td>
            <input type="text" id="subject" name="subject" value='<?php echo $summary_emails_data["subject"]; ?>' />
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="template">Template for each result in email?</label></th>
        <td>
            <textarea cols='80' rows='15' name="template" id="template"><?php echo $summary_emails_data["template"]; ?></textarea>
        </td>
      </tr>
    </table>
    <?php wp_nonce_field('summary_emails','summary_emails_nonce'); ?>
    <button class="button-primary">Save Settings</button>
  </form>
  <?php
}
?>
