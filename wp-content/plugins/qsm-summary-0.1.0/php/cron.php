<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * @since 0.1.0
 */
class QSM_Summary_Cron {

  /**
   * Main constructor function
   *
   * @uses QSM_Summary_Cron::add_hooks
   * @since 0.1.0
   */
  function __construct() {
    $this->add_hooks();
  }

  /**
   * Adds the hooks related to the cron
   *
   * @since 0.1.0
   */
  public function add_hooks() {
    add_filter( 'cron_schedules', array( $this, 'custom_schedule') );
    add_action( 'init', array( $this, 'cron_check' ) );
    add_action( 'qsm_summary_weekly', array( $this, 'weekly_cron' ) );
    add_action( 'qsm_summary_daily', array( $this, 'daily_cron' ) );
  }

  /**
   * Adds a 'weekly' option for cron
   *
   * @param array $schedules An array of non-default cron schedules
   * @return array Filtered array of non-default cron schedules
   */
  public function custom_schedule( $schedules ) {
    $schedules[ 'weekly' ] = array( 'interval' => 604800, 'display' => 'Once a week' );
    return $schedules;
  }

  /**
   * Checks to see if the cron is currently scheduled
   *
   * @since 0.1.0
   */
  public function cron_check() {
    if ( ! wp_next_scheduled( 'qsm_summary_weekly' ) ) {
      wp_schedule_event( current_time('timestamp'), 'weekly', 'qsm_summary_weekly');
    }
    if ( ! wp_next_scheduled( 'qsm_summary_daily' ) ) {
      wp_schedule_event( current_time('timestamp'), 'daily', 'qsm_summary_daily');
    }
  }

  /**
   * The weekly cron
   *
   * @uses QSM_Summary_Cron::cron
   * @since 0.1.0
   */
  public function weekly_cron() {
    $this->cron( 'weekly' );
  }

  /**
   * The daily cron
   *
   * @uses QSM_Summary_Cron::cron
   * @since 0.1.0
   */
  public function daily_cron() {
    $this->cron( 'daily' );
  }

  /**
   * Runs the cron
   */
  public function cron( $frequency ) {

    global $wpdb;

    // Retrieves all active quizzes and cycles through them
    $quizzes = $wpdb->get_results( "SELECT quiz_id, quiz_settings FROM {$wpdb->prefix}mlw_quizzes WHERE deleted=0" );
    foreach( $quizzes as $quiz ) {

      // Checks the quiz settings to ensure they exist and are in an array
      $settings = $quiz->quiz_settings;
      if ( is_serialized( $settings ) && is_array( @unserialize( $settings ) ) ) {
        $settings = unserialize( $settings );

        // Checks if the summary email settings exist
        if ( is_array( $settings ) && isset( $settings["summary_emails_settings"] ) ) {

          // Sets up defaults
          $summary_emails_defaults = array(
            'enabled' => 'no',
            'frequency' => 'weekly',
            'email_address' => '',
            'subject' => 'Quiz Summary Email',
            'template' => '%QUESTIONS_ANSWERS%'
          );
          $summary_emails_data = wp_parse_args( $settings["summary_emails_settings"], $summary_emails_defaults );

          // Checks if the summary emails are enabled and the frequency matches the cron's parameter
          if ( 'yes' == $summary_emails_data["enabled"] && $frequency == $summary_emails_data["frequency"] ) {

            $email_string = '';

            // Sets start time for SQL
            if ( 'daily' == $frequency ) {
              $start_time = date( "Y-m-d H:i:s", strtotime( '-1 day' ) );
            } else {
              $start_time = date( "Y-m-d H:i:s", strtotime( '-1 week' ) );
            }

            $now = date( "Y-m-d H:i:s");

            // Retrieves results and cycles through them
            $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}mlw_results WHERE quiz_id = {$quiz->quiz_id} AND (time_taken_real BETWEEN '$start_time' AND '$now') AND deleted = 0" );
            foreach ( $results as $result ) {

              // Prepare result data
              if ( is_serialized( $result->quiz_results ) && is_array( @unserialize( $result->quiz_results ) ) ) {
          			$result_array = unserialize( $result->quiz_results );
          		} else {
          			$result_array = array( 0, '', '' );
          		}

              // Prepare result array
              $quiz_results = array(
          			'quiz_id' => $result->quiz_id,
          			'quiz_name' => $result->quiz_name,
          			'quiz_system' => $result->quiz_system,
          			'user_name' => $result->name,
          			'user_business' => $result->business,
          			'user_email' => $result->email,
          			'user_phone' => $result->phone,
          			'user_id' => $result->user,
          			'timer' => $result_array[0],
          			'time_taken' => $result->time_taken,
          			'total_points' => $result->point_score,
          			'total_score' => $result->correct_score,
          			'total_correct' => $result->correct,
          			'total_questions' => $result->total,
          			'comments' => $result_array[2],
          			'question_answers_array' => $result_array[1]
          		);

              // Adds result to end of email
              $email_string .= apply_filters( 'mlw_qmn_template_variable_results_page', $summary_emails_data["template"], $quiz_results ) . "<br><hr><br>";
            }

            // Converts all new lines into standard HTML line breaks
            $email_string = str_replace( "\n" , "<br>", $email_string );
						$email_string = str_replace( "<br/>" , "<br>", $email_string );
						$email_string = str_replace( "<br />" , "<br>", $email_string );

            // Sends summary email
            if ( is_email( $summary_emails_data["email_address"] ) ) {

              // Sets up filter to enable HTML emails
              add_filter( 'wp_mail_content_type', 'mlw_qmn_set_html_content_type' );
              wp_mail( $summary_emails_data["email_address"], $summary_emails_data["subject"], $email_string );

              // Removes filter
              remove_filter( 'wp_mail_content_type', 'mlw_qmn_set_html_content_type' );
            }
          }
        }
      }
    }
  }
}

$qsm_summary_cron = new QSM_Summary_Cron();
?>
