<?php
//if uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

delete_option( 'qsm_addon_summary_emails_settings' );
wp_clear_scheduled_hook( 'qsm_summary_weekly' );
wp_clear_scheduled_hook( 'qsm_summary_daily' );
?>
