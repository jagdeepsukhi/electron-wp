<style>

    @-ms-viewport { 
        width: device-width;
        height: device-height;
    }  

    /* stec style generator */

    <?php
    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

    // top
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_top', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_top', 'top_important'));

    // agenda
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_agenda', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_agenda', 'agenda_important'));

    // day
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_day', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_day', 'day_important'));

    // preview
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_preview', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_preview', 'preview_important'));

    // month and week
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_monthweek', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_monthweek', 'monthweek_important'));

    // inner event
    $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_event', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_event', 'event_important'));

    // tooltip css
    echo $stachethemes_ec_main->get_style_from_setting('stec_menu__fontsandcolors_tooltip', $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_tooltip', 'tooltip_important'));

    // custom css
    echo $stachethemes_ec_main->get_admin_setting_value('stec_menu__fontsandcolors_custom', 'custom_css');

    ?>

</style>
