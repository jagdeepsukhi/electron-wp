<?php

// Add weekly interval to wp cron schedule
function stec_cron_weekly_filter($schedules) {
    
    $stachethemes_ec_main = stachethemes_ec_main::get_instance();
    
    // add a 'weekly' schedule to the existing set
    $schedules['weekly'] = array(
            'interval' => 604800,
            'display'  => $stachethemes_ec_main->lang('Once Weekly')
    );
    return $schedules;
}

add_filter('cron_schedules', 'stec_cron_weekly_filter');

/**
 * Register Cronjobs on Activate
 */
register_activation_hook(__DIR__ . '/stachethemes_event_calendar.php', function() {

    // register cronjob
    wp_schedule_event(time(), 'hourly', 'stec_cronjobs_hourly');
    wp_schedule_event(time(), 'twicedaily', 'stec_cronjobs_twice_daily');
    wp_schedule_event(time(), 'daily', 'stec_cronjobs_daily');
    wp_schedule_event(time(), 'weekly', 'stec_cronjobs_weekly');
});




/**
 * De-register Cronjobs on De-activate
 */
register_deactivation_hook(__DIR__ . '/stachethemes_event_calendar.php', function() {
    // clear cron
    wp_clear_scheduled_hook('stec_cronjobs_hourly');
    wp_clear_scheduled_hook('stec_cronjobs_twice_daily');
    wp_clear_scheduled_hook('stec_cronjobs_daily');
    wp_clear_scheduled_hook('stec_cronjobs_weekly');
});







/**
 * Reminder Cronjob
 */
add_action('stec_cronjobs_hourly', 'stec_cronjobs_reminder');



function stec_cronjobs_reminder() {

    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

    if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'reminder') == '1' ) {

        stachethemes_ec_admin::cronjobs_reminder();
    }
}

/**
 * Importer Cronjob
 */
// hourly import
add_action('stec_cronjobs_hourly', 'stec_cronjobs_import_hourly');



function stec_cronjobs_import_hourly() {

    stachethemes_ec_admin::cronjobs_import(0);
}

// twice daily import
add_action('stec_cronjobs_twice_daily', 'stec_cronjobs_import_twice_daily');



function stec_cronjobs_import_twice_daily() {

    stachethemes_ec_admin::cronjobs_import(1);
}

// once daily import
add_action('stec_cronjobs_daily', 'stec_cronjobs_import_daily');



function stec_cronjobs_import_daily() {

    stachethemes_ec_admin::cronjobs_import(2);
}

// weekly import && lcns check
add_action('stec_cronjobs_weekly', 'stec_cronjobs_import_weekly');



function stec_cronjobs_import_weekly() {

    stachethemes_ec_admin::cronjobs_import(3);
}
