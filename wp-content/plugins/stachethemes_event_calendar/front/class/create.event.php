<?php




/**
 * Front Create/Delete event
 */
class stachethemes_event_calendar_create_event_front {



    public static function post($var, $default = false, $filter = FILTER_DEFAULT, $opt = FILTER_REQUIRE_SCALAR) {

        return stachethemes_ec_admin::post($var, $default, $filter, $opt);
    }



    public static function make_alias($summary) {

        $alias = sanitize_title($summary);

        return stachethemes_ec_admin::validate_event_alias($alias);
    }

    /* Get relevant event $_POST data -- FRONT -- */



    public static function calendar_front_post_data() {

        $event_data = new stdClass();

        $event_data->calendar_id        = self::post('calendar_id', false, FILTER_VALIDATE_INT);
        $event_data->summary            = self::post('summary', '');
        $event_data->alias              = self::make_alias($event_data->summary);
        $event_data->color              = self::post('event_color');
        $event_data->icon               = self::post('icon');
        $event_data->visibility         = 'stec_cal_default';
        $event_data->featured           = self::post('featured');
        $event_data->start_date         = self::post('start_date');
        $event_data->start_time_hours   = self::post('start_time_hours');
        $event_data->start_time_minutes = self::post('start_time_minutes');
        $event_data->end_date           = self::post('end_date');
        $event_data->end_time_hours     = self::post('end_time_hours');
        $event_data->end_time_minutes   = self::post('end_time_minutes');
        $event_data->rrule              = self::post('rrule', '');
        $event_data->exdate             = self::post('exdate', '');
        $event_data->is_advanced_rrule  = self::post('is_advanced_rrule', 0);
        $event_data->keywords           = self::post('keywords');
        $event_data->all_day            = self::post('all_day', false) !== false ? 1 : 0;
        $event_data->counter            = self::post('counter');
        $event_data->comments           = 0;
        $event_data->link               = self::post('link');
        $event_data->images             = array();
        $event_data->location           = self::post('location', '');
        $event_data->description        = self::post('description', '');
        $event_data->description_short  = self::post('description_short', '');
        $event_data->contact_email      = self::post('contact_email', false, FILTER_VALIDATE_EMAIL);
        $event_data->review_note        = self::post('review_note', '');
        $event_data->approved           = 0;

        $calendar = stachethemes_event_calendar_query::get_calendar($event_data->calendar_id);

        if ( isset($calendar->req_approval) && $calendar->req_approval == 0 ) {
            $event_data->approved = 1;
        }

        return $event_data;
    }



    public static function get_user_cookie($create = true) {

        $cookie_name = "stachethemes_ec_anon_user";

        if ( !isset($_COOKIE[$cookie_name]) ) {

            if ( $create === true ) {

                $id = uniqid('stec-user-');

                if ( setcookie($cookie_name, $id, time() + (10 * 365 * 24 * 60 * 60), "/") === false ) {
                    return false;
                }

                $_COOKIE[$cookie_name] = $id;
            }
        }

        return strlen($_COOKIE[$cookie_name]) > 13 ? $_COOKIE[$cookie_name] : false;
    }



    /**
     * Creates event in #__stec_events
     * 
     * @global object $wpdb
     * @param object $general
     * @param object $optional
     * @return event|false
     */
    public static function create_event($event_data) {

        global $wpdb;

        $can_create_event = self::_if_user_can_create_event($event_data->calendar_id);

        if ( $can_create_event !== true ) {
            return false;
        }

        if ( trim($event_data->summary == "") ) {
            return false;
        }

        if ( is_nan($event_data->calendar_id) ) {
            return false;
        }

        $user_id     = 0;
        $user_cookie = false;

        if ( is_user_logged_in() ) {

            $user_id     = get_current_user_id();
            $user_cookie = self::get_user_cookie(false); // optional cookie if exists
        } else {

            $user_cookie = self::get_user_cookie(); // require cookie, create if not exists

            if ( $user_cookie === false ) {
                return false;
            }
        }

        if ( $event_data->contact_email === false ) { // require email
            return false;
        }

        $start_date = $event_data->start_date . ' ' . $event_data->start_time_hours . ':' . $event_data->start_time_minutes . ':00';
        $end_date   = $event_data->end_date . ' ' . $event_data->end_time_hours . ':' . $event_data->end_time_minutes . ':00';

        $wpdb->show_errors();

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events', array(
                'created_by'        => $user_id,
                'calid'             => $event_data->calendar_id,
                'summary'           => $event_data->summary,
                'alias'             => $event_data->alias,
                'color'             => $event_data->color,
                'icon'              => $event_data->icon,
                'visibility'        => $event_data->visibility,
                'featured'          => $event_data->featured,
                'start_date'        => $start_date,
                'end_date'          => $end_date,
                'keywords'          => $event_data->keywords,
                'all_day'           => $event_data->all_day,
                'counter'           => $event_data->counter,
                'comments'          => $event_data->comments,
                'link'              => $event_data->link,
                'location'          => $event_data->location,
                'description'       => $event_data->description,
                'description_short' => $event_data->description_short,
                'images'            => $event_data->images ? implode(',', $event_data->images) : '',
                'approved'          => $event_data->approved,
                ), array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%s',
                '%d',
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d'
                )
        );


        if ( $query === false ) {

            $wpdb->print_error();

            return false;
        }

        $event_id = (int) $wpdb->insert_id;

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events_repeater', array(
                'eventid'           => $event_id,
                'rrule'             => $event_data->rrule,
                'exdate'            => $event_data->exdate,
                'is_advanced_rrule' => $event_data->is_advanced_rrule
                ), array(
                '%d',
                '%s',
                '%s',
                '%s',
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events_meta', array(
                'eventid'           => $event_id,
                'created_by_cookie' => $user_cookie ? $user_cookie : NULL,
                'contact_email'     => $event_data->contact_email,
                'review_note'       => $event_data->review_note,
                'uid'               => uniqid('e-') . '_' . md5(microtime()) . '@stachethemes_ec.com'
                ), array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s'
                )
        );

        if ( $query === false ) {

            $wpdb->print_error();

            return false;
        }


        $event = stachethemes_event_calendar_query::get_events(false, $event_id, false);

        return isset($event) && !empty($event) ? $event[0] : false;
    }



    public static function is_image($mediapath) {
        return @is_array(getimagesize($mediapath));
    }



    public static function calendar_front_image_file_upload() {

        if ( !isset($_FILES['fileimage']) ) {
            return false;
        }

        $ext = strtolower(pathinfo($_FILES['fileimage']['name'], PATHINFO_EXTENSION));

        if ( $ext != 'jpg' && $ext != 'png' ) {
            return false;
        }

        if ( self::is_image($_FILES['fileimage']['tmp_name']) === false ) {
            return false;
        }

        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        $attachment_id = media_handle_upload('fileimage', 0);

        if ( is_wp_error($attachment_id) ) {

            return false;
        } else {

            return $attachment_id;
        }
    }



    public static function get_max_upload_size() {
        $u_bytes = wp_convert_hr_to_bytes(ini_get('upload_max_filesize'));
        $p_bytes = wp_convert_hr_to_bytes(ini_get('post_max_size'));
        $bytes   = apply_filters('upload_size_limit', min($u_bytes, $p_bytes), $u_bytes, $p_bytes);
        return round($bytes * 1e-6);
    }



    /**
     * Delete event ( #__stec_events, #__stec_events_repeater, #__stec_events_meta tables only! )
     * @global object $wpdb
     * @param int $event_id
     * @return boolean
     */
    public static function delete_event($event_id) {

        global $wpdb;

        if ( is_nan($event_id) ) {
            return false;
        }

        if ( is_user_logged_in() ) {
            $user_id = get_current_user_id();

            $sql = $wpdb->prepare("DELETE events, repeater, meta "
                    . "FROM {$wpdb->prefix}stec_events AS events "
                    . "LEFT JOIN {$wpdb->prefix}stec_events_meta AS meta ON events.id = meta.eventid "
                    . "LEFT JOIN {$wpdb->prefix}stec_events_repeater AS repeater ON events.id = repeater.eventid "
                    . "WHERE events.id = {$event_id} AND events.created_by = '%d' ", $user_id);

            $query = $wpdb->query($sql);

            if ( $query === false ) {
                return false;
            }
        }

        $user_cookie = self::get_user_cookie(false); // dont create , just get if exists

        if ( $user_cookie !== false ) {

            $sql = $wpdb->prepare("DELETE events, meta "
                    . "FROM {$wpdb->prefix}stec_events AS events "
                    . "LEFT JOIN {$wpdb->prefix}stec_events_meta AS meta ON events.id = meta.eventid "
                    . "LEFT JOIN {$wpdb->prefix}stec_events_repeater AS repeater ON events.id = repeater.eventid "
                    . "WHERE events.id = {$event_id} AND meta.created_by_cookie = '%s' ", $user_cookie);

            $query = $wpdb->query($sql);

            if ( $query === false ) {
                return false;
            }
        }

        return true;
    }



    /**
     * Returns array with icons
     * @return array
     */
    public static function get_icon_list() {
        return stachethemes_ec_admin::icon_list();
    }



    /**
     * Returns hours list
     * @return array
     */
    public static function get_hours_array() {



        return stachethemes_ec_admin::hours_array();
    }



    /**
     * Returns minutes list
     * @return array
     */
    public static function get_minutes_array() {



        return stachethemes_ec_admin::minutes_array();
    }



    /**
     * Returns list of calendars where this user can store events from the front-end
     * @returns object array 
     */
    public static function get_writable_calendar_list() {

        global $wpdb;

        $userid = is_user_logged_in() ? get_current_user_id() : false;

        if ( !$userid || $userid === 0 ) {
            $userid = -1; // set impossible userid
        }

        $filter = self::_get_calendar_writable_filter($userid);

        $result = $wpdb->get_results("SELECT CAL.id,CAL.color,CAL.title from {$wpdb->prefix}stec_calendars as CAL " . $filter, OBJECT);

        return $result !== false ? $result : array();
    }



    private static function _get_calendar_writable_filter($userid) {

        $user_roles = self::_get_user_roles($userid);

        $vis_filter = " WHERE ((CAL.writable IN ($user_roles)) ";

//      Uncomment lines below if you want to exclude super admin from this limitation
//        if (is_super_admin($userid)) {
//            $vis_filter .= ' OR 1 ) ';
//        } else {
        $vis_filter .= ' ) ';
//        }

        return $vis_filter;
    }



    /**
     * 
     * @param int $userid
     * @return string '...','...','...'
     */
    private static function _get_user_roles($userid) {

        $userid = (int) $userid;

        $user_info    = get_userdata($userid);
        $user_roles   = isset($user_info->roles) ? $user_info->roles : array();
        $user_roles[] = 'stec_public';

        if ( is_user_logged_in() ) {
            $user_roles[] = 'stec_logged_in';
        }

        $user_roles = "'" . implode("','", $user_roles) . "'";

        return $user_roles;
    }



    private static function _if_user_can_create_event($calid) {

        global $wpdb;

        $userid = is_user_logged_in() ? get_current_user_id() : false;

        if ( !$userid || $userid === 0 ) {
            $userid = -1; // set impossible userid
        }

        $filter = self::_get_calendar_writable_filter($userid);
        $filter .= " AND (CAL.id = '{$calid}') ";

        $result = $wpdb->get_results("SELECT CAL.id,CAL.title from {$wpdb->prefix}stec_calendars as CAL " . $filter, OBJECT);

        if ( $result === false ) {
            return false;
        }

        if ( empty($result) ) {
            return false;
        }

        return true;
    }



    public static function validate_captcha($secret, $response, $ip) {

        $fields = array(
                'secret'   => $secret,
                'response' => $response,
                'remoteip' => $ip
        );

        $fields_string = "";

        foreach ( $fields as $key => $value ) {

            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        $result = curl_exec($ch);

        curl_close($ch);

        $json = json_decode($result);

        return isset($json->success) && $json->success == 'true' ? true : false;
    }



    /**
     * Notify admin an event is awaiting approval via mail
     * @param int $event_id the event id
     */
    public static function notify_admin($event_id) {

        $stachethemes_ec = stachethemes_ec_main::get_instance();
        $event           = stachethemes_ec_admin::event($event_id);
        $user_info       = get_user_by('id', $event->created_by);
        $author          = isset($user_info->display_name) ? $user_info->display_name : 'Anonymous';
        $to              = get_option('admin_email');
        $subject         = $stachethemes_ec->lang('A new event is awaiting your approval');
        $admin_url       = admin_url('admin.php?page=stec_menu__events&view=edit&calendar_id=' . $event->calid . '&event_id=' . $event->id);
        $content         = sprintf($stachethemes_ec->lang("A new event has been submitted by %s. \n\nTo review the event visit %s"), $author, $admin_url);

        wp_mail($to, $subject, $content);
    }

}
