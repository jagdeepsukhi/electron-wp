<?php




/**
 * Front-end public exports
 * Goes through vis filter
 */
class stachethemes_event_calendar_export {



    /**
     * 
     * @param int $userid
     * @return string '...','...','...'
     */
    private static function get_user_roles($userid) {

        $userid = (int) $userid;

        $user_info    = get_userdata($userid);
        $user_roles   = isset($user_info->roles) ? $user_info->roles : array();
        $user_roles[] = 'stec_public';

        if ( is_user_logged_in() ) {
            $user_roles[] = 'stec_logged_in';
        }

        $user_roles = "'" . implode("','", $user_roles) . "'";

        return $user_roles;
    }



    private static function get_cal_vis_filter($userid, $join = 'WHERE') {

        $user_roles = self::get_user_roles($userid);

        $vis_filter = " $join ( (calendar.visibility IN ($user_roles) OR calendar.created_by = {$userid}) ";

        if ( is_super_admin($userid) ) {
            $vis_filter .= ' OR 1 ) ';
        } else {
            $vis_filter .= ' ) ';
        }

        return $vis_filter;
    }



    private static function get_event_vis_filter($userid, $join = 'WHERE') {

        $user_roles = self::get_user_roles($userid);

        $vis_filter = " $join ((event.visibility IN ($user_roles) OR event.created_by = {$userid} OR attendance.userid = {$userid}) ";
        $vis_filter .= " OR (event.visibility = 'stec_cal_default' AND calendar.visibility IN ($user_roles)) ";

        if ( is_super_admin($userid) ) {
            $vis_filter .= ' OR 1 ) ';
        } else {
            $vis_filter .= ' ) ';
        }

        return $vis_filter;
    }



    private static function calendar($calendar_id) {

        global $wpdb;

        $filter = self::get_cal_vis_filter(get_current_user_id(), 'AND');

        $query = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}stec_calendars as calendar WHERE id='{$calendar_id}' $filter ", OBJECT);

        return $query !== false ? $query : false;
    }



    private static function events($calendar_id) {


        global $wpdb;

        $filter = self::get_event_vis_filter(get_current_user_id());

        $query = $wpdb->get_results(" 
                SELECT event.*, repeater.rrule, repeater.exdate, repeater.is_advanced_rrule, meta.uid, meta.recurrence_id FROM {$wpdb->prefix}stec_events as event
                LEFT JOIN {$wpdb->prefix}stec_calendars as calendar ON event.calid = calendar.id
                LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON event.id = repeater.eventid 
                LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON event.id = meta.eventid 
                LEFT JOIN {$wpdb->prefix}stec_attendance as attendance ON event.id = attendance.eventid 
                $filter AND event.calid = '{$calendar_id}' AND approved = 1
                ORDER BY event.approved ASC, event.start_date DESC, event.id DESC ", OBJECT);
        return $query !== false ? $query : array();
    }



    private static function event($event_id) {
        global $wpdb;

        $filter = self::get_event_vis_filter(get_current_user_id(), 'AND');

        $event = $wpdb->get_row(""
                . " SELECT event.*, repeater.rrule, repeater.exdate, repeater.is_advanced_rrule, meta.review_note, meta.contact_email, meta.recurrence_id, meta.uid "
                . " FROM {$wpdb->prefix}stec_events as event "
                . " LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON  "
                . " event.id = repeater.eventid "
                . " LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON  "
                . " event.id = meta.eventid "
                . " WHERE event.id = '{$event_id}' $filter ", OBJECT);

        return $event;
    }



    public static function can_export($calendar_id, $event_id = false) {

        if ( is_nan($calendar_id) ) {
            return false;
        }
        
        $calendar = self::calendar($calendar_id);

        if ( !$calendar ) {
            return false;
        }
        
        if ( $event_id !== false ) :
            $event = self::event($event_id);
            if ( $event ) {
                return true;
            }
        endif;
        
        if ( $event_id === false ) {
            $events = self::events($calendar_id);
        }
        
        return !empty($events);

    }



    public static function export_ics($calendar_id, $event_id = false) {

        if ( is_nan($calendar_id) ) {
            return false;
        }

        $calendar = self::calendar($calendar_id);

        if ( !$calendar ) {
            return false;
        }

        $export   = new stachethemes_ec_ical_export($calendar);
        $events   = array();
        $filename = false;

        // include event by single id
        if ( $event_id !== false ) :

            $event    = self::event($event_id);
            $events[] = $event;

            $filename = $event->summary;

        endif;


        if ( $event_id === false ) {
            // Export all events from calendar id
            $events = self::events($calendar_id);
        }

        if ( empty($events) ) {
            return true;
        }

        foreach ( $events as $event ) :

            if ( empty($event) ) {
                continue;
            }

            $export->proccess_event($event);

        endforeach;


        $export->download($filename);
    }

}
