<?php




/**
 * Helper class function for single pages
 */
class stachethemes_ec_single {



    public static function meta_schema_datetime_iso8601($event) {

        $format          = 'Y-m-d\TH:i:sO';
        $start_date_unix = strtotime($event->general->start_date) + $event->general->repeat_time_offset;
        $end_date_unix   = strtotime($event->general->end_date) + $event->general->repeat_time_offset;
        $start           = date($format, $start_date_unix);
        $end             = date($format, $end_date_unix);
        ?>
        <meta itemprop="startDate" content="<?php echo $start; ?>"/>
        <meta itemprop="endDate" content="<?php echo $end; ?>"/>
        <?php
    }



    /**
     * Returns human readable format of the timespan
     * @param object $event the event object
     * @return string the timespan
     */
    public static function get_the_timespan($event) {

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        $dateformat      = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'date_format');
        $timeformat      = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'time_format');
        $show_utc_offset = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'date_label_gmtutc');
        $start_date_unix = strtotime($event->general->start_date) + $event->general->repeat_time_offset;
        $end_date_unix   = strtotime($event->general->end_date) + $event->general->repeat_time_offset;

        // Translate months
        $sm = '<span class="stec-layout-single-month-full">' . ucfirst($stachethemes_ec_main->lang(strtolower(date('F', $start_date_unix)))) . '</span>';
        $em = '<span class="stec-layout-single-month-full">' . ucfirst($stachethemes_ec_main->lang(strtolower(date('F', $end_date_unix)))) . '</span>';

        $sm .= '<span class="stec-layout-single-month-short">' . ucfirst($stachethemes_ec_main->lang(strtolower(date('M', $start_date_unix)))) . '</span>';
        $em .= '<span class="stec-layout-single-month-short">' . ucfirst($stachethemes_ec_main->lang(strtolower(date('M', $end_date_unix)))) . '</span>';

        switch ( $dateformat ) :

            case 'dd-mm-yy' :

                $start_date = '<span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ' .
                        $sm .
                        ' <span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span>';

                $end_date = '<span class="stec-layout-single-day">' . date('d', $end_date_unix) . '</span> ' .
                        $sm .
                        ' <span class="stec-layout-single-year">' . date('Y', $end_date_unix) . '</span>';
                break;

            case 'mm-dd-yy' :

                $start_date = $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ' .
                        ' <span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span>';

                $end_date = $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $end_date_unix) . '</span> ' .
                        ' <span class="stec-layout-single-year">' . date('Y', $end_date_unix) . '</span>';

                break;

            case 'yy-mm-dd' :
            default:
                $start_date = ' <span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span>' .
                        $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ';

                $end_date = ' <span class="stec-layout-single-year">' . date('Y', $end_date_unix) . '</span>' .
                        $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $end_date_unix) . '</span> ';

                break;
        endswitch;

        switch ( $timeformat ) :
            case '12':
                $timeformat = 'h:ia';
                break;

            case '24':
                $timeformat = 'H:i';
            default:

        endswitch;

        $start_time = '<span class="stec-layout-single-start-time">' . date($timeformat, $start_date_unix) . '</span>';
        $end_time   = '<span class="stec-layout-single-end-time">' . date($timeformat, $end_date_unix) . '</span>';

        if ( $start_date == $end_date ) {

            if ( $event->general->all_day == '1' ) {
                $the_date = $start_date;
            } else {
                $the_date = "$start_date $start_time - $end_time";
            }
        } else {

            if ( $event->general->all_day == '1' ) {
                $the_date = "$start_date - $end_date";
            } else {
                $the_date = "$start_date $start_time - $end_date $end_time";
            }
        }

        if ( $show_utc_offset == '1' ) {
            $the_date .= ' ' . '<span class="stec-layout-single-timezone">' . $event->general->timezone . '</span>';
        }

        return $the_date;
    }



    /**
     * Returns human readable format of the timespan
     */
    public static function get_the_schedule_timespan($date_string, $event) {

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        $dateformat = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'date_format');
        $timeformat = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'time_format');

        $start_date_unix = strtotime($date_string) + $event->general->repeat_time_offset;

        // Translate months
        $sm = '<span class="stec-layout-single-month-full">' . ucfirst($stachethemes_ec_main->lang(strtolower(date('M', $start_date_unix)))) . '</span>';


        switch ( $dateformat ) :

            case 'dd-mm-yy' :

                $start_date = '<span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ' .
                        $sm .
                        ' <span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span>';

                break;

            case 'mm-dd-yy' :
                $start_date = $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ' .
                        ' <span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span>';
                break;

            case 'yy-mm-dd' :
            default:
                $start_date = '<span class="stec-layout-single-year">' . date('Y', $start_date_unix) . '</span> ';
                $sm .
                        ' <span class="stec-layout-single-day">' . date('d', $start_date_unix) . '</span> ';
                break;
        endswitch;

        switch ( $timeformat ) :
            case '12':
                $timeformat = 'h:i a';
                break;

            case '24':
                $timeformat = 'H:i';
            default:

        endswitch;

        $start_time = date($timeformat, $start_date_unix);

        if ( $event->general->all_day == '1' ) {
            $the_date = $start_date;
        } else {
            $the_date = "$start_date <span class='stec-layout-single-time'>$start_time</span>";
        }

        return $the_date;
    }



    /**
     * PHP current site url
     * @return type url string
     */
    public static function get_page_url() {

        $pageURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
        $pageURL .= $_SERVER['SERVER_PORT'] != '80' ? $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"] : $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        return $pageURL;
    }



    /**
     * 
     * @param DateTime $date Date object
     * @param type $timezone event timezone string
     * @param type $format time format
     * @return type UTC time string
     */
    public static function to_utc_time($date, $timezone, $format = 'Ymd\THis') {

        $UTC    = new DateTimeZone("UTC");
        $caltTZ = new DateTimeZone($timezone);

        $date = new DateTime($date, $caltTZ);
        $date->setTimezone($UTC);

        return $date->format($format);
    }



    public static function get_ics_start_date($event) {

        // Get real date with repeat offset
        $start_date = date('Ymd\THis', strtotime($event->general->start_date) + $event->general->repeat_time_offset);

        if ( $event->general->all_day == '1' ) {
            // Since it's date specific don't UTC the date
            return date('Ymd', strtotime($start_date));
        } else {

            return self::to_utc_time($start_date, $event->general->timezone) . 'Z';
        }
    }



    public static function get_ics_end_date($event) {

        // Get real date with repeat offset
        $end_date = date('Ymd\THis', strtotime($event->general->end_date) + $event->general->repeat_time_offset);

        if ( $event->general->all_day == '1' ) {
            // Since it's date specific don't UTC the date
            return date('Ymd', strtotime($end_date) + 24 * 3600);
        } else {

            return self::to_utc_time($end_date, $event->general->timezone) . 'Z';
        }
    }



    public static function get_google_import_link($event) {

        return "https://calendar.google.com/calendar/render?action=TEMPLATE&amp;text={$event->general->summary}&amp;dates=" . self::get_ics_start_date($event) . "/" . self::get_ics_end_date($event) . "&amp;details={$event->general->description_short}&amp;location={$event->general->location}&amp;sf=true&amp;output=xml";
    }



    public static function get_guest_links($links) {

        $links = explode('||', $links);

        $the_links = array();

        foreach ( $links as $k => $link ) :

            $link = explode('::', $link);

            if ( !isset($link[1]) ) {
                continue;
                ;
            }

            $icon = $link[0];
            $url  = $link[1];

            $the_links[$k] = new stdClass();

            $the_links[$k]->icon = $icon;
            $the_links[$k]->url  = $url;

        endforeach;

        return $the_links;
    }



    public static function get_product_price($product) {

        $price = $product->html_price;

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        if ( $product->has_child ) {
            $price .= '<a href="' . $product->url . '">' . $stachethemes_ec_main->lang('(Select Options)') . '</a>';
        }
        
        return $price;
    }



    public static function user_is_invited($event) {

        if ( is_user_logged_in() === false ) {
            return false;
        }

        $userid = get_current_user_id();

        foreach ( $event->attendance as $attendee ) {

            if ( $attendee->userid == $userid ) {
                return true;
            }
        }

        return false;
    }



    public static function get_user_attendance_status($event, $userid = false) {

        if ( !$userid ) {
            $userid = get_current_user_id();
        }

        foreach ( $event->attendance as $attendee ) {

            if ( $attendee->userid == $userid && $event->general->repeat_time_offset == $attendee->repeat_offset ) {

                return $attendee->status;
            }
        }

        return 0;
    }



    /**
     * Returns unique attendees without offset repeats
     * @param type $event
     * @return array objects
     */
    public static function get_attendees($event) {

        $attendees = array();

        foreach ( $event->attendance as $attendee ) :

            if ( $attendee->repeat_offset == 0 ) {
                array_push($attendees, $attendee);
            }

        endforeach;

        return $attendees;
    }



    public static function add_rewrite_tags() {
        add_rewrite_tag('%stec_event_alias%', '([^&]+)');
        add_rewrite_tag('%stec_event_offset%', '([^&]+)');
    }



    public static function add_rewrite_rules() {

        $post_id = get_option('stec-single-page-id', false);

        if ( !$post_id ) {
            return false;
        }

        $post = get_post($post_id);

        if ( !$post ) {
            return false;
        }

        self::add_rewrite_tags();

        add_rewrite_rule('^' . $post->post_name . '/([^&]+)/([^&]+)?', 'index.php?page_id=' . $post_id . '&stec_event_alias=$matches[1]&stec_event_offset=$matches[2]', 'top');
        add_rewrite_rule('^' . $post->post_name . '/([^&]+)/?', 'index.php?page_id=' . $post_id . '&stec_event_alias=$matches[1]', 'top');
    }



    public static function update_rewrite_rules() {

        global $wp_rewrite;

        $wp_rewrite->flush_rules(false);

        $post_id = get_option('stec-single-page-id', false);

        if ( !$post_id ) {
            return false;
        }

        $post = get_post($post_id);

        if ( !$post ) {
            return false;
        }

        update_option('stec-single-page-url', get_the_permalink($post->ID));
    }



    public static function get_single_page_url() {

        $post_id = get_option('stec-single-page-id', false);

        if ( !$post_id ) {
            return false;
        }
        
        $post = get_post($post_id);

        return site_url() . '/' . $post->post_name . '/';
    }



    public static function show_404() {
        status_header(404);
        nocache_headers();

        if ( '' != get_query_template('404') ) {
            include( get_query_template('404') );
        }

        exit();
    }



    public static function create_page($title, $slug = false, $single_page_id = false) {

        return wp_insert_post(array(
                'ID'           => $single_page_id ? $single_page_id : 0,
                'post_type'    => 'page',
                'post_title'   => wp_strip_all_tags($title),
                'post_name'    => $slug ? wp_strip_all_tags($slug) : '',
                'post_content' => '[stachethemes_ec_single]',
                'post_status'  => 'publish',
                'post_author'  => get_current_user_id()
        ));
    }



    /**
     * Whether or not event has valid reminder
     * @param type $event
     * @return boolean
     */
    public static function reminder_expired($event) {

        $start = strtotime($event->general->start_date) + $event->general->repeat_time_offset;

        $event->general->all_day;

        $utc_start = self::to_utc_time(date('Y-m-d H:i:s', $start), $event->general->timezone, 'U');

        if ( gmdate('U') >= $utc_start ) {
            return true;
        }

        return false;
    }



    public static function event_has_tabs($event) {

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();
        $has_weather_api_key  = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'weather_api_key');

        if (
                !empty($event->schedule) ||
                !empty($event->guests) ||
                !empty($event->attendance) ||
                !empty($event->woocommerce) ||
                ($event->general->location_forecast != '' && $has_weather_api_key != '') ||
                $event->general->comments != '0'
        ) {
            return true;
        }

        return false;
    }

}
