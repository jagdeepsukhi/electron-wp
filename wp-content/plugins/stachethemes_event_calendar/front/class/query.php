<?php




/**
 * Front-end public queries
 * CAUTION RETURNED DATA VISIBLE FROM ALL
 */
class stachethemes_event_calendar_query {



    private static $cache_time = 12; // hours



    private static function _get_user_cookie() {

        $cookie_name = "stachethemes_ec_anon_user";

        return isset($_COOKIE[$cookie_name]) && strlen($_COOKIE[$cookie_name]) > 13 ? $_COOKIE[$cookie_name] : false;
    }



    private static function _get_admin_setting_value($page, $name = false) {
        $setting = get_option($page, array());

        if ( $name === false ) {
            return $setting;
        }

        return isset($setting[$name]["value"]) ? $setting[$name]["value"] : null;
    }



    private static function get_event_vis_filter($userid) {

        $user_roles = self::_get_user_roles($userid);

        $vis_filter = " WHERE ((event.visibility IN ($user_roles) OR event.created_by = {$userid} OR attendance.userid = {$userid}) ";
        $vis_filter .= " OR (event.visibility = 'stec_cal_default' AND calendar.visibility IN ($user_roles)) ";

        if ( is_super_admin($userid) ) {
            $vis_filter .= ' OR 1 ) ';
        } else {
            $vis_filter .= ' ) ';
        }

        return $vis_filter;
    }



    /**
     * 
     * @param int $userid
     * @return string '...','...','...'
     */
    private static function _get_user_roles($userid) {

        $user_info    = get_userdata((int) $userid);
        $user_roles   = isset($user_info->roles) ? $user_info->roles : array();
        $user_roles[] = 'stec_public';

        if ( is_user_logged_in() ) {
            $user_roles[] = 'stec_logged_in';
        }

        $user_roles = "'" . implode("','", $user_roles) . "'";

        return $user_roles;
    }



    private static function _wp_get_attachment($attachment_id) {

        $attachment = get_post($attachment_id);
        $src        = wp_get_attachment_image_src($attachment_id, 'full');
        $thumb      = wp_get_attachment_image_src($attachment_id, 'medium');

        return array(
                'alt'         => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
                'caption'     => $attachment->post_excerpt,
                'description' => $attachment->post_content,
                'src'         => $src[0],
                'thumb'       => $thumb[0],
                'title'       => $attachment->post_title
        );
    }



    public static function get_event_tabs($event_id) {

        global $wpdb;

        $tabs = array();

        $general     = $wpdb->get_row("SELECT event.comments, event.location, event.location_forecast FROM {$wpdb->prefix}stec_events as event WHERE `id` = '{$event_id}'", OBJECT);
        $schedule    = $wpdb->get_row("SELECT schedule.id FROM {$wpdb->prefix}stec_schedule as schedule WHERE `eventid` = '{$event_id}'", OBJECT);
        $guests      = $wpdb->get_row("SELECT guests.id FROM {$wpdb->prefix}stec_guests as guests WHERE `eventid` = '{$event_id}'", OBJECT);
        $attendance  = $wpdb->get_row("SELECT attendance.id FROM {$wpdb->prefix}stec_attendance as attendance WHERE `eventid` = '{$event_id}'", OBJECT);
        $attachments = $wpdb->get_row("SELECT attachments.id FROM {$wpdb->prefix}stec_attachments as attachments WHERE `eventid` = '{$event_id}'", OBJECT);
        $woocommerce = $wpdb->get_row("SELECT woocommerce.id FROM {$wpdb->prefix}stec_woocommerce as woocommerce WHERE `eventid` = '{$event_id}'", OBJECT);

        $tabs['comments'] = $general->comments;

        if ( $general->location != '' ) {
            $tabs['location'] = 1;
        }


        if ( $general->location_forecast != '' && self::_get_admin_setting_value('stec_menu__general', 'weather_api_key') != '' ) {
            $tabs['forecast'] = 1;
        }

        if ( $schedule ) {
            $tabs['schedule'] = 1;
        }

        if ( $guests ) {
            $tabs['guests'] = 1;
        }

        if ( $attendance ) {
            $tabs['attendance'] = 1;
        }

        if ( $woocommerce ) {
            $tabs['woocommerce'] = 1;
        }

        return $tabs;
    }



    /**
     * Retrieves calendar events
     * @global type $wpdb
     * @param type $cal expects string with calendar ids example: 1,2,3,4
     * @return object 
     */
    public static function get_events($cal = false, $event_id = false, $approved_only = false, $min_date = null, $max_date = null) {

        global $wpdb;

        $userid = is_user_logged_in() ? get_current_user_id() : false;

        if ( !$userid || $userid === 0 ) {
            $userid = -1; // set impossible userid
        }

        $cal_filter      = '';
        $event_filter    = '';
        $approved_filter = '';
        $min_max_filter  = '';

        if ( $cal !== false ) {

            // filter cal string, allow only numbers
            $cal = explode(',', $cal);
            $cal = array_filter($cal, 'is_numeric');
            $cal = implode(',', array_unique($cal));

            $cal_filter = "AND calendar.id IN ( $cal ) ";
        }

        if ( $event_id !== false ) {

            $event_filter = "AND event.id = {$event_id} ";
        }

        if ( $approved_only !== false ) {

            $meta_filter = "";

            if ( self::_get_user_cookie() !== false ) {
                $user_cookie = $wpdb->_real_escape(self::_get_user_cookie());
                $meta_filter = " OR (meta.created_by_cookie = '{$user_cookie}') ";
            }

            $approved_filter = "AND (event.approved = 1 OR event.created_by = {$userid} $meta_filter) ";
        }

        $vis_filter = self::get_event_vis_filter($userid);

        if ( $min_date || $max_date ) {
            $min_max_filter = self::get_event_min_max_filter($min_date, $max_date);
        }

        $query = "SELECT "
                . "event.id, "
                . "event.alias, "
                . "event.calid, "
                . "calendar.title as cal_title, "
                . "calendar.color as cal_color, "
                . "event.summary, "
                . "event.featured, "
                . "event.description_short, "
                . "event.images, "
                . "event.icon, "
                . "event.location, "
                . "event.start_date, "
                . "event.end_date, "
                . "event.all_day, "
                . "event.keywords, "
                . "event.counter, "
                . "event.color, "
                . "event.approved, "
                . "calendar.timezone, "
                . "repeater.rrule, "
                . "repeater.exdate, "
                . "meta.uid, "
                . "meta.recurrence_id "
                . "FROM {$wpdb->prefix}stec_events as event "
                . "LEFT JOIN {$wpdb->prefix}stec_attendance as attendance ON "
                . "event.id = attendance.eventid "
                . "LEFT JOIN {$wpdb->prefix}stec_calendars as calendar ON "
                . "event.calid = calendar.id "
                . "LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON "
                . "event.id = repeater.eventid "
                . "LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON "
                . "event.id = meta.eventid "
                . $vis_filter
                . $cal_filter
                . $event_filter
                . $approved_filter
                . $min_max_filter
                . " GROUP BY event.id ORDER BY event.featured DESC ";

        $cache_name          = md5($query);
        $absolute_cache_name = __DIR__ . '/../cache/events/' . $cache_name;

        $use_cache = stachethemes_ec_admin::get_admin_setting_value('stec_menu__cache', 'cache_events');

        // Check if file exists, not old and cache enabled
        if ( false != $use_cache &&
                file_exists($absolute_cache_name) &&
                is_file($absolute_cache_name) &&
                time() - filemtime($absolute_cache_name) < self::$cache_time * 60 * 60 ) {

            return json_decode(file_get_contents($absolute_cache_name));
        }

        // Request query
        $query = $wpdb->get_results($query, OBJECT);

        if ( $query === false ) {
            return array();
        }

        foreach ( $query as $k => $event ) {

            $query[$k]->tabs = self::get_event_tabs($event->id);

            // set calendar timezone offset from utc
            $dateTimeZoneUTC = new DateTimeZone("UTC");
            $dateTimeZoneCAL = new DateTimeZone($event->timezone);
            $dateTimeUTC     = new DateTime("now", $dateTimeZoneUTC);
            $dateTimeCAL     = new DateTime("now", $dateTimeZoneCAL);

            $query[$k]->timezone_utc_offset = $dateTimeZoneCAL->getOffset($dateTimeUTC);

            $event->images_meta = array();

            // add only one image
            if ( $event->images != '' ) {
                $event->images        = explode(',', $event->images);
                $event->images_meta[] = self::_wp_get_attachment(end($event->images));
            }
        }

        if ( $use_cache ) {
            file_put_contents($absolute_cache_name, json_encode($query));
        }

        return $query;
    }



    public static function get_event_data($event_id, $approved_only = false) {

        global $wpdb;

        if ( is_nan($event_id) ) {
            return array();
        }

        $userid = get_current_user_id();

        if ( !$userid || $userid === 0 ) {
            $userid = -1; // set impossible userid
        }

        $vis_filter = self::get_event_vis_filter($userid);

        $approved_filter = '';

        if ( $approved_only !== false ) {

            $approved_filter = $approved_filter = "AND (event.approved = 1 OR event.created_by = {$userid}) ";
        }

        $query = "SELECT "
                . "event.*,"
                . "calendar.timezone "
                . "FROM {$wpdb->prefix}stec_events as event "
                . "LEFT JOIN {$wpdb->prefix}stec_attendance as attendance ON "
                . "event.id = attendance.eventid "
                . "LEFT JOIN {$wpdb->prefix}stec_calendars as calendar ON "
                . "event.calid = calendar.id "
                . $vis_filter
                . $approved_filter
                . " AND event.id = '{$event_id}' "
                . " GROUP BY event.id ";


        $cache_name          = md5($query);
        $absolute_cache_name = __DIR__ . '/../cache/events/' . $cache_name;

        $use_cache = stachethemes_ec_admin::get_admin_setting_value('stec_menu__cache', 'cache_events');

        // Check if file exists, not old and cache enabled
        if ( false != $use_cache &&
                file_exists($absolute_cache_name) &&
                is_file($absolute_cache_name) &&
                time() - filemtime($absolute_cache_name) < self::$cache_time * 60 * 60 ) {

            return json_decode(file_get_contents($absolute_cache_name));
        }

        $general = $wpdb->get_row($query, OBJECT);

        if ( !$general ) {
            return array();
        }

        // hide visibility value
        $general->visibility = '';

        // set calendar timezone offset from utc
        $dateTimeZoneUTC = new DateTimeZone("UTC");
        $dateTimeZoneCAL = new DateTimeZone($general->timezone);
        $dateTimeUTC     = new DateTime("now", $dateTimeZoneUTC);
        $dateTimeCAL     = new DateTime("now", $dateTimeZoneCAL);

        $general->timezone_utc_offset = $dateTimeZoneCAL->getOffset($dateTimeUTC);

        $schedule    = $wpdb->get_results("SELECT schedule.* FROM {$wpdb->prefix}stec_schedule as schedule WHERE `eventid` = '{$event_id}' ORDER BY `start_date`", OBJECT);
        $guests      = $wpdb->get_results("SELECT guests.* FROM {$wpdb->prefix}stec_guests as guests WHERE `eventid` = '{$event_id}'", OBJECT);
        $attendance  = $wpdb->get_results("SELECT attendance.* FROM {$wpdb->prefix}stec_attendance as attendance WHERE `eventid` = '{$event_id}'", OBJECT);
        $attachments = $wpdb->get_results("SELECT attachments.* FROM {$wpdb->prefix}stec_attachments as attachments WHERE `eventid` = '{$event_id}'", OBJECT);
        $woocommerce = $wpdb->get_results("SELECT woocommerce.* FROM {$wpdb->prefix}stec_woocommerce as woocommerce WHERE `eventid` = '{$event_id}'", OBJECT);

        if ( $general->images != '' ) {
            $general->images_meta = array();
            $images               = explode(',', $general->images);
            foreach ( $images as $k => $image_id ) {
                $general->images_meta[$k] = self::_wp_get_attachment($image_id);
            }
        }

        if ( $guests ) {
            foreach ( $guests as $guest ) {
                if ( $guest->photo != '' ) {
                    $photo             = wp_get_attachment_image_src($guest->photo, 'full');
                    $guest->photo_full = $photo[0];
                }
            }
        }

        if ( $attachments ) {
            foreach ( $attachments as $attachment ) {
                $id                      = $attachment->attachment;
                $attachment->filename    = basename(get_attached_file($id, true));
                $attachment->size        = size_format(filesize(get_attached_file($id)));
                $attachment->link        = wp_get_attachment_url($id);
                $attachment->description = get_post_field('post_content', $id);
            }
        }

        if ( $attendance ) {

            foreach ( $attendance as $attendee ) {

                if ( is_numeric($attendee->userid) ) {
                    $userdata         = get_userdata($attendee->userid);
                    $attendee->name   = $userdata->display_name;
                    $avatar           = get_avatar_url($attendee->userid);
                    $attendee->avatar = $avatar;
                }

                if ( filter_var($attendee->email, FILTER_VALIDATE_EMAIL) !== false ) {
                    $mailname         = explode('@', $attendee->email);
                    $attendee->name   = $mailname[0];
                    $avatar           = get_avatar_url($attendee->email);
                    $attendee->avatar = $avatar;

                    $attendee->email = '';
                }
            }
        }

        if ( $woocommerce && class_exists('WooCommerce') ) {
            foreach ( $woocommerce as $k => $product ) {
                $wc_product           = WC()->product_factory->get_product($product->product_id);
                $wc_product_post_date = get_post($wc_product->get_id());

                $product->id             = $wc_product->get_id();
                $product->sku            = $wc_product->get_sku();
                $product->url            = $wc_product->get_permalink();
                $product->purchesable    = $wc_product->is_purchasable();
                $product->is_in_stock    = $wc_product->is_in_stock();
                $product->stock_quantity = $wc_product->get_stock_quantity();
                $product->is_featured    = $wc_product->is_featured();
                $product->is_on_sale     = $wc_product->is_on_sale();
                $product->has_child      = $wc_product->has_child();
                $product->image          = $wc_product->get_image();
                $product->html_price     = $wc_product->get_price_html();
                $product->title          = $wc_product->get_title();
                $product->post_data      = array(
                        'excerpt' => $wc_product_post_date->post_excerpt
                );
            }
        } else {
            // return empty woocommerce
            $woocommerce = array();
        }

        // shortcode support for introduction and location details fields
        if ( $general->description != '' ) {
            $general->description = wpautop(do_shortcode($general->description));
        }

        if ( $general->location_details != '' ) {
            $general->location_details = wpautop(do_shortcode($general->location_details));
        }

        $data = (object) array(
                        'general'     => $general,
                        'schedule'    => $schedule,
                        'guests'      => $guests,
                        'attendance'  => $attendance,
                        'attachments' => $attachments,
                        'woocommerce' => $woocommerce
        );

        if ( $use_cache ) {
            file_put_contents($absolute_cache_name, json_encode($data));
        }

        return $data;
    }



    public static function set_user_event_attendance($event_id, $repeat_offset, $user_id, $status) {

        global $wpdb;

        $exists = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}stec_attendance WHERE `eventid` = '{$event_id}' AND `userid` = '{$user_id}' AND `repeat_offset` = '{$repeat_offset}' ");

        if ( $exists === false || $exists === null ) {

            $query = $wpdb->insert(
                    $wpdb->prefix . 'stec_attendance', array(
                    'status'        => $status,
                    'eventid'       => $event_id,
                    'userid'        => $user_id,
                    'repeat_offset' => $repeat_offset
                    ), array(
                    '%d',
                    '%d',
                    '%d',
                    '%d'
                    )
            );

            if ( $query === false ) {
                return array('error' => 1);
            }
        } else {

            $query = $wpdb->update(
                    $wpdb->prefix . 'stec_attendance', array(
                    'status' => $status
                    ), array(
                    'eventid'       => $event_id,
                    'userid'        => $user_id,
                    'repeat_offset' => $repeat_offset
                    ), array(
                    '%d'
                    ), array(
                    '%d',
                    '%d',
                    '%d'
                    )
            );

            if ( $query === false ) {
                return array('error' => 1);
            }
        }

        $query = $wpdb->get_row("SELECT id,status FROM {$wpdb->prefix}stec_attendance WHERE `eventid` = '{$event_id}' AND `userid` = '{$user_id}' AND `repeat_offset` = '{$repeat_offset}' ", OBJECT);


        return $query === false ? array('error' => 1) : array(
                'status' => $query->status,
                'id'     => $query->id,
        );
    }



    /**
     * @return json
     */
    public static function get_weather_data($location) {

        $apikey = self::_get_admin_setting_value('stec_menu__general', 'weather_api_key');
        $url    = "https://api.darksky.net/forecast/{$apikey}/" . urlencode($location);

        $cache_name = str_replace(array(",", ".", "/", "\\"), "", $location);
        $cache_name = strtolower($cache_name);
        $cache_name = plugin_dir_path(__FILE__) . "../cache/forecast/$cache_name";
        $data       = '';

        $use_cache = stachethemes_ec_admin::get_admin_setting_value('stec_menu__cache', 'cache_forecast');

        if ( file_exists($cache_name) && is_file($cache_name) && time() - filemtime($cache_name) < 60 * 60 && false != $use_cache ) {

            $data = file_get_contents($cache_name);
        } else {

            $response = wp_remote_get($url);

            if ( is_array($response) ) {
                $data = ($response['body']);

                if ( $use_cache ) {
                    file_put_contents($cache_name, $data);
                }
            }
        }

        return $data;
    }



    public static function set_reminder($event_id, $repeat_offset, $email, $date) {

        if ( is_nan($event_id) ) {
            return array('error' => 1);
        }

        if ( is_nan($repeat_offset) ) {
            return array('error' => 1);
        }

        if ( !$email || filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {
            return array('error' => 1);
        }

        if ( !$date ) {
            return array('error' => 1);
        }

        global $wpdb;

        $exists = $wpdb->get_row($wpdb->prepare(
                        "SELECT `id` FROM {$wpdb->prefix}stec_reminder WHERE "
                        . " `eventid`       = %d AND "
                        . " `repeat_offset` = %d AND "
                        . " `email`         = %s AND "
                        . " `date`          = %s ", $event_id, $repeat_offset, $email, $date
        ));

        // exists
        if ( $exists !== false && !empty($exists) ) {
            return array('error' => 0);
        }

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_reminder', array(
                'eventid'       => $event_id,
                'repeat_offset' => $repeat_offset,
                'email'         => $email,
                'date'          => $date,
                ), array(
                '%d',
                '%d',
                '%s',
                '%s'
                )
        );

        if ( $query === false ) {
            return array('error' => 1);
        } else {
            return array('error' => 0);
        }
    }



    public static function get_calendar($id) {

        global $wpdb;

        $id = (int) $id;

        $result = $wpdb->get_row("SELECT * from {$wpdb->prefix}stec_calendars WHERE id = {$id}", OBJECT);

        return $result !== false ? $result : array();
    }



    public static function get_calendars() {

        global $wpdb;

        /**
         * @todo implement filter `visibible to user`
         */
        $filter = "";

        $result = $wpdb->get_results("SELECT CAL.id,CAL.title,CAL.color,CAL.timezone from {$wpdb->prefix}stec_calendars as CAL " . $filter, OBJECT);

        return $result !== false ? $result : array();
    }



    public static function get_unapproved_events_count() {

        global $wpdb;

        $wpdb->get_results("SELECT id FROM {$wpdb->prefix}stec_events WHERE approved = 0");

        return $wpdb->num_rows;
    }



    public static function get_current_user() {

        $the_user = false;

        if ( is_user_logged_in() ) {
            $the_user = wp_get_current_user();
        }

        return $the_user;
    }



    public static function get_current_user_email() {

        $the_user = self::get_current_user();

        return $the_user && isset($the_user->user_email) ? $the_user->user_email : false;
    }



    public static function get_event_data_by_alias($alias) {

        global $wpdb;

        if ( $alias === false ) {

            $event_id = ($wpdb->get_var("SELECT id FROM {$wpdb->prefix}stec_events ORDER BY `id` DESC LIMIT 1"));
        } else {

            $event_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$wpdb->prefix}stec_events AS events WHERE events.alias = '%s' ", $alias));
        }

        if ( !$event_id ) {
            return array();
        }

        return self::get_event_data($event_id);
    }



    public static function get_event_min_max_filter($min_date = null, $max_date = null) {

        $min_max_filter = 'AND ( ';

        if ( $min_date ) {
            $min_date_string = date('Y-m-d', $min_date);
            $min_max_filter  .= " ( (event.start_date >= '$min_date_string' OR event.end_date >= '$min_date_string')  OR repeater.rrule <> '' ) ";
        }

        if ( $max_date ) {
            $max_date_string = date('Y-m-d', $max_date);

            if ( $min_date ) {
                $min_max_filter .= " AND ";
            }

            $min_max_filter .= " ( event.start_date <= '$max_date_string' ) ";
        }

        $min_max_filter .= " ) ";

        return $min_max_filter;
    }

}
