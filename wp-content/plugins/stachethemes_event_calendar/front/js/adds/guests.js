(function ($) {

    "use strict";
    
    $.stecExtend(function (master) {

        // add preloader
        var $tab = master.$instance.find('.stec-layout-event.active').find('.stec-layout-event-inner-guests');
        $tab.wrapInner('<div class="stec-layout-event-inner-preload-wrap" />');
        $(master.glob.template.preloader).appendTo($tab);

    }, 'onBeforeEventDataAjax');

    $.stecExtend(function (master, data) {
        
        if (data.guests.length <= 0) {
            return;
        }
        
        var $instance = master.$instance;
        
        var $event = $instance.$events.find('.stec-layout-event.active');
        var $inner = $event.find('.stec-layout-event-inner-guests');
        
        if ($inner.length <= 0) {
            return;
        }

        var template = $inner.find('.stec-layout-event-inner-guests-guest-template')[0].outerHTML;
        $inner.find('.stec-layout-event-inner-guests-guest-template').remove();
        
        if (data.guests.length > 0) {
            
            $(data.guests).each(function(i){
                
                var th = this;
                
                $(template).html(function(index, html){
                    
                    var links = th.links.split('||');
                    
                    var lis = [];
                    
                    $(links).each(function(pos){
                        var link = this.split('::');
                        lis[pos] = '<li class="stec-layout-event-inner-guests-guest-left-avatar-icon-position-'+pos+'"><a href="'+link[1]+'"><i class="'+link[0]+'"></i></a></li>';
                    });
                    
                    var avatar;
                    
                    if (!th.photo_full) {
                        avatar = '<div class="stec-layout-event-inner-guests-guest-left-avatar-default"></div>';
                    } else {
                        avatar = '<img src="'+th.photo_full+'" alt="stec_replace_name" >';
                    }
                    
                    return html
                            .replace(/stec_replace_ico_position/g, i)
                            .replace(/stec_replace_avatar/g, avatar)
                            .replace(/stec_replace_social/g, lis.join(''))
                            .replace(/stec_replace_name/g, th.name)
                            .replace(/stec_replace_about/g, th.about);
                    
                })
                .removeClass('stec-layout-event-inner-guests-guest-template')
                .appendTo($inner);
                
            });
            
        } 
        
        // Remove tab preloaders
        $inner.find('.stec-layout-event-inner-preload-wrap').children().first().unwrap();
        $inner.find('.stec-layout-event-inner-preload-wrap').remove();
        $inner.find('.stec-preloader').remove();

    }, 'onEventDataReady');


})(jQuery);