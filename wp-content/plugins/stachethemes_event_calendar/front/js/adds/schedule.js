(function ($) {

    "use strict";
    
    $.stecExtend(function (master) {
        
        // add preloader
        var $tab = master.$instance.find('.stec-layout-event.active').find('.stec-layout-event-inner-schedule');
        $tab.wrapInner('<div class="stec-layout-event-inner-preload-wrap" />');
        $(master.glob.template.preloader).appendTo($tab);
        
    }, 'onBeforeEventDataAjax');
    
    $.stecExtend(function (master, data) {
        
        if (data.schedule.length <= 0) {
            return;
        }
        
        var $instance = master.$instance;
        var helper    = master.helper;
        
        var $event = $instance.$events.find('.stec-layout-event.active');
        var $inner = $event.find('.stec-layout-event-inner-schedule');
        
        if ($inner.length <= 0) {
            return;
        }

        var template = $inner.find('.stec-layout-event-inner-schedule-tab-template')[0].outerHTML;
        $inner.find('.stec-layout-event-inner-schedule-tab-template').remove();
        
        var timezoneOffset = data.general.timezone_utc_offset;
        var isAllDay       = data.general.all_day;
        
        if (data.schedule.length > 0) {
            
            $(data.schedule).each(function(){
                
                var th = this;
                
                var customKlass = '';
                
                if (th.description == '') {
                    customKlass += ' stec-layout-event-inner-schedule-tab-no-desc ';
                }
                
                if (th.icon == "" || th.icon == "fa") {
                    customKlass += ' stec-layout-event-inner-schedule-tab-no-icon ';
                }
                
                $(template)
                        .addClass(customKlass)
                        .html(function(index, html) {

                    if ( '1' != isAllDay && '1' == master.glob.options.general_settings.date_in_user_local_time ) {

                        var start    = helper.dbDateOffset(th.start_date, data.repeat_time_offset);
                        var utcStart = helper.dbDateOffset(start, -1 * timezoneOffset);
                        var offset   = -1 * new Date().getTimezoneOffset() * 60;
                        start        = helper.dbDateTimeToDate(helper.dbDateOffset(utcStart, offset));

                    } else {
                        
                        var start = helper.dbDateTimeToDate(helper.dbDateOffset(th.start_date, data.repeat_time_offset));
                    }
                        
                    var format = 'd m y';
                    switch (master.glob.options.general_settings.date_format) {
                        case 'dd-mm-yy' :
                            format = 'd M';
                            break;
                        case 'yy-mm-dd' :
                            format = 'M d';
                            break;
                        case 'mm-dd-yy' :
                            format = 'M d';
                            break;
                    }
                    
                    return html
                            .replace(/stec_replace_date/g, helper.dateToFormat(format,start))
                            .replace(/stec_replace_time/g, helper.dateToFormat('h:i',start))
                            .replace(/stec_replace_icon/g, th.icon)
                            .replace(/stec_replace_color/g, 'style="color:'+th.icon_color+'"') 
                            .replace(/stec_replace_title/g, th.title)
                            .replace(/stec_replace_desc/g, th.description);
                    
                })
                .removeClass('stec-layout-event-inner-schedule-tab-template')
                .appendTo($inner);
                
            });
            
            $inner.find('.stec-layout-event-inner-schedule-isempty').remove();
            $inner.find('.stec-layout-event-inner-schedule-tab').first().addClass('open');
            
        } 
        
        $inner.find('.stec-layout-event-inner-schedule-tab-preview')
                .off(helper.clickHandle('open'))
                .on(helper.clickHandle('open'), function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(this).parents(".stec-layout-event-inner-schedule-tab").not('.stec-layout-event-inner-schedule-tab-no-desc').toggleClass("open");
        });
        
        // Remove tab preloaders
        $inner.find('.stec-layout-event-inner-preload-wrap').children().first().unwrap();
        $inner.find('.stec-layout-event-inner-preload-wrap').remove();
        $inner.find('.stec-preloader').remove();

    }, 'onEventDataReady');


})(jQuery);