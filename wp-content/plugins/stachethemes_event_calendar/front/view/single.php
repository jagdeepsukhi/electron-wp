<div class="stec-layout-single" itemscope itemtype="http://schema.org/Event">

    <div class="stec-layout-single-preloader-template">
        <div class="stec-layout-single-preloader"></div>
    </div>

    <div class="stec-layout-single-preview">

        <div class="stec-layout-single-preview-left">

            <div style="background:<?php echo $event->general->color; ?>" class="stec-layout-single-preview-left-icon <?php
            if ( $event->general->icon == 'fa' ) :
                echo 'stec-layout-single-no-icon';
            endif;
            ?>">
                <i class="<?php echo $event->general->icon; ?>"></i>
            </div>

            <div class="stec-layout-single-preview-left-text">

                <?php if ( $event->general->featured >= '1' ) : ?>
                    <p class="stec-layout-single-preview-left-text-featured">
                        <i class="fa fa-star"></i>
                        <span><?php $stachethemes_ec_main->lang('Featured', true); ?></span>
                    </p>
                <?php endif; ?>

                <h1 class="stec-layout-single-preview-left-text-title" itemprop="name"><?php echo $event->general->summary; ?> <?php if ( is_super_admin() ) : ?>
                        <a class="stec-layout-single-edit-link" href="<?php echo get_admin_url(null, "admin.php?page=stec_menu__events&view=edit&calendar_id={$event->general->calid}&event_id={$event->general->id}"); ?>"><?php $stachethemes_ec_main->lang('(Edit event)', true) ?></a>
                    <?php endif; ?></h1>

                <p class="stec-layout-single-preview-left-text-date"><?php echo stachethemes_ec_single::get_the_timespan($event); ?></p>

                <?php stachethemes_ec_single::meta_schema_datetime_iso8601($event); ?>

                <?php if ( !stachethemes_ec_single::reminder_expired($event) ) : ?>
                    <a class="stec-layout-single-preview-left-reminder-toggle" href="javascript:void(0);">
                        <?php $stachethemes_ec_main->lang('Reminder', true); ?>
                    </a>
                <?php endif; ?>

            </div>

        </div>

        <?php if ( !stachethemes_ec_single::reminder_expired($event) ) : ?>
            <div class="stec-layout-single-preview-right">

                <div class="stec-layout-single-preview-right-reminder stec-layout-single-button-style stec-layout-single-btn-fontandcolor">
                    <div>
                        <i class="fa fa-bell"></i>
                        <span><?php $stachethemes_ec_main->lang('Reminder', true); ?></span>
                    </div>

                    <div>
                        <i class="fa fa-times"></i>
                        <span><?php $stachethemes_ec_main->lang('Close', true); ?></span>
                    </div>

                </div>

            </div>
        <?php endif; ?>

    </div>

    <div class="stec-layout-single-reminder-form">
        <ul>
            <li>
                <input class="stec-layout-event-input-fontandcolor" type="email" name="email" value="<?php echo stachethemes_event_calendar_query::get_current_user_email(); ?>" placeholder="<?php $stachethemes_ec_main->lang('E-Mail Address', true); ?>" />
            </li>
            <li>
                <input class="stec-layout-event-input-fontandcolor" type="text" name="number" value="3" placeholder="<?php $stachethemes_ec_main->lang('Time before', true); ?>" />
            </li>
            <li class="stec-layout-single-preview-reminder-units-selector">
                <p class="stec-layout-event-input-fontandcolor" data-value='hours'><?php $stachethemes_ec_main->lang('hours', true); ?></p>
                <ul>
                    <li data-value="hours"><?php $stachethemes_ec_main->lang('hours', true); ?></li>
                    <li data-value="days"><?php $stachethemes_ec_main->lang('days', true); ?></li>
                    <li data-value="weeks"><?php $stachethemes_ec_main->lang('weeks', true); ?></li>
                </ul>
            </li>
            <li>
                <button class="stec-layout-single-preview-remind-button stec-layout-single-button-style stec-layout-single-btn-fontandcolor"><?php $stachethemes_ec_main->lang('Remind me', true); ?></button>
            </li>
        </ul>

        <div class="stec-layout-single-reminder-status">
            <p class="stec-layout-event-title2-fontandcolor">-</p>
        </div>
    </div>

    <?php if ( isset($event->general->images_meta) ) : ?>

        <div class="stec-layout-single-media">

            <div class="stec-layout-single-media-content">

                <?php
                foreach ( $event->general->images_meta as $image ) :

                    $image = (object) $image;
                    ?>
                    <div style="background-image:url(<?php echo $image->src; ?>);">
                        <img alt="<?php echo $image->title; ?>" src="<?php echo $image->src; ?>">
                        <meta itemprop="image" content="<?php echo $image->src; ?>" />
                    </div>
                    <?php
                endforeach;
                ?>

            </div>

            <div class="stec-layout-single-media-content-subs">
                <div>
                    <p></p>
                    <span></span>
                </div>
            </div>

            <div class="stec-layout-single-media-controls">
                <div class="stec-layout-single-media-controls-prev stec-layout-single-btn-fontandcolor"><i class="fa fa-angle-left"></i></div>
                <div class="stec-layout-single-media-controls-list-wrap">
                    <ul class="stec-layout-single-media-controls-list">

                        <?php
                        if ( isset($event->general->images_meta) ) :
                            foreach ( $event->general->images_meta as $image ) :
                                $image = (object) $image;
                                ?>
                                <li style="background-image: url(<?php echo $image->thumb; ?>);"></li>
                                <?php
                            endforeach;
                        endif;
                        ?>

                    </ul>
                </div>
                <div class="stec-layout-single-media-controls-next stec-layout-single-btn-fontandcolor"><i class="fa fa-angle-right"></i></div>
            </div>

        </div>

    <?php endif; ?>

    <?php if ( $event->general->description != '' ) : ?>

        <div class="stec-layout-single-description stec-layout-event-text-fontandcolor" itemprop="description">
            <?php echo $event->general->description; ?>
        </div>

    <?php endif; ?>

    <?php if ( $event->general->link != '' ) : ?>
        <a href="<?php echo $event->general->link; ?>" class="stec-layout-single-external-link stec-layout-single-button-style stec-layout-event-btn-fontandcolor" target="_BLANK"><?php $stachethemes_ec_main->lang('Visit Website', true); ?></a>
    <?php endif; ?>

    <?php if ( $event->general->counter == '1' ) : ?>

        <ul class="stec-layout-single-counter">
            <li>
                <p class="stec-layout-single-counter-num">0</p>
                <p class="stec-layout-single-counter-label" data-plural-label="<?php $stachethemes_ec_main->lang('Days', true); ?>" data-singular-label="<?php $stachethemes_ec_main->lang('Day', true); ?>">days</p>
            </li>
            <li>
                <p class="stec-layout-single-counter-num">0</p>
                <p class="stec-layout-single-counter-label" data-plural-label="<?php $stachethemes_ec_main->lang('Hours', true); ?>" data-singular-label="<?php $stachethemes_ec_main->lang('Hour', true); ?>">hours</p>
            </li>
            <li>
                <p class="stec-layout-single-counter-num">0</p>
                <p class="stec-layout-single-counter-label" data-plural-label="<?php $stachethemes_ec_main->lang('Minutes', true); ?>" data-singular-label="<?php $stachethemes_ec_main->lang('Minute', true); ?>">minutes</p>
            </li>
            <li>
                <p class="stec-layout-single-counter-num">0</p>
                <p class="stec-layout-single-counter-label" data-plural-label="<?php $stachethemes_ec_main->lang('Seconds', true); ?>" data-singular-label="<?php $stachethemes_ec_main->lang('Second', true); ?>">seconds</p>
            </li>
        </ul>

    <?php endif; ?>

    <p class="stec-layout-single-event-status-text event-expired stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Event expired', true); ?></p>
    <p class="stec-layout-single-event-status-text event-inprogress stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Event is in progress', true); ?></p>

    <div class="stec-single-sections">

        <?php if ( stachethemes_ec_single::user_is_invited($event) ) : ?>

            <ul class="stec-layout-single-intro-attendance">

                <li class="stec-layout-single-button-style stec-layout-single-intro-attendance-attend stec-layout-event-btn-fontandcolor <?php
                if ( stachethemes_ec_single::get_user_attendance_status($event) == 1 ) {
                    echo 'active';
                }
                ?>"><p><?php $stachethemes_ec_main->lang('Attend', true); ?></p></li>

                <li class="stec-layout-single-button-style stec-layout-single-intro-attendance-decline stec-layout-event-btn-fontandcolor <?php
                if ( stachethemes_ec_single::get_user_attendance_status($event) == 2 ) {
                    echo 'active';
                }
                ?>"><p><?php $stachethemes_ec_main->lang('Decline', true); ?></p></li>
            </ul>

        <?php endif; ?>

        <?php if ( !empty($event->attachments) ) : ?>


            <div class="stec-layout-single-attachments">

                <div class="stec-layout-single-attachments-top">

                    <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Attachments', true); ?></p>

                    <div class="stec-layout-single-attachments-toggle">
                        <i class="fa fa-plus"></i>
                        <i class="fa fa-minus"></i>
                    </div>

                </div>

                <ul class="stec-layout-single-attachments-list">

                    <?php foreach ( $event->attachments as $file ) : ?>

                        <li class="stec-layout-single-attachment">
                            <div>
                                <p class="stec-layout-single-attachment-title stec-layout-event-title2-fontandcolor"><a href="<?php echo $file->link ?>"><?php echo $file->filename; ?></a></p>
                                <p class="stec-layout-single-attachment-desc stec-layout-event-text-fontandcolor"><?php echo $file->description; ?></p>
                            </div>

                            <div>
                                <a href="<?php echo $file->link ?>" class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Download', true); ?></a>
                                <p class="stec-layout-single-attachment-size stec-layout-event-text-fontandcolor"><?php echo $file->size; ?></p>
                            </div>
                        </li>

                    <?php endforeach; ?>
                </ul>
            </div>


        <?php endif; ?>

        <meta itemprop="url" content="<?php echo stachethemes_ec_single::get_page_url(); ?>"/>

        <?php
        if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'social_links') == '1' ||
                $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'show_export_buttons') == '1' ) :
            ?>  

            <div class="stec-layout-single-share-and-export">

                <div class="stec-layout-single-share">
                    <?php if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'social_links') == '1' ) : ?>

                        <a target="_BLANK" href="http://www.facebook.com/share.php?u=<?php echo stachethemes_ec_single::get_page_url(); ?>"><i class="fa fa-facebook"></i></a>
                        <a target="_BLANK" href="http://twitter.com/home?status=<?php echo stachethemes_ec_single::get_page_url(); ?>"><i class="fa fa-twitter"></i></a>
                        <a target="_BLANK" href="https://plus.google.com/share?url=<?php echo stachethemes_ec_single::get_page_url(); ?>"><i class="fa fa-google-plus"></i></a>
                        <a target="_BLANK" href="<?php echo stachethemes_ec_single::get_page_url(); ?>"><i class="fa fa-link"></i></a>

                    <?php endif; ?>
                </div>

                <?php if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'show_export_buttons') == '1' ) : ?>
                    <div class="stec-layout-single-export">
                        <form method="POST"> 
                            <button href="" class="stec-layout-single-button-sec-style stec-layout-event-btn-sec-fontandcolor"><?php $stachethemes_ec_main->lang('Export to .ICS file', true); ?></button>
                            <input type="hidden" value="<?php echo $event->general->id; ?>" name="event_id">
                            <input type="hidden" value="<?php echo $event->general->calid; ?>" name="calendar_id">
                            <input type="hidden" value="stec_export_to_ics" name="task">
                        </form>

                        <a class="stec-layout-single-button-sec-style stec-layout-event-btn-sec-fontandcolor" href="<?php echo stachethemes_ec_single::get_google_import_link($event); ?>" target="_BLANK" class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Import to Google Calendar', true); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if ( $event->general->location != '' ) : ?>

            <div class="stec-layout-single-location" itemprop="location" itemscope itemtype="http://schema.org/Place">

                <div class="stec-layout-single-location-flex">

                    <div class="stec-layout-single-location-left">
                        <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Location', true); ?></p>
                        <span class="stec-layout-single-location-address stec-layout-event-text-fontandcolor" data-location-use-coord="<?php
                        if ( $event->general->location_use_coord ) {
                            echo $event->general->location_forecast;
                        }
                        ?>" itemprop="name"><?php echo $event->general->location; ?></span>
                    </div>

                    <div class="stec-layout-single-location-right">
                        <p class="stec-layout-single-location-directions-title stec-layout-event-title2-fontandcolor"
                           itemprop="address" itemscope itemtype="http://schema.org/Text"
                           ><?php $stachethemes_ec_main->lang('Directions', true); ?></p>
                    </div>

                </div>

                <div class="stec-layout-single-location-flex">

                    <div class="stec-layout-single-location-left" itemprop="hasMap">
                        <div class="stec-layout-single-location-gmap" itemprop="map" itemtype="https://schema.org/Map"></div>
                    </div>

                    <div class="stec-layout-single-location-right">
                        <input class="stec-layout-event-input-fontandcolor" type="text" name="start" placeholder="<?php $stachethemes_ec_main->lang('Start Location', true); ?>" />
                        <input class="stec-layout-event-input-fontandcolor" type="text" name="end" placeholder="<?php $stachethemes_ec_main->lang('Destination', true); ?>" />
                        <div class="stec-layout-single-button-style stec-layout-single-location-get-direction-btn stec-layout-event-btn-fontandcolor"><p><?php $stachethemes_ec_main->lang('Get Directions', true); ?></p></div>
                        <p class="stec-layout-single-location-direction-error stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Could not find route!', true); ?></p>
                    </div>

                </div>

                <?php if ( $event->general->location_details != '' ) : ?>

                    <p class="stec-layout-single-location-details-title stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Location Details', true); ?></p>

                    <p class='stec-layout-single-location-details stec-layout-event-text-fontandcolor' itemprop="address">
                        <?php echo $event->general->location_details; ?>
                    </p>

                <?php endif; ?>

            </div>

        <?php endif; ?>

        <?php if ( stachethemes_ec_single::event_has_tabs($event) ) : ?>

            <div class="stec-layout-single-tabs">

                <ul class="stec-layout-single-tabs-list">

                    <?php if ( !empty($event->schedule) ) : ?>
                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-schedule">
                            <i class="fa fa-th-list"></i>
                            <p><?php $stachethemes_ec_main->lang('Schedule', true); ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if ( !empty($event->guests) ) : ?>
                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-guests">
                            <i class="fa fa-star-o"></i>
                            <p><?php $stachethemes_ec_main->lang('Guests', true); ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if ( !empty($event->woocommerce) ) : ?>
                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-woocommerce">
                            <i class="fa fa-shopping-cart"></i>
                            <p><?php $stachethemes_ec_main->lang('Shop', true); ?></p>
                        </li>
                    <?php endif; ?>

                    <?php
                    if ( ($event->general->location_forecast) &&
                            $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'weather_api_key') != "" ) :
                        ?>
                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-forecast">
                            <i class="fa fa-sun-o"></i>
                            <p><?php $stachethemes_ec_main->lang('Forecast', true); ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if ( $event->attendance ) : ?>

                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-attendance">
                            <i class="fa fa-user"></i>
                            <p><?php $stachethemes_ec_main->lang('Attendance', true); ?></p>
                        </li>

                    <?php endif; ?>

                    <?php if ( $event->general->comments == '1' ) : ?>

                        <li class="stec-layout-event-title2-fontandcolor" data-tab="stec-layout-single-comments">
                            <i class="fa fa-commenting-o"></i>
                            <p><?php $stachethemes_ec_main->lang('Comments', true); ?></p>
                        </li>

                    <?php endif; ?>

                </ul>

                <div class="stec-layout-event-single-tabs-content">

                    <?php if ( !empty($event->schedule) ) : ?>

                        <div class="stec-layout-single-schedule">

                            <?php foreach ( $event->schedule as $schedule ) : ?>

                                <div class="stec-layout-single-schedule-tab<?php
                                if ( $schedule->description == '' ) {
                                    echo ' stec-layout-single-schedule-tab-no-desc';
                                }

                                if ( $schedule->icon == 'fa' ) {
                                    echo ' stec-layout-single-schedule-tab-no-icon';
                                }
                                ?>">

                                    <div class="stec-layout-single-schedule-tab-preview">
                                        <div class="stec-layout-single-schedule-tab-left">
                                            <span class=""><?php echo stachethemes_ec_single::get_the_schedule_timespan($schedule->start_date, $event); ?></span>
                                        </div>
                                        <div class="stec-layout-single-schedule-tab-right">

                                            <div class="stec-layout-single-schedule-tab-right-title">
                                                <i style="color: <?php echo $schedule->icon_color; ?>" 
                                                   class="<?php echo $schedule->icon; ?>"></i><span class=""><?php echo $schedule->title; ?></span>
                                            </div>

                                            <div class="stec-layout-single-schedule-tab-toggle">
                                                <i class="fa fa-plus"></i>
                                                <i class="fa fa-minus"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ( $schedule->description != '' ) : ?>
                                        <div class="stec-layout-single-schedule-tab-desc">
                                            <span class="stec-layout-event-text-fontandcolor"><?php echo nl2br($schedule->description); ?></span>
                                        </div>
                                    <?php endif; ?>

                                </div>

                            <?php endforeach; ?>

                        </div>

                    <?php endif; ?>


                    <?php if ( !empty($event->guests) ) : ?>

                        <div class="stec-layout-single-guests">

                            <?php foreach ( $event->guests as $guest ) : ?>

                                <div class="stec-layout-single-guests-guest">

                                    <div class="stec-layout-single-guests-guest-left">
                                        <div class="stec-layout-single-guests-guest-left-avatar">
                                            <img alt="<?php echo $guest->name; ?>" src="<?php echo $guest->photo_full; ?>">
                                            <ul>
                                                <?php $links = stachethemes_ec_single::get_guest_links($guest->links); ?>

                                                <?php foreach ( $links as $k => $link ) : ?>
                                                    <li class="stec-layout-single-guests-guest-left-avatar-icon-position-<?php echo $k; ?>">
                                                        <a href="<?php echo $link->url; ?>">
                                                            <i class="<?php echo $link->icon; ?>"></i>
                                                        </a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="stec-layout-single-guests-guest-right">
                                        <p class="stec-layout-single-guests-guest-right-title stec-layout-event-title2-fontandcolor"><?php echo $guest->name; ?></p>
                                        <div class="stec-layout-single-guests-guest-right-desc  stec-layout-event-text-fontandcolor">
                                            <p><?php echo $guest->about; ?></p>
                                        </div>
                                    </div>

                                </div>

                            <?php endforeach; ?>

                        </div>

                    <?php endif; ?>


                    <?php if ( !empty($event->woocommerce) ) : ?>

                        <div class="stec-layout-single-woocommerce">

                            <div class="stec-layout-single-woocommerce-top">
                                <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Product', true); ?></p>
                                <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Name', true); ?></p>
                                <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Quantity', true); ?></p>
                                <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Price', true); ?></p>
                                <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Action', true); ?></p>
                            </div>

                            <div class='stec-layout-single-woocommerce-products'>

                                <?php foreach ( $event->woocommerce as $product ) : ?>

                                    <div class="stec-layout-single-woocommerce-product">

                                        <div class="stec-layout-single-woocommerce-product-image">
                                            <?php
                                            echo $product->image;
                                            ?>
                                        </div>

                                        <div class="stec-layout-single-woocommerce-product-desc">

                                            <div class="stec-layout-single-woocommerce-product-status">

                                                <?php if ( $product->is_featured ): ?>
                                                    <span class="stec-layout-event-text-fontandcolor stec-layout-single-woocommerce-product-about-featured"><?php $stachethemes_ec_main->lang('featured', true); ?></span>
                                                <?php endif; ?>

                                                <?php if ( $product->is_on_sale ): ?>
                                                    <span class="stec-layout-event-text-fontandcolor stec-layout-single-woocommerce-product-about-sale"><?php $stachethemes_ec_main->lang('sale', true); ?></span>
                                                <?php endif; ?>

                                                <?php if ( !$product->is_in_stock ): ?>
                                                    <span class="stec-layout-event-text-fontandcolor stec-layout-single-woocommerce-product-about-outofstock"><?php $stachethemes_ec_main->lang('out of stock', true); ?></span>
                                                <?php endif; ?>
                                            </div>

                                            <p class="stec-layout-event-title2-fontandcolor"><?php echo $product->title; ?></p>
                                            <p class="stec-layout-event-text-fontandcolor"><?php
                                                $product->post_data = (object) $product->post_data;
                                                echo $product->post_data->excerpt;
                                                ?></p>
                                        </div>

                                        <p class="stec-layout-single-woocommerce-product-quantity stec-layout-event-title2-fontandcolor"><span class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('QTY:', true); ?></span><span><?php echo $product->stock_quantity == '' ? '-' : $product->stock_quantity; ?></span></p>

                                        <p class="stec-layout-single-woocommerce-product-price stec-layout-event-title2-fontandcolor"><span class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('PRICE:', true); ?></span><?php echo stachethemes_ec_single::get_product_price($product); ?></p>

                                        <div>
                                            <?php if ( $product->is_in_stock ): ?>
                                                <div class="stec-layout-single-woocommerce-product-buy-addtocart 
                                                     stec-layout-single-button-style 
                                                     stec-layout-event-btn-fontandcolor"

                                                     data-pid="<?php echo $product->product_id; ?>" 
                                                     data-quantity="1" 
                                                     data-sku="<?php echo $product->sku; ?>">

                                                    <p><?php $stachethemes_ec_main->lang('Add to Cart', true); ?></p>
                                                    <i class="fa fa-shopping-cart"></i>
                                                </div>
                                                <div class="stec-layout-single-woocommerce-product-buy-ajax-status"></div>
                                            <?php endif; ?>
                                        </div>

                                    </div>

                                <?php endforeach; ?>

                            </div>

                            <div class='stec-layout-single-woocommerce-links'>
                                <a class="stec-layout-single-button-sec-style stec-layout-event-btn-sec-fontandcolor" href='<?php echo wc_get_checkout_url(); ?>'><?php echo $stachethemes_ec_main->lang('Checkout', true); ?></a>
                                <a class="stec-layout-single-button-sec-style stec-layout-event-btn-sec-fontandcolor" href='<?php echo wc_get_cart_url(); ?>'><?php echo $stachethemes_ec_main->lang('View Cart', true); ?></a>
                            </div>

                        </div>

                    <?php endif; ?>


                    <?php if ( $event->general->location_forecast ) : ?>

                        <div class="stec-layout-single-forecast">

                            <p class="errorna stec-layout-event-title-fontandcolor"><?php $stachethemes_ec_main->lang('Weather data is currently not available for this location', true); ?></p>

                            <div class="stec-layout-single-forecast-content">

                                <div class="stec-layout-single-forecast-top">

                                    <p class="stec-layout-single-forecast-top-title"><?php $stachethemes_ec_main->lang('Weather Report', true); ?></p>
                                    <p class="stec-layout-single-forecast-top-date"><?php $stachethemes_ec_main->lang('Today', true); ?> stec_replace_today_date</p>

                                </div>

                                <div class="stec-layout-single-forecast-today">

                                    <div class="stec-layout-single-forecast-today-left">

                                        <div class="stec-layout-single-forecast-today-left-icon">
                                            stec_replace_today_icon_div
                                        </div>

                                        <div>
                                            <p class="stec-layout-single-forecast-today-left-current-text">stec_replace_current_summary_text</p>
                                            <p class="stec-layout-single-forecast-today-left-current-temp">stec_replace_current_temp &deg;stec_replace_current_temp_units</p>
                                        </div>

                                    </div>

                                    <div class="stec-layout-single-forecast-today-right">
                                        <p class=""><?php $stachethemes_ec_main->lang('Wind', true); ?> <span>stec_replace_current_wind stec_replace_current_wind_units stec_replace_current_wind_direction</span></p>
                                        <p class=""><?php $stachethemes_ec_main->lang('Humidity', true); ?> <span>stec_replace_current_humidity %</span></p>
                                        <p class=""><?php $stachethemes_ec_main->lang('Feels like', true); ?> <span>stec_replace_current_feels_like &deg;stec_replace_current_temp_units</span></p>
                                    </div>

                                </div>

                                <div class="stec-layout-single-forecast-details">

                                    <div class="stec-layout-single-forecast-details-left">
                                        <p class=""><?php $stachethemes_ec_main->lang('Forecast', true); ?></p>

                                        <div class="stec-layout-single-forecast-details-left-forecast">
                                            <div class="stec-layout-single-forecast-details-left-forecast-top">
                                                <p><?php $stachethemes_ec_main->lang('Date', true); ?></p>
                                                <p><?php $stachethemes_ec_main->lang('Weather', true); ?></p>
                                                <p><?php $stachethemes_ec_main->lang('Temp', true); ?></p>
                                            </div>

                                            <div class="stec-layout-single-forecast-details-left-forecast-day stec-layout-single-forecast-details-left-forecast-day-template">
                                                <p>stec_replace_date</p>
                                                stec_replace_icon_div
                                                <p>stec_replace_min / stec_replace_max &deg;stec_replace_temp_units</p>
                                            </div>

                                            stec_replace_5days
                                        </div>

                                    </div>

                                    <div class="stec-layout-single-forecast-details-right">
                                        <p class=""><?php $stachethemes_ec_main->lang('Next 24 Hours', true); ?></p>

                                        <div class="stec-layout-single-forecast-details-chart">
                                            <canvas></canvas>
                                        </div>
                                    </div>
                                </div>

                                <p class="stec-layout-single-forecast-credits"><?php $stachethemes_ec_main->lang('Powered by', true); ?> Forecast.io</p>

                            </div>

                        </div>

                    <?php endif; ?>

                    <?php if ( $event->attendance ) : ?>

                        <div class="stec-layout-single-attendance">


                            <?php if ( stachethemes_ec_single::user_is_invited($event) ) : ?>

                                <div class="stec-layout-single-attendance-invited">
                                    <p class="stec-layout-event-title2-fontandcolor">
                                        <?php $stachethemes_ec_main->lang('You are invited to this event!', true); ?>
                                    </p>
                                    <ul class="stec-layout-single-attendance-invited-buttons">
                                        <li class="stec-layout-single-button-style stec-layout-single-attendance-invited-buttons-accept stec-layout-event-btn-fontandcolor <?php
                                        if ( stachethemes_ec_single::get_user_attendance_status($event) == 1 ) {
                                            echo 'active';
                                        }
                                        ?>">
                                            <p><?php $stachethemes_ec_main->lang('Attend', true); ?></p>
                                        </li>

                                        <li class="stec-layout-single-button-style stec-layout-single-attendance-invited-buttons-decline stec-layout-event-btn-fontandcolor <?php
                                        if ( stachethemes_ec_single::get_user_attendance_status($event) == 2 ) {
                                            echo 'active';
                                        }
                                        ?>">
                                            <p><?php $stachethemes_ec_main->lang('Decline', true); ?></p>
                                        </li>
                                    </ul>
                                </div>

                            <?php endif; ?>

                            <ul class="stec-layout-single-attendance-attendees">

                                <?php $attendees = stachethemes_ec_single::get_attendees($event); ?>

                                <?php foreach ( $attendees as $attendee ) : ?>

                                    <li class="stec-layout-single-attendance-attendee" itemprop="attendee" itemscope itemtype="http://schema.org/Person">

                                        <div data-userid="<?php echo $attendee->userid; ?>" class="stec-layout-single-attendance-attendee-avatar">
                                            <img src="<?php echo $attendee->avatar; ?>" alt="<?php echo $attendee->name; ?>" />
                                            <meta itemprop="image" contnet="<?php echo $attendee->avatar; ?>" />
                                            <ul>
                                                <?php
                                                switch ( stachethemes_ec_single::get_user_attendance_status($event, $attendee->userid) ) :

                                                    case 1:
                                                        echo '<li class=""><i class="fa fa-check"></i></li>';
                                                        break;

                                                    case 2:
                                                        echo '<li class=""><i class="fa fa-times"></i></li>';
                                                        break;

                                                    case 0:
                                                    default:
                                                        echo '<li class=""><i class="fa fa-question"></i></li>';

                                                endswitch;
                                                ?>
                                            </ul>
                                        </div>

                                        <p class="stec-layout-event-title2-fontandcolor" itemprop="name"><?php echo $attendee->name; ?></p>
                                    </li>

                                <?php endforeach; ?>
                            </ul>

                        </div>

                    <?php endif; ?>

                    <?php if ( $event->general->comments == '1' ) : ?>

                        <div class="stec-layout-single-comments">
                            <div id="disqus_thread"></div>
                        </div>

                    <?php endif; ?>

                </div>

            </div>

        <?php endif ?>

    </div>

</div>