<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<li class="stec-event-create-form stec-layout-event">

    <div class="stec-event-create-form-preview stec-layout-event-preview">

        <div class="stec-layout-event-preview-left">
            <div class="stec-layout-event-create-form-preview-left-text">
                <p class="stec-layout-event-preview-left-text-title"><?php $stachethemes_ec_main->lang('Create an event', true); ?></p>
                <p class="stec-layout-event-preview-left-text-sub"><?php $stachethemes_ec_main->lang('All events must be approved by our moderators before they go live', true); ?></p>
            </div>
        </div>

        <?php if ( !$is_single_form ) : ?>

            <div class="stec-layout-event-preview-right">
                <div class="stec-layout-event-preview-right-event-toggle stec-layout-event-create-form-preview-right-event-toggle">
                    <i class="fa fa-plus"></i>
                    <i class="fa fa-minus"></i>
                </div>
            </div>

        <?php elseif ( $is_single_form && $is_popup ) : ?>

            <div class="stec-layout-event-preview-right">
                <div class="stec-layout-event-preview-right-event-toggle stec-layout-event-create-form-preview-right-event-toggle">
                    <i class="fa fa-plus"></i>
                    <i class="fa fa-times"></i>
                </div>
            </div>

        <?php endif; ?>

    </div>

    <div class="stec-layout-event-create-form-inner stec-layout-event-inner">
        <form>
            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Title', true); ?>
            </p>
            <input required="required" class="stec-layout-event-input-fontandcolor" name="summary" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('Event Title', true); ?>" />

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Description (optional)', true); ?>
            </p>
            <textarea class="stec-layout-event-input-fontandcolor" name="description" placeholder="<?php $stachethemes_ec_main->lang('Event Description', true); ?>"></textarea>

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Short Description (optional)', true); ?>
            </p>
            <input class="stec-layout-event-input-fontandcolor" type="text" name="description_short" placeholder="<?php $stachethemes_ec_main->lang('Few words about your event shown on the tooltip', true); ?>" />


            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Image (optional)', true); ?>
            </p>
            <input class="stec-layout-event-input-fontandcolor stec-layout-event-create-form-inner-date-image" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('Accepts jpeg and png files up to ' . stachethemes_event_calendar_create_event_front::get_max_upload_size() . 'MB', true); ?>" /> 
            <input class="stec-layout-event-create-form-inner-date-image-file" type="file" name="fileimage" accept="image/jpeg, image/png" />

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Location (optional)', true); ?>
            </p>
            <input class="stec-layout-event-input-fontandcolor" name="location" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('Google maps location', true); ?>" />

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Website URL (optional)', true); ?>
            </p>
            <input class="stec-layout-event-input-fontandcolor" name="link" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('URL Address', true); ?>" />

            <div class="stec-layout-event-create-form-inner-flexbox">

                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Calendar', true); ?>
                    </p>
                    <select class="stec-layout-event-input-fontandcolor" name="calendar_id" required="required">
                        <option value="" disabled selected><?php $stachethemes_ec_main->lang('-- Select Calendar --', true); ?></option>
                        <?php
                        $writable_cals = stachethemes_event_calendar_create_event_front::get_writable_calendar_list();

                        foreach ( $writable_cals as $cal ) :
                            
                            if (!empty($writable_cal_array)) {
                                if (!in_array($cal->id, $writable_cal_array)) {
                                    continue;
                                }
                            }
                            
                            ?>
                            <option data-color="<?php echo $cal->color; ?>" value="<?php echo $cal->id; ?>"><?php echo $cal->title; ?></option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>

                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Icon', true); ?>
                    </p>
                    <select class="stec-layout-event-input-fontandcolor" name="icon">
                        <?php
                        $icons = stachethemes_event_calendar_create_event_front::get_icon_list();

                        foreach ( $icons as $key => $value ) :
                            ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>

            </div>

            <div class="stec-layout-event-create-form-inner-flexbox">
                <input class="stec-layout-event-create-form-inner-colorpicker" name="event_color" type="text" value="#fff" />
                <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                    <?php $stachethemes_ec_main->lang('Color. By default will use the calendar color', true); ?>
                </p>
            </div>

            <?php
            $hours_list   = stachethemes_event_calendar_create_event_front::get_hours_array();
            $minutes_list = stachethemes_event_calendar_create_event_front::get_minutes_array();
            ?>

            <div class="stec-layout-event-create-form-inner-flexbox">
                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Starts On', true); ?>
                    </p>
                    <input class="stec-layout-event-input-fontandcolor stec-layout-event-create-form-inner-date" name="start_date" type="text" value="" required="required" />
                </div>

                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('From', true); ?>
                    </p>
                    <div class="stec-layout-event-create-form-inner-flexbox stec-layout-event-create-form-time stec-layout-event-create-form-inner-element-nomargin">
                        <div>
                            <select class="stec-layout-event-input-fontandcolor" name="start_time_hours" required="required">
                                <?php
                                foreach ( $hours_list as $key => $value ) :
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div>
                            <select class="stec-layout-event-input-fontandcolor" name="start_time_minutes" required="required">
                                <?php
                                foreach ( $minutes_list as $key => $value ) :
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>


            <div class="stec-layout-event-create-form-inner-flexbox">
                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Ends On', true); ?>
                    </p>
                    <input class="stec-layout-event-input-fontandcolor stec-layout-event-create-form-inner-date" name="end_date" type="text" value="" required="required" />
                </div>

                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('To', true); ?>
                    </p>
                    <div class="stec-layout-event-create-form-inner-flexbox stec-layout-event-create-form-time stec-layout-event-create-form-inner-element-nomargin">
                        <div>
                            <select class="stec-layout-event-input-fontandcolor" name="end_time_hours" required="required">
                                <?php
                                foreach ( $hours_list as $key => $value ) :
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div>
                            <select class="stec-layout-event-input-fontandcolor" name="end_time_minutes" required="required">
                                <?php
                                foreach ( $minutes_list as $key => $value ) :
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>


            <div class="stec-layout-event-create-form-inner-flexbox">
                <input type="checkbox" name="all_day" />
                <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                    <?php $stachethemes_ec_main->lang('All Day', true); ?>
                </p>
            </div>

            <div class="stec-layout-event-create-form-inner-flexbox">

                <div>

                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Repeat Event', true); ?>
                    </p>

                    <select class="stec-layout-event-input-fontandcolor" name="event_repeat">
                        <option selected="selected" value="0"><?php $stachethemes_ec_main->lang('No Repeat', true); ?></option>
                        <option value="1"><?php $stachethemes_ec_main->lang('Daily', true); ?></option>
                        <option value="2"><?php $stachethemes_ec_main->lang('Weekly', true); ?></option>
                        <option value="3"><?php $stachethemes_ec_main->lang('Monthly', true); ?></option>
                        <option value="4"><?php $stachethemes_ec_main->lang('Yearly', true); ?></option>
                    </select>
                </div>

                <div class="stec-layout-event-create-form-inner-repeat-gap-block">
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Repeat gap', true); ?>
                    </p>
                    <input class="stec-layout-event-input-fontandcolor" name="repeat_gap" type="text" value="0" />
                </div>

            </div>

            <div class="stec-layout-event-create-form-inner-repeat-sub">


                <div class="stec-layout-event-create-form-inner-weekly-by-day">

                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Repeat by day', true); ?>
                    </p>

                    <div class="stec-layout-event-create-form-inner-flexbox stec-layout-event-create-form-inner-element-nomargin">
                        <span>
                            <input name="SU" title="<?php $stachethemes_ec_main->lang('Sunday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Sunday', true) ?>"><?php $stachethemes_ec_main->lang('SU', true) ?></label>
                        </span>
                        <span>
                            <input name="MO" title="<?php $stachethemes_ec_main->lang('Monday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Monday', true) ?>"><?php $stachethemes_ec_main->lang('MO', true) ?></label>
                        </span>
                        <span>
                            <input name="TU" title="<?php $stachethemes_ec_main->lang('Tuesday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Tuesday', true) ?>"><?php $stachethemes_ec_main->lang('TU', true) ?></label>
                        </span>
                        <span>
                            <input name="WE" title="<?php $stachethemes_ec_main->lang('Wednesday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Wednesday', true) ?>"><?php $stachethemes_ec_main->lang('WE', true) ?></label>
                        </span>
                        <span>
                            <input name="TH" title="<?php $stachethemes_ec_main->lang('Thursday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Thursday', true) ?>"><?php $stachethemes_ec_main->lang('TH', true) ?></label>
                        </span>
                        <span>
                            <input name="FR" title="<?php $stachethemes_ec_main->lang('Friday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Friday', true) ?>"><?php $stachethemes_ec_main->lang('FR', true) ?></label>
                        </span>
                        <span>
                            <input name="SA" title="<?php $stachethemes_ec_main->lang('Saturday', true) ?>" type="checkbox">
                            <label for="" class="stec-layout-event-title2-fontandcolor" title="<?php $stachethemes_ec_main->lang('Saturday', true) ?>"><?php $stachethemes_ec_main->lang('SA', true) ?></label>
                        </span>
                    </div>


                </div>


                <div class="stec-layout-event-create-form-inner-repeat-ends-on-block">

                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Repeat Ends On', true); ?>
                    </p>

                    <div class="">
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-never" name="repeat_endson" value="0" checked="checked" type="radio">
                            <label for="stec-repeater-popup-repeat-endson-never">
                                <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Never', true) ?></p>
                            </label>
                        </span>
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-after-n" name="repeat_endson" value="1" type="radio">
                            <label class="stec-layout-event-title2-fontandcolor"  for="stec-repeater-popup-repeat-endson-after-n">
                                <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('After', true) ?></p>
                                <input class="stec-layout-event-input-fontandcolor" name="repeat_occurences" size="2" value="" disabled="disabled" type="text">
                                <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('occurences', true) ?></p>
                            </label>
                        </span>
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-date" name="repeat_endson" value="2" type="radio">
                            <label class="stec-layout-event-title2-fontandcolor"  for="stec-repeater-popup-repeat-endson-date">
                                <p class="stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('On Date', true) ?></p>
                                <input id="repeat_end_date" class="stec-layout-event-input-fontandcolor" name="repeat_ends_on_date" size="14" value="" disabled="disabled" autocomplete="off" type="text">
                            </label>
                        </span>
                    </div>

                </div>

                <input type="hidden" name="rrule" value="" />

            </div>

            <div class="stec-layout-event-create-form-inner-flexbox">
                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Search Keywords', true); ?>
                    </p>
                    <input class="stec-layout-event-input-fontandcolor" name="keywords" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('Keywords separated by space', true); ?>" />
                </div>

                <div>
                    <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                        <?php $stachethemes_ec_main->lang('Counter', true); ?>
                    </p>
                    <select class="stec-layout-event-input-fontandcolor" name="counter">
                        <option value="0"><?php echo $stachethemes_ec_main->lang('Disable'); ?></option>
                        <option value="1"><?php echo $stachethemes_ec_main->lang('Enable'); ?></option>
                    </select>
                </div>
            </div>

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Your E-mail', true); ?>
            </p>

            <input required="required" class="stec-layout-event-input-fontandcolor" name="contact_email" type="email" value="<?php echo stachethemes_event_calendar_query::get_current_user_email(); ?>" placeholder="<?php $stachethemes_ec_main->lang('Contact E-Mail', true); ?>" />

            <p class="stec-layout-event-create-form-inner-label stec-layout-event-title2-fontandcolor">
                <?php $stachethemes_ec_main->lang('Notes to the reviewer (optional)', true); ?>
            </p>
            <textarea class="stec-layout-event-input-fontandcolor" name="review_note" type="text" value="" placeholder="<?php $stachethemes_ec_main->lang('Additional info visible to the reviewer only', true); ?>"></textarea>

            <div class="stec-layout-event-create-form-g-recaptcha"></div>

            <div class="stec-layout-event-create-form-inner-submit-flexbox">
                <button class="stec-layout-event-create-form-inner-submit stec-layout-event-btn-fontandcolor stec-layout-event-inner-button-style"><?php $stachethemes_ec_main->lang('Submit Event', true); ?></button>
                <i class="fa fa-check"></i>
                <i class="fa fa-times"></i>
            </div>

        </form>
    </div>

</li>