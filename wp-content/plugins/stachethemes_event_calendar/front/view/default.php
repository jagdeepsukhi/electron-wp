<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<div id="<?php echo $calendar; ?>" class="stec">
    
    <?php if ($calendar->get_shortcode_option('stec_menu__general', 'show_top') == '1') : ?>

        <?php include("top.inc.php"); ?>

    <?php endif; ?>

    <div class="stec-layout">
        <?php include("layout.agenda.inc.php"); ?>
        <?php include("layout.month.inc.php"); ?>
        <?php include("layout.week.inc.php"); ?>
        <?php include("layout.day.inc.php"); ?>
    </div>

    <div class="stec-layout-event-preview-reminder-template">
        <ul class="stec-layout-event-preview-reminder">
            <li>
                <input type="email" name="email" value="<?php echo stachethemes_event_calendar_query::get_current_user_email(); ?>" placeholder="<?php $stachethemes_ec_main->lang('E-Mail Address', true); ?>" />
            </li>
            <li>
                <input type="text" name="number" value="" />
            </li>
            <li class="stec-layout-event-preview-reminder-units-selector">
                <p data-value='hours'><?php $stachethemes_ec_main->lang('hours', true); ?></p>
                <ul>
                    <li data-value="hours"><?php $stachethemes_ec_main->lang('hours', true); ?></li>
                    <li data-value="days"><?php $stachethemes_ec_main->lang('days', true); ?></li>
                    <li data-value="weeks"><?php $stachethemes_ec_main->lang('weeks', true); ?></li>
                </ul>
            </li>
            <li>
                <button class="stec-layout-event-preview-remind-button"><?php $stachethemes_ec_main->lang('Remind me', true); ?></button>
            </li>
        </ul>
    </div>

    <div class="stec-tooltip-template">
        <?php include("layout.tooltip.inc.php"); ?>
    </div>

    <div class="stec-event-template">
        <?php include("layout.event.inc.php"); ?>
    </div>

    <div class="stec-event-create-form-template">
        <?php 
            if ($stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'show_create_event_form') == '1') {
                
                $is_single_form = false;
                $is_popup = false;
                
                include("forms/create.form.inc.php"); 
            }
        ?>
    </div>

    <div class="stec-event-awaiting-approval-template">
        <?php include("layout.event.aaproval.inc.php"); ?>
    </div>

    <div class="stec-event-inner-template">
        <?php include("layout.event.inner.inc.php"); ?>
    </div>

    <div class="stec-preloader-template">
        <div class="stec-preloader"></div>
    </div>

</div>