<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<div class="stec-layout-event-inner-woocommerce-product stec-layout-event-inner-woocommerce-product-template">

    
    <div class="stec-layout-event-inner-woocommerce-product-image">
        stec_replace_product_image
    </div>
    
    <div class="stec-layout-event-inner-woocommerce-product-desc">
        
        <div class="stec-layout-event-inner-woocommerce-product-status">
            <span class="stec-layout-event-text-fontandcolor stec-layout-event-inner-woocommerce-product-about-featured"><?php $stachethemes_ec_main->lang('featured', true); ?></span>
            <span class="stec-layout-event-text-fontandcolor stec-layout-event-inner-woocommerce-product-about-sale"><?php $stachethemes_ec_main->lang('sale', true); ?></span>
            <span class="stec-layout-event-text-fontandcolor stec-layout-event-inner-woocommerce-product-about-outofstock"><?php $stachethemes_ec_main->lang('out of stock', true); ?></span>
        </div>
        
        <p class="stec-layout-event-title2-fontandcolor">stec_replace_product_title</p>
        <p class="stec-layout-event-text-fontandcolor">stec_replace_product_short_desc</p>
    </div>
    
    <p class="stec-layout-event-inner-woocommerce-product-quantity stec-layout-event-title2-fontandcolor"><span class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('QTY:', true); ?></span><span>stec_replace_product_quantity</span></p>
   
    <p class="stec-layout-event-inner-woocommerce-product-price stec-layout-event-title2-fontandcolor"><span class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('PRICE:', true); ?></span>stec_replace_product_price</p>
    
    <div>
        <div class="stec-layout-event-inner-woocommerce-product-buy-addtocart stec-layout-event-inner-button-style stec-layout-event-btn-fontandcolor">
            <p><?php $stachethemes_ec_main->lang('Add to Cart', true); ?></p>
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="stec-layout-event-inner-woocommerce-product-buy-ajax-status"></div>
    </div>
  
</div>

<div class="stec-layout-event-inner-woocommerce-top">
    <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Product', true); ?></p>
    <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Name', true); ?></p>
    <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Quantity', true); ?></p>
    <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Price', true); ?></p>
    <p class="stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Action', true); ?></p>
</div>

<div class='stec-layout-event-inner-woocommerce-products'>

    <!-- Products List -->

</div>

<div class='stec-layout-event-inner-woocommerce-links'>
    <a class="stec-layout-event-inner-button-sec-style stec-layout-event-btn-sec-fontandcolor" href='<?php echo wc_get_checkout_url(); ?>'><?php echo $stachethemes_ec_main->lang('Checkout', true); ?></a>
    <a class="stec-layout-event-inner-button-sec-style stec-layout-event-btn-sec-fontandcolor" href='<?php echo wc_get_cart_url(); ?>'><?php echo $stachethemes_ec_main->lang('View Cart', true); ?></a>
</div>