<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<p class="errorna stec-layout-event-title-fontandcolor"><?php $stachethemes_ec_main->lang('Weather data is currently not available for this location', true); ?></p>

<div class="stec-layout-event-inner-forecast-content">

    <div class="stec-layout-event-inner-forecast-top">

        <p class="stec-layout-event-inner-forecast-top-title"><?php $stachethemes_ec_main->lang('Weather Report', true); ?></p>
        <p class="stec-layout-event-inner-forecast-top-date"><?php $stachethemes_ec_main->lang('Today', true); ?> stec_replace_today_date</p>

    </div>

    <div class="stec-layout-event-inner-forecast-today">

        <div class="stec-layout-event-inner-forecast-today-left">

            <div class="stec-layout-event-inner-forecast-today-left-icon">
                stec_replace_today_icon_div
            </div>

            <div>
                <p class="stec-layout-event-inner-forecast-today-left-current-text">stec_replace_current_summary_text</p>
                <p class="stec-layout-event-inner-forecast-today-left-current-temp">stec_replace_current_temp &deg;stec_replace_current_temp_units</p>
            </div>

        </div>

        <div class="stec-layout-event-inner-forecast-today-right">
            <p class=""><?php $stachethemes_ec_main->lang('Wind', true); ?> <span>stec_replace_current_wind stec_replace_current_wind_units stec_replace_current_wind_direction</span></p>
            <p class=""><?php $stachethemes_ec_main->lang('Humidity', true); ?> <span>stec_replace_current_humidity %</span></p>
            <p class=""><?php $stachethemes_ec_main->lang('Feels like', true); ?> <span>stec_replace_current_feels_like &deg;stec_replace_current_temp_units</span></p>
        </div>

    </div>

    <div class="stec-layout-event-inner-forecast-details">

        <div class="stec-layout-event-inner-forecast-details-left">
            <p class=""><?php $stachethemes_ec_main->lang('Forecast', true); ?></p>

            <div class="stec-layout-event-inner-forecast-details-left-forecast">
                <div class="stec-layout-event-inner-forecast-details-left-forecast-top">
                    <p><?php $stachethemes_ec_main->lang('Date', true); ?></p>
                    <p><?php $stachethemes_ec_main->lang('Weather', true); ?></p>
                    <p><?php $stachethemes_ec_main->lang('Temp', true); ?></p>
                </div>

                <div class="stec-layout-event-inner-forecast-details-left-forecast-day stec-layout-event-inner-forecast-details-left-forecast-day-template">
                    <p>stec_replace_date</p>
                    stec_replace_icon_div
                    <p>stec_replace_min / stec_replace_max &deg;stec_replace_temp_units</p>
                </div>

                stec_replace_5days
            </div>

        </div>

        <div class="stec-layout-event-inner-forecast-details-right">
            <p class=""><?php $stachethemes_ec_main->lang('Next 24 Hours', true); ?></p>

            <div class="stec-layout-event-inner-forecast-details-chart">
                <canvas></canvas>
            </div>
        </div>
    </div>

    <p class="stec-layout-event-inner-forecast-credits"><?php $stachethemes_ec_main->lang('Powered by', true); ?> Forecast.io</p>

</div>