<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<div class="stec-layout-event-inner-location-left">
    <p class="stec-layout-event-inner-location-left-title stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Location', true); ?></p>
    <p class='stec-layout-event-inner-location-address stec-layout-event-text-fontandcolor'>stec_replace_location</p>

    <p class="stec-layout-event-inner-location-left-title stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Get Directions', true); ?></p>
    <input class="stec-layout-event-input-fontandcolor" type="text" name="start" placeholder="<?php $stachethemes_ec_main->lang('Start Location', true); ?>" />
    <input class="stec-layout-event-input-fontandcolor" type="text" name="end" placeholder="<?php $stachethemes_ec_main->lang('Destination', true); ?>" />
    <div class="stec-layout-event-inner-button-style stec-layout-event-inner-location-left-button stec-layout-event-btn-fontandcolor"><p><?php $stachethemes_ec_main->lang('Get Directions', true); ?></p></div>
    <p class="stec-layout-event-inner-location-direction-error stec-layout-event-text-fontandcolor"><?php $stachethemes_ec_main->lang('Could not find route!', true); ?></p>
    
</div>

<div class="stec-layout-event-inner-location-right">
    <div class="stec-layout-event-inner-location-right-gmap"></div>
</div>

<div class="stec-layout-event-inner-location-left stec-layout-event-inner-location-optional-details">

    <p class="stec-layout-event-inner-location-left-title stec-layout-event-title2-fontandcolor"><?php $stachethemes_ec_main->lang('Details', true); ?></p>
    <div class='stec-layout-event-inner-location-details stec-layout-event-text-fontandcolor'>
        stec_replace_details
    </div>
</div>