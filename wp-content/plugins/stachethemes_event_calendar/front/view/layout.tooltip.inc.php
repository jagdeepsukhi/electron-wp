<?php $stachethemes_ec_main = stachethemes_ec_main::get_instance(); ?>

<div class="stec-tooltip">

    
    <div class="stec-tooltip-image"> 
        stec_replace_image
    </div>
    
    stec_replace_icon
    
    <p class="stec-tooltip-title">stec_replace_summary</p>
    <div class="stec-tooltip-desc">stec_replace_desc_short</div>
    
    <div class="stec-tooltip-counter">
        <span>0d 0h 0m 0s</span>
    </div>
    <div class="stec-tooltip-status stec-tooltip-expired"><?php $stachethemes_ec_main->lang('Expired',true); ?></div>
    <div class="stec-tooltip-status stec-tooltip-progress"><?php $stachethemes_ec_main->lang('In Progress',true); ?></div>
    
    <p class="stec-tooltip-location"><i class="fa fa-map-marker"></i>stec_replace_location</p>
    <p class="stec-tooltip-timespan"><i class="fa fa-clock-o"></i>stec_replace_timespan</p>
    
</div>