<?php
$stachethemes_ec_main = stachethemes_ec_main::get_instance();
?>
<div class="stec-fixed-message stec-fixed-message-error">

    <div>
        <i class="fa fa-exclamation"></i>
        <div>
            <p><?php $stachethemes_ec_main->lang('Calendar Notification', true); ?></p>
            <span>stec_replace_msg</span>
        </div>
    </div>

    <a href="javascript:void(0);"><i class="fa fa-times"></i></a>
</div>