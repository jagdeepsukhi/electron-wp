<?php

namespace stachethemes\event_calendar;




/**
 * Abstract Main Template Class
 * @version 1.5
 */
abstract class stachethemes_main_template {



    protected $_text_domain        = "";
    protected $_front_js_locale    = array();
    protected $_path               = array();
    protected $_main_menu          = array();
    protected $_sub_menu           = array();
    protected $_admin_js           = array();
    protected $_admin_css          = array();
    protected $_force_load_scripts = false;
    protected $_ras_ai             = 0;
    protected $_loaded_head        = false;
    protected $_loaded_locale      = array();



    function __toString() {
        return "";
    }



    protected function __construct() {

        $base = __dir__ . "/../";

        $this->set_paths(array(
                "ROOT"        => $base,
                "ADMIN"       => $base . "admin/",
                "ADMIN_CSS"   => $base . "admin/css/",
                "ADMIN_JS"    => $base . "admin/js/",
                "ADMIN_CLASS" => $base . "admin/class/",
                "ADMIN_LIBS"  => $base . "admin/libs/",
                "ADMIN_VIEW"  => $base . "admin/view/",
                "FRONT"       => $base . "front/",
                "FRONT_CSS"   => $base . "front/css/",
                "FRONT_JS"    => $base . "front/js/",
                "FRONT_CLASS" => $base . "front/class/",
                "FRONT_VIEW"  => $base . "front/view/",
                "FONTS"       => $base . "fonts/",
                "LANG"        => $base . "languages/",
        ));

        add_action('wp_head', array($this, "set_ajaxurl"));

        add_action('style_loader_tag', array($this, 'css_less'));
    }



    /**
     * Support for wp_enqueue_style .less files
     */
    public function css_less($tag) {

        if ( strpos($tag, '.less?ver') !== false ) {
            $tag = str_replace("text/css", "text/less", $tag);
        }

        return $tag;
    }



    /**
     * Define ajaxurl path for js
     */
    public function set_ajaxurl() {
        ?>
        <script type="text/javascript">
            if ( typeof ajaxurl === "undefined" ) {
                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            }
        </script>
        <?php
    }



    /**
     * Register admin ajax function
     * @param string $action
     * @param array $function ex. array($this, func)
     * @return this;
     */
    public function ajax_action($action, $function) {
        add_action("wp_ajax_$action", $function);
        return $this;
    }



    /**
     * Register public ajax function
     * @param string $action
     * @param array $function ex. array($this, func)
     * @return this;
     */
    public function ajax_public_action($action, $function) {
        add_action("wp_ajax_$action", $function);
        add_action("wp_ajax_nopriv_$action", $function);
        return $this;
    }



    /**
     * Add path to _path array
     * @param string $paths
     */
    protected function set_paths($paths) {
        $this->_path = $paths;
    }



    /**
     * Get path from _path array
     * @param string $path
     * @return type
     */
    public function get_path($path) {
        return $this->_path[$path];
    }



    /**
     * Language files must be named like following: domain-locale.mo
     * @param STRING $domain
     * @return bool
     */
    public function load_textdomain($domain) {

        $this->_text_domain = $domain;

        return load_plugin_textdomain($this->_text_domain, false, plugin_basename(__DIR__) . '/../languages');
    }



    /**
     * @return string plugin text domain
     */
    public function get_text_domain() {
        return $this->_text_domain;
    }



    /**
     * @param array $files PATH_TO_ADMIN_LIBS\$file
     * @return this;
     */
    public function load_admin_lib($files = array()) {

        foreach ( $files as $file ) {
            require_once($this->get_path("ADMIN_LIBS") . "$file");
        }

        return $this;
    }



    /**
     * @param array $files PATH_TO_ADMIN_CLASS\$file
     * @return this;
     */
    public function load_admin_classes($files = array()) {

        foreach ( $files as $file ) {
            require_once($this->get_path("ADMIN_CLASS") . $file);
        }

        return $this;
    }



    /**
     * @param array $files PATH_TO_FRONT_CLASS\$file
     * @return this;
     */
    public function load_front_classes($files = array()) {

        foreach ( $files as $file ) {
            require_once($this->get_path("FRONT_CLASS") . $file);
        }

        return $this;
    }



    /**
     * Adds the plugin main menu info to _main_menu array
     * @param string $name
     * @param string $slug
     * @param string $icon
     * @param int $priority
     * @return this;
     */
    public function add_menu($name, $slug, $icon, $priority) {

        $this->_main_menu = array(
                "name"     => $name,
                "slug"     => $slug,
                "icon"     => $icon,
                "priority" => $priority
        );

        return $this;
    }



    /**
     * Adds submenu to _sub_menu array
     * @param string $name
     * @param string $slug
     * @return this;
     */
    public function add_submenu($name, $slug, $literal = false) {

        $submenu = array(
                "name"    => $name,
                "slug"    => $slug,
                "literal" => $literal
        );

        array_push($this->_sub_menu, $submenu);

        return $this;
    }



    /**
     * Register admin menu (hook to admin_menu)
     */
    public function register_menus() {
        add_action("admin_menu", array($this, "create_menus"));
    }



    /**
     * Requires admin page
     * slug__foldername = PATH_TO_ADMIN_VIEW foldername/index.php
     */
    public function load_admin_page() {
        $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_STRING);

        if ( $page ) {
            $page = explode("__", $page);
            require($this->get_path("ADMIN_VIEW") . str_replace(array(".", "..", "/", "\\"), "", $page[1]) . "/index.php");
        }
    }



    /**
     * Create admin menu and enqueue styles and scripts from load_admin_css and load_admin_js arrays
     */
    public function create_menus() {

        $menu = add_menu_page(
                $this->_main_menu["name"], $this->_main_menu["name"], "edit_posts", $this->_main_menu["slug"], array($this, 'load_admin_page'), $this->_main_menu["icon"], $this->_main_menu["priority"]);

        add_action('admin_print_styles-' . $menu, array($this, "load_admin_css"));
        add_action('admin_print_scripts-' . $menu, array($this, "load_admin_js"));

        if ( is_array($this->_sub_menu) ) {
            foreach ( $this->_sub_menu as $submenu ) {

                if ( $submenu['literal'] !== false ) {

                    $submenu = add_submenu_page(
                            $this->_main_menu["slug"], $submenu["name"], $submenu["name"], "edit_posts", $submenu['literal'], false);
                } else {

                    $submenu = add_submenu_page(
                            $this->_main_menu["slug"], $submenu["name"], $submenu["name"], "edit_posts", $submenu["slug"], array($this, 'load_admin_page'));
                }



                add_action('admin_print_styles-' . $submenu, array($this, "load_admin_css"));
                add_action('admin_print_scripts-' . $submenu, array($this, "load_admin_js"));
            }
        }
    }



    /**
     * Enqueue admin js from _admin_css array
     */
    public function load_admin_js() {

        // media
        wp_enqueue_media();

        foreach ( $this->_admin_js as $js ) {
            $require = array();

            // enqueue required scripts if any
            if ( $js["require"] != "" ) {
                $require = explode(",", $js["require"]);
                foreach ( $require as $req ) {
                    wp_enqueue_script($req);
                }
            }

            wp_enqueue_script($js["slug"], $js["url"], $require);
        }
    }



    /**
     * Enqueue admin css from _admin_css array
     */
    public function load_admin_css() {

        foreach ( $this->_admin_css as $css ) {
            wp_enqueue_style($css["slug"], $css["url"]);
        }
    }



    /**
     * Add js script to the _admin_js array
     * @param string $slug slug
     * @param string $url style location. Relative path to admin fonts folder. Add // for absolute path (http://...)
     * @param string $require list with required scripts 'jquery','jquery-ui' etc... comma separated
     * @return this;
     */
    public function add_menu_js($slug, $url, $require = "") {

        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("ADMIN_JS") . "/.");
        }

        $js = array(
                "slug"    => $slug,
                "url"     => $url,
                "require" => $require
        );

        array_push($this->_admin_js, $js);

        return $this;
    }



    /**
     * Add font style to the _admin_css array
     * @param string $slug slug
     * @param string $url style location. Relative path to admin fonts folder. Add // for absolute path (http://...)
     * @return this;
     */
    public function add_menu_font($slug, $url) {

        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("FONTS") . "/.");
        }

        $css = array(
                "slug" => $slug,
                "url"  => $url
        );

        array_push($this->_admin_css, $css);

        return $this;
    }



    /**
     * Add css style to the _admin_css array
     * @param string $slug slug
     * @param string $url style location. Relative path to admin css folder. Add // for absolute path (http://...)
     * @return this;
     */
    public function add_menu_css($slug, $url) {

        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("ADMIN_CSS") . "/.");
        }

        $css = array(
                "slug" => $slug,
                "url"  => $url
        );

        array_push($this->_admin_css, $css);

        return $this;
    }



    /**
     * Load js locales
     */
    public function load_js_locales() {

        foreach ( $this->_front_js_locale as $locale ) :

            if ( in_array($locale['name'], $this->_loaded_locale) ) {
                continue;
            }

            if ( wp_localize_script($locale['handle'], $locale['name'], $locale['data']) ) {
                $this->_loaded_locale[] = $locale['name'];
            }
        endforeach;
    }



    /**
     * Add js locale to _front_js_locale array
     */
    public function localize($handle, $name, $data) {
        $locale = array(
                "handle" => $handle,
                "name"   => $name,
                "data"   => $data
        );
        array_push($this->_front_js_locale, $locale);
    }



    /**
     * Force load shortcode scripts and css true, false
     * @param bool $val
     */
    public function force_load_scripts($val) {

        $this->_force_load_scripts = (bool) $val;
    }



    public function scripts_are_forced() {

        return $this->_force_load_scripts;
    }



    /**
     *  Add Custom head attributes 
     */
    public function load_head() {
        
    }



    /**
     * Translates string to text-domain
     */
    public function lang($text, $echo = false) {

        if ( $echo ) {
            _e($text, $this->_text_domain);
        } else {
            return __($text, $this->_text_domain);
        }
    }



    public function get_admin_setting_value($page, $name) {
        $setting = get_option($page, array());

        return isset($setting[$name]["value"]) ? $setting[$name]["value"] : null;
    }



    /**
     * Get admin setting array by $page and or $name
     * @param string $page 
     * @param string $name
     * @return array
     */
    public function get_admin_setting($page, $name = false) {

        $setting = get_option($page, array());

        if ( $name === false ) {
            return $setting;
        }

        return !empty($setting) && isset($setting[$name]) ? $setting[$name] : null;
    }



    /**
     * Converts settings to style if setting has css key
     * @see register-settings.php
     */
    public function get_style_from_setting($page, $important = null) {

        $php_eol = '';

        $important = $important == 1 ? '!important' : '';

        $page = $this->get_admin_setting($page);

        foreach ( $page as $element ) :
            if ( isset($element["css"]) && is_array($element["css"]) ) :
                foreach ( $element["css"] as $css ) :
                    $css = explode("||", $css);
                    if ( trim($css[0]) != 'font' ) {
                        echo $css[1] . " { $css[0]: {$element['value']} $important; } " . $php_eol;
                    } else {
                        echo $css[1] . "{ font-family: {$element['value'][0]} $important; } " . $php_eol;
                        echo $css[1] . "{ font-weight: {$element['value'][1]} $important; } " . $php_eol;
                        echo $css[1] . "{ font-size: {$element['value'][2]} $important; } " . $php_eol;
                        if ( isset($element['value'][3]) && $element['value'][3] != '' ) {
                            echo $css[1] . "{ line-height: {$element['value'][3]} $important; } " . $php_eol;
                        }
                    }
                endforeach;
            endif;
        endforeach;
    }



    /**
     * Registers admin setting
     * @param type $setting
     * @return boolean
     */
    public function register_admin_setting($setting = false) {

        if ( !is_array($setting) ) {
            return false;
        }

        // page and name cannot be undefined or empty
        if ( !isset($setting["page"]) || !isset($setting["name"]) || $setting["page"] == "" || $setting["name"] == "" ) {
            return false;
        }

        // default key, vals
        $setting_default_values_template = array(
                "page"    => "",
                "title"   => "",
                "desc"    => "",
                "name"    => "",
                "type"    => "input",
                "select"  => array(),
                "value"   => "",
                "default" => "",
                "req"     => false,
                "css"     => false,
                "ai"      => $this->_ras_ai++
        );

        // fill empty keys if any
        foreach ( $setting_default_values_template as $k => $v ) {
            if ( !isset($setting[$k]) ) {
                $setting[$k] = $v;
            }
        }

        // check if setting value already exists
        $exists = $this->get_admin_setting_value($setting["page"], $setting["name"]);

        if ( $exists !== null ) {

            $setting["value"] = $exists;
        }

        $page_settings                   = get_option($setting["page"], array());
        $page_settings[$setting["name"]] = $setting;

        $page_settings = $this->reorder_admin_settings($page_settings);

        update_option($setting["page"], $page_settings);
        
        return $this;
    }



    public function reorder_admin_settings($page_settings) {

        uasort($page_settings, function($a, $b) {

            if ( isset($a['ai']) && isset($b['ai']) ) {
                return $a['ai'] > $b['ai'];
            }

            return 0;
        });

        return $page_settings;
    }



    public function delete_admin_settings($page, $name_array) {

        $settings = $this->get_admin_setting($page);

        foreach ( $settings as $s ) :

            $s = (object) $s;

            foreach ( $name_array as $name ) {

                if ( $s->name == $name ) {

                    unset($settings[$name]);
                }
            }

        endforeach;

        update_option($page, $settings);
    }



    public function load_front_css($handle, $url) {
        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("FRONT_CSS") . "/.");
        }
        wp_enqueue_style($handle, $url);
    }



    public function load_font($handle, $url) {
        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("FONTS") . "/.");
        }
        wp_enqueue_style($handle, $url);
    }



    public function load_front_js($handle, $url, $dep = false) {

        if ( strpos($url, '//') === false ) {
            $url = plugins_url($url, $this->get_path("FRONT_JS") . "/.");
        }

        if ( $dep ) {
            $dep = explode(',', $dep);
        }

        wp_enqueue_script($handle, $url, $dep);
    }

}
