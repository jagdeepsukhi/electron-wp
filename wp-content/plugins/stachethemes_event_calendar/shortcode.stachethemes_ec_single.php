<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
$yoast_active = is_plugin_active('wordpress-seo/wp-seo.php');

add_shortcode('stachethemes_ec_single', function($atts) {

    ob_start();

    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

    if ( isset($atts['id']) ) {

        // User provided attr id 

        $id = (int) $atts['id'];

        $event = stachethemes_event_calendar_query::get_event_data($id);
    } else {

        // Check alias; if no alias it will pull newest event 

        $event_alias = get_query_var('stec_event_alias', false);
        $event       = stachethemes_event_calendar_query::get_event_data_by_alias($event_alias);
    }

    if ( !$event ) {
        stachethemes_ec_single::show_404();
    }

    $event->calendar = stachethemes_event_calendar_query::get_calendar($event->general->calid);

    if ( isset($atts['id']) && isset($atts['offset']) ) {

        // User provided offset for the event

        $event
                ->general
                ->repeat_time_offset = (int) $atts['offset'];
    } else {

        // Check query offset

        $event
                ->general
                ->repeat_time_offset = get_query_var('stec_event_offset', 0);
    }

    $general = stachethemes_ec_admin::get_admin_setting('stec_menu__general');

    $general_settings_values = array();

    foreach ( $general as $g ) {

        // check if attribute is overrided from shortcode
        if ( isset($atts[$g["name"]]) ) {

            $general_settings_values[$g["name"]] = $atts[$g["name"]];

            // don't duplicate in global if setting is in general scope
            unset($atts[$g["name"]]);
        } else {

            $general_settings_values[$g["name"]] = $g["value"];
        }
    }

    $general_settings_values['userid']   = get_current_user_id();
    $general_settings_values['site_url'] = get_site_url();
    ?>
    <script type="text/javascript">
        var stecSingleOptions = <?php echo json_encode($general_settings_values); ?>;
        var stecSingleEvent = <?php echo json_encode($event); ?>;
    </script>
    <?php
    do_action('stachethemes_ec_single_before_html');

    if ( $stachethemes_ec_main->lcns() !== false ) {
        include ($stachethemes_ec_main->get_path('FRONT_VIEW') . 'single.php');
    } else {
        echo '<p>Stachethemes Event Calendar is not activated. To activate it go to <a href="' . get_admin_url(0, 'admin.php?page=stec_menu__license') . '">Dashboard -> Stachethemes Event Calendar -> Product License</a></p>';
    }

    // After html hook
    do_action('stachethemes_ec_single_after_html');

    return ob_get_clean();
});

add_action('wp_enqueue_scripts', function() use ($yoast_active) {

    global $post;

    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

    if ( $stachethemes_ec_main->scripts_are_forced() === true || (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'stachethemes_ec_single')) ) {

        // calendar default fonts
        $stachethemes_ec_main->load_font("stec-google-fonts", "//fonts.googleapis.com/css?family=Roboto:300,400,500,700");
        $stachethemes_ec_main->load_font("stec-font-awesome", "font-awesome-4.5.0/css/font-awesome.css");
        $stachethemes_ec_main->load_front_css("stec-forecast-css", "forecast/forecast.css");

        // main stylesheet
        $stachethemes_ec_main->load_front_css("stec-single", "style.single.css");
        $stachethemes_ec_main->load_front_css("stec-single-media-med-css", "style.single.media-med.css");
        $stachethemes_ec_main->load_front_css("stec-single-media-small-css", "style.single.media-small.css");

        // less compiler
//         $stachethemes_ec_main->load_front_js("stec-less-js", "libs/less.js");
        // js
        
        if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general_other', 'load_jmobile_js') == '1' ) {
            $stachethemes_ec_main->load_front_js("stec-jquery-mobile-js", "libs/jquery.mobile.js", "jquery");
        }

        if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general_other', 'load_gmaps') == '1' ) {
            $stachethemes_ec_main->load_front_js("stec-google-maps", "//maps.googleapis.com/maps/api/js?key=" . $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'gmaps_api_key'));
        }

        if ( $stachethemes_ec_main->get_admin_setting_value('stec_menu__general_other', 'load_chart_js') == '1' ) {
            $stachethemes_ec_main->load_front_js("stec-chart", "libs/chart-2.2.1.min.js", "jquery");
        }

        $stachethemes_ec_main->load_front_js("stec-single-js", "single/stec-single.js", "jquery");

        $stachethemes_ec_main->load_head();
        $stachethemes_ec_main->load_js_locales();

        $event_alias = get_query_var('stec_event_alias', false);

        if ( $event_alias ) {

            $event = stachethemes_event_calendar_query::get_event_data_by_alias($event_alias);

            if ( $event && !empty($event) ) {

                if ( $yoast_active ) {
                    add_action('wpseo_title', '__return_false');
                    add_action('wpseo_metadesc', '__return_false');
                    add_action('wpseo_opengraph_type', '__return_false');
                    add_action('wpseo_opengraph_image', '__return_false');
                    add_action('wpseo_canonical', '__return_false');
                }

                add_action('wp_head', function() use ($event) {

                    $image = null;

                    if ( isset($event->general->images_meta) ) :
                        $image = (object) array_shift($event->general->images_meta);
                    endif;
                    ?>
                    <meta property="og:title" content="<?php echo trim($event->general->summary); ?>" />
                    <meta property="og:description" content="<?php echo $event->general->description_short; ?>" />
                    <meta property="og:url" content="<?php echo stachethemes_ec_single::get_page_url(); ?>" />
                    <meta property="og:type" content="article" />
                    <meta property="og:site_name" content="<?php echo get_bloginfo('name'); ?>" />
                    <?php if ( $image ) : ?>
                        <meta property="og:image" content="<?php echo $image->src; ?>"/>
                    <?php endif; ?>

                    <meta name="twitter:card" content="summary" /> 
                    <meta name="twitter:title" content="<?php echo trim($event->general->summary); ?>" />
                    <meta name="twitter:description" content="<?php echo $event->general->description_short ? $event->general->description_short : $event->general->summary; ?>" />
                    <?php if ( $image ) : ?>
                        <meta name="twitter:image" content="<?php echo $image->src; ?>" />
                    <?php endif; ?>
                    <?php
                });
            }
        }
    }
});
