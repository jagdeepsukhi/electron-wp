<?php



function stec_integrateWithVC() {

    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

    vc_map(array(
            "name"        => "Event Calendar",
            'description' => $stachethemes_ec_main->lang("Event Calendar by Stachethemes"),
            "base"        => "stachethemes_ec",
            "class"       => "",
            "icon"        => plugins_url('admin/img/avatar.jpg', __FILE__),
            "category"    => "Stachethemes",
            "params"      => array(
                    array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "heading"     => $stachethemes_ec_main->lang("Calendar ID#"),
                            "param_name"  => "cal",
                            "value"       => "",
                            "description" => $stachethemes_ec_main->lang("Display only selected calendars (optional). Example: 1,2,3,4")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Default View"),
                            "param_name"  => "view",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Agenda") => 'agenda',
                                    $stachethemes_ec_main->lang("Month")  => 'month',
                                    $stachethemes_ec_main->lang("Week")   => 'week',
                                    $stachethemes_ec_main->lang("Day")    => 'day'
                            ),
                            "description" => $stachethemes_ec_main->lang("Default calendar view")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Top Menu"),
                            "param_name"  => "show_top",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("Show/Hide top menu")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Different Views Buttons"),
                            "param_name"  => "show_views",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("If set to hide users won't be able to switch calendar layouts")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Search Button"),
                            "param_name"  => "show_search",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("Show/Hide Search button")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Calendar Filter Button"),
                            "param_name"  => "show_calfilter",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("Show/Hide calendar filter button")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Agenda Calendar Display"),
                            "param_name"  => "agenda_cal_display",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("Show/Hide calendar slider from Agenda view")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang("Event Tooltip"),
                            "param_name"  => "tooltip_display",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang("Show/Hide Tooltip on event hover")
                    ),
                    array(
                            "type"        => "dropdown",
                            "heading"     => $stachethemes_ec_main->lang('Display "Create an event" form'),
                            "param_name"  => "show_create_event_form",
                            "admin_label" => true,
                            "value"       => array(
                                    $stachethemes_ec_main->lang("Default") => '',
                                    $stachethemes_ec_main->lang("Hide")    => 0,
                                    $stachethemes_ec_main->lang("Show")    => 1,
                            ),
                            "description" => $stachethemes_ec_main->lang('Show/Hide "Create an event" form')
                    ),
            )
    ));
    
    vc_map(array(
            "name"        => "Event Calendar - Create Event Form",
            'description' => $stachethemes_ec_main->lang("Create Event Form for Stachethemes Event Calendar"),
            "base"        => "stachethemes_ec_create_form",
            "class"       => "",
            "icon"        => plugins_url('admin/img/avatar.jpg', __FILE__),
            "category"    => "Stachethemes",
            "params"      => array(
                    array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "heading"     => $stachethemes_ec_main->lang("Button selector (.example-button or #example-button)"),
                            "param_name"  => "selector",
                            "value"       => "",
                            "description" => $stachethemes_ec_main->lang("Button class or id name. Transforms the form into popup")
                    ),
                    array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "heading"     => $stachethemes_ec_main->lang("Filter Calendars"),
                            "param_name"  => "create_form_cal",
                            "value"       => "",
                            "description" => $stachethemes_ec_main->lang("Include only following calendars by calendar id. Example 1,2,3,4")
                    ),
            )
    ));

}

add_action('vc_before_init', 'stec_integrateWithVC');
