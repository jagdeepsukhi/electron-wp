<?php

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

/**
 * Ver 1.2 uses new visibility schema
 */

$wpdb->query("ALTER TABLE {$wpdb->prefix}stec_calendars modify `visibility` varchar(255) NOT NULL");
$wpdb->query("ALTER TABLE {$wpdb->prefix}stec_events modify `visibility` varchar(255) NOT NULL");

$wpdb->query("UPDATE {$wpdb->prefix}stec_calendars SET visibility = 'stec_public' WHERE {$wpdb->prefix}stec_calendars.visibility = 1 ");
$wpdb->query("UPDATE {$wpdb->prefix}stec_calendars SET visibility = 'stec_private' WHERE {$wpdb->prefix}stec_calendars.visibility = 2 ");

$wpdb->query("UPDATE {$wpdb->prefix}stec_events SET visibility = 'stec_cal_default' WHERE {$wpdb->prefix}stec_events.visibility = 0 ");
$wpdb->query("UPDATE {$wpdb->prefix}stec_events SET visibility = 'stec_public' WHERE {$wpdb->prefix}stec_events.visibility = 1 ");
$wpdb->query("UPDATE {$wpdb->prefix}stec_events SET visibility = 'stec_private' WHERE {$wpdb->prefix}stec_events.visibility = 2 ");



