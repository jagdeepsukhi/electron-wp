<?php

$stachethemes_ec_main = stachethemes_ec_main::get_instance();
$stachethemes_ec_main->load_admin_classes(array(
    "calendar.php" // needed for validate_event_alias function
));

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

global $wpdb;

/**
 * Ver 1.5 uses new prop 'alias'
 */

$events = $wpdb->get_results("SELECT id,summary,alias FROM {$wpdb->prefix}stec_events", OBJECT);

if (is_array($events)) {
    
    foreach($events as $event) {
        
        if (trim($event->alias) == "") {
            
            $alias = sanitize_title($event->summary);
            $alias = stachethemes_ec_admin::validate_event_alias($alias, $event->id);
            
            $wpdb->update( 
                $wpdb->prefix.'stec_events', 
                array(
                    'alias' => $alias
                ), 
                array (
                    'id' => $event->id
                ),
                array( 
                    '%s', 
                ),
                array(
                    '%d'
                )
            ); 
        }
    }
}