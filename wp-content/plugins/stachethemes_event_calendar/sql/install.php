<?php
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE {$wpdb->prefix}stec_calendars (
    id int(11) NOT NULL AUTO_INCREMENT,
    created_by int(11) NOT NULL,
    title varchar(300) NOT NULL,
    color varchar(50) NOT NULL,
    icon varchar(100) NOT NULL,
    timezone varchar(300) NOT NULL,
    back_visibility varchar(255) NOT NULL DEFAULT 'stec_public',
    visibility varchar(255) NOT NULL,
    writable varchar(300) NOT NULL DEFAULT 0,
    req_approval tinyint(1) NOT NULL DEFAULT 1,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_events (
    id int(6) NOT NULL AUTO_INCREMENT,
    created_by int(11) NOT NULL,
    calid int(6) NOT NULL,
    color varchar(50) DEFAULT NULL,
    summary varchar(255) NOT NULL,
    alias varchar(255) NOT NULL,
    visibility varchar(255) NOT NULL,
    featured tinyint(1) NOT NULL,
    start_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    end_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    location varchar(255) DEFAULT NULL,
    location_details mediumtext NOT NULL,
    location_forecast varchar(300) NOT NULL,
    location_use_coord tinyint(1) NOT NULL DEFAULT 0,
    images mediumtext NOT NULL,
    description mediumtext NOT NULL,
    description_short mediumtext NOT NULL,
    icon varchar(100) NOT NULL,
    keywords mediumtext NOT NULL,
    all_day tinyint(1) NOT NULL,
    counter tinyint(1) NOT NULL,
    comments tinyint(1) NOT NULL,
    link varchar(300) NOT NULL,
    approved tinyint(1) NOT NULL DEFAULT 1,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_events_meta (
    id int(6) NOT NULL AUTO_INCREMENT,
    eventid int(6) NOT NULL,
    created_by_cookie varchar(32) NULL,
    contact_email varchar(300) NULL,
    review_note text NULL,
    recurrence_id varchar(300) NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    uid varchar(300) NULL,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_events_repeater (
    id int(6) NOT NULL AUTO_INCREMENT,
    eventid int(6) NOT NULL,
    rrule varchar(300) NULL,
    exdate varchar(300) NULL,
    is_advanced_rrule tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_attachments (
    id int(11) NOT NULL AUTO_INCREMENT,
    eventid int(11) NOT NULL,
    attachment int(11) NOT NULL,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_attendance (
    id int(6) NOT NULL AUTO_INCREMENT,
    eventid int(6) NOT NULL,
    repeat_offset int(11) NOT NULL,
    userid int(6) DEFAULT NULL,
    status tinyint(1) NOT NULL,
    email varchar(300) DEFAULT NULL,
    access_token varchar(32) DEFAULT NULL,
    mail_sent tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_guests (
    id int(6) NOT NULL AUTO_INCREMENT,
    eventid int(6) NOT NULL,
    name varchar(255) NOT NULL,
    photo varchar(255) DEFAULT NULL,
    about text NOT NULL,
    links text NOT NULL,
    PRIMARY KEY  (id)
) $charset_collate;";
dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_reminder (
    id int(11) NOT NULL AUTO_INCREMENT,
    repeat_offset int(11) NOT NULL,
    eventid int(11) NOT NULL,
    email varchar(300) NOT NULL,
    date datetime NOT NULL,
    PRIMARY KEY  (id)
) $charset_collate;";

dbDelta($sql);

$sql = "CREATE TABLE {$wpdb->prefix}stec_schedule (
    id int(6) NOT NULL AUTO_INCREMENT,
    eventid int(6) NOT NULL,
    start_date datetime NOT NULL,
    title varchar(300) NOT NULL,
    icon varchar(300) NOT NULL DEFAULT '',
    icon_color varchar(50) NOT NULL DEFAULT '#000000',
    description text NOT NULL,
    PRIMARY KEY  (id)) 
$charset_collate;";

dbDelta($sql);

$sql = "
    CREATE TABLE {$wpdb->prefix}stec_woocommerce (
     id int(6) NOT NULL AUTO_INCREMENT,
     eventid int(6) NOT NULL,
     product_id int(6) NOT NULL,
     PRIMARY KEY  (id)
) $charset_collate;";
    
dbDelta($sql);

$sql = "
    CREATE TABLE {$wpdb->prefix}stec_import (
     id int(6) NOT NULL AUTO_INCREMENT,
     calid int(6) NOT NULL,
     url varchar(300) NOT NULL,
     freq int(1) NOT NULL,
     icon varchar(100) NOT NULL,
     recurrent int(1) NOT NULL,
     ignore_expired int(1) NOT NULL DEFAULT 0, 
     overwrite_events int(1) NOT NULL DEFAULT 0, 
     delete_removed int(1) NOT NULL DEFAULT 0, 
     userid int(11) NOT NULL DEFAULT 1, 
     PRIMARY KEY  (id)
) $charset_collate;";

dbDelta($sql);

$current_db_version = get_option('stec-db-version', false);

if ($current_db_version) :
    
    if (version_compare($current_db_version, '1.2', '<')) {
        require(dirname(__FILE__) . '/1.2.php');
    }
    
    if (version_compare($current_db_version, '1.4', '<')) {
        require(dirname(__FILE__) . '/1.4.php');
    }
    
    if (version_compare($current_db_version, '1.5', '<')) {
        require(dirname(__FILE__) . '/1.5.php');
    }
    
    if (version_compare($current_db_version, '1.5.2', '<')) {
        require(dirname(__FILE__) . '/1.5.2.php');
    }
    
    if (version_compare($current_db_version, '1.6.4', '<')) {
        require(dirname(__FILE__) . '/1.6.4.php');
    }
    
endif;

update_option("stec-db-version", "1.7.1");