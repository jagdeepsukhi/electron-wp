<?php

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

/**
 * Ver 1.4 uses new table stec_events_meta
 * 
 * add meta to events without
 */

$events = $wpdb->get_results("SELECT id FROM {$wpdb->prefix}stec_events", OBJECT);

if (is_array($events)) {
    
    foreach($events as $event) {
        
        $wpdb->insert(
            "{$wpdb->prefix}stec_events_meta", 
            array(
                'eventid' => $event->id,
                'uid'     => uniqid('e-').'_'.md5(microtime()).'@stachethemes_ec.com'
            ), array(
                '%d',
                '%s'
            )
        );
    }
    
}