<?php

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

/**
 * Ver 1.5.2 uses new repeater 
 * Update stec_events repeat rules
 */

$events = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_events", OBJECT);

if (is_array($events)) {
    
    foreach($events as $event) {
        
        if (!isset($event->event_repeat) || $event->event_repeat == 0) {
            continue;
        }
        
        switch($event->event_repeat) :
            
            case '1' :
                
                $rrule = 'FREQ=DAILY';
                
            break;
            
            case '2' :
                
                $rrule = 'FREQ=WEEKLY';
                
            break;
            
            case '3' :
                
                $rrule = 'FREQ=MONTHLY';
                
            break;
            
            case '4' :
                
                $rrule = 'FREQ=YEARLY';
                
            break;
        
            default:
                continue; // unkmown repeat interval; skip
            
        endswitch;
        
        if (isset($event->repeat_gap) && $event->repeat_gap > 0) {
            $rrule .= ';INTERVAL=' . ($event->repeat_gap + 1);
        }
        
        if (isset($event->repeat_count) && $event->repeat_count > 0) {
            $rrule .= ';COUNT=' . $event->repeat_count;
        }
        
        $wpdb->query("UPDATE {$wpdb->prefix}stec_events SET rrule = '{$rrule}' WHERE {$wpdb->prefix}stec_events.id = {$event->id} ");
        
        
    }
    
}