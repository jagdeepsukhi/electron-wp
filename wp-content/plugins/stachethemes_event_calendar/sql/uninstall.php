<?php

global $wpdb;

$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_attachments");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_attendance");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_calendars");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_events");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_events_repeater");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_events_meta");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_guests");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_reminder");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_schedule");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_woocommerce");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}stec_import");

/**
 * @todo unify into single variable ?
 */
delete_option("stec-db-version");
delete_option("stec-activated");
delete_option("stec_menu__license");
delete_option("stec_menu__general");
delete_option("stec_menu__general_email_invitation");
delete_option("stec_menu__general_email_reminder");
delete_option("stec_menu__general_google_captcha");
delete_option("stec_menu__general_other");
delete_option("stec_menu__fontsandcolors_top");
delete_option("stec_menu__fontsandcolors_agenda");
delete_option("stec_menu__fontsandcolors_monthweek");
delete_option("stec_menu__fontsandcolors_day");
delete_option("stec_menu__fontsandcolors_event");
delete_option("stec_menu__fontsandcolors_preview");
delete_option("stec_menu__fontsandcolors_tooltip");
delete_option("stec_menu__fontsandcolors_custom");
delete_option("stec_menu__cache");