<?php

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
global $wpdb;

/**
 * Ver 1.6.4 external table for repeater ( stmp_events_repeater ) 
 * and includes 'exdate' field
 * 
 * rrule field removed from stmp_events
 */
$events = $wpdb->get_results("SELECT id,rrule FROM {$wpdb->prefix}stec_events", OBJECT);

if ( is_array($events) ) {

    $success = true;

    foreach ( $events as $event ) {
        
        $sql   = "INSERT INTO {$wpdb->prefix}stec_events_repeater (eventid,rrule,exdate) VALUES (%d, %s, '') ON DUPLICATE KEY UPDATE rrule = %s";
        $sql   = $wpdb->prepare($sql, $event->id, $event->rrule, $event->rrule);
        $query = $wpdb->query($sql);
        if ( $query === false ) {
            $success = false;
        }
    }

    if ( $success ) {
        $wpdb->query("ALTER TABLE {$wpdb->prefix}stec_events DROP COLUMN rrule");
    }
}