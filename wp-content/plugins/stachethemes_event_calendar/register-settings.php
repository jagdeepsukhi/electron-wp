<?php
$stachethemes_ec_main = stachethemes_ec_main::get_instance();

/**
 * LICENSE SETTINGS
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__license",
                "title"   => $stachethemes_ec_main->lang("Enter your purchase key below"),
                "desc"    => "",
                "name"    => "purchase_code",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => true
        )
);


/**
 * GENERAL SETTINGS
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("First day of the week"),
                "desc"    => "",
                "name"    => "first_day_of_the_week",
                "type"    => "select",
                "value"   => "mon",
                "default" => "mon",
                "select"  => array(
                        'mon' => $stachethemes_ec_main->lang('Monday'),
                        'sat' => $stachethemes_ec_main->lang('Saturday'),
                        'sun' => $stachethemes_ec_main->lang('Sunday')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Date Format"),
                "desc"    => "",
                "name"    => "date_format",
                "type"    => "select",
                "value"   => "mm-dd-yy",
                "default" => "mm-dd-yy",
                "select"  => array(
                        'yy-mm-dd' => $stachethemes_ec_main->lang('YY-MM-DD'),
                        'dd-mm-yy' => $stachethemes_ec_main->lang('DD-MM-YY'),
                        // 'dd.mm.yy' => $stachethemes_ec_main->lang('DD.MM.YY'),
                        'mm-dd-yy' => $stachethemes_ec_main->lang('MM-DD-YY')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Display Date 'GMT/UTC'"),
                "desc"    => "",
                "name"    => "date_label_gmtutc",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show'),
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Display date in user local time"),
                "desc"    => "",
                "name"    => "date_in_user_local_time",
                "type"    => "select",
                "value"   => "0",
                "default" => "0",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes'),
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Time Format"),
                "desc"    => "",
                "name"    => "time_format",
                "type"    => "select",
                "value"   => "24",
                "default" => "24",
                "select"  => array(
                        '12' => $stachethemes_ec_main->lang('12 Hours'),
                        '24' => $stachethemes_ec_main->lang('24 Hours')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Weather API Key"),
                "desc"    => "",
                "name"    => "weather_api_key",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => false
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Weather temperature units"),
                "desc"    => "",
                "name"    => "temp_units",
                "type"    => "select",
                "value"   => "C",
                "default" => "C",
                "select"  => array(
                        'C' => $stachethemes_ec_main->lang('Celsius'),
                        'F' => $stachethemes_ec_main->lang('Fahrenheit')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Weather wind units"),
                "desc"    => "",
                "name"    => "wind_units",
                "type"    => "select",
                "value"   => "KPH",
                "default" => "KPH",
                "select"  => array(
                        'MPH' => $stachethemes_ec_main->lang('MPH'),
                        'KPH' => $stachethemes_ec_main->lang('KPH')
                ),
                "req"     => true
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Google Maps API Key"),
                "desc"    => "",
                "name"    => "gmaps_api_key",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => false
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Disqus Shortname"),
                "desc"    => "",
                "name"    => "disqus_shortname",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => false
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Reminder feature"),
                "desc"    => "",
                "name"    => "reminder",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Disable'),
                        '1' => $stachethemes_ec_main->lang('Enable')
                ),
                "req"     => true
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Default View"),
                "desc"    => "",
                "name"    => "view",
                "type"    => "select",
                "value"   => "agenda",
                "default" => "agenda",
                "select"  => array(
                        'agenda' => $stachethemes_ec_main->lang('Agenda'),
                        'month'  => $stachethemes_ec_main->lang('Month'),
                        'week'   => $stachethemes_ec_main->lang('Week'),
                        'day'    => $stachethemes_ec_main->lang('Day')
                ),
                "req"     => true
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Top Menu"),
                "desc"    => "",
                "name"    => "show_top",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Different Views Buttons"),
                "desc"    => "",
                "name"    => "show_views",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Search Button"),
                "desc"    => "",
                "name"    => "show_search",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Calendar Filter Button"),
                "desc"    => "",
                "name"    => "show_calfilter",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Show event title on all cells"),
                "desc"    => "",
                "name"    => "show_event_title_all_cells",
                "type"    => "select",
                "value"   => "0",
                "default" => "0",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Auto-focus event"),
                "desc"    => "",
                "name"    => "event_auto_focus",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Auto-focus event offset (- / +) px"),
                "desc"    => "",
                "name"    => "event_auto_focus_offset",
                "type"    => "input",
                "value"   => "0",
                "default" => "0",
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Agenda events limit per click"),
                "desc"    => "",
                "name"    => "agenda_get_n",
                "type"    => "input",
                "value"   => "3",
                "default" => "3",
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Agenda Calendar Display"),
                "desc"    => "",
                "name"    => "agenda_cal_display",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Agenda List Display'),
                "desc"    => "",
                "name"    => "agenda_list_display",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Agenda List Order'),
                "desc"    => "",
                "name"    => "reverse_agenda_list",
                "type"    => "select",
                "value"   => "0",
                "default" => "0",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Ascending'),
                        '1' => $stachethemes_ec_main->lang('Descending')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang("Event Tooltip"),
                "desc"    => "",
                "name"    => "tooltip_display",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Disable'),
                        '1' => $stachethemes_ec_main->lang('Enable')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Display "Create an event" form'),
                "desc"    => "",
                "name"    => "show_create_event_form",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Allow creating expired events (front-end) '),
                "desc"    => "",
                "name"    => "create_event_form_allow_expired",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Allow export event from front-end'),
                "desc"    => "",
                "name"    => "show_export_buttons",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Social Share Links'),
                "desc"    => "",
                "name"    => "social_links",
                "type"    => "select",
                "value"   => "1",
                "default" => "1",
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('Hide'),
                        '1' => $stachethemes_ec_main->lang('Show')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general",
                "title"   => $stachethemes_ec_main->lang('Open events in'),
                "desc"    => "",
                "name"    => "open_event_in",
                "type"    => "select",
                "value"   => "self",
                "default" => "self",
                "select"  => array(
                        'self'   => $stachethemes_ec_main->lang('Calendar'),
                        'single' => $stachethemes_ec_main->lang('Single Page')
                ),
                "req"     => true
        )
);


/**
 * GENERAL - EMAIL NOTIFICATIONS
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_email_invitation",
                "title"   => $stachethemes_ec_main->lang("Subject"),
                "desc"    => "",
                "name"    => "email_invite_subject",
                "type"    => "input",
                "value"   => "You are invited to stec_replace_event_summary",
                "default" => "You are invited to stec_replace_event_summary",
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_email_invitation",
                "title"   => $stachethemes_ec_main->lang("Content"),
                "name"    => "email_invite_content",
                "type"    => "textarea",
                "value"   => "You are invited to stec_replace_event_summary \n\nTo accept invitation go to stec_replace_accept_url \nTo decline invitation go to stec_replace_decline_url",
                "default" => "You are invited to stec_replace_event_summary \nTo accept invitation go to stec_replace_accept_url \nTo decline invitation go to stec_replace_decline_url",
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_email_reminder",
                "title"   => $stachethemes_ec_main->lang("Subject"),
                "desc"    => "",
                "name"    => "email_reminder_subject",
                "type"    => "input",
                "value"   => "Event Reminder",
                "default" => "Event Reminder",
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_email_reminder",
                "title"   => $stachethemes_ec_main->lang("Content"),
                "name"    => "email_reminder_content",
                "type"    => "textarea",
                "value"   => "You have requested remind for stec_replace_event_summary.\n\nEvent starts on stec_replace_event_start.\nFor more info visit stec_replace_siteurl.",
                "default" => "You have requested remind for stec_replace_event_summary.\n\nEvent starts on stec_replace_event_start.\nFor more info visit stec_replace_siteurl.",
                "req"     => true
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_google_captcha",
                "title"   => $stachethemes_ec_main->lang("Use Captcha"),
                "name"    => "enabled",
                "type"    => "select",
                "value"   => '0',
                "default" => '0',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_google_captcha",
                "title"   => $stachethemes_ec_main->lang("Site Key"),
                "name"    => "site_key",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => false
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_google_captcha",
                "title"   => $stachethemes_ec_main->lang("Secret Key"),
                "name"    => "secret_key",
                "type"    => "input",
                "value"   => "",
                "default" => "",
                "req"     => false
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Load google maps script."),
                "name"    => "load_gmaps",
                "type"    => "select",
                "value"   => '1',
                "default" => '1',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Load jQuery UI style"),
                "name"    => "load_jquery_ui_css",
                "type"    => "select",
                "value"   => '1',
                "default" => '1',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Load Chart.js"),
                "name"    => "load_chart_js",
                "type"    => "select",
                "value"   => '1',
                "default" => '1',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Load Moment.js"),
                "name"    => "load_moment_js",
                "type"    => "select",
                "value"   => '1',
                "default" => '1',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Load jQuery Mobile"),
                "name"    => "load_jmobile_js",
                "type"    => "select",
                "value"   => '1',
                "default" => '1',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Force load calendar scripts"),
                "name"    => "force_load_scripts",
                "type"    => "select",
                "value"   => '0',
                "default" => '0',
                "select"  => array(
                        '0' => $stachethemes_ec_main->lang('No'),
                        '1' => $stachethemes_ec_main->lang('Yes')
                ),
                "req"     => true
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__general_other",
                "title"   => $stachethemes_ec_main->lang("Admin access level (admin user capability)"),
                "name"    => "admin_access_level",
                "type"    => "input",
                "value"   => 'edit_posts',
                "default" => 'edit_posts',
                "req"     => true
        )
);


/**
 *  Fonts and Colors TOP
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Button text color & text hover color"),
                "desc"    => "",
                "name"    => "top_btn_text_color",
                "type"    => "color",
                "value"   => "#bdc1c8",
                "default" => "#bdc1c8",
                "css"     => array(
                        'color || body .stec-top .stec-top-dropmenu-layouts > li i',
                        'color || body .stec-top .stec-top-menu > li',
                        'color || body .stec-top .stec-top-menu li[data-action="today"]:hover .stec-top-menu-count'
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "name"    => "top_btn_text_color_hover",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        'color || body .stec-top .stec-top-menu > li:hover i',
                        'color || body .stec-top .stec-top-menu > li.active i',
                        'color || body .stec-top .stec-top-menu > li:hover p',
                        'color || body .stec-top .stec-top-menu > li.active p',
                        'color || body .stec-top .stec-top-menu .stec-top-menu-count',
                        'color || body .stec-top .stec-top-dropmenu-layouts > li:hover i'
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Button text background color & background hover color"),
                "desc"    => "",
                "name"    => "top_btn_bg",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        'background || body .stec-top .stec-top-dropmenu-layouts > li',
                        'background || body .stec-top .stec-top-menu > li',
                        'background || body .stec-top .stec-top-menu li[data-action="today"]:hover .stec-top-menu-count',
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => "",
                "desc"    => "",
                "name"    => "top_btn_bg_hover",
                "type"    => "color",
                "value"   => "#f15e6e",
                "default" => "#f15e6e",
                "css"     => array(
                        'background || body .stec-top .stec-top-menu > li:hover',
                        'background || body .stec-top .stec-top-menu > li.active',
                        'background || body .stec-top .stec-top-menu .stec-top-menu-count',
                        'background || body .stec-top .stec-top-dropmenu-layouts > li:hover',
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Button text background color & background hover color"),
                "desc"    => "",
                "name"    => "top_dropdown_text_color",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        'color || body .stec-top .stec-top-dropmenu-layouts > li:hover > ul li p',
                        'color || body .stec-top .stec-top-menu-date-dropdown:hover .stec-top-menu-date-control-up i',
                        'color || body .stec-top .stec-top-menu-date-dropdown:hover .stec-top-menu-date-control-down i',
                        'color || body .stec-top .stec-top-menu-date-dropdown:hover li p',
                        'color || body .stec-top .stec-top-menu-date .mobile-hover .stec-top-menu-date-control-up i',
                        'color || body .stec-top .stec-top-menu-date .mobile-hover .stec-top-menu-date-control-down i',
                        'color || body .stec-top .stec-top-menu-date .mobile-hover li p'
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => "",
                "desc"    => "",
                "name"    => "top_dropdown_text_color_hover",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        'color || body .stec-top .stec-top-dropmenu-layouts > li:hover > ul li:hover',
                        'color || body .stec-top .stec-top-menu-date-dropdown:hover .stec-top-menu-date-control-up:hover i',
                        'color || body .stec-top .stec-top-menu-date-dropdown:hover .stec-top-menu-date-control-down:hover i',
                        'color || body .stec-top .stec-top-menu-date ul li:hover p',
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-results li.active i',
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-results li:hover i',
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-results li.active p',
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-results li:hover p',
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Dropdown menu background color & background hover color"),
                "desc"    => "",
                "name"    => "top_dropdown_bg",
                "type"    => "color",
                "value"   => "#f15e6e",
                "default" => "#f15e6e",
                "css"     => array(
                        'background || body .stec-top .stec-top-dropmenu-layouts > li:hover > ul li',
                        'background || body .stec-top .stec-top-menu-date-control-up',
                        'background || body .stec-top .stec-top-menu-date-control-down',
                        'background || body .stec-top .stec-top-menu-date ul li',
                        'background || body .stec-top .stec-top-menu-search .stec-top-search-dropdown',
                        'background || body .stec-top .stec-top-menu-filter-calendar-dropdown'
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => "",
                "desc"    => "",
                "name"    => "top_dropdown_bg_hover",
                "type"    => "color",
                "value"   => "#e04d5d",
                "default" => "#e04d5d",
                "css"     => array(
                        'background || body .stec-top .stec-top-dropmenu-layouts > li:hover > ul li.active',
                        'background || body .stec-top .stec-top-dropmenu-layouts > li:hover > ul li:hover',
                        'background || body .stec-top .stec-top-menu-date-control-up:hover',
                        'background || body .stec-top .stec-top-menu-date-control-down:hover',
                        'background || body .stec-top .stec-top-menu-date ul li.active',
                        'background || body .stec-top .stec-top-menu-date ul li:hover',
                        'background || body .stec-top .stec-top-menu-search .stec-top-search-results li.active',
                        'background || body .stec-top .stec-top-menu-search .stec-top-search-results li:hover'
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Search form background and text color"),
                "desc"    => "",
                "name"    => "top_search_bg",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        'background || body .stec-top .stec-top-menu-search .stec-top-search-form'
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => "",
                "desc"    => "",
                "name"    => "top_search_text_color",
                "type"    => "color",
                "value"   => "#bdc1c8",
                "default" => "#bdc1c8",
                "css"     => array(
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-form input',
                        'color || body .stec-top .stec-top-menu-search .stec-top-search-form a i'
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Top navigation Font, Weight and Size"),
                "desc"    => "",
                "name"    => "top",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        'font || body .stec-top-menu-date-small',
                        'font || body .stec-top .stec-top-menu > li p',
                        'font || body .stec-top .stec-top-dropmenu-layouts ul p',
                        'font || body .stec-top .stec-top-menu-search .stec-top-search-form input'
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_top",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "top_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);


/**
 *  Fonts and Colors AGENDA
 */
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "title"   => $stachethemes_ec_main->lang("Agenda month start cell background, month text color, year text color"),
        "desc"    => "",
        "name"    => "layouts_agenda_monthwrap_bg",
        "type"    => "color",
        "value"   => "#e6e8ed",
        "default" => "#e6e8ed",
        "css"     => array(
                "background || body .stec-layout-agenda-monthstart"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_monthwrap_month_text_color",
        "type"    => "color",
        "value"   => "#0c0c0c",
        "default" => "#0c0c0c",
        "css"     => array(
                "color || body .stec-layout-agenda-monthstart-month"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_monthwrap_year_text_color",
        "type"    => "color",
        "value"   => "#999999",
        "default" => "#999999",
        "css"     => array(
                "color || body .stec-layout-agenda-monthstart-year"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "title"   => $stachethemes_ec_main->lang("Agenda cell background, hover color and active color"),
        "desc"    => "",
        "name"    => "layouts_agenda_bg",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-layout-agenda-daycell"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_bg_hover",
        "type"    => "color",
        "value"   => "#f0f1f2",
        "default" => "#f0f1f2",
        "css"     => array(
                "background || body .stec-layout-agenda-daycell:hover"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_bg_active",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "background || body .stec-layout-agenda-daycell.active"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "title"   => $stachethemes_ec_main->lang("Agenda day label text color, date color, text active color, text today color"),
        "desc"    => "",
        "name"    => "layouts_agenda_daylabel_text_color",
        "type"    => "color",
        "value"   => "#999999",
        "default" => "#999999",
        "css"     => array(
                "color || body .stec-layout-agenda-daycell-label"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_date_text_color",
        "type"    => "color",
        "value"   => "#0c0c0c",
        "default" => "#0c0c0c",
        "css"     => array(
                "color || body .stec-layout-agenda-daycell-num "
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_date_text_color_active",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-agenda-daycell.active .stec-layout-agenda-daycell-label",
                "color || body .stec-layout-agenda-daycell.active .stec-layout-agenda-daycell-num",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_date_text_color_today",
        "type"    => "color",
        "value"   => "#e25261",
        "default" => "#e25261",
        "css"     => array(
                "color || body .stec-layout-agenda-daycell.stec-layout-agenda-daycell-today .stec-layout-agenda-daycell-label",
                "color || body .stec-layout-agenda-daycell.stec-layout-agenda-daycell-today .stec-layout-agenda-daycell-num",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "title"   => $stachethemes_ec_main->lang("Agenda list date title color"),
        "desc"    => "",
        "name"    => "layouts_agenda_list_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-agenda-events-all-datetext"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "title"   => $stachethemes_ec_main->lang('"Look for more" button background color, text color, hover background color, text hover color'),
        "desc"    => "",
        "name"    => "layouts_agenda_list_btn_bg",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "background || body .stec-layout-agenda-events-all-load-more"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_list_btn_text_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-agenda-events-all-load-more p"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_list_btn_bg_hover",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-layout-agenda-events-all-load-more:hover"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_agenda",
        "name"    => "layouts_agenda_list_btn_text_color_hover",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-agenda-events-all-load-more:hover p"
        )
));

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('"Look for more" button Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_list_btn",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-events-all-load-more"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('Agenda month start cell "Year" Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_monthwrap_year",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "12px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "12px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-monthstart-year"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('Agenda month start cell "Month" Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_monthwrap_month",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "18px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "18px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-monthstart-month"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('Daylabel Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_daylabel",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-daycell-label"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('Date Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_date",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "30px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "30px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-daycell-num"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang('Agenda list date title Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_agenda_list",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "700",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "700",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-agenda-events-all-datetext"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_agenda",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "agenda_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);


/**
 *  Fonts and Colors MONTH AND WEEK
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Day labels background, text color, today text color"),
                "desc"    => "",
                "name"    => "layouts_daylabel_bg",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "background || body .stec-layout-month-daylabel td",
                        "background || body .stec-layout-week-daylabel td",
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_daylabel_text_color",
                "type"    => "color",
                "value"   => "#bdc1c8",
                "default" => "#bdc1c8",
                "css"     => array(
                        "color || body .stec-layout-month-daylabel p",
                        "color || body .stec-layout-week-daylabel p",
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_daylabel_text_today_color",
                "type"    => "color",
                "value"   => "#f6bf64",
                "default" => "#f6bf64",
                "css"     => array(
                        "color || body .stec-layout-month-daylabel .stec-layout-month-daylabel-today p",
                        "color || body .stec-layout-week-daylabel .stec-layout-week-daylabel-today p"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Grid cells background color, hover color and active color"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_bg",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "background || body .stec-layout-month-daycell .stec-layout-month-daycell-wrap",
                        "background || body .stec-layout-week-daycell .stec-layout-week-daycell-wrap",
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_bg_hover",
                "type"    => "color",
                "value"   => "#f0f1f2",
                "default" => "#f0f1f2",
                "css"     => array(
                        "background || body .stec-layout-month-daycell:hover .stec-layout-month-daycell-wrap",
                        "background || body .stec-layout-week-daycell:hover .stec-layout-week-daycell-wrap",
                        "background || body .stec-layout-week-daycell.stec-layout-week-daycell-inactive:hover .stec-layout-week-daycell-wrap",
                        "background || body .stec-layout-month-daycell.stec-layout-month-daycell-inactive:hover .stec-layout-month-daycell-wrap"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_bg_active",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "background || body .stec-layout-month-daycell.active .stec-layout-month-daycell-wrap",
                        "background || body .stec-layout-week-daycell.active .stec-layout-week-daycell-wrap"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Grid date text color and active color"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_text_color",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "color || body .stec-layout-month-daycell:not(.stec-layout-month-daycell-today) .stec-layout-month-daycell-wrap .stec-layout-month-daycell-num",
                        "color || body .stec-layout-week-daycell:not(.stec-layout-week-daycell-today) .stec-layout-week-daycell-wrap .stec-layout-week-daycell-num",
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_text_color_active",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "background || body .stec-layout-week-daycell.active .stec-layout-week-daycell-eventmore-count-dot",
                        "background || body .stec-layout-month-daycell.active .stec-layout-month-daycell-eventmore-count-dot",
                        "color || body .stec-layout-month-daycell.active:not(.stec-layout-month-daycell-today) .stec-layout-month-daycell-wrap .stec-layout-month-daycell-num",
                        "color || body .stec-layout-week-daycell.active:not(.stec-layout-week-daycell-today) .stec-layout-week-daycell-wrap .stec-layout-week-daycell-num",
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Today date background and text color"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_today_bg",
                "type"    => "color",
                "value"   => "#f15e6e",
                "default" => "#f15e6e",
                "css"     => array(
                        "background || body .stec-layout-month-daycell.stec-layout-month-daycell-today .stec-layout-month-daycell-num::before",
                        "background || body .stec-layout-week-daycell.stec-layout-week-daycell-today .stec-layout-week-daycell-num::before"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_today_text_color",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "color || body .stec-layout-month-daycell-today .stec-layout-month-daycell-wrap .stec-layout-month-daycell-num",
                        "color || body .stec-layout-week-daycell-today .stec-layout-week-daycell-wrap .stec-layout-week-daycell-num"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Inactive cell background and date text color"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_inactive_bg",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "background || body .stec-layout-month-daycell.stec-layout-month-daycell-inactive .stec-layout-month-daycell-wrap",
                        "background || body .stec-layout-week-daycell.stec-layout-week-daycell-inactive .stec-layout-week-daycell-wrap"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_inactive_text_color",
                "type"    => "color",
                "value"   => "#a2a8b3",
                "default" => "#a2a8b3",
                "css"     => array(
                        "color || body .stec-layout-month-daycell:not(.stec-layout-month-daycell-today).stec-layout-month-daycell-inactive .stec-layout-month-daycell-wrap .stec-layout-month-daycell-num",
                        "color || body .stec-layout-week-daycell:not(.stec-layout-week-daycell-today).stec-layout-week-daycell-inactive .stec-layout-week-daycell-wrap .stec-layout-week-daycell-num",
                )
        )
);


$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Event name text color dark, text color bright"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_event_text_color_dark",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "background || body .stec-layout-month-daycell-eventmore-count-dot",
                        "background || body .stec-layout-week-daycell-eventmore-count-dot",
                        "color || body .stec-layout-month-daycell-eventmore-count",
                        "color || body .stec-layout-week-daycell-eventmore-count",
                        "color || body .stec-layout-month-daycell-events .stec-layout-month-daycell-event.stec-layout-month-daycell-event-bright .stec-layout-month-daycell-event-name",
                        "color || body .stec-layout-week-daycell-events .stec-layout-week-daycell-event.stec-layout-week-daycell-event-bright .stec-layout-week-daycell-event-name"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "name"    => "layouts_grid_cell_event_text_color_bright",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "color || body .stec-layout-month-daycell-events .stec-layout-month-daycell-event .stec-layout-month-daycell-event-name",
                        "color || body .stec-layout-week-daycell-events .stec-layout-week-daycell-event .stec-layout-week-daycell-event-name"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang('Day label Font, Weight and Size'),
                "desc"    => "",
                "name"    => "layouts_daylabel",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-month-daylabel p",
                        "font || body .stec-layout-week-daylabel p",
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Grid cell date Font, Weight and Size"),
                "desc"    => "",
                "name"    => "layouts_grid_cell",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "700",
                        "16px"
                ),
                "default" => array(
                        "Roboto",
                        "700",
                        "16px"
                ),
                "css"     => array(
                        "font || body .stec-layout-month-daycell .stec-layout-month-daycell-wrap .stec-layout-month-daycell-num",
                        "font || body .stec-layout-week-daycell .stec-layout-week-daycell-wrap .stec-layout-week-daycell-num"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Event name text Font, Weight and Size"),
                "desc"    => "",
                "name"    => "layouts_grid_cell_event",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "10px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "10px"
                ),
                "css"     => array(
                        "font || body .stec-layout-week-daycell-eventmore-count",
                        "font || body .stec-layout-month-daycell-eventmore-count",
                        "font || body .stec-layout-month-daycell-events .stec-layout-month-daycell-event .stec-layout-month-daycell-event-name",
                        "font || body .stec-layout-week-daycell-events .stec-layout-week-daycell-event .stec-layout-week-daycell-event-name",
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_monthweek",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "monthweek_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);


/**
 *  Fonts and Colors DAY
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_day",
                "title"   => $stachethemes_ec_main->lang("Misc text color"),
                "desc"    => "",
                "name"    => "layouts_day_misc_text_color",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "color || body .stec-layout-day-noevents"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_day",
                "title"   => $stachethemes_ec_main->lang("Misc text Font, Weight and Size"),
                "desc"    => "",
                "name"    => "layouts_day_misc",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "700",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "700",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-day-noevents"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_day",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "day_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);

/**
 *  Fonts and Colors EVENT PREVIEW
 */
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Background color, Background hover color"),
                "desc"    => "",
                "name"    => "preview_bg",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "background || body .stec-layout-agenda-eventholder-form .stec-layout-event-preview.stec-layout-event-preview-animate-complete",
                        "background || body .stec-event-holder .stec-layout-event-preview.stec-layout-event-preview-animate-complete",
                        "background || body .stec-layout-agenda-eventholder-form .stec-layout-event-preview.stec-layout-event-preview-animate",
                        "background || body .stec-event-holder .stec-layout-event-preview.stec-layout-event-preview-animate",
                        "background || body .stec-layout-agenda-events-all-list .stec-layout-event-preview.stec-layout-event-preview-animate",
                        "background || body .stec-layout-agenda-events-all-list .stec-layout-event-preview.stec-layout-event-preview-animate-complete"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "name"    => "preview_bg_hover",
                "type"    => "color",
                "value"   => "#f0f1f2",
                "default" => "#f0f1f2",
                "css"     => array(
                        "background || body .stec-layout-event-preview:hover",
                        "background || body .stec-event-holder .stec-layout-event-preview.stec-layout-event-preview-animate-complete:hover",
                        "background || body .stec-layout-agenda-eventholder-form .stec-layout-event-preview.stec-layout-event-preview-animate-complete:hover",
                        "background || body .stec-layout-agenda-events-all-list .stec-layout-event-preview.stec-layout-event-preview-animate-complete:hover",
                        "background || body .stec-layout-agenda-events-all-list .stec-layout-event-preview.stec-layout-event-preview-animate-complete:hover",
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Title color, Date color"),
                "desc"    => "",
                "name"    => "preview_title_text_color",
                "type"    => "color",
                "value"   => "#4d576c",
                "default" => "#4d576c",
                "css"     => array(
                        "color || body .stec-layout-event-preview-left-text-title",
                        "color || body .stec-layout-single-preview-left-text-title"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "name"    => "preview_date_text_color",
                "type"    => "color",
                "value"   => "#bdc1c8",
                "default" => "#bdc1c8",
                "css"     => array(
                        "color || body .stec-layout-event-preview-left-text-date",
                        "color || body .stec-layout-event-preview-left-text-sub",
                        "color || body .stec-layout-single-preview-left-text-date"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Reminder button icon/text color and icon/text hover color"),
                "desc"    => "",
                "name"    => "preview_reminder_btn_text_color",
                "type"    => "color",
                "value"   => "#bdc1c8",
                "default" => "#bdc1c8",
                "css"     => array(
                        "color || body .stec-layout-event-preview-right-menu",
                        "color || body .stec-layout-event-preview-left-reminder-toggle:not(.stec-layout-event-preview-left-reminder-success)",
                        "border-color || body .stec-layout-event-preview-left-reminder-toggle:not(.stec-layout-event-preview-left-reminder-success)",
                        "color || body .stec-layout-single-preview-left-reminder-toggle:not(.stec-layout-single-preview-left-reminder-success)",
                        "border-color || body .stec-layout-single-preview-left-reminder-toggle:not(.stec-layout-single-preview-left-reminder-success)",
                        "color || body .stec-layout-event-preview-left-approval-cancel",
                        "border-color || body .stec-layout-event-preview-left-approval-cancel"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "name"    => "preview_reminder_btn_text_color_hover",
                "type"    => "color",
                "value"   => "#343d46",
                "default" => "#343d46",
                "css"     => array(
                        "color || body .stec-layout-event-preview-right-menu:hover",
                        "color || body .stec-layout-event-preview-right-menu.active",
                        "color || body .stec-layout-event-preview-left-reminder-toggle.active:not(.stec-layout-event-preview-left-reminder-success)",
                        "color || body .stec-layout-single-preview-left-reminder-toggle.active:not(.stec-layout-single-preview-left-reminder-success)"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "title"   => $stachethemes_ec_main->lang("Expand button icon color and icon hover color"),
        "desc"    => "",
        "name"    => "preview_expand_btn_text_color",
        "type"    => "color",
        "value"   => "#bdc1c8",
        "default" => "#bdc1c8",
        "css"     => array(
                "color || body .stec-layout-event-preview-right-event-toggle",
                "color || body .stec-layout-event-inner-intro-exports-toggle",
                "color || body .stec-layout-event-inner-intro-attachments-toggle",
                "color || body .stec-layout-single-attachments-toggle",
                "color || body .stec-layout-event-inner-schedule-tab-toggle",
                "color || body .stec-layout-single-schedule-tab-toggle",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_expand_btn_text_color_hover",
        "type"    => "color",
        "value"   => "#343d46",
        "default" => "#343d46",
        "css"     => array(
                "color || body .stec-layout-event-inner-intro-exports-toggle:hover",
                "color || body .stec-layout-event-inner-intro-exports-toggle.active",
                "color || body .stec-layout-event-inner-intro-attachments-toggle:hover",
                "color || body .stec-layout-event-inner-intro-attachments-toggle.active",
                "color || body .stec-layout-single-attachments-toggle:hover",
                "color || body .stec-layout-single-attachments-toggle.active",
                "color || body .stec-layout-event-preview-right-event-toggle.active",
                "color || body .stec-layout-event-preview-right-event-toggle:hover",
                "color || body .stec-layout-event-inner-schedule-tab-toggle:hover",
                "color || body .stec-layout-event-inner-schedule-tab.open .stec-layout-event-inner-schedule-tab-toggle",
                "color || body .stec-layout-single-schedule-tab-toggle:hover",
                "color || body .stec-layout-single-schedule-tab.open .stec-layout-single-schedule-tab-toggle"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "title"   => $stachethemes_ec_main->lang("Reminder menu background, input background, input text color"),
        "desc"    => "",
        "name"    => "preview_reminder_bg",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder::before",
                "background || body .stec-layout-event-preview-reminder",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_reminder_input_bg",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder input",
                "background || body .stec-layout-event-preview-reminder-units-selector p",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_reminder_input_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-preview-reminder input",
                "color || body .stec-layout-event-preview-reminder-units-selector p ",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "title"   => $stachethemes_ec_main->lang("Remind me button background, text color, background hover color and text hover color"),
        "desc"    => "",
        "name"    => "preview_remind_me_btn_bg",
        "type"    => "color",
        "value"   => "#fcb755",
        "default" => "#fcb755",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder button",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_remind_me_btn_text_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-preview-reminder button",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_remind_me_btn_bg_hover",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder button:hover",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_remind_me_btn_text_color_hover",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-preview-reminder button:hover",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "title"   => $stachethemes_ec_main->lang("Reminder dropdown background color, text color, background hover color, text hover color"),
        "desc"    => "",
        "name"    => "preview_reminder_dropdown_bg",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder-units-selector li",
                "background || body .stec-layout-single-preview-reminder-units-selector li",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_reminder_dropdown_text_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-preview-reminder-units-selector li",
                "color || body .stec-layout-single-preview-reminder-units-selector li",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_reminder_dropdown_bg_hover",
        "type"    => "color",
        "value"   => "#e25261",
        "default" => "#e25261",
        "css"     => array(
                "background || body .stec-layout-event-preview-reminder-units-selector:hover p",
                "background || body .stec-layout-event-preview-reminder-units-selector li:hover",
                "background || body .stec-layout-single-preview-reminder-units-selector:hover p",
                "background || body .stec-layout-single-preview-reminder-units-selector li:hover",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_preview",
        "name"    => "preview_reminder_dropdown_text_color_hover",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-preview-reminder-units-selector:hover p",
                "color || body .stec-layout-event-preview-reminder-units-selector li:hover",
                "color || body .stec-layout-single-preview-reminder-units-selector:hover p",
                "color || body .stec-layout-single-preview-reminder-units-selector li:hover",
        )
));

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Featured tag background color, text color"),
                "desc"    => "",
                "name"    => "featured_bg",
                "type"    => "color",
                "value"   => "#f15e6e",
                "default" => "#f15e6e",
                "css"     => array(
                        "background || body .stec-layout-event-preview-left-text-featured span",
                        "color || body .stec-layout-event-preview-left-text-featured i",
                        "background || body .stec-layout-single-preview-left-text-featured span",
                        "color || body .stec-layout-single-preview-left-text-featured i"
                )
        )
);
$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "name"    => "featured_text_color",
                "type"    => "color",
                "value"   => "#ffffff",
                "default" => "#ffffff",
                "css"     => array(
                        "color || body .stec-layout-event-preview-left-text-featured span",
                        "color || body .stec-layout-single-preview-left-text-featured span"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Title Font, Weight, Size and Line-Height"),
                "desc"    => "",
                "name"    => "preview_title",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "18px",
                        "1.2"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "18px",
                        "1.2"
                ),
                "css"     => array(
                        "font || body .stec-layout-event-preview-left-text-title",
                        "font || body .stec-layout-single-preview-left-text-title"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Date Font, Weight and Size"),
                "desc"    => "",
                "name"    => "preview_date",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-event-preview-left-text-date",
                        "font || body .stec-layout-single-preview-left-text-date",
                        "font || body .stec-layout-event-preview-left-text-sub"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Reminder Font, Weight and Size"),
                "desc"    => "",
                "name"    => "preview_reminder",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "14px"
                ),
                "css"     => array(
                        "font || body .stec-layout-event-preview-reminder input",
                        "font || body .stec-layout-event-preview-reminder button",
                        "font || body .stec-layout-event-preview-reminder-units-selector p",
                        "font || body .stec-layout-event-preview-reminder-units-selector li",
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Reminder mobile button Font, Weight and Size"),
                "desc"    => "",
                "name"    => "preview_reminder_small",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "11px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "11px"
                ),
                "css"     => array(
                        "font || body .stec-layout-single-preview-left-reminder-toggle",
                        "font || body .stec-layout-event-preview-left-reminder-toggle",
                        "font || body .stec-layout-event-preview-left-approval-cancel"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Featured tag Font, Weight and Size"),
                "desc"    => "",
                "name"    => "featured_font",
                "type"    => "font",
                "value"   => array(
                        "Roboto",
                        "400",
                        "10px"
                ),
                "default" => array(
                        "Roboto",
                        "400",
                        "10px"
                ),
                "css"     => array(
                        "font || body .stec-layout-event-preview-left-text-featured span",
                        "font || body .stec-layout-single-preview-left-text-featured span"
                )
        )
);

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_preview",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "preview_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);

/**
 *  Fonts and Colors EVENT INNER
 */
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Tab background color and text color, background active color, text active color"),
        "desc"    => "",
        "name"    => "event_tab_bg",
        "type"    => "color",
        "value"   => "#f8f9fa",
        "default" => "#f8f9fa",
        "css"     => array(
                "background || body .stec-layout-event-inner-top-tabs"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_tab_text_color",
        "type"    => "color",
        "value"   => "#bdc1c8",
        "default" => "#bdc1c8",
        "css"     => array(
                "color || body .stec-layout-event-inner-top-tabs p",
                "color || body .stec-layout-event-inner-top-tabs i"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_tab_bg_active",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-layout-event-inner-top-tabs li.active"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_tab_text_color_active",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-inner-top-tabs li.active p",
                "color || body .stec-layout-event-inner-top-tabs li.active i"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Wrapper Background Color"),
        "desc"    => "",
        "name"    => "event_bg",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-layout-event-inner"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Buttons background color, buttons text color, background hover color, text hover color"),
        "desc"    => "",
        "name"    => "event_btn_bg",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "background || body .stec-layout-event-btn-fontandcolor",
                "background || body .stec-layout-single-btn-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_text_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-btn-fontandcolor",
                "color || body .stec-layout-single-btn-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_bg_hover",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-layout-event-btn-fontandcolor.active",
                "background || body .stec-layout-event-btn-fontandcolor:hover",
                "background || body .stec-layout-single-btn-fontandcolor.active",
                "background || body .stec-layout-single-btn-fontandcolor:hover"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_text_color_hover",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-btn-fontandcolor.active",
                "color || body .stec-layout-event-btn-fontandcolor:hover",
                "color || body .stec-layout-single-btn-fontandcolor.active",
                "color || body .stec-layout-single-btn-fontandcolor:hover"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Secondary buttons background color, border color, text color, background hover color, border hover color, text hover color"),
        "desc"    => "",
        "name"    => "event_btn_sec_bg",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-layout-event-btn-sec-fontandcolor",
                "background || body .stec-layout-single-btn-sec-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_sec_border_color",
        "type"    => "color",
        "value"   => "#e5e5e5",
        "default" => "#e5e5e5",
        "css"     => array(
                "border-color || body .stec-layout-event-btn-sec-fontandcolor",
                "border-color || body .stec-layout-single-btn-sec-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_sec_text_color",
        "type"    => "color",
        "value"   => "#999da2",
        "default" => "#999da2",
        "css"     => array(
                "color || body .stec-layout-event-btn-sec-fontandcolor",
                "color || body .stec-layout-single-btn-sec-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_sec_bg_hover",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-layout-event-btn-sec-fontandcolor.active",
                "background || body .stec-layout-event-btn-sec-fontandcolor:hover",
                "background || body .stec-layout-single-btn-sec-fontandcolor.active",
                "background || body .stec-layout-single-btn-sec-fontandcolor:hover"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_sec_border_color_hover",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "border-color || body .stec-layout-event-btn-sec-fontandcolor.active",
                "border-color || body .stec-layout-event-btn-sec-fontandcolor:hover",
                "border-color || body .stec-layout-single-btn-sec-fontandcolor.active",
                "border-color || body .stec-layout-single-btn-sec-fontandcolor:hover"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_btn_sec_text_color_hover",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-layout-event-btn-sec-fontandcolor.active",
                "color || body .stec-layout-event-btn-sec-fontandcolor:hover",
                "color || body .stec-layout-single-btn-sec-fontandcolor.active",
                "color || body .stec-layout-single-btn-sec-fontandcolor:hover"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Titles text color, secondary titles text color, normal text color, links text color, links text hover color"),
        "desc"    => "",
        "name"    => "event_title_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-title-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_title2_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-title2-fontandcolor",
                "color || body .stec-layout-event-title2-fontandcolor a",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_text_color",
        "type"    => "color",
        "value"   => "#999da2",
        "default" => "#999da2",
        "css"     => array(
                "color || body .stec-layout-event-text-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_link_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-inner-intro-exports form button",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_link_text_color_hover",
        "type"    => "color",
        "value"   => "#f15e6f",
        "default" => "#f15e6f",
        "css"     => array(
                "color || body .stec-layout-event-inner-intro-attachment a:hover",
                "color || body .stec-layout-single-attachment a:hover",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Input background color, text color"),
        "desc"    => "",
        "name"    => "event_input_bg",
        "type"    => "color",
        "value"   => "#f1f1f1",
        "default" => "#f1f1f1",
        "css"     => array(
                "background || body .stec-layout-event-input-fontandcolor"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_input_text_color",
        "type"    => "color",
        "value"   => "#999da2",
        "default" => "#999da2",
        "css"     => array(
                "color || body .stec-layout-event-input-fontandcolor"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Counter numbers color, text color"),
        "desc"    => "",
        "name"    => "event_counter_num_text_color",
        "type"    => "color",
        "value"   => "#202020",
        "default" => "#202020",
        "css"     => array(
                "color || body .stec-layout-event-inner-intro-counter-num",
                "color || body .stec-layout-single-counter-num",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_counter_text_color",
        "type"    => "color",
        "value"   => "#999da2",
        "default" => "#999da2",
        "css"     => array(
                "color || body .stec-layout-event-inner-intro-counter-label",
                "color || body .stec-layout-single-counter-label",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Schedule Title text color, Date text color"),
        "desc"    => $stachethemes_ec_main->lang("Schedule title text color"),
        "name"    => "event_schedule_title_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-inner-schedule-tab-right-title span",
                "color || body .stec-layout-single-schedule-tab-right-title span",
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "desc"    => $stachethemes_ec_main->lang("Schedule date text color"),
        "name"    => "event_schedule_date_text_color",
        "type"    => "color",
        "value"   => "#bdc1c8",
        "default" => "#bdc1c8",
        "css"     => array(
                "color || body .stec-layout-event-inner-schedule-tab-left span",
                "color || body .stec-layout-single-schedule-tab-left span",
        )
));


$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast section titles color, Summary text color, Today temperature text color, Table header background, General text color"),
        "desc"    => $stachethemes_ec_main->lang("Forecast section titles color"),
        "name"    => "event_forecast_section_title_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-inner-forecast-top-title",
                "color || body .stec-layout-event-inner-forecast-details > div > p",
                "color || body .stec-layout-single-forecast-top-title",
                "color || body .stec-layout-single-forecast-details > div > p"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "desc"    => $stachethemes_ec_main->lang("Summary text color"),
        "name"    => "event_forecast_summary_text_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-layout-event-inner-forecast-today-left-current-text",
                "color || body .stec-layout-single-forecast-today-left-current-text",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "desc"    => $stachethemes_ec_main->lang("Today temperature text color"),
        "name"    => "event_forecast_current_temp_text_color",
        "type"    => "color",
        "value"   => "#999da2",
        "default" => "#999da2",
        "css"     => array(
                "color || body .stec-layout-event-inner-forecast-today-left-current-temp",
                "color || body .stec-layout-single-forecast-today-left-current-temp",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "desc"    => $stachethemes_ec_main->lang("Table header background"),
        "name"    => "event_forecast_table_header_bg",
        "type"    => "color",
        "value"   => "#f8f9fa",
        "default" => "#f8f9fa",
        "css"     => array(
                "background || body .stec-layout-event-inner-forecast-details-left-forecast-top",
                "background || body .stec-layout-single-forecast-details-left-forecast-top",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "desc"    => $stachethemes_ec_main->lang("General text color"),
        "name"    => "event_forecast_text_color",
        "type"    => "color",
        "value"   => "#bdc1c8",
        "default" => "#bdc1c8",
        "css"     => array(
                "color || body .stec-layout-event-inner-forecast-details-left-forecast-top p",
                "color || body .stec-layout-event-inner-forecast-details-left-forecast-day p",
                "color || body .stec-layout-event-inner-forecast-today-right, body .stec-layout-event-inner-forecast-top-date",
                "color || body .stec-layout-event-inner-forecast-top-date",
                "color || body .stec-layout-single-forecast-details-left-forecast-top p",
                "color || body .stec-layout-single-forecast-details-left-forecast-day p",
                "color || body .stec-layout-single-forecast-today-right, body .stec-layout-single-forecast-top-date",
                "color || body .stec-layout-single-forecast-top-date"
        )
));


$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Title Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_title",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "30px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "30px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-title-fontandcolor"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Secondary Title Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_title2",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "16px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "16px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-intro-media-content-subs p",
                "font || body .stec-layout-event-inner-intro-media-content > div div p",
                "font || body .stec-layout-event-title2-fontandcolor",
                "font || body .stec-layout-event-inner-top-tabs p"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Buttons Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_btn",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "16px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "16px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-btn-fontandcolor",
                "font || body .stec-layout-single-btn-fontandcolor"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Secondary buttons Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_btn_sec",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "16px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "14px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-btn-sec-fontandcolor",
                "font || body .stec-layout-single-btn-sec-fontandcolor"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Normal text Font, Weight, Size and Line-Height"),
        "desc"    => "",
        "name"    => "event_text",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "16px",
                "1.6",
        ),
        "default" => array(
                "Roboto",
                "400",
                "16px",
                "1.6"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-intro-media-content > div div span",
                "font || body .stec-layout-event-inner-intro-media-content-subs span",
                "font || body .stec-layout-event-text-fontandcolor",
                "font || body .stec-layout-event-input-fontandcolor"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Event counter numbers Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_counter_num",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "700",
                "40px"
        ),
        "default" => array(
                "Roboto",
                "700",
                "40px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-intro-counter-num",
                "font || body .stec-layout-single-counter-num",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Event counter text Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_counter_text",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "14px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "14px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-intro-counter-label",
                "font || body .stec-layout-single-counter-label",
        )
));


$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Schedule Title Font, Weight and Size"),
        "desc"    => $stachethemes_ec_main->lang("Schedule Title Font, Weight and Size"),
        "name"    => "event_schedule_title",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "500",
                "18px"
        ),
        "default" => array(
                "Roboto",
                "500",
                "18px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-schedule-tab-right-title span",
                "font || body .stec-layout-single-schedule-tab-right-title span",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Schedule date Font, Weight and Size"),
        "desc"    => $stachethemes_ec_main->lang("Schedule date Font, Weight and Size"),
        "name"    => "event_schedule_date",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "14px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "14px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-schedule-tab-left span",
                "font || body .stec-layout-single-schedule-tab-left span",
        )
));


$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast section titles Font, Weight and Size"),
        "desc"    => "",
        "name"    => "event_forecast_section_title",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "500",
                "24px"
        ),
        "default" => array(
                "Roboto",
                "500",
                "24px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-forecast-top-title",
                "font || body .stec-layout-event-inner-forecast-details > div > p",
                "font || body .stec-layout-single-forecast-top-title",
                "font || body .stec-layout-single-forecast-details > div > p"
        )
));



$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast Summary Font, Weight and Size "),
        "desc"    => $stachethemes_ec_main->lang("Forecast Summary Font, Weight and Size"),
        "name"    => "event_forecast_summary",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "500",
                "20px"
        ),
        "default" => array(
                "Roboto",
                "500",
                "20px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-forecast-today-left-current-text",
                "font || body .stec-layout-single-forecast-today-left-current-text",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast Today temperature Font, Weight and Size"),
        "desc"    => $stachethemes_ec_main->lang("Forecast Today temperature Font, Weight and Size"),
        "name"    => "event_forecast_current_temp",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "43px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "43px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-forecast-today-left-current-temp",
                "font || body .stec-layout-single-forecast-today-left-current-temp",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast Table header Font, Weight and Size"),
        "desc"    => $stachethemes_ec_main->lang("Forecast Table header Font, Weight and Size"),
        "name"    => "event_forecast_table_header",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "500",
                "12px"
        ),
        "default" => array(
                "Roboto",
                "500",
                "12px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-forecast-details-left-forecast-top p",
                "font || body .stec-layout-single-forecast-details-left-forecast-top p",
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Forecast General text Font, Weight and Size"),
        "desc"    => $stachethemes_ec_main->lang("Forecast General text Font, Weight and Size"),
        "name"    => "event_forecast_text",
        "type"    => "font",
        "value"   => array(
                "Roboto",
                "400",
                "16px"
        ),
        "default" => array(
                "Roboto",
                "400",
                "16px"
        ),
        "css"     => array(
                "font || body .stec-layout-event-inner-forecast-details-left-forecast-day p",
                "font || body .stec-layout-event-inner-forecast-today-right p",
                "font || body .stec-layout-event-inner-forecast-top-date",
                "font || body .stec-layout-single-forecast-details-left-forecast-day p",
                "font || body .stec-layout-single-forecast-today-right p",
                "font || body .stec-layout-single-forecast-top-date"
        )
));





$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "title"   => $stachethemes_ec_main->lang("Event info title and text align"),
        "desc"    => "",
        "name"    => "event_intro_title_align",
        "type"    => "select",
        "value"   => 'center',
        "default" => 'center',
        "select"  => array(
                'left'   => $stachethemes_ec_main->lang('Left'),
                'center' => $stachethemes_ec_main->lang('Center'),
                'right'  => $stachethemes_ec_main->lang('Right')
        ),
        "req"     => true,
        "css"     => array(
                "text-align || body .stec-layout-event-inner-intro .stec-layout-event-inner-intro-title"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_event",
        "name"    => "event_intro_text_align",
        "type"    => "select",
        "value"   => 'left',
        "default" => 'left',
        "select"  => array(
                'left'   => $stachethemes_ec_main->lang('Left'),
                'center' => $stachethemes_ec_main->lang('Center'),
                'right'  => $stachethemes_ec_main->lang('Right')
        ),
        "req"     => true,
        "css"     => array(
                "text-align || body .stec-layout-event-inner-intro-desc"
        )
));

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_event",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "event_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);


/**
 * Tooltip css tab
 */
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Title Color, Text Color, Background Color"),
        "desc"    => "",
        "name"    => "tooltip_title_color",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "color || body .stec-tooltip-title"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "desc"    => "",
        "name"    => "tooltip_text_color",
        "type"    => "color",
        "value"   => "#9599a2",
        "default" => "#9599a2",
        "css"     => array(
                "color || body .stec-tooltip-desc",
                "color || body .stec-tooltip-location",
                "color || body .stec-tooltip-timespan"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "desc"    => "",
        "name"    => "tooltip_bg",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "background || body .stec-tooltip"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Counter Text Color & Background Color"),
        "desc"    => "",
        "name"    => "tooltip_clock_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-tooltip-counter"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "desc"    => "",
        "name"    => "tooltip_clock_bg",
        "type"    => "color",
        "value"   => "#4d576c",
        "default" => "#4d576c",
        "css"     => array(
                "background || body .stec-tooltip-counter"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Expired Text Color & Background Color"),
        "desc"    => "",
        "name"    => "tooltip_expired_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-tooltip-expired"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "desc"    => "",
        "name"    => "tooltip_expired_bg",
        "type"    => "color",
        "value"   => "#f15e6e",
        "default" => "#f15e6e",
        "css"     => array(
                "background || body .stec-tooltip-expired"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("In Progress Text Color & Background Color"),
        "desc"    => "",
        "name"    => "tooltip_prog_color",
        "type"    => "color",
        "value"   => "#ffffff",
        "default" => "#ffffff",
        "css"     => array(
                "color || body .stec-tooltip-progress"
        )
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "desc"    => "",
        "name"    => "tooltip_prog_bg",
        "type"    => "color",
        "value"   => "#53b32b",
        "default" => "#53b32b",
        "css"     => array(
                "background || body .stec-tooltip-progress"
        )
));


$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Title Font"),
        "desc"    => "",
        "name"    => "tooltip_title_font",
        "type"    => "font",
        "value"   => array(
                'Roboto',
                '400',
                '20px'
        ),
        "default" => array(
                'Roboto',
                '400',
                '20px'
        ),
        "css"     => array(
                "font || body .stec-tooltip-title"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Text Font"),
        "desc"    => "",
        "name"    => "tooltip_text_font",
        "type"    => "font",
        "value"   => array(
                'Roboto',
                '400',
                '14px',
                '1.3'
        ),
        "default" => array(
                'Roboto',
                '400',
                '14px',
                '1.3'
        ),
        "css"     => array(
                "font || body .stec-tooltip-desc",
                "font || body .stec-tooltip-location",
                "font || body .stec-tooltip-timespan"
        )
));

$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__fontsandcolors_tooltip",
        "title"   => $stachethemes_ec_main->lang("Status and Counter Font"),
        "desc"    => "",
        "name"    => "tooltip_status_clock_font",
        "type"    => "font",
        "value"   => array(
                'Roboto',
                '400',
                '10px'
        ),
        "default" => array(
                'Roboto',
                '400',
                '10px'
        ),
        "css"     => array(
                "font || body .stec-tooltip-status",
                "font || body .stec-tooltip-counter"
        )
));

$stachethemes_ec_main->register_admin_setting(
        array(
                "page"    => "stec_menu__fontsandcolors_tooltip",
                "title"   => $stachethemes_ec_main->lang("Add !important rule"),
                "desc"    => "",
                "name"    => "tooltip_important",
                "type"    => "checkbox",
                "value"   => 0,
                "default" => 0
        )
);


/**
 * Custom css Tab
 */
$stachethemes_ec_main->register_admin_setting(array(
        "page"  => "stec_menu__fontsandcolors_custom",
        "title" => $stachethemes_ec_main->lang("Custom Style"),
        "desc"  => "",
        "name"  => "custom_css",
        "type"  => "textarea"
));


/**
 * Cache settings
 */
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__cache",
        "title"   => $stachethemes_ec_main->lang("Cache Events"),
        "desc"    => "",
        "name"    => "cache_events",
        "type"    => "select",
        "value"   => "0",
        "default" => "0",
        "select"  => array(
                '0' => $stachethemes_ec_main->lang('No'),
                '1' => $stachethemes_ec_main->lang('Yes'),
        ),
        "req"     => true
));
$stachethemes_ec_main->register_admin_setting(array(
        "page"    => "stec_menu__cache",
        "title"   => $stachethemes_ec_main->lang("Cache Weather Data (Recommended)"),
        "desc"    => "",
        "name"    => "cache_forecast",
        "type"    => "select",
        "value"   => "1",
        "default" => "1",
        "select"  => array(
                '0' => $stachethemes_ec_main->lang('No'),
                '1' => $stachethemes_ec_main->lang('Yes'),
        ),
        "req"     => true
));



/**
 * Delete old unused settings
 */
$stachethemes_ec_main->delete_admin_settings('stec_menu__fontsandcolors_preview', array(
        'preview_expand_btn_bg_hover',
        'preview_reminder_btn_bg_hover'
        )
);

$stachethemes_ec_main->delete_admin_settings('stec_menu__fontsandcolors_event', array(
        'event_forecast_today_temp_text_color',
        'event_forecast_today_deg_text_color',
        'event_forecast_today_wind_text_color',
        'event_forecast_days_date_text_color',
        'event_forecast_days_temp_text_color',
        'event_forecast_today_temp',
        'event_forecast_today_deg',
        'event_forecast_today_wind',
        'event_forecast_days_date',
        'event_forecast_days_temp'
        )
);
