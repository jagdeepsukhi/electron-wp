<?php




/**
 * Static admin class
 */
class stachethemes_ec_admin {



    protected static $_wc        = false;
    protected static $_messages  = false;
    protected static $time_limit = 0;



    public static function session_set($var, $value = '') {

        $_SESSION[$var] = $value;
    }



    public static function session_get($var, $default = false) {

        return isset($_SESSION[$var]) ? $_SESSION[$var] : $default;
    }



    public static function get($var, $default = false, $filter = FILTER_DEFAULT) {

        $value = filter_input(INPUT_GET, $var, $filter);

        return $value ? $value : $default;
    }



    public static function post($var, $default = false, $filter = FILTER_DEFAULT, $opt = FILTER_REQUIRE_SCALAR) {

        $value = filter_input(INPUT_POST, $var, $filter, $opt);

        return $value || $value === 0 || $value === '0' ? $value : $default;
    }



    public static function display_message() {

        self::$_messages = self::session_get('stec-admin-message');

        if ( self::$_messages !== false ) {

            switch ( self::$_messages['type'] ) :

                case 'error' :
                    echo "<p class='stachethemes-admin-msg-error'>" . self::$_messages['text'] . "</p>";
                    break;

                default:
                    echo "<p class='stachethemes-admin-msg-notice'>" . self::$_messages['text'] . "</p>";

            endswitch;
        }

        self::session_set('stec-admin-message', false);
        self::$_messages = false;
    }



    public static function set_message($text, $type = '') {

        self::session_set('stec-admin-message', array(
                'text' => $text,
                'type' => $type
        ));
    }



    public static function html_color($name, $value, $default = '', $desc = '') {

        $val = $value ? $value : $default;

        echo "<input class='stachethemes-admin-colorpicker' name='{$name}' value='{$val}' autocomplete='off' title='{$desc}' />";
    }



    public static function html_info($text) {
        if ( $text == '' ) {
            return;
        }
        echo "<p class='stachethemes-admin-info'>{$text}</p>";
    }



    public static function html_input($name, $value, $default = "", $placeholder = "", $required = false, $type = "text", $custom = '') {

        $val = $value ? esc_html($value) : $default;
        $req = $required ? 'required="required"' : '';

        echo "<input class='stachethemes-admin-input' type='{$type}' name='{$name}' placeholder='{$placeholder}' value='{$val}' $req $custom />";
    }



    public static function html_checkbox($name, $checked, $default = false, $label = false, $required = false) {

        $id = 'id-' . uniqid();

        $checked = $checked ? 'checked' : $default;
        $req     = $required ? 'required="required"' : '';

        echo "<input id='$id' class='stachethemes-admin-checkbox' type='checkbox' name='{$name}' $checked $req />";

        if ( $label ) {
            echo "<label for='$id' class='stachethemes-admin-checkbox-label'>$label</label>";
        }
    }



    public static function html_radio($name, $checked, $default = false, $label = false, $required = false) {

        $id = 'id-' . uniqid();

        $checked = $checked ? 'checked' : $default;
        $req     = $required ? 'required="required"' : '';

        echo "<input id='$id' class='stachethemes-admin-checkbox' type='radio' name='{$name}' $checked $req />";

        if ( $label ) {
            echo "<label for='$id' class='stachethemes-admin-checkbox-label'>$label</label>";
        }
    }



    public static function html_date($name, $value, $default = "", $placeholder = "", $required = false, $type = "text") {

        $val = $value ? $value : $default;
        $req = $required ? 'required="required"' : '';

        echo "<input class='stachethemes-admin-input input-date' type='{$type}' name='{$name}' placeholder='{$placeholder}' value='{$val}' $req />";
    }



    public static function html_hidden($name, $value) {
        echo "<input type='hidden' name='{$name}' value='{$value}' />";
    }



    public static function html_textarea($name, $value, $default = "", $placeholder = "", $required = false, $custom = '') {

        $val = $value ? $value : $default;
        $req = $required ? 'required="required"' : '';

        echo "<textarea class='stachethemes-admin-textarea' name='{$name}' placeholder='{$placeholder}' $req $custom>{$val}</textarea>";
    }



    /**
     * @todo Graphic implementation
     */
    public static function html_icon($name, $list, $default = "", $required = false) {
        return self::html_select($name, $list, $default, $required);
    }



    public static function html_select($name, $options, $default = "", $required = false) {

        $req  = $required ? 'required="required"' : '';
        $html = "<select class='stachethemes-admin-select' name='{$name}' $req autocomplete='off'>";

        foreach ( $options as $val => $name ) :
            $sel  = $default == $val ? 'selected="selected"' : "";
            $html .= "<option value='{$val}' {$sel}>{$name}</option>";
        endforeach;

        $html .= "</select>";

        echo $html;
    }



    public static function html_button($text, $href = false, $noclear = false, $customclass = "") {

        if ( $noclear === true ) {
            $noclear = 'stachethemes-admin-button-no-clear';
        }

        if ( $href ) {
            echo "<a class='stachethemes-admin-button-a {$noclear} {$customclass}' href='{$href}' >{$text}</a>";
        } else {
            echo "<button class='stachethemes-admin-button {$noclear} {$customclass}'>{$text}</button>";
        }
    }



    public static function html_form_start($action = "/", $method = "POST", $formdata = false) {

        $enctype = $formdata ? 'enctype="multipart/form-data"' : '';

        echo "<form class='stachethemes-admin-form' method='{$method}' action='{$action}' {$enctype}>";

        wp_nonce_field($action, 'stec_admin_form_nonce');
    }



    public static function html_form_end() {

        echo "</form>";
    }



    public static function html_add_image($name, $default = false, $title = "Add Image", $required = false, $single = false) {

        $req = $required ? 'required="required"' : '';

        echo "<ul class='stachethemes-admin-add-image-list' data-single='{$single}' data-name='{$name}' $req>";

        if ( $default ) :

            $default = explode(',', $default);

            foreach ( $default as $image_id ) :
                $image_source = wp_get_attachment_image_src($image_id, "medium");
                ?>
                <li class="stachethemes-admin-image-container"><i class="fa fa-times"></i>
                    <img alt="" src="<?php echo $image_source[0]; ?>" data-id="<?php echo $image_id; ?>"><input type="hidden" value="<?php echo $image_id; ?>" name="<?php echo $name; ?>">
                </li>
                <?php
            endforeach;

        endif;

        echo "</ul>";

        echo "<button data-single='{$single}' data-name='{$name}' class='stachethemes-admin-add-image'>{$title}</button>";
    }



    public static function to_utc_time($date, $timezone, $format = 'Y-m-d H:i:s') {

        $UTC    = new DateTimeZone("UTC");
        $caltTZ = new DateTimeZone($timezone);

        $date = new DateTime($date, $caltTZ);
        $date->setTimezone($UTC);

        return $date->format($format);
    }



    public static function hours_array() {

        $arr = array();

        $time_format = self::get_admin_setting_value('stec_menu__general', 'time_format');

        if ( $time_format == '24' ) {
            // 24 hours

            $hour = 0;

            for ( $i = 0; $i < 24; $i++ ) {
                $hour       = sprintf("%02d", $hour);
                $arr[$hour] = $hour;
                $hour++;
            }
        } else {
            // 12/12 hours
            $h24  = 0;
            $hour = 12;
            $ampm = "am";

            for ( $i = 0; $i < 24; $i++ ) {


                if ( $i == 12 ) {
                    $ampm = "pm";
                }

                if ( $hour > 12 ) {
                    $hour = 1;
                }

                $hour = sprintf("%02d", $hour);
                $val  = $hour . " " . $ampm;

                $arr[sprintf("%02d", $h24)] = $val;

                $h24++;
                $hour++;
            }
        }

        return $arr;
    }



    public static function minutes_array() {
        $arr = array();
        for ( $i = 0; $i < 60; $i += 5 ) {
            $val       = sprintf("%02d", $i);
            $arr[$val] = $val;
        }
        return $arr;
    }



    public static function social_array() {

        $arr = array();

        $arr["fa fa-behance"]     = "Behance";
        $arr["fa fa-deviantart"]  = "Devian Art";
        $arr["fa fa-dribbble"]    = "Dribbble";
        $arr["fa fa-facebook"]    = "Facebook";
        $arr["fa fa-flickr"]      = "Flickr";
        $arr["fa fa-github"]      = "Github";
        $arr["fa fa-google-plus"] = "Google Plus";
        $arr["fa fa-lastfm"]      = "LastFM";
        $arr["fa fa-linkedin"]    = "LinkedIn";
        $arr["fa fa-reddit"]      = "Reddit";
        $arr["fa fa-soundcloud"]  = "Soundcloud";
        $arr["fa fa-tumblr"]      = "Tumblr";
        $arr["fa fa-twitch"]      = "Twitch";
        $arr["fa fa-twitter"]     = "Twitter";
        $arr["fa fa-vimeo"]       = "Vimeo";
        $arr["fa fa-youtube"]     = "Youtube";
        $arr["fa fa-instagram"]   = "Instagram";
        $arr["fa fa-pinterest"]   = "Pinterest";
        $arr["fa fa-skype"]       = "Skype";
        $arr["fa fa-steam"]       = "Steam";
        $arr["fa fa-envelope"]    = "E-Mail";

        return $arr;
    }



    public static function verify_nonce($action, $method = "POST") {

        switch ( strtolower($method) ) :

            case 'ajax' :

                if ( check_ajax_referer('stec-nonce-string', 'security', false) === false ) {
                    die();
                }

                break;

            case 'get' :

                $nonce = self::get('_wpnonce');

                if ( !wp_verify_nonce($nonce) ) {
                    die('<h1>Nonce did not verify</h1>');
                }

                break;


            default:

                $nonce = self::post('stec_admin_form_nonce');

                if ( !wp_verify_nonce($nonce, $action) ) {
                    die('<h1>Nonce did not verify</h1>');
                }
        endswitch;
    }



    /**
     * Creates calendar in #__stec_calendars
     * 
     * @global object $wpdb
     * @param string $title
     * @param string $color
     * @return bool
     */
    public static function create_calendar($title, $color, $icon, $timezone, $back_visibility, $visibility, $writable, $req_approval) {

        global $wpdb;

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_calendars', array(
                'created_by'      => get_current_user_id(),
                'title'           => $title,
                'color'           => $color,
                'icon'            => $icon,
                'timezone'        => $timezone,
                'back_visibility' => $back_visibility,
                'visibility'      => $visibility,
                'writable'        => $writable,
                'req_approval'    => $req_approval,
                ), array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d'
                )
        );


        return $query !== false ? true : false;
    }



    /**
     * Updates calendar in #__stec_calendars
     * 
     * @global object $wpdb
     * @param string $title
     * @param string $color
     * @param int $calendar_id
     * @return bool
     */
    public static function update_calendar($title, $color, $icon, $timezone, $back_visibility, $visibility, $writable, $req_approval, $calendar_id) {

        global $wpdb;

        $query = $wpdb->update(
                $wpdb->prefix . 'stec_calendars', array(
                'title'           => $title,
                'color'           => $color,
                'icon'            => $icon,
                'timezone'        => $timezone,
                'back_visibility' => $back_visibility,
                'visibility'      => $visibility,
                'writable'        => $writable,
                'req_approval'    => $req_approval
                ), array(
                'id' => $calendar_id
                ), array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d'
                ), array(
                '%d'
                )
        );


        return $query !== false ? true : false;
    }



    /**
     * 
     * @param int $userid
     * @return string '...','...','...'
     */
    private static function _get_user_roles($userid) {

        $userid = (int) $userid;

        $user_info    = get_userdata($userid);
        $user_roles   = isset($user_info->roles) ? $user_info->roles : array();
        $user_roles[] = 'stec_public';

        if ( is_user_logged_in() ) {
            $user_roles[] = 'stec_logged_in';
        }

        $user_roles = "'" . implode("','", $user_roles) . "'";

        return $user_roles;
    }



    private static function _get_cal_vis_filter($userid) {

        $user_roles = self::_get_user_roles($userid);

        $vis_filter = " WHERE ( (calendar.back_visibility IN ($user_roles) OR calendar.created_by = {$userid}) ";

        if ( is_super_admin($userid) ) {
            $vis_filter .= ' OR 1 ) ';
        } else {
            $vis_filter .= ' ) ';
        }

        return $vis_filter;
    }



    public static function calendars_list() {

        $calendars = self::calendars();
        $list      = array();

        foreach ( $calendars as $c ) {

            $list[$c->id] = $c->title;
        }

        return $list;
    }



    /**
     * Get object list with all calendars
     * @return object
     */
    public static function calendars() {

        global $wpdb;

        $filter = self::_get_cal_vis_filter(get_current_user_id());
        $query  = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_calendars as calendar $filter", OBJECT);

        return $query !== false ? $query : array();
    }



    /**
     * Get object list with calendar by id
     * @return object or FALSE
     */
    public static function calendar($calendar_id) {

        global $wpdb;

        $query = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}stec_calendars WHERE id='{$calendar_id}' ", OBJECT);

        return $query !== false ? $query : false;
    }



    /**
     * Delete calendar
     * @return bool
     */
    public static function delete_calendar($calendar_id) {

        global $wpdb;

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_calendars', array(
                'id' => $calendar_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        // Get all events with this calendar id
        $events = self::events($calendar_id);

        if ( $events ) {
            foreach ( $events as $event ) {
                self::delete_event($event->id);
            }
        }

        // Delete import cronjobs linked to this calendar
        $import_cronjobs = self::get_import_cronjobs($calendar_id);

        if ( $import_cronjobs ) {
            foreach ( $import_cronjobs as $job ) {
                self::delete_import_cronjob($job->id);
            }
        }

        return true;
    }



    /**
     * Get object list with all events
     * @return object
     */
    public static function events($calendar_id, $inc_unapproved = false) {

        global $wpdb;

        $filter = self::_get_cal_vis_filter(get_current_user_id());
        $filter .= " AND event.calid = '{$calendar_id}' ";

        if ( $inc_unapproved === false ) {
            $filter .= " AND approved = 1 ";
        }

        $query = $wpdb->get_results(" 
                SELECT event.*, repeater.rrule, repeater.exdate, repeater.is_advanced_rrule, meta.uid, meta.created, meta.recurrence_id FROM {$wpdb->prefix}stec_events as event
                LEFT JOIN {$wpdb->prefix}stec_calendars as calendar ON event.calid = calendar.id
                LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON event.id = repeater.eventid 
                LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON event.id = meta.eventid 
                $filter
                ORDER BY event.approved ASC, event.start_date DESC, event.id DESC ", OBJECT);
        return $query !== false ? $query : array();
    }



    /**
     * Get number of events that await approval
     * @param int $calendar_id
     */
    public static function get_aaproval_count($calendar_id = false) {

        global $wpdb;

        $filter = '';

        if ( $calendar_id ) {

            $calendar_id = (int) $calendar_id;

            $filter .= " AND calid = {$calendar_id} ";
        }

        $wpdb->get_results("SELECT id FROM {$wpdb->prefix}stec_events WHERE approved = 0 $filter ");

        return $wpdb->num_rows;
    }



    /**
     * Delete event
     * @return bool
     */
    public static function delete_event($event_id) {

        global $wpdb;

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_events', array(
                'id' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_events_repeater', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_events_meta', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_schedule', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_guests', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_attendance', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_attachments', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_reminder', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        return true;
    }



    /**
     * Creates event in #__stec_events
     * 
     * @global object $wpdb
     * @param object $general
     * @param object $optional
     * @return bool
     */
    public static function create_event($event_data) {

        global $wpdb;

        $start_date = $event_data->start_date . ' ' . $event_data->start_time_hours . ':' . $event_data->start_time_minutes . ':00';
        $end_date   = $event_data->end_date . ' ' . $event_data->end_time_hours . ':' . $event_data->end_time_minutes . ':00';

        if ( trim($event_data->alias) == '' ) {
            $event_data->alias = sanitize_title($event_data->summary);
        }

        $wpdb->show_errors();

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events', array(
                'created_by' => get_current_user_id(),
                'calid'      => $event_data->calendar->id,
                'alias'      => self::validate_event_alias($event_data->alias),
                'summary'    => $event_data->summary,
                'color'      => $event_data->color,
                'icon'       => $event_data->icon,
                'visibility' => $event_data->visibility,
                'featured'   => $event_data->featured,
                'start_date' => $start_date,
                'end_date'   => $end_date,
                'all_day'    => $event_data->all_day,
                'keywords'   => $event_data->keywords,
                'counter'    => $event_data->counter,
                'comments'   => $event_data->comments,
                'link'       => $event_data->link,
                'approved'   => $event_data->approved,
                ), array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%d',
                '%s',
                '%d',
                '%d',
                '%s',
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        $event_id = $wpdb->insert_id;

        if ( !$event_id ) {
            return false;
        }

        /**
         * Repeater
         */
        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events_repeater', array(
                'eventid'           => $event_id,
                'rrule'             => $event_data->rrule,
                'exdate'            => $event_data->exdate,
                'is_advanced_rrule' => $event_data->is_advanced_rrule,
                ), array(
                '%d',
                '%s',
                '%s',
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }


        /**
         * Meta
         */
        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_events_meta', array(
                'eventid'       => $event_id,
                'uid'           => isset($event_data->uid) && !empty($event_data->uid) ?
                $event_data->uid :
                uniqid('e-') . '_' . md5(microtime()) . '@stachethemes_ec.com',
                'recurrence_id' => isset($event_data->recurrence_id) ? $event_data->recurrence_id : NULL,
                ), array(
                '%d',
                '%s',
                '%s'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        /**
         * Optional data
         */
        $query = $wpdb->update($wpdb->prefix . 'stec_events', array(
                'location'           => $event_data->location,
                'location_details'   => $event_data->location_details,
                'location_forecast'  => $event_data->location_forecast,
                'location_use_coord' => $event_data->location_use_coord,
                'description'        => $event_data->description,
                'description_short'  => $event_data->description_short,
                'images'             => $event_data->images ? implode(',', $event_data->images) : ''
                ), array(
                'id' => $event_id
                ), array(
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%s',
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }


        if ( $event_data->schedule ) :


            foreach ( $event_data->schedule as $schedule ) :

                $schedule = (object) $schedule;

                if ( !isset($schedule->schedule_date_from) || $schedule->schedule_date_from == '' ) {
                    continue;
                }

                $schedule_start_date = $schedule->schedule_date_from . ' ' . $schedule->schedule_time_hours_from . ':' . $schedule->schedule_time_minutes_from . ':00';

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_schedule', array(
                        'eventid'     => $event_id,
                        'start_date'  => $schedule_start_date,
                        'title'       => $schedule->schedule_title,
                        'icon'        => $schedule->schedule_icon,
                        'icon_color'  => $schedule->schedule_icon_color,
                        'description' => $schedule->schedule_details,
                        ), array(
                        '%d',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }

            endforeach;
        endif;

        if ( $event_data->guests ) :

            foreach ( $event_data->guests as $guest ) :

                $guest = (object) $guest;

                if ( $guest->name == "" ) {
                    continue;
                }

                $social = array();

                if ( isset($guest->social) ) :
                    foreach ( $guest->social as $soc ) :
                        $soc      = (object) $soc;
                        $social[] = $soc->ico . '::' . $soc->link;
                    endforeach;
                endif;

                $social = implode('||', $social);

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_guests', array(
                        'eventid' => $event_id,
                        'photo'   => isset($guest->photo) ? $guest->photo : null,
                        'name'    => $guest->name,
                        'about'   => isset($guest->about) ? $guest->about : null,
                        'links'   => $social,
                        ), array(
                        '%d',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }

            endforeach;
        endif;

        if ( $event_data->attendee ) :
            foreach ( $event_data->attendee as $attendee ) :

                $attendee = (object) $attendee;

                if ( !isset($attendee->userid) && isset($attendee->email) && filter_var($attendee->email, FILTER_VALIDATE_EMAIL) !== false ) {

                    $query = $wpdb->insert(
                            $wpdb->prefix . 'stec_attendance', array(
                            'eventid'      => $event_id,
                            'email'        => $attendee->email,
                            'status'       => 0,
                            'access_token' => md5(microtime()),
                            ), array(
                            '%d',
                            '%s',
                            '%d',
                            '%s'
                            )
                    );

                    if ( $query === false ) {
                        $wpdb->print_error();
                        return false;
                    }
                } elseif ( isset($attendee->userid) && !is_nan($attendee->userid) ) {

                    $email = get_userdata($attendee->userid)->user_email;

                    $query = $wpdb->insert(
                            $wpdb->prefix . 'stec_attendance', array(
                            'eventid'      => $event_id,
                            'email'        => $email,
                            'userid'       => $attendee->userid,
                            'status'       => 0,
                            'access_token' => md5(microtime())
                            ), array(
                            '%d',
                            '%s',
                            '%s',
                            '%d',
                            '%s'
                            )
                    );

                    if ( $query === false ) {
                        $wpdb->print_error();
                        return false;
                    }
                }
            endforeach;
        endif;


        if ( $event_data->attachments ) :

            foreach ( $event_data->attachments as $attachment ) :

                $attachment = (object) $attachment;

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_attachments', array(
                        'eventid'    => $event_id,
                        'attachment' => $attachment->id
                        ), array(
                        '%d',
                        '%d'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }

            endforeach;

        endif;


        if ( $event_data->wc_product ) :

            foreach ( $event_data->wc_product as $product_id ) :

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_woocommerce', array(
                        'eventid'    => $event_id,
                        'product_id' => $product_id
                        ), array(
                        '%d',
                        '%d'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }

            endforeach;

        endif;

        self::send_mail_invites($event_id);

        return true;
    }



    /**
     * Get event by id
     * @return object or FALSE
     */
    public static function event($event_id) {

        global $wpdb;

        $event = $wpdb->get_row(""
                . " SELECT event.*, repeater.rrule, repeater.exdate, repeater.is_advanced_rrule, meta.created, meta.review_note, meta.contact_email, meta.recurrence_id, meta.uid "
                . " FROM {$wpdb->prefix}stec_events as event "
                . " LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON  "
                . " event.id = repeater.eventid "
                . " LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON  "
                . " event.id = meta.eventid "
                . " WHERE event.id = '{$event_id}' ", OBJECT);

        if ( $event === false ) {
            return false;
        }

        if ( empty($event) ) {
            return false;
        }

        // get schedule

        $schedule = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_schedule WHERE eventid='{$event_id}' ", OBJECT);

        if ( $schedule === false ) {
            $schedule = array();
        }

        $event->schedule = $schedule;


        // get guests

        $guests = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_guests WHERE eventid='{$event_id}' ", OBJECT);

        if ( $guests === false ) {
            $guests = array();
        }

        $event->guests = $guests;


        // get attendees

        $attendance = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_attendance WHERE eventid='{$event_id}' AND `repeat_offset` = '0' ", OBJECT);

        if ( $attendance === false ) {
            $attendance = array();
        }

        $event->attendance = $attendance;


        // get attachments

        $attachments = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_attachments WHERE eventid='{$event_id}' ", OBJECT);

        if ( $attachments === false ) {
            $attachments = array();
        }

        $event->attachments = $attachments;



        // get woocommerce

        $wc_products = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_woocommerce WHERE eventid='{$event_id}' ", OBJECT);

        if ( $wc_products === false ) {
            $wc_products = array();
        }

        $event->woocommerce = $wc_products;


        return $event;
    }



    /**
     * Update event
     * 
     * @global object $wpdb
     * @param int $event_id
     * @param object $general
     * @param object $optional
     * @return bool
     */
    public static function update_event($event_id, $event_data) {

        global $wpdb;

        $start_date = $event_data->start_date . ' ' . $event_data->start_time_hours . ':' . $event_data->start_time_minutes . ':00';
        $end_date   = $event_data->end_date . ' ' . $event_data->end_time_hours . ':' . $event_data->end_time_minutes . ':00';

        $wpdb->show_errors();

        if ( trim($event_data->alias) == '' ) {
            $event_data->alias = sanitize_title($event_data->summary);
        }

        $query = $wpdb->update(
                $wpdb->prefix . 'stec_events', array(
                'calid'              => $event_data->calid ? $event_data->calid : $event_data->calendar->id,
                'summary'            => $event_data->summary,
                'alias'              => self::validate_event_alias($event_data->alias, $event_id),
                'color'              => $event_data->color,
                'icon'               => $event_data->icon,
                'visibility'         => $event_data->visibility,
                'featured'           => $event_data->featured,
                'start_date'         => $start_date,
                'end_date'           => $end_date,
                'all_day'            => $event_data->all_day,
                'keywords'           => $event_data->keywords,
                'counter'            => $event_data->counter,
                'comments'           => $event_data->comments,
                'link'               => $event_data->link,
                'approved'           => $event_data->approved,
                'location'           => $event_data->location,
                'location_details'   => $event_data->location_details,
                'location_forecast'  => $event_data->location_forecast,
                'location_use_coord' => $event_data->location_use_coord,
                'description'        => $event_data->description,
                'description_short'  => $event_data->description_short,
                'images'             => $event_data->images ? implode(',', $event_data->images) : ''
                ), array(
                'id' => $event_id
                ), array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%d',
                '%s',
                '%d',
                '%d',
                '%s',
                '%d',
                '%s',
                '%s',
                '%s',
                '%d',
                '%s',
                '%s',
                '%s'
                ), array(
                '%d'
                )
        );


        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        // Update Repeater
        $query = $wpdb->update(
                $wpdb->prefix . 'stec_events_repeater', array(
                'rrule'             => $event_data->rrule,
                'exdate'            => $event_data->exdate,
                'is_advanced_rrule' => $event_data->is_advanced_rrule
                ), array(
                'eventid' => $event_id
                ), array(
                '%s',
                '%s',
                '%d'
                ), array(
                '%d'
                )
        );


        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }


        $result = true;

        if ( self::_update_event_schedule($event_id, $event_data->schedule) === false ) {
            $result = false;
        }

        if ( self::_update_event_guests($event_id, $event_data->guests) === false ) {
            $result = false;
        }

        if ( self::_update_event_attendance($event_id, $event_data->attendee) === false ) {
            $result = false;
        }

        if ( self::_update_event_attachments($event_id, $event_data->attachments) === false ) {
            $result = false;
        }

        if ( class_exists('WooCommerce') ) :

            if ( self::_update_event_woocommerce($event_id, $event_data->wc_product) === false ) {
                $result = false;
            }

        endif;

        return $result;
    }



    /**
     * @return bool
     */
    private static function _update_event_woocommerce($event_id, $wc_product) {

        global $wpdb;

        // delete current
        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_woocommerce', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        if ( !$wc_product ) {
            return true;
        }

        foreach ( $wc_product as $product_id ) :

            $query = $wpdb->insert(
                    $wpdb->prefix . 'stec_woocommerce', array(
                    'eventid'    => $event_id,
                    'product_id' => $product_id
                    ), array(
                    '%d',
                    '%d'
                    )
            );

            if ( $query === false ) {
                $wpdb->print_error();
                return false;
            }

        endforeach;


        return true;
    }



    /**
     * @return bool
     */
    private static function _update_event_attachments($event_id, $attachments) {

        global $wpdb;

        // delete current
        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_attachments', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        if ( !$attachments ) {
            return true;
        }

        foreach ( $attachments as $attachment ) :

            $attachment = (object) $attachment;

            $query = $wpdb->insert(
                    $wpdb->prefix . 'stec_attachments', array(
                    'eventid'    => $event_id,
                    'attachment' => $attachment->id
                    ), array(
                    '%d',
                    '%d'
                    )
            );

            if ( $query === false ) {
                $wpdb->print_error();
                return false;
            }

        endforeach;


        return true;
    }



    /**
     * @return bool
     */
    private static function _update_event_attendance($event_id, $attendees_arr) {

        global $wpdb;

        // delete current
        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_attendance', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        if ( !$attendees_arr ) {
            return true;
        }

        foreach ( $attendees_arr as $attendee ) :

            $attendee = (object) $attendee;

            if ( !isset($attendee->userid) && isset($attendee->email) && filter_var($attendee->email, FILTER_VALIDATE_EMAIL) !== false ) {

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_attendance', array(
                        'eventid'      => $event_id,
                        'email'        => $attendee->email,
                        'status'       => 0,
                        'access_token' => md5(microtime())
                        ), array(
                        '%d',
                        '%s',
                        '%d',
                        '%s'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }
            } elseif ( isset($attendee->userid) && !is_nan($attendee->userid) ) {

                $email = get_userdata($attendee->userid)->user_email;

                $query = $wpdb->insert(
                        $wpdb->prefix . 'stec_attendance', array(
                        'eventid'      => $event_id,
                        'email'        => $email,
                        'userid'       => $attendee->userid,
                        'status'       => 0,
                        'access_token' => md5(microtime())
                        ), array(
                        '%d',
                        '%s',
                        '%s',
                        '%d',
                        '%s'
                        )
                );

                if ( $query === false ) {
                    $wpdb->print_error();
                    return false;
                }
            }

        endforeach;

        self::send_mail_invites($event_id);

        return true;
    }



    /**
     * @return bool
     */
    private static function _update_event_guests($event_id, $guests_arr) {

        global $wpdb;

        // delete current
        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_guests', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        if ( !$guests_arr ) {
            return true;
        }

        foreach ( $guests_arr as $guest ) :

            $guest = (object) $guest;

            if ( $guest->name == "" ) {
                continue;
            }

            $social = array();

            if ( isset($guest->social) ) :
                foreach ( $guest->social as $soc ) :

                    $soc = (object) $soc;

                    if ( $soc->link == "" || $soc->ico == "" ) {
                        continue;
                    }

                    $social[] = $soc->ico . '::' . $soc->link;
                endforeach;
            endif;

            $social = implode('||', $social);

            $query = $wpdb->insert(
                    $wpdb->prefix . 'stec_guests', array(
                    'eventid' => $event_id,
                    'photo'   => isset($guest->photo) ? $guest->photo : null,
                    'name'    => $guest->name,
                    'about'   => isset($guest->about) ? $guest->about : null,
                    'links'   => $social,
                    ), array(
                    '%d',
                    '%s',
                    '%s',
                    '%s',
                    '%s'
                    )
            );

            if ( $query === false ) {
                $wpdb->print_error();
                return false;
            }


        endforeach;

        return true;
    }



    /**
     * @return bool
     */
    private static function _update_event_schedule($event_id, $schedule_arr) {

        global $wpdb;

        // delete current
        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_schedule', array(
                'eventid' => $event_id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        if ( !$schedule_arr ) {
            return true;
        }

        foreach ( $schedule_arr as $schedule ) :

            $schedule = (object) $schedule;

            if ( !isset($schedule->schedule_date_from) || $schedule->schedule_date_from == '' ) {
                continue;
            }

            $schedule_start_date = $schedule->schedule_date_from . ' ' . $schedule->schedule_time_hours_from . ':' . $schedule->schedule_time_minutes_from . ':00';

            $query = $wpdb->insert(
                    $wpdb->prefix . 'stec_schedule', array(
                    'eventid'     => $event_id,
                    'start_date'  => $schedule_start_date,
                    'title'       => $schedule->schedule_title,
                    'icon'        => $schedule->schedule_icon,
                    'icon_color'  => $schedule->schedule_icon_color,
                    'description' => $schedule->schedule_details,
                    ), array(
                    '%d',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s'
                    )
            );

            if ( $query === false ) {
                $wpdb->print_error();
                return false;
            }

        endforeach;

        return true;
    }

    /* Get relevant event $_POST data */



    public static function calendar_post_data() {

        $calendar_id = self::post('calendar_id');
        $calendar    = self::calendar($calendar_id);

        $event_data                     = new stec_event_data();
        $event_data->calendar           = $calendar;
        $event_data->calid              = self::post('calid', false, FILTER_VALIDATE_INT);
        $event_data->summary            = self::post('summary');
        $event_data->alias              = self::post('alias');
        $event_data->color              = self::post('event_color');
        $event_data->icon               = self::post('icon');
        $event_data->visibility         = self::post('visibility');
        $event_data->featured           = self::post('featured');
        $event_data->start_date         = self::post('start_date');
        $event_data->start_time_hours   = self::post('start_time_hours');
        $event_data->start_time_minutes = self::post('start_time_minutes');
        $event_data->end_date           = self::post('end_date');
        $event_data->end_time_hours     = self::post('end_time_hours');
        $event_data->end_time_minutes   = self::post('end_time_minutes');
        $event_data->all_day            = self::post('all_day', false) !== false ? 1 : 0;
        $event_data->keywords           = self::post('keywords');
        $event_data->rrule              = self::post('rrule', '');
        $event_data->exdate             = self::post('exdate', '');
        $event_data->is_advanced_rrule  = self::post('is_advanced_rrule', 0);
        $event_data->counter            = self::post('counter');
        $event_data->comments           = self::post('comments');
        $event_data->link               = self::post('link');
        $event_data->approved           = self::post('approved', 0);
        $event_data->location           = self::post('location', '');
        $event_data->location_details   = self::post('location_details', '');
        $event_data->location_forecast  = self::post('location_forecast', '');
        $event_data->location_use_coord = self::post('location_use_coord', false) !== false ? 1 : 0;
        $event_data->images             = self::post('images', false, FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);
        $event_data->description        = self::post('description');
        $event_data->description_short  = self::post('description_short');
        $event_data->schedule           = self::post('schedule', false, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $event_data->guests             = self::post('guests', false, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $event_data->attendee           = self::post('attendee', false, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $event_data->attachments        = self::post('attachment', false, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $event_data->wc_product         = self::post('wc_product', false, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        return $event_data;
    }



    /**
     * Delete bulk events
     * @return bool
     */
    public static function delete_bulk_events($events) {

        global $wpdb;

        $wpdb->show_errors();

        $events = filter_var_array($events, FILTER_VALIDATE_INT);

        $events = array_filter($events);

        if ( !$events || empty($events) ) {
            return false;
        }

        $ids_list = implode(',', $events);

        $query = $wpdb->query(
                "
                DELETE events, meta, schedule, guests, attendance, reminder FROM {$wpdb->prefix}stec_events as events
                        
                LEFT JOIN {$wpdb->prefix}stec_events_repeater as repeater ON 
                events.id = repeater.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_events_meta as meta ON 
                events.id = meta.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_schedule as schedule ON 
                events.id = schedule.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_guests as guests ON 
                events.id = guests.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_attendance as attendance ON 
                events.id = attendance.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_attachments as attachments ON 
                events.id = attachments.eventid
                        
                LEFT JOIN {$wpdb->prefix}stec_reminder as reminder ON 
                events.id = reminder.eventid
                        
                WHERE 
             
                events.id IN ( $ids_list )
            "
        );

        if ( $query === false ) {
            return false;
        }


        return true;
    }



    /**
     * Delete bulk calendars
     * @return bool
     */
    public static function delete_bulk_calendars($calendars) {

        global $wpdb;

        $wpdb->show_errors();

        $calendars = filter_var_array($calendars, FILTER_VALIDATE_INT);

        $calendars = array_filter($calendars);

        if ( !$calendars || empty($calendars) ) {
            return false;
        }

        $ids_list = implode(',', $calendars);

        $query = $wpdb->query(
                "
                DELETE calendars FROM {$wpdb->prefix}stec_calendars as calendars
                        
                WHERE 
             
                calendars.id IN ( $ids_list )
            "
        );

        if ( $query === false ) {
            $wpdb->print_error();
            return false;
        }

        $events = $wpdb->get_results(
                "
                SELECT `id` FROM {$wpdb->prefix}stec_events
                        
                WHERE 
             
                calid IN ( $ids_list )
            ", OBJECT
        );

        if ( $events !== false ) {
            foreach ( $events as $event ) {

                self::delete_event($event->id);
            }
        }

        $import_cronjobs = $wpdb->get_results(
                "
                SELECT `id` FROM {$wpdb->prefix}stec_import
                WHERE calid IN ( $ids_list )
            ", OBJECT
        );

        if ( $import_cronjobs !== false ) {
            foreach ( $import_cronjobs as $job ) {
                self::delete_import_cronjob($job->id);
            }
        }

        return true;
    }



    /**
     * Event icons
     * @return arr
     */
    public static function icon_list() {

        $stachethemes_ec = stachethemes_ec_main::get_instance();

        $arr = array();

        $arr['fa']                                        = $stachethemes_ec->lang("No Icon");
        $arr['fa fa-paw']                                 = $stachethemes_ec->lang("Animals");
        $arr['fa fa-plane']                               = $stachethemes_ec->lang("Airport");
        $arr['fa fa-glass']                               = $stachethemes_ec->lang("Bar");
        $arr['fa fa-birthday-cake']                       = $stachethemes_ec->lang("Birthday Party");
        $arr['fa fa-briefcase']                           = $stachethemes_ec->lang("Business");
        $arr['fa fa-bus']                                 = $stachethemes_ec->lang("Bus tour");
        $arr['fa fa-motorcycle']                          = $stachethemes_ec->lang("Bike");
        $arr['fa fa-bomb']                                = $stachethemes_ec->lang("Bomb");
        $arr['fa fa-tree']                                = $stachethemes_ec->lang("Camping ");
        $arr['fa fa-ship']                                = $stachethemes_ec->lang("Cruise");
        $arr['fa fa-coffee']                              = $stachethemes_ec->lang("Coffee");
        $arr['fa fa-bicycle']                             = $stachethemes_ec->lang("Cycling");
        $arr['fa fa-balance-scale']                       = $stachethemes_ec->lang("Court Hall");
        $arr['fa fa-futbol-o']                            = $stachethemes_ec->lang("Football");
        $arr['fa fa-gamepad']                             = $stachethemes_ec->lang("Gaming");
        $arr['fa fa-graduation-cap']                      = $stachethemes_ec->lang("Graduation");
        $arr['fa fa-suitcase']                            = $stachethemes_ec->lang("Hotel");
        $arr['fa fa-microphone']                          = $stachethemes_ec->lang("Karaoke");
        $arr['fa fa-book']                                = $stachethemes_ec->lang("Library ");
        $arr['fa fa-film']                                = $stachethemes_ec->lang("Movie");
        $arr['fa fa-university']                          = $stachethemes_ec->lang("Museum");
        $arr['fa fa-music']                               = $stachethemes_ec->lang("Music");
        $arr['fa fa-camera']                              = $stachethemes_ec->lang("Photo");
        $arr['fa fa-picture-o']                           = $stachethemes_ec->lang("Picture/Painting");
        $arr['fa fa-cutlery']                             = $stachethemes_ec->lang("Restaurant");
        $arr['fa fa-flag-checkered']                      = $stachethemes_ec->lang("Race");
        $arr['fa fa-shopping-basket']                     = $stachethemes_ec->lang("Shopping");
        $arr['fa fa-magic']                               = $stachethemes_ec->lang("Show");
        $arr['fa fa-beer']                                = $stachethemes_ec->lang("Tavern");
        $arr['fa fa-comments']                            = $stachethemes_ec->lang("Talks");
        $arr['fa fa-ticket']                              = $stachethemes_ec->lang("Ticket");
        $arr['fa fa-train']                               = $stachethemes_ec->lang("Train");
        $arr['fa fa-diamond']                             = $stachethemes_ec->lang("Wedding");
        $arr['fa fa-wheelchair']                          = $stachethemes_ec->lang("Wheelchair");
        $arr['fa fa-binoculars']                          = $stachethemes_ec->lang("Binocular");
        $arr['fa fa-bed']                                 = $stachethemes_ec->lang("Bed");
        $arr['fa fa-bolt']                                = $stachethemes_ec->lang("Bolt");
        $arr['fa fa-check']                               = $stachethemes_ec->lang("Check Sign");
        $arr['fa fa-comment-o']                           = $stachethemes_ec->lang("Conversation");
        $arr['fa fa-credit-card']                         = $stachethemes_ec->lang("Credit Card");
        $arr['fa fa-diamond']                             = $stachethemes_ec->lang("Diamond");
        $arr['fa fa-envelope-o']                          = $stachethemes_ec->lang("Envelope");
        $arr['fa fa-female']                              = $stachethemes_ec->lang("Female Silhouette");
        $arr['fa fa-male']                                = $stachethemes_ec->lang("Male Silhouette");
        $arr['fa fa-child']                               = $stachethemes_ec->lang("Child Silhouette");
        $arr['fa fa-fire-extinguisher']                   = $stachethemes_ec->lang("Fire Extinguisher");
        $arr['fa fa-hand-peace-o']                        = $stachethemes_ec->lang("Hand Peace Sign");
        $arr['fa fa-hand-o-up']                           = $stachethemes_ec->lang("Hand Point Up");
        $arr['fa fa-heart-o']                             = $stachethemes_ec->lang("Heart");
        $arr['fa fa-key']                                 = $stachethemes_ec->lang("Key");
        $arr['fa fa-gavel']                               = $stachethemes_ec->lang("Gravel");
        $arr['fa fa-life-ring']                           = $stachethemes_ec->lang("Life Ring");
        $arr['fa fa-lightbulb-o']                         = $stachethemes_ec->lang("Lightbulb");
        $arr['fa fa-money']                               = $stachethemes_ec->lang("Money");
        $arr['fa fa-moon-o']                              = $stachethemes_ec->lang("Moon");
        $arr['fa fa-shopping-bag']                        = $stachethemes_ec->lang("Shopping Bag");
        $arr['fa fa-video-camera']                        = $stachethemes_ec->lang("Video Camera");
        $arr['fa fa-ambulance']                           = $stachethemes_ec->lang("Ambulance");
        $arr['fa fa-paypal']                              = $stachethemes_ec->lang("PayPal");
        $arr['fa fa-google-wallet']                       = $stachethemes_ec->lang("Google Wallet");
        $arr['fa fa-usd']                                 = $stachethemes_ec->lang("USD Sign");
        $arr['fa fa-btc']                                 = $stachethemes_ec->lang("Bitcoin Sign");
        $arr['fa fa-gbp']                                 = $stachethemes_ec->lang("British Pound Sign");
        $arr['fa fa-eur']                                 = $stachethemes_ec->lang("Euro Sign");
        $arr['fa fa-apple']                               = $stachethemes_ec->lang("Apple Logo");
        $arr['fa fa-amazon']                              = $stachethemes_ec->lang("Amazon Logo");
        $arr['fa fa-android']                             = $stachethemes_ec->lang("Android Logo");
        $arr['fa fa-instagram']                           = $stachethemes_ec->lang("Instagram Logo");
        $arr['fa fa-facebook']                            = $stachethemes_ec->lang("Facebook Logo");
        $arr['fa fa-google-plus']                         = $stachethemes_ec->lang("Google Plus Logo");
        $arr['fa fa-steam']                               = $stachethemes_ec->lang("Steam Logo");
        $arr['fa fa-twitch']                              = $stachethemes_ec->lang("Twitch Logo");
        $arr['fa fa-yelp']                                = $stachethemes_ec->lang("Yelp Logo");
        $arr['fa fa-twitter']                             = $stachethemes_ec->lang("Twitter Logo");
        $arr['fa fa-youtube']                             = $stachethemes_ec->lang("YouTube Logo");
        $arr['fa fa-stethoscope']                         = $stachethemes_ec->lang("Stethoscope");
        $arr['fa fa-heartbeat']                           = $stachethemes_ec->lang("Heartbeat");
        $arr['fa fa-user-md']                             = $stachethemes_ec->lang("Doctor");
        $arr['fa fa-hospital-o']                          = $stachethemes_ec->lang("Hospital");
        $arr['fa fa-medkit']                              = $stachethemes_ec->lang("Medkit");
        $arr['fa fa-scissors']                            = $stachethemes_ec->lang("Scissors");
        $arr['fa fa-pie-chart']                           = $stachethemes_ec->lang("Pie Chart");
        $arr['fa fa-transgender']                         = $stachethemes_ec->lang("Transgender Symbol");
        $arr['fa fa-mars']                                = $stachethemes_ec->lang("Male Sex Symbol");
        $arr['fa fa-venus']                               = $stachethemes_ec->lang("Female Sex Symbol");
        $arr['fa fa-blind']                               = $stachethemes_ec->lang("Blind Men Symbol");
        $arr['fa fa-american-sign-language-interpreting'] = $stachethemes_ec->lang("Sign Language Symbol");
        $arr['fa fa-newspaper-o']                         = $stachethemes_ec->lang("Newspaper");
        $arr['fa fa-flag']                                = $stachethemes_ec->lang("Flag");
        $arr['fa fa-hashtag']                             = $stachethemes_ec->lang("Hashtag");
        $arr['fa fa-snapchat-ghost']                      = $stachethemes_ec->lang("Snapchat Logo");
        $arr['fa fa-subway']                              = $stachethemes_ec->lang("Subway");
        $arr['fa fa-share-alt']                           = $stachethemes_ec->lang("Share Icon");
        $arr['fa fa-sign-language']                       = $stachethemes_ec->lang("Sign Language Icon");
        $arr['fa fa-odnoklassniki']                       = $stachethemes_ec->lang("Odnoklassniki Logo");
        $arr['fa fa-language']                            = $stachethemes_ec->lang("Multi-language Icon");
        $arr['fa fa-book']                                = $stachethemes_ec->lang("Book Icon");
        $arr['fa fa-puzzle-piece']                        = $stachethemes_ec->lang("Puzzle Piece");
        $arr['fa fa-television']                          = $stachethemes_ec->lang("Television");
        $arr['fa fa-desktop']                             = $stachethemes_ec->lang("Desktop");
        $arr['fa fa-calculator']                          = $stachethemes_ec->lang("Calculator");
        $arr['fa fa-cubes']                               = $stachethemes_ec->lang("Cubes");

        return apply_filters('stec-icons', $arr);
    }



    public static function send_mail_invites($event_id) {

        if ( !$event_id || is_nan($event_id) ) {
            return false;
        }

        global $wpdb;

        $query = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_attendance"
                . " WHERE `eventid` = {$event_id}"
                . " AND `email` IS NOT NULL "
                . " AND `access_token` IS NOT NULL "
                . " AND `mail_sent` = '0' "
                . "", OBJECT);

        if ( $query === false ) {
            return false;
        }

        $event    = self::event($event_id);
        $site_url = site_url();

        foreach ( $query as $attendee ) {

            $accept_url  = $site_url . "?stec_attendance={$attendee->id}&access_token={$attendee->access_token}&status=1";
            $decline_url = $site_url . "?stec_attendance={$attendee->id}&access_token={$attendee->access_token}&status=2";

            $to = $attendee->email;

            $subject = self::get_admin_setting_value('stec_menu__general_email_invitation', 'email_invite_subject');
            $content = self::get_admin_setting_value('stec_menu__general_email_invitation', 'email_invite_content');

            $subject = str_replace('stec_replace_event_summary', $event->summary, $subject);

            $content = str_replace('stec_replace_event_summary', $event->summary, $content);
            $content = str_replace('stec_replace_accept_url', $accept_url, $content);
            $content = str_replace('stec_replace_decline_url', $decline_url, $content);

            $sent = wp_mail($to, $subject, $content);

            if ( !$sent ) {
                return false;
            } else {
                $wpdb->get_results("UPDATE {$wpdb->prefix}stec_attendance SET `mail_sent` = '1' WHERE `id` = '{$attendee->id}' ");
            }
        }
    }



    public static function update_attendance_by_url($attendee_id, $access_token, $status) {

        if ( is_nan($attendee_id) || $access_token == '' || is_nan($status) ) {
            return false;
        }

        if ( $status < 0 || $status > 2 ) {
            return false;
        }

        global $wpdb;

        $query = $wpdb->get_results("UPDATE {$wpdb->prefix}stec_attendance SET "
                . "`status` = '{$status}' WHERE `id` = '$attendee_id' "
                . " AND `access_token` = '{$access_token}' ");

        return $query === false ? false : true;
    }



    /**
     * Reminder cronjob fn
     */
    public static function cronjobs_reminder() {

        global $wpdb;

        $query = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}stec_reminder", OBJECT);

        if ( $query === false ) {
            return false;
        }

        foreach ( $query as $remind ) {

            $event = self::event($remind->eventid);

            if ( !$event ) {
                return false;
            }

            $calendar = self::calendar($event->calid);

            if ( !$calendar ) {
                return false;
            }

            $gm_now         = gmdate(time());
            $gm_remind_date = strtotime(self::to_utc_time($remind->date, $calendar->timezone));

            if ( $gm_now < $gm_remind_date ) {
                continue;
            }

            if ( !$event ) {
                // event does not exists
                $wpdb->get_results("DELETE FROM {$wpdb->prefix}stec_reminder WHERE `id` = {$remind->id}", OBJECT);
                continue;
            }

            $event_start_date = date("Y-m-d H:i:s", strtotime($event->start_date) + $remind->repeat_offset);
            $event_end_date   = date("Y-m-d H:i:s", strtotime($event->end_date) + $remind->repeat_offset);
            $site_url         = get_site_url();

            $subject = self::get_admin_setting_value('stec_menu__general_email_reminder', 'email_reminder_subject');
            $content = self::get_admin_setting_value('stec_menu__general_email_reminder', 'email_reminder_content');

            $subject = str_replace('stec_replace_event_summary', $event->summary, $subject);
            $subject = str_replace('stec_replace_event_start', $event_start_date, $subject);
            $subject = str_replace('stec_replace_event_end', $event_end_date, $subject);
            $subject = str_replace('stec_replace_siteurl', $site_url, $subject);

            $content = str_replace('stec_replace_event_summary', $event->summary, $content);
            $content = str_replace('stec_replace_event_start', $event_start_date, $content);
            $content = str_replace('stec_replace_event_end', $event_end_date, $content);
            $content = str_replace('stec_replace_siteurl', $site_url, $content);

            $sent = wp_mail($remind->email, $subject, $content);

            if ( $sent ) {
                $wpdb->query("DELETE FROM {$wpdb->prefix}stec_reminder WHERE `id` = {$remind->id}");
            }
        }
    }



    public static function timezones_list() {

        $zones_array = array();
        $timestamp   = time();

        foreach ( timezone_identifiers_list() as $zone ) {
            date_default_timezone_set($zone);
            $zones_array[$zone] = $zone . ' (' . 'UTC/GMT ' . date('P', $timestamp) . ')';
        }

        return $zones_array;
    }



    /**
     * Get admin setting array by $page and or $name
     * @param string $page 
     * @param string $name
     * @return array
     * Inherits from abstract.template.main
     */
    public static function get_admin_setting($page, $name = false) {
        $stachethemes_ec_main = stachethemes_ec_main::get_instance();
        return $stachethemes_ec_main->get_admin_setting($page, $name);
    }



    /**
     * Get admin setting value by $page and or $name
     * @param string $page 
     * @param string $name
     * @return mixed
     * Inherits from abstract.template.main 
     */
    public static function get_admin_setting_value($page, $name) {
        $stachethemes_ec_main = stachethemes_ec_main::get_instance();
        return $stachethemes_ec_main->get_admin_setting_value($page, $name);
    }



    /**
     * Build html settings from given $page and $name settings
     */
    public static function build_admin_setting($page, $name = false) {

        $setting = self::get_admin_setting($page, $name);

        if ( $setting === null || empty($setting) ) {
            return;
        }

        if ( $name === false ) {
            foreach ( $setting as $single ) {
                self::_setting_to_html($single);
            }
        } else {
            self::_setting_to_html($setting);
        }
    }



    private static function _setting_to_html($setting) {

        $setting = (object) $setting;

        switch ( $setting->type ) :

            case 'checkbox' :

                self::html_checkbox($setting->name, $setting->value, $setting->default, $setting->title, $setting->req);

                break;

            case 'input' :

                self::html_info($setting->title);
                self::html_input($setting->name, $setting->value, $setting->default, "", $setting->req);

                break;

            case 'textarea' :

                self::html_info($setting->title);
                self::html_textarea($setting->name, $setting->value, $setting->default, $placeholder = "", $setting->req);

                break;

            case 'select' :

                self::html_info($setting->title);
                self::html_select($setting->name, $setting->select, $setting->value, $setting->req);

                break;

            case 'color' :

                self::html_info($setting->title);
                self::html_color($setting->name, $setting->value, $setting->default, $setting->desc);

                break;

            case 'font' :

                $fontsizes = array();
                for ( $i = 9; $i <= 100; $i++ ) {
                    $fontsizes["{$i}px"] = "{$i}px";
                }

                $fontweights = array();
                for ( $i = 1; $i <= 9; $i++ ) {
                    $fontweights[$i * 100] = $i * 100;
                }

                $fontlineheights = array();
                $lh              = 0.9;
                while ( $lh < 2 ) {
                    $lh                     = $lh + 0.1;
                    $fontlineheights["$lh"] = $lh;
                }

                self::html_info($setting->title);
                echo '<div class="stachethemes-admin-section-flex">';
                self::html_input($setting->name . '_font_family', $setting->value[0], $setting->default[0], "Font-Family", false);
                self::html_select($setting->name . '_font_weight', $fontweights, $setting->value[1], $setting->req);
                self::html_select($setting->name . '_font_size', $fontsizes, $setting->value[2], $setting->req);
                if ( isset($setting->value[3]) && $setting->value[3] != "" ) {
                    self::html_select($setting->name . '_font_lineheight', $fontlineheights, $setting->value[3], $setting->req);
                }
                echo '</div>';
                break;

        endswitch;
    }



    /**
     * Updates all settings from this $page 
     * @see register-settings.php
     */
    public static function update_admin_settings($page) {

        $settings = self::get_admin_setting($page);

        foreach ( $settings as $k => $s ) :

            $s = (object) $s;

            switch ( $s->type ) :

                case 'font' :

                    $settings[$k]["value"] = array(
                            self::post($s->name . '_font_family', ''),
                            self::post($s->name . '_font_weight', ''),
                            self::post($s->name . '_font_size', ''),
                            self::post($s->name . '_font_lineheight', false)
                    );

                    break;

                case 'checkbox' :

                    $settings[$k]["value"] = self::post($s->name) ? 1 : null;

                    break;

                default:

                    $settings[$k]["value"] = self::post($s->name, '');

            endswitch;

        endforeach;

        update_option($page, $settings);
    }



    /**
     * Resets admin settings from this $page
     * @see register-settings.php
     */
    public static function reset_admin_settings($page) {

        $settings = self::get_admin_setting($page);

        foreach ( $settings as $k => $s ) :

            $s = (object) $s;

            switch ( $s->type ) :

                case 'font' :

                    $settings[$k]["value"] = array(
                            $s->default[0],
                            $s->default[1],
                            $s->default[2],
                            isset($s->default[3]) && $s->default[3] != "" ? $s->default[3] : false
                    );

                    break;

                default:

                    $settings[$k]["value"] = $s->default;

            endswitch;

        endforeach;

        update_option($page, $settings);
    }



    public static function wc() {
        if ( self::$_wc === false ) {
            self::$_wc = new stachethemes_ec_wc();
        }
        return self::$_wc;
    }



    public static function get_user_roles_list() {

        $roles = array_reverse(get_editable_roles());

        $arr = array();

        foreach ( $roles as $role => $details ) {

            $name = translate_user_role($details['name']);

            $arr[esc_attr($role)] = $name;
        }

        return $arr;
    }



    public static function calendar_writable_list() {
        $arr                   = self::get_user_roles_list();
        $arr['stec_private']   = 'Just Me';
        $arr['stec_logged_in'] = 'Logged in users';
        $arr['stec_public']    = 'Anyone';
        $arr['0']              = 'None';
        return array_reverse($arr);
    }



    public static function calendar_visibility_list() {
        $arr                   = self::get_user_roles_list();
        $arr['stec_private']   = 'Just Me';
        $arr['stec_logged_in'] = 'Logged in users';
        $arr['stec_public']    = 'Public';
        return array_reverse($arr);
    }



    public static function event_visibility_list() {
        $arr                     = self::get_user_roles_list();
        $arr['stec_private']     = 'Just Me';
        $arr['stec_logged_in']   = 'Logged in users';
        $arr['stec_public']      = 'Public';
        $arr['stec_cal_default'] = 'Calendar Default';
        return array_reverse($arr);
    }



    public static function import_ics($ics_url = false, $calendar_id = false, $recurrence = false, $icon = false, $ignore_expired = false, $overwrite_events = false, $delete_removed = false) {

        if ( $ics_url === false || is_nan($calendar_id) ) {
            return false;
        }

        set_time_limit(self::$time_limit);

        global $wpdb;

        $calendar = self::calendar($calendar_id);
        $ical     = new stachethemes_ec_ical_import($ics_url, $recurrence);
        $events   = $ical->events();

        $result            = true;
        $imported_count    = 0;
        $overwritten_count = 0;
        $ics_uids          = array();

        foreach ( $events as $event ) {

            $ics_uids[] = $event->uid;

            if ( !isset($event->summary) || $event->summary == '' ) {
                /**
                 * Don't import unnamed events
                 */
                continue;
            }

            $start_timestamp = $event->dtstart_array[2];
            $end_timestamp   = $event->dtend_array[2];
            $ical->setTheDateTz($calendar->timezone);

            if ( isset($event->dtstart_array[0]['VALUE']) ) {
                switch ( $event->dtstart_array[0]['VALUE'] ) {
                    case 'DATE' :
                        /**
                         * If VALUE is DATE the timestamp will be zeroed to UTC time (12:00AM) 
                         * It is not adjusted to the timetable timezone
                         * Adjust to timetable timezone
                         */
                        $calendar_date            = new DateTime('now', new DateTimeZone($calendar->timezone));
                        $calendar_datetime_offset = $calendar_date->getOffset();
                        $start_timestamp          -= $calendar_datetime_offset;
                        $end_timestamp            -= $calendar_datetime_offset;
                        break;
                }
            }

            $all_day = 0;

            if ( $end_timestamp - $start_timestamp === 86400 ) {
                /**
                 * Suggest whole day event and subtract 1 second
                 */
                $end_timestamp -= 1;
                $all_day       = 1;
            }

            // check expired filter
            if ( $ignore_expired === true ) {

                if ( gmdate('U', time()) > $end_timestamp && (!isset($event->rrule) || $event->rrule == '') ) {
                    continue;
                }
            }

            if ( !isset($event->recurrence_id) ) {
                $event->recurrence_id = '';
            }


            // check if uid exists in THIS ID calendar
            // allow recurrence-id override
            $exists = $wpdb->get_row("SELECT EVENT.id FROM {$wpdb->prefix}stec_events_meta as META "
                    . "LEFT JOIN {$wpdb->prefix}stec_events as EVENT ON "
                    . "EVENT.id = META.eventid "
                    . "WHERE META.uid = '{$event->uid}' AND EVENT.calid = {$calendar_id} "
                    . "AND META.recurrence_id = '{$event->recurrence_id}' ");

            if ( $exists !== false && $wpdb->num_rows > 0 ) {

                if ( true === $overwrite_events ) {

                    if ( isset($exists->id) ) {
                        if ( true === self::delete_event($exists->id) ) {
                            $overwritten_count++;
                        }
                    }
                } else {
                    // Event uid already exists; Don't add again
                    continue;
                }
            }

            $visibility = 'stec_cal_default';

            if ( isset($event->class) ) :

                if ( $event->class == 'PUBLIC' ) {
                    $visibility = 'stec_public';
                }

                if ( $event->class == 'PRIVATE' ) {
                    $visibility = 'stec_private';
                }

            endif;

            $event_data = new stec_event_data();

            $event_data->calendar           = $calendar;
            $event_data->summary            = stripslashes($event->summary);
            $event_data->alias              = self::validate_event_alias(sanitize_title($event->summary));
            $event_data->color              = $event_data->calendar->color;
            $event_data->icon               = $icon ? $icon : '';
            $event_data->visibility         = $visibility;
            $event_data->featured           = 0;
            $event_data->start_date         = $ical->theDate('Y-m-d', $start_timestamp);
            $event_data->start_time_hours   = $ical->theDate('H', $start_timestamp);
            $event_data->start_time_minutes = $ical->theDate('i', $start_timestamp);
            $event_data->end_date           = $ical->theDate('Y-m-d', $end_timestamp);
            $event_data->end_time_hours     = $ical->theDate('H', $end_timestamp);
            $event_data->end_time_minutes   = $ical->theDate('i', $end_timestamp);
            $event_data->all_day            = $all_day;
            $event_data->rrule              = isset($event->rrule) ? $event->rrule : '';
            $event_data->exdate             = isset($event->exdate) ? $event->exdate : '';
            $event_data->is_advanced_rrule  = isset($event->is_advanced_rrule) ? $event->is_advanced_rrule : 0;
            $event_data->keywords           = '';
            $event_data->counter            = 0;
            $event_data->comments           = 0;
            $event_data->link               = "";
            $event_data->approved           = 1;

            $event_data->location           = isset($event->location) ? $event->location : '';
            $event_data->location_details   = '';
            $event_data->location_forecast  = '';
            $event_data->location_use_coord = '0';
            $event_data->images             = false;
            $event_data->description        = isset($event->description) ? str_replace("\\n", "\n", $event->description) : '';
            $event_data->description_short  = '';
            $event_data->schedule           = false;
            $event_data->guests             = false;
            $event_data->attendee           = false;
            $event_data->attachments        = false;
            $event_data->wc_product         = false;
            $event_data->uid                = $event->uid;
            $event_data->recurrence_id      = $event->recurrence_id;

            $result = self::create_event($event_data);

            if ( $result === false ) {
                return false;
            }

            $imported_count++;
        }

        // Remove events with uid NOT in the ics and in this calendar 
        $deleted_count = 0;
        if ( $delete_removed === true ) :

            $calendar_events = self::events($calendar_id, true);

            foreach ( $calendar_events as $event ) {

                if ( in_array($event->uid, $ics_uids) !== true ) {
                    self::delete_event($event->id);
                    $deleted_count++;
                }
            }
        endif;

        return (object) array(
                        'deleted_count'     => $deleted_count,
                        'overwritten_count' => $overwritten_count,
                        'imported_count'    => $imported_count
        );
    }



    public static function get_import_cronjobs($calendar_id) {

        global $wpdb;

        $calendar_id = (int) $calendar_id;

        $query = $wpdb->get_results(" 
            
                   SELECT cal.color, import.* FROM {$wpdb->prefix}stec_import as import 

                   LEFT JOIN {$wpdb->prefix}stec_calendars as cal
                       
                   ON cal.id = import.calid

                   WHERE import.calid = {$calendar_id}
                
                ", OBJECT);

        return $query !== false ? $query : array();
    }



    public static function add_import_cronjob($calendar_id, $url, $icon, $freq, $recurrent, $ignore_expired, $overwrite_events, $delete_removed) {

        global $wpdb;

        $query = $wpdb->insert(
                $wpdb->prefix . 'stec_import', array(
                'calid'            => $calendar_id,
                'url'              => $url,
                'icon'             => $icon,
                'freq'             => $freq,
                'recurrent'        => $recurrent,
                'ignore_expired'   => $ignore_expired,
                'overwrite_events' => $overwrite_events,
                'delete_removed'   => $delete_removed,
                'userid'           => get_current_user_id()
                ), array(
                '%d',
                '%s',
                '%s',
                '%d',
                '%d',
                '%d',
                '%d',
                '%d'
                )
        );

        return $query === false ? false : true;
    }



    /**
     * Delete bulk cronjobs from import table
     * @return bool
     */
    public static function delete_bulk_import_cronjobs($jobs) {

        global $wpdb;

        $wpdb->show_errors();

        $jobs = filter_var_array($jobs, FILTER_VALIDATE_INT);

        $jobs = array_filter($jobs);

        if ( !$jobs || empty($jobs) ) {
            return false;
        }

        $ids_list = implode(',', $jobs);

        $query = $wpdb->query(
                "
                DELETE FROM {$wpdb->prefix}stec_import 
                        
                WHERE 
             
                id IN ( $ids_list )
            "
        );

        if ( $query === false ) {
            return false;
        }


        return true;
    }



    /**
     * Delete job from stec_import table
     * @return bool
     */
    public static function delete_import_cronjob($id) {

        global $wpdb;

        $query = $wpdb->delete(
                $wpdb->prefix . 'stec_import', array(
                'id' => $id
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        return true;
    }



    /**
     * Importer cronjob fn
     */
    public static function cronjobs_import($freq) {

        global $wpdb;

        $query = $wpdb->get_results(" 
            
                   SELECT cal.color, import.* FROM {$wpdb->prefix}stec_import as import 

                   LEFT JOIN {$wpdb->prefix}stec_calendars as cal
                       
                   ON cal.id = import.calid
                       
                   AND import.freq = $freq
                
                ", OBJECT);

        if ( $query === false ) {
            return false;
        }

        set_time_limit(self::$time_limit);

        foreach ( $query as $job ) :

            if ( filter_var($job->url, FILTER_VALIDATE_URL) === false ) {
                return false;
            }

            $url = str_replace('webcal://', 'http://', $job->url);

            if ( defined('DOING_CRON') ) {
                wp_set_current_user($job->userid);
            }

            $result = self::import_ics(
                            $url, $job->calid, false, $job->icon, $job->ignore_expired == '1' ? true : false, $job->overwrite_events == '1' ? true : false, $job->delete_removed == '1' ? true : false);

            if ( !$result ) {
                return false;
            }

        endforeach;
    }



    public static function export_ics($calendar_id, $event_id = false, $idlist = false) {

        if ( !is_user_logged_in() ) {
            return false;
        }

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        if ( is_nan($calendar_id) ) {
            return false;
        }

        $calendar = self::calendar($calendar_id);
        $export   = new stachethemes_ec_ical_export($calendar);
        $events   = array();
        $filename = false;

        // include event by single id
        if ( $event_id !== false ) :

            $event    = self::event($event_id);
            $events[] = $event;

            $filename = $event->summary;

        endif;

        // include selected events from idlist array
        if ( $idlist && is_array($idlist) ) :
            foreach ( $idlist as $event_id ) {
                $events[] = self::event($event_id);
            }

        endif;

        if ( $event_id === false && $idlist === false ) {
            // Export all events from calendar id
            $events = self::events($calendar_id);
        }

        if ( empty($events) ) {

            self::set_message($stachethemes_ec_main->lang('No events to export'), 'error');

            return true;
        }

        foreach ( $events as $event ) :

            if ( empty($event) ) {
                continue;
            }

            $export->proccess_event($event);

        endforeach;

        $export->download($filename);
    }



    private static function _get_columns($table, $substitude = array()) {

        global $wpdb;

        $coldata = $wpdb->get_results("SHOW COLUMNS FROM $table", OBJECT);

        $columns = array();

        foreach ( $coldata as $col ) {

            if ( $col->Field == 'id' ) {
                continue;
            }

            foreach ( $substitude as $key => $value ) {

                if ( $col->Field == $key ) {
                    $col->Field = $value;
                }
            }

            $columns[] = $col->Field;
        }

        return implode(',', $columns);
    }



    public static function duplicate_event($event_id) {

        global $wpdb;

        // duplicate STEC_EVENTS 

        $event = self::event($event_id);

        $summary = esc_sql($event->summary . ' - DUPLICATE -');

        $events_columns     = self::_get_columns($wpdb->prefix . "stec_events");
        $events_columns_new = self::_get_columns($wpdb->prefix . "stec_events", array(
                        'summary' => '"' . $summary . '"',
                        'alias'   => '"' . self::validate_event_alias($event->alias) . '"',
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_events 
                    ($events_columns) SELECT $events_columns_new FROM {$wpdb->prefix}stec_events WHERE id = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }

        $duplicate_event_id = $wpdb->insert_id;



        // duplicate STEC_EVENTS_REPEATER
        $repeater_columns     = self::_get_columns($wpdb->prefix . "stec_events_repeater");
        $repeater_columns_new = self::_get_columns($wpdb->prefix . "stec_events_repeater", array(
                        'eventid' => $duplicate_event_id
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_events_repeater
                    ($repeater_columns) SELECT $repeater_columns_new FROM {$wpdb->prefix}stec_events_repeater WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }


        // duplicate STEC_EVENTS_META
        $meta_columns     = self::_get_columns($wpdb->prefix . "stec_events_meta");
        $meta_columns_new = self::_get_columns($wpdb->prefix . "stec_events_meta", array(
                        'eventid' => $duplicate_event_id,
                        'uid'     => '"' . uniqid('e-') . '_' . md5(microtime()) . '@stachethemes_ec.com' . '"'
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_events_meta
                    ($meta_columns) SELECT $meta_columns_new FROM {$wpdb->prefix}stec_events_meta WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }








        // duplicate STEC_GUESTS

        $guests_columns     = self::_get_columns($wpdb->prefix . "stec_guests");
        $guests_columns_new = self::_get_columns($wpdb->prefix . "stec_guests", array(
                        'eventid' => $duplicate_event_id
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_guests
                    ($guests_columns) SELECT $guests_columns_new FROM {$wpdb->prefix}stec_guests WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }








        // duplicate STEC_ATTACHMENTS

        $attachments_columns     = self::_get_columns($wpdb->prefix . "stec_attachments");
        $attachments_columns_new = self::_get_columns($wpdb->prefix . "stec_attachments", array(
                        'eventid' => $duplicate_event_id
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_attachments
                    ($attachments_columns) SELECT $attachments_columns_new FROM {$wpdb->prefix}stec_attachments WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }








        // duplicate STEC_ATTENDANCE

        $attendance_columns     = self::_get_columns($wpdb->prefix . "stec_attendance");
        $attendance_columns_new = self::_get_columns($wpdb->prefix . "stec_attendance", array(
                        'eventid' => $duplicate_event_id,
                        'status'  => '0'
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_attendance
                    ($attendance_columns) SELECT $attendance_columns_new FROM {$wpdb->prefix}stec_attendance WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }








        // duplicate STEC_SCHEDULE

        $schedule_columns     = self::_get_columns($wpdb->prefix . "stec_schedule");
        $schedule_columns_new = self::_get_columns($wpdb->prefix . "stec_schedule", array(
                        'eventid' => $duplicate_event_id
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_schedule
                    ($schedule_columns) SELECT $schedule_columns_new FROM {$wpdb->prefix}stec_schedule WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }








        // duplicate STEC_WOOCOMMERCE

        $woocommerce_columns     = self::_get_columns($wpdb->prefix . "stec_woocommerce");
        $woocommerce_columns_new = self::_get_columns($wpdb->prefix . "stec_woocommerce", array(
                        'eventid' => $duplicate_event_id
        ));

        $query = $wpdb->query("
                    INSERT INTO {$wpdb->prefix}stec_woocommerce
                    ($woocommerce_columns) SELECT $woocommerce_columns_new FROM {$wpdb->prefix}stec_woocommerce WHERE eventid = {$event_id}
               ");

        if ( $query === false ) {
            return false;
        }


        return true;
    }



    public static function approve_event($event_id) {

        global $wpdb;

        $query = $wpdb->update(
                $wpdb->prefix . 'stec_events', array(
                'approved' => 1,
                ), array(
                'id' => $event_id
                ), array(
                '%d',
                ), array(
                '%d'
                )
        );

        if ( $query === false ) {
            return false;
        }

        return true;
    }



    public static function activate_license($purchase_code, $server_name) {

        $url = "https://api.stachethemes.com/calendar/activate/{$purchase_code}/{$server_name}";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Stachethemes Activator");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec($ch);

        curl_close($ch);

        $json = json_decode($result);

        if ( isset($json->success) && $json->success === 1 ) {

            $nfo = array(
                    'purchase_code' => $purchase_code,
                    'server_name'   => $server_name
            );

            update_option('stec-activated', $nfo);

            return true;
        }

        return false;
    }



    public static function deactivate_license($purchase_code, $server_name) {

        $url = "https://api.stachethemes.com/calendar/deactivate/{$purchase_code}/{$server_name}";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Stachethemes Activator");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec($ch);

        curl_close($ch);

        $json = json_decode($result);

        if ( isset($json->success) && $json->success === 1 ) {

            $nfo = array(
                    'purchase_code' => $purchase_code,
                    'server_name'   => $server_name
            );

            delete_option('stec-activated');

            return true;
        }

        return false;
    }



    public static function validate_event_alias($alias, $id = false) {

        global $wpdb;

        $event_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$wpdb->prefix}stec_events AS events WHERE events.alias = '%s' ", $alias));

        if ( !$event_id ) {
            return $alias;
        }

        if ( $id && $event_id === $id ) {
            return $alias;
        }

        $alias = explode('-', $alias);

        $last = array_pop($alias);

        if ( is_numeric($last) ) {

            $alias = implode('-', $alias) . '-' . ($last + 1);
        } else {

            $alias = implode('-', $alias) . '-' . $last . '-1';
        }

        return self::validate_event_alias($alias);
    }



    /**
     * Returns human readable format of the timespan
     * @param object $event the event object
     * @return string the timespan
     */
    public static function get_the_timespan($event) {

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        $dateformat = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'date_format');
        $timeformat = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'time_format');

        $show_utc_offset = $stachethemes_ec_main->get_admin_setting_value('stec_menu__general', 'date_label_gmtutc');

        $start_date_unix = strtotime($event->start_date) + (isset($event->repeat_time_offset) ? $event->repeat_time_offset : 0 );
        $end_date_unix   = strtotime($event->end_date) + (isset($event->repeat_time_offset) ? $event->repeat_time_offset : 0 );

        // Translate months
        $sm = ucfirst($stachethemes_ec_main->lang(strtolower(date('M', $start_date_unix))));
        $em = ucfirst($stachethemes_ec_main->lang(strtolower(date('M', $end_date_unix))));

        switch ( $dateformat ) :

            case 'dd-mm-yy' :

                $start_date = date('d', $start_date_unix) . ' ' . $sm . ' ' . date('Y', $start_date_unix);
                $end_date   = date('d', $end_date_unix) . ' ' . $em . ' ' . date('Y', $end_date_unix);
                break;

            case 'mm-dd-yy' :
                $start_date = $sm . ' ' . date('d', $start_date_unix) . ' ' . date('Y', $start_date_unix);
                $end_date   = $em . ' ' . date('d', $end_date_unix) . ' ' . date('Y', $end_date_unix);
                break;

            case 'yy-mm-dd' :
            default:
                $start_date = date('Y', $start_date_unix) . ' ' . $sm . ' ' . date('d', $start_date_unix);
                $end_date   = date('Y', $end_date_unix) . ' ' . $em . ' ' . date('d', $end_date_unix);
                break;
        endswitch;

        switch ( $timeformat ) :
            case '12':
                $timeformat = 'h:ia';
                break;

            case '24':
                $timeformat = 'H:i';
            default:

        endswitch;

        $start_time = date($timeformat, $start_date_unix);
        $end_time   = date($timeformat, $end_date_unix);

        if ( $start_date == $end_date ) {

            if ( $event->all_day == '1' ) {
                $the_date = $start_date;
            } else {
                $the_date = "$start_date $start_time - $end_time";
            }
        } else {

            if ( $event->all_day == '1' ) {
                $the_date = "$start_date - $end_date";
            } else {
                $the_date = "$start_date $start_time - $end_date $end_time";
            }
        }

        if ( $show_utc_offset == '1' ) {

            if ( !isset($event->timezone) ) {
                $event->timezone = self::get_calendar_timezone($event->calid);
            }

            $the_date .= ' ' . $event->timezone;
        }

        return $the_date;
    }



    public static function get_calendar_timezone($calid) {

        global $wpdb;

        $query = $wpdb->get_var("SELECT timezone FROM {$wpdb->prefix}stec_calendars WHERE id='{$calid}' ");

        return $query !== false ? $query : false;
    }



    public static function delete_cache() {

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        $cache_folder = $stachethemes_ec_main->get_path('FRONT') . '/cache';

        if ( file_exists($cache_folder) && is_dir($cache_folder) ) {

            $files = glob($cache_folder . '/events/*');
            foreach ( $files as $file ) {
                if ( is_file($file) ) {
                    unlink($file);
                }
            }

            $files = glob($cache_folder . '/forecast/*');
            foreach ( $files as $file ) {
                if ( is_file($file) ) {
                    if ( basename($file) == 'data-example' ) {
                        continue;
                    }
                    unlink($file);
                }
            }
        }

        return true;
    }



    public static function import_settings($file) {

        $content = file_get_contents($file);
        $content = unserialize($content);

        foreach ( $content as $page_settings ) {

            $page_name         = $page_settings[key($page_settings)]['page'];
            $current_setttings = get_option($page_name, array());

            foreach ( $page_settings as $key => $page_setting ) {

                if ( !isset($current_setttings[$key]) ) {
                    continue;
                }

                $current_setttings[$key] = $page_setting;
            }

            update_option($page_name, $current_setttings);
        }


        return true;
    }



    public static function export_settings() {


        $filename = 'STEC Settings ' . date('Y-M-j h-i');

        header('Content-type: text/plain; charset=utf-8');
        header("Content-Disposition: attachment; filename=\"" . $filename . ".stec\"");

        $stachethemes_ec_main = stachethemes_ec_main::get_instance();

        $pages = array(
                'stec_menu__general',
                'stec_menu__general_other',
                'stec_menu__general_google_captcha',
                'stec_menu__general_email_invitation',
                'stec_menu__general_email_reminder',
                'stec_menu__fontsandcolors_top',
                'stec_menu__fontsandcolors_monthweek',
                'stec_menu__fontsandcolors_day',
                'stec_menu__fontsandcolors_preview',
                'stec_menu__fontsandcolors_event',
                'stec_menu__fontsandcolors_tooltip',
                'stec_menu__fontsandcolors_agenda');

        $content = array();

        foreach ( $pages as $page ) {
            $content[] = $stachethemes_ec_main->get_admin_setting($page);
        }

        echo serialize($content);

        exit();
    }

}




class stec_event_data {



    public $calendar;
    public $calid              = false;
    public $summary            = 'Untitled';
    public $alias              = null;
    public $event_color        = null;
    public $icon               = '';
    public $visibility         = null;
    public $featured           = 0;
    public $start_date         = null;
    public $start_time_hours   = null;
    public $start_time_minutes = null;
    public $end_date           = null;
    public $end_time_hours     = null;
    public $end_time_minutes   = null;
    public $all_day            = 0;
    public $keywords           = '';
    public $rrule              = '';
    public $exdate             = '';
    public $counter            = 0;
    public $comments           = 0;
    public $link               = null;
    public $approved           = 0;
    public $location           = null;
    public $location_details   = null;
    public $location_forecast  = null;
    public $location_use_coord = 0;
    public $images             = false;
    public $description        = '';
    public $description_short  = '';
    public $schedule           = false;
    public $guests             = false;
    public $attendee           = false;
    public $attachment         = false;
    public $wc_product         = false;
    public $uid                = null;

}
