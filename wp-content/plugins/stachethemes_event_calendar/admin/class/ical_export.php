<?php




/**
 * Very basic ics generator
 * Converts calendar event objects to ics
 */
class stachethemes_ec_ical_export {



    private $_eol;
    private $_events_content;
    private $_calendar;



    public function __construct($calendar) {

        $this->_calendar = $calendar;

        $this->_events_content = '';

        $this->_eol = "\n";
    }



    public function date_to_cal($date_string) {

        $date = new DateTime(($date_string), new DateTimeZone($this->_calendar->timezone));

        return $date->format('Ymd\THis');
    }



    public function escape_string($string) {
        return preg_replace('/([\,;])/', '', $string);
    }



    private function uid($event) {
        return $event->uid ? $event->uid : uniqid('e-') . '_' . md5(microtime()) . '@stachethemes_ec.com';
    }



    public function proccess_event($event) {

        if ( !is_object($event) ) {
            return false;
        }

        if ( $event->all_day == '1' ) {
            $date            = new DateTime(($event->end_date), new DateTimeZone($this->_calendar->timezone));
            $date->setTime(24, 0, 0);
            $event->end_date = $date->format('Y-m-d H:i:s');
        }

        // multiline fix
        $event->description = preg_replace('/\r\n|\n\r|\r|\n/', '\n', $event->description);

        $content = '';
        $content .= 'BEGIN:VEVENT' . $this->_eol;
        $content .= 'DTEND:' . $this->date_to_cal($event->end_date) . $this->_eol;
        if ( $event->rrule != '' ) {
            $content .= 'RRULE:' . $event->rrule . $this->_eol;
        }
        $content .= 'UID:' . $this->uid($event) . $this->_eol;
        $content .= 'DTSTAMP:' . $this->date_to_cal('now') . $this->_eol;
        $content .= 'LOCATION:' . $this->escape_string($event->location) . $this->_eol;
        $content .= 'DESCRIPTION:' . $this->escape_string($event->description) . $this->_eol;
        $content .= 'SUMMARY:' . $this->escape_string($event->summary) . $this->_eol;
        $content .= 'DTSTART:' . $this->date_to_cal($event->start_date) . $this->_eol;

        if ( $event->exdate ) {
            $exdates = explode(',', $event->exdate);
            foreach ( $exdates as $exdate ) {
                $content .= 'EXDATE;VALUE=DATE:' . $exdate . $this->_eol;
            }
        }

        if ( $event->recurrence_id ) {
            
            // If is UTC do not include timezone
            if ( strpos($event->recurrence_id, 'Z') !== false ) {
                $content .= 'RECURRENCE-ID:' . $event->recurrence_id . $this->_eol;
            } else {
                $content .= 'RECURRENCE-ID;TZID=' . $this->_calendar->timezone . ':' . $event->recurrence_id . $this->_eol;
            }
        }

        $content .= 'END:VEVENT' . $this->_eol;

        $this->_events_content .= $content;

        return true;
    }



    public function get_content() {

        $content = '';

        $content .= 'BEGIN:VCALENDAR' . $this->_eol;
        $content .= 'VERSION:2.0' . $this->_eol;
        $content .= 'X-WR-TIMEZONE:' . $this->_calendar->timezone . $this->_eol;
        $content .= $this->_events_content;
        $content .= 'END:VCALENDAR';

        return $content;
    }



    public function download($filename = false) {

        $title = $filename ? $filename : $this->_calendar->title . ' ' . date('y-m-d His');

        header('Content-type: text/calendar; charset=utf-8');
        header("Content-Disposition: attachment; filename=\"" . $title . ".ics\"");

        echo $this->get_content();

        exit();
    }

}
