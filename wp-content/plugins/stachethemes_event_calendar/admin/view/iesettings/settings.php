<div class="stachethemes-admin-wrapper">

    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1><?php $stachethemes_ec_main->lang('Stachethemes Event Calendar - Import/Export Settings', true); ?></h1>

    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('Import .STEC File Settings', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST", true);

        stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Import settings from file'));
        stachethemes_ec_admin::html_input('settings_filename', '', '', $stachethemes_ec_main->lang('Settings FIle'), true, "file", 'accept=".stec"');
        
        ?><div class="stachethemes-admin-separator"></div><?php
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Import settings'));
        stachethemes_ec_admin::html_hidden('task', 'stec_import_settings');

        stachethemes_ec_admin::html_form_end();
        ?>

    </div>

    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('Export to .STEC File', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST", false);
        
        stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Export settings to file'));
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Export settings'));
        stachethemes_ec_admin::html_hidden('task', 'stec_export_settings');

        stachethemes_ec_admin::html_form_end();
        ?>

    </div>
</div>
