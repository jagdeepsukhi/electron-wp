<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch ( stachethemes_ec_admin::get("task") ) {

    case 'reset_calendar_session' :
        $_SESSION['stec_admin_calendar_id'] = null;
        break;

    case 'stec_export_to_ics' :
        // handled from stachethemes_event_calendar.php 
        // _no_headers_get_handler
        break;
}

// POST
switch ( stachethemes_ec_admin::post("task") ) {
    
}

// VIEW
switch ( stachethemes_ec_admin::get("view") ) :

    case "list" :

        $calendar_id = stachethemes_ec_admin::get('calendar_id', false, FILTER_VALIDATE_INT);

        if ( !$calendar_id && isset($_SESSION['stec_admin_calendar_id']) ) {
            // Check if session calendar_id is selected
            $calendar_id = $_SESSION['stec_admin_calendar_id'];
        }

        $calendar = stachethemes_ec_admin::calendar($calendar_id);

        $events = stachethemes_ec_admin::events($calendar_id);

        $_SESSION['stec_admin_calendar_id'] = $calendar_id;

        include __dir__ . '/list.php';

        break;

    case "export":
    default:

        $calendars = stachethemes_ec_admin::calendars();

        include __dir__ . '/export.php';
endswitch;



