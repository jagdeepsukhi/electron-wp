<!--

Create and list calendars

List with all created calendars
Calendars are like categories, you can store events in different 
calendars and show each calendar independently 

-->

<div class="stachethemes-admin-wrapper">
    
    <?php echo stachethemes_ec_admin::display_message(); ?>


    <h1 class="stachethemes-admin-flex-head">
        <?php 
            $stachethemes_ec_main->lang('Export Events Management ', true); 
            echo " / " .$calendar->title; 
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Switch Calendar'), "?page={$plugin_page}&task=reset_calendar_session", true);
        ?>
    </h1>

    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('Export Single Events', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('List of your created events'));

        if (empty($events)) :

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('(No events to display)'));

        else :

            ?>
            <div class="stec-list-bulk">
                <div>
                    <input type="checkbox" name="all_events" value="1" /> <?php $stachethemes_ec_main->lang('Select all events'); ?>
                    <p><?php $stachethemes_ec_main->lang('Selected all events', true); ?></p>

                    <?php stachethemes_ec_admin::html_form_start("?page=$plugin_page&view=list&calendar_id={$calendar_id}"); ?>

                        <button class='export-all-items'><p><i class="fa fa-calendar-check-o"></i><?php $stachethemes_ec_main->lang('Export selected events', true); ?></p></button>

                        <?php 
                        stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
                        stachethemes_ec_admin::html_hidden('task', 'stec_export_to_ics_bulk');
                        ?>

                    <?php stachethemes_ec_admin::html_form_end(); ?>

                </div>   
            </div>
            <?php

            echo '<ul class="stec-list">';

                foreach ($events as $event) :
                    ?>
                    <li>
                        <div class='calinfo'>
                            <input type="checkbox" name="eventid" value="<?php echo $event->id; ?>" />
                            <p class='color' style='background: <?php echo $event->color ? $event->color : $calendar->color; ?>'></p>
                            <p class='title'><a href='<?php echo "?page={$plugin_page}&task=stec_export_to_ics&calendar_id={$calendar_id}&event_id={$event->id}"; ?>'><?php echo $event->summary; ?> (id#<?php echo $event->id; ?>)</a></p>
                        </div>
                        <div class='ctrl'>
                        <a href='<?php echo "?page={$plugin_page}&task=stec_export_to_ics&calendar_id={$calendar_id}&event_id={$event->id}"; ?>'><?php $stachethemes_ec_main->lang('Export', true); ?></a>
                        </div>
                    </li>
                    <?php
                endforeach;

            echo '</ul>';
        endif;
        ?>

    </div>


</div>