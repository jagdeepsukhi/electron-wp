<!--

Export Calendar Events to .ICS

-->

<div class="stachethemes-admin-wrapper">

    <?php echo stachethemes_ec_admin::display_message(); ?>

    <?php if (isset($calendar)) : ?>

        <h1 class="stachethemes-admin-flex-head">
            <?php
            $stachethemes_ec_main->lang('Export Events Management', true);
            echo " / " . $calendar->title;
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Switch Calendar'), "?page={$plugin_page}&task=reset_calendar_session", true);
            ?>
        </h1>

    <?php else: ?>

        <h1>
            <?php
            $stachethemes_ec_main->lang('Export Events Management / Select Calendar', true);
            ?>
        </h1>

    <?php endif ?>


    <?php if (isset($calendars)) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Select the calendar from which events will be exported', true); ?></h2>

            <?php
            if (empty($calendars)) :

                stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('You have no calendars'));

                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Create calendar'), '?page=stec_menu__calendars');

            else:

                echo '<ul class="stec-list">';

                foreach ($calendars as $calendar) :
                    ?>
                    <li>
                        <div class='calinfo'>
                            <p class='color' style='background: <?php echo $calendar->color; ?>'></p>
                            <p class='title'><a href='<?php echo "?page=stec_menu__export&task=stec_export_to_ics&calendar_id={$calendar->id}"; ?>'><?php echo $calendar->title; ?> (id#<?php echo $calendar->id; ?>)</a></p>
                        </div>
                        <div class='ctrl'>
                            <a href='<?php echo "?page=stec_menu__export&view=list&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('View Events', true); ?></a>
                            <a href='<?php echo "?page=stec_menu__export&task=stec_export_to_ics&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('Export', true); ?></a>
                        </div>
                    </li>
                    <?php
                endforeach;

                echo '</ul>';

            endif;
            ?>

        </div>

    <?php endif; ?>


</div>