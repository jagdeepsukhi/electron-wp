<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch ( stachethemes_ec_admin::get("task") ) {

    case 'reset_calendar_session' :
        $_SESSION['stec_admin_calendar_id'] = null;
        break;

    case 'delete' :

        stachethemes_ec_admin::verify_nonce('', 'GET');

        $job_id = stachethemes_ec_admin::get('id', 0, FILTER_VALIDATE_INT);

        $result = stachethemes_ec_admin::delete_import_cronjob($job_id);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Cronjob deleted"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete cronjob"), 'error');
        }

        break;
}

// POST
switch ( stachethemes_ec_admin::post("task") ) {

    case 'import' :

        $has_file = !empty($_FILES["ics_filename"]['name']) ? true : false;
        $has_url  = stachethemes_ec_admin::post('ics_url', false, FILTER_SANITIZE_URL);

        $ignore_expired   = stachethemes_ec_admin::post('ignore_expired', false) !== false ? true : false;
        $overwrite_events = stachethemes_ec_admin::post('overwrite_events', false) !== false ? true : false;
        $delete_removed   = stachethemes_ec_admin::post('delete_removed', false) !== false ? true : false;
        $calid            = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);
        $icon             = stachethemes_ec_admin::post('icon', false, FILTER_SANITIZE_STRING);

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calid}");

        if ( $has_file ) :

            $result = stachethemes_ec_admin::import_ics($_FILES["ics_filename"]["tmp_name"], $calid, false, $icon, $ignore_expired, $overwrite_events, $delete_removed);

            if ( $result !== false && is_object($result) ) {

                stachethemes_ec_admin::set_message(
                        sprintf($stachethemes_ec_main->lang('%d event(s) imported. %d event(s) overwritten. %d event(s) removed.'), $result->imported_count, $result->overwritten_count, $result->deleted_count));
            } else {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to import events"), 'error');
            }

        endif;

        if ( $has_url ) {

            $url = stachethemes_ec_admin::post('ics_url', false, FILTER_SANITIZE_URL);
            $url = str_replace('webcal://', 'http://', $url);

            $result = stachethemes_ec_admin::import_ics($url, $calid, false, $icon, $ignore_expired, $overwrite_events, $delete_removed);

            if ( $result !== false && is_object($result) ) {

                stachethemes_ec_admin::set_message(
                        sprintf($stachethemes_ec_main->lang('%d event(s) imported. %d event(s) overwritten. %d event(s) removed.'), $result->imported_count, $result->overwritten_count, $result->deleted_count));
            } else {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to import events"), 'error');
            }
        }

        break;

    case 'create_cronjob' :

        $has_url          = stachethemes_ec_admin::post('ics_url', false, FILTER_SANITIZE_URL);
        $ignore_expired   = stachethemes_ec_admin::post('ignore_expired', false) !== false ? true : false;
        $overwrite_events = stachethemes_ec_admin::post('overwrite_events', false) !== false ? true : false;
        $delete_removed   = stachethemes_ec_admin::post('delete_removed', false) !== false ? true : false;
        $calid            = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);
        $icon             = stachethemes_ec_admin::post('icon', false, FILTER_SANITIZE_STRING);
        $freq             = stachethemes_ec_admin::post('ics_cronjob_freq', false, FILTER_VALIDATE_INT);

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calid}");

        if ( !$has_url ) :
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Invalid URL"), "error");
            break;
        endif;

        $url = str_replace('webcal://', 'http://', $has_url);

        $result = stachethemes_ec_admin::add_import_cronjob($calid, $url, $icon, $freq, false, $ignore_expired, $overwrite_events, $delete_removed);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Cronjob created"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to create cronjob"), 'error');
        }

        break;

    case 'delete_bulk' :

        $calid = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calid}");

        $jobs = stachethemes_ec_admin::post('idlist', false, FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);

        if ( $jobs ) {

            $result = stachethemes_ec_admin::delete_bulk_import_cronjobs($jobs);

            if ( $result ) {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Cronjobs deleted"));
            } else {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete cronjobs"), 'error');
            }
        }

        break;
}

// VIEW
switch ( stachethemes_ec_admin::get("view") ) :

    case "import":
    default:

        $calendar_id = stachethemes_ec_admin::get('calendar_id', false, FILTER_VALIDATE_INT);
        $cronjobs    = array();

        if ( !$calendar_id && isset($_SESSION['stec_admin_calendar_id']) ) {
            // Check if session calendar_id is selected
            $calendar_id = $_SESSION['stec_admin_calendar_id'];
        }

        if ( $calendar_id ) {
            // Check if calendar exists
            $calendar = stachethemes_ec_admin::calendar($calendar_id);
            if ( !$calendar ) {
                $calendar_id = null;
            }
        }

        if ( !$calendar_id ) {
            $calendars = stachethemes_ec_admin::calendars();
        } else {
            $calendar = stachethemes_ec_admin::calendar($calendar_id);
            $cronjobs = stachethemes_ec_admin::get_import_cronjobs($calendar_id);
        }

        $_SESSION['stec_admin_calendar_id'] = $calendar_id;

        include __dir__ . '/import.php';

endswitch;
