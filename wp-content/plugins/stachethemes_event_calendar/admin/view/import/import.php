<!--

Import .ICS events to Calendar

-->

<div class="stachethemes-admin-wrapper">

    <?php echo stachethemes_ec_admin::display_message(); ?>

    <?php if (isset($calendar)) : ?>

        <h1 class="stachethemes-admin-flex-head">
            <?php
            $stachethemes_ec_main->lang('Import Events Management', true);
            echo " / " . $calendar->title;
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Switch Calendar'), "?page={$plugin_page}&task=reset_calendar_session", true);
            ?>
        </h1>

    <?php else: ?>

        <h1>
            <?php
            $stachethemes_ec_main->lang('Import Events Management / Select Calendar', true);
            ?>
        </h1>

    <?php endif ?>

    <?php if (isset($calendars)) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Select the calendar on which the events should be imported', true); ?></h2>

            <?php
            if (empty($calendars)) :

                stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('You have no calendars'));

                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Create calendar'), '?page=stec_menu__calendars');

            else:

                echo '<ul class="stec-list">';

                foreach ($calendars as $calendar) :
                    ?>
                    <li>
                        <div class='calinfo'>
                            <p class='color' style='background: <?php echo $calendar->color; ?>'>
                                <i class="<?php echo $calendar->icon; ?>"></i>
                            </p>
                            <p class='title'><a href='<?php echo "?page=stec_menu__import&calendar_id={$calendar->id}"; ?>'><?php echo $calendar->title; ?> (id#<?php echo $calendar->id; ?>)</a></p>
                        </div>
                        <div class='ctrl'>
                            <a href='<?php echo "?page=stec_menu__import&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('Select', true); ?></a>
                        </div>
                    </li>
                    <?php
                endforeach;

                echo '</ul>';

            endif;
            ?>

        </div>

    <?php endif; ?>

    <?php if ($calendar_id) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Import .ics Events', true); ?></h2>

            <?php
            stachethemes_ec_admin::html_form_start("?page=$plugin_page&calendar_id={$calendar_id}", "POST", true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Import from URL'));
            stachethemes_ec_admin::html_input('ics_url', '', '', $stachethemes_ec_main->lang('URL'), false);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Import from .ics file'));
            stachethemes_ec_admin::html_input('ics_filename', '', '', $stachethemes_ec_main->lang('ICS File'), false, "file", 'accept=".ics"');
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Event Icon'));
            stachethemes_ec_admin::html_icon('icon', stachethemes_ec_admin::icon_list(), $calendar->icon, true);
            
            stachethemes_ec_admin::html_checkbox('ignore_expired', 1, 1, $stachethemes_ec_main->lang("Don't import expired events"), false);
            ?>
            <p class="desc">
                <?php $stachethemes_ec_main->lang('If checked non-recurring expired events will be ignored.',true); ?>
            </p>
            <?php
            
            stachethemes_ec_admin::html_checkbox('overwrite_events', 0, 0, $stachethemes_ec_main->lang("Overwrite events"), false);
            ?>
            <p class="desc">
                <?php $stachethemes_ec_main->lang('Existing events will be overwritten.',true); ?>
            </p>
            <?php
            
            
            stachethemes_ec_admin::html_checkbox('delete_removed', 0, 0, $stachethemes_ec_main->lang("Delete events"), false);
            ?>
            <p class="desc">
                <?php $stachethemes_ec_main->lang('Delete calendar event if it does not exist in the ics file.',true); ?>
            </p>
            <?php
            
            
            
            stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
            stachethemes_ec_admin::html_hidden('task', 'import');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Import Events'));

            stachethemes_ec_admin::html_form_end();
            ?>
            

        </div>

        <h1><?php $stachethemes_ec_main->lang('Auto-import events from URL', true); ?></h1>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Schedule Cronjob', true); ?></h2>

            <?php
            stachethemes_ec_admin::html_form_start("?page=$plugin_page&calendar_id={$calendar_id}");

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Import URL'));
            stachethemes_ec_admin::html_input('ics_url', '', '', $stachethemes_ec_main->lang('URL'), false);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Cronjob Frequency'));
            stachethemes_ec_admin::html_select('ics_cronjob_freq', array(
                '0' => $stachethemes_ec_main->lang('Hourly'),
                '1' => $stachethemes_ec_main->lang('Twice Daily'),
                '2' => $stachethemes_ec_main->lang('Daily'),
                '3' => $stachethemes_ec_main->lang('Weekly')
                ), 0, false);
            
                stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Event Icon'));
                stachethemes_ec_admin::html_icon('icon', stachethemes_ec_admin::icon_list(), $calendar->icon, true);

                stachethemes_ec_admin::html_checkbox('ignore_expired', 1, 1, $stachethemes_ec_main->lang("Don't import expired events"), false);
           
            ?>
            
            <p class="desc">
                <?php $stachethemes_ec_main->lang('If checked non-recurring expired events will be ignored.',true); ?>
            </p>
            
            <?php
            
            stachethemes_ec_admin::html_checkbox('overwrite_events', 0, 0, $stachethemes_ec_main->lang("Overwrite events"), false);
            ?>
            <p class="desc">
                <?php $stachethemes_ec_main->lang('Existing events will be overwritten.',true); ?>
            </p>
                
            <?php
                stachethemes_ec_admin::html_checkbox('delete_removed', 0, 0, $stachethemes_ec_main->lang("Delete events"), false);
            ?>
            <p class="desc">
                <?php $stachethemes_ec_main->lang('Delete calendar event if it does not exist in the ics file.',true); ?>
            </p>
            
            <?php
            
            stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
            stachethemes_ec_admin::html_hidden('task', 'create_cronjob');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Create Cronjob'));

            stachethemes_ec_admin::html_form_end();
            ?>

        </div>

        <h1><?php $stachethemes_ec_main->lang('Manage Cronjob list', true); ?></h1>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('List of your created cronjobs', true); ?></h2>

            <?php

            if (empty($cronjobs)) :

                stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('(No cronjobs to display)'));

            else :

                ?>
                <div class="stec-list-bulk">
                    <div>
                        <input type="checkbox" name="all_cronjobs" value="1" /> <?php $stachethemes_ec_main->lang('Select all jobs'); ?>
                        <p><?php $stachethemes_ec_main->lang('Selected all jobs', true); ?></p>

                        <?php stachethemes_ec_admin::html_form_start("?page=$plugin_page&calendar_id={$calendar_id}"); ?>

                            <button class='delete-all-items' data-confirm='<?php $stachethemes_ec_main->lang('Delete all jobs?',true); ?>'><p><i class="fa fa-trash-o"></i><?php $stachethemes_ec_main->lang('Delete selected jobs', true); ?></p></button>

                            <?php 
                            stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
                            stachethemes_ec_admin::html_hidden('task', 'delete_bulk');
                            ?>

                        <?php stachethemes_ec_admin::html_form_end(); ?>
                    </div>   
                </div>
                <?php

                echo '<ul class="stec-list">';

                foreach ($cronjobs as $job) :

                    ?>
                    <li>
                        <div class='calinfo'>
                            <input type="checkbox" name="id" value="<?php echo $job->id; ?>" />
                            <p class='color' style='background: <?php echo $job->color; ?>'><i class="<?php echo $job->icon; ?>"></i></p>
                            <p class='title'>
                                <?php echo htmlspecialchars($job->url); ?>
                                (<?php 
                                    switch($job->freq) :
                                        case 0: $stachethemes_ec_main->lang('hourly', true); break;
                                        case 1: $stachethemes_ec_main->lang('twice daily', true); break;
                                        case 2: $stachethemes_ec_main->lang('daily', true); break;
                                        case 3: $stachethemes_ec_main->lang('once weekly', true); break;
                                    endswitch;
                                    
                                    if ($job->ignore_expired == 1) :
                                        echo ', '.$stachethemes_ec_main->lang('ignores expired');
                                    endif;
                                    
                                    if ($job->delete_removed == 1) :
                                        echo ', '.$stachethemes_ec_main->lang('deletes removed');
                                    endif;
                                ?>)
                            </p>
                        </div>
                        <div class='ctrl'>
                            <a class='delete-item' data-confirm='<?php $stachethemes_ec_main->lang('Delete item?',true); ?>' href='<?php echo wp_nonce_url("?page={$plugin_page}&task=delete&id={$job->id}&calendar_id={$calendar_id}"); ?>'><?php $stachethemes_ec_main->lang('Delete', true); ?></a>
                        </div>
                    </li>
                    <?php
                endforeach;

                echo '</ul>';
            endif;
            ?>

        </div>
    
    <?php endif; ?>

</div>

