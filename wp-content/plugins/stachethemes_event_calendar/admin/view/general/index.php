<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch(stachethemes_ec_admin::get("task")) {
    case 'reset' :
        
        stachethemes_ec_admin::verify_nonce('','GET');
        
        stachethemes_ec_admin::reset_admin_settings($plugin_page);
        stachethemes_ec_admin::reset_admin_settings($plugin_page.'_email_invitation');
        stachethemes_ec_admin::reset_admin_settings($plugin_page.'_email_reminder');
        stachethemes_ec_admin::reset_admin_settings($plugin_page.'_google_captcha');
        stachethemes_ec_admin::reset_admin_settings($plugin_page.'_other');
        
        stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Settings Reset"));


      break;
}


// POST
switch(stachethemes_ec_admin::post("task")) {
    
    case 'save' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        stachethemes_ec_admin::update_admin_settings($plugin_page);
        stachethemes_ec_admin::update_admin_settings($plugin_page.'_email_invitation');
        stachethemes_ec_admin::update_admin_settings($plugin_page.'_email_reminder');
        stachethemes_ec_admin::update_admin_settings($plugin_page.'_google_captcha');
        stachethemes_ec_admin::update_admin_settings($plugin_page.'_other');
        
        stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Settings updated"));
        
    break;
    
}

// VIEW
switch(stachethemes_ec_admin::get("view")) :
    
    default:
        include __dir__ . '/settings.php';
       
endswitch;



