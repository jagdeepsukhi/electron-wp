<div class="stachethemes-admin-wrapper">
    
    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1><?php $stachethemes_ec_main->lang('Stachethemes Event Calendar - General Settings', true); ?></h1>
    
    <ul class="stachethemes-admin-tabs-list">
        <li data-tab="general"><?php $stachethemes_ec_main->lang('General', true); ?></li>
        <li data-tab="email_invitation"><?php $stachethemes_ec_main->lang('Invitation E-Mail', true); ?></li>
        <li data-tab="email_reminder"><?php $stachethemes_ec_main->lang('Reminder E-Mail', true); ?></li>
        <li data-tab="google_captcha"><?php $stachethemes_ec_main->lang('Captcha Settings', true); ?></li>
        <li data-tab="other"><?php $stachethemes_ec_main->lang('Other Settings', true); ?></li>
    </ul>

    <div class="stachethemes-admin-section">

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");
        
            ?>
            
            <div class="stachethemes-admin-section-tab" data-tab="general">
                <h2><?php $stachethemes_ec_main->lang('General Settings', true); ?></h2>
                <?php stachethemes_ec_admin::build_admin_setting($plugin_page); ?>
            </div>
        
            <div class="stachethemes-admin-section-tab" data-tab="email_invitation">
                <h2><?php $stachethemes_ec_main->lang('Invitation E-Mail', true); ?></h2>
                <?php stachethemes_ec_admin::build_admin_setting($plugin_page.'_email_invitation'); ?>
            </div>
        
            <div class="stachethemes-admin-section-tab" data-tab="email_reminder">
                <h2><?php $stachethemes_ec_main->lang('Reminder E-Mail', true); ?></h2>
                <?php stachethemes_ec_admin::build_admin_setting($plugin_page.'_email_reminder'); ?>
            </div>
        
            <div class="stachethemes-admin-section-tab" data-tab="google_captcha">
                <h2><?php $stachethemes_ec_main->lang('Captcha Settings', true); ?></h2>
                <?php stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('NOTE: To manage your API keys go to').' <a target="_BLANK" href="https://www.google.com/recaptcha/admin#list">Google reCAPTCHA</a>'); ?>
                <?php stachethemes_ec_admin::build_admin_setting($plugin_page.'_google_captcha'); ?>
            </div>
        
            <div class="stachethemes-admin-section-tab" data-tab="other">
                <h2><?php $stachethemes_ec_main->lang('Other Settings', true); ?></h2>
                <?php stachethemes_ec_admin::build_admin_setting($plugin_page.'_other'); ?>
            </div>
        
            <?php
            
            // save
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Save settings'));
            stachethemes_ec_admin::html_hidden('task', 'save');
            
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Reset all'), wp_nonce_url("?page={$plugin_page}&task=reset"), true);

        stachethemes_ec_admin::html_form_end();
        ?>
        
    </div>
</div>
