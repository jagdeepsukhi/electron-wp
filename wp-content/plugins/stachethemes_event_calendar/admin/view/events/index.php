<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();


// GET
switch ( stachethemes_ec_admin::get("task") ) {

    case 'reset_calendar_session' :
        $_SESSION['stec_admin_calendar_id'] = null;
        break;

    case 'duplicate' :

        stachethemes_ec_admin::verify_nonce('', 'GET');

        $event_id = stachethemes_ec_admin::get('event_id', 0, FILTER_VALIDATE_INT);

        $result = stachethemes_ec_admin::duplicate_event($event_id);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Event duplicated"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to duplicate event"), 'error');
        }

        break;

    case 'approve' :

        stachethemes_ec_admin::verify_nonce('', 'GET');

        $event_id = stachethemes_ec_admin::get('event_id', 0, FILTER_VALIDATE_INT);

        $result = stachethemes_ec_admin::approve_event($event_id);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Event approved"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to approve event"), 'error');
        }

        break;

    case 'delete' :

        stachethemes_ec_admin::verify_nonce('', 'GET');

        $event_id = stachethemes_ec_admin::get('event_id', 0, FILTER_VALIDATE_INT);

        $result = stachethemes_ec_admin::delete_event($event_id);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Event deleted"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete event"), 'error');
        }

        break;
}

// POST
switch ( stachethemes_ec_admin::post("task") ) {

    case 'create' :

        $calendar_id = stachethemes_ec_admin::post('calendar_id');

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calendar_id}");

        $event_data = stachethemes_ec_admin::calendar_post_data();

        $result = stachethemes_ec_admin::create_event($event_data);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Event created"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to create event"), 'error');
        }

        break;

    case 'update' :

        $calendar_id = stachethemes_ec_admin::post('calendar_id');

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calendar_id}");

        $event_id = stachethemes_ec_admin::post('event_id');

        $event_data = stachethemes_ec_admin::calendar_post_data();

        $result = stachethemes_ec_admin::update_event($event_id, $event_data);

        if ( $result ) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Event updated"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to update event"), 'error');
        }

        break;

    case 'delete_bulk' :

        $calendar_id = stachethemes_ec_admin::post('calendar_id');

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}&calendar_id={$calendar_id}");

        $events = stachethemes_ec_admin::post('idlist', false, FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);


        if ( $events ) {

            $result = stachethemes_ec_admin::delete_bulk_events($events);

            if ( $result ) {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Events deleted"));
            } else {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete events"), 'error');
            }
        }


        break;
}

// VIEW
switch ( stachethemes_ec_admin::get("view") ) :

    case "edit" :

        $event_id    = stachethemes_ec_admin::get('event_id');
        $event       = stachethemes_ec_admin::event($event_id);
        $calendar_id = $event->calid;
        $calendar    = stachethemes_ec_admin::calendar($calendar_id);
        $cal_array   = stachethemes_ec_admin::calendars_list();
        $users       = get_users();

        include __dir__ . '/addedit.php';

        break;

    case "add" :

        $event_id    = false;
        $event       = false;
        $calendar_id = stachethemes_ec_admin::get('calendar_id');
        $calendar    = stachethemes_ec_admin::calendar($calendar_id);
        $users       = get_users();

        include __dir__ . '/addedit.php';

        break;

    case "list":
    default:

        $calendar_id = stachethemes_ec_admin::get('calendar_id');

        if ( !$calendar_id && isset($_SESSION['stec_admin_calendar_id']) ) {
            // Check if session calendar_id is selected
            $calendar_id = $_SESSION['stec_admin_calendar_id'];
        }
        
        if ($calendar_id) {
            // Check if calendar exists
            $calendar = stachethemes_ec_admin::calendar($calendar_id);
            if (!$calendar) {
                $calendar_id = null;
            }
        }
        
        if ( !$calendar_id ) {
            $calendars = stachethemes_ec_admin::calendars();
        } else {
            $calendar = stachethemes_ec_admin::calendar($calendar_id);
            $events   = stachethemes_ec_admin::events($calendar_id, true);
        }

        $_SESSION['stec_admin_calendar_id'] = $calendar_id;

        include __dir__ . '/list.php';
endswitch;



