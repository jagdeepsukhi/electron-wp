<!--

Create and list calendars

List with all created calendars
Calendars are like categories, you can store events in different 
calendars and show each calendar independently 

-->

<div class="stachethemes-admin-wrapper">
    
    <?php echo stachethemes_ec_admin::display_message(); ?>
    
    <?php if (isset($calendar)) : ?>

        <h1 class="stachethemes-admin-flex-head">
            <?php 
                $stachethemes_ec_main->lang('Events Management', true); 
                echo " / " .$calendar->title; 
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Switch Calendar'), "?page={$plugin_page}&task=reset_calendar_session", true); 
            ?>
        </h1>

    <?php else: ?>

        <h1>
            <?php 
                $stachethemes_ec_main->lang('Events Management / Select Calendar', true); 
            ?>
        </h1>

    <?php endif ?>

    <?php if (isset($calendars)) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Select Calendar', true); ?></h2>
            
            <?php 
                if (empty($calendars)) :
                    
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('You have no calendars'));
                
                    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Create calendar'), '?page=stec_menu__calendars');
                
                else:
    
                echo '<ul class="stec-list">';

                foreach ($calendars as $calendar) :
                    
                    $add_text = '';
                    $aaproval_count = stachethemes_ec_admin::get_aaproval_count($calendar->id);
                    if ($aaproval_count > 0) {
                        $add_text = '-- '. $aaproval_count . ' ' . ($aaproval_count == 1 ? $stachethemes_ec_main->lang('event awaiting approval') : $stachethemes_ec_main->lang('events awaiting approval')) . ' --';
                    }
                
                    ?>
                    <li>
                        <div class='calinfo'>
                            <p class='color' style='background: <?php echo $calendar->color; ?>'>
                                <i class="<?php echo $calendar->icon; ?>"></i>
                            </p>
                            <p class='title'>
                                <a href='<?php echo "?page=stec_menu__events&calendar_id={$calendar->id}"; ?>'>
                                    <?php echo $calendar->title; ?> (id#<?php echo $calendar->id; ?>)
                                </a> 
                                <span class="stec-text-red"><?php echo $add_text; ?></span></p>
                        </div>
                        <div class='ctrl'>
                            <a href='<?php echo "?page=stec_menu__events&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('View Events', true); ?></a>
                        </div>
                    </li>
                    <?php
                endforeach;

                echo '</ul>';
            
            endif;
            ?>

        </div>

    <?php endif; ?>

    <?php if (isset($events)) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Add Events', true); ?></h2>

            <?php
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Add new event to this calendar'));
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add Event'), "?page={$plugin_page}&view=add&calendar_id={$calendar_id}");
            ?>

        </div>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Manage Events', true); ?></h2>

            <?php
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('List of your created events'));

            if (empty($events)) :

                stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('(No events to display)'));

            else :
                
                ?>
                <div class="stec-list-bulk">
                    <div>
                        <input type="checkbox" name="all_events" value="1" /> <?php $stachethemes_ec_main->lang('Select all events'); ?>
                        <p><?php $stachethemes_ec_main->lang('Selected all events', true); ?></p>
                        
                        <?php stachethemes_ec_admin::html_form_start("?page=$plugin_page&calendar_id={$calendar_id}"); ?>

                            <button class='delete-all-items' data-confirm='<?php $stachethemes_ec_main->lang('Delete all items?',true); ?>'><p><i class="fa fa-trash-o"></i><?php $stachethemes_ec_main->lang('Delete selected events', true); ?></p></button>

                            <?php 
                            stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
                            stachethemes_ec_admin::html_hidden('task', 'delete_bulk');
                            ?>
                       
                        <?php stachethemes_ec_admin::html_form_end(); ?>
                        
                    </div>   
                </div>
                <?php
                
                echo '<ul class="stec-list">';

                    foreach ($events as $event) :
                        
                        ?>
                        <li>
                            <div class='calinfo'>
                                <input type="checkbox" name="eventid" value="<?php echo $event->id; ?>" />
                                <p class='color' style='background: <?php echo $event->color ? $event->color : $calendar->color; ?>'><i class="<?php echo $event->icon; ?>"></i></p>
                                <p class='title'>
                                    <a href='<?php echo "?page={$plugin_page}&view=edit&calendar_id={$calendar_id}&event_id={$event->id}"; ?>'>
                                        <?php echo $event->summary; ?> (id#<?php echo $event->id; ?>)</a> 
                                        <?php 
                                            if ($event->approved == "0") :
                                                echo '<span class="stec-text-red"> -- '.$stachethemes_ec_main->lang('Awaiting approval').' -- </span>';
                                            endif 
                                        ?>
                                    <span class="stec-event-timespan">
                                        <?php
                                            echo stachethemes_ec_admin::get_the_timespan($event);
                                            
                                            if ($event->rrule != '') {
                                                echo ' ('.$stachethemes_ec_main->lang('Recurring').')';
                                            }
                                            
                                            if ($event->recurrence_id != '') {
                                                echo ' ('.$stachethemes_ec_main->lang('Recurrence Override: '). $event->recurrence_id .')';
                                            }
                                        ?>
                                    </span>
                                </p>
                            </div>
                            <div class='ctrl'>
                            
                            <?php if ($event->approved == '0') : ?>

                                <a href='<?php echo wp_nonce_url("?page={$plugin_page}&task=approve&view=list&calendar_id={$calendar_id}&event_id={$event->id}"); ?>'><?php $stachethemes_ec_main->lang('Approve', true); ?></a>
                                <a href='<?php echo wp_nonce_url("?page={$plugin_page}&view=edit&calendar_id={$calendar_id}&event_id={$event->id}"); ?>'><?php $stachethemes_ec_main->lang('Review', true); ?></a>
                                
                            <?php else: ?>
                                
                                <a href='<?php echo wp_nonce_url("?page={$plugin_page}&task=duplicate&view=list&calendar_id={$calendar_id}&event_id={$event->id}"); ?>'><?php $stachethemes_ec_main->lang('Duplicate', true); ?></a>
                                <a href='<?php echo ("?page={$plugin_page}&view=edit&calendar_id={$calendar_id}&event_id={$event->id}"); ?>'><?php $stachethemes_ec_main->lang('Edit', true); ?></a>

                            <?php endif; ?>
                            
                            <a class='delete-item' data-confirm='<?php $stachethemes_ec_main->lang('Delete item?',true); ?>' href='<?php echo wp_nonce_url("?page={$plugin_page}&calendar_id={$calendar_id}&task=delete&event_id={$event->id}"); ?>'><?php $stachethemes_ec_main->lang('Delete', true); ?></a>
                            </div>
                        </li>
                        <?php
                    endforeach;

                echo '</ul>';
            endif;
            ?>

        </div>

    <?php endif; ?>

</div>