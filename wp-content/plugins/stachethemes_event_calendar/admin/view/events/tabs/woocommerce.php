<div class="stachethemes-admin-section-tab" data-tab="woocommerce">

    <?php  
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Add Woocommerce Product')); 
    
    $products_list = stachethemes_ec_admin::wc()->get_products_as_array_list();

    stachethemes_ec_admin::html_select('wc_item_list', $products_list, '', false);

    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add to Event'), '', false, 'add-wc-item blue-button');
    ?>

    <!-- product template --> 
    <div class="stachethemes-admin-woocommerce-product stachethemes-admin-woocommerce-product-template">

        <div class="stachethemes-admin-woocommerce-product-head">
            <p class="stachethemes-admin-woocommerce-product-title"><span></span><span>stec_replace_wc_product_name</span></p>
            <input type="hidden" name="wc_product[]" value="stec_replace_wc_product_id" />
            <div>
                <?php
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Remove'), '', true, "light-btn delete");
                ?>
            </div>
        </div>

    </div>
    
    <?php if ($event) : ?>
    
        <?php foreach($event->woocommerce as $product) : ?>
                
            <?php
                $product_info = WC()->product_factory->get_product($product->product_id);
            ?>
        
            <div class="stachethemes-admin-woocommerce-product">

                <div class="stachethemes-admin-woocommerce-product-head">
                    <p class="stachethemes-admin-woocommerce-product-title"><span></span><span><?php echo $product_info->get_title(); ?></span></p>
                    <input type="hidden" name="wc_product[]" value="<?php echo $product_info->get_id(); ?>" />
                    <div>
                        <?php
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Remove'), '', true, "light-btn delete");
                        ?>
                    </div>
                </div>

            </div>
    
        <?php endforeach; ?>
    
    <?php endif;?>
    
</div>