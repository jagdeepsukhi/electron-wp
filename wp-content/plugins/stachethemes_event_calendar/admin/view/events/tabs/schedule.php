<div class="stachethemes-admin-section-tab" data-tab="schedule">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Schedule Timespan'));

    if ($event) :
        foreach ($event->schedule as $schedule) :
            ?>
            <div class="stachethemes-admin-schedule-timespan">

                <div class="stachethemes-admin-schedule-timespan-head">
                    <p class="stachethemes-admin-schedule-timespan-title"><span></span><span><?php $stachethemes_ec_main->lang('Schedule Timespan (optional)', true); ?></span></p>

                    <div>
                        <?php
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Expand'), '', true, "light-btn expand");
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Collapse'), '', true, "light-btn collapse");
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                        ?>
                    </div>
                </div>

                <div class="stachethemes-admin-schedule-toggle-wrap">
                    <?php
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Date and time'));
                    ?>
                    <div class="stachethemes-admin-section-flex">
                        <?php
                        $schedule_fulldate = explode(' ', $schedule->start_date);
                        $schedule_fulltime = explode(':', $schedule_fulldate[1]);

                        stachethemes_ec_admin::html_date("schedule[0][schedule_date_from]", $schedule_fulldate[0], '', $stachethemes_ec_main->lang('Date'), false);
                        stachethemes_ec_admin::html_select("schedule[0][schedule_time_hours_from]", stachethemes_ec_admin::hours_array(), $schedule_fulltime[0], '', true);
                        stachethemes_ec_admin::html_select("schedule[0][schedule_time_minutes_from]", stachethemes_ec_admin::minutes_array(), $schedule_fulltime[1], '', true);
                        ?>
                    </div>
                    <?php
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Title'));
                    stachethemes_ec_admin::html_input("schedule[0][schedule_title]", $schedule->title, '', $stachethemes_ec_main->lang('Timespan Title'), false);
                    
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Icon'));
                    stachethemes_ec_admin::html_select("schedule[0][schedule_icon]", stachethemes_ec_admin::icon_list(), $schedule->icon, false);
                    
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Icon Color'));
                    stachethemes_ec_admin::html_color("schedule[0][schedule_icon_color]", $schedule->icon_color, "#000000");
                    
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Details'));
                    stachethemes_ec_admin::html_textarea("schedule[0][schedule_details]", $schedule->description, '', $stachethemes_ec_main->lang('Timespan Details'), false);
                    ?>
                </div>

                <?php stachethemes_ec_admin::html_hidden("schedule[0][id]", $schedule->id); ?>
            </div>

            <?php
        endforeach;

    endif;
    ?>

    <!-- schedule template --> 
    <div class="stachethemes-admin-schedule-timespan stachethemes-admin-schedule-timespan-template">

        <div class="stachethemes-admin-schedule-timespan-head">
            <p class="stachethemes-admin-schedule-timespan-title"><span></span><span><?php $stachethemes_ec_main->lang('Schedule Timespan (optional)', true); ?></span></p>

            <div>
                <?php
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Expand'), '', true, "light-btn expand");
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Collapse'), '', true, "light-btn collapse");
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                ?>
            </div>
        </div>

        <div class="stachethemes-admin-schedule-toggle-wrap">
            <?php
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Date and time'));
            ?>
            <div class="stachethemes-admin-section-flex">
                <?php
                stachethemes_ec_admin::html_date('schedule[0][schedule_date_from]', '', '', $stachethemes_ec_main->lang('Date'), false);
                stachethemes_ec_admin::html_select('schedule[0][schedule_time_hours_from]', stachethemes_ec_admin::hours_array(), '00', '', true);
                stachethemes_ec_admin::html_select('schedule[0][schedule_time_minutes_from]', stachethemes_ec_admin::minutes_array(), '00', '', true);
                ?>
            </div>
            <?php
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Title'));
            stachethemes_ec_admin::html_input('schedule[0][schedule_title]', '', '', $stachethemes_ec_main->lang('Timespan Title'), false);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Icon'));
            stachethemes_ec_admin::html_select("schedule[0][schedule_icon]", stachethemes_ec_admin::icon_list(), 'fa', false);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Icon Color'));
            stachethemes_ec_admin::html_color("schedule[0][schedule_icon_color]", "#000000", "#000000");
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Details'));
            stachethemes_ec_admin::html_textarea('schedule[0][schedule_details]', '', '', $stachethemes_ec_main->lang('Timespan Details'), false);
            ?>
        </div>
    </div>


    <?php
    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add Timespan'), false, false, 'add-schedule-timespan');
    ?>

</div>
