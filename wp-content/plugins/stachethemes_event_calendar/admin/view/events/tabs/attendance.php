
<div class="stachethemes-admin-section-tab" data-tab="attendance">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Invite by e-mail'));

    stachethemes_ec_admin::html_input('attendee', '', '', $stachethemes_ec_main->lang('E-Mail Address'), false);

    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Send Invitation'), '', false, 'add-attendee-mail blue-button');
    ?>

    <ul class="attendee-email-list" start="1">
        <li>
            %mail%<span class="attendee-email-remove"><?php $stachethemes_ec_main->lang('Remove', true); ?></span>
            <?php stachethemes_ec_admin::html_hidden('attendee[][email]', '%mail%'); ?>
        </li>

        <?php
        if ( $event ) :
            foreach ( $event->attendance as $attendee ) : 
            
                if ( $attendee->userid || $attendee->email == '' ) {
                    continue;
                }
                ?>
                <li>
                    <?php echo $attendee->email; ?><span class="attendee-email-remove"><?php $stachethemes_ec_main->lang('Remove', true); ?></span>
                    <?php stachethemes_ec_admin::html_hidden('attendee[][email]', $attendee->email); ?>
                    <?php stachethemes_ec_admin::html_hidden('attendee[][id]', $attendee->id); ?>
                </li>

                <?php
            endforeach;
        endif;
        ?>
    </ul>

    <div class="stachethemes-admin-section-tab-attendance-wrap">

        <div class="attendance-all">
            <p><?php $stachethemes_ec_main->lang('All users', true); ?></p>
            <?php
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Invite All'), '', true, 'invite-all light-btn');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Uninvite All'), '', true, 'uninvite-all light-btn');
            ?>
        </div>
        <?php
        echo '<ul class="attendee-list">';

        foreach ( $users as $user ) :
            ?>
            <li data-userid="<?php echo $user->ID; ?>">
                <p><?php echo $user->display_name; ?></p>
                <div>
                    <?php
                    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Invite'), '', true, 'light-btn invite-user');
                    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Uninvite'), '', true, 'light-btn uninvite-user');
                    ?>
                </div>

                <?php
                if ( $event ) :
                    foreach ( $event->attendance as $attendee ) :
                        if ( $attendee->userid == '' ) {
                            continue;
                        }

                        if ( $attendee->userid == $user->ID ) {
                            echo "<input type='hidden' name='attendee[][userid]' value='{$user->ID}' />";
                            echo "<input type='hidden' name='attendee[][id]' value='{$attendee->id}' />";
                        }
                    endforeach;
                endif;
                ?>  

            </li>
            <?php
        endforeach;

        echo '</ul>';
        ?>

    </div>

</div>

