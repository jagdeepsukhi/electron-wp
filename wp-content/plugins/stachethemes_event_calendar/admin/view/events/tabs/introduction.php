<div class="stachethemes-admin-section-tab" data-tab="introduction">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Images (optional)'));
    stachethemes_ec_admin::html_add_image('images[]', $event ? $event->images : false, $stachethemes_ec_main->lang('Add Images'), false);
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Introduction'));
    wp_editor($event ? $event->description : null, 'description', array(
        'editor_height' => 250
    ));
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Short Description '));
    stachethemes_ec_admin::html_textarea('description_short', $event ? $event->description_short : null, '', $stachethemes_ec_main->lang('Short info about the event'), false);
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('External Link'));
    stachethemes_ec_admin::html_input('link', $event ? $event->link : null, '', $stachethemes_ec_main->lang('External Link'), false);
    ?>

</div>