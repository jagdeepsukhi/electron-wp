<div class="stachethemes-admin-section-tab" data-tab="general">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired name of the event (Summary)'));
    stachethemes_ec_admin::html_input('summary', $event ? $event->summary : null, '', $stachethemes_ec_main->lang('Event Name'), true);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Title slug. Must be unique. Leave empty to auto-generate.'));
    stachethemes_ec_admin::html_input('alias', $event ? $event->alias : null, '', $stachethemes_ec_main->lang('Slug'), false);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired color. By default will use the calendar color.'));
    stachethemes_ec_admin::html_color('event_color', $event ? $event->color : null, $calendar->color);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Event Icon'));
    stachethemes_ec_admin::html_icon('icon', stachethemes_ec_admin::icon_list(), $event ? $event->icon : $calendar->icon, true);

    if ( $event_id !== false && isset($cal_array) && count($cal_array) > 1 ) {
        stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Calendar'));
        stachethemes_ec_admin::html_select('calid', $cal_array, $calendar_id, true);
        
        $calendars = stachethemes_ec_admin::calendars();
        foreach ( $calendars as $cal ) :
            stachethemes_ec_admin::html_hidden("calendar_colors_by_id[$cal->id]", $cal->color);
        endforeach;
    }

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Front-End Visibility'));
    stachethemes_ec_admin::html_select('visibility', stachethemes_ec_admin::event_visibility_list(), $event ? $event->visibility : null, true);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Featured'));
    stachethemes_ec_admin::html_select('featured', array(
            0 => $stachethemes_ec_main->lang('No'),
            1 => $stachethemes_ec_main->lang('Yes'),
            2 => $stachethemes_ec_main->lang('Yes with Background')
            ), $event ? $event->featured : 0, true);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Starts On'));
    ?>
    <div class="stachethemes-admin-section-flex">
        <?php
        if ( $event ) {
            $start_fulldate = explode(' ', $event->start_date);
            $start_fulltime = explode(':', $start_fulldate[1]);

            $end_fulldate = explode(' ', $event->end_date);
            $end_fulltime = explode(':', $end_fulldate[1]);
        }

        stachethemes_ec_admin::html_date('start_date', isset($start_fulldate) ? $start_fulldate[0] : null, '', $stachethemes_ec_main->lang('Start Date'), true);
        stachethemes_ec_admin::html_select('start_time_hours', stachethemes_ec_admin::hours_array(), isset($start_fulltime) ? $start_fulltime[0] : null, '', true);
        stachethemes_ec_admin::html_select('start_time_minutes', stachethemes_ec_admin::minutes_array(), isset($start_fulltime) ? $start_fulltime[1] : null, '', true);
        ?>
    </div>


    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Ends On'));
    ?>
    <div class="stachethemes-admin-section-flex">
        <?php
        stachethemes_ec_admin::html_date('end_date', isset($end_fulldate) ? $end_fulldate[0] : null, '', $stachethemes_ec_main->lang('End Date'), true);
        stachethemes_ec_admin::html_select('end_time_hours', stachethemes_ec_admin::hours_array(), isset($end_fulltime) ? $end_fulltime[0] : null, '', true);
        stachethemes_ec_admin::html_select('end_time_minutes', stachethemes_ec_admin::minutes_array(), isset($end_fulltime) ? $end_fulltime[1] : null, '', true);
        ?>
    </div>

    <?php
    stachethemes_ec_admin::html_checkbox('all_day', ($event && $event->all_day ? 1 : 0), 0, $stachethemes_ec_main->lang('All Day'));
    ?>

    <?php
    
    if ( !isset($event->recurrence_id) || $event->recurrence_id == ''  ) :
        stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Repeater Scheme '));
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Set repeater'), '', false, "blue-button set-repeater-button");
        ?>
        <p class="stec-repeater-summary"><?php $stachethemes_ec_main->lang('Summary: ', true) ?>
            <span>-</span>
        </p>
    <?php endif; ?>

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Search keywords (optional)'));
    stachethemes_ec_admin::html_input('keywords', $event ? $event->keywords : null, '', $stachethemes_ec_main->lang('Search keywords'), false);
    ?>

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Counter'));
    stachethemes_ec_admin::html_select('counter', array(
            '0' => $stachethemes_ec_main->lang('Disable'),
            '1' => $stachethemes_ec_main->lang('Enable')
            )
            , $event ? $event->counter : null, true);
    ?>

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Comments'));
    stachethemes_ec_admin::html_select('comments', array(
            '0' => $stachethemes_ec_main->lang('Disable'),
            '1' => $stachethemes_ec_main->lang('Enable')
            )
            , $event ? $event->comments : null, true);
    ?>

</div>

<div class="stec-repeater-popup-bg"></div>
<div class="stec-repeater-popup" tabindex="0">
    <div>
        <table>
            <tbody>
                <tr>
                    <th><?php $stachethemes_ec_main->lang('Repeats:', true) ?></th>
                    <td>
                        <select name="repeat_freq">
                            <option value="0"><?php $stachethemes_ec_main->lang('No Repeat', true) ?></option>
                            <option value="1"><?php $stachethemes_ec_main->lang('Daily', true) ?></option>
                            <option value="2"><?php $stachethemes_ec_main->lang('Weekly', true) ?></option>
                            <option value="3"><?php $stachethemes_ec_main->lang('Monthly', true) ?></option>
                            <option value="4"><?php $stachethemes_ec_main->lang('Yearly', true) ?></option>
                        </select>
                    </td>
                </tr>

                <tr class="stec-repeater-popup-weekdays">
                    <th><?php $stachethemes_ec_main->lang('By Day:', true) ?></th>
                    <td class="stec-repeater-popup-repeat-on">
                        <div>
                            <span>
                                <input name="SU" title="<?php $stachethemes_ec_main->lang('Sunday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Sunday', true) ?>"><?php $stachethemes_ec_main->lang('SU', true) ?></label>
                            </span>
                            <span>
                                <input name="MO" title="<?php $stachethemes_ec_main->lang('Monday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Monday', true) ?>"><?php $stachethemes_ec_main->lang('MO', true) ?></label>
                            </span>
                            <span>
                                <input name="TU" title="<?php $stachethemes_ec_main->lang('Tuesday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Tuesday', true) ?>"><?php $stachethemes_ec_main->lang('TU', true) ?></label>
                            </span>
                            <span>
                                <input name="WE" title="<?php $stachethemes_ec_main->lang('Wednesday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Wednesday', true) ?>"><?php $stachethemes_ec_main->lang('WE', true) ?></label>
                            </span>
                            <span>
                                <input name="TH" title="<?php $stachethemes_ec_main->lang('Thursday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Thursday', true) ?>"><?php $stachethemes_ec_main->lang('TH', true) ?></label>
                            </span>
                            <span>
                                <input name="FR" title="<?php $stachethemes_ec_main->lang('Friday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Friday', true) ?>"><?php $stachethemes_ec_main->lang('FR', true) ?></label>
                            </span>
                            <span>
                                <input name="SA" title="<?php $stachethemes_ec_main->lang('Saturday', true) ?>" type="checkbox">
                                <label for="" title="<?php $stachethemes_ec_main->lang('Saturday', true) ?>"><?php $stachethemes_ec_main->lang('SA', true) ?></label>
                            </span>
                        </div>

                    </td>
                </tr>

            </tbody>
            <tbody>
                <tr>
                    <th><?php $stachethemes_ec_main->lang('Repeat gap:', true) ?></th>
                    <td>
                        <select name="repeat_gap">
                            <option value=""><?php $stachethemes_ec_main->lang('No gap', true) ?></option>
                            <?php
                            for ( $i = 2; $i < 31; $i++ ) {
                                ?>
                                <option value="<?php echo $i; ?>"><?php echo $i - 1; ?></option>
                                <?php
                            }
                            ?>
                        </select>

                    </td>
                </tr>
                <tr>
                    <th>
                        <?php $stachethemes_ec_main->lang('Repeat ends on:', true) ?>
                    </th>
                    <td class="stec-repeater-popup-endson-options">
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-never" name="repeat_endson" value="0" checked="checked" type="radio">
                            <label for="stec-repeater-popup-repeat-endson-never"><?php $stachethemes_ec_main->lang('Never', true) ?></label>
                        </span>
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-after-n" name="repeat_endson" value="1" type="radio">
                            <label for="stec-repeater-popup-repeat-endson-after-n">
                                <?php $stachethemes_ec_main->lang('After', true) ?> 
                                <input name="repeat_occurences" size="3" value="" disabled="disabled">
                                <?php $stachethemes_ec_main->lang('occurences', true) ?>
                            </label>
                        </span>
                        <span>
                            <input id="stec-repeater-popup-repeat-endson-date" name="repeat_endson" value="2" type="radio">
                            <label for="stec-repeater-popup-repeat-endson-date">
                                <?php $stachethemes_ec_main->lang('On Date', true) ?>
                                <input id="repeat_end_date" name="repeat_ends_on_date" size="10" value="" disabled="disabled" autocomplete="off">
                            </label>
                        </span>
                    </td>
                </tr>
                <tr class="stec-repeater-popup-exdate-menu">
                    <th>
                        <?php $stachethemes_ec_main->lang('Date Exception:', true) ?>
                    </th>
                    <td class="stec-repeater-popup-exdate-options">
                        <input id="stec-repeater-popup-exdate-datepicker" value="" type="text">
                        <input id="stec-repeater-popup-exdate-datepicker-exdate-value" value="" type="hidden">
                        <a id="stec-add-exdate" href="javascript:void(0);"><?php $stachethemes_ec_main->lang('Add exdate', true); ?></a>
                        <ul class="stec-repeater-popup-exdate-datelist">
                            <li class="stec-repeater-popup-exdate-datelist-template">
                                <span>stec_replace_date</span>
                                <span class="stec-repeater-popup-exdate-datelist-submit-value">stec_replace_altdate</span>
                                <a class="stec-remove-exdate" href="javascript:void(0);">
                                    <?php $stachethemes_ec_main->lang('Remove', true); ?>
                                </a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th><?php $stachethemes_ec_main->lang('Summary:', true) ?></th>
                    <td class="stec-repeater-popup-repeat-summary">-</td>
                </tr>
            </tbody>
            <tbody class="stec-repeater-popup-repeat-advanced">
                <tr>
                    <th><?php $stachethemes_ec_main->lang('RRULE String:', true) ?></th>
                    <td>
                        <input type="text" name="advanced_rrule" value="<?php echo $event ? $event->rrule : null; ?>" />
                        <input type="hidden" name="is_advanced_rrule" value="<?php echo $event ? $event->is_advanced_rrule : 0; ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="stec-repeater-popup-buttons">
        <?php
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Update'), '', false, "blue-button");
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Cancel'), '', true, "blue-button");
        ?>
    </div>
    <?php
    stachethemes_ec_admin::html_hidden('rrule', $event ? $event->rrule : '');
    stachethemes_ec_admin::html_hidden('exdate', $event ? $event->exdate : '');
    ?>
</div>