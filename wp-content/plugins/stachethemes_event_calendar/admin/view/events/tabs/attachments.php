
<div class="stachethemes-admin-section-tab" data-tab="attachments">

    <?php stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Attachments List')); ?>

    <?php if ($event) : foreach ($event->attachments as $attachment) : ?>

            <?php $filename = basename(get_attached_file($attachment->attachment, true)); ?>

            <div class="stachethemes-admin-attachments-attachment">

                <div class="stachethemes-admin-attachments-attachment-head">
                    <p class="stachethemes-admin-attachments-attachment-title"><span><?php echo $filename; ?></span></p>

                    <div>
                        <?php
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                        ?>
                    </div>
                </div>

                <?php
                stachethemes_ec_admin::html_hidden('attachment[0][id]', $attachment->attachment);
                ?>
            </div>

        <?php endforeach;
    endif; ?>

    <div class="stachethemes-admin-attachments-attachment stachethemes-admin-attachments-attachment-template">

        <div class="stachethemes-admin-attachments-attachment-head">
            <p class="stachethemes-admin-attachments-attachment-title"><span>%title%</span></p>

            <div>
                <?php
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                ?>
            </div>
        </div>

        <?php
        stachethemes_ec_admin::html_hidden('attachment[0][id]', '%id%');
        ?>
    </div>

    <?php
    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add Attachment'), false, false, 'add-attachments-attachment');
    ?>

</div>
