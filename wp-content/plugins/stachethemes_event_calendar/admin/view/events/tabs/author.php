<div class="stachethemes-admin-section-tab" data-tab="author">

    <?php
    $user_info = false;

    if ( (int) $event->created_by != 0 ) {
        $user_info = get_user_by('id', $event->created_by);
    }
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Created by'));
    stachethemes_ec_admin::html_input('', $user_info ? $user_info->display_name . " (id#$user_info->ID) " : null, 'Anonymous', '', false, "text", 'disabled');

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('User E-Mail'));
    stachethemes_ec_admin::html_input('', $user_info ? $user_info->user_email : $event->contact_email, '', '', false, "text", 'disabled');

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('User Roles'));
    stachethemes_ec_admin::html_input('', $user_info ? implode(',', $user_info->roles) : $stachethemes_ec_main->lang('No Roles'), '', '', false, "text", 'disabled');
    
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Submitted on'));
    stachethemes_ec_admin::html_input('', $event->created, '', '', false, "text", 'disabled');

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Note to reviewer'));
    stachethemes_ec_admin::html_textarea('', $event->review_note, "", "", false, 'disabled');
    ?>

</div>
