<div class="stachethemes-admin-section-tab" data-tab="guests">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Guests List'));

    if ($event) : foreach ($event->guests as $guest) :
            ?>

            <div class="stachethemes-admin-guests-guest">

                <div class="stachethemes-admin-guests-guest-head">
                    <p class="stachethemes-admin-guests-guest-title"><span></span><span><?php $stachethemes_ec_main->lang('Add Guest (optional)', true); ?></span></p>

                    <div>
                        <?php
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Expand'), '', true, "light-btn expand");
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Collapse'), '', true, "light-btn collapse");
                        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                        ?>
                    </div>
                </div>

                <div class="stachethemes-admin-guests-toggle-wrap">

                    <?php
                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Photo'));
                    stachethemes_ec_admin::html_add_image('guests[0][photo]', $guest->photo, $stachethemes_ec_main->lang('Add Photo'), false, true);

                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Name'));
                    stachethemes_ec_admin::html_input('guests[0][name]', $guest->name, '', $stachethemes_ec_main->lang('Name'), false);

                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('About'));
                    stachethemes_ec_admin::html_textarea('guests[0][about]', $guest->about, '', $stachethemes_ec_main->lang('About your guest'), false);

                    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Social Links'));

                    $guest_social_links = explode('||', $guest->links);

                    foreach ($guest_social_links as $link) :
                        $link = explode('::', $link);

                        if ($link[0] == "") {
                            continue;
                        }
                        ?>
                        <div class="stachethemes-admin-section-flex-guest-social">
                            <?php
                            stachethemes_ec_admin::html_select('guests[0][social][0][ico]', stachethemes_ec_admin::social_array(), $link[0]);
                            stachethemes_ec_admin::html_input('guests[0][social][0][link]', $link[1], '', $stachethemes_ec_main->lang('Link'));
                            ?>
                            <i class="stachethemes-admin-guests-social-remove fa fa-times"></i>
                        </div>
                        <?php
                    endforeach;
                    ?>

                    <div class="stachethemes-admin-section-flex-guest-social">
                        <?php
                        stachethemes_ec_admin::html_select('guests[0][social][0][ico]', stachethemes_ec_admin::social_array(), '');
                        stachethemes_ec_admin::html_input('guests[0][social][0][link]', '', '', $stachethemes_ec_main->lang('Link'));
                        ?>
                        <i class="stachethemes-admin-guests-social-remove fa fa-times"></i>
                    </div>

                    <?php
                    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add social link'), false, false, 'light-btn add-guests-soclink');
                    ?>

                    <?php stachethemes_ec_admin::html_hidden("guest[0][id]", $guest->id); ?>
                </div>

            </div>

        <?php endforeach;
    endif; ?>

    <!-- guest template --> 
    <div class="stachethemes-admin-guests-guest stachethemes-admin-guests-guest-template">

        <div class="stachethemes-admin-guests-guest-head">
            <p class="stachethemes-admin-guests-guest-title"><span></span><span><?php $stachethemes_ec_main->lang('Add Guest (optional)', true); ?></span></p>

            <div>
                <?php
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Expand'), '', true, "light-btn expand");
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Collapse'), '', true, "light-btn collapse");
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete'), '', true, "light-btn delete");
                ?>
            </div>
        </div>

        <div class="stachethemes-admin-guests-toggle-wrap">

            <?php
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Photo'));
            stachethemes_ec_admin::html_add_image('guests[0][photo]', false, $stachethemes_ec_main->lang('Add Photo'), false, true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Name'));
            stachethemes_ec_admin::html_input('guests[0][name]', '', '', $stachethemes_ec_main->lang('Name'), false);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('About'));
            stachethemes_ec_admin::html_textarea('guests[0][about]', '', '', $stachethemes_ec_main->lang('About your guest'), false);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Social Links'));
            ?>
            <div class="stachethemes-admin-section-flex-guest-social">
                <?php
                stachethemes_ec_admin::html_select('guests[0][social][0][ico]', stachethemes_ec_admin::social_array(), '', true);
                stachethemes_ec_admin::html_input('guests[0][social][0][link]', '', '', $stachethemes_ec_main->lang('Link'));
                ?>
                <i class="stachethemes-admin-guests-social-remove fa fa-times"></i>
            </div>
            <?php
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add social link'), false, false, 'light-btn add-guests-soclink');
            ?>
        </div>

    </div>


    <?php
    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add Guest'), false, false, 'add-guests-guest');
    ?>

</div>
