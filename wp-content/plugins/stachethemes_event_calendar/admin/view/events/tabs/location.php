<div class="stachethemes-admin-section-tab" data-tab="location">

    <?php
    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Where (optional)'));
    stachethemes_ec_admin::html_input('location', $event ? $event->location : null, '', $stachethemes_ec_main->lang('Event Location'), false);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Location additional details (optional)'));
    stachethemes_ec_admin::html_textarea('location_details', $event ? $event->location_details : null, '', $stachethemes_ec_main->lang('Location Details'), false);

    stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Longitude and latitude coordinates in decimal format. Used by Weather service and optionally by Google Maps. (optional)'));
    stachethemes_ec_admin::html_input('location_forecast', $event ? $event->location_forecast : null, '', $stachethemes_ec_main->lang('Forecast Location'), false);
    
    stachethemes_ec_admin::html_checkbox('location_use_coord', $event && $event->location_use_coord ? true : false, false, $stachethemes_ec_main->lang('Search location by coordinates instead by address name. Check this box if google maps has problems finding your event location.'));
    ?>

</div>