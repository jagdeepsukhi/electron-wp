<!--

Add/edit event to calendar

-->

<div class="stachethemes-admin-wrapper">
    
    <?php if ($calendar) : ?>

        <h1>
            <?php
            $stachethemes_ec_main->lang('Events Management', true);

            if ($event) {
                echo " / " . $calendar->title . " / " . $event->summary . " - " . $stachethemes_ec_main->lang('Edit Event');
            } else {
                echo " / " . $calendar->title . " - " . $stachethemes_ec_main->lang('Add Event');
            }
            ?>
        </h1>

    <?php endif; ?>

    <ul class="stachethemes-admin-tabs-list">
        
        <?php if ($event && $event->approved == "0") : ?>
            <li data-tab="author"><?php $stachethemes_ec_main->lang('Author Info', true); ?></li>
        <?php endif; ?>
        
        <li data-tab="general"><?php $stachethemes_ec_main->lang('General', true); ?></li>
        <li data-tab="introduction"><?php $stachethemes_ec_main->lang('Introduction', true); ?></li>
        <li data-tab="location"><?php $stachethemes_ec_main->lang('Location', true); ?></li>
        <li data-tab="schedule"><?php $stachethemes_ec_main->lang('Schedule', true); ?></li>
        <li data-tab="guests"><?php $stachethemes_ec_main->lang('Guests', true); ?></li>
        <li data-tab="attendance"><?php $stachethemes_ec_main->lang('Attendance', true); ?></li>
        
        <?php if (class_exists( 'WooCommerce' )) : ?>
            <li data-tab="woocommerce"><?php $stachethemes_ec_main->lang('WooCommerce', true); ?></li>
        <?php endif; ?>
        
        <li data-tab="attachments"><?php $stachethemes_ec_main->lang('Attachments', true); ?></li>
    </ul>

    <div class="stachethemes-admin-section">

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page&calendar_id={$calendar_id}", "POST");

        if ($event && $event->approved == "0") {
            include('tabs/author.php');
        }
        
        include('tabs/general.php');
        include('tabs/location.php');
        include('tabs/introduction.php');
        include('tabs/schedule.php');
        include('tabs/guests.php');
        include('tabs/attendance.php');
        
        if (class_exists( 'WooCommerce' )) :
            include('tabs/woocommerce.php');
        endif;
        
        include('tabs/attachments.php');
        ?>

    </div>

    <div class="stachethemes-admin-separator"></div>
    <?php
    stachethemes_ec_admin::html_hidden('calendar_id', $calendar_id);
    stachethemes_ec_admin::html_hidden('approved', '1');

    if ($event_id !== false) {
        stachethemes_ec_admin::html_hidden('event_id', $event_id);
        stachethemes_ec_admin::html_hidden('task', 'update');
        
        if ($event->approved == '0') {
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Approve Event'));
        } else {
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Update Event'));
        }
        
    } else {
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Add Event'));
        stachethemes_ec_admin::html_hidden('task', 'create');
    }

    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Back'), "?page={$plugin_page}&calendar_id={$calendar_id}", true);

    stachethemes_ec_admin::html_form_end();
    ?>

</div>