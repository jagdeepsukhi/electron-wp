<div class="stachethemes-admin-wrapper">
    
    <?php
        $fontsizes = array();
        for ($i = 12; $i <= 100; $i++) {
            $fontsizes["{$i}px"] = "{$i}px";
        }
        
        $fontweights = array();
        for ($i = 1; $i <= 9; $i++) {
            $fontweights[$i*100] = $i*100;
        }
        
    ?>
    
    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1><?php $stachethemes_ec_main->lang('Fonts and Colors', true); ?></h1>
    
    <ul class="stachethemes-admin-tabs-list">
        <li data-tab="top"><?php $stachethemes_ec_main->lang('Top Navigation', true); ?></li>
        <li data-tab="agenda"><?php $stachethemes_ec_main->lang('Agenda Layout', true); ?></li>
        <li data-tab="monthweek"><?php $stachethemes_ec_main->lang('Month & Week Layout', true); ?></li>
        <li data-tab="day"><?php $stachethemes_ec_main->lang('Day Layout', true); ?></li>
        <li data-tab="preview"><?php $stachethemes_ec_main->lang('Event Preview', true); ?></li>
        <li data-tab="event"><?php $stachethemes_ec_main->lang('Event Content', true); ?></li>
        <li data-tab="tooltip"><?php $stachethemes_ec_main->lang('Tooltip', true); ?></li>
        <li data-tab="custom"><?php $stachethemes_ec_main->lang('Custom Style', true); ?></li>
    </ul>
      
    <div class="stachethemes-admin-section">

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");

        // STEP
        ?>
        
        <div class="stachethemes-admin-section-tab" data-tab="top">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_top"); ?>
            
        </div>
         
        <div class="stachethemes-admin-section-tab" data-tab="agenda">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_agenda"); ?>
                
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="monthweek">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_monthweek"); ?>
            
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="day">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_day"); ?>
            
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="preview">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_preview"); ?>
            
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="event">
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_event"); ?>
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="tooltip">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_tooltip"); ?>
            
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="custom">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_custom"); ?>
            
        </div>
        
        <div class="stachethemes-admin-section-tab" data-tab="notification">
            
            <?php stachethemes_ec_admin::build_admin_setting($plugin_page."_notification"); ?>
            
        </div>

        
        <?php
            stachethemes_ec_admin::build_admin_setting($plugin_page);
        ?>
        
    </div>
    
    <div class="stachethemes-admin-separator"></div>
    <?php

    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Save Settings'));
    stachethemes_ec_admin::html_hidden('task', 'save');
    stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Reset all'), wp_nonce_url("?page={$plugin_page}&task=reset"), true);

    stachethemes_ec_admin::html_form_end();
    
    ?>

</div>