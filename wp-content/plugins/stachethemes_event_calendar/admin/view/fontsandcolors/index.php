<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch(stachethemes_ec_admin::get("task")) {
    case 'reset' :
        
        stachethemes_ec_admin::verify_nonce('','GET');
        
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_top");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_agenda");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_monthweek");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_day");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_custom");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_preview");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_event");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_tooltip");
        stachethemes_ec_admin::reset_admin_settings($plugin_page."_custom");
        
        stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Settings Reset"));


      break;
}

// POST
switch(stachethemes_ec_admin::post("task")) {
    
    case 'save' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        stachethemes_ec_admin::update_admin_settings($plugin_page."_top");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_agenda");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_monthweek");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_day");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_custom");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_preview");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_event");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_tooltip");
        stachethemes_ec_admin::update_admin_settings($plugin_page."_custom");
        
        stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Settings updated"));

        
    break;
}


// VIEW
switch(stachethemes_ec_admin::get("view")) :
    
    default:
        include __dir__ . '/settings.php';
       
endswitch;



