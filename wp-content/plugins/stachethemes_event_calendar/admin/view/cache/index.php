<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();


// GET
switch(stachethemes_ec_admin::get("task")) :
    
    case 'delete_cache' :
        
        stachethemes_ec_admin::verify_nonce('', 'GET');
        
        $result = stachethemes_ec_admin::delete_cache();
        
        if ($result) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Cache files deleted"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete cache fiels!"), 'error');
        }
        
    break;
    
endswitch;

// POST
switch(stachethemes_ec_admin::post("task")) :
    
    case 'save' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        stachethemes_ec_admin::update_admin_settings($plugin_page);
        
        stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Settings updated"));
        
    break;
    
endswitch;

// VIEW
switch(stachethemes_ec_admin::get("view")) :
    
    case "cache":
    default:
        
        include __dir__ . '/cache.php';
        
endswitch;