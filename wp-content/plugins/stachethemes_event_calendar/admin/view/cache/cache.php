<div class="stachethemes-admin-wrapper">

    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1>
        <?php
        $stachethemes_ec_main->lang('Cache Settings', true);
        ?>
    </h1>
    
    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('Cache Settings', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");
        
        stachethemes_ec_admin::build_admin_setting($plugin_page);

        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Update Settings'));
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Delete Cache'), wp_nonce_url("?page={$plugin_page}&task=delete_cache"), true, 'blue-button');
        
        stachethemes_ec_admin::html_hidden('task', 'save');
        
        stachethemes_ec_admin::html_form_end();
        ?>

    </div>

</div>

