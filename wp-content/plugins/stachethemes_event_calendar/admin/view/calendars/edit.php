<!--

Create and list calendars

List with all created calendars
Calendars are like categories, you can store events in different 
calendars and show each calendar independently 

-->
<div class="stachethemes-admin-wrapper">
    
    <h1><?php $stachethemes_ec_main->lang('Calendars Management / Edit', true); ?></h1>

    <?php if (isset($calendar) && $calendar !== false) : ?>

        <div class="stachethemes-admin-section">

            <h2><?php $stachethemes_ec_main->lang('Edit calendar', true); ?></h2>

            <?php

            stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired name of the calendar:'));
            stachethemes_ec_admin::html_input('calendar_name', $calendar->title, '', $stachethemes_ec_main->lang('Calendar Name'), true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired color. This will be the default color of the events. Can be changed later.'));
            stachethemes_ec_admin::html_color('calendar_color', $calendar->color, '#f15e6e');
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Default Icon'));
            stachethemes_ec_admin::html_icon('calendar_icon', stachethemes_ec_admin::icon_list(), $calendar->icon, true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Timezone'));
            stachethemes_ec_admin::html_select('calendar_timezone', stachethemes_ec_admin::timezones_list(), $calendar->timezone, $stachethemes_ec_main->lang('Calendar Timezone'), true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Front-End Visibility'));
            stachethemes_ec_admin::html_select('calendar_visibility', stachethemes_ec_admin::calendar_visibility_list(), $calendar->visibility, $stachethemes_ec_main->lang('Calendar Front-End Visibility'), true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Back-End Visibility'));
            stachethemes_ec_admin::html_select('calendar_back_visibility', stachethemes_ec_admin::calendar_visibility_list(), $calendar->back_visibility, $stachethemes_ec_main->lang('Calendar Back-End Visibility'), true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Who can add events from the front-end'));
            stachethemes_ec_admin::html_select('calendar_writable', stachethemes_ec_admin::calendar_writable_list(), $calendar->writable , true);
            
            stachethemes_ec_admin::html_checkbox('calendar_req_approval', $calendar->req_approval ? true : false, true, $stachethemes_ec_main->lang('Require approval by admin for events added from font-end'), false);
            
            stachethemes_ec_admin::html_hidden('calendar_id', $calendar->id);
            stachethemes_ec_admin::html_hidden('task', 'update');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Update Calendar'));
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Go Back'), "?page={$plugin_page}", true);

            stachethemes_ec_admin::html_form_end();
            ?>

        </div>

    <?php endif; ?>

</div>
