<!--

Create and list calendars

List with all created calendars
Calendars are like categories, you can store events in different 
calendars and show each calendar independently 

-->

<div class="stachethemes-admin-wrapper">
    
    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1><?php $stachethemes_ec_main->lang('Calendars Management', true); ?></h1>

    <div class="stachethemes-admin-section">
        
        <h2><?php $stachethemes_ec_main->lang('Create new calendar', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired name of the calendar:'));
            stachethemes_ec_admin::html_input('calendar_name', '', '', $stachethemes_ec_main->lang('Calendar Name'), true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Desired color. This will be the default color of the events. Can be changed later.'));
            stachethemes_ec_admin::html_color('calendar_color', '', '#f15e6e');
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Default Icon'));
            stachethemes_ec_admin::html_icon('calendar_icon', stachethemes_ec_admin::icon_list(), null, true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Timezone'));
            stachethemes_ec_admin::html_select('calendar_timezone', stachethemes_ec_admin::timezones_list(), "UTC", $stachethemes_ec_main->lang('Calendar Timezone'), true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Front-End Visibility'));
            stachethemes_ec_admin::html_select('calendar_visibility', stachethemes_ec_admin::calendar_visibility_list(), '1', $stachethemes_ec_main->lang('Calendar Front-End Visibility'), true);
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Back-End Visibility'));
            stachethemes_ec_admin::html_select('calendar_back_visibility', stachethemes_ec_admin::calendar_visibility_list(), '1', $stachethemes_ec_main->lang('Calendar Back-End Visibility'), true);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Who can add events from the front-end'));
            stachethemes_ec_admin::html_select('calendar_writable', stachethemes_ec_admin::calendar_writable_list(), '0', true);

            stachethemes_ec_admin::html_checkbox('calendar_req_approval', true, true, $stachethemes_ec_main->lang('Require approval by admin for events added from font-end'), false);
        
        stachethemes_ec_admin::html_hidden('task', 'create');
        stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Create Calendar'));

        stachethemes_ec_admin::html_form_end();
        ?>

    </div>

    <h1><?php $stachethemes_ec_main->lang('Manage Calendars', true); ?></h1>

    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('List with your created calendars', true); ?></h2>

        <?php

        if (empty($calendars)) :

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('(No calendars to display)'));

        else :
            
            ?>
            <div class="stec-list-bulk">
                <div>
                    <input type="checkbox" name="all_calendars" value="1" /> <?php $stachethemes_ec_main->lang('Select all calendars'); ?>
                    <p><?php $stachethemes_ec_main->lang('Selected all calendars', true); ?></p>

                    <?php stachethemes_ec_admin::html_form_start("?page=$plugin_page"); ?>

                        <button class='delete-all-items' data-confirm='<?php $stachethemes_ec_main->lang('Delete all items?',true); ?>'><p><i class="fa fa-trash-o"></i><?php $stachethemes_ec_main->lang('Delete selected calendars', true); ?></p></button>

                        <?php 
                        stachethemes_ec_admin::html_hidden('task', 'delete_bulk');
                        ?>

                    <?php stachethemes_ec_admin::html_form_end(); ?>
                </div>   
            </div>
            <?php

            echo '<ul class="stec-list">';

            foreach ($calendars as $calendar) :
                
                $add_text = '';
                $aaproval_count = stachethemes_ec_admin::get_aaproval_count($calendar->id);
                if ($aaproval_count > 0) {
                    $add_text = '-- '. $aaproval_count . ' ' . ($aaproval_count == 1 ? $stachethemes_ec_main->lang('event awaiting approval') : $stachethemes_ec_main->lang('events awaiting approval')) . ' --';
                }
                
                ?>
                <li>
                    <div class='calinfo'>
                        <input type="checkbox" name="calid" value="<?php echo $calendar->id; ?>" />
                        <p class='color' style='background: <?php echo $calendar->color; ?>'>
                            <i class="<?php echo $calendar->icon; ?>"></i>
                        </p>
                        <p class='title'><a href='<?php echo "?page=stec_menu__events&calendar_id={$calendar->id}"; ?>'><?php echo $calendar->title; ?> (id#<?php echo $calendar->id; ?>)</a> <span class="stec-text-red"><?php echo $add_text; ?></span></p>
                    </div>
                    <div class='ctrl'>
                        <a href='<?php echo "?page=stec_menu__events&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('View Events', true); ?></a>
                        <a href='<?php echo "?page={$plugin_page}&view=edit&calendar_id={$calendar->id}"; ?>'><?php $stachethemes_ec_main->lang('Edit', true); ?></a>
                        <a class='delete-item' data-confirm='<?php $stachethemes_ec_main->lang('Delete item?',true); ?>' href='<?php echo wp_nonce_url("?page={$plugin_page}&task=delete&calendar_id={$calendar->id}"); ?>'><?php $stachethemes_ec_main->lang('Delete', true); ?></a>
                    </div>
                </li>
                <?php
            endforeach;

            echo '</ul>';
        endif;
        ?>

    </div>

</div>
