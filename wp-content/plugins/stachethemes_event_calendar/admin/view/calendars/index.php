<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();


// GET
switch(stachethemes_ec_admin::get("task")) {
    
    case 'delete' :
        
        $calendar_id = (int) stachethemes_ec_admin::get("calendar_id", false, FILTER_VALIDATE_INT);
        
        stachethemes_ec_admin::verify_nonce('', 'GET');

        if (!$calendar_id) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar has no id"), 'error');
            break;
        }
        
        $result = stachethemes_ec_admin::delete_calendar($calendar_id);
        
        if ($result) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar deleted"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete calendar"), 'error');
        }
        
        
    break;
    
}

// POST
switch(stachethemes_ec_admin::post("task")) {
    
    case 'update' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        $calendar_id     = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);
        $title           = stachethemes_ec_admin::post('calendar_name', false, FILTER_SANITIZE_STRING);
        $color           = stachethemes_ec_admin::post('calendar_color', "#f15e6e", FILTER_SANITIZE_STRING);
        $icon            = stachethemes_ec_admin::post('calendar_icon', 'fa', FILTER_SANITIZE_STRING);
        $timezone        = stachethemes_ec_admin::post('calendar_timezone', "UTC", FILTER_SANITIZE_STRING);
        $back_visibility = stachethemes_ec_admin::post('calendar_back_visibility', "", FILTER_SANITIZE_STRING);
        $visibility      = stachethemes_ec_admin::post('calendar_visibility', "", FILTER_SANITIZE_STRING);
        $writable        = stachethemes_ec_admin::post('calendar_writable', "", FILTER_SANITIZE_STRING);
        $req_approval    = stachethemes_ec_admin::post('calendar_req_approval', false) ? 1 : 0;
        
        if (!$calendar_id) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar has no id"), 'error');
            break;
        }
        
        if (!$title) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar has no title"), 'error');
            break;
        }
        
        $result = stachethemes_ec_admin::update_calendar($title, $color, $icon, $timezone, $back_visibility, $visibility, $writable, $req_approval, $calendar_id);
        
        if ($result) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar updated"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to update calendar"), 'error');
        }
        
    break;
    
    case 'create' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        $title             = stachethemes_ec_admin::post('calendar_name', false, FILTER_SANITIZE_STRING);
        $color             = stachethemes_ec_admin::post('calendar_color', "#f15e6e", FILTER_SANITIZE_STRING);
        $icon              = stachethemes_ec_admin::post('calendar_icon', 'fa', FILTER_SANITIZE_STRING);
        $timezone          = stachethemes_ec_admin::post('calendar_timezone', "UTC", FILTER_SANITIZE_STRING);
        $visibility        = stachethemes_ec_admin::post('calendar_visibility', "", FILTER_SANITIZE_STRING);
        $back_visibility   = stachethemes_ec_admin::post('calendar_back_visibility', "", FILTER_SANITIZE_STRING);
        $writable          = stachethemes_ec_admin::post('calendar_writable', "", FILTER_SANITIZE_STRING);
        $req_approval      = stachethemes_ec_admin::post('calendar_req_approval', false) ? 1 : 0;
        
        if (!$title) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar has no title"), 'error');
            break;
        }
        
        $result = stachethemes_ec_admin::create_calendar($title, $color, $icon, $timezone, $back_visibility, $visibility, $writable, $req_approval);

        if ($result) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar created"));
        } else {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to create new calendar"), 'error');
        }
        
    break;
    
    case 'delete_bulk' :
        
        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");
        
        $calendars = stachethemes_ec_admin::post('idlist', false, FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);

        if ($calendars) {
            
            $result = stachethemes_ec_admin::delete_bulk_calendars($calendars);

            if ($result) {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendars deleted"));
            } else {
                stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Unable to delete calendars"), 'error');
            }
        }


    break;
}

// VIEW
switch(stachethemes_ec_admin::get("view")) :
    
    case "edit":
        
        $calendar_id = (int) stachethemes_ec_admin::get("calendar_id", false, FILTER_VALIDATE_INT);
        
        if (!$calendar_id) {
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang("Calendar has no id"), 'error');
        } else {
            $calendar = stachethemes_ec_admin::calendar($calendar_id);
        }
        
        include __dir__ . '/edit.php';
        
    break;
        
    case "list":
    default:
        
        $calendars = stachethemes_ec_admin::calendars();
        
        include __dir__ . '/list.php';
endswitch;



