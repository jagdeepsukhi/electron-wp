<div class="stachethemes-admin-wrapper">
    
    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1><?php $stachethemes_ec_main->lang('Stachethemes Event Calendar - Product License', true); ?></h1>
    

    <div id="stec-activator" class="stachethemes-admin-section">

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST");
        
            $activated = false;
            
            if (is_array($stachethemes_ec_main->lcns())) {
                $activated = true;
            } 

            if ($activated) {
                ?><h2><?php $stachethemes_ec_main->lang('Deactivate License', true); ?></h2><?php
            } else {
                ?><h2><?php $stachethemes_ec_main->lang('Activate License', true); ?></h2><?php
            }

            stachethemes_ec_admin::build_admin_setting($plugin_page);
            
            ?>
            <div class="stec-activator-beforesend">
            <?php stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Connecting to server...')); ?>
            </div>
            <div class="stec-activator-success">
            <?php stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Validating...')); ?>
            </div>
            <?php
            
            if ($activated) {
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Deactivate'));
                stachethemes_ec_admin::html_hidden('task', 'deactivate');
            } else {
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Activate'));
                stachethemes_ec_admin::html_hidden('task', 'activate');
            }
            
            stachethemes_ec_admin::html_hidden('action', 'stec_ajax_action');
            
        stachethemes_ec_admin::html_form_end();
        ?>
        
    </div>
</div>
