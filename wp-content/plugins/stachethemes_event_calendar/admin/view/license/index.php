<?php
global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch ( stachethemes_ec_admin::get("task") ) {
    
}

// POST
switch ( stachethemes_ec_admin::post("task") ) {

    case 'activate' :

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");

        $server_name   = implode(array_slice(explode('/', preg_replace('/https?:\/\/(www\.)?/', '', $_SERVER['SERVER_NAME'])), 0, 1));
        $purchase_code = stachethemes_ec_admin::post('purchase_code', false, FILTER_SANITIZE_STRING);

        $result = stachethemes_ec_admin::activate_license($purchase_code, $server_name);

        if ( $result === true ) {

            stachethemes_ec_admin::update_admin_settings($plugin_page);
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang('License activated'));
        } else {

            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang('Error activating license'), 'error');
        }

        break;

    case 'deactivate' :

        stachethemes_ec_admin::verify_nonce("?page={$plugin_page}");

        $server_name   = implode(array_slice(explode('/', preg_replace('/https?:\/\/(www\.)?/', '', $_SERVER['SERVER_NAME'])), 0, 1));
        $purchase_code = stachethemes_ec_admin::post('purchase_code', false, FILTER_SANITIZE_STRING);

        $result = stachethemes_ec_admin::deactivate_license($purchase_code, $server_name);

        if ( $result === true ) {

            stachethemes_ec_admin::update_admin_settings($plugin_page);
            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang('License deactivated'));
        } else {

            stachethemes_ec_admin::set_message($stachethemes_ec_main->lang('Error deactivating license'), 'error');
        }

        break;
}

// VIEW
switch ( stachethemes_ec_admin::get("view") ) :

    default:
        ?>
        <script type="text/javascript">
            var stec_ajax_nonce = "<?php echo wp_create_nonce("stec-nonce-string"); ?>";
        </script>
        <?php
        include __dir__ . '/license.php';
endswitch;



