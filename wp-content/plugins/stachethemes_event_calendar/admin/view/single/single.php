<div class="stachethemes-admin-wrapper">

    <?php echo stachethemes_ec_admin::display_message(); ?>

    <h1>
        <?php
        $stachethemes_ec_main->lang('Single Page Settings', true);
        ?>
    </h1>

    <div class="stachethemes-admin-section">

        <h2><?php $stachethemes_ec_main->lang('Create Single Page', true); ?></h2>

        <?php
        stachethemes_ec_admin::html_form_start("?page=$plugin_page", "POST", true);


        if ( $manual ) :
            
            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Set Single Page ID'));
            stachethemes_ec_admin::html_input('single_page_id', null);
            stachethemes_ec_admin::html_hidden('task', 'stec-single-page-set-id');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Set page id'));
            
        else :

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Page Name'));
            stachethemes_ec_admin::html_input('title', '', $single_page ? $single_page->post_title : null, $stachethemes_ec_main->lang('Page Name'), false);

            stachethemes_ec_admin::html_info($stachethemes_ec_main->lang('Slug (Must be unique). Leave empty to auto-generate slug.'));
            stachethemes_ec_admin::html_input('slug', '', $single_page ? $single_page->post_name : null, $stachethemes_ec_main->lang('Slug'), false);
            ?>

            <p class="desc">
                <?php $stachethemes_ec_main->lang('This will create a Wordpress Page with the shortcode [stachethemes_ec_single] inside.', true); ?>
                <br>
                <?php $stachethemes_ec_main->lang('It is required in order for the events\' single pages to work.', true); ?>
                <br>
                <?php $stachethemes_ec_main->lang('Ideally this page should not be visible in your Site Menu.', true); ?>
                <br><br>
                <strong><?php $stachethemes_ec_main->lang('NOTE: This page should not display your full calendar.', true); ?></strong>
            </p>

            <?php
            stachethemes_ec_admin::html_hidden('task', 'stec-single-page-create');
            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Save & Refresh rewrite rules'));

            if ( $single_page ) {
                stachethemes_ec_admin::html_hidden('single_page_id', $single_page->ID);
                stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Edit Page'), get_edit_post_link($single_page->ID), true);
//            stachethemes_ec_admin::html_button($stachethemes_ec_main->lang('Visit Page'), get_option('stec-single-page-url'), true);
            }

        endif;

        stachethemes_ec_admin::html_form_end();
        ?>


    </div>

</div>

