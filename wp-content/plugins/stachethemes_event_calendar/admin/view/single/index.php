<?php

global $plugin_page;
$stachethemes_ec_main = stachethemes_ec_main::get_instance();



// GET
switch ( stachethemes_ec_admin::get("task") ) :

endswitch;

// POST
switch ( stachethemes_ec_admin::post("task") ) :

    case 'stec-single-page-create' :

        break;


endswitch;

// VIEW
switch ( stachethemes_ec_admin::get("view") ) :

    case "single":
    default:

        $single_page    = false;
        $single_page_id = get_option('stec-single-page-id', false);

        $manual = stachethemes_ec_admin::get("manual", false);

        if ( $single_page_id ) {
            $single_page = get_post($single_page_id);
        }

        include __dir__ . '/single.php';

endswitch;
