<?php
/*
  Plugin Name: Stachethemes Event Calendar
  Version: 1.7.1
  Description: Stachethemes Event Calendar
  Author: Stachethemes
  Author URI: http://www.stachethemes.com/
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
  Text Domain: stec
  Domain Path: /languages/
 */







// Do not load directly
if ( !defined('ABSPATH') ) {
    die('-1');
}







require_once(dirname(__FILE__) . "/stachethemes/load.php");




class stachethemes_ec_main extends stachethemes\event_calendar\stachethemes_main_template {



    private static $_instance;



    private function __clone() {
        
    }



    private function __wakeup() {
        
    }



    public static function get_instance() {

        if ( null === static::$_instance ) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }



    protected function __construct() {

        parent::__construct();

        $this->load_textdomain("stec");

        include($this->get_path("LANG") . 'js-locale.php');

        add_action("init", array($this, "init"));

        add_action('init', function() {
            if ( !session_id() ) {
                session_start();
            }
        }, 1);

        add_action('wp_logout', function() {
            session_destroy();
        });

        add_action('wp_login', function() {
            session_destroy();
        });
    }



    public function init() {

        // Add meta links in dashboard/plugins/
        add_filter('plugin_row_meta', function($links, $file) {

            $stachethemes_ec_main = stachethemes_ec_main::get_instance();

            if ( $file == "stachethemes_event_calendar/stachethemes_event_calendar.php" ) {
                $row_meta = array(
                        'cc'      => '<a href="' . esc_url('http://codecanyon.net/item/stachethemes-event-calendar/16168229?ref=Stachethemes') . '">' . $stachethemes_ec_main->lang('Codecanyon') . '</a>',
                        'support' => '<a href="' . esc_url('http://codecanyon.net/item/stachethemes-event-calendar/16168229/support/contact?ref=Stachethemes') . '">' . $stachethemes_ec_main->lang('Support') . '</a>',
                        'license' => '<a href="' . get_admin_url(0, 'admin.php?page=stec_menu__license') . '">' . $stachethemes_ec_main->lang('Activator') . '</a>'
                );
                return array_merge($links, $row_meta);
            }
            return (array) $links;
        }, 10, 2);

        /**
         * Load classes 
         */
        $this->load_admin_lib(array(
                "ics/vendor/autoload.php",
        ));

        $this->load_admin_classes(array(
                "wc.php",
                "ical_import.php",
                "ical_export.php",
                "calendar.php"
        ));

        $this->load_front_classes(array(
                "calendar.php",
                "single.php",
                "query.php",
                "export.php",
                "create.event.php"
        ));



        /**
         * Admin Menus 
         */
        $this->add_menu("Stachethemes Event Calendar", "stec_menu__general", "dashicons-calendar-alt", 26);
        $this->add_submenu("General", "stec_menu__general");
        $this->add_submenu("Fonts & Colors", "stec_menu__fontsandcolors");

        $this->add_submenu("Calendars", "stec_menu__calendars");
        $this->add_submenu("Events", "stec_menu__events");
        $this->add_submenu("Import Events", "stec_menu__import");
        $this->add_submenu("Export Events", "stec_menu__export");
        $this->add_submenu("Single Page", "stec_menu__single");
        $this->add_submenu("Cache", "stec_menu__cache");
        $this->add_submenu("Import/Export Settings", "stec_menu__iesettings");
        $this->add_submenu("Product License", "stec_menu__license");

        $this->add_menu_font("stec-fa", "font-awesome-4.5.0/css/font-awesome.css");
        $this->add_menu_css("stec-ui", "//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css");
        $this->add_menu_css("stec-fonts", "//fonts.googleapis.com/css?family=Roboto:300,400,700");
        $this->add_menu_css("stec-css", "style.css");
        $this->add_menu_css("stec-colorpicker", "colorpicker/css/colorpicker.css");
        $this->add_menu_js("stec-colorpicker-js", "libs/colorpicker/colorpicker.js", 'jquery');
        $this->add_menu_js("stec-rrule-js", "libs/rrule.js");
        $this->add_menu_js("stec-nlp-js", "libs/nlp.js", "stec-rrule-js");
        $this->add_menu_js("stec-js", "admin.js", 'jquery-ui-datepicker,jquery-ui-sortable');
        // less compiler for admin section
//        $this->add_menu_js("stec-less-js", "less.js");

        $access_level = $this->get_admin_setting_value('stec_menu__general_other', 'admin_access_level');

        if ( current_user_can($access_level) || is_super_admin() ) {
            $this->register_menus();
        }


        /**
         * Shortcode 
         * [stachethemes_ec], [stachethemes_ec_single],  [stachethemes_ec_create_form], [stachethemes_ec_export]
         */
        require(dirname(__FILE__) . '/shortcode.stachethemes_ec.php');
        require(dirname(__FILE__) . '/shortcode.stachethemes_ec_single.php');
        require(dirname(__FILE__) . '/shortcode.stachethemes_ec_create_form.php');
        require(dirname(__FILE__) . '/shortcode.stachethemes_ec_create_form.php');
        require(dirname(__FILE__) . '/shortcode.stachethemes_ec_export.php');



        /**
         * Admin Ajax handler 
         */
        $this->ajax_action("stec_ajax_action", array($this, "stec_ajax_action"));

        /**
         * Public (inc nopriv) Ajax handler 
         */
        $this->ajax_public_action("stec_public_ajax_action", array($this, "stec_public_ajax_action"));


        /**
         * Add event inner tabs
         * You can comment out tab to disable it or change tabs order
         */
        $this->add_event_tab("intro", $this->lang('Event Info'), "fa fa-info", "", "tabs/intro.php");
        $this->add_event_tab("location", $this->lang('Location'), "fa fa-map-marker", "", "tabs/location.php");
        $this->add_event_tab("schedule", $this->lang('Schedule'), "fa fa-th-list", "", "tabs/schedule.php");
        $this->add_event_tab("guests", $this->lang('Guests'), "fa fa-star-o", "", "tabs/guests.php");
        $this->add_event_tab("attendance", $this->lang('Attendance'), "fa fa-user", "", "tabs/attendance.php");

        // if woocommerce is running
        if ( class_exists('WooCommerce') ) {
            $this->add_event_tab("woocommerce", $this->lang('Shop'), "fa fa-shopping-cart", "", "tabs/woocommerce.php");

            $wc = stachethemes_ec_admin::wc();
            $wc->add_filters();
        }

        $this->add_event_tab("forecast", $this->lang('Forecast'), "fa fa-sun-o", "", "tabs/forecast.php");
        $this->add_event_tab("comments", $this->lang('Comments'), "fa fa-commenting-o", '');

        /**
         * Display fixed message if any in session
         */
        if ( isset($_SESSION['stec-fixed-message']) ) {

            // force load scripts
            $this->force_load_scripts(true);

            $this->_display_fixed_message();
        }

        /**
         * Add rewrite rules
         */
        stachethemes_ec_single::add_rewrite_rules();

        /**
         * Intercept requests before headers sent
         */
        $this->_before_headers_requests_handler();


        /**
         * Check force load scripts
         */
        if ( $this->get_admin_setting_value('stec_menu__general_other', 'force_load_scripts') == '1' ) {
            $this->force_load_scripts(true);
        }

        /**
         * Display dashboard admin notices if any
         */
        $this->_admin_notices();
    }



    private function _admin_notices() {

        // Check for awaiting approval events and display notification if any

        $count = stachethemes_ec_admin::get_aaproval_count();

        if ( $count && $count > 0 ) :

            add_action('admin_notices', function() use($count) {

                $stachethemes_ec_main = stachethemes_ec_main::get_instance();
                ?>
                <div class="notice notice-info is-dismissible">

                    <p>
                        <?php
                        if ( $count == 1 ) {
                            printf($stachethemes_ec_main->lang('You have %d new event awaiting approval!'), $count);
                        } else {
                            printf($stachethemes_ec_main->lang('You have %d new events awaiting approval!'), $count);
                        }
                        ?>
                    </p>
                </div>
                <?php
            });

        endif;

        // Check if single page is set up

        $single_page = get_option('stec-single-page-url', false);

        if ( $single_page === false || $single_page == '' ) {

            $page = stachethemes_ec_admin::get('page');

            if ( $page != 'stec_menu__single' ) :

                add_action('admin_notices', function() {

                    $stachethemes_ec_main = stachethemes_ec_main::get_instance();
                    ?>
                    <div class="notice notice-info is-dismissible">
                        <p>
                            <?php
                            $url                  = get_admin_url(0, 'admin.php?page=stec_menu__single');
                            printf($stachethemes_ec_main->lang('You have not set up Single Page for Stachethemes Event Calendar yet! Click <a href="%s">here</a> to set up single page.'), $url);
                            ?>
                        </p>
                    </div>
                    <?php
                });

            endif;
        }
    }



    private function _theme_list() {
        return array();
    }



    public function lcns() {
        $theme = wp_get_theme();
        $nfo   = get_option('stec-activated', false);
        if ( isset($nfo['purchase_code']) ) {
            return $nfo;
        } else if ( in_array($theme->name, $this->_theme_list()) ) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Intercept $_GET/$_POST before Headers sent
     * When handling jobs requiring return before headers (like downloading a file)
     */
    private function _before_headers_requests_handler() {

        // Attendance by URL Listener
        $attendee_id  = filter_input(INPUT_GET, "stec_attendance", FILTER_VALIDATE_INT);
        $access_token = filter_input(INPUT_GET, "access_token", FILTER_SANITIZE_STRING);
        $status       = filter_input(INPUT_GET, "status", FILTER_VALIDATE_INT);

        if ( $attendee_id && $access_token && $status ) {

            $result = stachethemes_ec_admin::update_attendance_by_url($attendee_id, $access_token, $status);

            if ( $result === true ) {

                switch ( $status ) :

                    case 1:
                        $this->_set_fixed_message($this->lang('You have accepted event invitation'), 'success');
                        break;

                    case 2:
                        $this->_set_fixed_message($this->lang('You have declined event invitation'), 'error');
                        break;

                endswitch;
            }

            wp_redirect(site_url('/'));

            exit;
        }


        // $_GET
        switch ( stachethemes_ec_admin::get("task") ) {

            case 'stec_export_to_ics' :

                $calendar_id = stachethemes_ec_admin::get('calendar_id', false, FILTER_VALIDATE_INT);
                $event_id    = stachethemes_ec_admin::get('event_id', false, FILTER_VALIDATE_INT);

                if ( stachethemes_ec_admin::export_ics($calendar_id, $event_id) === false ) {

                    if ( is_admin() ) {
                        stachethemes_ec_admin::set_message($this->lang('Error exporting events'), 'error');
                    }
                }

                break;

            case 'stec_public_export_to_ics' :

                $calendar_id = stachethemes_ec_admin::get('calendar_id', false, FILTER_VALIDATE_INT);
                $event_id    = stachethemes_ec_admin::get('event_id', false, FILTER_VALIDATE_INT);
                
                stachethemes_event_calendar_export::export_ics($calendar_id, $event_id) ;
                        
                break;
        }

        // $_POST
        switch ( stachethemes_ec_admin::post("task") ) {


            case "stec_export_settings" :

                stachethemes_ec_admin::export_settings();

                exit;
                break;

            case "stec_import_settings" :

                if ( strtolower(pathinfo($_FILES["settings_filename"]["name"], PATHINFO_EXTENSION)) !== 'stec' ) {
                    stachethemes_ec_admin::set_message($this->lang("File extension is not .stec"), 'error');
                    break;
                }

                $file   = $_FILES["settings_filename"]["tmp_name"];
                $result = stachethemes_ec_admin::import_settings($file);

                if ( true === $result ) {
                    stachethemes_ec_admin::set_message($this->lang("Settings Imported"));
                } else {
                    stachethemes_ec_admin::set_message($this->lang("Error importing settings"), 'error');
                }

                break;

            case "stec-single-page-set-id":

                $single_page_id = filter_input(INPUT_POST, 'single_page_id', FILTER_VALIDATE_INT);

                if ( $single_page_id ) {
                    update_option('stec-single-page-id', $single_page_id);
                    stachethemes_ec_admin::set_message(sprintf($this->lang("Single Page ID set to %s"), $single_page_id));
                }

                break;
            case "stec-single-page-create":

                stachethemes_ec_admin::verify_nonce('?page=stec_menu__single');

                $title          = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
                $slug           = filter_input(INPUT_POST, 'slug', FILTER_SANITIZE_STRING);
                $single_page_id = filter_input(INPUT_POST, 'single_page_id', FILTER_VALIDATE_INT);

                // Create post 
                $id = stachethemes_ec_single::create_page($title, $slug, $single_page_id);

                if ( $id ) {

                    update_option('stec-single-page-id', $id);

                    if ( $single_page_id == 0 ) {
                        stachethemes_ec_admin::set_message($this->lang("Single Page Created"));
                    } else {
                        stachethemes_ec_admin::set_message($this->lang("Single Page Updated"));
                    }
                } else {

                    if ( $single_page_id == 0 ) {
                        stachethemes_ec_admin::set_message($this->lang("Unable to create Single Page"), 'error');
                    } else {
                        stachethemes_ec_admin::set_message($this->lang("Unable to update Single Page"), 'error');
                    }
                }

                stachethemes_ec_single::add_rewrite_rules();
                stachethemes_ec_single::update_rewrite_rules();

                break;

            case "stec_export_to_ics" :

                // used by front-end export
                $calendar_id = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);
                $event_id    = stachethemes_ec_admin::post('event_id', false, FILTER_VALIDATE_INT);

                stachethemes_ec_admin::export_ics($calendar_id, $event_id);

                exit;
                break;

            case 'stec_export_to_ics_bulk' :

                $calendar_id = stachethemes_ec_admin::post('calendar_id', false, FILTER_VALIDATE_INT);
                $idlist      = stachethemes_ec_admin::post('idlist', array(), FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);

                if ( stachethemes_ec_admin::export_ics($calendar_id, false, $idlist) === false ) {

                    stachethemes_ec_admin::set_message($this->lang('Error exporting events'), 'error');
                }

                break;
        }
    }



    /**
     * Method POST
     * @action stec_ajax_action
     * @return json
     */
    public function stec_ajax_action() {

        header('Content-Type: application/json');

        $task = filter_input(INPUT_POST, "task", FILTER_SANITIZE_STRING);

        switch ( $task ) :

            case 'throwerror_license' :

                stachethemes_ec_admin::verify_nonce('', 'AJAX');

                stachethemes_ec_admin::set_message($this->lang('License activator encountered an error. Make sure your purchase key is not activated somewhere else.'), 'error');

                echo json_encode(array(
                        'error' => 0,
                        'text'  => $this->lang('Success')
                ));

                break;

            case 'activate_license' :

                stachethemes_ec_admin::verify_nonce('', 'AJAX');

                $purchase_code = filter_input(INPUT_POST, 'purchase_code', FILTER_SANITIZE_STRING);
                $server_name   = filter_input(INPUT_POST, 'server_name', FILTER_DEFAULT);

                if ( !$purchase_code || !$server_name ) {

                    echo json_encode(array(
                            'error' => 1,
                            'text'  => $this->lang('Error occured')
                    ));

                    stachethemes_ec_admin::set_message($this->lang('Error activating license'), 'error');

                    break;
                }

                update_option('stec-activated', array(
                        'purchase_code' => $purchase_code,
                        'server_name'   => $server_name
                ));

                stachethemes_ec_admin::update_admin_settings('stec_menu__license');

                stachethemes_ec_admin::set_message($this->lang('License Activated'));

                echo json_encode(array(
                        'error' => 0,
                        'text'  => $this->lang('Success')
                ));

                break;

            case 'deactivate_license' :

                stachethemes_ec_admin::verify_nonce('', 'AJAX');

                $purchase_code = filter_input(INPUT_POST, 'purchase_code', FILTER_SANITIZE_STRING);
                $server_name   = filter_input(INPUT_POST, 'server_name', FILTER_DEFAULT);

                if ( !$purchase_code || !$server_name ) {

                    echo json_encode(array(
                            'error' => 1,
                            'text'  => $this->lang('Error occured')
                    ));

                    stachethemes_ec_admin::set_message($this->lang('Error deactivating license'), 'error');

                    break;
                }

                update_option('stec-activated', false);

                stachethemes_ec_admin::reset_admin_settings('stec_menu__license');

                stachethemes_ec_admin::set_message($this->lang('License Deactivated'));

                echo json_encode(array(
                        'error' => 0,
                        'text'  => $this->lang('Success')
                ));

                break;

        endswitch;

        exit;
    }



    /**
     * Method POST
     * @action stec_public_ajax_action
     * @return json
     */
    public function stec_public_ajax_action() {

        header('Content-Type: application/json');

        $task = filter_input(INPUT_POST, "task", FILTER_SANITIZE_STRING);

        switch ( $task ) :

            case 'front_delete_event' :

                $event_id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

                if ( $event_id === false ) {
                    echo json_encode(array(
                            'error' => 1,
                            'text'  => ''
                    ));
                }

                $result = stachethemes_event_calendar_create_event_front::delete_event($event_id);

                if ( $result === false ) {
                    echo json_encode(array(
                            'error' => 1,
                            'text'  => $this->lang('Error occured')
                    ));
                } else {

                    echo json_encode(array(
                            'error' => 0,
                            'text'  => $this->lang('Event deleted')
                    ));
                }

                break;

            case 'front_create_event' :

                if ( $this->get_admin_setting_value('stec_menu__general_google_captcha', 'enabled') == '1' ) {

                    $secret   = $this->get_admin_setting_value('stec_menu__general_google_captcha', 'secret_key');
                    $response = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_DEFAULT);
                    $ip       = $_SERVER['REMOTE_ADDR'];

                    $is_captcha_valid = stachethemes_event_calendar_create_event_front::validate_captcha($secret, $response, $ip);

                    if ( $is_captcha_valid !== true ) {

                        echo json_encode(array(
                                'error' => 1,
                                'text'  => $this->lang('Error occured')
                        ));

                        die();
                    }
                }

                $post_data = stachethemes_event_calendar_create_event_front::calendar_front_post_data();
                $image_id  = stachethemes_event_calendar_create_event_front::calendar_front_image_file_upload();

                if ( $image_id !== false ) {
                    $post_data->images = array((int) $image_id);
                }

                $the_event = stachethemes_event_calendar_create_event_front::create_event($post_data);

                if ( $the_event !== false ) {

                    if ( $the_event->approved == 0 ) {
                        // Send email to administrator
                        stachethemes_event_calendar_create_event_front::notify_admin($the_event->id);
                    }

                    echo json_encode(array(
                            'error' => 0,
                            'text'  => $this->lang('Event created'),
                            'event' => $the_event
                    ));
                } else {

                    echo json_encode(array(
                            'error' => 1,
                            'text'  => $this->lang('Error occured')
                    ));
                }

                break;

            case 'get_events' :

                $cal      = filter_input(INPUT_POST, 'cal', FILTER_SANITIZE_STRING);
                $min_date = filter_input(INPUT_POST, 'min_date', FILTER_SANITIZE_STRING);
                $max_date = filter_input(INPUT_POST, 'max_date', FILTER_SANITIZE_STRING);

                if ( empty($cal) ) {
                    $cal = false;
                }

                if ( $min_date ) {
                    $min_date = date('U', strtotime($min_date));
                }

                if ( $max_date ) {
                    $max_date = date('U', strtotime($max_date));
                }

                $events = stachethemes_event_calendar_query::get_events($cal, false, true, $min_date, $max_date);

                echo json_encode($events);

                break;

            case 'get_event_data' :

                $event_id = filter_input(INPUT_POST, 'event_id', FILTER_VALIDATE_INT);

                if ( $event_id === false ) {
                    return array();
                }

                $event = stachethemes_event_calendar_query::get_event_data($event_id, true);

                echo json_encode($event);

                break;

            case 'set_user_event_attendance' :

                $event_id = filter_input(INPUT_POST, 'event_id', FILTER_VALIDATE_INT);

                if ( $event_id === false ) {
                    return array();
                }

                if ( !is_user_logged_in() ) {
                    return array();
                }

                $user = wp_get_current_user();

                $status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT);
                $repeat_offset = filter_input(INPUT_POST, 'repeat_offset', FILTER_VALIDATE_INT);

                $event = stachethemes_event_calendar_query::set_user_event_attendance($event_id, $repeat_offset, $user->ID, $status);

                echo json_encode($event);

                break;

            case 'get_weather_data' :

                $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);

                $weather_data = stachethemes_event_calendar_query::get_weather_data($location);

                echo $weather_data;

                break;

            case 'add_reminder' :

                $event_id      = filter_input(INPUT_POST, 'event_id', FILTER_VALIDATE_INT);
                $repeat_offset = filter_input(INPUT_POST, 'repeat_offset', FILTER_VALIDATE_INT);
                $date          = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING);
                $email         = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

                $reminder = stachethemes_event_calendar_query::set_reminder($event_id, $repeat_offset, $email, $date);

                echo json_encode($reminder);

                break;

        endswitch;

        exit;
    }



    /**
     * Custom head attributes
     */
    public function load_head() {

        if ( $this->_loaded_head === true ) {
            return;
        }

        /**
         * Meta viewport tag for mobile devices
         * @todo make optional
         */
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php
        // admin fonts and colors
        $this->_generate_css();

        $this->_loaded_head = true;
    }



    public function add_event_tab($slug, $title, $icon, $content = "", $file = false) {

        add_action('stachethemes_ec_add_event_tab', function() use($slug, $title, $icon) {
            echo "<li data-tab='stec-layout-event-inner-{$slug}'><i class='{$icon}'></i><p>{$title}</p></li>";
        });

        add_action('stachethemes_ec_add_event_tab_content', function() use($slug, $content, $file) {

            $stachethemes_ec_main = stachethemes_ec_main::get_instance();

            echo "<div class='stec-layout-event-inner-{$slug}'>";

            if ( $file !== false ) :
                include($stachethemes_ec_main->get_path('FRONT_VIEW') . $file);
            endif;

            if ( $content !== "" ) :
                echo $content;
            endif;

            echo "</div>";
        });
    }



    private function _set_fixed_message($msg, $type = '') {

        $_SESSION['stec-fixed-message'] = array(
                'msg'  => $msg,
                'type' => $type
        );
    }



    private function _display_fixed_message() {

        $session_msg = $_SESSION['stec-fixed-message'];

        $msg  = $session_msg['msg'];
        $type = $session_msg['type'];

        switch ( $type ) :

            case 'success' :

                add_action('wp_footer', function() use($msg) {

                    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

                    ob_start();
                    include $stachethemes_ec_main->get_path('FRONT') . '/view/msg/success.php';
                    $html = ob_get_clean();
                    $html = preg_filter('/stec_replace_msg/', $msg, $html);
                    echo $html;
                });

                break;

            case 'error' :

                add_action('wp_footer', function() use($msg) {
                    ob_start();

                    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

                    include $stachethemes_ec_main->get_path('FRONT') . '/view/msg/error.php';
                    $html = ob_get_clean();
                    $html = preg_filter('/stec_replace_msg/', $msg, $html);
                    echo $html;
                });

                break;

            default :

                add_action('wp_footer', function() use($msg) {

                    $stachethemes_ec_main = stachethemes_ec_main::get_instance();

                    ob_start();
                    include $stachethemes_ec_main->get_path('FRONT') . '/view/msg/msg.php';
                    $html = ob_get_clean();
                    $html = preg_filter('/stec_replace_msg/', $msg, $html);
                    echo $html;
                });

                break;

        endswitch;

        unset($_SESSION['stec-fixed-message']);
    }



    private function _generate_css() {

        include_once dirname(__FILE__) . '/generate-css.php';
    }

}

$stachethemes_ec_main = stachethemes_ec_main::get_instance();






/**
 * Activation Hook 
 */
register_activation_hook(__FILE__, 'stec_on_activate');



function stec_on_activate($networkwide) {

    global $wpdb;

    if ( function_exists('is_multisite') && is_multisite() ) {

        // check if it is a network activation - if so, run the activation function for each blog id

        if ( $networkwide ) {

            $old_blog = $wpdb->blogid;

            // Get all blog ids
            $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

            foreach ( $blogids as $blog_id ) {
                switch_to_blog($blog_id);
                _stec_on_activate_tables_and_settings();
            }

            switch_to_blog($old_blog);
            return;
        }
    }

    _stec_on_activate_tables_and_settings();
}



function _stec_on_activate_tables_and_settings() {

    // sql
    require(dirname(__FILE__) . '/sql/install.php');

    // register plugin settings
    require(dirname(__FILE__) . '/register-settings.php');
}

/**
 * On theme switch
 */
add_action('after_switch_theme', function () {

    stachethemes_ec_single::add_rewrite_rules();
    stachethemes_ec_single::update_rewrite_rules();
});



/**
 * Deactivation hook
 */
register_deactivation_hook(__FILE__, 'stec_on_deactivate');



function stec_on_deactivate() {
    
}

/**
 * Un-install hook
 */
register_uninstall_hook(__FILE__, 'stec_on_uninstall');



function stec_on_uninstall() {

    global $wpdb;

    if ( function_exists('is_multisite') && is_multisite() ) {

        $old_blog = $wpdb->blogid;

        // Get all blog ids
        $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

        foreach ( $blogids as $blog_id ) {
            switch_to_blog($blog_id);
            _stec_on_uninstall_tables_and_settings();
        }

        switch_to_blog($old_blog);
        return;
    }

    _stec_on_uninstall_tables_and_settings();
}



function _stec_on_uninstall_tables_and_settings() {
    // sql
    require(dirname(__FILE__) . '/sql/uninstall.php');
}

/**
 * Register/De-register Cronjobs
 */
include(dirname(__FILE__) . '/cron.php');





/**
 *  Visual Composer Integration
 */
if ( function_exists("vc_map") ) {

    include(dirname(__FILE__) . '/vc-settings.php');
}