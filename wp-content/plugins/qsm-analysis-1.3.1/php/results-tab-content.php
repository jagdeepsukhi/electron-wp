<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the addon  settings page
 *
 * @since 1.3.0
 * @return void
 */
function qsm_addon_analysis_register_stats_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_stats_settings_tab( "Reporting And Analysis", 'qsm_addon_analysis_stats_tabs_content' );
  }
}

/**
 * Generates the content for your addon settings tab
 *
 * @since 1.3.0
 * @return void
 */
function qsm_addon_analysis_stats_tabs_content() {

  //Enqueue your scripts and styles
  wp_enqueue_script('plotlyjs', plugins_url( '../js/plotly.min.js' , __FILE__ ), array( 'jquery' ) );
  wp_enqueue_script( 'qsm_analysis_admin', plugins_url( '../js/qsm-analysis-admin.js' , __FILE__ ), array( 'plotlyjs', 'jquery' ) );
  wp_enqueue_style( 'qsm_analysis_admin_style', plugins_url( '../css/qsm-analysis-admin.css' , __FILE__ ) );
  wp_enqueue_script( 'jquery-ui-core' );
  wp_enqueue_script( 'jquery-ui-button' );
  wp_enqueue_script( 'jquery-ui-datepicker' );
  global $wpdb;
  //load all quizzes
  $quiz_data = $wpdb->get_results( "SELECT quiz_id, quiz_name	FROM " . $wpdb->prefix . "mlw_quizzes WHERE deleted=0 ORDER BY quiz_id DESC" );

  //display form to select quiz
  ?>
  <script>
  jQuery(function() {
    jQuery( "#date_start, #date_end" ).datepicker();
  });
  </script>
  <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" />
  <form action="" method="post" class="retrieve_stats_form">
    <select name="qmn_selected_quiz">
      <?php
      foreach ($quiz_data as $quiz) {
        echo '<option value="'.$quiz->quiz_id.'">'.$quiz->quiz_name.'</option>';
      }
      ?>
    </select>
    <button class="button-primary">Analysis Quiz Results</button>
    <?php wp_nonce_field('analyize_quiz_results','analyize_quiz_results_nonce'); ?>
  </form>
  <?php
  if ( isset( $_POST['analyize_quiz_results_nonce'] ) || isset( $_POST['analyze_quiz_results_filter_nonce'] ) ) {
    ?>
    <section class="qsm_filter_options">
      <form action="" method="post">
      <h3>Filter Results</h3>
      <label>Filter by Date (Leave blank for all results):</label>
      <input type="text" placeholder="Beginning Date" id="date_start" name="date_start"/>
      <input type="text" placeholder="End Date" id="date_end" name="date_end"/>
      <br>
      <label>Filter by User ID: </label>
      <input type="text" name="filter_user" />
      <br>
      <label>Filter by name: </label>
      <input type="text" name="filter_name" />
      <br>
      <label>Filter by business: </label>
      <input type="text" name="filter_business" />
      <?php wp_nonce_field('analyze_quiz_results_filter','analyze_quiz_results_filter_nonce'); ?>
      <input type="hidden" name="qmn_selected_quiz" value="<?php echo intval( $_POST["qmn_selected_quiz"] ); ?>" />
      <br>
      <button class="button-primary">Filter</button>
      </form>
    </section>

    <section class="qsm_filter_options">
      <h3>Export Results</h3>
      <p>Export results for quiz using current filters if any.</p>
      <button class="button-primary qsm_export_results_button">Export As CSV</button>
      <script>
        var filterData = {
          quizID: <?php echo intval( $_POST["qmn_selected_quiz"] ); ?>,
          user: <?php if ( isset( $_POST["filter_user"] ) ) { echo intval( $_POST["filter_user"] ); } else { echo "''"; } ?>,
          name: <?php if ( isset( $_POST["filter_name"] ) ) { echo "'".sanitize_text_field( $_POST["filter_name"] )."'"; } else { echo "''"; } ?>,
          business: <?php if ( isset( $_POST["filter_name"] ) ) { echo "'".sanitize_text_field( $_POST["filter_business"] )."'"; } else { echo "''"; } ?>,
          start_date: <?php if ( isset( $_POST["filter_name"] ) ) { echo "'".sanitize_text_field( $_POST["date_start"] )."'"; } else { echo "''"; } ?>,
          end_date: <?php if ( isset( $_POST["filter_name"] ) ) { echo "'".sanitize_text_field( $_POST["date_end"] )."'"; } else { echo "''"; } ?>,
        }
      </script>
    </section>
    <?php
  }

  //retrieve results from selected quiz
  if ( ( isset( $_POST['analyize_quiz_results_nonce'] ) && wp_verify_nonce( $_POST['analyize_quiz_results_nonce'], 'analyize_quiz_results') ) ||  ( isset( $_POST['analyze_quiz_results_filter_nonce'] ) && wp_verify_nonce( $_POST['analyze_quiz_results_filter_nonce'], 'analyze_quiz_results_filter') ) ) {

    $quiz_id = intval( $_POST["qmn_selected_quiz"] );
    $table_name = $wpdb->prefix . "mlw_results";
    if ( isset( $_POST['analyze_quiz_results_filter_nonce'] ) ) {
    $filter_sql = '';
    $filter_user = intval( $_POST["filter_user"] );
    $filter_name = sanitize_text_field( $_POST["filter_name"] );
    $filter_business = sanitize_text_field( $_POST["filter_business"] );
    if ( !empty( $_POST["date_start"] ) && !empty( $_POST["date_end"] ) ) {
      $start_date = date( "Y-m-d 00:00:00", strtotime( sanitize_text_field( $_POST["date_start"] ) ) );
      $end_date = date( "Y-m-d 23:59:59", strtotime( sanitize_text_field( $_POST["date_end"] ) ) );
      $filter_sql .= " AND (time_taken_real BETWEEN '$start_date' AND '$end_date')";
    }
    if ( !empty( $filter_user ) ) {
      $filter_sql .= " AND (user = $filter_user)";
    }
    if ( !empty( $filter_name ) ) {
      $filter_sql .= " AND (name LIKE '%$filter_name%')";
    }
    if ( !empty( $filter_business ) ) {
      $filter_sql .= " AND (business LIKE '%$filter_business%')";
    }
    $results_data = $wpdb->get_results( "SELECT quiz_system, point_score, correct_score, correct, total, name, business, email, phone, user,
        time_taken, time_taken_real, quiz_results FROM $table_name WHERE (quiz_id = $quiz_id) AND (deleted=0)$filter_sql ORDER BY time_taken_real ASC" );
    } else {
      $results_data = $wpdb->get_results( "SELECT quiz_system, point_score, correct_score, correct, total, name, business, email, phone, user,
          time_taken, time_taken_real, quiz_results FROM $table_name WHERE quiz_id = $quiz_id AND deleted=0 ORDER BY time_taken_real ASC" );
    }

    //compile results
    $results_total = 0;
    $results_average_points = 0;
    $results_average_score = 0;
    $results_average_correct_answers = 0;
    $results_total_questions = 0;
    $results_average_time = 0;
    $answer_data = array();
    $quiz_system = '';
    foreach ($results_data as $results) {
      $results_total += 1;
      $results_average_points += $results->point_score;
      $results_average_score += $results->correct_score;
      $results_average_correct_answers += $results->correct;
      $results_total_questions = $results->total;
      $quiz_system = $results->quiz_system;
      if (is_serialized($results->quiz_results) && is_array(@unserialize($results->quiz_results)))
      {
        $quiz_results = unserialize($results->quiz_results);
        $results_average_time += $quiz_results[0];
        foreach ($quiz_results[1] as $answer) {
          if ( isset( $answer_data[$answer[0]][$answer[1]] ) ) {
            $answer_data[$answer[0]][$answer[1]] += 1;
          } else {
            $answer_data[$answer[0]][$answer[1]] = 1;
          }
          if ( isset( $answer_data[$answer[0]]['total'] ) ) {
            $answer_data[$answer[0]]['total'] += 1;
          } else {
            $answer_data[$answer[0]]['total'] = 1;
          }
          if ( isset( $answer_data[$answer[0]]['points'] ) ) {
            $answer_data[$answer[0]]['points'] += $results->point_score;
          } else {
            $answer_data[$answer[0]]['points'] = $results->point_score;
          }
          if ( isset( $answer[0]["correct"] ) ) {
            if ( "correct" == $answer[0]["correct"] ) {
              if ( isset( $answer_data[$answer[0]]['correct'] ) ) {
                $answer_data[$answer[0]]['correct'] += 1;
              } else {
                $answer_data[$answer[0]]['correct'] = 1;
              }
            }
          } else {
            if ( $answer[1] == $answer[2] ) {
              if ( isset( $answer_data[$answer[0]]['correct'] ) ) {
                $answer_data[$answer[0]]['correct'] += 1;
              } else {
                $answer_data[$answer[0]]['correct'] = 1;
              }
            }
          }
        }
      }
    }

    $array_for_json = array();
    $question_counter = 0;
    foreach ($answer_data as $question_text => $question) {
      $array_for_json[$question_counter]["questionText"] = strip_tags( htmlspecialchars_decode( $question_text, ENT_QUOTES ) );;
      $array_for_json[$question_counter]["totalAnswers"] = $question['total'];
      $array_for_json[$question_counter]["averagePoints"] = $question['points'] / $question['total'];
      $array_for_json[$question_counter]["correct"] = $question['correct'] / $question['total'];
      $answer_counter = 0;
      $answer_array = array();
      foreach ($question as $key => $answer) {
        if ( ! in_array( $key, array( 'total', 'points', 'correct' ) ) ) {
          $answer_array["answerText"] = $key;
          $answer_array["totalSelected"] = $answer;
          $array_for_json[$question_counter]["answers"][] = $answer_array;
        }
        $answer_counter++;
      }
      $question_counter++;
    }

    ?>
    <script>
      var qsmAnswerData = {
        question: <?php echo json_encode( $array_for_json ); ?>,
        system: <?php echo $quiz_system; ?>
      };
    </script>

    <?php
    if ( $results_total > 0 ) {
      $results_average_points = $results_average_points/$results_total;
      $results_average_score = $results_average_score/$results_total;
      $results_average_correct_answers = $results_average_correct_answers/$results_total;
      $results_average_time = $results_average_time/$results_total;
    } else {
      $results_average_points = 0;
      $results_average_score = 0;
      $results_average_correct_answers = 0;
      $results_average_time = 0;
    }

    ?>
    <h3>Results Overview</h3>
    <section class="qsm_results_overview_section">
      <div class="qsm_stats_container">
        <div class="qsm_stats_container_title">Total Submissions</div>
        <div class="qsm_stats_container_content"><?php echo $results_total; ?></div>
      </div>
      <div class="qsm_stats_container">
        <div class="qsm_stats_container_title">Average Points</div>
        <div class="qsm_stats_container_content"><?php echo round( $results_average_points, 2); ?></div>
      </div>
      <div class="qsm_stats_container">
        <div class="qsm_stats_container_title">Average Score</div>
        <div class="qsm_stats_container_content"><?php echo round( $results_average_score, 2)."%"; ?></div>
      </div>
    </section>
    <h3>Question Results</h3>
    <section class="qsm_answer_data_section">

    </section>
    <?php

  }
}
?>
