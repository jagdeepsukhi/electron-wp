<?php
/**
 * Plugin Name: QSM - URL Parameters
 * Plugin URI: https://quizandsurveymaster.com/
 * Description: Enables URL parameters on your quizzes and surveys
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com/
 * Version: 1.0.0
 *
 * @author Frank Corso
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * This class is the main class of the plugin
 *
 * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
 *
 * @since 1.0.0
 */
class QSM_URL_Parameters {

	/**
	 * Version Number
	 *
	 * @var string
	 * @since 1.0.0
	 */
	public $version = '1.0.0';

	/**
	 * Main Construct Function
	 *
	 * Call functions within class
	 *
	 * @since 1.0.0
	 * @uses QSM_URL_Parameters::load_dependencies() Loads required filed
	 * @uses QSM_URL_Parameters::add_hooks() Adds actions to hooks and filters
	 * @return void
	 */
	public function __construct() {
		$this->load_dependencies();
		$this->add_hooks();
		$this->check_license();
	}

	/**
	 * Load File Dependencies
	 *
	 * @since 1.0.0
	 * @return void
	 * @todo If you are not setting up the addon settings tab, the quiz settings tab, or variables, simply remove the include file below
	 */
	public function load_dependencies() {
		include 'php/addon-settings-tab-content.php';
		include 'php/quiz-settings-tab-content.php';
		include 'php/class-qsm-parameter-settings.php';
	}

	/**
	 * Add Hooks
	 *
	 * Adds functions to relavent hooks and filters
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function add_hooks() {
		add_action( 'admin_init', 'qsm_addon_qsm_url_parameters_register_quiz_settings_tabs' );
		add_action( 'admin_init', 'qsm_addon_qsm_url_parameters_register_addon_settings_tabs' );

		add_action( 'qsm_contact_fields_end', array( $this, 'add_parameter_fields' ) );
		add_filter( 'qsm_results_array', array( $this, 'save_parameter_values' ), 10, 2 );
		add_action( 'qsm_below_admin_results', array( $this, 'display_parameter_values' ) );

		add_action( 'wp_ajax_qsm_save_parameters', array( 'QSM_Parameter_Settings', 'ajax_save_parameters' ) );
		add_action( 'wp_ajax_nopriv_qsm_save_parameters', array( 'QSM_Parameter_Settings', 'ajax_save_parameters' ) );
	}

	/**
	 * Checks license
	 *
	 * Checks to see if license is active and, if so, checks for updates
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function check_license() {
		if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
			include 'php/EDD_SL_Plugin_Updater.php';
		}

		// Retrieve our license key from the DB.
		$addon_data = get_option( 'qsm_addon_url_parameters_settings', '' );
		if ( isset( $addon_data['license_key'] ) ) {
			$license_key = trim( $addon_data['license_key'] );
		} else {
			$license_key = '';
		}

		// Setup the updater.
		$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
			'version'   => $this->version,
			'license'   => $license_key,
			'item_name' => 'URL Parameters',
			'author'    => 'Frank Corso',
		));
	}

	/**
	 * Adds hidden fields for each parameter in quiz
	 *
	 * @since 1.0.0
	 * @param object $options The settings for the quiz
	 */
	public function add_parameter_fields( $options = '' ) {
		$parameters = QSM_Parameter_Settings::load_parameters();
		if ( ! empty( $parameters ) ) {
			foreach ( $parameters as $parameter ) {
				$value = '';
				if ( isset( $_GET[ $parameter['name'] ] ) ) {
					$value = $_GET[ $parameter['name'] ];
				}
				?>
				<input type="hidden" name="<?php echo $parameter['name']; ?>" value="<?php echo $value; ?>">
				<?php
			}
		}
	}

	/**
	 * Adds the parameters to the results array
	 *
	 * @since 1.0.0
	 * @param array $results The results for the quiz
	 * @param array $response_data The data associated with the resuts
	 * @return array The modified $results array
	 */
	public function save_parameter_values( $results, $response_data ) {
		$parameters = QSM_Parameter_Settings::load_parameters();
		$values = array();
		foreach ( $parameters as $parameter ) {
			$temp = $parameter['default'];
			if ( isset( $_POST[ $parameter['name'] ] ) && ! empty( $_POST[ $parameter['name'] ] ) ) {
				$temp = sanitize_text_field( $_POST[ $parameter['name'] ] );
			}
			$values[ $parameter['name'] ] = $temp;
		}
		$results['parameters'] = $values;
		return $results;
	}

	/**
	 * Displays the URL parameters
	 *
	 * @since 1.0.0
	 * @param array The results data.
	 */
	public function display_parameter_values( $results ) {
		$parameters = QSM_Parameter_Settings::load_parameters();
		if ( ! empty( $parameters ) ) {
			echo '<h3>URL Parameters</h3>';
			if ( isset( $results['results']['parameters'] ) && is_array( $results['results']['parameters'] ) ) {
				foreach( $parameters as $parameter ) {
					if ( isset( $results['results']['parameters'][ $parameter['name'] ] ) ) {
						echo "<p>{$parameter['label']}: {$results['results']['parameters'][ $parameter['name'] ]}</p>";
					} else {
						echo "<p>{$parameter['label']}: {$parameter['default']}</p>";
					}
				}
			}
		}
	}
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 1.0.0
 */
function qsm_addon_qsm_url_parameters_load() {
	// Make sure QSM is active.
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_url_parameters = new QSM_URL_Parameters();
	} else {
		add_action( 'admin_notices', 'qsm_addon_qsm_url_parameters_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_qsm_url_parameters_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since 1.0.0
 */
function qsm_addon_qsm_url_parameters_missing_qsm() {
	echo '<div class="error"><p>QSM - URL Parameters requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
