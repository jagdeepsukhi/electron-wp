<?php
/**
 * The functions needed for loading and saving the quiz parameters
 */
class QSM_Parameter_Settings {
	
	/**
	 * Loads the parameters for quiz
	 *
	 * @since 1.0.0
	 * @return array The parameters for the quiz, if any
	 */
	public static function load_parameters() {
		global $mlwQuizMasterNext;
		$parameters = $mlwQuizMasterNext->pluginHelper->get_quiz_setting( 'parameters', array() );
		return $parameters;
	}

	/**
	 * Saves the parameters
	 *
	 * @since 1.0.0
	 * @param array $parameters The parameters for the quiz
	 * @return bool True if successful
	 */
	public static function save_parameters( $parameters ) {
		global $mlwQuizMasterNext;
		$success = $mlwQuizMasterNext->pluginHelper->update_quiz_setting( 'parameters', $parameters );
		return $success;
	}

	/**
	 * AJAX endpoint for saving parameters in admin
	 *
	 * @since 1.0.0
	 */
	public static function ajax_save_parameters() {
		global $mlwQuizMasterNext;
		$quiz = intval( $_POST["quiz_id"] );
		$mlwQuizMasterNext->pluginHelper->prepare_quiz( $quiz );
		$success = QSM_Parameter_Settings::save_parameters( $_POST['parameters'] );
		echo json_encode( array( 'event' => $success ) );
		wp_die();
	}
}


?>