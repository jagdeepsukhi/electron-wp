<?php
/**
 * This file handles the Quiz Settings tab for the addon
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Registers your tab in the quiz settings page
 *
 * @since 1.0.0
 * @return void
 */
function qsm_addon_qsm_url_parameters_register_quiz_settings_tabs() {
	global $mlwQuizMasterNext;
	if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
		$mlwQuizMasterNext->pluginHelper->register_quiz_settings_tabs( 'URL Parameters', 'qsm_addon_qsm_url_parameters_quiz_settings_tabs_content' );
	}
}

/**
 * Generates the content for your quiz settings tab
 *
 * @since 1.0.0
 * @return void
 */
function qsm_addon_qsm_url_parameters_quiz_settings_tabs_content() {

	$quiz_id = intval( $_GET['quiz_id'] );

	// Enqueue your scripts and styles.
	wp_enqueue_script( 'micromodal_script', plugins_url( '/js/micromodal.min.js', QSM_PLUGIN_BASENAME ) );
	wp_enqueue_script( 'qsm_url_parameters_admin_script', plugins_url( '../js/qsm-url-parameters-admin.js', __FILE__ ), array( 'jquery', 'wp-util', 'underscore', 'micromodal_script' ), '1.0' );
	wp_enqueue_style( 'qsm_url_parameters_admin_style', plugins_url( '../css/qsm-url-parameters-admin.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'qsm_admin_parameter_css', plugins_url( '/css/qsm-admin-question.css', QSM_PLUGIN_BASENAME ) );

	$parameters = QSM_Parameter_Settings::load_parameters();
	wp_localize_script( 'qsm_url_parameters_admin_script', 'qsmParametersObject', array( 'parameters' => $parameters, 'quizID' => $quiz_id ) );
	?>
	<h3>URL Parameters</h3>
	<p></p>
	<div class="parameter-messages"></div>
	<button id="create-new-button" class="button">Create New Parameter</button>
	<button id="save-parameters" class="button-primary">Save Parameters</button>
	<table class="widefat">
		<thead>
			<tr>
				<th scope="col"></th>
				<th scope="col">Parameter</th>
				<th scope="col">Label</th>
				<th scope="col">Default Value</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody id="parameters">

		</tbody>
	</table>

	<!-- Popup for editing parameter -->
	<div class="qsm-popup qsm-popup-slide" id="modal-1" aria-hidden="true">
		<div class="qsm-popup__overlay" tabindex="-1" data-micromodal-close>
			<div class="qsm-popup__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
				<header class="qsm-popup__header">
					<h2 class="qsm-popup__title" id="modal-1-title">Edit Parameter</h2>
					<a class="qsm-popup__close" aria-label="Close modal" data-micromodal-close></a>
				</header>
				<main class="qsm-popup__content" id="modal-1-content">
					<input type="hidden" name="edit_parameter_id" id="edit_parameter_id" value="">
					<div class="qsm-row">
						<label for="parameter">Parameter</label>
						<input type="text" name="name" value="" id="name"/>
					</div>
					<div class="qsm-row">
						<label for="label">Label</label>
						<input type="text" name="label" value="" id="label"/>
					</div>
					<div class="qsm-row">
						<label for="default-value">Default Value</label>
						<input type="text" name="default-value" value="" id="default-value"/>
					</div>
				</main>
				<footer class="qsm-popup__footer">
					<button id="save-popup-button" class="qsm-popup__btn qsm-popup__btn-primary">Add Parameter</button>
					<button class="qsm-popup__btn" data-micromodal-close aria-label="Close this dialog window">Cancel</button>
				</footer>
			</div>
		</div>
	</div>

	<!-- Views -->

	<!-- Individual Parameter View -->
	<script type="text/template" id="tmpl-parameter">
		<tr data-id="{{data.id}}" class="parameter">
			<td><button class="edit-parameter"><span class="dashicons dashicons-edit"></span></button></th>
			<td>{{data.name}}</td>
			<td>{{data.label}}</td>
			<td>{{data.default}}</td>
			<td><button class="delete-parameter"><span class="dashicons dashicons-trash"></span></button></td>
		</tr>
	</script>

	<!-- View for Notices -->
	<script type="text/template" id="tmpl-notice">
		<div class="notice notice-{{data.type}}">
			<p>{{data.message}}</p>
		</div>
	</script>

	<?php
}
?>
