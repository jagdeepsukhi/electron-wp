# QSM - URL Parameters
An addon for Quiz And Survey Master that adds the ability to have URL parameters in quizzes and surveys.

## Getting Started
This repo can be downloaded as a zip and installed as a WordPress plugin as-is with no build script needing to be run.

## Contributing
Please read [CONTRIBUTING.md](https://github.com/fpcorso/quiz_master_next/blob/master/CONTRIBUTING.md) for the process for submitting pull requests or issues to us.

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see [the releases in this repository](https://github.com/fpcorso/qsm-url-parameters/releases).

## Developers
* Frank Corso - Lead Developer

See also [the list of contributors](https://github.com/fpcorso/qsm-url-parameters/graphs/contributors) who participated in this project.

## License
This project is licensed under the GPLv2 License.

## Support ##
This is a developer's portal for this addon for Quiz And Survey Master and should _not_ be used for support. Please create a support ticket [here](http://quizandsurveymaster.com/contact-us/).

