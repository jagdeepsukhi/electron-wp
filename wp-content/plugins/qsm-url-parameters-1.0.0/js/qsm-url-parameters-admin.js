/**
 * QSM Parameters
 */

var QSMParameters;
(function ($) {
	QSMParameters = {
		parameters: qsmParametersObject.parameters,

		/**
		 * Cycles through loaded parameters and adds to the table
		 */
		init: function() {
			for ( var i = 0; i < QSMParameters.parameters.length; i++ ) {
				QSMParameters.parameters[ i ].id = i;
				QSMParameters.addParameterRow( QSMParameters.parameters[ i ] );
			}
		},
		/**
		 * Adds a parameter to the parameter collection
		 *
		 * @param object data The data of the parameter
		 */
		addParameter: function( data ) {
			QSMParameters.parameters.push( data );
			QSMParameters.addParameterRow( data );
		},
		/**
		 * Adds a parameter to the table
		 *
		 * @param object data The data of the parameter
		 */
		addParameterRow: function( data ) {
			var template = wp.template( 'parameter' );
			$( '#parameters' ).append( template( data ) );
		},
		/**
		 * Creates a new parameter
		 */
		createParameter: function() {
			var data = {
				id: QSMParameters.parameters.length,
				name: '',
				label: '',
				default: ''
			}
			QSMParameters.addParameter( data );
			QSMParameters.openParameterPopup( data );
		},
		/**
		 * Opens the popup and adds parameter data to relevant fields
		 */
		openParameterPopup: function( data ) {
			$( '#edit_parameter_id' ).val( data.id );
			$( '#name' ).val( data.name );
			$( '#label' ).val( data.label );
			$( '#default-value' ).val( data.default );
			MicroModal.show( 'modal-1' );
		},
		/**
		 * Modifies the parameter in the parameters collection
		 */
		editParameter: function( id, name, label, value ) {
			var data = {
				id: parseInt( id ),
				name: name,
				label: label,
				default: value
			}
			QSMParameters.parameters[ id ] = data;
			var template = wp.template( 'parameter' );
			$( 'tr[data-id=' + id + ']' ).replaceWith( template( data ) );
			MicroModal.close( 'modal-1' );
		},
		/**
		 * Removes a parameter from the table and parameters collection
		 */
		deleteParameter: function( id ) {
			QSMParameters.parameters[ id ] = '';
			$( 'tr[data-id=' + id + ']' ).remove();
		},
		/**
		 * Saves all parameters
		 */
		saveParameters: function() {
			QSMParameters.displayAlert( 'Saving parameters...', 'info' );

			// Checks to see if we removed any parameters
			var parameterArray = [];
			for ( var i = 0; i < QSMParameters.parameters.length; i++ ) {
				if ( _.isObject( QSMParameters.parameters[ i ] ) || _.isArray( QSMParameters.parameters[ i ] ) ) {
					parameterArray.push( QSMParameters.parameters[ i ] );
				}		
			}

			// Prepare data and send to WP AJAX endpoint
			var data = {
				action: 'qsm_save_parameters',
				parameters: parameterArray,
				quiz_id : qsmParametersObject.quizID
			};
			jQuery.post( ajaxurl, data, function( response ) {
				QSMParameters.saved( JSON.parse( response ) );
			});
		},
		saved: function( event ) {
			QSMParameters.displayAlert( 'Parameters successfully saved!', 'success' );
		},
		displayAlert: function( message, type ) {
			QSMParameters.clearAlerts();
			var template = wp.template( 'notice' );
			var data = {
				message: message,
				type: type
			};
			$( '.parameter-messages' ).append( template( data ) );
		},
		clearAlerts: function() {
			$( '.parameter-messages' ).empty();
		}
	};

	$(function() {
		QSMParameters.displayAlert( 'Loading parameters...', 'info' );

		$( '#create-new-button' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMParameters.createParameter();
		});

		$( '#save-parameters' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMParameters.saveParameters();
		});

		$( '#save-popup-button' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMParameters.editParameter(
				$( '#edit_parameter_id' ).val(),
				$( '#name' ).val(),
				$( '#label' ).val(),
				$( '#default-value' ).val()
			);
		});

		$( '#parameters' ).on( 'click', '.edit-parameter', function( event ) {
			event.preventDefault();
			var id = $( this ).parents( '.parameter' ).data( 'id' );
			QSMParameters.openParameterPopup( QSMParameters.parameters[ id ] );
		});

		$( '#parameters' ).on( 'click', '.delete-parameter', function( event ) {
			event.preventDefault();
			var id = $( this ).parents( '.parameter' ).data( 'id' );
			QSMParameters.deleteParameter( id );
		});
		QSMParameters.init();
		QSMParameters.clearAlerts();
	});
}(jQuery));