<?php
function qmn_gr_option_page()
{
	$options = get_option('qmn_gr_options');
	$api_key = isset($options['api_key']) ? $options['api_key'] : '';
	
	
	?>
	<div class="wrap">
		<h2>QMN Get Response Integration</h2>
		<?php
		if (empty($api_key))
		{
			echo "<p><strong>To begin, you need to authorize this website to access your Get Response account. </p>";
		}
		if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true')
		{
			echo "<div id=\"message\" class=\"updated below-h2\"><p><strong>Success! </strong>Your site has been authorized successfully.</p></div>";
		}
		?>
		<form action="options.php" method="post">
		<?php settings_fields('qmn_gr_options'); ?>
		<?php do_settings_sections('qmn_gr_settings_section'); ?>

		<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
		</form>
	</div>
	<?php
}

function qmn_gr_register_settings()
{
	register_setting( 'qmn_gr_options', 'qmn_gr_options', 'qmn_gr_options_validate' );
	add_settings_section('qmn_gr_settings_main', 'QMN Get Response Settings', 'qmn_gr_settings_display', 'qmn_gr_settings_section');
	add_settings_field('qmn_gr_api_key_field', 'API Key', 'qmn_gr_api_key_field_display', 'qmn_gr_settings_section', 'qmn_gr_settings_main');

}

function qmn_gr_settings_display()
{
	echo "<p></p>";
}

function qmn_gr_api_key_field_display()
{
	$options = get_option('qmn_gr_options');
	$api_key = isset($options['api_key']) ? $options['api_key'] : '';
	echo "<input id='qmn_gr_api_key_field' name='qmn_gr_options[api_key]' size='40' type='text' value='{$api_key}' />";
}



function qmn_gr_options_validate($input)
{
	$newinput = array();
	if (isset($input['api_key']))
	{
		$newinput['api_key'] = trim($input['api_key']);
	}
	
	return $newinput;
}



?>
