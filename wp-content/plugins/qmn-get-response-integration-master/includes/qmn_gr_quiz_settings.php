<?php


function qmn_gr_quiz_settings_tab_content()
{
    if ( !current_user_can('moderate_comments') ) {
		echo "You do not have proper authority to access this page";
		return '';
	}
  global $mlwQuizMasterNext;

  if (isset($_POST['update_gr']) && wp_verify_nonce( $_POST['edit_gr_nonce'], 'edit_gr'))
  {
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting("gr_selected_list",$_POST["selected_list"]);
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting("gr_checkbox_text",$_POST["checkbox_text"]);
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting("gr_display_checkbox",$_POST["checkbox_option"]);
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting("gr_show_access",$_POST["gr_access"]);
  }

  $selected_list = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_selected_list");
  $checkbox_text = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_checkbox_text");
  $display_checkbox = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_display_checkbox");
  $gr_access = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_show_access");
  $lists = array();
  $options = get_option('qmn_gr_options');
  
  ?>
  <div id="tabs-aweber" class="mlw_tab_content">
    <?php
    if (!isset($options['api_key']) || (isset($options['api_key']) && empty($options['api_key'])))
    {
      echo "<h2>You must first go to the Get Response Integration tab found on the Add on settings page.</h2>";
    }
    else
    {
        require_once('GetResponseAPI.class.php');
        $api = new GetResponse($options['api_key']);
        $campaigns = (array)$api->getCampaigns();
       
      ?>
      <form action='' method='post'>
        <label>Would you like to add Get Response Integration to the quiz?</label>  
        <select name="gr_access">
        <option value="yes" <?php if (isset($gr_access) && $gr_access == 'yes') { echo 'selected="selected"'; }?> >Yes</option>    
        <option value="no" <?php if (isset($gr_access) && $gr_access == 'no') { echo 'selected="selected"'; }?>>No</option>   
        </select>
        <br />  
        <label>List to add to</label>
        <select name="selected_list">
          <?php
          foreach($campaigns as $id => $list)
          {
            $selected_text = '';
            if ($selected_list == $id)
            {
              $selected_text = ' selected=selected';
            }
            echo "<option value='".$id."'$selected_text>".$list->name."</option>";
          }
          ?>
      
        </select>
        <br />
         <label>Do you want the checkbox to appear at the end of the quiz? If set to No it will automatically subscribe to the mailing list. </label>
        <select name="checkbox_option">
        <option value="yes" <?php if (isset($display_checkbox) && $display_checkbox == 'yes') { echo 'selected="selected"'; }?> >Yes</option>    
        <option value="no" <?php if (isset($display_checkbox) && $display_checkbox == 'no') { echo 'selected="selected"'; }?>>No</option>   
        </select>
        <br /> 
        <label>Text For Checkbox</label>
        <input type="text" name="checkbox_text" value="<?php echo $checkbox_text; ?>" />
        <br />
        <input type="hidden" name="update_gr" value="confirmation"/>
        <?php wp_nonce_field('edit_gr','edit_gr_nonce'); ?>
        <input type="submit" value="Save Changes" class="button" />
      </form>
      <?php
    }
    ?>
  </div>
  <?php
}
?>
