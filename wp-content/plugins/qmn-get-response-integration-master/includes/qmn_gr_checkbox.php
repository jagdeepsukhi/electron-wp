<?php
function qmn_gr_checkbox()
{
  global $mlwQuizMasterNext;
  $checkbox_text = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_checkbox_text");
  $display_checkbox = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_display_checkbox");
  $gr_access = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_show_access");
  if ($gr_access == 'yes' && !empty($checkbox_text) && $display_checkbox == 'yes')
  {
    echo "<input type='checkbox' name='qmn_gr_check' id='qmn_gr_check' value='1' /><label for='qmn_gr_check'>$checkbox_text</label><br /><br />";
  }
}
?>
