<?php
function qmn_gr_integration()
{ 
    global $mlwQuizMasterNext;
    $display_checkbox = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_display_checkbox");
    $gr_access = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_show_access");
    
  if ($gr_access == 'yes' && (isset($_POST['qmn_gr_check']) || $display_checkbox == 'no'))
  {
    $user_email = isset($_POST["mlwUserEmail"]) ? $_POST["mlwUserEmail"] : '';
    $user_name = isset($_POST["mlwUserName"]) ? $_POST["mlwUserName"] : '';
   
    $selected_list = $mlwQuizMasterNext->pluginHelper->get_quiz_setting("gr_selected_list");
    $options = get_option('qmn_gr_options');
  	$api_key = $options['api_key'];
        require_once('GetResponseAPI.class.php');
        $api = new GetResponse($options['api_key']);
        $api->addContact($selected_list,$user_name,$user_email);
  	
   
  }
}
?>
