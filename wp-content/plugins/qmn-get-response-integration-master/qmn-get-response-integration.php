<?php
/**
 * Plugin Name: QMN Get Response
 * Plugin URI: http://mylocalwebstop.com
 * Description: Adds Get Response integration to your Quiz Master Next
 * Author: Frank Corso
 * Author URI: http://mylocalwebstop.com
 * Version: 1.0.0
 *
 * Disclaimer of Warranties
 * The plugin is provided "as is". My Local Webstop and its suppliers and licensors hereby disclaim all warranties of any kind,
 * express or implied, including, without limitation, the warranties of merchantability, fitness for a particular purpose and non-infringement.
 * Neither My Local Webstop nor its suppliers and licensors, makes any warranty that the plugin will be error free or that access thereto will be continuous or uninterrupted.
 * You understand that you install, operate, and uninstall the plugin at your own discretion and risk.
 *
 * @author Frank Corso
 * @version 1.0.0
 */


class QMN_GetResponse
{
	public function __construct()
	{
		$this->load_dependencies();
		$this->add_hooks();
	}
	private function load_dependencies()
	{
		include("includes/qmn_gr_options.php");
                include("includes/qmn_gr_checkbox.php");
                include("includes/qmn_gr_integration.php");
                include("includes/qmn_gr_quiz_settings.php");
		
	}

	private function add_hooks()
	{
		
		add_action('admin_init', 'qmn_gr_register_settings');
                 
                add_action("plugins_loaded", array($this,'create_gr_menu'));
                add_action('plugins_loaded', array($this,'qmn_add_new_gr_tab'));                
                add_action('mlw_qmn_load_results_page', 'qmn_gr_integration');
                add_action('mlw_qmn_end_quiz_section', 'qmn_gr_checkbox');
	}

	public function create_gr_menu()
	{
		global $mlwQuizMasterNext;
             if (!is_null($mlwQuizMasterNext) && !is_null($mlwQuizMasterNext->pluginHelper) && method_exists($mlwQuizMasterNext->pluginHelper, 'register_addon_settings_tab'))
             {
                $mlwQuizMasterNext->pluginHelper->register_addon_settings_tab("Get Response Integration","qmn_gr_option_page");
             }
	}
        
        public function qmn_add_new_gr_tab()
    {
	global $mlwQuizMasterNext;
        if (!is_null($mlwQuizMasterNext) && !is_null($mlwQuizMasterNext->pluginHelper) && method_exists($mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs'))
        {
	$mlwQuizMasterNext->pluginHelper->register_quiz_settings_tabs(__('Get Response Integration', 'quiz-master-next'), 'qmn_gr_quiz_settings_tab_content');
        }
    }
}
$QMN_GetResponse = new qmn_GetResponse();

register_activation_hook( __FILE__, 'qmn_gr_activate');
function qmn_gr_activate()
{
  $data = '';
  if ( ! get_option('qmn_gr_options'))
	{
		add_option('qmn_gr_options' , $data);
	}
}
?>
