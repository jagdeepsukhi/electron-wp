<?php
/*
* All common used Popups
*/

add_action("lm_recall_popup","lm_recall_popup"); 
function lm_recall_popup()
{
?>
<!-- Loan Request Modal -->
<div id="recallModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Recall Popup</h4>
      </div>
      <div class="modal-body">
      	<form id="recall-asset" method="post">
      		<div class="recall-text"><label>Type Message:</label><textarea name="msg_to_current_user" id="msg_to_current_user" required></textarea></div>
	        <br>
	        <input type="hidden" name="recall_asset_id" id="recall_asset_id">
	        <input type="hidden" name="action" value="recall_asset">
	       	<button type="submit" id="recall-submit" class="btn btn-success form-btn" data-form-type="recall-checkout" >Submit</button>
	       	<button type="button" class="btn btn-danger" id="recall-cancel" data-dismiss="modal">Cancel</button>
      	</form>
      </div>
    </div>
  </div>
</div>
<?php
}



add_action("lm_one_click_warrning_popup","lm_one_click_warrning_popup"); 
function lm_one_click_warrning_popup()
{
?>
<!-- Confirm if warrning trigger Modal -->
<div id="onlcick-checkout-warnningModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Please confirm?</h4>
      </div>
      <div class="modal-body">
      
          <div><p class="warrningMsg"></p></div>
          <br>
          <input type="hidden" name="confirm_recall_asset_id" id="confirm_recall_asset_id">
          <button type="submit" id="confirm-onclick-warrning-checkout" class="btn btn-success form-btn" data-form-type="confirm-onclick-warrning-checkout" data-asset-id="">Accept</button>
          <button type="button" class="btn btn-danger" id="cancel-onclick-warrning-checkout" data-dismiss="modal">Cancel</button>
       
      </div>
    </div>
  </div>
</div>

<?php
}

add_action("lm_loader","lm_loader"); 
function lm_loader()
{
?>
<!-- Loader -->
 <div class="lm-loader">
  <div class="modal-dialog">
     <img src="<?= get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/lm-loader.gif' ?>">
  </div>
</div> 

<?php
}



add_action("lm_fast_form_checkout_popup","lm_fast_form_checkout_popup"); 
function lm_fast_form_checkout_popup()
{

	$visitorData = lm_get_visitors_data(); 



?>
<!-- Loan Request with Fast Form -->
<div id="QiuckFormModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Type Popup</h4>
      </div>
      <div class="modal-body">
        <?php //echo do_shortcode("[Form id='31']"); ?>
        <form id="fast-checkout-form" method="post" action="">
          <div class="form-group">
             <label for="maximum-loan">Client Name</label>
             <select class="form-control visitor-client" name="client_name" data-name="lm_client_name"  required>
             	<option value="">Select Client</option>
             <?php foreach ($visitorData as $key => $visitor) { ?>
             	<?php if (!empty($visitor['name'])) { ?>
             	 <option value="<?= $visitor['name'] ?>" data-company="<?= $visitor['company'] ?>" data-project="<?= $visitor['project'] ?>" data-parent="<?= $visitor['parent'] ?>"><?= $visitor['name'] ?></option>
            	 <?php } // ednif ?>
             <?php } ?>
             </select>
             
          </div>
           <div class="form-group">
             <label for="maximum-loan">Client Organization </label>
              <input type="text" class="form-control visitor-organization" data-name="lm_client_organization" name="client_organization" required readonly="">
          </div>
          <div class="form-group">
             <label for="maximum-loan">Project</label>
              <input type="text" class="form-control visitor-project" name="project" data-name="lm_project"  required readonly>
              
             
          </div> 
          <div class="form-group">
             <div class="fast-wrn"><p class="warrningMsg"></p></div>
          </div>
          <input type="hidden" name="is_confirmed" id="fast_is_confirm">
          <input type="hidden" name="asset_id" id="fast_asset_id">
          <input type="hidden" name="action" value="save_loan_checkout_quick_form">
          <?php wp_nonce_field('my_delete_action'); ?>
          <button type="submit" class="btn btn-success fast-submit">Submit</button>  
          <button type="submit" class="btn btn-success fast-accept" style="display: none;">Accept</button>  
          <button type="button" class="btn btn-danger" id="recall-cancel" data-dismiss="modal">Cancel</button>
        </form>

      </div>
    </div>
  </div>
</div>

<?php
}


add_action("lm_checkin_buttons_popup","lm_checkin_buttons_popup"); 
function lm_checkin_buttons_popup()
{
?>
 
<!-- Checlout button inital Modal -->
<div id="checkout-confirm" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Return Type Popup</h4>
      </div>
      <div class="modal-body">
       <button type="button" class="btn btn-primary form-btn" data-form-type="one-click-quick-checkin" data-asset-id="">One Click/Quick Check-in</button>
       <button type="button" class="btn btn-success form-btn" data-form-type="full-return-form" data-asset-id="">Full Form</button>
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<?php
}

add_action("lm_checkout_buttons_popup","lm_checkout_buttons_popup"); 
function lm_checkout_buttons_popup()
{
?>
 
<!-- Return type button Modal -->
<div id="RequestModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Checkout Type</h4>
      </div>
      <div class="modal-body">
       <button type="button" class="btn btn-primary form-btn" data-form-type="one-click-quick-checkout" data-asset-id="">One Click</button>
       <button type="button" class="btn btn-info form-btn" data-form-type="fast-quick-checkout-form" data-asset-id="">Fast Form</button>
       <button type="button" class="btn btn-success form-btn" data-form-type="full-request-form" data-asset-id="">Full Form</button>
       <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<?php

}

add_action("lm_mm_warning","lm_mm_warning_popup"); 
function lm_mm_warning_popup()
{
?>
<!-- Modal -->
  <div id="lm-loan-trigger" class="modal fade" role="dialog" style="z-index: 9999;">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Warnnig!!</h4>
        </div>
        <div class="modal-body">
      <div class="alert alert-danger">
         <p class="lm-wr"></p>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary mm-recall" data-recall-status="" data-asset-id="" style="display: inline-block;">Send Recall</button>
          <a href="" class="btn btn-success form-btn waves-effect lm-goto" style="display: inline-block;">Go to Asset Check-out Overview</a>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

<?php
}