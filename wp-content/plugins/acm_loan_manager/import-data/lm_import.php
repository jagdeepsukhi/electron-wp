<?php
/*
* Import functions
*/


$lm_pages = array(); 
$lm_pages ['_lm_page_loan_front'] 		= array(
											"title"=>"Asset Loan Front Page",
											"shortcode"=>"[en_lm_loan_front_table]"
										);
$lm_pages ['_lm_page_loan_overview'] 		= array(
												"title"=>"Asset Loan Overview",
												"shortcode"=>"[en_lm_requests_overview]"
										);
$lm_pages ['_lm_page_full_form_checkout'] 	= array(
												"title"=>"Full Form Checkout",
												"shortcode"=>"[en_lm_loan_request_full_form]"
										); 
$lm_pages ['_lm_page_pending_request_form']	= array(
												"title"=>"Loan Pending Request Form",
												"shortcode"=>"[en_lm_loan_pending_request_form]"
										); 
$lm_pages ['_lm_page_return_full_form'] 	= array(
											"title"=>"Full Form Check-in",
											"shortcode"=>"[en_lm_loan_return_equipment_form]"
										); 
$lm_pages ['_lm_page_loan_settings'] 		= array(
											"title"=>"Asset Loan Settings",
											"shortcode"=>"[en_lm_loan_settings]"
										);
$lm_pages ['_lm_page_individual_settings'] 	= array(
											"title"=>"Facility Equipment Loan Individual Settings",
											"shortcode"=>"[en_lm_loan_equipment_individual_settings]"
										);

$lm_pages ['_lm_page_loan_status'] 		= array(
											"title"=>"Loan Status Options",
											"shortcode"=>"[en_lm_loan_status_master]"
										);

$lm_pages ['_lm_page_loan_location'] 	= array(
											"title"=>"Location Options",
											"shortcode"=>"[en_lm_loan_locations_master]"
										);

$lm_pages ['_lm_page_loan_rational'] 	= array(
											"title"=>"Loan Request Rationale Options",
											"shortcode"=>"[en_lm_loan_reason_master]"
										);

$lm_pages ['_lm_page_reports'] 			= array(
											"title"=>"Loan Reports",
											"shortcode"=>"[en_lm_reports]"
										);
$lm_pages ['_lm_page_history'] 			= array(
											"title"=>"Equipment Loan Manager History",
											"shortcode"=>"[en_lm_history_audit]"
										);

$lm_pages ['_lm_page_direct_approve']   = array(
											"title"=>"Loan Request Direct Approve/Deny",
											"shortcode"=>"[en_lm_loan_request_direct_approve_or_deny]"
										);

$lm_pages ['_lm_page_mobile_menu']   = array(
											"title"=>"Loan Mobile Menu",
											"shortcode"=>"[en_lm_mobilemenu]"
										);

 

$lm_datatabels = array(); 
$lm_datatabels ['_lm_table_application_front'] 	= "Loan Application Front Table";
$lm_datatabels ['_lm_table_overview'] 			= "Equipment Check in/out Overview";
$lm_datatabels ['_lm_table_status_master'] 		= "Loan Status Master Table";
$lm_datatabels ['_lm_table_location_master'] 	= "LM Loan Locations Master Table";
$lm_datatabels ['_lm_table_reason_master'] 		= "LM Loan Reasons Master Table";
$lm_datatabels ['_lm_table_individual_settings']= "LM Facility Equipment Individual Settings";
$lm_datatabels ['_lm_table_reports'] 			= "Loan Reports";
$lm_datatabels ['_lm_table_reports_for_client']	= "Loan Reports for client";
$lm_datatabels ['_lm_table_history'] 			= "Loan Manager History";

$lm_wpforms = array(); 
$lm_wpforms ['_lm_form_full_checkout_request'] 	= "Loan Check­out Full Form";
$lm_wpforms ['_lm_form_full_checkin_return'] 	= "Loan Equipment Return Form";

$lm_emailTemplates = array(); 
$lm_emailTemplates ['_lm_email_overdue'] 			= "Asset Overdue Email";
$lm_emailTemplates ['_lm_email_return_reminder'] 	= "Asset Return Return Reminder Email";
$lm_emailTemplates ['_lm_email_damage_reported'] 	= "Damage Reported Email";
$lm_emailTemplates ['_lm_email_return_email'] 		= "Asset Returned Email";
$lm_emailTemplates ['_lm_email_request_approved'] 	= "Asset Loan Request Approved Email";
$lm_emailTemplates ['_lm_email_request_denied'] 	= "Asset Loan Request Denied Email";
$lm_emailTemplates ['_lm_email_request_confirmation'] 		= "Loan Request Confirmation Email";
$lm_emailTemplates ['_lm_email_request_email_to_approvers'] = "Loan Request Email to Approvers";
$lm_emailTemplates ['_lm_email_asset_recall'] 		= "Asset Recall Email";
 


define("LM_DATATABELS", $lm_datatabels);
define("LM_EMAIL_TEMPLATES", $lm_emailTemplates);
define("LM_WPFORMS", $lm_wpforms);
define("LM_PAGES", $lm_pages);


 /***********************Import source tables************************/
function lm_import_source_tables(){

	global $wpdb;
	$response = array();

	if(get_option('_lm_import_source_tables')==1){

		$response[] = "<strong style='color:red'>Source tables already exist.</strong>";
		return $response;

	}

	// Name of the file
	$filename = dirname(__FILE__).'/lm-source-tables.sql';

	set_time_limit(0);

	header('Content-Type: text/html;charset=utf-8');
	sqlImport($filename);

	echo "Peak MB: ", memory_get_peak_usage(true)/1024/1024;
	 
	update_option('_lm_import_source_tables', 1);
	$response[] = "<strong style='color:green'>All source tables has been imported.</strong>";
	
	return $response;
}

/* Add new columns for loan application in the equipment main table*/
function lm_alter_equipment_main_table(){
	global $wpdb;
	$response=array();
 
	if(get_option( '_lm_import_source_tables')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}
	if(get_option( '_lm_alter_main_table')==1){
		$response[] = "<p><strong style='color:red'>Wp Data tables already imported.</strong><p>";
		return $response;
	}

	$alterSql = "ALTER TABLE en_mb_facility_equipment
				ADD `loan_navigation` longtext NOT NULL,
				ADD  `loan_status` varchar(250) NOT NULL,
				ADD  `loan_enable` varchar(250) DEFAULT NULL,
				ADD  `lm_current_custodian` varchar(250) NOT NULL,
				ADD `lm_checking_for_client` varchar(10) NOT NULL,
				ADD `lm_organization` varchar(250) NOT NULL,
				ADD `lm_current_location` int(11) NOT NULL,
				ADD `lm_due_for_return` varchar(255) NOT NULL,
				ADD `lm_auth_required` varchar(255) NOT NULL,
				ADD `lm_expedited_checkout` varchar(255) NOT NULL,
				ADD `lm_max_loan_period` int(11) NOT NULL";

	if($wpdb->query($alterSql)){
		$response[]="<p><strong style='color:green'>All new required columns has been added to en_mb_facility_equipment table.</strong><p>";
		$updateData = array();
		$updateData["loan_navigation"]	= lm_generate_nav_icons(3); // default checkin icons
		$wpdb->update("en_mb_facility_equipment",$updateData, array("loan_navigation"=>""));

		update_option( '_lm_alter_main_table', 1 );
	}else{
		$response[]="<p><strong style='color:red'>Table en_mb_facility_equipment table could not altered.</strong><p>";
	}	
 
			
	return $response;


}
function lm_import_wp_data_tables(){


	$response=array();
 
	if(get_option( '_lm_alter_main_table')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}
	if(get_option( '_lm_import_wp_data_tables')==1){
		$response[] = "<p><strong style='color:red'>Wp Data tables already imported.</strong><p>";
		return $response;
	}

 
	$wpdatatables 				= file_get_contents(dirname(__FILE__).'/wp_wpdatatables.json');
	$wpdatatablesArr			= json_decode($wpdatatables, true); 

	
	
	$wpdatatables_columns 		= file_get_contents(dirname(__FILE__).'/wp_wpdatatables_columns.json');
	$wpdatatables_columnsArr	= json_decode($wpdatatables_columns, true); 

	global $wpdb;

	$table_number=1;
	foreach ($wpdatatablesArr as $dtkey => $table) {

		$old_table_id 	= $table['id'];

		$table_name 	= $table['title'];

		unset($table['id']);

		$dataType= en_mb_create_data_types(count($table));

		$contactus_table = $wpdb->prefix."wpdatatables";

		if($wpdb->insert( $contactus_table, $table,$dataType)){
			$new_table_id = $wpdb->insert_id;

			$meta_columns = en_mb_search_meta($wpdatatables_columnsArr, 'table_id',$old_table_id);

			en_mb_insert_wpdatatables_columns($meta_columns,$new_table_id);

			$tablekey = array_keys(LM_DATATABELS, $table_name)[0];
			update_option( $tablekey, $new_table_id );

			$response[] = "<p><strong style='color:green'>Table Name: $table_name, Old Table ID: $old_table_id, New Table ID: $new_table_id : Configured successfully.</strong><p>";
		}else {
			$response[] = "<p><strong style='color:red'>Table Name: $table_name, Old Table ID: $old_table_id : Error in configuration.</strong><p>";
		}
		$table_number++;  
		 
	}
	update_option( '_lm_import_wp_data_tables', 1 );
	return $response;
}


/* Import all wp forms related to loan application*/
function lm_import_wp_formmaker_data(){


	$response=array();

	if(get_option( '_lm_import_wp_data_tables')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}

 
	if(get_option( '_lm_create_wpforms')==1){
		$response[] = "<p><strong style='color:red'>Wp forms already imported.</strong><p>";
		//return $response;
	}

 
	$formJson 	= file_get_contents(dirname(__FILE__).'/wp_form_maker.json');
	$formArr	= json_decode($formJson, true); 
 
	global $wpdb;

	$table_number=1;
	foreach ($formArr as $dtkey => $form) {

		$old_form_id 	= $form['id'];

		$form_name 	= $form['title'];

		unset($form['id']);

		$dataType= en_mb_create_data_types(count($form));

		$formmaker_table = $wpdb->prefix."formmaker";

		if($wpdb->insert( $formmaker_table, $form,$dataType)){
			$new_table_id = $wpdb->insert_id;

			$tablekey = array_keys(LM_WPFORMS, $form_name)[0];
			update_option( $tablekey, $new_table_id );

			$response[] = "<p><strong style='color:green'>Form Name: $form_name, Old Table ID: $old_form_id,  New Table ID: $new_table_id : Configured successfully.</strong><p>";
		}else {
			$response[] = "<p><strong style='color:red'>Table Name: $form_name, Old Table ID: $old_form_id : Error in configuration.</strong><p>";
		}
		$table_number++;  
		 
	}
	update_option( '_lm_create_wpforms', 1 );
	return $response;
}





/***********************Create required Pages************************/
function lm_create_required_pages(){
    global $wpdb;

    $response = array();

    if(get_option( '_lm_create_wpforms')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}

	if(get_option('_en_lm_create_page')==1){

		$response[] = "<strong style='color:red'>Required pages already created.</strong>";
		return $response;

	}

	foreach (LM_PAGES as $option_key => $page) {
		
		$pageName  = $page['title'];
		$shortcode = $page['shortcode'];
		 
		$query=$wpdb->prepare('SELECT ID FROM '. $wpdb->posts .' WHERE post_status="publish" AND post_title = %s AND post_type = \'page\'',$pageName);

		$wpdb->query( $query );

		if ( $wpdb->num_rows ) {
		  
		    $page_id = $wpdb->get_var( $query );

			// Update the post into the database
			wp_update_post( array('ID'=> $page_id,'post_content' => $shortcode ));

		    update_post_meta( $page_id, '_et_pb_page_layout', 'et_full_width_page' );

		    update_option( $option_key, $page_id);

		    $response[] = "<strong style='color:green'>Page Name: $pageName already exists and upated.</strong>";
		    
		} else{

		   	$new_post = array(
		       'post_title' 	=> $pageName,
		       'post_content' 	=> $shortcode,
		       'post_status' 	=> 'publish',
		       'post_author' 	=> get_current_user_id(),
		       'post_type' 		=> 'page',
		       'post_category' 	=> array(0)
		  	);

	       $page_id = wp_insert_post($new_post);

	       update_post_meta( $page_id, '_wp_page_template', $TemplatesArr[$option_key] );

	       update_post_meta( $page_id, '_et_pb_page_layout', 'et_full_width_page' );

	       update_option( $option_key, $page_id);

	       $response[] = "<strong style='color:green'>Page Name: $pageName created.</strong>";

		}
 
	}

	update_option( '_en_lm_create_page', 1);
 	
 	$response[] = "<strong style='color:green'>All required pages has been created.</strong>";
	
	return $response;
	 
}

/***********************Create required Pages************************/
function lm_create_email_templates(){
    global $wpdb;

    $response = array();

    if(get_option( '_en_lm_create_page')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}

	if(get_option('_en_lm_create_email_templates')==1){

		$response[] = "<strong style='color:red'>Email Templates already imported.</strong>";
		return $response;

	}

	$emailTemplates		= file_get_contents(dirname(__FILE__).'/email-templates.json');
	$emailTemplatesArr	= json_decode($emailTemplates, true); 
 
 
	foreach ($emailTemplatesArr as $option_key => $template) {
		
		$post_title  	= $template['post_title'];
		$post_content 	= $template['post_content'];
		$email_subject  = $template['email_subject'];
		 
		$query=$wpdb->prepare('SELECT ID FROM '. $wpdb->posts .' WHERE post_status="publish" AND post_title = %s AND post_type = \'page\'',$post_title);

		$wpdb->query( $query );

		if ( $wpdb->num_rows ) {
		  
		    $tempID = $wpdb->get_var( $query );

			// Update the post into the database
			wp_update_post( array('ID'=> $tempID,'post_content' => $post_content ));

		    update_post_meta( $tempID, 'email_subject', $email_subject);

		    $tablekey = array_keys(LM_EMAIL_TEMPLATES, $post_title)[0];
			update_option( $tablekey, $tempID );

		    $response[] = "<strong style='color:green'>Template Name: $post_title already exists and upated.</strong>";
		    
		} else{

		   	$new_post = array(
		       'post_title' 	=> $post_title,
		       'post_content' 	=> $post_content,
		       'post_status' 	=> 'publish',
		       'post_author' 	=> get_current_user_id(),
		       'post_type' 		=> 'template',
		       'post_category' 	=> array(0)
		  	);

	       $tempID = wp_insert_post($new_post);

	       update_post_meta( $tempID, 'email_subject', $email_subject);

	       $tablekey = array_keys(LM_EMAIL_TEMPLATES, $post_title)[0];
			update_option( $tablekey, $tempID );

	       $response[] = "<strong style='color:green'>Template Name: $post_title imported.</strong>";

		}
 
	}

	update_option( '_en_lm_create_email_templates', 1);
 	
 	$response[] = "<strong style='color:green'>All Templates has been imported.</strong>";
	
	return $response;
	 
}




function lm_add_capability_for_single_role($roleName){

	if(get_option( '_en_lm_create_email_templates')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}
	if(get_option( '_en_lm_add_caps')==1){
		$response[] = "<p><strong style='color:red'>Capabilities already imported.</strong><p>";
		//return $response;
	}

	$role = get_role( $roleName );

	if(!empty($role)){
		$role_caps 		= file_get_contents(dirname(__FILE__).'/'.$roleName.'-caps.json');
		$role_caps_arr	= json_decode($role_caps, true); 

		foreach ($role_caps_arr as $capability => $status) {
			if($status==1){
				$role->add_cap( $capability );
			}	 
		}
		$response[] = "<p><strong style='color:green'>All Capabilities imported/refreshed for $roleName.</strong><p>";
		update_option( '_en_lm_add_caps', 1 );
	}else{
		$response[] = "<p><strong style='color:red'>All Capabilities couldn't import for $roleName. Role does not exist.</strong><p>";
	}


	return $response;
	 
}


/***********************************en_mb_refresh_wp_data_tables**********************************************/

function lm_refresh_wp_data_tables(){
	
	global $wpdb;

	$response=array();

	//Configurations for Loan Front Table
	en_mb_update_column_config(get_option('_lm_table_application_front'),get_option('_en_mb_wp_datatable_2'),'equipment_type');
	en_mb_update_column_config(get_option('_lm_table_application_front'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_lm_table_application_front'),get_option('_lm_table_status_master'),'loan_status');


	//Configurations for Loan reason Table
	en_mb_update_column_config(get_option('_lm_table_reason_master'),get_option('_en_mb_wp_datatable_5'),'created_by');
	en_mb_update_column_config(get_option('_lm_table_location_master'),get_option('_en_mb_wp_datatable_5'),'created_by');
	en_mb_update_column_config(get_option('_lm_table_status_master'),get_option('_en_mb_wp_datatable_5'),'created_by');
	 

	$response[] = "<p><strong style='color:green'>Wpdatatables configurations refreshed.</strong><p>";
	//update_option( '_en_mb_refresh_wp_data_tables_columns', 1 );
	return $response;
}


function lm_get_pages_dropdown($opt_name){
	 $args = array(
			'sort_order' 	=> 'ASC',
			'sort_column' 	=> 'post_title',
			'hierarchical' 	=> 1,
			'parent' 		=> -1,
			'exclude_tree' 	=> '',
			'number' 		=> '',
			'offset' 		=> 0,
			'post_type' 	=> 'page',
			'post_status' 	=> 'publish'
			); 
	$pages = get_pages($args); 

	$selectbox ='<select name="'.$opt_name.'">';
	$option ="<option value=''>Select page</option>";
	foreach ($pages as $key => $value) {
		if(get_option($opt_name)==$value->ID){
			$select="selected";
		}else{
			$select="";
		}
		$option.='<option value="'.$value->ID.'" '.$select.'>'.$value->post_title.'</option>';
	}
	$selectbox .= $option.'</select>';
	return $selectbox;

}


function lm_get_email_templates_dropdown($opt_name){

	$args = array( 'post_type' => 'template', 'posts_per_page' => -1 );
	$loop = new WP_Query( $args );
	
	$selectbox ='<select name="'.$opt_name.'">';
	$option ="<option value=''>Select Email Template</option>";

	while ( $loop->have_posts() ) : $loop->the_post();
	global $post;
		if(get_option($opt_name)==$post->ID){
			$select="selected";
		}else{
			$select="";
		}
		$option.='<option value="'.$post->ID.'" '.$select.'>'.$post->post_title.'</option>';
	 endwhile;
	$selectbox .= $option.'</select>';
	return $selectbox;

}



function lm_update_frontend_menu(){

	if(get_option( '_en_lm_add_caps')!=1){
		$response[] = "<p><strong style='color:red'>Please complete the all steps in sequence.</strong><p>";
		return $response;
	}
	if(get_option("_lm_update_menu")=='1'){

		$response[] = "<p><strong style='color:red'>Menu already updated.</strong><p>";
		return $response;
		
	}	


	$menuLocations 		= get_nav_menu_locations();
	$primary_menu_id 	= $menuLocations['primary-menu']; 

    $main_item_id = get_option('_en_mb_menu_item_1');   // get Maintenance menu item ID
 
    wp_update_post( array('ID'=> $main_item_id,'post_title' => "Equipment Manager" ));
	// Sub menu Items	
	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Equipment Loan Manager'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_lm_page_loan_front')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> $main_item_id
								    	)
							);

	update_option("_lm_menuID_loan_manager",$main_sub_item_id);

	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Loan Manager Settings'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_lm_page_loan_settings')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> get_option('_lm_menuID_loan_manager')
								    	)
							);

	update_option("_lm_menuID_loan_settings",$main_sub_item_id);


	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Reports'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_lm_page_reports')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> get_option('_lm_menuID_loan_manager')
								    	)
							);

	update_option("_lm_menuID_loan_reports",$main_sub_item_id);

	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Equipment Loan Manager History'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_lm_page_history')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> get_option('_lm_menuID_loan_manager')
								    	)
							);

	update_option("_lm_menuID_loan_history",$main_sub_item_id);


	update_option("_lm_update_menu",'1');

	$response[] = "<p><strong style='color:green'>Menu has been updated successfully.</strong><p>";
	return $response;

}
 
function lm_delete_loan_menu_items(){ 
	wp_delete_post(get_option("_lm_menuID_loan_manager"),true);
	wp_delete_post(get_option("_lm_menuID_loan_settings"),true);
	wp_delete_post(get_option("_lm_menuID_loan_history"),true);
	wp_delete_post(get_option("_lm_menuID_loan_reports"),true);

	delete_option("_lm_menuID_loan_manager");
	delete_option("_lm_menuID_loan_settings");
	delete_option("_lm_menuID_loan_history");
	delete_option("_lm_menuID_loan_reports");

	update_option("_lm_update_menu",'0');
}
function lm_remove_loan_capability($roleName){
	global $wp_roles; 
	 
	$role_caps 		= file_get_contents(dirname(__FILE__).'/administrator-caps.json');
	$role_caps_arr	= json_decode($role_caps, true); 

	foreach ($role_caps_arr as $capability => $status) {

		$wp_roles->remove_cap($roleName, $capability);

	}

	$response = "<p><strong style='color:green'>All Capabilities removed for $roleName.</strong><p>";

	return $response;
	 
}
/***********************Remove all configrations************************/
function lm_remove_loan_application(){ 
		global $wpdb;
 		lm_delete_loan_menu_items();
 		lm_remove_loan_capability('administrator');
 		// Delete source tables
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_equipment_settings');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_locations');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_reasons');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_requests');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_requests_meta');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_loan_status');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_manager_history');
		$wpdb->query('DROP TABLE IF EXISTS en_lm_return_reminders');


		// Delete loan application columns from main table
		$alterSql = "ALTER TABLE en_mb_facility_equipment
				DROP `loan_navigation`,
				DROP  `loan_status`,
				DROP  `loan_enable`,
				DROP  `lm_current_custodian`,
				DROP `lm_checking_for_client`,
				DROP `lm_organization` ,
				DROP `lm_current_location`,
				DROP `lm_due_for_return`,
				DROP `lm_auth_required`,
				DROP `lm_expedited_checkout`,
				DROP `lm_max_loan_period`";

	 	$wpdb->query($alterSql);


		$tableName 	= $wpdb->prefix."options";
		$wpdb->query('Delete from '.$tableName.' WHERE option_name LIKE "%_lm_%"');
	  
 
		
		// Delete all wpdatatables
		foreach (LM_DATATABELS as $tablekey => $value) {
			
			$tableName = $wpdb->prefix."wpdatatables";
			$wpdb->query('Delete from '.$tableName.' WHERE id ='.get_option($tablekey));
			$tableName = $wpdb->prefix."wpdatatables_columns";
			$wpdb->query('Delete from '.$tableName.' WHERE table_id ='.get_option($tablekey));
			delete_option( $tablekey);
			
		}

		// Delete all wpdatatables
		foreach (LM_WPFORMS as $formkey => $value) {
			
			$tableName = $wpdb->prefix."formmaker";
			$wpdb->query('Delete from '.$tableName.' WHERE id ='.get_option($formkey));
			delete_option( $formkey);
			
		}

		// Delete all pages
		foreach (LM_PAGES as $pagekey => $value) {
			wp_delete_post(get_option($pagekey),true);	 
			delete_option($pagekey);
		}
		
		// Delete all email templates
		foreach (LM_EMAIL_TEMPLATES as $tempkey => $value) {
			wp_delete_post(get_option($tempkey),true);	 
			delete_option($tempkey);
		}


		$response[] = "<p><strong style='color:green'>All configurations has been removed.</strong><p>";
		return $response;
}