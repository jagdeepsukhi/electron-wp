-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 18, 2019 at 03:31 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.1.25-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electrondemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_equipment_settings`
--

CREATE TABLE `en_lm_loan_equipment_settings` (
  `id` int(11) NOT NULL,
  `equipment_type` int(11) NOT NULL,
  `request_approvers` varchar(255) NOT NULL,
  `max_loan_period` int(11) NOT NULL,
  `advance_return_reminder` int(11) NOT NULL,
  `return_reminder_email_frequency` varchar(255) NOT NULL,
  `enable_reminder_email` int(11) NOT NULL,
  `return_ad_next_sch_event_days` varchar(255) NOT NULL,
  `return_ad_next_sch_event_miles` varchar(255) NOT NULL,
  `expedited_checkout_duration` int(11) NOT NULL,
  `enable_asset_loan` varchar(100) NOT NULL,
  `Auth_required_for_checkout` varchar(100) NOT NULL,
  `modify_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_lm_loan_equipment_settings`
--

INSERT INTO `en_lm_loan_equipment_settings` (`id`, `equipment_type`, `request_approvers`, `max_loan_period`, `advance_return_reminder`, `return_reminder_email_frequency`, `enable_reminder_email`, `return_ad_next_sch_event_days`, `return_ad_next_sch_event_miles`, `expedited_checkout_duration`, `enable_asset_loan`, `Auth_required_for_checkout`, `modify_on`, `modify_by`) VALUES
(1, 1, '25', 5, 0, '2', 1, '0', '10', 10, 'Yes', 'Yes', '2019-02-15 06:05:25', 1),
(2, 2, '153,25', 20, 4, '2', 1, '5', '20', 11, 'Yes', 'Yes', '2019-02-15 06:05:25', 1),
(3, 3, '1', 1, 0, '3', 0, '5', '10', 10, 'Yes', 'No', '2019-02-15 06:05:25', 1),
(4, 4, '25', 5, 1, '2', 1, '1', '1', 3, 'Yes', 'Yes', '2019-02-15 06:05:25', 1),
(5, 6, '25', 10, 10, '1', 1, '1', '0', 12, 'Yes', 'Yes', '2019-02-15 06:05:25', 1),
(6, 7, '25', 4, 1, '1', 1, '0', '1', 1, 'Yes', 'Yes', '2019-02-15 06:05:25', 1);

--
-- Triggers `en_lm_loan_equipment_settings`
--
DELIMITER $$
CREATE TRIGGER `Loan Manager Settings Audit On Insert` AFTER INSERT ON `en_lm_loan_equipment_settings` FOR EACH ROW INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES 
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'equipment_type','',NEW.equipment_type,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'request_approvers','',NEW.request_approvers,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'max_loan_period','',NEW.max_loan_period,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'advance_return_reminder','',NEW.advance_return_reminder,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'return_reminder_email_frequency','',NEW.return_reminder_email_frequency,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'enable_reminder_email','',NEW.enable_reminder_email,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'return_ad_next_sch_event_days','',NEW.return_ad_next_sch_event_days,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'return_ad_next_sch_event_miles','',NEW.return_ad_next_sch_event_miles,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'expedited_checkout_duration','',NEW.expedited_checkout_duration,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'enable_asset_loan','',NEW.enable_asset_loan,'add'),
 ('', CONCAT('LS',NEW.id), NEW.modify_by,'Auth_required_for_checkout','',NEW.Auth_required_for_checkout,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Manager Settings History Audit Update` AFTER UPDATE ON `en_lm_loan_equipment_settings` FOR EACH ROW BEGIN 

 IF (OLD.request_approvers <> NEW.request_approvers) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'request_approvers',OLD.request_approvers,NEW.request_approvers,'edit'); 

 END IF; 

IF (OLD.max_loan_period <> NEW.max_loan_period) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'max_loan_period',OLD.max_loan_period,NEW.max_loan_period,'edit'); 
END IF; 

IF (OLD.advance_return_reminder <> NEW.advance_return_reminder) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'advance_return_reminder',OLD.advance_return_reminder,NEW.advance_return_reminder,'edit'); 
END IF; 


IF (OLD.return_reminder_email_frequency <> NEW.return_reminder_email_frequency) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'return_reminder_email_frequency',OLD.return_reminder_email_frequency,NEW.return_reminder_email_frequency,'edit'); 
END IF; 


IF (OLD.enable_reminder_email <> NEW.enable_reminder_email) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'enable_reminder_email',OLD.enable_reminder_email,NEW.enable_reminder_email,'edit'); 
END IF; 


IF (OLD.return_ad_next_sch_event_days <> NEW.return_ad_next_sch_event_days) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'return_ad_next_sch_event_days',OLD.return_ad_next_sch_event_days,NEW.return_ad_next_sch_event_days,'edit'); 
END IF; 


IF (OLD.return_ad_next_sch_event_miles <> NEW.return_ad_next_sch_event_miles) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'return_ad_next_sch_event_miles',OLD.return_ad_next_sch_event_miles,NEW.return_ad_next_sch_event_miles,'edit'); 
END IF; 



IF (OLD.expedited_checkout_duration <> NEW.expedited_checkout_duration) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'expedited_checkout_duration',OLD.expedited_checkout_duration,NEW.expedited_checkout_duration,'edit'); 
END IF; 

IF (OLD.enable_asset_loan <> NEW.enable_asset_loan) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'enable_asset_loan',OLD.enable_asset_loan,NEW.enable_asset_loan,'edit'); 
END IF; 


IF (OLD.Auth_required_for_checkout <> NEW.Auth_required_for_checkout) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS',OLD.id), NEW.modify_by,'Auth_required_for_checkout',OLD.Auth_required_for_checkout,NEW.Auth_required_for_checkout,'edit'); 
END IF; 


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_locations`
--

CREATE TABLE `en_lm_loan_locations` (
  `id` int(11) NOT NULL,
  `location` varchar(250) NOT NULL,
  `support_eq_types` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_lm_loan_locations`
--

INSERT INTO `en_lm_loan_locations` (`id`, `location`, `support_eq_types`, `created_by`, `created_on`) VALUES
(1, 'Battery Lab', '1,2,3,4,6,7', 140, '2019-02-01 11:14:16'),
(2, 'Washing ', '1,2,3,4,6,7', 0, '2019-02-01 11:14:16'),
(3, 'Suspension Lab ', '1,2,3,4,6,7', 140, '2019-02-01 11:14:16'),
(4, 'Quick Checkout', '1,2,3,4,6,7', 1, '2019-02-01 11:14:16'),
(5, 'Rubbing Center', '1,2,3,4,6,7', 1, '2019-02-01 11:14:16'),
(6, 'Sunspaction', '1,2,3,4,6,7', 1, '2019-02-01 10:17:50');

--
-- Triggers `en_lm_loan_locations`
--
DELIMITER $$
CREATE TRIGGER `Loan Location History Audit on Delete` AFTER DELETE ON `en_lm_loan_locations` FOR EACH ROW BEGIN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Location',OLD.id), OLD.created_by,'location',OLD.location,'','delete'),
      ('', CONCAT('Location',OLD.id), OLD.created_by,'support_eq_types',OLD.support_eq_types,'','delete'); 

 END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Location History Audit on Update` AFTER UPDATE ON `en_lm_loan_locations` FOR EACH ROW BEGIN 

IF (OLD.location <> NEW.location) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Location',NEW.id), NEW.created_by,'location',OLD.location,NEW.location,'edit'); 
END IF;



IF (OLD.support_eq_types <> NEW.support_eq_types) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Location',NEW.id), NEW.created_by,'support_eq_types',OLD.support_eq_types,NEW.support_eq_types,'edit'); 
END IF;


 END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Locations History Audit on Insert` AFTER INSERT ON `en_lm_loan_locations` FOR EACH ROW BEGIN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES 
    ('', CONCAT('Location',NEW.id), NEW.created_by,'location','',NEW.location,'add'), 
    ('', CONCAT('Location',NEW.id), NEW.created_by,'support_eq_types','',NEW.support_eq_types,'add'); 

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_reasons`
--

CREATE TABLE `en_lm_loan_reasons` (
  `id` int(11) NOT NULL,
  `reason` varchar(250) NOT NULL,
  `support_eq_types` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_lm_loan_reasons`
--

INSERT INTO `en_lm_loan_reasons` (`id`, `reason`, `support_eq_types`, `created_by`, `created_on`) VALUES
(1, 'Yes will generate an email to maintenance managers regard.', '1,2,3,4', 1, '2019-01-31 19:28:11');

--
-- Triggers `en_lm_loan_reasons`
--
DELIMITER $$
CREATE TRIGGER `Loan Reason History Audit on Insert` AFTER INSERT ON `en_lm_loan_reasons` FOR EACH ROW BEGIN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES 
    ('', CONCAT('Reason',NEW.id), NEW.created_by,'reason','',NEW.reason,'add'), 
    ('', CONCAT('Reason',NEW.id), NEW.created_by,'support_eq_types','',NEW.support_eq_types,'add'); 

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Reason History Audit on Update` AFTER UPDATE ON `en_lm_loan_reasons` FOR EACH ROW BEGIN 

IF (OLD.reason <> NEW.reason) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Reason',NEW.id), NEW.created_by,'reason',OLD.reason,NEW.reason,'edit'); 
END IF;



IF (OLD.support_eq_types <> NEW.support_eq_types) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Reason',NEW.id), NEW.created_by,'support_eq_types',OLD.support_eq_types,NEW.support_eq_types,'edit'); 
END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Resaon History Audit on Delete` AFTER DELETE ON `en_lm_loan_reasons` FOR EACH ROW BEGIN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('Reason',OLD.id), OLD.created_by,'reason',OLD.reason,'','delete'),
      ('', CONCAT('Reason',OLD.id), OLD.created_by,'support_eq_types',OLD.support_eq_types,'','delete'); 

 END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_requests`
--

CREATE TABLE `en_lm_loan_requests` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(100) NOT NULL,
  `date_of_checkout` date NOT NULL,
  `check_out_type` varchar(255) NOT NULL,
  `check_in_type` varchar(255) NOT NULL,
  `requested_return_date` date NOT NULL,
  `actual_return_date` date NOT NULL,
  `odometer_reading_checkout` varchar(100) NOT NULL,
  `odometer_reading_return` varchar(100) NOT NULL,
  `usage_location` varchar(250) NOT NULL,
  `checkin_location` varchar(250) NOT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `organization` varchar(250) NOT NULL,
  `checking_for_client` varchar(100) NOT NULL,
  `client_name` varchar(250) NOT NULL,
  `project` varchar(250) NOT NULL,
  `loan_policy` varchar(100) NOT NULL,
  `approve_status` varchar(100) NOT NULL,
  `check_in_out_status` varchar(100) NOT NULL,
  `experience_issue` varchar(100) NOT NULL,
  `issue_to_manager` varchar(100) NOT NULL,
  `additional_damage` varchar(100) NOT NULL,
  `experience_note` longtext NOT NULL,
  `common_notes` longtext NOT NULL,
  `approved_deny_by` int(11) NOT NULL,
  `checkin_by` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modify_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `request_token` longtext NOT NULL,
  `last_return_reminder` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `en_lm_loan_requests`
--
DELIMITER $$
CREATE TRIGGER `Loan Request History Audit On Insert` AFTER INSERT ON `en_lm_loan_requests` FOR EACH ROW INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'date_of_checkout','',NEW.date_of_checkout,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'check_out_type','',NEW.check_out_type,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'requested_return_date','',NEW.requested_return_date,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'odometer_reading_checkout','',NEW.odometer_reading_checkout,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'usage_location','',NEW.usage_location,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'reason','',NEW.reason,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'organization','',NEW.organization,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'checking_for_client','',NEW.checking_for_client,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'client_name','',NEW.client_name,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'project','',NEW.project,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'loan_policy','',NEW.loan_policy,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'approve_status','',NEW.approve_status,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'check_in_out_status','',NEW.check_in_out_status,'Add'),
 (NEW.asset_id, CONCAT('LR',NEW.id), NEW.created_by,'check_in_out_status','',NEW.check_in_out_status,'Add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Request History Audit On Update` AFTER UPDATE ON `en_lm_loan_requests` FOR EACH ROW BEGIN 

 IF (OLD.check_in_type <> NEW.check_in_type) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'check_in_type',OLD.check_in_type,NEW.check_in_type,'edit'); 

 END IF; 


IF (OLD.actual_return_date <> NEW.actual_return_date) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'actual_return_date',OLD.actual_return_date,NEW.actual_return_date,'edit'); 

 END IF; 
 
IF (OLD.odometer_reading_checkout <> NEW.odometer_reading_checkout) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'odometer_reading_checkout',OLD.odometer_reading_checkout,NEW.odometer_reading_checkout,'edit'); 

END IF;  

IF (OLD.odometer_reading_return <> NEW.odometer_reading_return) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'odometer_reading_return',OLD.odometer_reading_return,NEW.odometer_reading_return,'edit'); 

 END IF; 

 IF (OLD.usage_location <> NEW.usage_location) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'usage_location',OLD.usage_location,NEW.usage_location,'edit'); 

 END IF; 

IF (OLD.checkin_location <> NEW.checkin_location) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'checkin_location',OLD.checkin_location,NEW.checkin_location,'edit'); 

 END IF; 

IF (OLD.reason <> NEW.reason) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'reason',OLD.reason,NEW.reason,'edit'); 

 END IF;  

IF (OLD.organization <> NEW.organization) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'organization',OLD.organization,NEW.organization,'edit'); 

 END IF; 

IF (OLD.checking_for_client <> NEW.checking_for_client) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'checking_for_client',OLD.checking_for_client,NEW.checking_for_client,'edit'); 

 END IF; 

IF (OLD.client_name <> NEW.client_name) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'client_name',OLD.client_name,NEW.client_name,'edit'); 

 END IF;  

IF (OLD.project <> NEW.project) THEN 

 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'project',OLD.project,NEW.project,'edit'); 

 END IF;  

 IF (OLD.loan_policy <> NEW.loan_policy) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'loan_policy',OLD.loan_policy,NEW.loan_policy,'edit'); 
 END IF; 


IF (OLD.approve_status <> NEW.approve_status) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'approve_status',OLD.approve_status,NEW.approve_status,'edit'); 
 END IF;  

 IF (OLD.check_in_out_status <> NEW.check_in_out_status) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'check_in_out_status',OLD.check_in_out_status,NEW.check_in_out_status,'edit'); 
 END IF;   

IF (OLD.experience_issue <> NEW.experience_issue) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'experience_issue',OLD.experience_issue,NEW.experience_issue,'edit'); 
 END IF; 

   
IF (OLD.issue_to_manager <> NEW.issue_to_manager) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'issue_to_manager',OLD.issue_to_manager,NEW.issue_to_manager,'edit'); 
 END IF;  

     
IF (OLD.additional_damage <> NEW.additional_damage) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'additional_damage',OLD.additional_damage,NEW.additional_damage,'edit'); 
 END IF;   

    
IF (OLD.experience_note <> NEW.experience_note) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'experience_note',OLD.experience_note,NEW.experience_note,'edit'); 
 END IF;   

    
IF (OLD.common_notes <> NEW.common_notes) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'common_notes',OLD.common_notes,NEW.common_notes,'edit'); 
 END IF;   

    
IF (OLD.approved_deny_by <> NEW.approved_deny_by) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'approved_deny_by',OLD.approved_deny_by,NEW.approved_deny_by,'edit'); 
 END IF;     
 
IF (OLD.checkin_by <> NEW.checkin_by) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'checkin_by',OLD.checkin_by,NEW.checkin_by,'edit'); 
 END IF;   


IF (OLD.checkin_by <> NEW.checkin_by) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('LR',OLD.id), NEW.modify_by,'checkin_by',OLD.checkin_by,NEW.checkin_by,'edit'); 
 END IF;   

 END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_requests_meta`
--

CREATE TABLE `en_lm_loan_requests_meta` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `component_name` varchar(250) NOT NULL,
  `component_description` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `is_additional` int(11) NOT NULL,
  `is_old` int(11) NOT NULL,
  `modify_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `en_lm_loan_requests_meta`
--
DELIMITER $$
CREATE TRIGGER `Loan Request Meta History Audit on Delete` AFTER DELETE ON `en_lm_loan_requests_meta` FOR EACH ROW INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LR-META-',OLD.id), OLD.modify_by,'component_name',OLD.component_name,'','delete'),
  ('', CONCAT('LR-META-',OLD.id), OLD.modify_by,'component_description',OLD.component_description,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Request Meta History audit On Update` AFTER UPDATE ON `en_lm_loan_requests_meta` FOR EACH ROW BEGIN 

 IF (OLD.component_name <> NEW.component_name) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LR-META-',NEW.id), NEW.created_by,'component_name',OLD.component_name,NEW.component_name,'update'); 

 END IF; 


IF (OLD.component_description <> NEW.component_description) THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LR-META-',NEW.id), NEW.created_by,'component_description',OLD.component_description,NEW.component_description,'update'); 

 END IF; 

 END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Request Meta Audit History On Insert` AFTER INSERT ON `en_lm_loan_requests_meta` FOR EACH ROW BEGIN 
 IF(NEW.component_name!="") THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LR-META-',NEW.id), NEW.created_by,'component_name','',NEW.component_name,'add');
  END IF; 

IF(NEW.component_description!="") THEN 
 INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LR-META-',NEW.id), NEW.created_by,'component_description','',NEW.component_description,'add');
  END IF; 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_loan_status`
--

CREATE TABLE `en_lm_loan_status` (
  `id` int(11) NOT NULL,
  `status` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_lm_loan_status`
--

INSERT INTO `en_lm_loan_status` (`id`, `status`, `created_by`, `created_date`) VALUES
(1, 'Pending', 1, '2018-12-04 10:04:27'),
(2, 'Checked Out', 1, '2018-12-04 10:04:21'),
(3, 'Checked In', 1, '2018-12-05 06:28:32'),
(4, 'No', 1, '2018-12-05 07:55:00'),
(5, 'Canceled', 1, '2018-12-27 06:04:59'),
(6, 'Yes', 1, '2018-12-27 06:15:00'),
(7, 'Past Due', 1, '2019-01-23 11:41:18'),
(8, 'Recalled', 1, '2019-02-01 09:18:17');

--
-- Triggers `en_lm_loan_status`
--
DELIMITER $$
CREATE TRIGGER `Loan Status History Audit on Insert` BEFORE INSERT ON `en_lm_loan_status` FOR EACH ROW BEGIN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES 
    ('', CONCAT('LS-',NEW.id), NEW.created_by,'status','',NEW.status,'add'); 

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Loan Status History Audit on Update` AFTER UPDATE ON `en_lm_loan_status` FOR EACH ROW BEGIN 

IF (OLD.status <> NEW.status) THEN 
INSERT INTO en_lm_manager_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('LS-',NEW.id), NEW.created_by,'status',OLD.status,NEW.status,'edit'); 
END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_manager_history`
--

CREATE TABLE `en_lm_manager_history` (
  `equipment_record_id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `before_value` longtext NOT NULL,
  `after_value` longtext,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_lm_manager_history`
--

INSERT INTO `en_lm_manager_history` (`equipment_record_id`, `asset_id`, `date_time`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES
(1, '', '2019-02-18 09:59:11', 'Reason3', 1, 'reason', 'fffsdfsf', '', 'delete'),
(2, '', '2019-02-18 09:59:11', 'Reason3', 1, 'support_eq_types', '1,2,3,4', '', 'delete'),
(3, '', '2019-02-18 09:59:11', 'Reason4', 1, 'reason', 'AS', '', 'delete'),
(4, '', '2019-02-18 09:59:11', 'Reason4', 1, 'support_eq_types', '1,2,3,4', '', 'delete'),
(5, '', '2019-02-18 09:59:11', 'Reason5', 1, 'reason', 'asdasdasdasdasd', '', 'delete'),
(6, '', '2019-02-18 09:59:11', 'Reason5', 1, 'support_eq_types', '1,2,4', '', 'delete'),
(7, '', '2019-02-18 09:59:11', 'Reason6', 1, 'reason', 'yyyyyyyy', '', 'delete'),
(8, '', '2019-02-18 09:59:11', 'Reason6', 1, 'support_eq_types', '1,2,4,6,7', '', 'delete'),
(9, '', '2019-02-18 09:59:11', 'Reason20', 1, 'reason', 'fghdgjcx', '', 'delete'),
(10, '', '2019-02-18 09:59:11', 'Reason20', 1, 'support_eq_types', '', '', 'delete'),
(11, '', '2019-02-18 09:59:19', 'Location7', 1, 'location', 'asgwwwwwwwwwwwwwww', '', 'delete'),
(12, '', '2019-02-18 09:59:19', 'Location7', 1, 'support_eq_types', '', '', 'delete');

-- --------------------------------------------------------

--
-- Table structure for table `en_lm_return_reminders`
--

CREATE TABLE `en_lm_return_reminders` (
  `reminder_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `reminder_type` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_sent_on` date NOT NULL,
  `next_send_on` date NOT NULL,
  `message` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `en_lm_loan_equipment_settings`
--
ALTER TABLE `en_lm_loan_equipment_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_loan_locations`
--
ALTER TABLE `en_lm_loan_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_loan_reasons`
--
ALTER TABLE `en_lm_loan_reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_loan_requests`
--
ALTER TABLE `en_lm_loan_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_loan_requests_meta`
--
ALTER TABLE `en_lm_loan_requests_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_loan_status`
--
ALTER TABLE `en_lm_loan_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_lm_manager_history`
--
ALTER TABLE `en_lm_manager_history`
  ADD PRIMARY KEY (`equipment_record_id`);

--
-- Indexes for table `en_lm_return_reminders`
--
ALTER TABLE `en_lm_return_reminders`
  ADD PRIMARY KEY (`reminder_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `en_lm_loan_equipment_settings`
--
ALTER TABLE `en_lm_loan_equipment_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `en_lm_loan_locations`
--
ALTER TABLE `en_lm_loan_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `en_lm_loan_reasons`
--
ALTER TABLE `en_lm_loan_reasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `en_lm_loan_requests`
--
ALTER TABLE `en_lm_loan_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `en_lm_loan_requests_meta`
--
ALTER TABLE `en_lm_loan_requests_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `en_lm_loan_status`
--
ALTER TABLE `en_lm_loan_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `en_lm_manager_history`
--
ALTER TABLE `en_lm_manager_history`
  MODIFY `equipment_record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `en_lm_return_reminders`
--
ALTER TABLE `en_lm_return_reminders`
  MODIFY `reminder_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
