<?php 
global $wpdb;

$action = $_GET['action'];
$token 	= $_GET['token'];

if(empty($token) || empty($action)){
	wp_die("URL is not valid!");
}

$tokenArr 		= explode("__", $token);
$originalToken 	= $tokenArr[0];
$approverString = $tokenArr[1];
$asset_id 		= $tokenArr[2];
$salt 			= "AJHSGHAGSAGJS";
$approver_raw 	= base64_decode($approverString);
$approver_id 	= preg_replace(sprintf('/%s/', $salt), '', $approver_raw);
$data			= array();


if($action=="aprrove"){
	
	$data['approved_deny_by'] 	= $approver_id;
	$data['common_notes']		= "Approved From Mail";
	$data['approve_status']		= 6;  // Approved
	$data['check_in_out_status']= 2;  //Checked out  
	$data['request_token']		= ''; //Destroy token 


}elseif ($action=="deny") {

	$data['approved_deny_by'] 	= $approver_id;
	$data['common_notes']		= "Deny From Mail";
	$data['approve_status']		= 4;  // Deny
	$data['check_in_out_status']= 4;  //Deny
	$data['request_token']		= ''; //Destroy token 

}else{
	wp_die("Somthing went wronge. Please try agian!");
}

 

$response = $wpdb->update("en_lm_loan_requests", $data, array("request_token"=>$originalToken ) );

if($response){

	lm_update_facility_equipment_table($asset_id);
	echo "<p style='color:green'>Request status has been updated successfully.</p>";

	if($data['approve_status']==4){
		lm_send_notification_loan_request_denied($asset_id);
	}else if($data['approve_status']==6){
		lm_send_notification_loan_request_approved($asset_id);
	}

}else{

	echo "<p style='color:red'>Sorry! Token has been expired or already used by another aprrover.</p>";
}


