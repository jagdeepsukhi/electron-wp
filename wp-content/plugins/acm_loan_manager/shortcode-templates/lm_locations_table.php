<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
global $wpdb;
?>
<a href="<?= get_permalink(get_option('_lm_page_loan_settings')) ?>" class="btn btn-info"> << Back</a>

<?php 
/*
* Form Template for loan locations
*/

if(!lm_can_view_loan_settings()){
  wp_die("You do not have permission to view the loan settings.");
}
global $allow_edit;
if(lm_can_edit_loan_settings()) {
  $allow_edit =1;
}else{
  $allow_edit =0;
}

/* *******************Set Edit capability***************** */
function wpdatatables_allow_edit_table_callback( $edit ) {
    global $allow_edit;
    return $allow_edit;  
}

add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );
?>

<?php $tableID = get_option("_lm_table_location_master"); ?>
<?php echo do_shortcode("[wpdatatable id=".$tableID."]");

$equip_Arr    = lm_get_all_equipment_types();
$sql2         = "SELECT * FROM `en_lm_loan_locations` "; 
$alllocations = $wpdb->get_results($sql2);

?>




<h2>Supported Equipment Types</h2>

<form id="location-relation" method="post">
  <table class="responsive">
    <thead>
      <tr><th> </th> 
      <?php foreach ($equip_Arr as $key => $type) { ?>
            <th><?= $type->equipment_type ?> </th>
      <?php } ?>
      <tr>
    </thead>
    <tbody>
      <?php foreach ($alllocations as $key => $location) { 
        $savedEqTypes = explode(',', $location->support_eq_types)
      ?>
          <tr>
          <td><?= $location->location?></td>
          <?php foreach ($equip_Arr as $key => $type) { ?>
            <td><input type="checkbox" name="support_types[<?= $location->id ?>][]" value="<?= $type->id ?>" <?php if(in_array($type->id,  $savedEqTypes)) { echo "checked"; } ?> ></td>
          <?php } ?>
           
        </tr>
      <?php } ?>
    </tbody>
  </table>
  <input type="hidden" name="action" value="save_location_relation">
<?php if(lm_can_edit_loan_settings()) { ?>
  <button type="submit" class="btn btn-success">Save</button>
<?php }  ?>
  <a href="<?= get_permalink(get_option('_lm_page_loan_location')) ?>" title="Load newly added/updated location" class="refresh-a"><img class="refresh-icon" src="<?= get_bloginfo('url').'/wp-content/plugins/acm_loan_manager/icons/refresh.png' ?>"></a>
 </form> 
<!-- Loder -->
<?php do_action("lm_loader"); ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
   
    jQuery("body").on('click', '.DTTT_button_edit',function(){  
  
        jQuery("#table_1_created_by").parent().parent().parent().parent().hide();
        jQuery("#table_1_created_by").val("<?php echo get_current_user_id();?>");  

    });
    jQuery("body").on('click', '.DTTT_button_new',function(){  
       
        jQuery("#table_1_created_by").parent().parent().parent().parent().hide();
        jQuery("#table_1_created_by").val("<?php echo get_current_user_id();?>");  

    });

 });
</script> 