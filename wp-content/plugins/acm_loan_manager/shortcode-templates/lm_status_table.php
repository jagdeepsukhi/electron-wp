<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
if(!lm_can_view_loan_settings()){
  wp_die("You do not have permission to view the loan settings.");
}
global $allow_edit;
if(lm_can_edit_loan_settings()) {
  $allow_edit =1;
}else{
  $allow_edit =0;
}

/* *******************Set Edit capability***************** */
function wpdatatables_allow_edit_table_callback( $edit ) {
    global $allow_edit;
    return $allow_edit;  
}

add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );


?>

<a href="<?= get_permalink(get_option('_lm_page_loan_settings')) ?>" class="btn btn-info"> << Back</a>
<?php $tableID = get_option("_lm_table_status_master"); ?>
<?php echo do_shortcode("[wpdatatable id=".$tableID."]"); ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
   
   jQuery("body").on('click', '.DTTT_button_new',function(){
    jQuery("#table_1_created_by").parent().parent().parent().parent().hide();
    jQuery("#table_1_created_by").val("<?php echo get_current_user_id();?>");  

  });

  jQuery("body").on('click', '.DTTT_button_edit',function(){   
       jQuery("#table_1_created_by").parent().parent().parent().parent().hide();
       jQuery("#table_1_created_by").val("<?php echo get_current_user_id();?>");      
  });


  });
</script> 
<style type="text/css">
  .DTTT_button_delete, .DTTT_button_new {display: none;}
</style>