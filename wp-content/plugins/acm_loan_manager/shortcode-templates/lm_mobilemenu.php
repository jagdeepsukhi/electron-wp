<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/


$asset_id   = $_GET['asset_id'];
if(empty($asset_id)){
  echo "<p style='color:red'> <strong>Asset ID is required</strong></p>";
   return;
}

// Check permissions
if(!lm_can_view_overview_page($asset_id)){
  wp_die("You do not have permission to view the overview page for this item.");
} 


$assetData  = lm_get_equipment_by_asset_id($asset_id);
$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);


?>
<p><strong>Asset Loan Options:</strong> <?= $asset_id  ?></p><hr> 
<?php  
$maintenance_status = lm_check_maintenance_status($asset_id);  
if($maintenance_status!=1){
    echo  '<div class="alert alert-danger">This asset cannot be checked out at this time because it is not in Service. Please reach out to the manager of this equipment to learn when it will be back in service.</div>';
    return;
}


 if(lm_loan_enabled($asset_id)=='No'){
    echo  '<div class="alert alert-danger">This piece of equipment is not enabled for Loan Manager Application.</div>';
    return;
}

?>


<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkout_buttons_popup"); ?>

<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkin_buttons_popup"); ?>

<!-- Recall Modal -->
<?php do_action("lm_recall_popup"); ?>

<!-- Warrning modal: if onclick need to confirm -->
<?php do_action("lm_one_click_warrning_popup"); ?>

<!-- Loder -->
<?php do_action("lm_loader"); ?>

<!-- Fast Form Checkout Form Popup -->
<?php do_action("lm_fast_form_checkout_popup"); ?>

<!-- ////////////////////////////////////////////////////////////////////////////////////////////// -->
 
<?php if($assetData->loan_status==3 || empty($assetData->loan_status) ) { ?> 
		
		<?php
		// Skip confirmation process if authorization not reqiured.
		$Auth_required = lm_is_authorization_required($asset_id);
		if($Auth_required=='Yes'){
			$approveArr = lm_return_all_approvers($asset_id);

			// If current user is approver also, No need to follow confirmation process.
			if(!in_array(get_current_user_id(), $approveArr)){
				echo  '<div class="alert alert-warning">Authorization is reqiured!</div>';
			} 
		} 

		?>
		<!-- Action Buttons -->
		<div class="lm-mobile">  
			<?php if(lm_can_checkout_with_oneclick($asset_id)){ $anybtn=true; ?> 
			 <button type="button" class="btn btn-primary form-btn col-md-4" data-form-type="one-click-quick-checkout" data-asset-id="<?= $asset_id ?>">One Click Checkout</button>
			<?php } ?>

			<?php if(lm_can_checkout_with_fastform($asset_id)){ $anybtn=true; ?> 
			  <button type="button" class="btn btn-info form-btn col-md-4" data-form-type="fast-quick-checkout-form" data-asset-id="<?= $asset_id ?>">Fast Checkout</button>
			<?php } ?>

			<?php if(lm_can_checkout_with_fullform($asset_id)){ $anybtn=true; ?> 
			  <button type="button" class="btn btn-success form-btn col-md-4" data-form-type="full-request-form" data-asset-id="<?= $asset_id ?>">Full Checkout</button> 
			<?php } ?>
		</div>
<?php 
if($anybtn == false){
	echo '<div class="alert alert-danger">You do not have permission to check out this item.</div>';
    return;
  	}
?>
<?php return;  } // endif checkin?>


<!-- ////////////////////////////////////////////////////////////////////////////////////////////// -->


<?php if($assetData->loan_status==1 ) { // Pending
	
	if(!lm_can_edit_pending_request($asset_id)){
		echo  '<div class="alert alert-danger">This piece of equipment is pending checkout approval for another user.</div>';
    	return;

  	}
 ?> 

<!-- Action Buttons -->
<div class="lm-mobile">  
 <a type="button" class="btn btn-warning form-btn col-md-4" href="<?= get_bloginfo('url') ?>/loan-pending-request-form/?asset_id=<?= $asset_id ?>">Checkout Pending</a>
</div>

<?php return; } ?>




<!-- ////////////////////////////////////////////////////////////////////////////////////////////// -->


<?php if($assetData->loan_status==2 || $assetData->loan_status==8 || $assetData->loan_status==7) { // Checkout
	$anybtn = false;	 
?> 

<!-- Action Buttons -->
<div class="lm-mobile"> 

	<div class="alert alert-danger">Due back on <?= $loanData['loanRequest']->requested_return_date ?></div>
	<?php if(lm_can_checkin_with_quickform($asset_id)){ $anybtn=true; ?> 
	<button type="button" class="btn btn-primary form-btn" data-form-type="one-click-quick-checkin" data-asset-id="<?= $asset_id ?>">One Click/Quick Check-in</button>
	<?php } ?>

	<?php if(lm_can_checkin_with_fullform($asset_id)){ $anybtn=true; ?> 
    <button type="button" class="btn btn-success form-btn" data-form-type="full-return-form" data-asset-id="<?= $asset_id ?>">Full Checkin</button>
    <?php } ?>
</div>

<?php 
if($anybtn == false){
	echo '<div class="alert alert-danger">This piece of equipment is checked out for another user.</div>';
    return;
  	}
?>
<?php return; } ?>

 
<!-- ////////////////////////////////////////////////////////////////////////////////////////////// -->      

<input type="hidden" id="overview-assest_id" value="<?= $asset_id?>">
<!-- End -->
<?php //echo do_shortcode("[wpdatatable id=68 VAR1='".$asset_id ."']"); ?>
 
