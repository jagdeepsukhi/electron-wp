<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}

if(!lm_can_view_loan_settings()){
  wp_die("You do not have permission to view the loan settings.");
}
global $allow_edit;
if(lm_can_edit_loan_settings()) {
  $allow_edit =1;
}else{
  $allow_edit =0;
}

/* *******************Set Edit capability***************** */
function wpdatatables_allow_edit_table_callback( $edit ) {
    global $allow_edit;
    return $allow_edit;  
}

add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );


global $wpdb;
/* ************************Get All Equipments************************ */
$page_slug                = 'facility-equipment';
$equipmentData            = array();
$equipment_type_ids_array = array();
$all_equipment_types_name = array();
 
$equipment_sql        = "SELECT equipment_type,id FROM en_mb_equipment_types";
$equipmentArr         = $wpdb->get_results( $equipment_sql, OBJECT );
 

if(!empty($_GET['equipment_type'])){
   $eq_type = $_GET['equipment_type'];
   $var1 ='AND e.equipment_type IN ('.$eq_type.')';
}else{
    $var1 = ' ';
}



if(!empty($_GET['loan_enable'])){
    $loan_enable = $_GET['loan_enable'];
    if($loan_enable=="Not Set"){
       $var2 .= 'AND  e.loan_enable = "" ';
    }else{
      $var2 .= 'AND  e.loan_enable = '.'"'.$loan_enable.'"'.'';
    }
    
}else{
  $var2 = ' ';
}


if(!empty($_GET['loan_status'])){
    $loan_status = $_GET['loan_status'];
    $var2 .= ' AND  e.loan_status = '.'"'.$loan_status.'"'.'';
    
}else{
  $var2 .= ' ';
}


if(!empty($_GET['keyword'])){
    $keyword = $_GET['keyword'];
    $var3 .= 'AND ( e.make LIKE '.'"%'.$keyword.'%"'.'';
    $var3 .= ' OR e.model LIKE '.'"%'.$keyword.'%"'.'';
    $var3 .= ' OR e.asset_id LIKE '.'"%'.$keyword.'%"'.')';
    
}else{
  $var3 = ' ';
}
  $tableID = get_option("_lm_table_individual_settings"); 
  $wpTable_Shortcode = "[wpdatatable id=".$tableID."  VAR1='".$var1."' VAR2='".$var2."' VAR3='".$var3."' ]";
?>

<form method="get" id="status_filter_form">
     
    <select name="equipment_type" id="equipment_type" class="btn btn-warning">
      <option value="">All Equipment Type</option>
      <?php foreach ($equipmentArr as $key => $equipment) { ?>
      <option value="<?=  $equipment->id ?>" <?php if($_GET['equipment_type']==$equipment->id) { echo "selected"; } ?>><?=  $equipment->equipment_type ?></option>
      <?php } ?>
    </select>
     
    <select name="loan_status" id="loan_status" class="btn  btn-warning">
      <option value="">All Loan Status</option>
      <option value="3" <?php if($_GET['loan_status']=='3') { echo "selected"; } ?>>Checked In</option>
      <option value="2" <?php if($_GET['loan_status']=='2') { echo "selected"; } ?>>Checked Out</option>
      <option value="1" <?php if($_GET['loan_status']=='1') { echo "selected"; } ?>>Pending</option>  
      <option value="8" <?php if($_GET['loan_status']=='8') { echo "selected"; } ?>>Recalled</option>  
    </select>

     <select name="loan_enable" id="loan_enable" class="btn  btn-warning">
      <option value="">All Loan Enable/Disable</option>
      <option value="Not Set" <?php if($_GET['loan_enable']=='Not Set') { echo "selected"; } ?>>Not Set</option>
      <option value="Yes" <?php if($_GET['loan_enable']=='Yes') { echo "selected"; } ?>>Yes</option>
      <option value="No" <?php if($_GET['loan_enable']=='No') { echo "selected"; } ?>>No</option>s  
    </select>
     
    <input type="text" name="keyword" placeholder="AssetID,Make,Model" value="<?= @$_GET['keyword'] ?>" class="custom-filter">
    <button type="submit" class="btn btn-success">Filter</button>
</form>




<?php
echo do_shortcode($wpTable_Shortcode);

$equip_Arr    = lm_get_all_equipment_types();
$options      = "<option value=''></option>";
$optionsli    = '<li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text"></span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>';

$index = 1;
foreach ($equip_Arr as $Ekey => $equip) {
 
  $options    .= "<option value='".$equip->id."'>".$equip->equipment_type."</option>";
  $optionsli  .= '<li data-original-index="'.$index.'" class="lm-multi-select"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">'.$equip->equipment_type.'</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>';
  $index++;

}
 
?>

<script type="text/javascript">
  jQuery(document).ready(function(){
   
    jQuery("body").on('click', '.DTTT_button_edit',function(){  
        
        jQuery("#table_1_support_eq_types").html("<?= $options;?>");
        jQuery("#table_1_support_eq_types").prev().children('ul').html('<?= $optionsli; ?>');
        jQuery('.lm-multi-select').click(function(){  
              jQuery(this).toggleClass("selected");
        });

    });
    jQuery("body").on('click', '.DTTT_button_new',function(){  
        
        jQuery("#table_1_support_eq_types").html("<?= $options;?>");
        jQuery("#table_1_support_eq_types").prev().children('ul').html('<?= $optionsli; ?>');
        jQuery('.lm-multi-select').click(function(){  
            jQuery(this).toggleClass("selected");
        });

    });

  });
</script> 
<style type="text/css">
  .DTTT_button_new{ display: none; }
  .DTTT_button_delete{ display: none; }
</style>