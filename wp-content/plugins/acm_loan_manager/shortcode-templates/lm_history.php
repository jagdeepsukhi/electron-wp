<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
if(!lm_can_view_loan_settings()){
  wp_die("You do not have permission to view the loan settings.");
}
?>

<?php
$tableID = get_option("_lm_table_history");
echo do_shortcode("[wpdatatable id=".$tableID."]"); 
?>
 