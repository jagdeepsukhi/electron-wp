<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
global $wpdb;

$assetData  = lm_get_equipment_by_asset_id($asset_id);
$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);

$sql =  "Select DISTINCT organization FROM en_lm_loan_requests WHERE organization !='' ORDER BY organization ASC";
$organizations = $wpdb->get_results($sql);

?>
<div class="loan-report-page row">
  <form method="get" id="loan-report-select-form">
    <div class="row main-row">
      <div class="col-md-4">
        <div class="row row-label">
          <label>Select Report</label>
        </div>
          <select class="lm-filters" id="report_type" name="report_type">
            <option value="monthly" <?php if(!empty($_GET['report_type'] && $_GET['report_type']=='monthly')){echo "selected" ;}?> >Month End Asset Loan Report</option>
            <option value="client" <?php if(!empty($_GET['report_type'] && $_GET['report_type']=='client')){echo "selected" ;}?>>Client Loan Report</option>
          </select>
      </div>
    </div>
  </form>
</div>

<section id="monthly-section">
    <div class="loan-report-page row">
      <div class="form-heading col-md-12">
        <?php if(empty($_GET['report_type']) || $_GET['report_type']=='monthly'){ ?>
          <h3 style="text-align: left">Month End Asset Loan Report</h3>
        <?php } else { ?>
          <h3 style="text-align: left">Client Loan Report</h3>
        <?php } ?>  
      </div>
      <div class="row main-row">
        <div class="col-md-6">
          <form method="get" id="loan-report-period-form">
            <div class="row row-label">
              <label>Time Period</label>
            </div>
              <select id="time_period" name="time_period">
                <option value="">Select time period</option>
                <option value="current_month" <?php if(!empty($_GET['time_period'] && $_GET['time_period']=='current_month')){echo "selected" ;}?> >Current Month</option>
                <option value="last_month" <?php if(!empty($_GET['time_period'] && $_GET['time_period']=='last_month')){echo "selected" ;}?> >Last Month</option>
                <option value="custom" <?php if(!empty($_GET['time_period'] && $_GET['time_period']=='custom')){echo "selected" ;}?> >Custom Range</option>
              </select>
          </div> 
        <div class="col-md-6">
          <div class="row row-label">
            <label>Organization</label>
          </div>
            <select id="company_name" name="company_name">
              <option value="">All</option>
              <option value="ONR" <?php if(!empty($_GET['company_name'] && $_GET['company_name']== 'ONR' )){echo "selected" ;}?>>Organization Not Reported</option>
              <?php foreach ($organizations as $key => $organization) { ?>

                <option value="<?= $organization->organization ?>" <?php if(!empty($_GET['company_name'] && $_GET['company_name']== $organization->organization )){echo "selected" ;}?>><?= $organization->organization ?></option>
                
             <?php } ?>     
            </select>
        </div>
      </div>
      <div class="row main-row">
        <div class="col-md-3 date_range">
          <label>Start Date</label>
            <input type="text" name="startDate" class="wdt-datepicker" value="<?= $_GET['startDate'] ?>">
        </div>
        <div class="col-md-3 date_range">
          <label>End Date</label>
            <input type="text" name="endDate" class="wdt-datepicker" value="<?= $_GET['endDate'] ?>">
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-info filter-btn"> Filter</button> 
        </div>
      </div>
    </div>
    <input type="hidden" name="report_type" value="<?= $_GET['report_type']; ?>">
  </form>  
  <hr>  
      
<?php 

if(!empty($_GET['company_name'])){
    $company_name = $_GET['company_name'];
    if($company_name=="ONR"){
       $var1 = 'AND  r.organization = "" ';
    }else{
      $var1 = 'AND  r.organization = '.'"'.$company_name.'"'.'';
    }
    
}else{
   $var1 = ' ';
}


if(!empty($_GET['time_period'])){
    if($_GET['time_period']=="current_month"){

      $startDate = date('Y-m-01'); 
      $endDate   = date('Y-m-t'); 

    }else if($_GET['time_period']=="last_month"){

      $startDate = date('Y-m-01',strtotime('last month'));
      $endDate   = date('Y-m-t',strtotime('last month'));

    }else if($_GET['time_period']=="custom"){

      $startDate  = date( 'Y-m-d',strtotime($_GET['startDate']) );
      $endDate    = date( 'Y-m-d',strtotime($_GET['endDate']) );
      echo '<style type="text/css">.date_range{ display: block; }</style>';

    }
    $var2 = 'AND ( (r.date_of_checkout BETWEEN "'.$startDate.'" AND "'.$endDate.'") OR (r.actual_return_date BETWEEN "'.$startDate.'" AND "'.$endDate.'"))';
}else{
    $var2 = ' AND 1=1';
}
                                                                                

  ?>     

    <?php
    if(empty($_GET['report_type']) || $_GET['report_type']=='monthly'){
      $tableID = get_option("_lm_table_reports");
      echo do_shortcode("[wpdatatable id=".$tableID."  VAR1='".$var1."' VAR2='".$var2."' ]");
    }else{
      echo $tableID = get_option("_lm_table_reports_for_client");
       echo do_shortcode("[wpdatatable id=".$tableID."  VAR1='".$var1."' VAR2='".$var2."' ]");
    }
    ?>
    <!-- checkout Modal -->
    <div id="checkout-confirm" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
    1      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Return Type Popup</h4>
          </div>
          <div class="modal-body">
           <button type="button" class="btn btn-info btn-lg form-btn" data-form-type="quick-form" data-asset-id="">One Click/Quick Check-in</button>
           <button type="button" class="btn btn-info btn-lg form-btn" data-form-type="full-return-form" data-asset-id="">Full Form</button>
          </div>

        </div>
      </div>
    </div>

    <!-- Loan Request Modal -->
    <div id="RequestModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Request Type Popup</h4>
          </div>
          <div class="modal-body">
           <button type="button" class="btn btn-primary form-btn" data-form-type="one-click-quick-checkout" data-asset-id="">One Click</button>
           <button type="button" class="btn btn-info form-btn" data-form-type="fast-quick-checkout-form" data-asset-id="">Fast Form</button>
           <button type="button" class="btn btn-success form-btn" data-form-type="full-request-form" data-asset-id="">Full Form</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </section>
<style type="text/css">
  .loan-report-page .main-row {margin: 0px 0px 10px 0;}
  .loan-report-page .row-label {margin: 0px;}
  .loan-report-page label {font-weight: bold;}
  .loan-report-page select,.loan-report-page input{width: 100%;}
  .loan-report-page .form-heading {
    text-align: center;
    margin: 20px 0px 20px 0;
  }
</style>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#report_type').on('change',function(){

      jQuery('#loan-report-select-form').submit();
    });
    jQuery('#time_period').on('change',function(){
        var time_period = jQuery(this).val();
        if(time_period=='custom'){
            jQuery(".date_range").show();
        }else{
            jQuery(".date_range").hide(); 
        }

    });
    /* jQuery('#company_name').on('change',function(){
      jQuery('#loan-report-period-form').submit();
    });*/
  });
</script> 