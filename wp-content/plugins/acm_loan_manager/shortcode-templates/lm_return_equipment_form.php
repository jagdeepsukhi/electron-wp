<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/

$asset_id   = $_GET['asset_id'];
if(empty($asset_id)){
  wp_die("No direct access allowed!");
}
$assetData  = lm_get_equipment_by_asset_id($asset_id);
$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);

$formID           = get_option("_lm_form_full_checkin_return");
$locationsData    = lm_get_locations_for_eq_type($assetData->equipment_type);
$locationsOptions =  '<option value="" selected="selected">Select value</option>';
foreach ($locationsData as $key => $loc) {
   $locationsOptions .='<option value="'.$loc->id.'">'.$loc->location.'</option>'; 
}

?>
 <a href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''; ?>" class="btn btn-info lm-back"> << Back</a>
<?= do_shortcode("[Form id='".$formID."']"); ?>
  
<!-- Loan Request Modal -->
<div id="approveRequestModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Message to requestor</h4>
      </div>
      <div class="modal-body">
        <label>Message to requestor:</label>
        <div><textarea class="msg_to_requestor" name="msg_to_requestor"></textarea></div>
        <br>
        <button type="button" class="btn btn-info form-btn cancel-continue" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-info form-btn approve-continue" >Continue</button>
      </div>
    </div>
  </div>
</div>
<!-- Loan Request Modal -->
<div id="denyRequestModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Reason for Denied</h4>
      </div>
      <div class="modal-body">
        <label>Reason the Request was denied:</label>
        <div><textarea class="reason_request_deny" name="reason_request_deny"></textarea></div>
        <br>
        <button type="button" class="btn btn-info cancel-continue" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-info deny-continue">Continue</button>
      </div>
    </div>
  </div>
</div>
<!-- Ajax Loder -->
<?php do_action("lm_loader"); ?> 

<script type="text/javascript">
    jQuery(document).ready(function(){
      set_default_values();   

  });
 

  // Hide odometer if criteria is Date name=""
  jQuery("[data-name=lm_criteria]").parent().parent().hide();
  var maintenance_criteria = "<?= $assetData->maintenance_criteria ?>";
  if(maintenance_criteria=='Date'){
    jQuery("[data-name=lm_criteria]").val('Date');
  }


  function set_default_values(){

    jQuery("#form<?= $formID ?> :input").each(function(){
          var input = jQuery(this); 
          if ( input.hasAttr("data-name") ) { 
            var name = input.attr("data-name");
            input.attr("name",name);
          }
      });

    //Set request id 
    jQuery("[data-name=lm_request_id]").val("<?= $loanData['loanRequest']->id ?>");
    jQuery("[data-name=lm_checkin_location]").html('<?= $locationsOptions ?>'); // Location dropdown

    var d = new Date();
   // var todayDate = (d.getMonth()+1) + "-" + d.getDate() + "-" +  d.getFullYear();  
    var todayDate = d.getFullYear() + "-" +(d.getMonth()+1) + "-" +  d.getDate() ;  
    jQuery("[data-name=lm_asset_id]").val("<?= $assetData->asset_id ?>");
    jQuery("[data-name=lm_year]").val("<?= $assetData->year ?>");
    jQuery("[data-name=lm_make]").val("<?= $assetData->make ?>");
    jQuery("[data-name=lm_model]").val("<?= $assetData->model ?>");

    setTimeout(function(){  

      jQuery("[data-name=lm_date_of_checkout]").val("<?= $loanData['loanRequest']->date_of_checkout ?>"); // checkout Date

    }, 1000);

    jQuery("[data-name=lm_odometer_reading_return]").val("<?= $loanData['loanRequest']->odometer_reading ?>");
    jQuery("[data-name=lm_checkin_location]").val("<?= $loanData['loanRequest']->usage_location ?>"); // Usage Location
    jQuery("[data-name=lm_date_of_return]").attr("readonly",'readonly');
    //jQuery("[data-name=lm_experience_issue]").click();
    //jQuery("[data-name=lm_additional_damage]").click();
    //jQuery("[data-name=issue_to_manager]").prop('checked', true);


    //SET LOAN REQUEST META DATA
    var loanRequestMeta = '<?php echo json_encode($loanData["loanRequestMeta"] ); ?>';
    var rows = "";
    if(!jQuery.isEmptyObject(JSON.parse(loanRequestMeta))){
        jQuery.each(JSON.parse(loanRequestMeta), function( index, value ) {
          
          if(index==0){
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" class="damage_item" type="hidden" value="'+value.id+'"></td><td><a class="add-more-damage">+</a></td></tr>';
          }else{
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" class="damage_item" type="hidden" value="'+value.id+'"></td><td><a class="remove-damage">x</a></td></tr>';
          }

        });
        jQuery("#damage-table tbody").html(rows);
    }
     setTimeout(function(){  
          jQuery(".repeate-this textarea").removeAttr("disabled");
       }, 1000);

  }
  </script>
  <style type="text/css">
    .repeate-this input { width:100%;  border: 1px solid #dfdfdf;  }
    .repeate-this .remove-damage {color: red;}
    .repeate-this .add-more-damage {color: green; font-weight: bold; font-size: 15px;}
  </style>
 

 