<?php 
/*
* Form Template for loan settings page
*/

if(!lm_can_view_loan_settings()){
	wp_die("You do not have permission to view the loan settings.");
}
?>
<div class="lm-settings">
<?php if(lm_can_edit_loan_settings()) { ?>
<form id="form-loan-settings" method="post">
<?php } ?>
 <input type="hidden" name="action" value="save_loan_settings">

 	 
 		<?php if(lm_can_edit_loan_settings()) { ?>
	 	<input type="submit" class="btn btn-success" name="Save" value="Save Settings">
		 <?php } ?>
	 	<a class="asset-setting-btn btn" href="<?= get_permalink(get_option('_lm_page_individual_settings')) ?>" class="btn btn-info"> Individual Asset Settings</a>
	 	<a href="<?= get_permalink(get_option('_lm_page_loan_status')) ?>" class="btn btn-info">Loan Status Options</a>
	 	<a href="<?= get_permalink(get_option('_lm_page_loan_location')) ?>" class="btn btn-info">Loan Locations Options</a>
	 	<a href="<?= get_permalink(get_option('_lm_page_loan_rational')) ?>" class="btn btn-info">Loan Rationale Options</a>
	 

 
 	<br>
	<div class="loan-settings">    
	   <h4>Asset Loan Police </h4> 
	   <textarea name="loan_policy_text" rows="1" placeholder="Loan Policy Text is entered here"><?= get_option('lm_loan_policy_text') ?></textarea>   
	    <hr>
	</div>

	
	<?php 
		global $wpdb;
		$equipmenttypes = $wpdb->get_results("SELECT id,equipment_type FROM en_mb_equipment_types");
		foreach ($equipmenttypes as $key => $equipmenttype) { 
			$i = $equipmenttype->id;
			$savedSettings = lm_get_loan_equipment_settings($equipmenttype->id);
	 
	?>


	<input type="hidden" name="equipment_type[]" value="<?= $i ?>">
  	<div class="auto-mobile">
	<h4><?= $equipmenttype->equipment_type ?></h4>
	 
	  <div class="row">
	
		  <div class="request-approve col-md-2 col-xs-12">

			  <label>Select all request approvers</label>
			
			  <select class="custom-select" name="request_approvers[<?= $i ?>][]" id="request_approvers" multiple> 
			  	<?php $approversArr = explode(",", $savedSettings->request_approvers); ?>
			  	<?php foreach (get_users() as $key => $user ) {

			  	$allCpabilities 	= lm_get_all_capabilities($user->ID);
			  	$equipment_type_key = str_replace(' ', '-', strtolower($equipmenttype->equipment_type) );
			  	$required_capability= "enet_".$equipment_type_key."_ALM_approve-requests";

				/* *******************Get Capebilitiies for this Page***************** */
				 

				if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
					 

			  	 ?>
			  		<option value="<?= $user->ID ?>" <?php if (in_array($user->ID, $approversArr)) { echo "selected"; } ?>><?= $user->display_name ?></option>
			  	<?php	} } ?> 
			      
			  </select> 		 
	      </div>
		
		  <div class="loan-period col-md-1 col-xs-12">
		    <div class="form-group">
			   <label for="maximum-loan">Maximum Loan period </label><p>(days)</p>
			   <input type="number" class="form-control" name="max_loan_period[<?= $i ?>]" value="<?= @$savedSettings->max_loan_period ?>" min="0">
			   
			</div>  
	     </div>   
	 
	 
	 
		  <div class="loan-period col-md-1 col-xs-12">
		     <div class="form-group">
				  <label for="maximum-loan">Advance return reminder </label><p>(days)</p>
				  <input type="number" class="form-control"  name="advance_return_reminder[<?= $i ?>]" value="<?= @$savedSettings->advance_return_reminder ?>" min="0">
				  
			</div> 
	     </div>   
	   
	   
		 <div class="loan-period reminder col-md-3 col-xs-12">
		   <label>Return Reminder Email Frequency </label> <p>(days) (Blank = No Reminder)</p>
		   <input type="number" class="form-control"  name="return_reminder_email_frequency[<?= $i ?>]"  value="<?= @$savedSettings->return_reminder_email_frequency ?>" min="0">
	 
		   <label>Enable reminder emails 
 
		   <input type="checkbox" class="ls-checkbox" name="enable_reminder_email[<?= $i ?>]" value='1' <?php if ($savedSettings->enable_reminder_email=='1' ){ echo "checked" ; } ?> > </label>
	   </div>   
	   
	   
		<div class="loan-period col-md-1 col-xs-12">
		     <div class="form-group">
				  <label for="maximum-loan">Return in advance of next scheduled event</label> <p>(days)</p>
				   <input type="number" class="form-control"  name="return_ad_next_sch_event_days[<?= $i ?>]"  value="<?= @$savedSettings->return_ad_next_sch_event_days ?>" min="0">
				 
			</div> 	  
	   </div>   
	   
	   
	<div class="loan-period return col-md-1 col-xs-12">
		   <div class="form-group">
				  <label for="maximum-loan">Return in advance of next scheduled event</label> <p> (Miles /Kilometers /Hours used</p>
				   <input type="number" class="form-control" name="return_ad_next_sch_event_miles[<?= $i ?>]" value="<?= @$savedSettings->return_ad_next_sch_event_miles ?>" min="0">
			</div> 
	  </div>   
	   
	   
	  <div class="loan-period return col-md-1 col-xs-12">
		   <div class="form-group">
				  <label for="maximum-loan">Expedited checkout duration</label> <p>(days)</p>
				  <input type="number" class="form-control" name="expedited_checkout_duration[<?= $i ?>]" value="<?= @$savedSettings->expedited_checkout_duration ?>" min="0">
				 
			</div> 
	  </div>   
	      
	  <div class="loan-period return col-md-1 col-xs-12">
		 <label>Enable Asset Loan</label>
			
			  <select class="custom-select" name="enable_asset_loan[<?= $i ?>]">    
				<option  value="Yes" <?php if($savedSettings->enable_asset_loan=='Yes') { echo "selected"; } ?>>Yes</option>
				<option  value="No" <?php if($savedSettings->enable_asset_loan=='No') { echo "selected"; } ?>>No</option> 
			  </select> 		 
	   </div>

	   <div class="loan-period return col-md-1 col-xs-12">
		 <label>Authorization Required for Checkout </label>
			  <select class="custom-select" name="auth_required_for_checkout[<?= $i ?>]">    

				 <option value="Yes" <?php if($savedSettings->Auth_required_for_checkout=='Yes') { echo "selected"; } ?>>yes</option>
				 <option value="No" <?php if($savedSettings->Auth_required_for_checkout=='No') { echo "selected"; } ?>>no </option>
				 
			  </select> 		 
	   </div>
     </div>	
	<hr>
  </div>



<?php } ?>
<?php if(lm_can_edit_loan_settings()) { ?>
<input type="submit" class="btn btn-success" name="Save" value="Save Settings">
<?php } ?>


<?php if(lm_can_edit_loan_settings()) { ?>	
</form>	
</div> <!-- end lm-settings-->
<?php } ?>
<!-- Loan Request Modal -->
 <div class="lm-loader">
  <div class="modal-dialog">
     <img src="<?= get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/lm-loader.gif' ?>">
  </div>
</div> 

<script type="text/javascript">
jQuery(document).ready(function(){
		jQuery('option').mousedown(function(e) {
	    e.preventDefault();
	    var originalScrollTop = jQuery(this).parent().scrollTop();
	    console.log(originalScrollTop);
	    jQuery(this).prop('selected', jQuery(this).prop('selected') ? false : true);
	    var self = this;
	    jQuery(this).parent().focus();
	    setTimeout(function() {
	        jQuery(self).parent().scrollTop(originalScrollTop);
	    }, 0);
	    
	    return false;
	});
});
	
	
</script>
<style type="text/css">
select {
  width: 400px;
}
select option {
  font-size: 14px;
  padding: 8px 8px 8px 28px;
  position: relative;
}
select option:before {
  content: "";
  position: absolute;
  height: 15px;
  width: 15px;
  top: 0;
  bottom: 0;
  margin: auto;
  left: 8px;
  border: 1px solid #ccc;
  border-radius: 2px;
  z-index: 1;
}
select option:checked:after {
  content: attr(title);
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  padding: 8px 8px 8px 28px;
  border: none;
}
select option:checked:before {
  border-color: blue;
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxMC42MSA4LjQ4Ij48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6IzNlODhmYTt9PC9zdHlsZT48L2RlZnM+PHRpdGxlPkFzc2V0IDg8L3RpdGxlPjxnIGlkPSJMYXllcl8yIiBkYXRhLW5hbWU9IkxheWVyIDIiPjxnIGlkPSJfMSIgZGF0YS1uYW1lPSIxIj48cmVjdCBjbGFzcz0iY2xzLTEiIHg9Ii0wLjAzIiB5PSI1LjAxIiB3aWR0aD0iNSIgaGVpZ2h0PSIyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0Ljk3IDAuMDEpIHJvdGF0ZSg0NSkiLz48cmVjdCBjbGFzcz0iY2xzLTEiIHg9IjUuMzYiIHk9Ii0wLjc2IiB3aWR0aD0iMiIgaGVpZ2h0PSIxMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNC44NiAtMy4yNikgcm90YXRlKDQ1KSIvPjwvZz48L2c+PC9zdmc+);
  background-size: 10px;
  background-repeat: no-repeat;
  background-position: center;
}
.asset-setting-btn.btn {
    background-color: #FF6347;
    color: #fff;
}
</style>