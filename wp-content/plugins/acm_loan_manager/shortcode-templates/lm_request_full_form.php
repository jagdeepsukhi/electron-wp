<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
  global $wpdb;
  $asset_id   = $_GET['asset_id'];
?>


<a href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''; ?>" class="btn btn-info lm-back"> << Back</a>
<?php
  // If Item is not In Service on Maintenace Manager application
  $maintenance_status = lm_check_maintenance_status($asset_id);  
  if($maintenance_status!=1){
    echo  '<div class="alert alert-danger">This asset cannot be checked out at this time because it is not in Service. Please reach out to the manager of this equipment to learn when it will be back in service.</div>';
    return;
  }

?>

<?php
  $assetData  = lm_get_equipment_by_asset_id($asset_id);
  $formID     = get_option("_lm_form_full_checkout_request");
 
  $loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
  
  $locationsData      = lm_get_locations_for_eq_type($assetData->equipment_type);
  $locationsOptions   =  '<option value="" selected="selected">Select value</option>';
  foreach ($locationsData as $key => $loc) {
     $locationsOptions .='<option value="'.$loc->id.'">'.$loc->location.'</option>'; 
  }

  $reasonsData    = lm_get_reasons_for_eq_type($assetData->equipment_type);
  $reasonsOptions =  '<option value="" selected="selected">Select value</option>';
  foreach ($reasonsData as $key => $reasons) {
     $reasonsOptions .='<option value="'.$reasons->id.'">'.$reasons->reason.'</option>'; 
  } 
?>


<!-- If Date based next sechduled maintenance due -->
<?php
  $next_event   = lm_nearest_effective_event_date($asset_id);
  if( strtotime($next_event ) > 0 ){
  echo '<div class="alert alert-danger"><strong>Warning!</strong> This piece of equipment must returned before '.$next_event.'.</div>';
  }
?>

<!-- If Odometer based item coming to maintenance due -->
<?php 
  $MD_reading         = lm_nearest_effective_event_reading($asset_id);
  if(!empty($MD_reading) && $MD_reading > 0){
    echo '<div class="alert alert-danger"><strong>Warning!</strong> This piece of equipment must returned before its reaches '.$MD_reading.' '.$assetData->maintenance_criteria.'</div>';
  }
?>



<?php 
// Set notice for max loan period

$max_loan_period = lm_max_loan_period($asset_id); 
if($max_loan_period>0){
  echo '<div class="alert alert-info"><strong>Note!</strong> This equipment can be loaned for up to '.$max_loan_period.'  days, unless an earlier date is indicated above.</div>';
}
?>





<?php 
$visitorData    = lm_get_visitors_data(); 
$clientDropdown = "<option>--</option>";
foreach ($visitorData as $key => $visitor) {
    if (!empty( $visitor['name'] ))  { 
        $clientDropdown .= '<option value="'.$visitor['name'].'" data-company="'.$visitor['company'].'" data-project="'.$visitor['project'].'" data-parent="'.$visitor['parent'].'">'.$visitor['name'].'</option>';
    } 
 } 

 
?>

<?= do_shortcode("[Form id='".$formID."']"); ?>
<script type="text/javascript">
  jQuery(document).ready(function(){
    set_default_values();
  });

  // How may day item can be loanded
  var max_date = new Date();
  max_date.setDate(max_date.getDate()+<?= $max_loan_period ?>);


  // next effective event
/*  var next_event = new Date('<?= $next_event ?>');

  jQuery("#wdform_7_element<?= $formID ?>").change(function(){
      var returnDate = jQuery(this).val();
      returnDate = new Date(returnDate);

      if(new Date(returnDate) > new Date(next_event))
      { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $next_event ?></p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      } else if( new Date(returnDate) > new Date(max_date) ) { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be loaned for <?= $max_loan_period ?> days</p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      }else{
          jQuery(".lmwarn").remove();
      }

  })*/

  // next effective event
  var next_event = new Date('<?= $next_event ?>');

  jQuery("[data-name=lm_date_of_return]").change(function(){
      var returnDate = jQuery(this).val();
      returnDate = new Date(returnDate);

      if(new Date(returnDate) > new Date(next_event))
      { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $next_event ?></p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      } else if( new Date(returnDate) > new Date(max_date) ) { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be loaned for <?= $max_loan_period ?> days</p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      }else{
          jQuery(".lmwarn").remove();
      }

  })

  // Warnning on odometer reading if exceeded from effective 
/*  jQuery('#wdform_8_element<?= $formID ?>').on('blur',function(){
        var odometer = parseInt(jQuery(this).val());
        var MD_reading = parseInt("<?= $MD_reading ?>");
        if(MD_reading > 0 && odometer > MD_reading){
             var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $MD_reading.' '.$assetData->maintenance_criteria ?></p></div>";
            jQuery(".lmwarn").remove();
            jQuery(this).parent().parent().append(warning);
            jQuery(this).val("");
        }else{
            jQuery(".lmwarn").remove();
        }
  });*/

  jQuery("[data-name=lm_odometer_reading]").on('blur',function(){
          var odometer = parseInt(jQuery(this).val());
          var MD_reading = parseInt("<?= $MD_reading ?>");
          if(MD_reading > 0 && odometer > MD_reading){
               var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $MD_reading.' '.$assetData->maintenance_criteria ?></p></div>";
              jQuery(".lmwarn").remove();
              jQuery(this).parent().parent().append(warning);
              jQuery(this).val("");
          }else{
              jQuery(".lmwarn").remove();
          }
    });




 /* jQuery('#wdform_21_element<?= $formID ?>0').on('click',function(){
    if(!jQuery(this).prop('checked') ){
       jQuery("#wdform_12_element<?= $formID ?>").val('');
       jQuery("#wdform_11_element<?= $formID ?>").val('');
       jQuery("#wdform_13_element<?= $formID ?>").val('');
    }

  });*/
  jQuery("[data-name=lm_checking_for_client]").on('click',function(){
    if(!jQuery(this).prop('checked') ){
       jQuery("[data-name=lm_client_name]").val('');
       jQuery("[data-name=lm_client_organization]").val('');
       jQuery("[data-name=lm_project]").val('');
    }

  });
 

  function set_default_values(){


      jQuery(".lm_client_name select").attr("name","lm_client_name");
      jQuery(".lm_client_name select").html('<?= $clientDropdown ?>');

      jQuery("#form<?= $formID ?> :input").each(function(){
          var input = jQuery(this); 
          if ( input.hasAttr("data-name") ) { 
            var name = input.attr("data-name");
            input.attr("name",name);
          }
      });



      jQuery("[data-name=lm_asset_id]").val("<?= $assetData->asset_id ?>");
      jQuery("[data-name=lm_year]").val("<?= $assetData->year ?>");
      jQuery("[data-name=lm_make]").val("<?= $assetData->make ?>");
      jQuery("[data-name=lm_model]").val("<?= $assetData->model ?>");
      jQuery("[data-name=lm_date_of_checkout]").val('<?= date("Y-m-d"); ?>');
      jQuery("[data-name=lm_policy]").parent().parent().prepend("<div class='policy-txt'><?= get_option('lm_loan_policy_text') ?></div>"); 
      jQuery("[data-name=lm_usage_location]").html('<?= $locationsOptions ?>'); // Location dropdown 
      jQuery("[data-name=lm_reason]").html('<?= $reasonsOptions ?>'); // Location dropdown 

      setTimeout(function(){  
          jQuery(".repeate-this textarea").removeAttr("disabled");
      }, 1000);


      //SET LOAN REQUEST META DATA
      var loanRequestMeta = '<?php echo json_encode($loanData["loanRequestMeta"] ); ?>';
      var rows = "";
      if(!jQuery.isEmptyObject(JSON.parse(loanRequestMeta))){
        jQuery.each(JSON.parse(loanRequestMeta), function( index, value ) {
          
          if(index==0){
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" type="hidden" value="'+value.id+'"></td><td><a class="add-more-damage">+</a></td></tr>';
          }else{
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" type="hidden" value="'+value.id+'"></td><td><a class="remove-damage">x</a></td></tr>';
          }

        });
        jQuery("#damage-table tbody").html(rows);
      }

 
      // Hide odometer if criteria is Date
      jQuery("[data-name=lm_criteria]").parent().parent().hide();
      var maintenance_criteria = "<?= $assetData->maintenance_criteria ?>";
      if(maintenance_criteria=='Date'){
        jQuery("[data-name=lm_criteria]").val('Date');
      }


      //jQuery("#wdform_3_element<?= $formID ?>").val("<?= $assetData->year ?>");
      //jQuery("#wdform_4_element<?= $formID ?>").val("<?= $assetData->make ?>");
     // jQuery("#wdform_5_element<?= $formID ?>").val("<?= $assetData->model ?>");
      //jQuery("#wdform_28_element<?= $formID ?>").val('<?= date("Y-m-d"); ?>'); // checkout Date
      //jQuery("#wdform_14_element<?= $formID ?>0").parent().parent().prepend("<div class='policy-txt'><?= get_option('lm_loan_policy_text') ?></div>"); 
    
      //jQuery("#wdform_9_element<?= $formID ?>").html('<?= $locationsOptions ?>'); // Location dropdown 
      //jQuery("#wdform_20_element<?= $formID ?>").html('<?= $reasonsOptions ?>'); // Location dropdown 

      
      
    
  }
</script>
<style type="text/css">
    .repeate-this input { width:100%;  border: 1px solid #dfdfdf;  }
    .repeate-this .remove-damage {color: red;}
    .repeate-this .add-more-damage {color: green; font-weight: bold; font-size: 15px;}
</style>
 
<!-- Ajax Loder -->
<?php do_action("lm_loader"); ?>
