<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/

global $wpdb;
?>

<a href="<?= get_permalink(get_option('_lm_page_loan_settings')) ?>" class="btn btn-info"> << Back</a>
<?php $tableID = get_option("_lm_table_reason_master"); ?>
<?php
echo do_shortcode("[wpdatatable id=".$tableID."]");
$equip_Arr    = lm_get_all_equipment_types();

$sql2       = "SELECT * FROM `en_lm_loan_reasons` "; 
$allReasons = $wpdb->get_results($sql2);


if(!lm_can_view_loan_settings()){
  wp_die("You do not have permission to view the loan settings.");
}

global $allow_edit;
if(lm_can_edit_loan_settings()) {
  $allow_edit =1;
}else{
  $allow_edit =0;
}

/* *******************Set Edit capability***************** */
function wpdatatables_allow_edit_table_callback( $edit ) {
    global $allow_edit;
    return $allow_edit;  
}

add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );


?>
<h2>Supported Equipment Types</h2>

<form id="reason-relation" method="post">
  <table class="responsive">
    <thead>
      <tr><th>Reason</th> 
      <?php foreach ($equip_Arr as $key => $type) { ?>
            <th><?= $type->equipment_type ?> </th>
      <?php } ?>
      <tr>
    </thead>
    <tbody>
      <?php foreach ($allReasons as $key => $reason) { 
        $savedEqTypes = explode(',', $reason->support_eq_types)
      ?>
          <tr>
          <td><?= $reason->reason?></td>
          <?php foreach ($equip_Arr as $key => $type) { ?>
            <td><input type="checkbox" name="support_types[<?= $reason->id ?>][]" value="<?= $type->id ?>" <?php if(in_array($type->id,  $savedEqTypes)) { echo "checked"; } ?> ></td>
          <?php } ?>
           
        </tr>
      <?php } ?>
    </tbody>
  </table>
  <input type="hidden" name="action" value="save_reason_relation">
  <?php if(lm_can_edit_loan_settings()) {?>
  <button type="submit" class="btn btn-success">Save</button>
<?php } ?>
  <a href="<?= get_permalink(get_option('_lm_page_loan_rational')) ?>" title="Load newly added reason" class="refresh-a"><img class="refresh-icon" src="<?= get_bloginfo('url').'/wp-content/plugins/acm_loan_manager/icons/refresh.png' ?>"></a>
 </form> 
<!-- Loder -->
<?php do_action("lm_loader"); ?>
 
<script type="text/javascript">
  jQuery(document).ready(function(){
   
    jQuery("body").on('click', '.DTTT_button_edit',function(){  

        jQuery("#table_1_created_by").val("<?php echo get_current_user_id(); ?>");
        jQuery("#table_1_created_by").parent().parent().parent().parent().hide();
    });

    jQuery("body").on('click', '.DTTT_button_new',function(){  
        
        jQuery("#table_1_created_by").val("<?php echo get_current_user_id(); ?>");
        jQuery("#table_1_created_by").parent().parent().parent().parent().hide();

    });
});
</script> 