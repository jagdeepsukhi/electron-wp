<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
  global $wpdb;
  $asset_id   = $_GET['asset_id'];

  if(!lm_can_edit_pending_request($asset_id)){
    wp_die("This piece of equipment is pending checkout approval for another user.");
  }



  $assetData  = lm_get_equipment_by_asset_id($asset_id);
  $loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
  $formID     = get_option("_lm_form_full_checkout_request");

  $locationsData    = lm_get_locations_for_eq_type($assetData->equipment_type);
  $locationsOptions =  '<option value="" selected="selected">Select value</option>';
  foreach ($locationsData as $key => $loc) {
      if($loc->id==$loanData['loanRequest']->usage_location){
          $locationsOptions .='<option value="'.$loc->id.'" selected>'.$loc->location.'</option>'; 
      }else{
           $locationsOptions .='<option value="'.$loc->id.'">'.$loc->location.'</option>'; 
      } 
  }

  
  $reasonsData    = lm_get_reasons_for_eq_type($assetData->equipment_type);
  $reasonsOptions =  '<option value="" selected="selected">Select value</option>';
  foreach ($reasonsData as $key => $reasons) {
      if($reasons->id==$loanData['loanRequest']->reason){
          $reasonsOptions .='<option value="'.$reasons->id.'" selected>'.$reasons->reason.'</option>'; 
      }else{
         $reasonsOptions .='<option value="'.$reasons->id.'">'.$reasons->reason.'</option>'; 
      } 
  }


  ?>
 <a href="<?= get_bloginfo('url') ?>/lm-asset-loan-front/" class="btn btn-info lm-back"> << Back</a></br>
 <!-- If Date based next sechduled maintenance due -->
<?php
  $next_event   = lm_nearest_effective_event_date($asset_id);
  if( strtotime($next_event ) > 0 ){
  echo '<div class="alert alert-danger"><strong>Warning!</strong> This piece of equipment must returned before '.$next_event.'.</div>';
  }
?>

<!-- If Odometer based item coming to maintenance due -->
<?php 
  $MD_reading         = lm_nearest_effective_event_reading($asset_id);
  if(!empty($MD_reading) && $MD_reading > 0){
    echo '<div class="alert alert-danger"><strong>Warning!</strong> This piece of equipment must returned before its reaches '.$MD_reading.' '.$assetData->maintenance_criteria.'</div>';
  }
?>



<?php 
// Set notice for max loan period
$max_loan_period = lm_max_loan_period($asset_id); 
if($max_loan_period>0){
  echo '<div class="alert alert-info"><strong>Note!</strong> This equipment can be loaned for up to '.$max_loan_period.'  days, unless an earlier date is indicated above.</div>';
}
?>
<?php 
$visitorData    = lm_get_visitors_data(); 
$clientDropdown = "<option>--</option>";
foreach ($visitorData as $key => $visitor) {
    if (!empty( $visitor['name'] ))  { 
        $clientDropdown .= '<option value="'.$visitor['name'].'" data-company="'.$visitor['company'].'" data-project="'.$visitor['project'].'" data-parent="'.$visitor['parent'].'">'.$visitor['name'].'</option>';
    } 
 } 

 
?>
<?= do_shortcode("[Form id='".$formID."']"); ?>

<!-- Add approve/deny button basis on maintenace status -->
<?php 
  $maintenance_status = lm_check_maintenance_status($asset_id);  
  if($maintenance_status!=1){ ?>
    <div class="approve-action-container">
      <button class="action-btn deny-btn">Deny</button>
      <button class="action-btn cancel-btn">Cancel</button>
      <p style='color:red'>This asset cannot be approve/checked out at this time because it is not in Service. Please reach out to the manager of this equipment to learn when it will be back in service.</p>
    </div>
 <?php  }else{ ?>

  <div class="approve-action-container">
    <?php if(lm_can_approve_request($asset_id)){ ?>
    <button class="action-btn approve-btn">Approve</button>
    <button class="action-btn deny-btn">Deny</button>
    <?php } ?>
    <button class="action-btn cancel-btn">Cancel</button>
  </div>

  <?php   }  ?>



<!-- Loan Request Modal -->
<div id="approveRequestModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Message to requestor Popup</h4>
      </div>
      <div class="modal-body">
        <label>Message to requestor:</label>
        <div><textarea class="msg_to_requestor" name="msg_to_requestor"></textarea></div>
        <br>
        <button type="button" class="btn btn-success approve-continue" >Continue</button>
        <button type="button" class="btn btn-danger cancel-continue" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>
<!-- Loan Request Modal -->
<div id="denyRequestModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Reason Denied Popup</h4>
      </div>
      <div class="modal-body">
        <label>Reason the Request was denied:</label>
        <div><textarea class="reason_request_deny" name="reason_request_deny"></textarea></div>
        <br>
        <button type="button" class="btn btn-success deny-continue">Continue</button>
        <button type="button" class="btn btn-danger cancel-continue" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Loan Request Modal -->
<div id="cancelRequestModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Are you sure to cancel this request?</h4>
      </div>
      <div class="modal-body">
        <button type="button" class="btn btn-success cancel-req-continue" >Yes</button>
        <button type="button" class="btn btn-danger cancel-continue" data-dismiss="modal">No</button>
        
      </div>
    </div>
  </div>
</div>
 
<!-- Ajax Loder -->
<?php do_action("lm_loader"); ?> 

  <script type="text/javascript">
    jQuery(document).ready(function(){
      set_default_values();
      jQuery(".approve-btn").show();
      jQuery(".deny-btn").show();

      // Approve modal trigger
      jQuery("body").on("click",".approve-btn",function(e){
        e.preventDefault();
        jQuery("#approveRequestModal").modal("show");
         
      });


      jQuery("body").on("click",".approve-continue",function(e){
        e.preventDefault();
        jQuery("[data-name=lm_request_status]").val(6);// set status Approved
        var msg = jQuery(".msg_to_requestor").val();
        jQuery("[data-name=lm_approve_deny_note]").val(msg);
        jQuery(".fm-form").submit();
      });

      // Deny modal trigger
      jQuery("body").on("click",".deny-btn",function(e){
        e.preventDefault();
        jQuery("#denyRequestModal").modal("show");

      });
      jQuery("body").on("click",".deny-continue",function(e){
        e.preventDefault();
        jQuery("[data-name=lm_request_status]").val(4); // set status Deny
        var msg = jQuery(".reason_request_deny").val();
        jQuery("[data-name=lm_approve_deny_note]").val(msg);
        jQuery(".fm-form").submit();
      });


      // Cancel request modal trigger
      jQuery("body").on("click",".cancel-btn",function(e){
        e.preventDefault();
        jQuery("#cancelRequestModal").modal("show");

      });
      jQuery("body").on("click",".cancel-req-continue",function(e){
        e.preventDefault();
        jQuery("[data-name=lm_request_status]").val(5); // set status Canceled
        jQuery(".fm-form").submit();
      });

      // Cancel btn on popup trigger continue
      jQuery("body").on("click",".cancel-continue",function(e){
        e.preventDefault();
        jQuery("[data-name=lm_request_status]").val(1); // set pending as deafult status
        jQuery("[data-name=lm_approve_deny_note]").val("");
      });

       // Hide odometer if criteria is Date
      jQuery("[data-name=lm_criteria]").parent().parent().hide();
      var maintenance_criteria = "<?= $assetData->maintenance_criteria ?>";
      if(maintenance_criteria=='Date'){
        jQuery("[data-name=lm_criteria]").val('Date');
      }

  });

  function set_default_values(){

    // Replace name attr with data-name of each input
    jQuery("#form<?= $formID ?> :input").each(function(){
        var input = jQuery(this); 
        if ( input.hasAttr("data-name") ) { 
          var name = input.attr("data-name");
          input.attr("name",name);
        }
    });

    // client drop down create
    jQuery(".lm_client_name select").attr("name","lm_client_name");
    jQuery(".lm_client_name select").html('<?= $clientDropdown ?>');


    //Set request id 
    jQuery("[name=request_id]").val("<?= $loanData['loanRequest']->id ?>");

    var d = new Date();
    var todayDate = (d.getMonth()+1) + "/" + d.getDate() + "/" +  d.getFullYear();  
    jQuery("[data-name=lm_asset_id]").val("<?= $assetData->asset_id ?>");

    jQuery("[data-name=lm_year]").val("<?= $assetData->year ?>");
    jQuery("[data-name=lm_make]").val("<?= $assetData->make ?>");
    jQuery("[data-name=lm_model]").val("<?= $assetData->model ?>");

    setTimeout(function(){  

      jQuery("[data-name=lm_date_of_checkout]").val("<?= $loanData['loanRequest']->date_of_checkout ?>");
      jQuery("[data-name=lm_date_of_return]").val("<?= $loanData['loanRequest']->requested_return_date ?>"); // Retrun Date 
      jQuery(".repeate-this textarea").removeAttr("disabled");

    }, 1000);

    jQuery("[data-name=lm_odometer_reading]").val("<?= $loanData['loanRequest']->odometer_reading_checkout ?>");
    jQuery("[data-name=lm_usage_location]").val("<?= $loanData['loanRequest']->usage_location ?>"); // Usage Location
    jQuery("[data-name=lm_reason]").val("<?= $loanData['loanRequest']->reason ?>"); // Reason
    jQuery("[data-name=lm_client_organization]").val("<?= $loanData['loanRequest']->organization ?>"); // organization
    jQuery("[data-name=lm_client_name]").val("<?= $loanData['loanRequest']->client_name ?>"); // client name
    jQuery("[data-name=lm_project]").val("<?= $loanData['loanRequest']->project ?>"); // client project
 
    // Seetings for loan Policy 
    jQuery("[data-name=lm_policy]").parent().parent().hide();
    jQuery("[data-name=lm_policy]").parent().parent().prepend("<div class='policy-txt'><?= get_option('lm_loan_policy_text') ?></div>");

    <?php if($loanData['loanRequest']->loan_policy=="Yes"){  ?> 
      jQuery("[data-name=lm_policy]").prop('checked', true);
    <?php } ?> 
    
    <?php if($loanData['loanRequest']->checking_for_client=="Yes"){ ?>
      jQuery("[data-name=lm_checking_for_client]").click();
    <?php } ?>

    //SET LOAN REQUEST META DATA
    var loanRequestMeta = '<?php echo json_encode($loanData["loanRequestMeta"] ); ?>';
    var rows = "";
    if(!jQuery.isEmptyObject(JSON.parse(loanRequestMeta))){
        jQuery.each(JSON.parse(loanRequestMeta), function( index, value ) {
          
          if(index==0){
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" type="hidden" value="'+value.id+'"></td><td><a class="add-more-damage">+</a></td></tr>';
          }else{
            rows+='<tr class="repeate-this"><td><input name="component[]" type="" value="'+value.component_name+'"></td><td><textarea name="description[]">'+value.component_description+'</textarea><input name="damage_item_id[]" type="hidden" value="'+value.id+'"></td><td><a class="remove-damage">x</a></td></tr>';
          }

        });

        jQuery("#damage-table tbody").html(rows);
        
        jQuery("[data-name=lm_usage_location]").html('<?= $locationsOptions ?>'); // Location dropdown 
        jQuery("[data-name=lm_reason]").html('<?= $reasonsOptions ?>'); // Location dropdown 
    }

  }


  //////////////////////////////////////////////////////////////////////////////////////////////////////
  // Apply checks for max return date and max odometer reading  

  // How may day item can be loanded
  var max_date = new Date();
  max_date.setDate(max_date.getDate()+<?= $max_loan_period ?>);


  // next effective event
  var next_event = new Date('<?= $next_event ?>');

  jQuery("[data-name=lm_date_of_return]").change(function(){
      var returnDate = jQuery(this).val();
      returnDate = new Date(returnDate);

      if(new Date(returnDate) > new Date(next_event))
      { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $next_event ?></p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      } else if( new Date(returnDate) > new Date(max_date) ) { 
          
          jQuery(this).val('');
          jQuery(this).css('border-color','red');
 
          var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be loaned for <?= $max_loan_period ?> days</p></div>";
          jQuery(".lmwarn").remove();
          jQuery(this).parent().parent().append(warning);


      }else{
          jQuery(".lmwarn").remove();
      }

  })

  
 // Warnning on odometer reading if exceeded from effective 
  jQuery("[data-name=lm_odometer_reading]").on('blur',function(){
        var odometer = parseInt(jQuery(this).val());
        var MD_reading = parseInt("<?= $MD_reading ?>");
        if(MD_reading > 0 && odometer > MD_reading){
             var warning = "<div class='wdform-element-section wd-flex lmwarn wd-flex-row wd-width-100'><p style='color:red'>This piece of equipment can be checked out through <?= $MD_reading.' '.$assetData->maintenance_criteria ?></p></div>";
            jQuery(".lmwarn").remove();
            jQuery(this).parent().parent().append(warning);
            jQuery(this).val("");
        }else{
            jQuery(".lmwarn").remove();
        }
  });
  </script>
  <style type="text/css">
    .repeate-this input { width:100%;  border: 1px solid #dfdfdf;  }
    .repeate-this .remove-damage {color: red;}
    .repeate-this .add-more-damage {color: green; font-weight: bold; font-size: 15px;}
  </style>
 

 