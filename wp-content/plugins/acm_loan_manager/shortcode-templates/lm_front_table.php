<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/
global $wpdb;
?>


<?php
 /* ************************Get All Equipments************************ */
$page_slug                = 'alm-front-page';
$equipmentData            = array();
$equipment_type_ids_array = array();
$all_equipment_types_name = array();
 
$equipment_sql        = "SELECT equipment_type,id FROM en_mb_equipment_types";
$equipmentArr         = $wpdb->get_results( $equipment_sql, OBJECT );

foreach ($equipmentArr as $key => $equipment) {
  $equipmentData[str_replace(' ', '-', strtolower($equipment->equipment_type))]=$equipment->id;
  $all_equipment_types_name[]=$equipment->equipment_type;
}

 
/* *******************Get Capebilitiies for this Page***************** */
$userData = get_userdata( get_current_user_id() );
if ( is_object( $userData) ) {
    $current_user_caps  = $userData->allcaps; 
    $current_user_roles = $userData->roles;     

    $current_user_caps_custom = array();

    // prepare all capabilities basis on all roles.
    
    foreach ($current_user_roles as $kkey => $role) {
      $roleData     = get_role($role);
      $capabilities = $roleData->capabilities;
      $current_user_roles = $userData->roles; 

      $current_user_caps_custom = array();
      foreach ($capabilities as $capability => $status) {
          if (array_key_exists($capability,$current_user_caps_custom) && $current_user_caps_custom[$capability]==1) {
                  continue;
          }else{
                $current_user_caps_custom[$capability]=$status;
          }
      }

    }
     
    // Get equipment types to show
    foreach ($current_user_caps_custom as $capability => $status) {
      if($status==1){
        $capabilityString_Arr = explode("_",$capability);
        $equipment_type_name  = $capabilityString_Arr[1];
        $is_edit              = $capabilityString_Arr[3];
        if(in_array($page_slug, $capabilityString_Arr)) {
          $equipment_types_view_array[]=$equipmentData[$equipment_type_name];   
        }
        
      }
    }
}

if(!empty($equipment_types_view_array)){
    $equipment_types_view_array = array_unique($equipment_types_view_array);
    $equipment_types_string=implode(",", array_filter($equipment_types_view_array));
}else{
    wp_die("You do not have permission to view any equipment.");
}
 
?>

<?php $tableID = get_option("_lm_table_application_front"); ?>

<?php echo do_shortcode("[wpdatatable id=".$tableID." var1='(".$equipment_types_string.")' ]"); ?>

<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkout_buttons_popup"); ?>

<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkin_buttons_popup"); ?>

<!-- Recall Modal -->
<?php do_action("lm_recall_popup"); ?>

<!-- Warrning modal: if onclick need to confirm -->
<?php do_action("lm_one_click_warrning_popup"); ?>

<!-- Loder -->
<?php do_action("lm_loader"); ?>

<!-- Fast Form Checkout Form Popup -->
<?php do_action("lm_fast_form_checkout_popup"); ?>
 

