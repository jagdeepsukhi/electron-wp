<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/

  $asset_id   = $_GET['asset_id'];
  if(empty($asset_id)){ 
   echo "<p style='color:red'> <strong>Asset ID is required</strong></p>";
   return;
  }

  ?>
  <p>Quick checkout for <strong><?= $asset_id ?></strong></p>
  <?php
  $assetData  = lm_get_equipment_by_asset_id($asset_id);
  echo do_shortcode("[Form id='31']");

?>
<script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery("#wdform_6_element31").val("<?= $assetData->asset_id ?>");
    });
</script>
<style type="text/css">
    .repeate-this input { width:100%;  border: 1px solid #dfdfdf;  }
    .repeate-this .remove-damage {color: red;}
    .repeate-this .add-more-damage {color: green; font-weight: bold; font-size: 15px;}
 </style>
 
<!-- Ajax Loder -->
<?php do_action("lm_loader"); ?> 


 