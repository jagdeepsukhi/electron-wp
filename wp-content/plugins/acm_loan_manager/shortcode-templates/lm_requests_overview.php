<?php
/*
Description: ACM Loan Manager application: Loan manager frontpage shortcode template
*/


$asset_id   = $_GET['asset_id'];
if(empty($asset_id)){
  echo "<p style='color:red'> <strong>Asset ID is required</strong></p>";
   return;
}

// Check permissions
if(!lm_can_view_overview_page($asset_id)){
  wp_die("You do not have permission to view the overview page for this item.");
} 


$assetData  = lm_get_equipment_by_asset_id($asset_id);
$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
?>
<a href="<?= get_permalink(get_option('_lm_page_loan_front')) ?>" class="btn btn-info"> << Back</a></br>
<p><strong>Asset Id:</strong> <?= $asset_id  ?></p>
<p><strong>Equipment Type:</strong> <?= lm_get_eq_type_by_type_id($assetData->equipment_type) ?></p>
<p><strong>Make:</strong> <?= $assetData->make ?></p>
<p><strong>Model:</strong> <?= $assetData->model ?></p>
<p><strong>Year:</strong> <?= $assetData->year ?></p>

<!-- Action Buttons -->
<div class="lm-overview">  
<?= lm_overview_btn_icon($asset_id) ?>
</div>

<input type="hidden" id="overview-assest_id" value="<?= $asset_id?>">
<!-- End -->
<?php $tableID = get_option("_lm_table_overview"); ?>
<?php echo do_shortcode("[wpdatatable id=".$tableID." VAR1='".$asset_id ."']"); ?>
 

<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkout_buttons_popup"); ?>

<!-- Checkout Type buttons Popup -->
<?php do_action("lm_checkin_buttons_popup"); ?>

<!-- Recall Modal -->
<?php do_action("lm_recall_popup"); ?>

<!-- Warrning modal: if onclick need to confirm -->
<?php do_action("lm_one_click_warrning_popup"); ?>

<!-- Loder -->
<?php do_action("lm_loader"); ?>

<!-- Fast Form Checkout Form Popup -->
<?php do_action("lm_fast_form_checkout_popup"); ?>