<?php
/*
* Changes in OLD version Maintenance Manager
*/

 
if ( ! function_exists( 'lm_wpdatatables_after_frontent_edit_row_callback' ) ):
function lm_wpdatatables_after_frontent_edit_row_callback( $formdata){
	global $wpdb;
	$formdata  = $_POST['formdata'];
	$table_id  = $formdata['table_id'];
	$asset_id  = $formdata['asset_id'];
	$record_id = $formdata['id'];
 
 

	/*********************************************************************************************/
	// Facility Equipment Main Page | Prevent user to select "In Service" status if any Maintenance/Calibration is due.
	// _en_mb_wp_datatable_1 = Main Maintenance manager table
	if($table_id==get_option( '_en_mb_wp_datatable_1')){

		if(empty( $record_id )){
			$last_sql = "SELECT asset_id FROM en_mb_facility_equipment WHERE equipment_type='".$formdata['equipment_type']."' AND status='".$formdata['status']."' ORDER BY id DESC LIMIT 1";

			
			$last_arr= $wpdb->get_row($last_sql);
			$asset_id = $last_arr->asset_id;
			lm_update_facility_equipment_table($asset_id);
		}
	}

}
add_action( 'wpdatatables_after_frontent_edit_row', 'lm_wpdatatables_after_frontent_edit_row_callback' );
endif;


/***************************************************/
/****************Loan Application Check************/

function lm_check_asset_in_loan_app_activetable($formdata){

	global $wpdb;
	
	$record_id = $formdata['id'];  
	$asset_id  = $formdata['asset_id'];
	$due_date  = $formdata['due_date'];
	$table_id  = $formdata['table_id'];
	$activity_status  = $formdata['activity_status'];
 
	$assetData = lm_get_equipment_by_asset_id($asset_id);
	$loanData  = lm_get_loan_request_display_data_by_asset_id($asset_id);
	// check if item is checkout in loan application
	if($assetData->loan_status==2 || $assetData->loan_status==7 || $assetData->loan_status==8){

		// Check if item is checlout for Maintenace, Calibration or Repair , Maintenace Due
		if($activity_status==4 || $activity_status==8  || $activity_status==6 || $activity_status==7 ){


			if($assetData->lm_checking_for_client!="Yes"){
				$user = $wpdb->get_row("SELECT display_name FROM wp_users WHERE ID= $assetData->lm_current_custodian");
				$userName=$user->display_name;
			}else{
				$userName = $assetData->lm_current_custodian;
			}
			

			$response = array(
					"error"=>"WARNING!",
					"msg"  =>"WARNING!!! This Asset has not yet been returned. It cannot move forward in the maintenance or calibration processes until it is returned or the maintenance/calibration schedule is changed. You can have an asset recall email sent to the user or rescedulethe event. The current user of this asset is $userName.",
					"code" =>404,
					"asset_id" =>$asset_id,	
					"lm_goto" =>get_permalink(get_option('_lm_page_loan_overview'))."?asset_id=".$asset_id,	
					);
			echo json_encode($response); die;
		}
		
	}
	 
}

function lm_check_asset_in_loan_app_main_table($formdata){

	global $wpdb;
 
	$record_id 		= $formdata['id'];  
	$asset_id  		= $formdata['asset_id']; 
	$activity_status = $formdata['status'];
	$due_date = $formdata['due_date'];
	 
 
	$assetData = lm_get_equipment_by_asset_id($asset_id);
	$loanData  = lm_get_loan_request_display_data_by_asset_id($asset_id);
	// check if item is already checkout in loan application
	if($assetData->loan_status==2 || $assetData->loan_status==7 || $assetData->loan_status==8){

		// Check if item is checlout for Maintenace, Calibration or Repair , Maintenace Due
		if($activity_status==4 || $activity_status==8  || $activity_status==6){

			if($assetData->lm_checking_for_client!="Yes"){
				$user = $wpdb->get_row("SELECT display_name FROM wp_users WHERE ID= $assetData->lm_current_custodian");
				$userName=$user->display_name;
			}else{
				$userName = $assetData->lm_current_custodian;
			}	

			$response = array(
					"error"=>"WARNING!",
					"msg"  =>"WARNING!!! This Asset has not yet been returned. It cannot move forward in the maintenance or calibration processes until it is returned or the maintenance/calibration schedule is changed. You can have an asset recall email sent to the user or rescedulethe event. The current user of this asset is $userName.",
					"code" =>404,
					"asset_id" =>$asset_id,	
					"lm_goto" =>get_permalink(get_option('_lm_page_loan_overview'))."?asset_id=".$asset_id,	
					);
			echo json_encode($response); die;
		}
		
	}
}

add_action('wp_footer', 'lm_asset_available_for_mm_scripts');
function lm_asset_available_for_mm_scripts() { 

	$currentPage = get_the_ID(); 
	$activeMaintenaceAlertPage  = get_option("_en_mb_page_5"); 
	$activeCalibrationAlertPage = get_option("_en_mb_page_6"); 
	$equipmentTrackerPage 		= get_option("_en_mb_page_1"); 

	if($currentPage == $activeMaintenaceAlertPage || $currentPage == $equipmentTrackerPage || $currentPage == $activeCalibrationAlertPage) {
?>

	<script type="text/javascript">
	jQuery(document).ready(function(){  
			jQuery(document).ajaxSuccess(function(event, xhr, settings ) {
			    //console.log(event);
			    var formData = parseQueryString(settings.data);
			    if(formData.action=="wdt_save_table_frontend"){
			    	console.log(formData);
			        var response= xhr.responseText;
			        var resObj 	= jQuery.parseJSON(response);
			        if(resObj.code==404){
			        	jQuery("#wdt-frontend-modal").modal("hide");
			        	jQuery("#lm-loan-trigger").modal("show");
			        	jQuery(".lm-wr").html(resObj.msg);
			        	jQuery(".lm-goto").attr("href",resObj.lm_goto);
			        	jQuery(".mm-recall").attr("data-asset-id",resObj.asset_id);
			        }
			    }			   
		  	});

		  	jQuery(".mm-recall").on("click", function(){
		  		var recall_status 	= jQuery(this).attr("data-recall-status");
		  		var asset_id 		= jQuery(this).attr("data-asset-id");
		  		 
		        // Check do user have permission to recall this item?
		        jQuery(".lm-loader").show();
		        jQuery.ajax({
		               type: "POST",
		               url: LMURLS.ajaxurl,
		               data: {'action':'check_recall_permission',asset_id:asset_id},  
		               success: function(res)
		               {
		                     if(IsJsonString(res)){
		                        var response = jQuery.parseJSON(res);
		                        if(response.code ==200){
		                              // check if item is alrady recalled, remove message textarea box
		                              if(recall_status==1){
		                                jQuery(".recall-text").html("<p>Item is already recalled. Please submit to revert?</p>");
		                              }
		                              jQuery("#lm-loan-trigger").modal("hide");
		                              jQuery("#recallModal").modal("show");
		                              jQuery("#recall_asset_id").val(asset_id);
		                              jQuery(".lm-loader").hide(); 
		                        }else if(response.code ==202){
		                            toastr.error(response.message , { timeOut: 6000 }); 
		                            jQuery(".lm-loader").hide();  
		                        }else{
		                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
		                            jQuery(".lm-loader").hide();
		                        }
		                     }else{
		                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
		                        jQuery(".lm-loader").hide(); 
		                     }
		               }
		          });     
		  	});
	});
	function parseQueryString(queryString) {
	    if (!queryString) {
	        return false;
	    }

	    let queries = queryString.split("&"), params = {}, temp;

	    for (let i = 0, l = queries.length; i < l; i++) {
	        temp = queries[i].split('=');
	        if (temp[1] !== '') {
	            params[temp[0]] = temp[1];
	        }
	    }
	    return params;
	}
	</script>
	<!-- Loder -->
	<?php do_action("lm_loader"); ?>
	<?php do_action("lm_recall_popup"); ?>
	<?php do_action("lm_mm_warning"); ?>
	 
<?php 

	} // endif page check
} // end function 


/*
* Create loan request from Equipment manager
*/
 
function lm_checkout_item_from_MM_application($formdata){
 	global $wpdb;
 

 	$record_id 		= $formdata['id'];  
	$asset_id  		= $formdata['asset_id']; 
	$activity_status= $formdata['activity_status'];
	$location 	    = $formdata['description'];

	// if status is Out for repair, Out for Maintenace, Out for Calibration
	if($activity_status==4 || $activity_status==8 || $activity_status==6 ){

	 	$checkinDays 		= lm_expedited_Return_days($asset_id);
	 	$requested_checkin 	= date('Y-m-d', strtotime('+ '.$checkinDays.' days'));
	 	
	 	// Get next upcoming event
		$next_event = lm_nearest_effective_event_date($asset_id);  
	 	if($requested_checkin >= $next_event && strtotime($next_event) > 0) {
	 		$requested_checkin = $next_event;
	 	}

	 	$locationCheck = $wpdb->get_row("select * from en_lm_loan_locations WHERE location='$location'");
	 	if(!empty($locationCheck)){
	 		$location_id = $locationCheck->id;
	 	}else{
	 		$wpdb->insert("en_lm_loan_locations",array('location'=>$location, 'created_by'=>get_current_user_id()) );
			$location_id = $wpdb->insert_id;
	 	}

	 	$reason = "Maintenance/Calibration";
	 	$locationCheck = $wpdb->get_row("select * from en_lm_loan_reasons WHERE reason='$reason'");
	 	if(!empty($locationCheck)){
	 		$reason_id = $locationCheck->id;
	 	}else{
	 		$wpdb->insert("en_lm_loan_reasons",array('reason'=>$reason, 'created_by'=>get_current_user_id()) );
			$reason_id = $wpdb->insert_id;
	 	}

		$data 							= array();
		$data['asset_id'] 				= $asset_id;
		$data['date_of_checkout'] 		= date("Y-m-d");
		$data['requested_return_date'] 	= $requested_checkin; 
		$data['checking_for_client']	= "No";
		$data['modify_by'] 				= get_current_user_id();
	 	$data['modify_date'] 			= date("Y-m-d H:i:s");
	 	
		$loan_status 					= 2; // Checked out
		$data['approve_status'] 		= 6;// Approved
		$data['approved_deny_by'] 		= get_current_user_id(); //Set current requester as reviewer
		
		$data['created_by'] 			= get_current_user_id();
		$data['created_date'] 			= date("Y-m-d H:i:s");
		$data['check_in_out_status']	= $loan_status;
		$data['check_out_type'] 		= "Maintenance/Calibration";
		$data['loan_policy'] 			= 'Yes';
		$data['usage_location'] 		= $location_id;
		$data['reason'] 				= $reason_id;
		
		$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
		 
		$wpdb->insert("en_lm_loan_requests", $data );
		$request_id = $wpdb->insert_id;
		$loanRequestMeta 	= $loanData["loanRequestMeta"];

		foreach ($loanRequestMeta as $key => $component) {
			if(!empty($component)){

				$meta = array(
								"request_id"			=> $request_id,
								"component_name"		=> $component->component_name,
								"component_description"	=> $component->component_description,
								"created_by"			=> get_current_user_id(),
								"modify_by"				=> get_current_user_id(),
								"created_date"			=> date("Y-m-d H:i:s"),
								"modify_date"			=> date("Y-m-d H:i:s"),
								"is_additional"			=> 0,
								"is_old"				=> 1
							);

				$wpdb->insert("en_lm_loan_requests_meta", $meta);	
			}
		
			$i++;
		} // end foreach
	 
		// Update loan navigation
		lm_update_facility_equipment_table($asset_id);
 
	} // end if
	return true;
}

/**
 * Saves the table from frontend
 */
function wdtSaveTableFrontend_before() {
    global $wpdb, $wp_version;
    $formData = $_POST['formdata'];
    $tableId  = (int)$formData['table_id'];

    /*********************************************************************************************/
	// Action hook for update Facility Equipment Table Status on update Active Maintenance Items
	if(	$tableId==get_option( '_en_mb_wp_datatable_20') || 
		$tableId==get_option( '_en_mb_wp_datatable_21') || 
		$tableId==get_option( '_en_mb_wp_datatable_1'))
	{	 
		lm_check_asset_in_loan_app_activetable($formData);
		lm_checkout_item_from_MM_application($formData);
	}
	
	if(	$tableId==get_option( '_en_mb_wp_datatable_1'))
	{	 
		lm_check_asset_in_loan_app_main_table($formData);
		lm_checkout_item_from_MM_application($formData);	
	}
}

add_action('wp_ajax_wdt_save_table_frontend', 'wdtSaveTableFrontend_before');
add_action('wp_ajax_nopriv_wdt_save_table_frontend', 'wdtSaveTableFrontend_before'); 

function lm_get_latest_odometer_reading($asset_id){
	global $wpdb;
	$loan_reading = 0;
	$fuel_reading = 0;
	$final_reading = 0;

	$sql1 = "SELECT odometer FROM en_avte_fuel WHERE en_avte_fuel.car_id='$asset_id' ORDER BY en_avte_fuel.id DESC LIMIT 1";
	$result1 = $wpdb->get_row($sql1);
	
	if(!empty($result1)){
		$fuel_reading = $result1->odometer;
	} 

	$sql2 = "SELECT odometer_reading_checkout,odometer_reading_return FROM `en_lm_loan_requests`WHERE approve_status=6 AND asset_id='$asset_id' ORDER BY id DESC LIMIT 1";
	$result2 = $wpdb->get_row($sql2);

	if( !empty($result2) ){
		$odometer_reading_checkout 	= $result2->odometer_reading_checkout;
		$odometer_reading_return 	= $result2->odometer_reading_return;
		if($odometer_reading_checkout >=$odometer_reading_return){
			$loan_reading = $odometer_reading_checkout;
		}else{
			$loan_reading = $odometer_reading_return;
		}
	}

	if($loan_reading >= $fuel_reading){
		$final_reading = $loan_reading;
	}else{
		$final_reading = $fuel_reading;
	}

	return $final_reading;

}
?>