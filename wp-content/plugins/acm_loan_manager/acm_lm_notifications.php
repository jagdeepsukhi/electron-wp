<?php
/*
* NOTIFICATION FUNCTIONS
*/

/*
* Declare Function: This function use to send email through SMTP or WP_MAIL function
*/ 
function lm_send_emails($subject,$message,$toEmail) {

	$headers 			= array('Content-Type: text/html; charset=UTF-8');			

	$toArr = explode(',', $toEmail);
	foreach ($toArr as $key => $to) {
		wp_mail( $to, $subject, $message, $headers );
	}				

	
}

/*
* Declare Function: This function creates common loan fields for email tags ##details##
*/
function lm_create_detail_tag_html($asset_id,$loanData){

	global $wpdb;
	$dash  ="--";
 
  	$next_schedule_event     = lm_nearest_effective_event_date($asset_id);
 	$next_schedule_distance  = lm_nearest_effective_event_reading($asset_id);

	
	$who_recall = $loanData->created_by_name . ", <a href='mailto:".$loanData->created_by."'>".$loanData->created_by."</a>";
	
	$tagHtml = "<h4><strong>Details</strong></h4>";
	$tagHtml .= "<p><strong>Who: </strong> ".$who_recall."</p>";

	$tagHtml .= "<p><strong>Asset: </strong> ".$loanData->asset_id.",".(!empty($loanData->year) ? $loanData->year : $dash)." ".(!empty($loanData->make) ? $loanData->make : $dash)." ".(!empty($loanData->model) ? $loanData->model : $dash)."</p>";

	$tagHtml .= "<p><strong>Data of Checkout: </strong>".(!empty($loanData->date_of_checkout) ? $loanData->date_of_checkout : $dash)."</p>";
	
	$tagHtml .= "<p><strong>Date to be returned: </strong>".(!empty($loanData->requested_return_date) ? $loanData->requested_return_date : $dash)."</p>";

	$tagHtml .= "<p><strong>Odometer/Hours Used: </strong>".(!empty($loanData->odometer_reading_checkout) ? $loanData->odometer_reading_checkout : $dash)."</p>";

	$tagHtml .= "<p><strong>Reason for loan request: </strong>".(!empty($loanData->reason) ? $loanData->reason : $dash)."</p>";

	$tagHtml .= "<p><strong>Next Scheduled Event Date: </strong>".(!empty($next_schedule_event) ? $next_schedule_event : 'Nothing Scheduled')."</p>";

	$tagHtml .= "<p><strong>Next Scheduled Event Odometer: </strong>".(!empty($next_schedule_distance) ? $next_schedule_distance : 'Nothing Scheduled')."</p>";

	$tagHtml .= "<p><strong>Client Use:</strong> ".(!empty($loanData->checking_for_client) ? $loanData->checking_for_client : $dash)."</p>";


	if($loanData->checking_for_client=="Yes"){
		$tagHtml .= "<h2>Client Details:</h2>";

		$tagHtml .= "<p><strong>Organization:</strong> ".(!empty($loanData->organization) ? $loanData->organization : $dash)."</p>";

		$tagHtml .= "<p><strong>Client Name:</strong> ".(!empty($loanData->client_name) ? $loanData->client_name : $dash)."</p>";
		$tagHtml .= "<p><strong>Project #:</strong> ".(!empty($loanData->project) ? $loanData->project : $dash)."</p>";
	}


	$tagHtml .= "<h3>Previous damage:</h3>"; 
	$tagHtml .= "<p>".(!empty($loanData->previous_damage) ? $loanData->previous_damage : $dash)."</p>";

	
	$tagHtml .= "<h3>Newly discovered damage:</h3>"; 
	$tagHtml .= "<p>".(!empty($loanData->new_damage) ? $loanData->new_damage : $dash)."</p>";

	return $tagHtml;
} 


 
/*
* Declare Function: This function use to send notification to approvers on new loan request.
*/ 
function lm_send_new_request_notification_to_approvers($asset_id){
 	global $wpdb;
 	//$template_id		= 1308;
 	$template_id		= get_option("_lm_email_request_email_to_approvers");
 	if(empty($template_id)){
 		return;
 	}
 	$templateContent 		= get_post_field('post_content', $template_id);

	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);

 	$subject 			= get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$request_token = getToken(60);

 	$wpdb->update("en_lm_loan_requests",array("request_token"=>$request_token) , array("id"=>$loanData->request_id) );

 	
 	// create approve/disapprove buttons for tag ##approveDenyButtons##
 	$salt 			="AJHSGHAGSAGJS";
	$encrpt_approver= base64_encode($approver_id . $salt);

 	$approversArr  	= lm_get_request_approvers($loanData->equipment_type);
 	foreach ($approversArr  as $key => $approver) {
 		$approver_id = $approver['approver_id'];
 		$toEmail 	 = $approver['email'];

	 	$approveLink= get_bloginfo('url')."/loan-request-direct-approve-deny?action=aprrove&token=".$request_token."__".$encrpt_approver."__".$asset_id;
	 	$denyLink 	= get_bloginfo('url')."/loan-request-direct-approve-deny?action=deny&token=".$request_token."__".$encrpt_approver."__".$asset_id;

	 	$btnHtml  = "<div>";
	 	$btnHtml .= "<a style='color: white;border: navajowhite;padding: 7px 15px;font-size: 17px;background: green;margin-right: 11px;text-decoration: none;' href='".$approveLink."'>Approve</a>";

	 	$btnHtml .= "<a style='color: white;border: navajowhite;padding: 7px 15px;font-size: 17px;background: red;margin-right: 11px;text-decoration: none;' href='".$denyLink."'>Deny</a>";
	 
	 	$btnHtml .= "</div>";

	 	$bodyContent = str_replace("##details##", $tagHtml, $templateContent);	
	 	$bodyContent = str_replace("##approveDenyButtons##", $btnHtml, $bodyContent);	

		lm_send_emails($subject,$bodyContent,$toEmail);	

	} 
}


/*
* Declare Function: This function use to send reacll notification to requestor.
*/ 

function lm_send_recall_email_to_requestor($asset_id,$reason){
 	global $wpdb;
 	//$template_id		= 1305;
 	$template_id		= get_option("_lm_email_asset_recall");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);


 	$currentUserData  	= wp_get_current_user() ;

	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	
 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;
	// get common details for ##details## tag used in email template
	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);

	// get additional details for ##details## tag used in email template
	$tagHtml .= "<p><h2>Recall Details:</h2></p>";

	$tagHtml .= "<p><strong>Recall Requestor: </strong>".(!empty($currentUserData->data->display_name) ? $currentUserData->data->display_name : $dash)."</p>";

	// $tagHtml .= "<p><strong>Phone:</strong> (XXX) XXX­XXXX</p>";
	$tagHtml .= "<p><strong>Email: </strong>".(!empty($currentUserData->data->user_email) ? $currentUserData->data->user_email : $dash)."</p>";

	$tagHtml .= "<p><strong>Reason for Recall: </strong>".(!empty($reason) ? $reason : 'No reason defined.')."</p>";


 	$bodyContent = str_replace("##details##", $tagHtml, $bodyContent);	
 	$bodyContent = str_replace("##approveDenyButtons##", $btnHtml, $bodyContent);	 
	$toEmail     = $loanData->created_by;
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}



/*
* Declare Function: This function use to send notification for loan requuest confirmation.
*/ 

function lm_send_notification_loan_request_confirmation($asset_id){
 	global $wpdb;
 	//$template_id		= 1311;
 	$template_id		= get_option("_lm_email_request_confirmation");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);

 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail = $currentUserData->data->user_email;
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}

/*
* Declare Function: This function use to send notification for loan requuest denied.
*/ 

function lm_send_notification_loan_request_denied($asset_id){
 	global $wpdb;
 	//$template_id		= 1312;
 	$template_id		= get_option("_lm_email_request_denied");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);

	$tagHtml 			= "<p><strong>Reviewing manager:</strong><a href='mailto:".$loanData->approved_deny_by_email."'>".(!empty($loanData->approved_deny_by_name) ? $loanData->approved_deny_by_name : '--')."</a></p>";

	$tagHtml 			.= "<p><strong>Message from reviewer</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->common_notes) ? $loanData->common_notes : '---')."</p>";

 	$tagHtml 			.= lm_create_detail_tag_html($asset_id,$loanData);

 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail = $loanData->created_by;
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}

/*
* Declare Function: This function use to send notification for Damage Reported to Maintenance Manager.
*/ 

function lm_send_notification_damage_reported($asset_id){
 	global $wpdb;
 	//$template_id		= 1332;
 	$template_id		= get_option("_lm_email_damage_reported");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);

	// prepare email html
	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);

	$tagHtml 			.= "<h2>Asset Return Details:</h2>";
	$tagHtml 			.= "<p><strong>Return By:</strong><a href='mailto:".$loanData->approved_deny_by_email."'>".(!empty($loanData->approved_deny_by_name) ? $loanData->approved_deny_by_name : '--')."</a></p>";	
	$tagHtml 			.= "<p><strong>Phone: </strong>".(!empty($loanData->approved_deny_by_name) ? $loanData->approved_deny_by_name : '--')."</p>";	
	$tagHtml 			.= "<p><strong>Email: </strong>".(!empty($loanData->approved_deny_by_email) ? $loanData->approved_deny_by_email : '--')."</p>";
	$tagHtml 			.= "<p><strong>Issued experienced with asset:</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->experience_note) ? $loanData->experience_note : 'Not Provided.')."</p>";
	$tagHtml 			.= "<p><strong>Newly reported damange:</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->reported_damage) ? $loanData->reported_damage : 'Not Provided.')."</p>";
	
	// send emails
 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail 			= lm_get_maintenance_managers($loanData->equipment_type);
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}

/*
* Declare Function: This function use to send notification for loan requuest approved.
*/ 

function lm_send_notification_loan_request_approved($asset_id){
 	global $wpdb;
 	//$template_id		= 1328;
 	$template_id		= get_option("_lm_email_request_approved");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
	
	$tagHtml 			= "<p><strong>Reviewing manager:</strong><a href='mailto:".$loanData->approved_deny_by_email."'>".(!empty($loanData->approved_deny_by_name) ? $loanData->approved_deny_by_name : '--')."</a></p>";

	$tagHtml 			.= "<p><strong>Message from reviewer</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->common_notes) ? $loanData->common_notes : '---')."</p>";

 	$tagHtml 			.= lm_create_detail_tag_html($asset_id,$loanData);

 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail 			= $loanData->created_by;
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}

/*
* Declare Function: This function use to send notification for asset return.
*/ 

function lm_send_notification_on_asset_return($asset_id){
 	global $wpdb;
 	$template_id		= get_option("_lm_email_return_email");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
	
 	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);
 	$tagHtml 			.= "<h4>Asset Return Details:</h4>";
	$tagHtml 			.= "<p><strong>Returned by:</strong><a href='mailto:".$loanData->checkin_by."'>".(!empty($loanData->checkin_by) ? $loanData->checkin_by : '--')."</a></p>";

	$tagHtml 			.= "<p><strong>Issued experienced with asset:</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->experience_note) ? $loanData->experience_note : 'N/A')."</p>";
	$tagHtml 			.= "<p><strong>Newly reported damange:</strong></p>";
	$tagHtml 			.= "<p>".(!empty($loanData->reported_damage) ? $loanData->reported_damage : 'N/A')."</p>";

 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail = $loanData->created_by;
	lm_send_emails($subject,$bodyContent,$toEmail);	 
}

/*
* Declare Function: Get all maintenance managers emails
*/ 

function lm_get_maintenance_managers($equipment_type){
	global $wpdb;

	$sql ="SELECT emails FROM `en_mb_equipment_manager_settings` WHERE equipment_type ='$equipment_type' ";
	$row = $wpdb->get_row($sql);
	
	if(!empty($row)){
		
		return $row->emails;
	}

	 
}

/*
* Declare Function: Get all apporvers emails
*/ 

function lm_get_request_approvers($equipment_type){
	$prepArr  	= array();
	$approvers 	= lm_get_loan_equipment_settings($equipment_type,'request_approvers');
	if(!empty($approvers)){
		$approversArr = explode(',', $approvers);
		foreach ($approversArr as $key => $approver_id) {
			$user =get_userdata( $approver_id );
 			$prepArr[] = array("email"=>$user->data->user_email,"approver_id"=>$approver_id);
		}
	}

	if(!empty($prepArr)){
		return $prepArr;
	}
}

/*
* Declare Function: Create token for request used to approve/disapprove a request
*/ 

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
    }

    return $token;
}
function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}

/*
* function to create email templates for Loan Manager Notifications
*/
function lm_email_type_post_type() {

	$labels = array(
		'name'                => _x( 'LM Email Template', 'Post Type General Name' ),
		'singular_name'       => _x( 'LM Email Template', 'Post Type Singular Name'),
		'menu_name'           => __( 'LM Email Templates'),
		'parent_item_colon'   => __( 'Parent Template'),
		'all_items'           => __( 'All Templates'),
		'view_item'           => __( 'View Template'),
		'add_new_item'        => __( 'Add New Template' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Template' ),
		'update_item'         => __( 'Update Template' ),
		'search_items'        => __( 'Search Template' ),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);

	$args = array(
		'label'               => __( 'template' ),
		'description'         => __( 'Template news and reviews' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'template', $args );

}
add_action( 'init', 'lm_email_type_post_type', 0 );


function lm_add_tags_description_metabox() {

    add_meta_box(
        'tags_description_meta_box_callback',
        __( 'Subject', 'sitepoint' ),
        'tags_description_meta_box_callback',
        'template',
        'advanced',  
		'high'
    );
}
add_action( 'add_meta_boxes', 'lm_add_tags_description_metabox' );

function tags_description_meta_box_callback(){

	global $post;
	$custom = get_post_custom( $post->ID );
 
	?>
	<p>
		<label><strong>Email Subject:</strong></label><br />
		<input type="text" name="email_subject" style="width: 100%" value="<?= @$custom["email_subject"][0] ?>" />
	</p>
	<?php
	echo "Here is a list of tags you can use in this email :</br></br><strong>##details##</strong> - Loan Request Data</br><strong>##approveDenyButtons##</strong> - Loan Request Approve/Deny buttons";

}

/**
 * Save custom field data
 */
function lm_email_template_custom_field_save(){
  global $post;
 
  if ( $post )
  {
    update_post_meta($post->ID, "email_subject", @$_POST["email_subject"]);
  }
}
add_action( 'save_post', 'lm_email_template_custom_field_save' );



function lm_effective_event_due($asset_id,$type=''){
	// Type can be : maintenance_date_based, calibration_due, maintenance_usage_based, maintenance_distance_based

	global $wpdb;
 
	if($type=="maintenance_date_based"){
		
		$sql = "SELECT (mp.scheduled_date - INTERVAL ls.return_ad_next_sch_event_days DAY ) as scheduled_date FROM en_mb_maintenance_plans as mp LEFT JOIN en_mb_facility_equipment as e ON mp.asset_id = e.asset_id LEFT JOIN en_lm_loan_equipment_settings as ls on ls.equipment_type = e.equipment_type WHERE mp.asset_id='$asset_id' ";

	
	  	$commingEventData     = $wpdb->get_row($sql);	
	  	return $commingEventData ->scheduled_date;

	}else if($type=="calibration_due"){
		
		$sql = "SELECT (cp.scheduled_calibration - INTERVAL ls.return_ad_next_sch_event_days DAY) as scheduled_calibration FROM en_mb_calibrations_plans as cp LEFT JOIN en_mb_facility_equipment e ON cp.asset_id = e.asset_id LEFT JOIN en_lm_loan_equipment_settings as ls on ls.equipment_type = e.equipment_type WHERE cp.asset_id='$asset_id' ";

	  	$commingEventData     = $wpdb->get_row($sql);
	  	return $commingEventData ->scheduled_calibration;

	}else if($type=="maintenance_usage_based"){

		$milesSql = "SELECT (MIN(mp.benchmark_odometer + mp.criteria) - ls.return_ad_next_sch_event_miles ) as reading, mp.criteria_type FROM en_mb_maintenance_plans_meta as mp LEFT JOIN en_mb_facility_equipment e ON mp.asset_id = e.asset_id LEFT JOIN en_lm_loan_equipment_settings as ls on ls.equipment_type = e.equipment_type WHERE mp.asset_id='$asset_id' and mp.maintenance_type='Usage Based' order by mp.distance ASC LIMIT 0,1";
 		$milesEvent = $wpdb->get_row($milesSql);
  		return $milesEvent->reading;



	}else if($type=="maintenance_distance_based"){

		 
		$milesSql = "SELECT (mp.distance - ls.return_ad_next_sch_event_miles ) as distance, mp.criteria_type FROM en_mb_maintenance_plans_meta as mp LEFT JOIN en_mb_facility_equipment e ON mp.asset_id = e.asset_id LEFT JOIN en_lm_loan_equipment_settings as ls on ls.equipment_type = e.equipment_type WHERE mp.asset_id='$asset_id' and mp.maintenance_type='Distance Based' and mp.due_status = 0 order by mp.distance ASC LIMIT 0,1";

 		$milesEvent = $wpdb->get_row($milesSql);
  		return $milesEvent->distance;

	}
	 
  }







/*function lm_get_next_sechduled($asset_id,$type){
	// Type can be : maintenance_date_based, calibration_due, maintenance_usage_based, maintenance_distance_based

	global $wpdb;
 
	if($type=="maintenance_date_based"){
		
		$sql = "SELECT scheduled_date FROM en_mb_maintenance_plans WHERE asset_id='$asset_id'";
	  	$commingEventData     = $wpdb->get_row($sql);	
	  	return $commingEventData ->scheduled_date;

	}else if($type=="calibration_due"){
		
		$sql = "SELECT scheduled_calibration FROM en_mb_calibrations_plans WHERE asset_id='$asset_id'";
	  	$commingEventData     = $wpdb->get_row($sql);
	  	return $commingEventData ->scheduled_calibration;

	}else if($type=="maintenance_usage_based"){

		$milesSql = "SELECT MIN(benchmark_odometer+criteria) as reading,criteria_type FROM en_mb_maintenance_plans_meta WHERE asset_id='$asset_id' and maintenance_type='Usage Based' order by distance ASC LIMIT 0,1";
 		$milesEvent = $wpdb->get_row($milesSql);
  		return $milesEvent->reading;



	}else if($type=="maintenance_distance_based"){

		$milesSql = "SELECT distance,criteria_type FROM en_mb_maintenance_plans_meta WHERE asset_id='$asset_id' and maintenance_type='Distance Based' and due_status = 0 order by distance ASC LIMIT 0,1";
 		$milesEvent = $wpdb->get_row($milesSql);
  		return $milesEvent->distance;

	}
	 
  }*/

// this function use to get nearest event b/w calibration and date based maintenace due
function lm_nearest_effective_event_date($asset_id){

  $calibration_due = lm_effective_event_due($asset_id,'calibration_due');
  $maintenance_due = lm_effective_event_due($asset_id,'maintenance_date_based'); 

  // get the nearest very event
  if(strtotime($calibration_due ) != 0 && strtotime($maintenance_due ) != 0 ){
      if($maintenance_due < $calibration_due ){
          $next_event  = $maintenance_due;
      }else{
          $next_event  = $calibration_due;
      }

  }else if(strtotime($calibration_due ) != 0 ){

      $next_event  = $calibration_due;

  }else if(strtotime($maintenance_due ) != 0 ){

      $next_event  = $maintenance_due;

  }

  return $next_event;
}

function lm_nearest_effective_event_reading($asset_id){
	$MD_usage_based  	= lm_effective_event_due($asset_id,'maintenance_usage_based');
    $MD_distance_based  = lm_effective_event_due($asset_id,'maintenance_distance_based');
 
 	if($MD_usage_based > 0 && $MD_distance_based >0 ){
 			if($MD_usage_based < $MD_distance_based ){
		  		$next_schedule_distance  = $MD_usage_based;
		  	}else{
		  		$next_schedule_distance  = $MD_distance_based;
		  	}
 	}else if($MD_usage_based > 0){
 		
 			$next_schedule_distance  = $MD_usage_based;

 	}else if($MD_distance_based > 0){

 		$next_schedule_distance  = $MD_distance_based;
 	}	
 	return $next_schedule_distance;
}