<?php
/*
* CRON ACTIONS
*/

 
/*
*  This cron function find the all upcoming events and match with seetings , if found anything need send in reminder then it insert the infomation into the reminder table.
*/


if ( ! wp_next_scheduled( 'lm_update_return_reminder_table' ) ) {
  wp_schedule_event( time(), 'daily', 'lm_update_return_reminder_table' );
}
add_action( 'lm_update_return_reminder_table', 'lm_update_return_reminder_table' );

if ( ! function_exists( 'lm_update_return_reminder_table' ) ):

	function lm_update_return_reminder_table(){
		global $wpdb;

		// Return Overdue Reminder -> It is overdue reminderIf exist no other reminder will be send
		// Return Reminder -> It is reminder for How before days reminder email will send

		$sql= "SELECT  * FROM en_lm_loan_requests lr LEFT JOIN en_mb_facility_equipment e ON e.asset_id=lr.asset_id WHERE lr.check_in_out_status  = '2' OR lr.check_in_out_status  = '8' ";
		$all_checkedout_items 	  = $wpdb->get_results($sql);


		if(!empty($all_checkedout_items)){
			foreach ($all_checkedout_items as $key => $checkedout_item) {

				$count 					= 0;
				$asset_id 				= $checkedout_item->asset_id;
				$equipment_type 		= $checkedout_item->equipment_type;
				$date_of_checkout 		= $checkedout_item->date_of_checkout;
				$requestedReturn  		= $checkedout_item->requested_return_date;
						
				$frequency   			= lm_get_loan_equipment_settings($equipment_type,'return_reminder_email_frequency');
				$adReturnReminder   	= lm_get_loan_equipment_settings($equipment_type,'advance_return_reminder');
				$enableReminder     	= lm_get_loan_equipment_settings($equipment_type,'enable_reminder_email');

				 
			
				if( !is_reminder_exist($asset_id,"Return Overdue Reminder") ){
					
					if( $requestedReturn < date('Y-m-d') ){

							$resonMsg	 = "<hr>";
						 	$resonMsg 	.= "<p>Asset Was due for return on ".$requestedReturn."</p>";
							$resonMsg 	.= "<p>Reason for reminder: The asset loan request specified this return date.</p>";

							// If frequency = 0 No next reminder email will be send
							if(!empty($frequency)){
								$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
							}else{
								$next_send_on = date('Y-m-d');
							}
						 	
						 	$reminderData ['request_id']	= $checkedout_item->id;
						 	$reminderData ['asset_id'] 		= $asset_id;
						 	$reminderData ['reminder_type'] = "Return Overdue Reminder";
						 	$reminderData ['created_on'] 	= date('Y-m-d');
						 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
						 	$reminderData ['next_send_on'] 	= $next_send_on;
						 	$reminderData ['message'] 		= $resonMsg;

						 	// send overdue email to requestor and save into reminder table for next reminder
						 	lm_send_overdue_email($asset_id,$resonMsg );
						 	
						 	//Delete all other reminders for this request.
						 	lm_flush_reminder_table($asset_id);

						 	//Insert Overdue reminder.
						 	$wpdb->insert("en_lm_return_reminders",$reminderData);

						 	// update request status to Past Due/ Overdue
						 	$loan_status = 7 ; 
						 	$navigation  = lm_generate_nav_icons($loan_status );
						 	$updateData  = array("loan_status"=> $loan_status,"loan_navigation"=>$navigation); // Past Due 
							$wpdb->update("en_mb_facility_equipment",$updateData, array("asset_id"=>$asset_id));

						 	$count++;

						 	// Prevent all other reminder to check if item is already triggered as overdue 
						 	continue; 
					 	
					} 
				}else{
					echo " <br> Overdue reminder exist for $asset_id <br>";
					continue; 

				}

				// If reminder email disbaled system will not send normal reminders email.
				if($enableReminder !=1){
					continue;
				}

				// Return Reminder: Normal return rminder before X days
				if(is_reminder_exist($asset_id,"Return Reminder")==false){
					
					if( strtotime($requestedReturn) > 0){

						if( !empty($adReturnReminder) ){
							$adDate = date('Y-m-d', strtotime("-".$adReturnReminder." days", strtotime($requestedReturn)));
						}else{
							$adDate = date('Y-m-d');
						}

						if($adDate <= date('Y-m-d')){
					 
							$resonMsg	 = "<hr>";
						 	$resonMsg 	.= "<p>Asset is due for return on ".$requestedReturn."</p>";
							$resonMsg 	.= "<p>Reason for reminder: The asset loan request specified this return date.</p>";

							// If frequency = 0 No next reminder email will be send
							if(!empty($frequency)){
								$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
							}else{
								$next_send_on = date('Y-m-d');
							}
						 	
						 	$reminderData ['request_id']	= $checkedout_item->id;
						 	$reminderData ['asset_id'] 		= $asset_id;
						 	$reminderData ['reminder_type'] = "Return Reminder";
						 	$reminderData ['created_on'] 	= date('Y-m-d');
						 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
						 	$reminderData ['next_send_on'] 	= $next_send_on;
						 	$reminderData ['message'] 		= $resonMsg;

						 	// send email to requestor and save into reminder table for next reminder
						 	lm_send_reminder_email($asset_id,$resonMsg );
	
						 	//Insert Return reminder.
						 	$wpdb->insert("en_lm_return_reminders",$reminderData);

						 	$count++;
					 	} // end if advance date is = today date 
					}
				}
				


				 
				// Some are additional cases

				// Check if calibration due with in the advance reminder days set

				if(is_reminder_exist($asset_id,"Calibration Due")==false){
					$next_calibration_due 	= lm_effective_event_due($asset_id,'calibration_due');	
					if(strtotime($next_calibration_due ) ){

				 		if( strtotime( $next_calibration_due ) == strtotime( date('Y-m-d') ) ){	
				 			$resonMsg			 = "<hr>";
						 	$resonMsg 			.= "<p>Asset is due for calibration on ".$next_calibration_due."</p>";
							$resonMsg 			.= "<p>Reason for reminder: The asset requires calibration.</p>";

							// If frequency = 0 No next reminder email will be send
							if(!empty($frequency)){
								$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
							}else{
								$next_send_on = date('Y-m-d');
							}
						 	
						 	
						 	$reminderData ['request_id']	= $checkedout_item->id;
						 	$reminderData ['asset_id'] 		= $asset_id;
						 	$reminderData ['reminder_type'] = "Calibration Due";
						 	$reminderData ['created_on'] 	= date('Y-m-d');
						 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
						 	$reminderData ['next_send_on'] 	= $next_send_on;
						 	$reminderData ['message'] 		= $resonMsg;

						 	// send email to requestor and save into reminder table for next reminder
						 	lm_send_reminder_email($asset_id,$resonMsg );
						 	$wpdb->insert("en_lm_return_reminders",$reminderData);
						 	$count++;
						}

					}
				}  // endif is reminder exist	



				// Check if date based maintenance due within the advance reminder days sets
				if(is_reminder_exist($asset_id,"Maintenance Date Based Due")==false){
					$MD_date_based 	= lm_effective_event_due($asset_id,'maintenance_date_based');
	
					if( strtotime($MD_date_based ) ){

						if( strtotime( $MD_date_based ) == strtotime( date('Y-m-d') ) ){
				 			
				 			$resonMsg			 = "<hr>";
						 	$resonMsg 			.= "<p>Asset is due for maintenance on ".$MD_date_based."</p>";
							$resonMsg 			.= "<p>Reason for reminder: The asset requires maintenance.</p>";

							// If frequency = 0 No next reminder email will be send
							if(!empty($frequency)){
								$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
							}else{
								$next_send_on = date('Y-m-d');
							}
						 	
						 	
						 	$reminderData ['request_id']	= $checkedout_item->id;
						 	$reminderData ['asset_id'] 		= $asset_id;
						 	$reminderData ['reminder_type'] = "Maintenance Date Based Due";
						 	$reminderData ['created_on'] 	= date('Y-m-d');
						 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
						 	$reminderData ['next_send_on'] 	= $next_send_on;
						 	$reminderData ['message'] 		= $resonMsg;

						 	// send email to requestor and save into reminder table for next reminder
						 	lm_send_reminder_email($asset_id,$resonMsg );
						 	$wpdb->insert("en_lm_return_reminders",$reminderData);
						 	$count++;
						}

					}
				}  // endif is reminder exist
				



				// Check if Usage based maintenance due within the advance reminder days sets
				if(is_reminder_exist($asset_id,"Maintenance Usage Based Due")==false){
					$returnAdvanceMiles = lm_get_loan_equipment_settings($equipment_type,'return_ad_next_sch_event_miles');
					$odo_reading 		= lm_odometer_reading_from_fuel_log($asset_id);
					$MD_usage_based 	= lm_effective_event_due($asset_id,'maintenance_usage_based');
					if(!empty($MD_usage_based) && !empty($odo_reading) ){
							
							if( $MD_usage_based >= $odo_reading){

								$resonMsg			 = "<hr>";
							 	$resonMsg 			.= "<p>Asset is due for maintenance on ".$MD_usage_based."</p>";
								$resonMsg 			.= "<p>Reason for reminder: The asset requires maintenance.</p>";

								// If frequency = 0 No next reminder email will be send
								if(!empty($frequency)){
									$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
								}else{
									$next_send_on = date('Y-m-d');
								}
						 	
							 	
							 	$reminderData ['request_id']	= $checkedout_item->id;
							 	$reminderData ['asset_id'] 		= $asset_id;
							 	$reminderData ['reminder_type'] = "Maintenance Usage Based Due";
							 	$reminderData ['created_on'] 	= date('Y-m-d');
							 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
							 	$reminderData ['next_send_on'] 	= $next_send_on;
							 	$reminderData ['message'] 		= $resonMsg;

							 	// send email to requestor and save into reminder table for next reminder
							 	lm_send_reminder_email($asset_id,$resonMsg );
							 	$wpdb->insert("en_lm_return_reminders",$reminderData);
							 	$count++;

							}
					}
				} // endif is reminder exist



				// Check if Distance based maintenance due within the advance reminder days sets
				if(is_reminder_exist($asset_id,"Maintenance Distance Based Due")==false){
					$returnAdvanceMiles = lm_get_loan_equipment_settings($equipment_type,'return_ad_next_sch_event_miles');
					$odo_reading 		= lm_odometer_reading_from_fuel_log($asset_id);
					$MD_distance_based 	= lm_effective_event_due($asset_id,'maintenance_distance_based');
					if(!empty($MD_distance_based) && !empty($odo_reading) ){
							
							if( $MD_usage_based >= $odo_reading){	
								$resonMsg			 = "<hr>";
							 	$resonMsg 			.= "<p>Asset is due for maintenance on ".$MD_distance_based."</p>";
								$resonMsg 			.= "<p>Reason for reminder: The asset requires maintenance.</p>";

								// If frequency = 0 No next reminder email will be send
								if(!empty($frequency)){
									$next_send_on = date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
								}else{
									$next_send_on = date('Y-m-d');
								}
						 	
							 	
							 	$reminderData ['request_id']	= $checkedout_item->id;
							 	$reminderData ['asset_id'] 		= $asset_id;
							 	$reminderData ['reminder_type'] = "Maintenance Distance Based Due";
							 	$reminderData ['created_on'] 	= date('Y-m-d');
							 	$reminderData ['last_sent_on'] 	= date('Y-m-d');
							 	$reminderData ['next_send_on'] 	= $next_send_on;
							 	$reminderData ['message'] 		= $resonMsg;

							 	// send email to requestor and save into reminder table for next reminder
							 	lm_send_reminder_email($asset_id,$resonMsg );
							 	$wpdb->insert("en_lm_return_reminders",$reminderData);
							 	$count++;

							}
					}
				} // endif is reminder exist
 			
				echo "<br>Reminder for $asset_id = $count "; 
				 
			} // for each
		}else {
			echo "No open request found!";
		} // if empty requests

	}

endif;

/*
* This cron function get all active reminders from the reminder table and send emails according to frequancy set in settings page.
*/

if ( ! wp_next_scheduled( 'lm_send_return_reminder_email' ) ) {
  wp_schedule_event( time(), 'daily', 'lm_send_return_reminder_email' );
}
add_action( 'lm_send_return_reminder_email', 'lm_send_return_reminder_email' );

if ( ! function_exists( 'lm_send_return_reminder_email' ) ):

	function lm_send_return_reminder_email(){
		global $wpdb;

		$sql = "SELECT * FROM en_lm_return_reminders r LEFT JOIN en_mb_facility_equipment e ON e.asset_id=r.asset_id";
		$allreminders 	  = $wpdb->get_results($sql);

		if(!empty($allreminders )){
			foreach ($allreminders as $key => $reminder) {
				$next_send_on 	= $reminder->next_send_on;
				$resonMsg		= $reminder->message;
				$asset_id		= $reminder->asset_id;
				$equipment_type	= $reminder->equipment_type;
				$today 			= date('Y-m-d');
				$frequency   	= lm_get_loan_equipment_settings($equipment_type,'return_reminder_email_frequency');
				if($next_send_on==$today ){

					if($reminder->reminder_type=='Return Overdue Reminder'){
						lm_send_overdue_email($asset_id,$resonMsg );
					}else{
						lm_send_reminder_email($asset_id,$resonMsg);
					}

					$next_send_on 			= date('Y-m-d', strtotime("+".$frequency." days", strtotime(date('Y-m-d'))));
					$data['next_send_on']	= $next_send_on;
					$data['last_sent_on']	= $today;
					$wpdb->update('en_lm_return_reminders',$data,array('request_id'=>$reminder->request_id));
				}
			} // end foreach
		} // endif empty $allreminders	
	}
endif;




function check_in_range($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts 	= strtotime($end_date);
  $user_ts 	= strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

function check_miles_in_range($start_ts, $end_ts, $user_ts)
{
 
  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}



function lm_send_reminder_email($asset_id,$resonMsg ){
	//$template_id		= 1348; // reminder email templates
	$template_id		= get_option("_lm_email_return_reminder");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	
 	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);
 	$tagHtml 		   .= '<h3>Return Details:</h3>';
 	$tagHtml 		   .= $resonMsg;


 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail 			= $loanData->created_by;
	if( lm_send_emails($subject,$bodyContent,$toEmail) ){
		echo "<br>Reminder email sent  for $asset_id <br>";
	}else{
		echo "<br>Reminder email couldn't send for $asset_id <br>";
	}	 
}


function lm_send_overdue_email($asset_id,$resonMsg ){
	//$template_id		= 1351; // overdue email templates
	$template_id		= get_option("_lm_email_overdue");
 	if(empty($template_id)){
 		return;
 	}
 	$bodyContent 		= get_post_field('post_content', $template_id);
 
 	$currentUserData  	= wp_get_current_user() ;
	$loanData 			= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	
 	$tagHtml 			= lm_create_detail_tag_html($asset_id,$loanData);
 	$tagHtml 		   .= '<h3>Return Details:</h3>';
 	$tagHtml 		   .= $resonMsg;


 	$subject 			=  get_post_meta($template_id,'email_subject',true).": ".$asset_id.", ".$loanData->year." ".$loanData->make." ".$loanData->model;

 	$bodyContent 		= str_replace("##details##", $tagHtml, $bodyContent);	
	$toEmail 			= $loanData->created_by;
	if( lm_send_emails($subject,$bodyContent,$toEmail) ){
		echo "<br>Reminder email sent  for $asset_id <br>";
	}else{
		echo "<br>Reminder email couldn't send for $asset_id <br>";
	}	 
}

function lm_odometer_reading_from_fuel_log($asset_id)
{
   global $wpdb;
   $result = $wpdb->get_row("SELECT odometer FROM en_avte_fuel WHERE car_id = '$asset_id' ORDER BY id DESC LIMIT 0,1");

   if(!empty($result->odometer)){
   		return  $result->odometer;
   }else{
   		return  0;
   }
   
}

function is_reminder_exist($asset_id,$reminder_type)
{
   global $wpdb;
   $sql = "SELECT reminder_id FROM en_lm_return_reminders WHERE asset_id = '$asset_id' AND reminder_type='$reminder_type'";
   $result = $wpdb->get_row($sql);  
   
   if(empty($result)){	 
   		return  false;
   }else{
   		return  true;
   }
   
}

function lm_flush_reminder_table($asset_id)
{
   global $wpdb;
   $result = $wpdb->query("DELETE FROM en_lm_return_reminders WHERE asset_id = '$asset_id'");
}