<?php
/*
 *  
 * Description: A Page Template with a darker design.
 */
global $wpdb;

?>

 
<div class = "wrap">
 	<h1 class = "wp-heading-inline">ACM Maintenance Manager Configrations</h1>
 	<?php echo @$mess; ?>

 	<h2 class="nav-tab-wrapper wp-clearfix em-tabs">
	<a href="" class="nav-tab en-mb-tabs nav-tab-active" data-id="em-instructions-steps">Instructions & Steps</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-page-config">Page Configrations</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-datatables-config">WP Datatables ID</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-formmaker-config">WP Formmaker ID</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-email-templates-config">Email Templates</a>
	 
	</h2>
	<table class="form-table em-tab-content" id="em-instructions-steps">
 		<tbody>
            <tr>
	 			<th scope="row" style="width:50px; ">Step 1</th>	
	 			<th scope="row">Import source tables</th>
	 			<?php if(is_en_mb_tables_exist('en_lm_loan_equipment_settings')==true && is_en_mb_tables_exist('en_lm_return_reminders')==true) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=import_source_tables" onclick="en_mb_show_loader()">Import Now >></a>
	 					OR 
	 					<a href="<?php bloginfo('url'); ?>/wp-content/plugins/acm_loan_manager/import-data/lm-source-tables.sql" download>Download & Import Manually</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>  
 
 			 <tr>
	 			<th scope="row" style="width:50px; ">Step 2</th>	
	 			<th scope="row">Update Equipment Manager Source Table: en_mb_facility_equipment</th>
	 			<?php if(get_option('_lm_alter_main_table')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-update" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=alter_table" onclick="en_mb_show_loader()">Update Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

            <tr>
	 			<th scope="row" style="width:50px; ">Step 3</th>	
	 			<th scope="row">Import wpdatatables</th>
	 			<?php if(get_option('_lm_import_wp_data_tables')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=wpdatatable_import" onclick="en_mb_show_loader()">Import Now >></a>
	 				</th>
	 			<?php } ?>

            </tr>
 			
 			<tr>
	 			<th scope="row" style="width:50px; ">Step 4</th>	
	 			<th scope="row">Import WP Forms</th>
	 			<?php if(get_option('_lm_create_wpforms')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=create_forms" onclick="en_mb_show_loader()">Import Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

             <tr>
	 			<th scope="row" style="width:50px; ">Step 5</th>	
	 			<th scope="row">Import Pages</th>
	 			<?php if(get_option('_en_lm_create_page')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=create_pages" onclick="en_mb_show_loader()">Import Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

             <tr>
	 			<th scope="row" style="width:50px; ">Step 6</th>	
	 			<th scope="row">Import Email Templates</th>
	 			<?php if(get_option('_en_lm_create_email_templates')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=import_email" onclick="en_mb_show_loader()">Import Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

            <tr>
	 			<th scope="row" style="width:50px; ">Step 7</th>	
	 			<th scope="row">Add Capabilities<br><small><i style="font-weight: normal;"> It will add the required capabilities to the Adminstrator, You can assign Capabilities to other roles as per your need.</i></small></th>
	 			<?php if(get_option('_en_lm_add_caps')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons  dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=add_caps" onclick="en_mb_show_loader()">Add Now >></a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>
             <tr>
	 			<th scope="row" style="width:50px; ">Step 8</th>	
	 			<th scope="row">Update Maintenance Menu<br><small><i style="font-weight: normal;"> It will update fornt-end menu of Maintenance Manager Application and add Loan Application menu items.</i></small></th>
	 			<?php if(get_option('_lm_update_menu')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons  dashicons-update" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=update_menu" onclick="en_mb_show_loader()">Update Now >></a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>


             <tr> 

	 			<th colspan="3">
	 				<hr>
	 				<p >Remove All Configurations:<i style="font-weight: normal;"> Type DELETE in below text box and click on "Delete" button to remove all configurations & source data related to ACM Loan Manager application (Including wpdatatables, Source tables, Pages, WP Forms, Menu Items etc).</i> </p><br> 
	 				
	 				&nbsp;<input type="text" name="" id="delete-conf" placeholder="Type DELETE here">
					&nbsp;<span class="dashicons dashicons-trash" style="color:red;"></span>
					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=loan-settings&action=remove_loan_application" onclick="return en_mb_delete_confirm()" style="color:red;">Delete</a>
					
	 			</th>

            </tr>

		</tbody>
	</table>

 	<form name="form-pages" method="post">	 
 		<table class="form-table em-tab-content" id="em-page-config" style="display: none;">
	 		<tbody>
	 			<tr>
			 		 
			 		<th colspan="2">
			 			<i> Note: All pages will be import on step 5. You do not need to configure manually. In case of duplicate selection it will overwrite the previous template & setting.</i> 
		            </th>
	            </tr>

	            <?php foreach (LM_PAGES as $option_key => $page) { ?>
 				<tr id="<?php echo $option_key; ?>">
			 		<th scope="row" style="width: 300px;"><?php echo $page['title']; ?></th>
			 		<td class="en-mb-select">
			 			<?php echo lm_get_pages_dropdown($option_key); ?>	
		            </td>
	            </tr>
 				<?php } ?>

	             <tr>
	            	<td><input name="submit_pages" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
	            </tr>

			</tbody>
		</table>
 	</form>	


 	<form name="form-tables" method="post">	  
 		<table class="form-table em-tab-content" id="em-datatables-config" style="display: none;">
 		<tbody>
 				<tr>
			 		<th colspan="2">
			 			<i> Once you done with import wpdatatables (Step 3), all the following table ids will automatically populate. You do not need to fill these manually. </i> 
		            </th>
	            </tr>
 			<?php  foreach (LM_DATATABELS as $slug => $tableName) { ?>
 				<tr>
			 		<th scope="row" style="width: 300px;"><?php echo $tableName; ?></th>
			 		<td>
			 			<input name="<?php echo $slug; ?>" type="number" value="<?php echo get_option($slug);?>" style="width: 70px;" required>
		            </td>
	            </tr>
 			<?php } ?>
	 		
            <tr>
            	<td><input name="submit_wptables" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
            </tr>
        </tbody>
        </table>
        <?php echo wp_nonce_field();?>
        </form>	


        <form name="form-tables" method="post">	  
 		<table class="form-table em-tab-content" id="em-formmaker-config" style="display: none;">
 		<tbody>
 				<tr>
			 		<th colspan="2">
			 			<i> Once you are done with importing WP Forms (Step 4), all the following WP Forms IDs will be automatically populate. You do not need to fill these manually. </i> 
		            </th>
	            </tr>
 			<?php  foreach (LM_WPFORMS as $formKey => $form) { ?>
 				<tr>
			 		<th scope="row" style="width: 300px;"><?php echo $form; ?></th>
			 		<td>
			 			<input name="<?php echo $formKey; ?>" type="number" value="<?php echo get_option($formKey);?>" style="width: 70px;" required>
		            </td>
	            </tr>
 			<?php } ?>
	 		
            <tr>
            	<td><input name="submit_wpforms" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
            </tr>
        </tbody>
        </table>
        <?php echo wp_nonce_field();?>
        </form>	

        <form id="form-email-templates" method="post">	  
 		<table class="form-table em-tab-content" id="em-email-templates-config" style="display: none;">
 		<tbody>
 				<tr>
			 		<th colspan="2">
			 			<i> Once you are done with importing Email Templates in (Step 6), all the following templates ids will automatically populate. You do not need to choose templates manually. </i> 
		            </th>
	            </tr>
 			<?php  foreach (LM_EMAIL_TEMPLATES as $tmplateKey => $template) { ?>
 				<tr>
			 		<th scope="row" style="width: 300px;"><?php echo $template; ?></th>
			 		<td>
			 			 
			 			<?php echo lm_get_email_templates_dropdown($tmplateKey); ?>
			 			
		            </td>
	            </tr>
 			<?php } ?>
	 		
            <tr>
            	<td><input name="submit_email_templates" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
            </tr>
        </tbody>
        </table>
        <?php echo wp_nonce_field();?>
        </form>	



        <hr>  
 	
 	<div id="em-datatables-shortcodes" class="em-tab-content" style="display: none;">

 		
 	</div>
 </div>

<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery("body").on("click",".en-mb-tabs",function(e){
			e.preventDefault();
			jQuery(".em-tabs .nav-tab").removeClass("nav-tab-active");
			jQuery(this).addClass("nav-tab-active");
			jQuery(".em-tab-content").hide();
			jQuery("#"+jQuery(this).data("id")).show();
			
		});

		jQuery("body").on("click",".en-mb-page-config",function(e){
			e.preventDefault();
			jQuery('html,body').animate({
		        scrollTop: jQuery("#_en_mb_page_15").offset().top},
		        'slow');

			    jQuery("#_en_mb_page_15 select").css("border","1px solid red");
			
		});


		jQuery('.en-mb-select select').on('change', function() {
		  jQuery('option').prop('disabled', false);
		  jQuery('.en-mb-select select').each(function() {
		    var val = this.value;
		    if(val==''){
		    	return;
		    }
		    jQuery('.en-mb-select select').not(this).find('option').filter(function() {
		      return this.value === val;
		    }).prop('disabled', true);
		  });
		}).change();


	});


function en_mb_delete_confirm() {

	var delete_conf=jQuery("#delete-conf").val();	
	if(delete_conf!="DELETE"){
			jQuery("#delete-conf").css("border","1px solid red");
			return false;
	}

	if(confirm("It will remove all configurations & source data related to ACM Loan Manage application (Including wpdatatables, Source tables, Email Templates, WP Forms, Pages, Menu Items etc). Are you sure to delete plugin configurations?")){
		en_mb_show_loader();
		return true;
	}else{
		return  false; 
	}
   	
   		 
}
function en_mb_show_loader() {

	 jQuery(".mb-loader").show(); 
   		 
}
</script>
<div class="mb-loader" style="display: none;"><img src="<?php echo ACM_PLUGIN_PATH; ?>img/material-loader.gif"></div>
<style type="text/css">
.mb-loader {
    position: fixed;
    top: 0;
    left: 0;
    background: rgba(255,255,255, 0.6);
    width: 100%;
    padding: 15% 50%;
    height: 100%;
}
.mb-loader img {
    width: 100px;
}
</style>