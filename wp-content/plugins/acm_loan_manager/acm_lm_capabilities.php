<?php
/*
* Check caps 
*/

/*
* Global function to get all capabilities of loged-in user
*/
function lm_get_all_capabilities($userID = null){

	if( empty($userID) ){
		$userID =  get_current_user_id() ;
	}
	$current_user_caps_custom = array();
	$userData = get_userdata($userID );
	if ( is_object( $userData) ) {
	    $current_user_caps  = $userData->allcaps; 
	    $current_user_roles = $userData->roles;     
		
	    // prepare all capabilities basis on all roles.  
	    foreach ($current_user_roles as $kkey => $role) {
		      $roleData    	 		= get_role($role);
		      $capabilities 		= $roleData->capabilities;
		     
		      $current_user_caps_custom = array();
		      foreach ($capabilities as $capability => $status) {
		          if (array_key_exists($capability,$current_user_caps_custom) && $current_user_caps_custom[$capability]==1) {
		                  continue;
		          }else{
		                $current_user_caps_custom[$capability]=$status;
		          }
		      }
	    }

	}

	return $current_user_caps_custom;
}

/*
* Function to check user have permission to view overview page or not
*/
function lm_can_view_overview_page($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability= "enet_".$equipment_type_key."_alm-overview-page_view";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}
	 
 	return false;	 	 
}

/*
* Function to check user have permission to recall the item or not
*/
function lm_can_recall( $asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability= "enet_".$equipment_type_key."_ALM_approve-requests";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}
	 
 	return false;	 	 
}


/*
* Function to check user have permission to checkout with fast form or not
*/
function lm_can_checkout_with_fastform( $asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );
 
	$required_capability= "enet_".$equipment_type_key."_ALM-quick-checkout-in_edit";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}
	 
 	return false;	 	 
}


/*
* Function to check user have permission to checkout with one click button or not
*/
function lm_can_checkout_with_oneclick( $asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability= "enet_".$equipment_type_key."_ALM-one-click-checkout-in_edit";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}
	 
 	return false;	 	 
}



/*
* Function to check user have permission to checkout with full form or not
*/
function lm_can_checkout_with_fullform($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability = "enet_".$equipment_type_key."_ALM_edit-any-requests";
	$own_edit_capability = "enet_".$equipment_type_key."_ALM_edit-myown-requests";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}

	if (array_key_exists( $own_edit_capability,$allCpabilities ) && $allCpabilities[$own_edit_capability]==1){
		return true;
	}
	 
 	return false;	 	 
}



/*
* Function to check user have permission to edit pending request or not
*/
function lm_can_edit_pending_request($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$any_edit_capability = "enet_".$equipment_type_key."_ALM_edit-any-requests";
	$own_edit_capability = "enet_".$equipment_type_key."_ALM_edit-myown-requests";
	$approver_capability = "enet_".$equipment_type_key."_ALM_approve-requests";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();

	if (array_key_exists( $any_edit_capability,$allCpabilities ) && $allCpabilities[$any_edit_capability]==1){
		return true;
	}

	if (array_key_exists( $approver_capability,$allCpabilities ) && $allCpabilities[$approver_capability]==1){
		return true;
	}

	if (array_key_exists( $own_edit_capability,$allCpabilities ) && $allCpabilities[$own_edit_capability]==1){
		
		$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
		$created_by = $loanData['loanRequest']->created_by;

		if( $created_by==get_current_user_id() ){
			return true;
		}
		
	}
	 
 	return false;	 	 
}



/*
* Function to check user has permission to approve a request or not
*/
function lm_can_approve_request($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$approver_capability = "enet_".$equipment_type_key."_ALM_approve-requests";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();

	if (array_key_exists( $approver_capability,$allCpabilities ) && $allCpabilities[$approver_capability]==1){
		return true;
	}

 	return false;	 	 
}

/*
* Function to check user have permission to checkin with quick form or not
*/
function lm_can_checkin_with_quickform($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability = "enet_".$equipment_type_key."_ALM_edit-any-requests";
	$oneclick_capability = "enet_".$equipment_type_key."_ALM-one-click-checkout-in_edit";
	$quick_in_capability = "enet_".$equipment_type_key."_ALM-quick-checkout-in_edit";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}
 
	// If user has oneclick_capability AND user organization = request organization
	if (array_key_exists( $oneclick_capability,$allCpabilities ) && $allCpabilities[$oneclick_capability]==1){
		$sql = "SELECT lm_organization FROM en_mb_facility_equipment WHERE asset_id='$asset_id'";
		$et  = $wpdb->get_row($sql);	

		$user_organization = get_user_meta( get_current_user_id(), 'user_organization',true );

		if($et->lm_organization == $user_organization ){
			return true;
		}

	}
	
	// If user has quick_in_capability AND user organization = request organization
	if (array_key_exists( $quick_in_capability,$allCpabilities ) && $allCpabilities[$quick_in_capability]==1){
		$sql = "SELECT lm_organization FROM en_mb_facility_equipment WHERE asset_id='$asset_id'";
		$et  = $wpdb->get_row($sql);	

		$user_organization = get_user_meta( get_current_user_id(), 'user_organization',true );

		if($et->lm_organization ==$user_organization ){
			return true;
		}

	}
	 
 	return false;	 	 
}

/*
* Function to check user have permission to checkin with full form or not
*/
function lm_can_checkin_with_fullform($asset_id){
	global $wpdb;
	
	$sql = "SELECT t.equipment_type FROM en_mb_facility_equipment as e LEFT JOIN en_mb_equipment_types as t ON e.equipment_type = t.id WHERE e.asset_id='$asset_id'";
	$eq_type = $wpdb->get_row($sql);
	$equipment_type_key = str_replace(' ', '-', strtolower($eq_type->equipment_type) );

	$required_capability = "enet_".$equipment_type_key."_ALM_edit-any-requests";
	$own_edit_capability = "enet_".$equipment_type_key."_ALM_edit-myown-requests";

	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 

	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}

	if (array_key_exists( $own_edit_capability,$allCpabilities ) && $allCpabilities[$own_edit_capability]==1){
		
		$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
		$created_by = $loanData['loanRequest']->created_by;

		if( $created_by==get_current_user_id() ){
			return true;
		}
		
	}
	 
 	return false;	 	 
}

/*
* Function to check loan settings page view
*/
function lm_can_view_loan_settings(){
	global $wpdb;
	$required_capability = "enet_equipment-manager-settings_view";
	 
	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 
	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}

 	return false;	 	 
}

/*
* Function to check loan settings page edit
*/
function lm_can_edit_loan_settings(){
	global $wpdb;
	$required_capability = "enet_equipment-manager-settings_edit";
	 
	/* *******************Get Capebilitiies for this Page***************** */
	$allCpabilities = lm_get_all_capabilities();
 
	if (array_key_exists( $required_capability,$allCpabilities ) && $allCpabilities[$required_capability]==1){
		return true;
	}

 	return false;	 	 
}



/*
* Check overview page permission before open the page
*/
add_action("wp_ajax_check_overview_permission", "check_overview_permission");
add_action("wp_ajax_nopriv_check_overview_permission", "check_overview_permission");
function check_overview_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_view_overview_page($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to to overview page.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to overview page for this item.")); 
		die();
	}
}

/*
* Check recall permission before trogger recall action
*/
add_action("wp_ajax_check_recall_permission", "check_recall_permission");
add_action("wp_ajax_nopriv_check_recall_permission", "check_recall_permission");
function check_recall_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_recall($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to recall this piece of equipment.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to recall this piece of equipment.")); 
		die();
	}
}

/*
* Check fast form checkout permission before show Fast Form
*/
add_action("wp_ajax_check_fast_from_permission", "check_fast_from_permission");
add_action("wp_ajax_nopriv_check_fast_from_permission", "check_fast_from_permission");
function check_fast_from_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_checkout_with_fastform($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to checkout this piece of equipment with Fast Form.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to checkout this piece of equipment with Fast Form.")); 
		die();
	}
}

/*
* Check one click checkout permission before trigger request
*/
add_action("wp_ajax_check_oneclick_checkout_permission", "check_oneclick_checkout_permission");
add_action("wp_ajax_nopriv_check_oneclick_checkout_permission", "check_oneclick_checkout_permission");
function check_oneclick_checkout_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_checkout_with_oneclick($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to one click checkout this piece of equipment.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to one click checkout this piece of equipment.")); 
		die();
	}
}


/*
* Check Full Form checkout permission before open the full form request
*/
add_action("wp_ajax_check_full_from_permission", "check_full_from_permission");
add_action("wp_ajax_nopriv_check_full_from_permission", "check_full_from_permission");
function check_full_from_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_checkout_with_fullform($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to full form for checkout this piece of equipment.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to full form for checkout this piece of equipment.")); 
		die();
	}
}


/*  
* Get all avilable/permitted options for checkout
*/
add_action("wp_ajax_get_avilable_checkout_options", "get_avilable_checkout_options");
add_action("wp_ajax_nopriv_get_avilable_checkout_options", "get_avilable_checkout_options");
function get_avilable_checkout_options(){
	$asset_id = $_POST['asset_id'];
	
	$avilableOptions = array();
	

	if( lm_can_checkout_with_oneclick($asset_id) ){
		$avilableOptions[]="one-click-quick-checkout";
	}

	if( lm_can_checkout_with_fastform($asset_id) ){
		$avilableOptions[]="fast-quick-checkout-form";
	}

	if( lm_can_checkout_with_fullform($asset_id) ){
		$avilableOptions[]="full-request-form";
	}


	if(!empty($avilableOptions)){
		echo json_encode( array("code"=>200,"options"=>$avilableOptions,"count"=>count($avilableOptions) ) ); die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to request piece of equipment.")); die();	
	}
	
}


/*
* Check pending request permission before open the request
*/
add_action("wp_ajax_check_pending_request_permission", "check_pending_request_permission");
add_action("wp_ajax_nopriv_check_pending_request_permission", "check_pending_request_permission");
function check_pending_request_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_edit_pending_request($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to edit this request.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"This piece of equipment is pending checkout approval for another user.")); 
		die();
	}
}

/*
* Check checkin permission for quick form
*/
add_action("wp_ajax_check_quick_checkin_permission", "check_quick_checkin_permission");
add_action("wp_ajax_nopriv_check_quick_checkin_permission", "check_quick_checkin_permission");
function check_quick_checkin_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_checkin_with_quickform($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to checkin this item.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"This piece of equipment is checked out by another user.")); 
		die();
	}
}

/*
* Check checkin permission for quick form
*/
add_action("wp_ajax_check_fullform_checkin_permission", "check_fullform_checkin_permission");
add_action("wp_ajax_nopriv_check_fullform_checkin_permission", "check_fullform_checkin_permission");
function check_fullform_checkin_permission(){
	$asset_id = $_POST['asset_id'];
	if(lm_can_checkin_with_fullform($asset_id)){
		echo json_encode(array("code"=>200,"message"=>"You have permission to checkin this item.")); 
		die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"This piece of equipment is checked out by another user.")); 
		die();
	}
}



/*  
* Get all avilable/permitted options for checkin
*/
add_action("wp_ajax_get_avilable_checkin_options", "get_avilable_checkin_options");
add_action("wp_ajax_nopriv_get_avilable_checkin_options", "get_avilable_checkin_options");
function get_avilable_checkin_options(){
	$asset_id = $_POST['asset_id'];
	
	$avilableOptions = array();
	
 
	if( lm_can_checkin_with_quickform($asset_id) ){
		$avilableOptions[]="one-click-quick-checkin";
	}

	if( lm_can_checkin_with_fullform($asset_id) ){
		$avilableOptions[]="full-return-form";
	}
 
	if(!empty($avilableOptions)){
		echo json_encode( array("code"=>200,"options"=>$avilableOptions,"count"=>count($avilableOptions) ) ); die();
	}else{
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to request piece of equipment.")); die();	
	}
	
}