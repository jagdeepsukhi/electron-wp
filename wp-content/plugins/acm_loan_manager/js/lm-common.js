﻿jQuery(document).ready(function(){
   
  
     // Loan Manager Overview Icons action
    jQuery("body").on("click",".loan-action",function(){
      var action    = jQuery(this).data("action");
      

      if(jQuery(this).parent().hasClass('lm-overview')){
          var asset_id  = jQuery("#overview-assest_id").val();
      }else{
          var asset_id  = jQuery(this).parent().next().text();
      }
 
      if(action =="checkout"){ // Retrun actions

        // Check how many ways user can create request?
        jQuery(".form-btn").attr("data-asset-id",asset_id);  
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'get_avilable_checkin_options','asset_id':asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              
                              if(response.count==1){ 
                                  
                                  // If only 1 option is available for checkout, trigger that option direclty
                                  jQuery.each(response.options, function(key,option) {             
                                      jQuery("[data-form-type="+option+"]").trigger("click");   
                                  }); 

                              }else{

                                  // If multiple options are available for checkout, trigger open the option popup
                                  jQuery("#checkout-confirm .form-btn").hide();
                                  jQuery.each(response.options, function(key,option) {             
                                      jQuery("[data-form-type="+option+"]").show();   
                                      jQuery("#checkout-confirm").modal("show");    
                                  });  

                              }
                              jQuery(".lm-loader").hide(); 
                             
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });
      } // endif action = checkout

      if(action =="loan-request"){
         
        // Check how many ways user can create request?
        jQuery(".lm-loader").show();
        jQuery(".form-btn").attr("data-asset-id",asset_id); 
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'get_avilable_checkout_options','asset_id':asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              
                              if(response.count==1){ 
                                  
                                  // If only 1 option is available for checkout, trigger that option direclty
                                  jQuery.each(response.options, function(key,option) {             
                                      jQuery("[data-form-type="+option+"]").trigger("click");   
                                  }); 

                              }else{

                                  // If multiple options are available for checkout, trigger open the option popup
                                  jQuery("#RequestModal .form-btn").hide();
                                  jQuery.each(response.options, function(key,option) {             
                                      jQuery("[data-form-type="+option+"]").show();   
                                      jQuery("#RequestModal").modal("show");    
                                  });  

                              }
                              jQuery(".lm-loader").hide(); 
                             
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });
      }// endif action = loan-request
      if(action =="request-pending"){
        
        // Check do user have permission to recall this item?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_pending_request_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // check if item is alrady recalled, remove message textarea box
                              url = LMURLS.pendingrequestformPageUrl+"/?asset_id="+asset_id;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide(); 
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });     
      } // endif action = request-pending

      if(action =="overview"){
  
        // Check do user have permission to overview page?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_overview_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // check if item is alrady recalled, remove message textarea box
                              url =  LMURLS.overviewPageUrl+"/?asset_id="+asset_id;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide(); 
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });     
      } // endif  action =="overview"

      if(action =="recall"){
        
        var recall_status = jQuery(this).attr("data-recall-status");
        // Check do user have permission to recall this item?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_recall_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // check if item is alrady recalled, remove message textarea box
                              if(recall_status==1){
                                jQuery(".recall-text").html("<p>Item is already recalled. Please submit to revert?</p>");
                              }
                              jQuery("#recallModal").modal("show");
                              jQuery("#recall_asset_id").val(asset_id);
                              jQuery(".lm-loader").hide(); 
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });     
      }




    });

    //Make navigation colum white if request is in past due
    setTimeout(function(){
       jQuery(".loan-navigation").css("background-color","#ffffff");
    },800);


////////////////////Form Actions//////////////////////////////
    
    jQuery("body").on("click",".form-btn",function(){
      var asset_id  = jQuery(this).attr("data-asset-id");
      var formType  = jQuery(this).attr("data-form-type"); 

      if(formType=="full-request-form"){
       
         // Check do user have permission to Fast From request?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_full_from_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // Open Fast Form
                              url = LMURLS.fullformcheckoutPageUrl+"/?asset_id="+asset_id;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide(); 
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });       
      } // endif full-request-form
 
      if(formType=="fast-quick-checkout-form"){
        // Check do user have permission to Fast From request?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_fast_from_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // Open Fast Form
                              jQuery("#fast-checkout-form")[0].reset();
                              jQuery("#fast_asset_id").val(asset_id);

                              jQuery(".warrningMsg").html("");
                              jQuery(".fast-accept ").hide();
                              jQuery(".fast-submit ").show();

                              jQuery("#RequestModal").modal("hide");
                              jQuery("#QiuckFormModal").modal("show"); 
                              jQuery(".lm-loader").hide(); 
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });       
      } // endif fast-quick-checkout-form

      if(formType=="one-click-quick-checkout"){
        // Check do user have permission to one-click From request?
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_oneclick_checkout_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // Trigger One click quick checkout
                              jQuery("#RequestModal").modal("hide");
                              oneclick_quick_checkout(asset_id);
                              //jQuery(".lm-loader").hide();

                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });         
      } // endif one-click-quick-checkout

      if(formType=="one-click-quick-checkin"){  

        jQuery("#checkout-confirm").modal("hide");
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_quick_checkin_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // Trigger quick checkin
                               oneclick_quick_checkin(asset_id);
                              //jQuery(".lm-loader").hide();

                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });         
        
      } // endif one-click-quick-checkin

      if(formType=="full-return-form"){
        
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: {'action':'check_fullform_checkin_permission',asset_id:asset_id},  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                              // open full checkin form
                              url = LMURLS.returnfullformPageUrl+"/?asset_id="+asset_id;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide();

                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Something went wronge. Please try again.", { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide(); 
                     }
               }
          });         
      } // end if formType=="full-return-form"


      if(formType=="confirm-onclick-warrning-checkout"){
          oneclick_quick_checkout(asset_id,1);
      }

      if(formType=="confirm-fast-warrning-checkout"){
          oneclick_quick_checkout(asset_id,1);
      }
       
    });

      // Save form data
      jQuery(".fm-form").submit(function(e){
        e.preventDefault();
        var form  = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                            toastr.success(response.message );  
                            setTimeout(function(){
                               url = LMURLS.loanfrontPageUrl;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide();
                            },2000);
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.");  
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Settings couldn't save."); 
                        jQuery(".lm-loader").hide(); 
                       
                     }
   
               }
          });  
      });
      // add more damage items
      jQuery("body").on("click",".add-more-damage",function(){

        var damageFields = '<tr class="repeate-this"><td><input name="component[]" type=""></td><td><textarea name="description[]"></textarea><input name="damage_item_id[]" class="damage_item" type="hidden"></td><td><a class="remove-damage">x</a></td></tr>';
        jQuery("#damage-table").append(damageFields);

      });

      jQuery("body").on("click",".remove-damage",function(){

        var count= jQuery('#damage-table').find('tr').length;
        if(count>1){

          jQuery(this).parent().parent().remove();
          var need_to_delete = jQuery('.need_to_delete').val();
          var damage_id = jQuery(this).parent().prev().children('.damage_item').val();

          if(need_to_delete=="" && damage_id!=""){

              jQuery('.need_to_delete').val(damage_id);

          }else if(need_to_delete!="" && damage_id!=""){
            
              var array = need_to_delete.split(',');
              array.push(damage_id);
              var str1 = array.toString();
              jQuery('.need_to_delete').val(str1);

          }

        } 

      });

      // Save form data
      jQuery("#form-loan-settings").submit(function(e){
        e.preventDefault();
         
        var form  = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,  
               success: function(res)
               {
                      
                      if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                            toastr.success(response.message );  
                            setTimeout(function(){
                               //url = LMURLS.loanfrontPageUrl;
                             // window.location.href = url;
                            },2000);
                            jQuery(".lm-loader").hide();
                        }else{
                            toastr.error("Something went wronge. Please try again.");  
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        jQuery(".lm-loader").hide();
                        toastr.error("Settings couldn't save.");  
                       
                     }
               }
          });  
      }); 
      // Save form data reason relation
     
      jQuery(" a.refresh-a").on("click",function(e){
         jQuery(".lm-loader").show();
      })
      jQuery("#reason-relation").submit(function(e){
        e.preventDefault();
        var form    = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,  
               success: function(res)
               {
                      
                  if(IsJsonString(res)){
                      var response = jQuery.parseJSON(res);
                      if(response.code ==200){
                          toastr.success(response.message );  
                          setTimeout(function(){
                             //url =LMURLS.loanfrontPageUrl;
                           // window.location.href = url;
                          },2000);
                          jQuery(".lm-loader").hide();
                      }else{
                          toastr.error("Something went wronge. Please try again.");  
                          jQuery(".lm-loader").hide();
                      }
                  }else{
                      jQuery(".lm-loader").hide();
                      toastr.error("Settings couldn't save.");  
                     
                  }
             }
          });  
      });
    jQuery("#location-relation").submit(function(e){
        e.preventDefault();
        var form    = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,  
               success: function(res)
               {
                      
                  if(IsJsonString(res)){
                      var response = jQuery.parseJSON(res);
                      if(response.code ==200){
                          toastr.success(response.message );  
                          setTimeout(function(){
                            //url = LMURLS.loanfrontPageUrl;
                            // window.location.href = url;
                          },2000);
                          jQuery(".lm-loader").hide();
                      }else{
                          toastr.error("Something went wronge. Please try again.");  
                          jQuery(".lm-loader").hide();
                      }
                  }else{
                      jQuery(".lm-loader").hide();
                      toastr.error("Settings couldn't save.");  
                     
                  }
             }
          });  
      });

        // Save form data
      jQuery("#recall-asset").submit(function(e){
        e.preventDefault();
        var form  = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,
               beforeSend: function(){
                jQuery("#recall-submit").text('Submitting..');
                jQuery("#recall-submit").attr('disabled','disabled');
                jQuery("#recall-cancel").attr('disabled','disabled');
               },  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                            toastr.success(response.message );  

                            setTimeout(function(){
                              jQuery("#recallModal").modal('hide');
                              jQuery("#recall-submit").text('Submit');
                              jQuery("#recall-submit").removeAttr('disabled');
                              jQuery("#recall-cancel").removeAttr('disabled');
                              jQuery(".lm-loader").hide();
                              location.reload();
                            },2000);
        
                        }else{
                            jQuery("#recallModal").modal('hide');
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide(); 
                            jQuery("#recall-submit").text('Submit');
                            jQuery("#recall-submit").removeAttr('disabled');
                            jQuery("#recall-cancel").removeAttr('disabled'); 
                        }
                     }else{
                        
                        jQuery("#recallModal").modal('hide');
                        toastr.error("Item cannot recall. Please try again." , { timeOut: 6000 }); 
                        jQuery(".lm-loader").hide();
                        jQuery("#recall-submit").text('Submit');
                        jQuery("#recall-submit").removeAttr('disabled');
                        jQuery("#recall-cancel").removeAttr('disabled'); 
                       
                     }
                     
               }
          });  
      });
      // Save form data
      jQuery("#fast-checkout-form").submit(function(e){
        e.preventDefault();
        var form  = jQuery(this);
        var formData= form.serialize(); 
        jQuery(".lm-loader").show();
        jQuery.ajax({
               type: "POST",
               url: LMURLS.ajaxurl,
               data: formData,  
               success: function(res)
               {
                     if(IsJsonString(res)){
                        var response = jQuery.parseJSON(res);
                        if(response.code ==200){
                            toastr.success(response.message );  
                            setTimeout(function(){
                               url = LMURLS.loanfrontPageUrl;
                              window.location.href = url;
                              //jQuery(".lm-loader").hide();
                            },2000);
                        }else if(response.code ==202){
                            toastr.error(response.message , { timeOut: 6000 }); 
                            jQuery(".lm-loader").hide();  
                        }else if(response.code ==203){
                            jQuery(".fast-accept").show();
                            jQuery(".fast-submit").hide();
                            jQuery(".fast-wrn .warrningMsg").html(response.message); 
                            jQuery(".lm-loader").hide();  
                        }else{
                            toastr.error("Something went wronge. Please try again.");  
                            jQuery(".lm-loader").hide();
                        }
                     }else{
                        
                        toastr.error("Settings couldn't save."); 
                        jQuery(".lm-loader").hide(); 
                       
                     }

               }
          });  
      });

      jQuery("body").on("click",".fast-accept",function(){
        jQuery("#fast_is_confirm").val("1");
      });




      /* Visitors Drop down build */

      jQuery("[data-name=lm_client_name]").change(function(){
        var client  = jQuery(this).val();
        
        if(client!=""){
            var parent  = jQuery('option:selected', this).attr('data-parent');
            var company = jQuery('option:selected', this).attr('data-company');
            var project = jQuery('option:selected', this).attr('data-project');
            if(company!=""){
               jQuery(".lm-wc").remove();
               jQuery("[data-name=lm_client_organization]").val(company);
               jQuery("[data-name=lm_client_organization]").attr("readonly","readonly");
            }else{
                jQuery("[data-name=lm_client_organization]").val("");
                jQuery("[data-name=lm_client_organization]").removeAttr("readonly");
                jQuery("[data-name=lm_client_organization]").parent().append("<p class='lm-wc' style='color:red'>Organization not found, Please type manually.</p>")
            }


            if(project!=""){
               jQuery(".lm-wp").remove();
               jQuery("[data-name=lm_project]").val(project);
               jQuery("[data-name=lm_project]").attr("readonly","readonly");
            }else{
                jQuery("[data-name=lm_project]").removeAttr("readonly");
                jQuery("[data-name=lm_project]").val("");
                jQuery("[data-name=lm_project]").parent().append("<p class='lm-wp' style='color:red'>Project not found, Please type manually.</p>")
            }
 
        }else{
              jQuery(".lm-wc").remove();
              jQuery(".lm-wp").remove();
              jQuery("[data-name=lm_client_organization]").val("");
              jQuery("[data-name=lm_project]").val("");
              jQuery("[data-name=lm_client_organization]").attr("readonly","readonly");
              jQuery("[data-name=lm_project]").attr("readonly","readonly");
        } 
        
      });
});
 




function  oneclick_quick_checkout(asset_id,is_confirmed=null){
  jQuery(".lm-loader").show();
  jQuery.ajax({
         type: "POST",
         url: LMURLS.ajaxurl,
         data: {action:"quick_checkout_callback",asset_id:asset_id,is_confirmed:is_confirmed},  
         success: function(res)
         {
              if(IsJsonString(res)){
                  var response = jQuery.parseJSON(res);
                  if(response.code ==200){
                      toastr.success(response.message );  
                      setTimeout(function(){
                        url = LMURLS.loanfrontPageUrl;
                        window.location.href = url;
                        //jQuery(".lm-loader").hide();
                      },2000);
                  }else if(response.code ==202){
                      // If item is not in service
                      toastr.error(response.message , { timeOut: 6000 }); 
                      jQuery(".lm-loader").hide();  
                  }else if(response.code ==203){
                      jQuery("#confirm-onclick-warrning-checkout").attr("data-asset-id",response.asset_id);
                      jQuery(".warrningMsg").html(response.message);
                      jQuery("#onlcick-checkout-warnningModal").modal("show");
                      jQuery(".lm-loader").hide();  
                  }else{
                      toastr.error("Something went wronge. Please try again."); 
                      jQuery(".lm-loader").hide();
                  }
              }else{
                  jQuery(".lm-loader").hide();
                  toastr.error("Settings couldn't save.");  
                 
              }
               
         }
    }); 
}

function  oneclick_quick_checkin(asset_id){
    jQuery(".lm-loader").show();
   jQuery.ajax({
         type: "POST",
         url: LMURLS.ajaxurl,
         data: {action:"quick_checkin_callback",asset_id:asset_id},  
         success: function(res)
         {
              if(IsJsonString(res)){
                  var response = jQuery.parseJSON(res);
                  if(response.code ==200){
                      toastr.success(response.message );  
                      setTimeout(function(){
                        jQuery(".lm-loader").hide();
                        url = LMURLS.loanfrontPageUrl;
                        window.location.href = url;
                      },2000);
                  }else{
                      toastr.error("Something went wronge. Please try again."); 
                      jQuery(".lm-loader").hide(); 
                  }
              }else{
                  
                  toastr.error("Settings couldn't save.");
                  jQuery(".lm-loader").hide();  
                 
              }

         }
    }); 
}



function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}


function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
}
function checkInArray(value,arr){
  var status = 'Not exist';
 
  for(var i=0; i<arr.length; i++){
    var name = arr[i];
    if(name == value){
      status = 'Exist';
      break;
    }
  }

  return status;
}

jQuery.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};

 