<?php
/*
Plugin Name: ACM Loan Manager
Plugin URI: 
Description: ACM Loan Manager application (This plugin only supports to wpdatatable version 2.1)
Version: 2.1
Author: MB
Author URI:  
*/
////////////////////////////////////////////////////////////////////////*/
/*************Enqueue required script and style**************************/
//////////////////////////////////////////////////////////////////////*/
function lm_enqueue_style() {
	wp_enqueue_style( 'lm-style', plugins_url( 'css/lm-style.css', __FILE__ )); 
	wp_enqueue_script( 'lm-common-js', plugins_url( 'js/lm-common.js', __FILE__ )); 
	wp_enqueue_script( 'lm-boot-js', plugins_url( 'js/bootstrap.min.js', __FILE__ )); 
	wp_localize_script('lm-common-js', 'LMURLS', 
						array( 
							'siteurl' => get_option('siteurl'),
							'loanfrontPageUrl' => get_permalink(get_option('_lm_page_loan_front')),
							'overviewPageUrl' => get_permalink(get_option('_lm_page_loan_overview')),
							'fullformcheckoutPageUrl' => get_permalink(get_option('_lm_page_full_form_checkout')),
							'pendingrequestformPageUrl' => get_permalink(get_option('_lm_page_pending_request_form')),
							'returnfullformPageUrl' => get_permalink(get_option('_lm_page_return_full_form')),
							'ajaxurl'=> admin_url('admin-ajax.php') 

							)
					);
}
add_action( 'wp_enqueue_scripts', 'lm_enqueue_style',11,0 );

// include all required files
require 'acm_lm_notifications.php';
require 'acm_lm_crons.php';
require 'acm_mm_changes.php';
require 'acm_lm_popups.php';
require 'acm_lm_capabilities.php';
require 'import-data/lm_import.php';



////////////////////////////////////////////////////////////////////////*/
/*************Check ACM Maintence Manger Dependency**************************/
//////////////////////////////////////////////////////////////////////*/
function lm_anspress_activate( $network_wide ) {
    //replace this with your dependent plugin
    $category_ext = 'acm_maintenance_manager/acm_maintenance_manager.php';
    $category_error = false;
    if(!file_exists(WP_PLUGIN_DIR.'/'.$category_ext)){
		$error = true;     
    }else if ( !is_plugin_active( $category_ext ) ) {
		  $error = true;
	}    

    if ( $error ) {
    	deactivate_plugins( plugin_basename( __FILE__ ) );
        wp_die( __( 'Please install and Activate ACM Mainatenace Mananger Plugin.', 'woocommerce-addon-slug' ), 'Plugin dependency check', array( 'back_link' => true ) );    
    }
    // if configuration is done only menu update is pending
    if(get_option( '_en_lm_add_caps')==1){
    		set_transient( 'acm_loan_manager_menu_update', true, 5 ); 
	}
}

register_activation_hook(__FILE__, 'lm_anspress_activate');


function lm_check_runtime_dependency(){
	//replace this with your dependent plugin
    $category_ext = 'acm_maintenance_manager/acm_maintenance_manager.php';
    $category_error = false;
    if(!file_exists(WP_PLUGIN_DIR.'/'.$category_ext)){
		$error = true;     
    }else if ( !is_plugin_active( $category_ext ) ) {
		  $error = true;
	}    

    if ( $error ) {
    	
    	set_transient( 'acm_maintenance_manager_deactivated', true, 5 ); 
        deactivate_plugins( plugin_basename( __FILE__ ) );	  
    }
}
add_action( 'admin_notices',"lm_disabled_notice");
function lm_disabled_notice() {
	if( get_transient( 'acm_maintenance_manager_deactivated' ) ){
            
    	echo '<div class="notice notice-warning is-dismissible error">
             	<p><strong>ACM Loan Manager requires the ACM Maintenance Mananger Plugin to be activated!</strong></p>
         		</div>'; 	
     	delete_transient( 'acm_maintenance_manager_deactivated' );
    }

    if( get_transient( 'acm_loan_manager_menu_update' ) ){
         
        $settings_link = '<a href="admin.php?page=loan-settings">'.__( 'Configurations' ).'</a>'; 
    	echo '<div class="notice notice-warning is-dismissible warning">
             	<p><strong>ACM Loan Manager</strong> plugin activated and reqiure the update menu from <strong>'.$settings_link.'</strong>!</p>
         		</div>'; 	
     	delete_transient( 'acm_loan_manager_menu_update' );
    }
} 
add_action( 'admin_init', 'lm_check_runtime_dependency'  ); 



/***********************Plugin Activation/Deactivation hook************************/
function lm_plugin_deactivation() {
	if(get_option("_lm_update_menu")==1){
		lm_delete_loan_menu_items();
    }	

}
register_deactivation_hook( __FILE__, 'lm_plugin_deactivation' );


/***********************Plugin Configuration link ************************/
add_filter("plugin_action_links_".plugin_basename(__FILE__), 'lm_plugin_add_settings_link');

// addd link to configrations page under plugin listing
function lm_plugin_add_settings_link( $links ) {

    $settings_link = '<a href="admin.php?page=loan-settings">'.__( 'Configurations' ).'</a>';
    array_push( $links, $settings_link );

    return $links;

}
 

////////////////////////////////////////////////////////////////////////*/
/***********************Register admin menu*****************************/
//////////////////////////////////////////////////////////////////////*/
function en_lm_admin_menu()
{
    //add_menu_page('Electron', 'Electron', 'manage_options', 'en-admin');
    add_submenu_page('en-admin', 'ACM WP Data Tables Relation', 'ACM Loan Manager', 'manage_options', 'loan-settings','en_lm_admin_setting_page');
}

if ( is_admin() ) {
    add_action('admin_menu', 'en_lm_admin_menu',11);
}

function en_lm_admin_setting_page(){
	if(!empty($_POST) && isset($_POST['submit_wptables'])){
		foreach (LM_DATATABELS as $slug => $tableName) {
			update_option( $slug, $_POST[$slug]);
		}
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}

	if(!empty($_POST) && isset($_POST['submit_email_templates'])){
		foreach (LM_EMAIL_TEMPLATES as $slug => $tableName) {
			update_option( $slug, $_POST[$slug]);
		}
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}

	if(!empty($_POST) && isset($_POST['submit_wpforms'])){
		foreach (LM_WPFORMS as $slug => $tableName) {
			update_option( $slug, $_POST[$slug]);
		}
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}

	if(!empty($_POST) && isset($_POST['submit_pages'])){	 
		foreach (LM_PAGES as $option_key => $page) {	
			update_option( $option_key, $_POST[$option_key]);	
		}
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}


	// load view

	if($_GET['action']=='import_source_tables'){

		$response = lm_import_source_tables();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='alter_table'){
		
		$response = lm_alter_equipment_main_table();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='wpdatatable_import'){
		
		$response = lm_import_wp_data_tables();
		lm_refresh_wp_data_tables();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='create_forms'){
		
		$response = lm_import_wp_formmaker_data();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='create_pages'){
		
		$response = lm_create_required_pages();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='import_email'){
		
		$response = lm_create_email_templates();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='update_menu'){

		$response = lm_update_frontend_menu();
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='add_caps'){

		$response = lm_add_capability_for_single_role('administrator');
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}elseif($_GET['action']=='remove_loan_application'){

		$response = lm_remove_loan_application();
		
		include(dirname(__FILE__)."/admin-templates/lm_configartion.php");

	}else{
		include(dirname(__FILE__)."/admin-templates/lm_admin_settings.php");
	}
 
}

////////////////////////////////////////////////////////////////////////*/
/*****************Loan Mananger Front Page Shortcode*******************/
//////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_front_table","en_lm_asset_loan_front_page_shortcode_callback");
function en_lm_asset_loan_front_page_shortcode_callback(){
	ob_start();
	include 'shortcode-templates/lm_front_table.php';
	return ob_get_clean();
}


////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger All Loan Request Overview Shortcode**************/
//////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_requests_overview","en_lm_requests_overview_shortcode_callback");
function en_lm_requests_overview_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_requests_overview.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger MObile Menu Shortcode**************/
//////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_mobilemenu","en_lm_en_lm_mobilemenu_callback");
function en_lm_en_lm_mobilemenu_callback(){
	ob_start();
	include 'shortcode-templates/lm_mobilemenu.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger Reports Page Shortcode**************/
//////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_reports","en_lm_reports_shortcode_callback");
function en_lm_reports_shortcode_callback(){
	ob_start();
	include 'shortcode-templates/lm_reports.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/****************Loan Mananger status_master Shortcode******************/
//////////////////////////////////////////////////////////////////////*/

add_shortcode("en_lm_loan_status_master","en_lm_loan_status_master_shortcode_callback");
function en_lm_loan_status_master_shortcode_callback(){
 
  	ob_start();
	include 'shortcode-templates/lm_status_table.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/****************Loan Mananger locations Shortcode******************/
//////////////////////////////////////////////////////////////////////*/

add_shortcode("en_lm_loan_locations_master","en_lm_loan_locations_master_shortcode_callback");
function en_lm_loan_locations_master_shortcode_callback(){

  	ob_start();
	include 'shortcode-templates/lm_locations_table.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/********************Loan Mananger resons Shortcode*********************/
//////////////////////////////////////////////////////////////////////*/

add_shortcode("en_lm_loan_reason_master","en_lm_loan_reason_master_shortcode_callback");
function en_lm_loan_reason_master_shortcode_callback(){
  ob_start();
	include 'shortcode-templates/lm_reasons_table.php';
	return ob_get_clean();
}



 ////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger Return Request full form shortcode****************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_return_equipment_form","en_lm_loan_return_equipment_form_shortcode_callback");
function en_lm_loan_return_equipment_form_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_return_equipment_form.php';
	return ob_get_clean();
}
 
////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger Request full form shortcode***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_request_full_form","en_lm_loan_request_full_form_shortcode_callback");
function en_lm_loan_request_full_form_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_request_full_form.php';
	return ob_get_clean();
}
////////////////////////////////////////////////////////////////////////*/
/********Loan Mananger Request full form shortcode***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_request_quick_form","en_lm_loan_request_quick_form_shortcode_callback");
function en_lm_loan_request_quick_form_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_request_quick_form.php';
	return ob_get_clean();
}


////////////////////////////////////////////////////////////////////////*/
/**************Loan pending request form shortcode***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_pending_request_form","en_lm_loan_pending_request_form_shortcode_callback");
function en_lm_loan_pending_request_form_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_pending_request_form.php';
	return ob_get_clean();
}


////////////////////////////////////////////////////////////////////////*/
/****************Loan Mananger Settings shortcode***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_settings","en_lm_loan_settings_shortcode_callback");
function en_lm_loan_settings_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm-settings.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/****************loan-request-direct-approve-deny/***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_request_direct_approve_or_deny","en_lm_loan_request_direct_approve_or_deny_shortcode_callback");
function en_lm_loan_request_direct_approve_or_deny_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm-direct-approve-deny.php';
	return ob_get_clean();
}


////////////////////////////////////////////////////////////////////////*/
/****************loan-request-direct-approve-deny/***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_loan_equipment_individual_settings","en_lm_loan_equipment_individual_settings_shortcode_callback");
function en_lm_loan_equipment_individual_settings_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm-equipment-individual-setting.php';
	return ob_get_clean();
}

////////////////////////////////////////////////////////////////////////*/
/****************loan-request-direct-approve-deny/***********************/
///////////////////////////////////////////////////////////////////////*/
add_shortcode("en_lm_history_audit","en_lm_history_audit_shortcode_callback");
function en_lm_history_audit_shortcode_callback(){
	
	ob_start();
	include 'shortcode-templates/lm_history.php';
	return ob_get_clean();
}

/*
*Create loan request / checkout with full Form AJAX callback
*/
add_action("wp_ajax_save_loan_checkout_full_form", "save_loan_checkout_full_form_callback");
add_action("wp_ajax_nopriv_save_loan_checkout_full_form", "save_loan_checkout_full_form_callback");
function save_loan_checkout_full_form_callback(){
 	global $wpdb;
 		
	$data 								= array();
	$data['asset_id'] 					= $asset_id = $_POST['lm_asset_id'];
	$data['date_of_checkout'] 			= $_POST['lm_date_of_checkout'];
	$data['requested_return_date'] 		= date("Y-m-d", strtotime($_POST['lm_date_of_return']));
	$data['odometer_reading_checkout'] 	= $_POST['lm_odometer_reading'];
	$data['usage_location'] 			= $_POST['lm_usage_location'];
	$data['reason'] 					= $_POST['lm_reason'];
	$data['loan_policy'] 				= (isset($_POST['lm_policy'])) ? $_POST['lm_policy'] : '';
	$data['approve_status'] 			= $_POST['lm_request_status'];
	$data['checking_for_client']= (isset($_POST['lm_checking_for_client'])) ? $_POST['lm_checking_for_client'] : 'No' ;
	$data['modify_by'] 					= get_current_user_id();
 	$data['modify_date'] 				= date("Y-m-d H:i:s");

	if($data['checking_for_client']=="Yes"){
		$data['client_name'] 		= $_POST['lm_client_name'];
		$data['organization'] 		= $_POST['lm_client_organization'];
		$data['project'] 			= $_POST['lm_project'];
	}

 	$request_id 		= $_POST['request_id']; // request ID
 	
 	
 	// Create new reuest
 	if(empty($request_id)){

 		// Skip confirmation process if authorization not reqiured.
		$Auth_required = lm_is_authorization_required($asset_id);

		if($Auth_required=='Yes'){
			$approveArr = lm_return_all_approvers($asset_id);

			// If current user is approver also, No need to follow confirmation process.
			if(in_array(get_current_user_id(), $approveArr)){
				$loan_status 				= 2; // Checked out
				$data['approved_deny_by'] 	= get_current_user_id(); //Set current requester as reviewer
				$message []= "Item has been approved and checked out.";
			}else{
				$loan_status = 1; //Pending
				$message []= "Check out request has been generated.";
			}
		}else{
			$loan_status = 2; //Checked out 
			$message []= "Item has been approved and checked out."; 
		}

 		
 		$data['created_by'] 		= get_current_user_id();
 		$data['created_date'] 		= date("Y-m-d H:i:s");
 		$data['check_in_out_status']= $loan_status;
 		$data['check_out_type'] 	= "Full Form";
 		
 		$wpdb->insert("en_lm_loan_requests", $data );
		$request_id = $wpdb->insert_id;
		$components 	= $_POST['component'];
		$descriptions 	= $_POST['description'];
		$damage_item_ids= $_POST['damage_item_id'];
		$i=0;
		foreach ($components as $key => $component) {
			if(!empty($component)){
					$meta = array(
									"request_id"			=> $request_id,
									"component_name"		=> $component,
									"component_description"	=> $descriptions[$i],
									"created_by"			=> get_current_user_id(),
									"modify_by"				=> get_current_user_id(),
									"created_date"			=> date("Y-m-d H:i:s"),
									"modify_date"			=> date("Y-m-d H:i:s"),
									"is_additional"			=> 0,
								);
					if(!empty($damage_item_ids[$i])){
						$meta['is_old'] = 1 ;
					}else{
						$meta['is_old'] = 0 ;
					}


					$wpdb->insert("en_lm_loan_requests_meta", $meta);	

			}
		
			$i++;
		} // end foreach

		// Send notification to approvers if authorization is required
		if($Auth_required=='Yes'){
			if($loan_status == 1){

				lm_send_new_request_notification_to_approvers($asset_id);
				lm_send_notification_loan_request_confirmation($asset_id);
			}
			
		}


 	}else{
 		// Update previous request

 		if($data['approve_status'] == 6){ // Approved
			$data['approved_deny_by'] 		= get_current_user_id();
			$data['common_notes']			= $_POST['approve_deny_note'];
			$data["request_token"	]	= ''; //make empty token
			$loan_status 					= 2; 	//Checked out  
			$message []= "Item has been approved and checked out.";
			

		}else if($data['approve_status']==4){ 	// Deny
			$data['approved_deny_by'] 		= get_current_user_id();
			$data['common_notes']			= $_POST['approve_deny_note']; 
			$data["request_token"	]	= ''; //make empty token
			$loan_status 					= 4;  	// Make status back to Checked-in in equipment table
			$message []						= "Request has been denied.";
			
		}else if($data['approve_status']==5){ 	// canceled
			$data['approved_deny_by'] 		= get_current_user_id();
			$data['common_notes']			= $_POST['approve_deny_note']; 
			$loan_status 					= 5;  	//Deny
			$data["request_token"	]	= ''; //make empty token
			$message []						= "Request has been canceled.";
		}else{
			$loan_status = 1;   // Pending
			$message []						= "Request has been updated.";
		}
		$data['check_in_out_status']= $loan_status;
		
 		$wpdb->update("en_lm_loan_requests", $data, array("id"=>$request_id ) );

 		$components 	= $_POST['component'];
		$descriptions 	= $_POST['description'];
		$damage_item_ids= $_POST['damage_item_id'];
		$i=0;
		foreach ($components as $key => $component) {
			if(!empty($component)){
					$meta = array(
									"request_id"			=> $request_id,
									"component_name"		=> $component,
									"component_description"	=> $descriptions[$i],
									"modify_by"				=> get_current_user_id(),
									"modify_date"			=> date("Y-m-d H:i:s"),
									"is_additional"			=> 0,
								);
					if(empty($damage_item_ids[$i])){
						$meta['is_old'] 		= 0;
						$meta['created_by'] 	= get_current_user_id();
						$meta['created_date'] 	= date("Y-m-d H:i:s");
						$wpdb->insert("en_lm_loan_requests_meta", $meta);	
					}else{
						$wpdb->update("en_lm_loan_requests_meta", $meta, array("id"=>$damage_item_ids[$i]));
					}		
			}
		
			$i++;
		} // end foreach

		//Send notification on request approve/denied
		if($data['approve_status'] == 6){
			lm_send_notification_loan_request_approved($asset_id); 
		}else if($data['approve_status'] == 4){
			lm_send_notification_loan_request_denied($asset_id);
		}
 	}
	
	 
	// Update Facility Equipment Main Table
	lm_update_facility_equipment_table($asset_id);
	echo json_encode(array("code"=>200,"message"=>implode(" ", $message))); die();
}


/*
* Create loan request with Fast Form AJAX Call back.
*/
add_action("wp_ajax_save_loan_checkout_quick_form", "save_loan_checkout_quick_form_callback");
add_action("wp_ajax_nopriv_save_loan_checkout_quick_form", "save_loan_checkout_quick_form_callback");
function save_loan_checkout_quick_form_callback(){
 	global $wpdb;

 	$asset_id 		= $_POST['asset_id'];
 	$is_confirmed 	= $_POST['is_confirmed'];
 	$message  		= array();

 	// if mainatenace status not in service, item cannot be checkout
 	$maintenance_status = lm_check_maintenance_status($asset_id);  
 	if($maintenance_status!=1){

		echo json_encode(array("code"=>202,"message"=>"This asset cannot be checked out at this time because it is not in service.  Please reach out to the manager of this equipment to learn when it will be back in service.")); 
		die();
 	}


 	$checkinDays 		= lm_expedited_Return_days($asset_id);
 	$requested_checkin 	= date('Y-m-d', strtotime('+ '.$checkinDays.' days'));
 	
 	// Get next upcoming event
	$next_event = lm_nearest_effective_event_date($asset_id);  
 	if($requested_checkin >= $next_event && strtotime($next_event) > 0) {

 		$requested_checkin = $next_event;
 		if( $is_confirmed !=1 ){
			echo json_encode(array("code"=>203,"asset_id"=>$asset_id,"message"=>"This asset is nearing a service appointment and therefore must be returned by ".$requested_checkin.". Please click “Accept” to continue with the asset checkout.")); 
			die();
 		}
 		$message [] = "This is scheduled for an event on the same day so item must be return on $requested_checkin.";
 	}


	$data 							= array();
	$data['asset_id'] 				= $asset_id;
	$data['date_of_checkout'] 		= date("Y-m-d");
	$data['requested_return_date'] 	= $requested_checkin; 
	//$data['usage_location'] 		= 4;  // Quick Checkout
	//$data['reason'] 				= $_POST['wdform_20_element29'];
	$data['checking_for_client']	= "Yes";
	$data['modify_by'] 				= get_current_user_id();
 	$data['modify_date'] 			= date("Y-m-d H:i:s");
	$data['client_name'] 			= $_POST['client_name'];
	$data['organization'] 			= $_POST['client_organization'];
	$data['project'] 				= $_POST['project'];
	$data['check_out_type'] 		= "Fast Form";

	// Skip confirmation process if authorization not reqiured.
	$Auth_required 		= lm_is_authorization_required($asset_id);
	if($Auth_required=='Yes'){
		$approveArr 	= lm_return_all_approvers($asset_id);

		// If current user is approver also, No need to follow confirmation process.
		if(in_array(get_current_user_id(), $approveArr)){
			$loan_status 				= 2; // Checked out
			$data['approve_status'] 	= 6;// Approved
			$data['approved_deny_by'] 	= get_current_user_id(); //Set current requester as reviewer
			$message []= "Item has been approved and checked out."; 
		}else{
			$loan_status = 1; //Pending
			$message []= "Check out request has been generated and waiting for approval.";
		} 
	}else{
		$loan_status = 2; //Checked out  
		$message []= "Item has been approved and checked out."; 
	}
	 
	$data['created_by'] 			= get_current_user_id();
	$data['created_date'] 			= date("Y-m-d H:i:s");
	$data['check_in_out_status']	= $loan_status;
	$data['loan_policy'] 			= 'Yes';
	
	$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
	
	$wpdb->insert("en_lm_loan_requests", $data );
	
	$request_id = $wpdb->insert_id;
	
	$loanRequestMeta 	= $loanData["loanRequestMeta"];

	foreach ($loanRequestMeta as $key => $component) {
		if(!empty($component)){

				$meta = array(
								"request_id"			=> $request_id,
								"component_name"		=> $component->component_name,
								"component_description"	=> $component->component_description,
								"created_by"			=> get_current_user_id(),
								"modify_by"				=> get_current_user_id(),
								"created_date"			=> date("Y-m-d H:i:s"),
								"modify_date"			=> date("Y-m-d H:i:s"),
								"is_additional"			=> 0,
								"is_old"				=> 1
							);
		
				$wpdb->insert("en_lm_loan_requests_meta", $meta);	
		}
	
		$i++;
	} // end foreach
 

	// Send notification to approvers if authorization is required
	if($Auth_required=='Yes'){
		if($loan_status == 1){
				lm_send_new_request_notification_to_approvers($asset_id);
				lm_send_notification_loan_request_confirmation($asset_id);
		}
		 
		
	}

	// Update loan navigation
	lm_update_facility_equipment_table($asset_id);
	echo json_encode(array("code"=>200,"message"=>implode("", $message) )); die();
}


/*
* Create loan request with one click button form AJAX Call back
*/
add_action("wp_ajax_quick_checkout_callback", "save_loan_quick_checkout_callback");
add_action("wp_ajax_nopriv_quick_checkout_callback", "save_loan_quick_checkout_callback");
function save_loan_quick_checkout_callback(){
 	global $wpdb;

 	$asset_id 		= $_POST['asset_id'];
 	$is_confirmed 	= @$_POST['is_confirmed'];
 	$message  		= array();
 	// if mainatenace status not in service, item cannot be checkout
 	$maintenance_status = lm_check_maintenance_status($asset_id);
 	if($maintenance_status!=1){

		echo json_encode(array("code"=>202,"message"=>"This asset cannot be checked out at this time because it is not in service.  Please reach out to the manager of this equipment to learn when it will be back in service.")); 
		die();
 	}

 	$checkinDays 		= lm_expedited_Return_days($asset_id);
 	$requested_checkin 	= date('Y-m-d', strtotime('+ '.$checkinDays.' days'));
 	
 	// Get next upcoming event
	$next_event = lm_nearest_effective_event_date($asset_id);  
 	if($requested_checkin >= $next_event && strtotime($next_event) > 0) {

 		$requested_checkin = $next_event;
 		if( $is_confirmed !=1 ){
			echo json_encode(array("code"=>203,"asset_id"=>$asset_id,"message"=>"This asset is nearing a service appointment and therefore must be returned by ".$requested_checkin.". Please click “Accept” to continue with the asset checkout.")); 
			die();
 		}
 		$message [] = "This is scheduled for an event on the same day so item must be return on $requested_checkin.";
 	}


	$data 							= array();
	$data['asset_id'] 				= $asset_id;
	$data['date_of_checkout'] 		= date("Y-m-d");
	$data['requested_return_date'] 	= $requested_checkin; 
	//$data['usage_location'] 		= 4; //Quick Checkout
	$data['checking_for_client']	= "No";
	$data['modify_by'] 				= get_current_user_id();
 	$data['modify_date'] 			= date("Y-m-d H:i:s");
 	
 	// Skip confirmation process if authorization not reqiured.
 	$Auth_required = lm_is_authorization_required($asset_id);
	if($Auth_required=='Yes'){
		$approveArr = lm_return_all_approvers($asset_id);

		// If current user is approver also, No need to follow confirmation process.
		if(in_array(get_current_user_id(), $approveArr)){
			$loan_status 				= 2; // Checked out
			$data['approve_status'] 	= 6;// Approved
			$data['approved_deny_by'] 	= get_current_user_id(); //Set current requester as reviewer
			$message []= "Item has been approved and checked out."; 
		}else{
			$loan_status 				= 1; //Pending
			$message []= "Check out request has been generated and waiting for approval";
		}
	}else{
		$loan_status 				= 2; //Checked out 
		$message []= "Item has been approved and checked out."; 
	} 
	  
	$data['created_by'] 			= get_current_user_id();
	$data['created_date'] 			= date("Y-m-d H:i:s");
	$data['check_in_out_status']	= $loan_status;
	$data['check_out_type'] 		= "One Click";
	$data['loan_policy'] 			= 'Yes';
	
	$loanData   = lm_get_latest_loan_request_by_asset_id($asset_id);
	
	$wpdb->insert("en_lm_loan_requests", $data );
	
	$request_id = $wpdb->insert_id;

	$loanRequestMeta 	= $loanData["loanRequestMeta"];

	foreach ($loanRequestMeta as $key => $component) {
		if(!empty($component)){

				$meta = array(
								"request_id"			=> $request_id,
								"component_name"		=> $component->component_name,
								"component_description"	=> $component->component_description,
								"created_by"			=> get_current_user_id(),
								"modify_by"				=> get_current_user_id(),
								"created_date"			=> date("Y-m-d H:i:s"),
								"modify_date"			=> date("Y-m-d H:i:s"),
								"is_additional"			=> 0,
								"is_old"				=> 1
							);

				$wpdb->insert("en_lm_loan_requests_meta", $meta);	
		}
	
		$i++;
	} // end foreach
 
	// Send notification to approvers if authorization is required
	if($Auth_required=='Yes'){
		if($loan_status == 1){
				lm_send_new_request_notification_to_approvers($asset_id);
				lm_send_notification_loan_request_confirmation($asset_id);
		}
		
	}

	// Update loan navigation
	lm_update_facility_equipment_table($asset_id);
	
	echo json_encode(array("code"=>200,"message"=>implode(" ", $message))); die();

}



/*
* Checkin/Return back loan request with one click button AJAX callback
*/
add_action("wp_ajax_quick_checkin_callback", "save_loan_quick_checkin_callback");
add_action("wp_ajax_nopriv_quick_checkin_callback", "save_loan_quick_checkin_callback");
function save_loan_quick_checkin_callback(){
 	global $wpdb;

 	$asset_id = $_POST['asset_id'];
 
  	$loan_status = 3; // Checkin
	$data 							= array();
	$data['asset_id']				= $asset_id;
	$data['actual_return_date'] 	= date("Y-m-d");
	$data['odometer_reading_return']= '';
	$data['additional_damage'] 		= '';
	$data['experience_issue'] 		= '';
	$data['issue_to_manager'] 		= '';
	$data['checkin_by'] 			= get_current_user_id();
	$data['modify_by'] 				= get_current_user_id();
 	$data['modify_date'] 			= date("Y-m-d H:i:s");
	$data['check_in_out_status']	= $loan_status;
	$data['check_in_type'] 			= "One Click/Quick Check in";

	$loanData   	= lm_get_latest_loan_request_by_asset_id($asset_id);
	$request_id 	= $loanData['loanRequest']->id;
	$requestor_id 	= $loanData['loanRequest']->created_by;

	$wpdb->update("en_lm_loan_requests", $data, array("id"=>$request_id ) );

	//Update Facility Equipment Table
	lm_update_facility_equipment_table($asset_id);
	if($requestor_id != get_current_user_id()){
		// Send notification on asset return 
		lm_send_notification_on_asset_return($asset_id);
	}
	

	//remove reminder items from reminder table.
	lm_flush_reminder_table($asset_id);

	echo json_encode(array("code"=>200,"message"=>"Item has been quick checked-In.")); die();
}

/*
* Checkin / Return back loan request with Full form AJAX callback
*/
add_action("wp_ajax_save_loan_equipment_return", "save_loan_equipment_return_form_callback");
add_action("wp_ajax_nopriv_save_loan_equipment_return", "save_loan_equipment_return_form_callback");
function save_loan_equipment_return_form_callback(){
 	global $wpdb;
 	
 	$loan_status = 3; // Checked in
	$data 							= array();
	$data['asset_id'] 				= $asset_id = $_POST['lm_asset_id'];
	$request_id 					= $_POST['lm_request_id'];
	$data['date_of_checkout'] 		= $_POST['lm_date_of_checkout'];
	$data['actual_return_date'] 	= date("Y-m-d", strtotime($_POST['lm_date_of_return']));
	$data['odometer_reading_return']= $_POST['lm_odometer_reading_return'];
	$data['checkin_location'] 		= $_POST['lm_checkin_location'];
	$data['experience_issue'] 		= (isset($_POST['lm_experience_issue'])) ? $_POST['lm_experience_issue'] : 'No';
	$data['issue_to_manager'] 		= (isset($_POST['lm_issue_to_manager'])) ? $_POST['lm_experience_issue'] : 'No';
	$data['additional_damage'] 		= (isset($_POST['lm_additional_damage'])) ? $_POST['lm_experience_issue'] : 'No';
	$data['experience_note'] 		= $_POST['lm_experience_note'];
	$data['modify_by'] 				= get_current_user_id();
	$data['checkin_by'] 			= get_current_user_id();
 	$data['modify_date'] 			= date("Y-m-d H:i:s");
	$data['check_in_out_status']	= $loan_status;
	$data['check_in_type'] 			= "Full Form";
 		
 	$wpdb->update("en_lm_loan_requests", $data, array("id"=>$request_id ) );

 	$components 	= $_POST['component'];
	$descriptions 	= $_POST['description'];
	$damage_item_ids= $_POST['damage_item_id'];
	$need_to_delete = $_POST['need_to_delete'];
	$i=0;
	foreach ($components as $key => $component) {
		if(!empty($component)){
			$meta = array(
						"request_id"			=> $request_id,
						"component_name"		=> $component,
						"component_description"	=> $descriptions[$i],
						"modify_by"				=> get_current_user_id(),
						"modify_date"			=> date("Y-m-d H:i:s"),
						);
			if(empty($damage_item_ids[$i])){
				$meta['is_additional'] 	= 1;
				$meta['created_by'] 	= get_current_user_id();
				$meta['created_date'] 	= date("Y-m-d H:i:s");
				$wpdb->insert("en_lm_loan_requests_meta", $meta);	
			}else{
				$wpdb->update("en_lm_loan_requests_meta", $meta, array("id"=>$damage_item_ids[$i]));
			}		
		}
		$i++;
	} // end foreach

	// delete from damage items
	if(!empty($need_to_delete)){
		$wpdb->query("DELETE FROM en_lm_loan_requests_meta WHERE id IN ('$need_to_delete')");
	}

	//Update Facility Equipment Table
	lm_update_facility_equipment_table($asset_id);


	$loanData   	= lm_get_latest_loan_request_by_asset_id($asset_id);
	$requestor_id 	= $loanData['loanRequest']->created_by;
	if($requestor_id!=get_current_user_id()){
		// Send notification on asset return 
		lm_send_notification_on_asset_return($asset_id);
	}
	

	// Send report damage issue to maintenance managers
	if($data['issue_to_manager']=="Yes"){		
		lm_send_notification_damage_reported($asset_id);
	}

	//remove reminder items from reminder table.
	lm_flush_reminder_table($asset_id);

	echo json_encode(array("code"=>200,"message"=>"Item has been checked-In.")); die();
}

/*
* Recall a loan request AJAX callback
*/
add_action("wp_ajax_recall_asset", "recall_asset_form_callback");
add_action("wp_ajax_nopriv_recall_asset", "recall_asset_form_callback");
function recall_asset_form_callback(){
	
 	global $wpdb;

	$asset_id 	= $_POST['recall_asset_id'];
	$reason 	= $_POST['msg_to_current_user'];

	if(!lm_can_recall( $asset_id)){
		echo json_encode(array("code"=>202,"message"=>"You do not have permission to recall this piece of equipment.")); 
		die();
	}


	$loanData 	= lm_get_loan_request_display_data_by_asset_id($asset_id);
 	$assetData  = lm_get_equipment_by_asset_id($asset_id);

 	if($assetData->loan_status==2){
 		
 		// update request status to recalled
	 	$loan_status = 8; // Recalled
		$navigation  = lm_generate_nav_icons($loan_status); 
		$updateData["loan_navigation"]	= $navigation; 
		$updateData["loan_status"]		= $loan_status; 
	 	lm_send_recall_email_to_requestor($asset_id,$reason);
		
		$wpdb->update("en_mb_facility_equipment",$updateData, array("asset_id"=>$asset_id));
		echo json_encode(array("code"=>200,"message"=>"Item has been re-called.")); die();

 	}else if($assetData->loan_status==8){

 		// update request status to checkout on again recall
	 	$loan_status = 2; // checkout
		$navigation  = lm_generate_nav_icons($loan_status); 
		$updateData["loan_navigation"]	= $navigation; 
		$updateData["loan_status"]		= $loan_status; 
		
		$wpdb->update("en_mb_facility_equipment",$updateData, array("asset_id"=>$asset_id));
		echo json_encode(array("code"=>200,"message"=>"Item status has been updated.")); die();
 	}else{
 		echo json_encode(array("code"=>202,"message"=>"Item cannot recall.")); die();
 	}
 	
 	
}



///////////////////////////////////////////////////////////////////////*/
//Save loan_settings

add_action("wp_ajax_save_loan_settings", "save_loan_settings_callback");
add_action("wp_ajax_nopriv_save_loan_settings", "save_loan_settings_callback");
function save_loan_settings_callback(){
 	global $wpdb;
 	$equipment_types = $_POST['equipment_type'];
 	foreach ($equipment_types as $key => $type_id) {
 		 
 		$data 							= array();
 		$data['equipment_type']			= $type_id;
 		$data['request_approvers']		= implode(",", $_POST['request_approvers'][$type_id]);
 		$data['max_loan_period']		= $_POST['max_loan_period'][$type_id];
 		$data['advance_return_reminder']		= $_POST['advance_return_reminder'][$type_id];
 		$data['return_ad_next_sch_event_days']	= $_POST['return_ad_next_sch_event_days'][$type_id];
 		$data['return_ad_next_sch_event_miles']	= $_POST['return_ad_next_sch_event_miles'][$type_id];
 		$data['expedited_checkout_duration']	= $_POST['expedited_checkout_duration'][$type_id];
 		$data['enable_asset_loan']				= $_POST['enable_asset_loan'][$type_id];
 		$data['auth_required_for_checkout']		= $_POST['auth_required_for_checkout'][$type_id];
 		$data['return_reminder_email_frequency']= $_POST['return_reminder_email_frequency'][$type_id];
 		$data['enable_reminder_email']			= (isset($_POST['enable_reminder_email'][$type_id])) ? '1' : '0';
 		
 		$data['modify_by']						= get_current_user_id();
 		$data['modify_on']						= date("Y-m-d H:i:s");

  
 		$getRow = $wpdb->get_row("SELECT equipment_type from en_lm_loan_equipment_settings WHERE equipment_type='$type_id'");
 	 
 		if(empty($getRow)){
			$wpdb->insert("en_lm_loan_equipment_settings", $data );
  
		}else{
			$wpdb->update("en_lm_loan_equipment_settings",$data, array("equipment_type"=>$type_id) );
		}
		update_option("lm_loan_policy_text",$_POST['loan_policy_text']);	
 	}  
	echo json_encode( array("code"=>200,"message"=>"Settings has been updated.") ); die();
}


///////////////////////////////////////////////////////////////////////*/
//Save Reason table relation
add_action("wp_ajax_save_reason_relation", "save_reason_relation_callback");
add_action("wp_ajax_nopriv_save_reason_relation", "save_reason_relation_callback");
function save_reason_relation_callback(){
 
	
 	global $wpdb;
 	$supportsArr = $_POST['support_types'];

 	$sql      = "SELECT * FROM `en_lm_loan_reasons` "; 
	$allReasons = $wpdb->get_results($sql);

	foreach ($allReasons as $key => $reason) {
		$support_types 					= $supportsArr[ $reason->id ];
		$data 							= array();
	 	$data['support_eq_types'] 		= (!empty($support_types)) ? implode(',', $support_types) : "";
	 	$wpdb->update("en_lm_loan_reasons",$data, array("id"=>$reason->id) );
	}
 
	echo json_encode( array("code"=>200,"message"=>"Relation has been updated.") ); die();
}
///////////////////////////////////////////////////////////////////////*/
//Save Location table relation
add_action("wp_ajax_save_location_relation", "save_location_relation_callback");
add_action("wp_ajax_nopriv_save_location_relation", "save_location_relation_callback");
function save_location_relation_callback(){
 	global $wpdb;
 	$sql          = "SELECT * FROM en_lm_loan_locations "; 
	$alllocations = $wpdb->get_results($sql);

	$supportsArr = $_POST['support_types'];

	foreach ($alllocations as $key => $location) {
		$support_types 					= $supportsArr[ $location->id ];
	
		$data 							= array();
	 	$data['support_eq_types'] 		= (!empty($support_types)) ? implode(',', $support_types) : "";

	 	$wpdb->update("en_lm_loan_locations",$data, array("id"=>$location->id) );
	}

	echo json_encode( array("code"=>200,"message"=>"Relation has been updated.") ); die();
}

////////////////////////////////////////////////////////////////////////*/
/***********************Declare Common Used Functions********************/
///////////////////////////////////////////////////////////////////////*/

/*
* Declare Function: This function used to update the facilty equipment main table columns
*/
function lm_update_facility_equipment_table($asset_id){
	global $wpdb;

	$lastLoanRequest= lm_get_latest_loan_request_by_asset_id($asset_id);
	$requestData 	= $lastLoanRequest['loanRequest'];
	$loan_status    = $requestData->check_in_out_status;
	$updateData		= array();

	 
	
	if(!empty($requestData ) ){	
  
		if( $loan_status == 4 ||  $loan_status == 5 ){ // if status denied or canceled
			
			$lm_current_custodian	= '';
			$lm_organization 		= '';
			$locationArr 			= $wpdb->get_row("SELECT checkin_location FROM en_lm_loan_requests WHERE asset_id='$asset_id' and check_in_out_status=3 ORDER BY id DESC LIMIT 0, 1");
			$lm_current_location 	= $locationArr->checkin_location;
			$lm_due_for_return 		= '';

			$updateData["loan_status"] = 3; // checkin back Item if request denoed or cancelled


		}else if($loan_status == 3){ // if status Checked In

			$lm_current_custodian	= '';
			$lm_organization 		= '';
			$lm_current_location 	= $requestData->checkin_location;
			$lm_due_for_return 		= '';
			$updateData["loan_status"] = $loan_status;
			

		}else if($loan_status == 2){ // if status Checked OUT

			if($requestData->checking_for_client=="Yes"){
		 		$lm_current_custodian 	= $requestData->client_name;
		 		$lm_organization 		= $requestData->organization;
		 		$lm_checking_for_client = "Yes";

		 	}else{
		 		$lm_current_custodian	= get_current_user_id();
		 		$lm_organization 		= get_user_meta( get_current_user_id(), 'user_organization',true );
		 	}
		 	$lm_current_location 		=  $requestData->usage_location;
		 	$lm_due_for_return 			=  $requestData->requested_return_date;
		 	
		 	$updateData["loan_status"]	= $loan_status;

		}else{ // IF Status Pending 
			
			if($requestData->checking_for_client=="Yes"){
		 		$lm_current_custodian 	= $requestData->client_name;
		 		$lm_organization 		= $requestData->organization;
		 		$lm_checking_for_client = "Yes";

		 	}else{
		 		$lm_current_custodian	= get_current_user_id();
		 		$lm_organization 		= get_user_meta( get_current_user_id(), 'user_organization',true );
		 	}
		 	$lm_current_location 		=  $requestData->usage_location;
		 	$lm_due_for_return 			=  $requestData->requested_return_date;
		 	$updateData["loan_status"]	=  $loan_status;
		}

 	 
		$navigation 			= lm_generate_nav_icons($loan_status);
		
		$updateData["loan_navigation"]			= $navigation; 
		$updateData["lm_checking_for_client"] 	= $lm_checking_for_client;
		$updateData["lm_current_custodian"]		= $lm_current_custodian;
		$updateData["lm_organization"]			= $lm_organization;
		$updateData["lm_current_location"]		= $lm_current_location;
		$updateData["lm_due_for_return"]		= $lm_due_for_return;
 
	} else {

		// if there is no request exist for asset
		$loan_status = 3 ; // By deafult status will be check in
		$navigation  = lm_generate_nav_icons($loan_status); 
		$updateData["loan_navigation"]		 = $navigation; 
		$updateData["loan_status"]			 = $loan_status;
		$updateData["lm_checking_for_client"]= '';
		$updateData["lm_current_custodian"]	 = get_current_user_id();
		$updateData["lm_organization"]		 = get_user_meta( get_current_user_id(), 'user_organization',true );
	
	}
 

	$wpdb->update("en_mb_facility_equipment",$updateData, array("asset_id"=>$asset_id)); 
 	
}



/*
* Declare Function: This function used to get equipment data by asset id
*/
function lm_get_equipment_by_asset_id($asset_id){
	global $wpdb;
	$sql = "select * from en_mb_facility_equipment where asset_id='$asset_id'"; 
	$assetDetails = $wpdb->get_row($sql);
	return $assetDetails;
}
/*
* Declare Function: This function used to get equipment type name by type id
*/
function lm_get_eq_type_by_type_id($id){
	global $wpdb;
	$sql = "select * from en_mb_equipment_types where id='$id'"; 
	$Details = $wpdb->get_row($sql);
	return $Details->equipment_type;
}
/*
* Declare Function: This function used to get all equipment types
*/
function lm_get_all_equipment_types(){
	global $wpdb;
	$sql = "select * from en_mb_equipment_types"; 
	$Details = $wpdb->get_results($sql);
	return $Details;
}
/*
* Declare Function: This function used to get loan request Data for internal use
*/
function lm_get_latest_loan_request_by_asset_id($asset_id){
	global $wpdb;

	$sql1 = "SELECT * FROM en_lm_loan_requests WHERE asset_id='$asset_id' ORDER BY id DESC LIMIT 0,1"; 
	$loanRequest = $wpdb->get_row($sql1);
	

	$sql2 = "SELECT * FROM `en_lm_loan_requests_meta` WHERE request_id='$loanRequest->id'"; 
	$loanRequestMeta = $wpdb->get_results($sql2);

	$data 					= array();
	$data['loanRequest'] 	= $loanRequest;
	$data['loanRequestMeta']= $loanRequestMeta;
	return $data;
}
/*
* Declare Function: This function used to get loan request Data for display
*/
function lm_get_loan_request_display_data_by_asset_id($asset_id){
	global $wpdb;

	$sql1 = "SELECT 
    r.asset_id,
    eq.make,
    eq.model,
    eq.equipment_type,
    eq.year,
    r.date_of_checkout,
    r.id as request_id,
    r.requested_return_date,
    r.actual_return_date,
    r.odometer_reading_checkout,
    r.odometer_reading_return,
    (SELECT location FROM en_lm_loan_locations WHERE id =r.usage_location) AS usage_location, 
    (SELECT location FROM en_lm_loan_locations WHERE id =r.checkin_location) AS checkin_location, 
    (SELECT reason FROM en_lm_loan_reasons WHERE id =r.reason) AS reason, 
    r.organization,
    r.checking_for_client,
    r.client_name,
    r.project,
    r.loan_policy,
    (SELECT status FROM en_lm_loan_status WHERE id =r.usage_location) AS loan_request_status,
    r.experience_issue,
    r.issue_to_manager,
    r.additional_damage,
    r.experience_note,
    r.common_notes,
    (SELECT display_name FROM wp_users WHERE ID =r.approved_deny_by) AS approved_deny_by_name,
    (SELECT user_email FROM wp_users WHERE ID =r.approved_deny_by) AS approved_deny_by_email,
    (SELECT display_name FROM wp_users WHERE ID =r.checkin_by) AS checkin_by,
    (SELECT user_email FROM wp_users WHERE ID =r.created_by) AS created_by,
    (SELECT display_name FROM wp_users WHERE ID =r.created_by) AS created_by_name,
    (SELECT display_name FROM wp_users WHERE ID =r.modify_by) AS modify_by,
    (SELECT  GROUP_CONCAT(CONCAT('<strong>',component_name,':-</strong>',component_description) SEPARATOR '<br>')  FROM `en_lm_loan_requests_meta` where request_id= r.id AND is_old=1) as previous_damage,
    (SELECT  GROUP_CONCAT(CONCAT('<strong>',component_name,':-</strong>',component_description) SEPARATOR '<br>')   FROM `en_lm_loan_requests_meta` where request_id= r.id and is_old=0) as new_damage,
    (SELECT  GROUP_CONCAT(CONCAT('<strong>',component_name,':-</strong>',component_description) SEPARATOR '<br>')  FROM `en_lm_loan_requests_meta` where request_id= r.id and is_additional=1) as reported_damage
    FROM `en_lm_loan_requests` as r
	LEFT JOIN en_mb_facility_equipment as eq ON eq.asset_id = r.asset_id
    WHERE r.asset_id='$asset_id' ORDER BY r.id DESC LIMIT 0,1";

	$loanRequest = $wpdb->get_row($sql1);
	 
	return $loanRequest;
}

/*
* Declare Function: This function used for get Equipment Type settings
*/
function lm_get_loan_equipment_settings($type,$key=null){
	global $wpdb;

	$sql2 = "SELECT * FROM `en_lm_loan_equipment_settings` WHERE equipment_type='$type'"; 
	$equipment_settings = $wpdb->get_row($sql2);

	if($key==null){

		return $equipment_settings;

	}else{
		
		return $equipment_settings->$key;
	}	
}
/*
* Declare Function: This function used for get supported location for a equipment type, used in locations dropdowns
*/
  
function lm_get_locations_for_eq_type($typeID){
	global $wpdb;

	$sql2 = "SELECT * FROM `en_lm_loan_locations` where FIND_IN_SET( $typeID,REPLACE(`support_eq_types`, ' ', ''))"; 
	$equipmentLocations = $wpdb->get_results($sql2);

	return $equipmentLocations;
}
/*
* Declare Function: This function used for get supported location for a equipment type, used in locations dropdowns
*/
function lm_get_reasons_for_eq_type($typeID){
	global $wpdb;

	$sql2 = "SELECT * FROM `en_lm_loan_reasons` where FIND_IN_SET( $typeID,REPLACE(`support_eq_types`, ' ', ''))"; 
	$reasonsLocations = $wpdb->get_results($sql2);

	return $reasonsLocations;
}
/*
* Declare Function: This function creates Icons for Loan Front page
*/
function lm_generate_nav_icons($loan_status){
	$actionLinks="";
	switch ($loan_status) {

		case '1':  // Pending
			 
			$actionLinks.='<a title="Request Pending" class="loan-action request-pending" data-action="request-pending"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/stamp.png"></a>';

			$actionLinks.='<a title="Overview" class="loan-action barcode-scanner" data-action="overview"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/barcode-scanner.png"></a>';

			break;	

		case '2':  // Checked Out
			 
			$actionLinks.='<a title="Checkout" class="loan-action checkout-red" data-action="checkout"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/checkout-2.png"></a>';

			$actionLinks.='<a title="Overview" class="loan-action barcode-scanner" data-action="overview"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/barcode-scanner.png"></a>';

			$actionLinks.='<a title="Recall" class="loan-action recall" data-action="recall" data-recall-status="0"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/recall-btn.png"></a>';

			break;	

		case '8':  // recalled
			 
			$actionLinks.='<a title="Checkout" class="loan-action checkout-red" data-action="checkout"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/checkout-2.png"></a>';

			$actionLinks.='<a title="Overview" class="loan-action barcode-scanner" data-action="overview"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/barcode-scanner.png"></a>';

			$actionLinks.='<a title="Recall" class="loan-action recall" data-action="recall" data-recall-status="1"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/recall-btn.png"></a>';

			break;		

		case '7':  // Past Due
			 
			$actionLinks.='<a title="Checkout" class="loan-action checkout-red" data-action="checkout"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/checkout-2.png"></a>';

			$actionLinks.='<a title="Overview" class="loan-action barcode-scanner" data-action="overview"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/barcode-scanner.png"></a>';


			break;			
		
		default: 
			$actionLinks.='<a title="Loan Request" class="loan-action checkin-green" data-action="loan-request"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/checkin-1.png"></a>';

			$actionLinks.='<a title="Overview" class="loan-action barcode-scanner" data-action="overview"><img class="action-icon" src="'.get_bloginfo("url").'/wp-content/plugins/acm_loan_manager/icons/barcode-scanner.png"></a>';
			break;
	}
	return $actionLinks;
}
/*
* Declare Function: This function creates Icon btn for Overview page
*/
function lm_overview_btn_icon($asset_id){
	 global $wpdb;
	$sql1 = "SELECT loan_navigation FROM `en_mb_facility_equipment` where asset_id='$asset_id'"; 
	$result = $wpdb->get_row($sql1);
	$loan_navigation = $result->loan_navigation;
	 
	return $loan_navigation;
	 
}

/*
* Declare Function: This function will check authorization for checkout
*/
function lm_is_authorization_required($asset_id){
	global $wpdb;

	$sql1 = "SELECT lm_auth_required,equipment_type FROM `en_mb_facility_equipment` where asset_id='$asset_id'"; 
	$authorization_result = $wpdb->get_row($sql1);
	$equipment_type = $authorization_result->equipment_type;
	if(!empty($authorization_result->lm_auth_required)){
		return $authorization_result->lm_auth_required;
	}
	else{
		$sql2 = "SELECT Auth_required_for_checkout FROM `en_lm_loan_equipment_settings` where equipment_type='$equipment_type'";
		$authorization_result = $wpdb->get_row($sql2);
		return $authorization_result->Auth_required_for_checkout;
	}
}

/*
* Declare Function: This function will will return all approvers
*/
function lm_return_all_approvers($asset_id){
	$eq_type 	= lm_get_equipment_by_asset_id($asset_id);
	$approvers 	= lm_get_loan_equipment_settings($eq_type->equipment_type,'request_approvers');
	$approversArr = explode(",",$approvers);
	return $approversArr;
}
/*
* Declare Function: This function will return expedited return days
*/
function lm_expedited_Return_days($asset_id){
	global $wpdb;
	$sql1 = "SELECT lm_expedited_checkout,equipment_type FROM `en_mb_facility_equipment` where asset_id='$asset_id'";
	$authorization_result = $wpdb->get_row($sql1);
	$equipment_type = $authorization_result->equipment_type;
	if(!empty($authorization_result->lm_expedited_checkout)){
		return $authorization_result->lm_expedited_checkout;
	}
	else{
		$sql2 = "SELECT expedited_checkout_duration FROM `en_lm_loan_equipment_settings` where equipment_type='$equipment_type'";
		$authorization_result = $wpdb->get_row($sql2);
		return $authorization_result->expedited_checkout_duration;
	}
}
/*
* Declare Function: This function will return max loan period
*/
function lm_max_loan_period($asset_id){
	global $wpdb;
	$sql = "SELECT lm_max_loan_period,equipment_type FROM `en_mb_facility_equipment` where asset_id='$asset_id'";
	$authorization_result = $wpdb->get_row($sql);
	$equipment_type = $authorization_result->equipment_type;
	if(!empty($authorization_result->lm_max_loan_period)){
		return $authorization_result->lm_max_loan_period;
	}
	else{
		$sql2 = "SELECT max_loan_period FROM `en_lm_loan_equipment_settings` where equipment_type='$equipment_type'";
		$authorization_result = $wpdb->get_row($sql2);
		return $authorization_result->max_loan_period;
	}
}

/*
* Declare Function: This function will return the maintenance status from MM application
*/
function lm_check_maintenance_status($asset_id){
	global $wpdb;
	$sql 	= "SELECT status FROM `en_mb_facility_equipment` WHERE `asset_id` LIKE '$asset_id'";
	$result = $wpdb->get_row($sql);
	 
	if(!empty($result)){
		return $result->status;
	}else{
		return '1' ;
	}
	
}

/*
* Declare Function: This function will loan enabled status from loan application
*/
function lm_loan_enabled($asset_id){
	global $wpdb;
	$sql 	= "SELECT loan_enable,equipment_type FROM `en_mb_facility_equipment` WHERE `asset_id` LIKE '$asset_id'";
	$result = $wpdb->get_row($sql);
 
	if(!empty($result) && !empty($result->loan_enable)){
		return $result->loan_enable;
	}else{
		$sql2 = "SELECT enable_asset_loan FROM `en_lm_loan_equipment_settings` where equipment_type='$result->equipment_type'";
		$authorization_result = $wpdb->get_row($sql2);
		 
		
		return $authorization_result->enable_asset_loan;
	}
	
}

function custom_query_vars_filter() {
  set_query_var("pagename",'lm-settings');

  global $wp_query;
  $wp_query->query['pagename']='lm-settings';

 
}
add_filter( 'wp_head', 'custom_query_vars_filter',10,0 );



function lm_get_visitors_data() {
 
  global $wp_query;

	$date1 = date('Y-m-d', strtotime('-4 days', strtotime(date("Y-m-d"))));
	$date2 = date('Y-m-d', strtotime('+4 days', strtotime(date("Y-m-d"))));

	$args = array(
	    'posts_per_page'   => -1,
	    'post_type'        => 'vms_visits',
		'meta_query' => array(
		        array(
		            'key' => 'en_vms_time_start',
		            'value' => array( $date1, $date2 ),
		            'compare' => 'BETWEEN',
		            'type' => 'DATE' 
		        )
		    )    
	);
	$posts = get_posts($args);	
	$visitorData = array(); 
	foreach ($posts as $key => $post) {
		$en_vms_visitors =  get_post_meta($post->ID, 'en_vms_visitors',true);
		 
		
		if(!empty($en_vms_visitors )){
			foreach ($en_vms_visitors  as $vkey => $visitor) {
	
				$visitorData[] =  array(
										'name'   => $visitor['name'],
										'company'=> $visitor['company'],
										'project'=> $post->post_title,
										'parent' => $post->ID
										);
			}
		}
		 
	}
   
return $visitorData;  

 
}
 
