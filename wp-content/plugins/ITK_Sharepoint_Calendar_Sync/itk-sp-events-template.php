<?php /* Template Name: SPEVENTS Template */

    require_once dirname( __FILE__ ) . '/sp-cal.php';
?>

<?php
	if(isset($_POST['cal_events'])):
		$data =  json_encode(preg_replace('/[^A-Za-z0-9\{\}\-\:\[\]\,\"\" ]/', '',  $_POST['cal_events']));
		$data = preg_replace('/\\\\/', '', $data);

		$content = rtrim(ltrim($data, '"'), '"');

		$obj=json_decode($content, true);
        //echo $_POST['cal_events'];
		//$obj['HighwayCalendar']['events'][0]['startTime'];
		
	endif;

    /** Color **/
    $color_1='#00475b';
    $color_2='#13402a';
    $color_3='#767956';
    $color_4='#ed0033';

    try {
	    itk_empty_data();

	    itk_insert( $obj['HighwayCalendar'], 1, $color_1 );
	    itk_insert( $obj['UrbanCalendar'], 2, $color_2 );
	    itk_insert( $obj['IntersectionCalendar'], 3, $color_3 );
	    itk_insert( $obj['InterUrbanArterialCalendar'], 4, $color_4 );
    }
    catch (Exception $e) {
	    echo 'Error: ',  $e->getMessage(), "\n";
    }


?>