<?php

// Include file
//include("SharePointAPI.php");
//use Thybag\SharePointAPI;

// Get Lists
function getSPCalEvents($username,$password,$wsdl,$calendarName){	
		// Call API Class
	try {
		$sp = new SharePointAPI($username, $password, $wsdl, 'SPONLINE');		
		$events = $sp->query($calendarName)->where('Title', '!=', '2018-02-05 12:00:00')->get();
		
		return $events;	
	}
	catch (Exception $e) {
		echo 'Error: ',  $e->getMessage(), "\n";
	}
}

/**Inserting data**/
function itk_insert($calendar,$cid,$cl){
	foreach($calendar['events'] as $cal):
		itk_save_data($cal,$cid,$cl);
	endforeach;
	return;
}

/** Save Data **/

function itk_save_data($event_data,$cal_id,$color){
	global $wpdb;

	/** Common data **/
	$created_by=1;
	$featured=0;
	$visibility = 'stec_cal_default';
	$icon='fa';
	$all_day=0;
	$keywords ='';
	$counter =0;
	$comments =0;
	$link ='';
	$approved=1;
	$title = $event_data['exclusivity'] . ' - ' . $event_data['status'];//'IN USE';
	$alias = $event_data['exclusivity'] . ' - ' . $event_data['status'];//'in-use';
	$user_id =1;
	$des = ''; //removeExtra($event_data['description']);
	$s_date = $event_data['startTime'];
	$e_date = $event_data['endTime'];
	$location = 'ACM'; //$event_data['location'];

	$query = $wpdb->insert(
		$wpdb->prefix . 'stec_events', array(
		'created_by' => $user_id,
		'calid'      => $cal_id,
		'alias'      => itk_validate_event_alias($alias),
		'summary'    => $title,
		'color'      => $color,
		'icon'       => $icon,
		'visibility' => $visibility,
		'description'   => $des,
		'featured'   => $featured,
		'start_date' => $s_date,
		'end_date'   => $e_date,
		'location'   => $location,
		'all_day'    => $all_day,
		'keywords'   => $keywords,
		'counter'    => $counter,
		'comments'   => $comments,
		'link'       => $link,
		'approved'   => $approved,
	), array(
			'%d',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%d',
			'%d',
			'%s',
			'%d'
		)
	);

	if ( $query === false ) {
		$wpdb->print_error();
		return false;
	}
	return;
}

/** Validate event slug (Collected form plugin) **/

function itk_validate_event_alias($alias, $id = false) {

	global $wpdb;

	$event_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$wpdb->prefix}stec_events AS events WHERE events.alias = '%s' ", $alias));

	if ( !$event_id ) {
		return $alias;
	}

	if ( $id && $event_id === $id ) {
		return $alias;
	}

	$alias = explode('-', $alias);

	$last = array_pop($alias);

	if ( is_numeric($last) ) {

		$alias = implode('-', $alias) . '-' . ($last + 1);
	} else {

		$alias = implode('-', $alias) . '-' . $last . '-1';
	}

	return itk_validate_event_alias($alias);
}

/**Empty Data**/

function itk_empty_data(){
	global $wpdb;
	$table  = $wpdb->prefix . 'stec_events';
	$delete = $wpdb->query("TRUNCATE TABLE $table");
	return;
}

// Remove Extra characters
function removeExtra($str){
	return $clear = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($str))))));
}


?>
