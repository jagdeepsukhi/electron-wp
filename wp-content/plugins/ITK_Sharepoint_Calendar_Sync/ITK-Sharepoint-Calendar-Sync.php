<?php
/*
Plugin Name: ITK Sharepoint Calendar Sync
Plugin URI: http://acm.itselectron.com
Description: Sharepoint Calendar Synchronization
Author: Marco Gaertner, Intertek
Version: 1.0
Author URI: http://intertek.com
*/

// Include Module
require_once dirname( __FILE__ ) . '/sp-cal.php';

function itk_sp_cal() {
	if( get_page_by_title( 'spevents' ) == NULL ):
		itk_create_pages( 'spevents' );
	endif;
}

/**Activation**/
register_activation_hook( __FILE__, 'itk_sp_cal' );

add_filter( 'template_include', 'itk_page_template' );
function itk_page_template( $page_template )
{
	if ( is_page("spevents") ) :
		$page_template = plugin_dir_path( __FILE__ ) . 'itk-sp-events-template.php';
	endif;
	return $page_template;
}

function itk_create_pages($pageName) {
	$createPage = array(
		'post_title'    => $pageName,
		'post_status'   => 'publish',
		'post_author'   => 1,
		'post_type'     => 'page',
		'post_name'     => $pageName
	);

	// Insert the post into the database
	wp_insert_post( $createPage );
}

//register_activation_hook(__FILE__, 'itk_activation');

function itk_activation() {
	itk_sp_cal();
	if (! wp_next_scheduled ( 'itk_hourly_event' )) {
		wp_schedule_event(time()+ 3600, 'hourly', 'itk_hourly_event');
	}
}

//add_action('itk_hourly_event', 'itk_sp_cal');


/**Deactivation**/

/*register_deactivation_hook(__FILE__, 'itk_deactivation');

function itk_deactivation() {
	wp_clear_scheduled_hook('itk_hourly_event');
}*/
