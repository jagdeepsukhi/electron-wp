<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit241bc51bf85d514e7af187332583dbd2
{
    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Thybag' => 
            array (
                0 => __DIR__ . '/../..' . '/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInit241bc51bf85d514e7af187332583dbd2::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
