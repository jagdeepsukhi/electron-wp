# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* It will retrive all "Sharepoint Calendar Events" and create CSV file as per.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Function to call:
------------------
saveSPCalendar($username,$password,$wsdl,$calendarName);

* Configuration

Where- 
$username - User Name
$password - Password
$wsdl - WSDL file for the SharePoint Lists (as per test account- https://bdswebtech.sharepoint.com/_vti_bin/Lists.asmx?WSDL)
$calendarName - Name of the calendar we would like to import, we might have more than one calendar. There are two calendar in test account- Schedule, Appointment.

* Dependencies

API Library Used:
-----------------
https://github.com/thybag/PHP-SharePoint-Lists-API

* Database configuration
* How to run tests

Call this function with required credentials -

saveSPCalendar($username,$password,$wsdl,$calendarName);

CSV File:
----------
CSV file will save inside a folder called "csvfiles"

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact