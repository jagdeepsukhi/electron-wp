<?php
/**
 * Plugin Name: QSM - Extra Shortcodes
 * Plugin URI: https://quizandsurveymaster.com/
 * Description: Add extra shortcodes to use with Quiz And Survey Master
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com/
 * Version: 2.0.0
 *
 * @author Frank Corso
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 2.0.0
  */
class QSM_Extra_Shortcodes {

    /**
     * Version Number
     *
     * @var string
     * @since 0.1.0
     */
    public $version = '2.0.0';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 2.0.0
  	  * @uses QSM_Extra_Shortcodes::load_dependencies() Loads required filed
  	  * @uses QSM_Extra_Shortcodes::add_hooks() Adds actions to hooks and filters
  	  * @return void
  	  */
    function __construct() {
      $this->load_dependencies();
      $this->add_hooks();
      $this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 2.0.0
  	  * @return void
  	  */
    public function load_dependencies() {
		    include( "php/addon-settings-tab-content.php" );
        include( "php/shortcodes.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 2.0.0
  	  * @return void
  	  */
    public function add_hooks() {
      add_action( 'admin_init', 'qsm_addon_extra_shortcodes_register_addon_settings_tabs' );
			add_shortcode( 'qsm_passed', 'qsm_addon_extra_shortcodes_passed_score_shortcode' );
			add_shortcode( 'qsm_passed_points', 'qsm_addon_extra_shortcodes_passed_point_shortcode' );
			add_shortcode( 'qsm_failed', 'qsm_addon_extra_shortcodes_failed_score_shortcode' );
			add_shortcode( 'qsm_failed_points', 'qsm_addon_extra_shortcodes_failed_point_shortcode' );
			add_shortcode( 'qsm_average', 'qsm_addon_extra_shortcodes_average_score_shortcode' );
			add_shortcode( 'qsm_average_points', 'qsm_addon_extra_shortcodes_average_point_shortcode' );
			add_shortcode( 'qsm_taken', 'qsm_addon_extra_shortcodes_taken_shortcode' );
      add_shortcode( 'qsm_recent', 'qsm_addon_extra_shortcodes_recent_shortcode' );
      add_shortcode( 'qsm_popular', 'qsm_addon_extra_shortcodes_popular_shortcode' );
    }

    /**
     * Checks license
     *
     * Checks to see if license is active and, if so, checks for updates
     *
     * @since 2.0.0
     * @return void
     */
     public function check_license() {

       if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {

       	// load our custom updater
       	include( 'php/EDD_SL_Plugin_Updater.php' );
       }

      // retrieve our license key from the DB
      $extra_shortcodes_data = get_option( 'qsm_addon_extra_shortcodes_settings', '' );
      if ( isset( $extra_shortcodes_data["license_key"] ) ) {
        $license_key = trim( $extra_shortcodes_data["license_key"] );
      } else {
        $license_key = '';
      }

     	// setup the updater
     	$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com/', __FILE__, array(
     			'version' 	=> $this->version, 				// current version number
     			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
     			'item_name' => 'Extra Shortcodes', 	// name of this plugin
     			'author' 	=> 'Frank Corso'  // author of this plugin
     		)
     	);
     }
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 2.0.0
 * @return void
 */
function qsm_addon_extra_shortcodes_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_extra_shortcodes = new QSM_Extra_Shortcodes();
	} else {
		add_action( 'admin_notices', 'qsm_addon_extra_shortcodes_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_extra_shortcodes_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       2.0.0
 * @return      string The notice to display
 */
function qsm_addon_extra_shortcodes_missing_qsm() {
  echo '<div class="error"><p>QSM - Extra Shortcodes requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
