<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Creates function for the qsm_passed shortcode
 *
 * @since 2.0.0
 * @return int The amount of results with score greater than the supplied score in shortcode parameters
 */
function qsm_addon_extra_shortcodes_passed_score_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'score' => 90,
    'quiz_id' => 1
  ), $atts));

  $score = intval( $score );
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(quiz_name) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d AND correct_score>=%d", $quiz_id, $score ) );
}

/**
 * Creates function for the qsm_passed_points shortcode
 *
 * @since 2.0.0
 * @return int The amount of results with score greater than the supplied score in shortcode parameters
 */
function qsm_addon_extra_shortcodes_passed_point_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'score' => 90,
    'quiz_id' => 1
  ), $atts));

  $score = intval( $score );
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(quiz_name) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d AND point_score>=%d", $quiz_id, $score ) );
}

/**
 * Creates function for the qsm_failed_points shortcode
 *
 * @since 2.0.0
 * @return int The amount of results with a point score less than the supplied score shortcode parameter
 */
function qsm_addon_extra_shortcodes_failed_point_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'score' => 90,
    'quiz_id' => 1
  ), $atts));

  $score = intval( $score );
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(quiz_name) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d AND point_score<%d", $quiz_id, $score ) );
}

/**
 * Creates function for the qsm_failed shortcode
 *
 * @since 2.0.0
 * @return int The amount of results with a score less than the supplied score shortcode parameter
 */
function qsm_addon_extra_shortcodes_failed_score_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'score' => 90,
    'quiz_id' => 1
  ), $atts));

  $score = intval( $score );
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(quiz_name) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d AND correct_score<%d", $quiz_id, $score ) );
}

/**
 * Creates function for the qsm_taken shortcode
 *
 * @since 2.0.0
 * @return int The amount of times the quiz has been taken
 */
function qsm_addon_extra_shortcodes_taken_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'quiz_id' => 1
  ), $atts));
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(quiz_name) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d", $quiz_id ) );
}

/**
 * Creates function for the qsm_average shortcode
 *
 * @since 2.0.0
 * @return int The average score of the results for the quiz
 */
function qsm_addon_extra_shortcodes_average_score_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'quiz_id' => 1
  ), $atts));
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT AVG(correct_score) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d", $quiz_id ) );
}

/**
 * Creates function for the qsm_average_points shortcode
 *
 * @since 2.0.0
 * @return int The average point score of the results for the quiz
 */
function qsm_addon_extra_shortcodes_average_point_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'quiz_id' => 1
  ), $atts));
  $quiz_id = intval( $quiz_id );


  global $wpdb;
  return $wpdb->get_var( $wpdb->prepare( "SELECT AVG(point_score) FROM {$wpdb->prefix}mlw_results WHERE deleted=0 AND quiz_id=%d", $quiz_id ) );
}

/**
 * Creates function for the qsm_recent shortcode
 *
 * @since 2.0.0
 * @return string An HTML list of the most recent quizzes
 */
function qsm_addon_extra_shortcodes_recent_shortcode( $atts ) {
  extract(shortcode_atts(array(
    'amount' => 5
  ), $atts));
  $amount = intval( $amount );

  $return = '';
  global $wpdb;
  $recent_quizzes = $wpdb->get_results( "SELECT quiz_id, quiz_name FROM {$wpdb->prefix}mlw_quizzes ORDER BY quiz_id DESC LIMIT $amount" );
  $quizzes = array();
  foreach( $recent_quizzes as $quiz ) {
    $quizzes[ $quiz->quiz_id ] = $quiz->quiz_name;
  }

  $args = array(
    'post_type' => 'quiz',
    'meta_query' => array(
      array(
        'key'     => 'quiz_id',
        'value'   => array_keys( $quizzes ),
        'compare' => 'IN',
      ),
    ),
  );
  $my_query = new WP_Query( $args );

  if( $my_query->have_posts() ) {
    $return .= '<ul class="qsm-popular-list">';
    while( $my_query->have_posts() ) {
      $my_query->the_post();
      $return .= "<li><a href='" . get_the_permalink() . "'>" . $quizzes[get_post_meta( get_the_ID(), 'quiz_id', true )] . "</a></ul>";
    }
    $return .= '</ul>';
  }
  return $return;
}

/**
 * Creates function for the qsm_popular shortcode
 *
 * @since 2.0.0
 * @return string An HTML list of the most popular quizzes
 */
function qsm_addon_extra_shortcodes_popular_shortcode( $atts ) {

  extract(shortcode_atts(array(
    'amount' => 5
  ), $atts));
  $amount = intval( $amount );

  $return = '';
  global $wpdb;
  $recent_quizzes = $wpdb->get_results( "SELECT quiz_id, quiz_name FROM {$wpdb->prefix}mlw_quizzes ORDER BY quiz_taken DESC LIMIT $amount" );
  $quizzes = array();
  foreach( $recent_quizzes as $quiz ) {
    $quizzes[ $quiz->quiz_id ] = $quiz->quiz_name;
  }

  $args = array(
    'post_type' => 'quiz',
    'meta_query' => array(
      array(
        'key'     => 'quiz_id',
        'value'   => array_keys( $quizzes ),
        'compare' => 'IN',
      ),
    ),
  );
  $my_query = new WP_Query( $args );

  if( $my_query->have_posts() ) {
    $return .= '<ul class="qsm-popular-list">';
    while( $my_query->have_posts() ) {
      $my_query->the_post();
      $return .= "<li><a href='" . get_the_permalink() . "'>" . $quizzes[get_post_meta( get_the_ID(), 'quiz_id', true )] . "</a></li>";
    }
    $return .= '</ul>';
  }
  return $return;
}

?>
