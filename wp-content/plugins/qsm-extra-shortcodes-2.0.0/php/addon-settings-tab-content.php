<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the addon  settings page
 *
 * @since 2.0.0
 * @return void
 */
function qsm_addon_extra_shortcodes_register_addon_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_addon_settings_tab( "Extra Shortcodes", 'qsm_addon_extra_shortcodes_addon_settings_tabs_content' );
  }
}

/**
 * Generates the content for your addon settings tab
 *
 * @since 2.0.0
 * @return void
 */
function qsm_addon_extra_shortcodes_addon_settings_tabs_content() {

  global $mlwQuizMasterNext;

  //If nonce is correct, update settings from passed input
  if ( isset( $_POST["extra_shortcodes_nonce"] ) && wp_verify_nonce( $_POST['extra_shortcodes_nonce'], 'extra_shortcodes') ) {

    // Load previous license key
    $extra_shortcodes_data = get_option( 'qsm_addon_extra_shortcodes_settings', '' );
    if ( isset( $extra_shortcodes_data["license_key"] ) ) {
      $license = trim( $extra_shortcodes_data["license_key"] );
    } else {
      $license = '';
    }

    // Save settings
    $saved_license = sanitize_text_field( $_POST["license_key"] );
    $extra_shortcodes_data = array(
      'license_key' => $saved_license
    );
    update_option( 'qsm_addon_extra_shortcodes_settings', $extra_shortcodes_data );

    // Checks to see if the license key has changed
    if ( $license != $saved_license ) {

      // Prepares data to activate the license
      $api_params = array(
        'edd_action'=> 'activate_license',
        'license' 	=> $saved_license,
        'item_name' => urlencode( 'Extra Shortcodes' ), // the name of our product in EDD
        'url'       => home_url()
      );

      // Call the custom API.
      $response = wp_remote_post( 'https://quizandsurveymaster.com/', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

      // If previous license key was entered
      if ( ! empty( $license ) ) {

        // Prepares data to deactivate changed license
        $api_params = array(
          'edd_action'=> 'deactivate_license',
          'license' 	=> $license,
          'item_name' => urlencode( 'Extra Shortcodes' ), // the name of our product in EDD
          'url'       => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post( 'https://quizandsurveymaster.com/', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
      }
    }
    $mlwQuizMasterNext->alertManager->newAlert( 'Your settings has been saved successfully! You can now use the shortcodes listed below.', 'success' );
  }

  // Load settings
  $extra_shortcodes_data = get_option( 'qsm_addon_extra_shortcodes_settings', '' );
  $extra_shortcodes_defaults = array(
    'license_key' => ''
  );
  $extra_shortcodes_data = wp_parse_args( $extra_shortcodes_data, $extra_shortcodes_defaults );

  // Show any alerts from saving
  $mlwQuizMasterNext->alertManager->showAlerts();

  ?>
  <form action="" method="post">
    <table class="form-table" style="width: 100%;">
      <tr valign="top">
        <th scope="row"><label for="license_key">Addon License Key</label></th>
        <td><input type="text" name="license_key" id="license_key" value="<?php echo $extra_shortcodes_data["license_key"]; ?>"></td>
      </tr>
    </table>
    <?php wp_nonce_field('extra_shortcodes','extra_shortcodes_nonce'); ?>
    <button class="button-primary">Save Changes</button>
  </form>
  <h2>Extra Shortcodes</h2>
  <p>There are a variety of shortcodes that you can use with this addon. Examples are below.</p>
  <strong>[qsm_passed quiz_id=1 score=90]</strong> The number of users who passed the quiz with a score above the value provided.<br />
  <strong>[qsm_passed_points quiz_id=1 score=90]</strong> The number of users who passed the quiz with a point score above the value provided.<br />
  <strong>[qsm_failed quiz_id=1 score=90]</strong> The number of users who failed the quiz with a score below the value provided.<br />
  <strong>[qsm_failed_points quiz_id=1 score=90]</strong> The number of users who failed the quiz with a point score below the value provided.<br />
  <strong>[qsm_average quiz_id=1]</strong> The average score of the users who have taken the quiz.<br />
  <strong>[qsm_average_points quiz_id=1]</strong> The average point score of the users who have taken the quiz.<br />
  <strong>[qsm_taken quiz_id=1]</strong> The number of times the quiz has been taken.<br />
  <strong>[qsm_recent amount=1]</strong> Lists the most recent quizzes with links to the quiz. Use the "amount" to set how many quizzes to list.<br />
  <strong>[qsm_popular amount=1]</strong> Lists the quizzes that have been taken the most. Use the "amount" to set how many quizzes to list.<br />
  <?php
}
?>
