<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the addon  settings page
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_export_import_register_addon_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_addon_settings_tab( "Export And Import", 'qsm_addon_export_import_addon_settings_tabs_content' );
  }
}

/**
 * Generates the content for your addon settings tab
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_export_import_addon_settings_tabs_content() {

  //Enqueue your scripts and styles
  wp_enqueue_script( 'qsm_export_import_admin_script', plugins_url( '../js/qsm-export-import-admin.js' , __FILE__ ), array( 'jquery' ) );
  wp_enqueue_style( 'qsm_export_import_admin_style', plugins_url( '../css/qsm-export-import-admin.css' , __FILE__ ) );

  global $mlwQuizMasterNext;
  global $wpdb;

  // If import form has been submitted
  if ( isset( $_POST["import_file_nonce"] ) && wp_verify_nonce( $_POST['import_file_nonce'], 'import_file') ) {

    // Check if import file is missing and, if so, through error
    if ( empty( $_FILES["import-quizzes"] ) ) {
      $mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", "Error: File was missing!", 0, 'error' );
      $mlwQuizMasterNext->alertManager->newAlert( 'Error uploading file: Missing File. Please try again.', 'error' );
    } else {

      $accepted_mime_types = array(
        'application/json',
        'text/json',
        'text/javascript',
        'text/plain',
        'text/anytext',
        'text/*',
        'application/octet-stream'
      );

      // Checks to ensure the file is of a supported mime type
      if( empty( $_FILES['import-quizzes']['type'] ) || ! in_array( strtolower( $_FILES['import-quizzes']['type'] ), $accepted_mime_types ) ) {
        $mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", "Error: Incorrect type. Supplied type: {$_FILES['import-quizzes']['type']}", 0, 'error' );
      	$mlwQuizMasterNext->alertManager->newAlert( 'Error uploading file: Incorrect file format. Please try again.', 'error' );
      } else {

        // Checks to make sure file does not already exist
        if( ! file_exists( $_FILES['import-quizzes']['tmp_name'] ) ) {
          $mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", "File may possibly already exist!", 0, 'error' );
        	$mlwQuizMasterNext->alertManager->newAlert( 'Error uploading file. Please try again.', 'error' );
        } else {

          // Pass acceptable mimes
          $mimes = array(
            'json' => 'application/octet-stream'
          );

          // Let WordPress import the file. We will remove it after import is complete
          $import_file = wp_handle_upload( $_FILES['import-quizzes'], array( 'test_form' => false, 'mimes' => $mimes ) );

          // Checks if the file has been uploaded correctly
          if ( $import_file && empty( $import_file["error"] ) ) {

            // Import file into variable
            $quiz_json_string = file_get_contents( $import_file["file"] );
            $quiz_data = json_decode( $quiz_json_string, true );

            // Cycle through the quizzes and insert the quiz
            foreach ( $quiz_data as $quiz ) {
              $results = $wpdb->insert(
          			$wpdb->prefix . "mlw_quizzes",
          			array(
          				'quiz_name' => $quiz["quiz_name"],
          				'message_before' => $quiz["message_before"],
          				'message_after' => $quiz["message_after"],
          				'message_comment' => $quiz["message_comment"],
          				'message_end_template' => $quiz["message_end_template"],
          				'user_email_template' => $quiz["user_email_template"],
          				'admin_email_template' => $quiz["admin_email_template"],
          				'submit_button_text' => $quiz["submit_button_text"],
          				'name_field_text' => $quiz["name_field_text"],
          				'business_field_text' => $quiz["business_field_text"],
          				'email_field_text' => $quiz["email_field_text"],
          				'phone_field_text' => $quiz["phone_field_text"],
          				'comment_field_text' => $quiz["comment_field_text"],
          				'email_from_text' => $quiz["email_from_text"],
          				'question_answer_template' => $quiz["question_answer_template"],
          				'leaderboard_template' => $quiz["leaderboard_template"],
          				'system' => $quiz["system"],
          				'randomness_order' => $quiz["randomness_order"],
          				'loggedin_user_contact' => $quiz["loggedin_user_contact"],
          				'show_score' => $quiz["show_score"],
          				'send_user_email' => $quiz["send_user_email"],
          				'send_admin_email' => $quiz["send_admin_email"],
          				'contact_info_location' => $quiz["contact_info_location"],
          				'user_name' => $quiz["user_name"],
          				'user_comp' => $quiz["user_comp"],
          				'user_email' => $quiz["user_email"],
          				'user_phone' => $quiz["user_phone"],
          				'admin_email' => $quiz["admin_email"],
          				'comment_section' => $quiz["comment_section"],
          				'question_from_total' => $quiz["question_from_total"],
          				'total_user_tries' => $quiz["total_user_tries"],
          				'total_user_tries_text' => $quiz["total_user_tries_text"],
          				'certificate_template' => $quiz["certificate_template"],
          				'social_media' => $quiz["social_media"],
          				'social_media_text' => $quiz["social_media_text"],
          				'pagination' => $quiz["pagination"],
          				'pagination_text' => $quiz["pagination_text"],
          				'timer_limit' => $quiz["timer_limit"],
          				'quiz_stye' => $quiz["quiz_stye"],
          				'question_numbering' => $quiz["question_numbering"],
          				'quiz_settings' => $quiz["quiz_settings"],
          				'theme_selected' => $quiz["theme_selected"],
          				'last_activity' => date("Y-m-d H:i:s"),
          				'require_log_in' => $quiz["require_log_in"],
          				'require_log_in_text' => $quiz["require_log_in_text"],
          				'limit_total_entries' => $quiz["limit_total_entries"],
          				'limit_total_entries_text' => $quiz["limit_total_entries_text"],
          				'scheduled_timeframe' => $quiz["scheduled_timeframe"],
          				'scheduled_timeframe_text' => $quiz["scheduled_timeframe_text"],
          				'quiz_views' => $quiz["quiz_views"],
          				'quiz_taken' => $quiz["quiz_taken"],
          				'deleted' => 0
          			),
          			array(
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%s',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%d',
          				'%s',
          				'%d',
          				'%d',
          				'%d',
          				'%s',
          				'%s',
          				'%d',
          				'%s',
          				'%d',
          				'%s',
          				'%d',
          				'%s',
          				'%d',
          				'%s',
          				'%s',
          				'%s',
          				'%d',
          				'%s',
          				'%d',
          				'%s',
          				'%s',
          				'%s',
          				'%d',
          				'%d',
          				'%d'
          			)
          		);
              if ( false === $results ) {
                $mlwQuizMasterNext->alertManager->newAlert( 'Error importing a quiz/survey!', 'error');
          			$mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", $wpdb->last_error.' from '.$wpdb->last_query, 0, 'error' );
              } else {

                // Create quiz post for inserted quiz
                $new_quiz = $wpdb->insert_id;
          			$current_user = wp_get_current_user();
          			$quiz_post = array(
          			  'post_title'    => $quiz["quiz_name"],
          			  'post_content'  => "[mlw_quizmaster quiz=$new_quiz]",
          			  'post_status'   => 'publish',
          			  'post_author'   => $current_user->ID,
          			  'post_type' => 'quiz'
          			);
          			$quiz_post_id = wp_insert_post( $quiz_post );
          			add_post_meta( $quiz_post_id, 'quiz_id', $new_quiz );

          			$mlwQuizMasterNext->alertManager->newAlert( 'A quiz/survey has been imported successfully!', 'success');
          			$mlwQuizMasterNext->audit_manager->new_audit( "New Quiz Has Been Created: {$quiz["quiz_name"]}" );
          			do_action( 'qmn_quiz_created', $new_quiz );

                // Cycle through each question for the quiz and insert question
                foreach ( $quiz["questions"] as $question ) {
                  $results = $wpdb->insert(
        						$wpdb->prefix."mlw_questions",
        						array(
        							'quiz_id' => $new_quiz,
        							'question_name' => $question['question_name'],
        							'answer_array' => $question['answer_array'],
        							'answer_one' => $question['answer_one'],
        							'answer_one_points' => $question['answer_one_points'],
        							'answer_two' => $question['answer_two'],
        							'answer_two_points' => $question['answer_two_points'],
        							'answer_three' => $question['answer_three'],
        							'answer_three_points' => $question['answer_three_points'],
        							'answer_four' => $question['answer_four'],
        							'answer_four_points' => $question['answer_four_points'],
        							'answer_five' => $question['answer_five'],
        							'answer_five_points' => $question['answer_five_points'],
        							'answer_six' => $question['answer_six'],
        							'answer_six_points' => $question['answer_six_points'],
        							'correct_answer' => $question['correct_answer'],
        							'question_answer_info' => $question['question_answer_info'],
        							'comments' => $question['comments'],
        							'hints' => $question['hints'],
        							'question_order' => $question['question_order'],
        							'question_type_new' => $question['question_type_new'],
        							'question_settings' => $question['question_settings'],
        							'category' => $question['category'],
        							'deleted' => $question['deleted']
        						),
        						array(
        							'%d',
        							'%s',
        							'%s',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%d',
        							'%s',
        							'%s',
        							'%s',
        							'%d'
        						)
        					);
                  if ( false === $results ) {
                    $mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", $wpdb->last_error.' from '.$wpdb->last_query, 0, 'error' );
                  }
                }
              }
            }
          } else {
            $mlwQuizMasterNext->log_manager->add( "Error uploading QSM - Export & Import file", "Error from wp_handle_upload: {$import_file['error']}", 0, 'error' );
          	$mlwQuizMasterNext->alertManager->newAlert( 'Error uploading file. Please try again.', 'error' );
          }
        }
      }
    }
  }

  //If nonce is correct, update settings from passed input
  if ( isset( $_POST["export_import_nonce"] ) && wp_verify_nonce( $_POST['export_import_nonce'], 'export_import') ) {

    // Load previous license key
    $export_import_data = get_option( 'qsm_addon_export_import_settings', '' );
    if ( isset( $logic_data["license_key"] ) ) {
      $license = trim( $export_import_data["license_key"] );
    } else {
      $license = '';
    }

    // Save settings
    $saved_license = sanitize_text_field( $_POST["license_key"] );
    $export_import_data = array(
      'license_key' => $saved_license
    );
    update_option( 'qsm_addon_export_import_settings', $export_import_data );

    // Checks to see if the license key has changed
    if ( $license != $saved_license ) {

      // Prepares data to activate the license
      $api_params = array(
        'edd_action'=> 'activate_license',
        'license' 	=> $saved_license,
        'item_name' => urlencode( 'Export & Import' ), // the name of our product in EDD
        'url'       => home_url()
      );

      // Call the custom API.
      $response = wp_remote_post( 'https://quizandsurveymaster.com', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

      // If previous license key was entered
      if ( ! empty( $license ) ) {

        // Prepares data to deactivate changed license
        $api_params = array(
          'edd_action'=> 'deactivate_license',
          'license' 	=> $license,
          'item_name' => urlencode( 'Export & Import' ), // the name of our product in EDD
          'url'       => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post( 'https://quizandsurveymaster.com', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
      }
    }
    $mlwQuizMasterNext->alertManager->newAlert( 'Your settings has been saved successfully!', 'success' );
  }

  // Load settings
  $export_import_data = get_option( 'qsm_addon_export_import_settings', '' );
  $export_import_defaults = array(
    'license_key' => ''
  );
  $export_import_data = wp_parse_args( $export_import_data, $export_import_defaults );

  // load all quizzes
  $quiz_data = $wpdb->get_results( "SELECT quiz_id, quiz_name	FROM {$wpdb->prefix}mlw_quizzes WHERE deleted=0 ORDER BY quiz_id DESC" );

  // Show any alerts from saving
  $mlwQuizMasterNext->alertManager->showAlerts();

  ?>
  <form action="" method="post" class="addon-settings-form">
    <h2>Settings</h2>
    <table class="form-table" style="width: 100%;">
      <tr valign="top">
        <th scope="row"><label for="license_key">Addon License Key</label></th>
        <td><input type="text" name="license_key" id="license_key" value="<?php echo $export_import_data["license_key"]; ?>"></td>
      </tr>
    </table>
    <?php wp_nonce_field('export_import','export_import_nonce'); ?>
    <button class="button-primary">Save Changes</button>
  </form>
  <div class="export-section">
    <h2>Export Quizzes/Surveys</h2>
    <div class="settings-row">
      <select name="export-select" class="export-select">
        <option value="all">All Quizzes & Surveys</option>
        <?php
        foreach ( $quiz_data as $quiz ) {
          echo "<option value='{$quiz->quiz_id}'>{$quiz->quiz_name}</option>";
        }
        ?>
      </select>
    </div>
    <button class="button-primary export-submit">Export Quizzes/Surveys</button>
  </div>
  <div class="import-section">
    <h2>Import Quizes/Surveys</h2>
    <form action="" method="post" enctype="multipart/form-data">
      <div class="settings-row">
        Select file to upload:
        <input type="file" name="import-quizzes" id="import-quizzes">
      </div>
      <input type="submit" class="button-primary" value="Upload Quizzes/Surveys" name="submit">
      <?php wp_nonce_field('import_file','import_file_nonce'); ?>
    </form>
  </div>
  <?php
}
?>
