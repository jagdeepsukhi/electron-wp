<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Creates export file
 *
 * @since 0.1.0
 */
function qsm_addon_export_import_quiz_json() {

  global $wpdb;

  // If the export is set
  if ( isset( $_POST["export"] ) ) {

    // Check if user wants all quizzes exported
    if ( 'all' == $_POST["export"] ) {
      $quiz_data = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}mlw_quizzes WHERE deleted = 0 ORDER BY quiz_id DESC", ARRAY_A );
    } else {
      $export_ID = intval( $_POST["export"] );
      $quiz_data = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}mlw_quizzes WHERE deleted = 0 AND quiz_id = %d ORDER BY quiz_id DESC", $export_ID ), ARRAY_A );
    }
  }

  // Cycle through each quiz and retrieve all of quiz's questions
  foreach ( $quiz_data as $key => $quiz ) {
    $question_data = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}mlw_questions WHERE deleted = 0 AND quiz_id = %d", $quiz["quiz_id"] ), ARRAY_A );
    $quiz_data[ $key ]["questions"] = $question_data;
  }

  // Encode data into json
  $json_quizzes = json_encode( $quiz_data );

  // Put json into export file
  $time = time();
  file_put_contents( plugin_dir_path( __FILE__ ) . "../exports/exported_quizzes_$time.json", $json_quizzes, FILE_APPEND );

  // Return file URL to front-end for download
  $file = plugin_dir_url( __FILE__ ) . "../exports/exported_quizzes_$time.json";
  echo json_encode( array( 'filename' => $file ) );

  die();
}

?>
