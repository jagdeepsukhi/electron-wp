<?php
/**
 * Plugin Name: QSM - Export & Import
 * Plugin URI: https://quizandsurveymaster.com
 * Description: Export and import your quizzes and surveys
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com
 * Version: 0.1.0
 *
 * @author Frank Corso
 * @version 0.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 0.1.0
  */
class QSM_Export_Import {

    /**
  	 * Version Number
  	 *
  	 * @var string
  	 * @since 0.1.0
  	 */
  	public $version = '0.1.0';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 0.1.0
  	  * @uses QSM_Export_Import::load_dependencies() Loads required filed
  	  * @uses QSM_Export_Import::add_hooks() Adds actions to hooks and filters
  	  * @return void
  	  */
    function __construct() {
      $this->load_dependencies();
      $this->add_hooks();
      $this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 0.1.0
  	  * @return void
  	  */
    public function load_dependencies() {
      include( "php/addon-settings-tab-content.php" );
      include( "php/ajax.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 0.1.0
  	  * @return void
  	  */
    public function add_hooks() {
      add_action( 'admin_init', 'qsm_addon_export_import_register_addon_settings_tabs' );
      add_action( 'wp_ajax_qsm_export_json', 'qsm_addon_export_import_quiz_json' );
      add_action( 'wp_ajax_nopriv_qsm_export_json', 'qsm_addon_export_import_quiz_json' );
    }

    /**
     * Checks license
     *
     * Checks to see if license is active and, if so, checks for updates
     *
     * @since 1.3.0
     * @return void
     */
     public function check_license() {

       if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {

       	// load our custom updater
       	include( 'php/EDD_SL_Plugin_Updater.php' );
       }

      // retrieve our license key from the DB
      $analysis_data = get_option( 'qsm_addon_export_import_settings', '' );
      if ( isset( $logic_data["license_key"] ) ) {
        $license_key = trim( $analysis_data["license_key"] );
      } else {
        $license_key = '';
      }

     	// setup the updater
     	$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
     			'version' 	=> $this->version, 				// current version number
     			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
     			'item_name' => 'Export & Import', 	// name of this plugin
     			'author' 	=> 'Frank Corso'  // author of this plugin
     		)
     	);
     }
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_export_import_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_export_import = new QSM_Export_Import();
	} else {
		add_action( 'admin_notices', 'qsm_addon_export_import_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_export_import_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       0.1.0
 * @return      string The notice to display
 */
function qsm_addon_export_import_missing_qsm() {
  echo '<div class="error"><p>QSM - Export & Import requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
