/**
 * Sends the selected export data to called AJAX function
 */
function qsmLoadExport( exportID ) {
  var data = {
    action: 'qsm_export_json',
    export: exportID
  };

  jQuery.post( ajaxurl, data, function( response ) {
    qsmDownloadExport( JSON.parse( response ) );
  });
}

/**
 * Opens retrieved export file
 */
function qsmDownloadExport( exportFile ) {
  window.open( exportFile.filename );
}

jQuery( '.export-submit' ).on( 'click', function( event ) {
  qsmLoadExport( jQuery( '.export-select' ).val() );
});
