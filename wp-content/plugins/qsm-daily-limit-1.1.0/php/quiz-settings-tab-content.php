<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the quiz settings page
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_daily_limit_register_quiz_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_quiz_settings_tabs( "Daily Limit", 'qsm_addon_daily_limit_quiz_settings_tabs_content' );
  }
}

/**
 * Generates the content for your quiz settings tab
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_daily_limit_quiz_settings_tabs_content() {
  global $mlwQuizMasterNext;

  //If nonce is set and correct, save the form
  if ( isset( $_POST["daily_limit_nonce"] ) && wp_verify_nonce( $_POST['daily_limit_nonce'], 'daily_limit') ) {
    $daily_limit_data = array(
      'enabled' => sanitize_text_field( $_POST["daily_limit_enable"] ),
      'amount' => sanitize_text_field( $_POST["amount"] ),
      'text' => htmlspecialchars( stripslashes( $_POST["text"] ), ENT_QUOTES )
    );
    $mlwQuizMasterNext->pluginHelper->update_quiz_setting( "daily_limit_settings", $daily_limit_data );
    $mlwQuizMasterNext->alertManager->newAlert( 'Your Daily Limit settings has been saved successfully!', 'success' );
  }

  // Load the settings
  $daily_limit_data = $mlwQuizMasterNext->pluginHelper->get_quiz_setting( "daily_limit_settings" );
  $daily_limit_defaults = array(
    'enabled' => 'no',
    'amount' => 0,
    'text' => ''
  );
  $daily_limit_data = wp_parse_args( $daily_limit_data, $daily_limit_defaults );

  ?>
  <h2>Daily Limit</h2>
  <form action="" method="post">
    <button class="button-primary">Save Settings</button>
    <table class="form-table" style="width: 100%;">
      <tr valign="top">
        <th scope="row"><label for="daily_limit_enable">Enable a daily limit for this quiz/survey?</label></th>
        <td>
            <input type="radio" id="daily_limit_enable_radio1" name="daily_limit_enable" <?php checked( $daily_limit_data["enabled"], 'yes' ); ?> value='yes' /><label for="daily_limit_enable_radio1">Yes</label><br>
            <input type="radio" id="daily_limit_enable_radio2" name="daily_limit_enable" <?php checked( $daily_limit_data["enabled"], 'no' ); ?> value='no' /><label for="daily_limit_enable_radio2">No</label><br>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="amount">How many times can a user take the quiz/survey per day?</label></th>
        <td>
            <input type="number" id="amount" name="amount" value="<?php echo $daily_limit_data["amount"]; ?>">
        </td>
      </tr>
      <tr valign="top">
        <th scope="row"><label for="text">What text should be shown to the user who has already reach the daily limit?</label></th>
        <td>
            <?php wp_editor( htmlspecialchars_decode( $daily_limit_data["text"], ENT_QUOTES ), 'text' ); ?>
        </td>
      </tr>
    </table>
    <?php wp_nonce_field('daily_limit','daily_limit_nonce'); ?>
    <button class="button-primary">Save Settings</button>
  </form>
  <?php
}
?>
