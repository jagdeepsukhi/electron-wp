<?php
/**
 * Plugin Name: QSM - Daily Limit
 * Plugin URI: https://quizandsurveymaster.com
 * Description: This addon adds a daily limit to your quiz or survey
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com
 * Version: 1.1.0
 *
 * @author Frank Corso
 * @version 1.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 1.1.0
  */
class QSM_Daily_Limit {

    /**
  	 * Version Number
  	 *
  	 * @var string
  	 * @since 1.1.0
  	 */
  	public $version = '1.1.0';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 1.1.0
  	  * @uses QSM_Daily_Limit::load_dependencies() Loads required filed
  	  * @uses QSM_Daily_Limit::add_hooks() Adds actions to hooks and filters
  	  * @return void
  	  */
    function __construct() {
      $this->load_dependencies();
      $this->add_hooks();
      $this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 1.1.0
  	  * @return void
      */
    public function load_dependencies() {
      include( "php/addon-settings-tab-content.php" );
      include( "php/quiz-settings-tab-content.php" );
      include( "php/limit.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 1.1.0
  	  * @return void
      * @todo If you are not setting up the addon settings tab, the quiz settings tab, or variables, simply remove the relevant add_action below
  	  */
    public function add_hooks() {
      add_action( 'admin_init', 'qsm_addon_daily_limit_register_quiz_settings_tabs' );
      add_action( 'admin_init', 'qsm_addon_daily_limit_register_addon_settings_tabs' );
      add_filter( 'qmn_begin_shortcode', 'qsm_addon_daily_limit_begin_shortcode', 10, 3 );
      add_filter( 'qmn_begin_results', 'qsm_addon_daily_limit_begin_results', 10, 3 );
    }

    /**
     * Checks license
     *
     * Checks to see if license is active and, if so, checks for updates
     *
     * @since 1.1.0
     * @return void
     */
     public function check_license() {

       if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {

       	// load our custom updater
       	include( 'php/EDD_SL_Plugin_Updater.php' );
       }

      // retrieve our license key from the DB
      $daily_limit_data = get_option( 'qsm_addon_daily_limit_settings', '' );
      if ( isset( $daily_limit_data["license_key"] ) ) {
        $license_key = trim( $daily_limit_data["license_key"] );
      } else {
        $license_key = '';
      }

     	// setup the updater
     	$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
     			'version' 	=> $this->version, 				// current version number
     			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
     			'item_name' => 'Daily Limit', 	// name of this plugin
     			'author' 	=> 'Frank Corso'  // author of this plugin
     		)
     	);
     }
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_daily_limit_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_daily_limit = new QSM_Daily_Limit();
	} else {
		add_action( 'admin_notices', 'qsm_addon_daily_limit_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_daily_limit_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       1.1.0
 * @return      string The notice to display
 */
function qsm_addon_daily_limit_missing_qsm() {
  echo '<div class="error"><p>QSM - Daily Limit requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
