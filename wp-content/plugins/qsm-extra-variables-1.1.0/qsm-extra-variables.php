<?php
/**
 * Plugin Name: QSM - Extra Template Variables
 * Plugin URI: https://quizandsurveymaster.com
 * Description: Adds several new template variables to be used in your quizzes and surveys
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com
 * Version: 1.1.0
 *
 * @author
 * @version 1.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 1.1.0
  */
class QSM_Extra_Variables {

    /**
  	 * Version Number
  	 *
  	 * @var string
  	 * @since 1.1.0
  	 */
  	public $version = '1.1.0';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 1.1.0
  	  * @uses QSM_Extra_Variables::load_dependencies() Loads required filed
  	  * @uses QSM_Extra_Variables::add_hooks() Adds actions to hooks and filters
  	  * @return void
  	  */
    function __construct() {
      $this->load_dependencies();
      $this->add_hooks();
      $this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 1.1.0
  	  * @return void
      */
    public function load_dependencies() {
      include( "php/addon-settings-tab-content.php" );
      include( "php/variables.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 1.1.0
  	  * @return void
      */
    public function add_hooks() {
      add_action( 'admin_init', 'qsm_addon_extra_variables_register_addon_settings_tabs' );
      add_action( 'qmn_template_variable_list', 'qsm_addon_extra_variables_display_variables' );
      add_filter( 'mlw_qmn_template_variable_results_page', 'qsm_addon_extra_variables_group_by_answer', 10, 2 );
      add_filter( 'mlw_qmn_template_variable_results_page', 'qsm_addon_extra_variables_incorrect_answers', 10, 2 );
      add_filter( 'mlw_qmn_template_variable_results_page', 'qsm_addon_extra_variables_correct_answers', 10, 2 );
      add_filter( 'mlw_qmn_template_variable_results_page', 'qsm_addon_extra_variables_custom_message_points', 10, 2 );
      add_filter( 'mlw_qmn_template_variable_results_page', 'qsm_addon_extra_variables_custom_message_correct', 10, 2 );
    }

    /**
     * Checks license
     *
     * Checks to see if license is active and, if so, checks for updates
     *
     * @since 1.3.0
     * @return void
     */
     public function check_license() {

       if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {

       	// load our custom updater
       	include( 'php/EDD_SL_Plugin_Updater.php' );
       }

      // retrieve our license key from the DB
      $template_data = get_option( 'qsm_addon_extra_variables_settings', '' );
      if ( isset( $template_data["license_key"] ) ) {
        $license_key = trim( $template_data["license_key"] );
      } else {
        $license_key = '';
      }

     	// setup the updater
     	$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
     			'version' 	=> $this->version, 				// current version number
     			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
     			'item_name' => 'Extra Template Variables', 	// name of this plugin
     			'author' 	=> 'Frank Corso'  // author of this plugin
     		)
     	);
     }
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_extra_variables_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_extra_variables = new QSM_Extra_Variables();
	} else {
		add_action( 'admin_notices', 'qsm_addon_extra_variables_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_extra_variables_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       1.1.0
 * @return      string The notice to display
 */
function qsm_addon_extra_variables_missing_qsm() {
  echo '<div class="error"><p>QSM - Extra Template Variables requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
