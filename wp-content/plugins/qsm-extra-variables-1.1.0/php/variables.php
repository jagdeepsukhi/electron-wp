<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Generates your template variable
 *
 * @since 1.1.0
 * @param string $content The string from various templates including email and results pages
 * @param array $quiz_array An array of the results from the quiz/survey that was completed
 * @return string The string to be used in email, results page, social sharing, etc..
 */
function qsm_addon_extra_variables_group_by_answer( $content, $quiz_array ) {

  global $wpdb;

  // Retieve template
	$answer_template = $wpdb->get_var( $wpdb->prepare( "SELECT question_answer_template FROM {$wpdb->prefix}mlw_quizzes WHERE quiz_id=%d", $quiz_array['quiz_id'] ) );

  // While the variable is in content
	while ( false !== strpos( $content, '%QUESTION_ANSWER_GROUP%' ) ) {
		preg_match( "~%QUESTION_ANSWER_GROUP%(.*?)%/QUESTION_ANSWER_GROUP%~i", $content, $answer_text );
		$display_return = '';

    // Retieve questions and create array with ID as key
		$questions = $wpdb->get_results( $wpdb->prepare( "SELECT question_id, question_answer_info FROM {$wpdb->prefix}mlw_questions WHERE quiz_id=%d AND deleted=0", $quiz_array['quiz_id'] ) );
		$question_array = array();
		foreach( $questions as $question ) {
			$question_array[ $question->question_id ] = $question->question_answer_info;
		}

    // Cycle through question responses in results
		foreach ( $quiz_array['question_answers_array'] as $answer ) {
			if ( strtoupper( htmlspecialchars_decode( $answer[1], ENT_QUOTES ) ) == strtoupper( $answer_text[1] ) ) {
				$question_answer_display = htmlspecialchars_decode( $answer_template, ENT_QUOTES );
				$question_answer_display = str_replace( "%QUESTION%" , htmlspecialchars_decode( $answer[0], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_ANSWER%" , htmlspecialchars_decode( $answer[1], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER%" , htmlspecialchars_decode( $answer[2], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_COMMENTS%" , htmlspecialchars_decode( $answer[3], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER_INFO%" , htmlspecialchars_decode( $question_array[ $answer['id'] ], ENT_QUOTES ), $question_answer_display );
				$display_return .= $question_answer_display . "<br />";
			}
		}
		$content = str_replace( $answer_text[0] , $display_return, $content);
	}

  // Returns the content
  return $content;
}

/**
 * Generates your template variable
 *
 * @since 1.1.0
 * @param string $content The string from various templates including email and results pages
 * @param array $quiz_array An array of the results from the quiz/survey that was completed
 * @return string The string to be used in email, results page, social sharing, etc..
 */
function qsm_addon_extra_variables_incorrect_answers( $content, $quiz_array ) {

  // While the variable is in content
  while ( false !== strpos( $content, '%QUESTION_ANSWER_INCORRECT%' ) ) {
    $display_return = '';
    global $wpdb;

    // Retieve template
  	$template = $wpdb->get_var( $wpdb->prepare( "SELECT question_answer_template FROM {$wpdb->prefix}mlw_quizzes WHERE quiz_id=%d", $quiz_array['quiz_id'] ) );

    // Retieve questions and create array with ID as key
		$questions = $wpdb->get_results( $wpdb->prepare( "SELECT question_id, question_answer_info FROM {$wpdb->prefix}mlw_questions WHERE quiz_id=%d AND deleted=0", $quiz_array['quiz_id'] ) );
		$question_array = array();
		foreach( $questions as $question ) {
			$question_array[ $question->question_id ] = $question->question_answer_info;
		}

    // Cycle through question responses in results
    foreach ( $quiz_array['question_answers_array'] as $answer ) {
      if ( 'incorrect' == $answer["correct"] ) {
        $question_answer_display = htmlspecialchars_decode( $template, ENT_QUOTES );
				$question_answer_display = str_replace( "%QUESTION%" , htmlspecialchars_decode( $answer[0], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_ANSWER%" , htmlspecialchars_decode( $answer[1], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER%" , htmlspecialchars_decode( $answer[2], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_COMMENTS%" , htmlspecialchars_decode( $answer[3], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER_INFO%" , htmlspecialchars_decode( $question_array[ $answer['id'] ], ENT_QUOTES ), $question_answer_display );
				$display_return .= $question_answer_display . "<br />";
      }
    }
    $content = str_replace( '%QUESTION_ANSWER_INCORRECT%' , $display_return, $content );
  }

  // Returns the content
  return $content;
}

/**
 * Generates your template variable
 *
 * @since 1.1.0
 * @param string $content The string from various templates including email and results pages
 * @param array $quiz_array An array of the results from the quiz/survey that was completed
 * @return string The string to be used in email, results page, social sharing, etc..
 */
function qsm_addon_extra_variables_correct_answers( $content, $quiz_array ) {

  // While the variable is in content
  while ( false !== strpos( $content, '%QUESTION_ANSWER_CORRECT%' ) ) {
    $display_return = '';
    global $wpdb;

    // Retieve template
  	$template = $wpdb->get_var( $wpdb->prepare( "SELECT question_answer_template FROM {$wpdb->prefix}mlw_quizzes WHERE quiz_id=%d", $quiz_array['quiz_id'] ) );

    // Retieve questions and create array with ID as key
		$questions = $wpdb->get_results( $wpdb->prepare( "SELECT question_id, question_answer_info FROM {$wpdb->prefix}mlw_questions WHERE quiz_id=%d AND deleted=0", $quiz_array['quiz_id'] ) );
		$question_array = array();
		foreach( $questions as $question ) {
			$question_array[ $question->question_id ] = $question->question_answer_info;
		}

    // Cycle through question responses in results
    foreach ( $quiz_array['question_answers_array'] as $answer ) {
      if ( 'correct' == $answer["correct"] ) {
        $question_answer_display = htmlspecialchars_decode( $template, ENT_QUOTES );
				$question_answer_display = str_replace( "%QUESTION%" , htmlspecialchars_decode( $answer[0], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_ANSWER%" , htmlspecialchars_decode( $answer[1], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER%" , htmlspecialchars_decode( $answer[2], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%USER_COMMENTS%" , htmlspecialchars_decode( $answer[3], ENT_QUOTES ), $question_answer_display );
				$question_answer_display = str_replace( "%CORRECT_ANSWER_INFO%" , htmlspecialchars_decode( $question_array[ $answer['id'] ], ENT_QUOTES ), $question_answer_display );
				$display_return .= $question_answer_display . "<br />";
      }
    }
    $content = str_replace( '%QUESTION_ANSWER_CORRECT%' , $display_return, $content );
  }

  // Returns the content
  return $content;
}

/**
 * Generates your template variable
 *
 * @since 1.1.0
 * @param string $content The string from various templates including email and results pages
 * @param array $quiz_array An array of the results from the quiz/survey that was completed
 * @return string The string to be used in email, results page, social sharing, etc..
 */
function qsm_addon_extra_variables_custom_message_points( $content, $quiz_array ) {

  // While the variable is in content
  while ( false !== strpos($content, '%CUSTOM_MESSAGE_POINTS%') ) {

    $replace_text = '';

    // Separate passed messages
		preg_match( "~%CUSTOM_MESSAGE_POINTS%(.*?)%/CUSTOM_MESSAGE_POINTS%~i", $content, $answer_text );
    preg_match_all( "~(.*?):(.*?)-(.*?);~i", $answer_text[1], $matches );

    // Cycle through all messages
    $total_matches = count( $matches[0] );
    for( $i = 0; $i < $total_matches; $i++ ) {
      // If points are between message values, use message
      if ( $quiz_array["total_points"] >= $matches[2][ $i ] && $quiz_array["total_points"] <= $matches[3][ $i ] ) {
				$replace_text = $matches[1][ $i ];
      }
    }
		$content = str_replace( $answer_text[0], $replace_text, $content );
  }

  // Returns the content
  return $content;
}

/**
 * Generates your template variable
 *
 * @since 1.1.0
 * @param string $content The string from various templates including email and results pages
 * @param array $quiz_array An array of the results from the quiz/survey that was completed
 * @return string The string to be used in email, results page, social sharing, etc..
 */
function qsm_addon_extra_variables_custom_message_correct( $content, $quiz_array ) {

  // While the variable is in content
  while ( false !== strpos($content, '%CUSTOM_MESSAGE_CORRECT%') ) {

    $replace_text = '';

    // Separate passed messages
		preg_match( "~%CUSTOM_MESSAGE_CORRECT%(.*?)%/CUSTOM_MESSAGE_CORRECT%~i", $content, $answer_text );
    preg_match_all( "~(.*?):(.*?)-(.*?);~i", $answer_text[1], $matches );

    // Cycle through all messages
    $total_matches = count( $matches[0] );
    for( $i = 0; $i < $total_matches; $i++ ) {
      // If points are between message values, use message
      if ( $quiz_array["total_score"] >= $matches[2][ $i ] && $quiz_array["total_score"] <= $matches[3][ $i ] ) {
				$replace_text = $matches[1][ $i ];
      }
    }
		$content = str_replace( $answer_text[0], $replace_text, $content );
  }

  // Returns the content
  return $content;
}

/**
 * Displays the available variables in the variable list
 *
 * @since 1.1.0
 */
function qsm_addon_extra_variables_display_variables() {
  ?>
  <div class="template_variable">
    <span class="template_name">%QUESTION_ANSWER_CORRECT%</span> - <?php _e('This variable shows all questions and answers for questions the user got correct.', 'quiz-master-next'); ?>
  </div>
  <div class="template_variable">
    <span class="template_name">%QUESTION_ANSWER_INCORRECT%</span> - <?php _e('This variable shows all questions and answers for questions the user got incorrect.', 'quiz-master-next'); ?>
  </div>
  <div class="template_variable">
    <span class="template_name">%QUESTION_ANSWER_GROUP%%/QUESTION_ANSWER_GROUP%</span> - <?php _e('This variable shows all questions and answers for questions where the user selected the matching answer.', 'quiz-master-next'); ?>
  </div>
  <div class="template_variable">
    <span class="template_name">%CUSTOM_MESSAGE_POINTS%%/CUSTOM_MESSAGE_POINTS%</span> - <?php _e('Shows a custom message based on the amount of points a user has earned.', 'quiz-master-next'); ?>
  </div>
  <div class="template_variable">
    <span class="template_name">%CUSTOM_MESSAGE_CORRECT%%/CUSTOM_MESSAGE_CORRECT%</span> - <?php _e('Shows a custom message based on the score a user has earned.', 'quiz-master-next'); ?>
  </div>
  <?php
}

?>
