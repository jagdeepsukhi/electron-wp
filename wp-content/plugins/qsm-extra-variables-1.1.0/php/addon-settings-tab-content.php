<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the addon  settings page
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_extra_variables_register_addon_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_addon_settings_tab( "Extra Variables", 'qsm_addon_extra_variables_addon_settings_tabs_content' );
  }
}

/**
 * Generates the content for your addon settings tab
 *
 * @since 1.1.0
 * @return void
 */
function qsm_addon_extra_variables_addon_settings_tabs_content() {

  global $mlwQuizMasterNext;

  //If nonce is correct, update settings from passed input
  if ( isset( $_POST["extra_variables_nonce"] ) && wp_verify_nonce( $_POST['extra_variables_nonce'], 'extra_variables') ) {

    // Load previous license key
    $template_data = get_option( 'qsm_addon_extra_variables_settings', '' );
    if ( isset( $template_data["license_key"] ) ) {
      $license = trim( $template_data["license_key"] );
    } else {
      $license = '';
    }

    // Save settings
    $saved_license = sanitize_text_field( $_POST["license_key"] );
    $template_data = array(
      'license_key' => $saved_license
    );
    update_option( 'qsm_addon_extra_variables_settings', $template_data );

    // Checks to see if the license key has changed
    if ( $license != $saved_license ) {

      // Prepares data to activate the license
      $api_params = array(
        'edd_action'=> 'activate_license',
        'license' 	=> $saved_license,
        'item_name' => urlencode( 'Extra Template Variables' ), // the name of our product in EDD
        'url'       => home_url()
      );

      // Call the custom API.
      $response = wp_remote_post( 'https://quizandsurveymaster.com', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

      // If previous license key was entered
      if ( ! empty( $license ) ) {

        // Prepares data to deactivate changed license
        $api_params = array(
          'edd_action'=> 'deactivate_license',
          'license' 	=> $license,
          'item_name' => urlencode( 'Extra Template Variables' ), // the name of our product in EDD
          'url'       => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post( 'https://quizandsurveymaster.com', array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
      }
    }
    $mlwQuizMasterNext->alertManager->newAlert( 'Your settings has been saved successfully! You can now use the extra template variables when editing your quiz.', 'success' );
  }

  // Load settings
  $template_data = get_option( 'qsm_addon_extra_variables_settings', '' );
  $template_defaults = array(
    'license_key' => ''
  );
  $template_data = wp_parse_args( $template_data, $template_defaults );

  // Show any alerts from saving
  $mlwQuizMasterNext->alertManager->showAlerts();

  ?>
  <form action="" method="post">
    <table class="form-table" style="width: 100%;">
      <tr valign="top">
        <th scope="row"><label for="license_key">Addon License Key</label></th>
        <td><input type="text" name="license_key" id="license_key" value="<?php echo $template_data["license_key"]; ?>"></td>
      </tr>
    </table>
    <?php wp_nonce_field('extra_variables','extra_variables_nonce'); ?>
    <button class="button-primary">Save Changes</button>
  </form>
  <?php
}
?>
