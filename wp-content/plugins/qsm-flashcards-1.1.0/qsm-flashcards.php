<?php
/**
 * Plugin Name: QSM - Flashcards
 * Plugin URI: https://www.quizandsurveymaster.com/
 * Description: Create and display flashcards for users to study with!
 * Author: Frank Corso
 * Author URI: https://www.quizandsurveymaster.com/
 * Version: 1.1.0
 *
 * @author Frank Corso
 * @version 1.1.0
 * @package QSM-Flashcards
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * This class is the main class of the plugin
 *
 * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
 *
 * @since 1.0.0
 */
class QSM_Flashcards {

	/**
	 * Version Number
	 *
	 * @var string
	 * @since 1.0.0
	 */
	public $version = '1.0.2';

	/**
	 * Main Construct Function
	 *
	 * Call functions within class
	 *
	 * @since 1.0.0
	 * @uses QSM_Flashcards::load_dependencies() Loads required filed
	 * @uses QSM_Flashcards::add_hooks() Adds actions to hooks and filters
	 * @return void
	 */
	public function __construct() {
		$this->load_dependencies();
		$this->add_hooks();
		$this->check_license();
	}

	/**
	 * Load File Dependencies
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function load_dependencies() {
		include 'php/flashcards-admin.php';
		include 'php/flashcards-admin-edit.php';
		include 'php/addon-settings-tab-content.php';
		include 'php/shortcode.php';
	}

	/**
	 * Add Hooks
	 *
	 * Adds functions to relavent hooks and filters
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function add_hooks() {
		add_action( 'admin_init', 'qsm_addon_flashcards_register_addon_settings_tabs' );
		add_action( 'init', array( $this, 'register_post_type' ) );
		add_action( 'admin_menu', array( $this, 'setup_admin_menu' ) );
		add_action( 'admin_head', array( $this, 'admin_head' ), 900 );
		add_shortcode( 'flashcards', 'qsm_flashcards_shortcode' );
	}

	/**
	 * Registers the post type in WordPress
	 *
	 * @since 1.0.0
	 */
	public function register_post_type() {
		/* flashcards post type */
		$flashcards_args = array(
			'labels'          => array( 'name' => 'Flashcards' ),
			'public'          => false,
			'query_var'       => false,
			'rewrite'         => false,
			'capability_type' => 'post',
			'supports'        => array( 'title', 'editor' ),
			'can_export'      => false,
		);

		// Registers QSM logs post type with filtered $args.
		register_post_type( 'qsm_flashcards', apply_filters( 'qsm_flashcards_post_type_args', $flashcards_args ) );
	}

	/**
	 * Creates the menu and pages in WordPress admin
	 *
	 * @since 1.0.0
	 */
	public function setup_admin_menu() {
		if ( function_exists( 'add_menu_page' ) ) {
			add_menu_page( 'Flashcards', 'Flashcards', 'moderate_comments', 'qsm_addon_flashcards', 'qsm_addon_flashcards_generate_admin_page', 'dashicons-clipboard' );
			add_submenu_page( 'qsm_addon_flashcards', 'Edit Flashcards', 'Edit Flashcards', 'moderate_comments', 'qsm_addon_flashcards_edit', 'qsm_addon_flashcards_generate_admin_edit_page' );
		}
	}

	/**
	 * Removes Unnecessary Admin Page
	 *
	 * @since 1.0.0
	 */
	public function admin_head() {
		remove_submenu_page( 'qsm_addon_flashcards', 'qsm_addon_flashcards_edit' );
	}

	/**
	 * Checks license
	 *
	 * Checks to see if license is active and, if so, checks for updates
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function check_license() {

		if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
			// load our custom updater.
			include 'php/EDD_SL_Plugin_Updater.php';
		}

		// Retrieve our license key from the DB.
		$flashcard_data = get_option( 'qsm_addon_flashcard_settings', '' );
		if ( isset( $flashcard_data['license_key'] ) ) {
			$license_key = trim( $flashcard_data['license_key'] );
		} else {
			$license_key = '';
		}

		// Setup the updater.
		$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
			'version'   => $this->version,
			'license'   => $license_key,
			'item_name' => 'Flashcards',
			'author'    => 'Frank Corso',
		));
	}
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 1.0.0
 * @return void
 */
function qsm_addon_flashcards_load() {
	// Make sure QSM is active.
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_flashcards = new QSM_Flashcards();
	} else {
		add_action( 'admin_notices', 'qsm_addon_flashcards_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_flashcards_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since 1.0.0
 */
function qsm_addon_flashcards_missing_qsm() {
	echo '<div class="error"><p>QSM - Flashcards requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
