<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Creates the HTML for the shortcode
 *
 * @since 1.0.0
 * @param array $atts The attributes from the shortcode.
 * @return string The HTML to replace the shortcode with
 */
function qsm_flashcards_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'id' => 0,
	), $atts));

	$display         = '';
	$flashcards_id   = intval( $id );
	$flashcards_post = get_post( $flashcards_id );

	if ( $flashcards_post && 'qsm_flashcards' == $flashcards_post->post_type && 'publish' == $flashcards_post->post_status ) {
		$flashcards = get_post_meta( $flashcards_id, 'flashcards', true );
		$json = array( 'flashcards' => $flashcards );
		wp_enqueue_script( 'jquery_flip', plugins_url( '../js/jquery.flip.min.js', __FILE__ ), array( 'jquery' ), '1.0.0' );
		wp_enqueue_script( 'qsm_flashcards_script', plugins_url( '../js/qsm-flashcards.js', __FILE__ ), array( 'jquery', 'jquery_flip', 'underscore' ), '1.1.0' );
		wp_localize_script( 'qsm_flashcards_script', 'flashcardsObject', $json );
		wp_enqueue_style( 'qsm_flashcards_style', plugins_url( '../css/qsm-flashcards.css', __FILE__ ), array(), '1.1.0' );
		ob_start();
		?>
		<div class="cards">
			<?php
			foreach ( $flashcards as $flashcard ) {
				?>
				<div class="card" style="display:none;">
					<div class="front"><?php echo wpautop( htmlspecialchars_decode( $flashcard["front"], ENT_QUOTES ) ); ?></div>
					<div class="back"><?php echo wpautop( htmlspecialchars_decode( $flashcard["back"], ENT_QUOTES ) ); ?></div>
				</div>
				<?php
			}
		?>
		</div>
		<div class="cards-panel">
			<a href='javascript:();' class="btn button prev-card">Previous</a>
			<span>Card <span class="current-card"></span> of <span class="total-cards"></span></span>
			<a href='javascript:();' class="btn button next-card">Next</a>
		</div>
		<?php
		$display = ob_get_contents();
		ob_end_clean();
	}
	return $display;
}
?>