<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Generates the main admin page
 *
 * @since 1.0.0
 */
function qsm_addon_flashcards_generate_admin_page() {

	global $mlwQuizMasterNext;

	// Only let admins and editors see this page
	if ( ! current_user_can( 'moderate_comments' ) ) {
		return;
	}

	wp_enqueue_script( 'qsm_flashcards_admin_script', plugins_url( '../js/qsm-flashcards-admin.js' , __FILE__ ), array( 'jquery', 'jquery-ui-dialog', 'jquery-ui-button' ), '1.0.0' );
	wp_enqueue_style( 'qsm_jquery_redmond_theme', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/redmond/jquery-ui.css' );

	// Create new set of flashcards
	if ( isset( $_POST['qsm_new_flashcard_nonce'] ) && wp_verify_nonce( $_POST['qsm_new_flashcard_nonce'], 'qsm_new_flashcard' ) ) {
		$flashcards_name = htmlspecialchars( stripslashes( $_POST["flashcards_name"] ), ENT_QUOTES );
		$args = array(
			'post_type'    => 'qsm_flashcards',
			'post_status'  => 'publish',
			'post_parent'  => 0,
			'post_title' => $flashcards_name,
			'post_content' => ''
		);
		$flashcards_id = wp_insert_post( $args );
		$mlwQuizMasterNext->alertManager->newAlert( 'Your new flashcards have been created!', 'success' );
	}

	// Delete flashcards
	if ( isset( $_POST['qsm_delete_flashcard_nonce'] ) && wp_verify_nonce( $_POST['qsm_delete_flashcard_nonce'], 'qsm_delete_flashcard' ) ) {
		$flashcards_id = intval( $_POST["delete_flashcard_id"] );
		$my_post = array(
			'ID'           => $flashcards_id,
			'post_status' => 'trash'
		);
		wp_update_post( $my_post );
		$mlwQuizMasterNext->alertManager->newAlert( 'Your new flashcards have been deleted!', 'success' );
	}
	?>
	<div class="wrap">
		<h2>Flashcards<a id="new-flashcard" href="#" class="add-new-h2">Add New</a></h2>
		<?php
		// Show any alerts from saving
		$mlwQuizMasterNext->alertManager->showAlerts();
		?>
		<table class="widefat">
			<thead>
				<tr>
					<th>Name</th>
					<th>Shortcode</th>
				</tr>
			</thead>
			<tbody id="the-list">
				<?php
				$my_query = new WP_Query( array( 'post_type' => 'qsm_flashcards', 'posts_per_page' => -1, 'post_status' => 'publish' ) );
				if ( $my_query->have_posts() ) {
					while ( $my_query->have_posts() ) {
						$my_query->the_post();
						$flashcards_id = get_the_ID();
						?>
						<tr data-id='<?php echo $flashcards_id; ?>'>
							<td>
								<?php the_title(); ?>
								<div class="row-actions">
									<a href="admin.php?page=qsm_addon_flashcards_edit&&flashcards_id=<?php echo $flashcards_id; ?>"><?php _e( 'Edit', 'quiz-master-next' ); ?></a> | 
									<a class="flashcards-delete" href="#"><?php _e( 'Delete', 'quiz-master-next' ); ?></a>
								</div>
							</td>
							<td>[flashcards id=<?php echo $flashcards_id; ?>]</td>
						</tr>
						<?php
					}
				}
				wp_reset_postdata();
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Name</th>
					<th>Shortcode</th>
				</tr>
			</tfoot>
		</table>
	</div>

	<!-- New Flashcards Dialog -->
	<div id="new_flashcards_dialog" title="Create new set of flashcards" style="display:none;">
		<form action="" method="post" class="qsm-dialog-form">
			<?php wp_nonce_field( 'qsm_new_flashcard','qsm_new_flashcard_nonce' ); ?>
			<h3>Create new set of flashcards</h3>
			<label>Name </label><input type="text" name="flashcards_name" value="" />
			<p class='submit'><input type='submit' class='button-primary' value='Create' /></p>
		</form>
	</div>

	<!-- Delete Flashcards Dialog -->
	<div id="delete_flashcards" title="<?php _e( 'Delete', 'quiz-master-next' ); ?>" style="display:none;">
		<form action='' method='post' class="qsm-dialog-form">
			<h3>Are you sure you want to delete this set of flashcards?</h3>
			<?php wp_nonce_field( 'qsm_delete_flashcard','qsm_delete_flashcard_nonce' ); ?>
			<input type='hidden' id='delete_flashcard_id' name='delete_flashcard_id' value='' />
			<p class='submit'><input type='submit' class='button-primary' value='<?php _e( 'Delete', 'quiz-master-next' ); ?>' /></p>
		</form>
	</div>
	<?php
}