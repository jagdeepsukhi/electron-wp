<?php
/**
 * This file handles the flashcards edit page.
 *
 * @package QSM-Flashcards
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Generates the main admin page
 *
 * @since 1.0.0
 */
function qsm_addon_flashcards_generate_admin_edit_page() {

	// Only let admins and editors see this page.
	if ( ! current_user_can( 'moderate_comments' ) ) {
		return;
	}

	$flashcards_id = intval( $_GET['flashcards_id'] );
	$flashcards_post = get_post( $flashcards_id );
	if ( $flashcards_post && 'qsm_flashcards' == $flashcards_post->post_type && 'publish' == $flashcards_post->post_status ) {
		$flashcards_title = $flashcards_post->post_title;
	} else {
		$flashcards_title = 'Invalid Flashcards. Please go back to Flashcards page and click on edit.';
	}

	$flashcards = get_post_meta( $flashcards_id, 'flashcards', true );
	wp_enqueue_script( 'qsm_flashcards_admin_script', plugins_url( '../js/qsm-flashcards-admin.js', __FILE__ ), array( 'jquery', 'wp-util', 'underscore', 'jquery-ui-dialog' ), '1.1.0' );
	wp_localize_script( 'qsm_flashcards_admin_script', 'flashcardsObject', array( 'flashcards_id' => $flashcards_id, 'flashcards' => $flashcards ) );	
	wp_enqueue_style( 'qsm_flashcards_style', plugins_url( '../css/qsm-flashcards.css', __FILE__ ) );
	wp_enqueue_editor();
	wp_enqueue_media();
	?>
	<h2><?php echo esc_html( $flashcards_title ); ?></h2>
	<div class="flashcard-messages"></div>
	<a href='#' class="save-flashcards button-primary">Save Flashcards</a> <a href='#' class="add-flashcard button-primary">Add Flashcard</a>
	<div id="flashcards"></div>
	<a href='#' class="save-flashcards button-primary">Save Flashcards</a> <a href='#' class="add-flashcard button-primary">Add Flashcard</a>

	<!-- Templates -->
	<script type="text/template" id="tmpl-flashcard">
		<div class="flashcard new" data-cardID='{{ data.cardID }}'>
			<textarea id='flashcard-{{ data.cardID }}-front'>{{{ data.frontContent }}}</textarea>
			<textarea id='flashcard-{{ data.cardID }}-back'>{{{ data.backContent }}}</textarea>
			<a href="#" class="delete-flashcard">Delete Flashcard</a>
		</div>
	</script>

	<script type="text/template" id="tmpl-flashcard-message">
		<div class="flashcard-message notice notice-{{ data.noticeClass }}">
			<p>{{ data.msg }}</p>
		</div>
	</script>
	<?php
}

add_action( 'wp_ajax_qsm_flashcards_save', 'qsm_flashcards_ajax_save' );
add_action( 'wp_ajax_nopriv_qsm_flashcards_save', 'qsm_flashcards_ajax_save' );

/**
 * Saves the contact form from the quiz settings tab
 *
 * @since 1.0.0
 * @return void
 */
function qsm_flashcards_ajax_save() {
	$return = array(
		'status' => false,
	);

	$flashcards           = $_POST['flashcards'];
	$flashcards_id        = intval( $_POST['flashcards_id'] );
	$sanitized_flashcards = array();
	$total_cards          = count( $flashcards );
	for ( $i = 0;  $i < $total_cards; $i++ ) {
		$sanitized_flashcards[] = array(
			'back'  => wp_kses_post( stripslashes( $flashcards[ $i ]['back'] ) ),
			'front' => wp_kses_post( stripslashes( $flashcards[ $i ]['front'] ) ),
		);
	}
	$status = update_post_meta( $flashcards_id, 'flashcards', $sanitized_flashcards );
	if ( $status ) {
		$return['status'] = true;
	}
	echo wp_json_encode( $return );
	die();
}
