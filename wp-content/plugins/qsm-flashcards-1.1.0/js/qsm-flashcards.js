/**
 * QSM - Flashcards
 */

var QSMFlashcards;
(function ($) {
	QSMFlashcards = {
		totalCards: 0,
		currentCard: 0,

		/**
		 * Goes to the next card
		 * 
		 * @uses QSMFlashcards.showCard
		 */
		nextCard: function() {
			QSMFlashcards.currentCard += 1;
			if ( QSMFlashcards.currentCard > QSMFlashcards.totalCards ) {
				QSMFlashcards.currentCard = QSMFlashcards.totalCards;
			}
			QSMFlashcards.showCard( QSMFlashcards.currentCard );
		},

		/**
		 * Goes to the previous card
		 * 
		 * @uses QSMFlashcards.showCard
		 */
		prevCard: function() {
			QSMFlashcards.currentCard -= 1;
			if ( QSMFlashcards.currentCard < 1 ) {
				QSMFlashcards.currentCard = 1;
			}
			QSMFlashcards.showCard( QSMFlashcards.currentCard );			
		},

		/**
		 * Shows the card
		 */
		showCard: function( card ) {

			// Hides all cards
			QSMFlashcards.hideAll();

			// Shows the correct card
			$( '.card:nth-child(' + card + ')' ).show();

			// Set the card # of # cards
			$( '.cards-panel .current-card' ).html( card );
			$( '.cards-panel .total-cards' ).html( QSMFlashcards.totalCards );

			// Hides previous button if first card
			if ( QSMFlashcards.currentCard == 1) {
				$('.prev-card').css('visibility','hidden');
			} else {
				$('.prev-card').css('visibility','visible');
			}

			// Hides next button if last card
			if ( QSMFlashcards.currentCard == QSMFlashcards.totalCards) {
				$('.next-card').css('visibility','hidden');
			} else {
				$('.next-card').css('visibility','visible');
			}
		},

		/**
		 * Hides all cards
		 */
		hideAll: function() {
			$( '.card' ).hide();
		}
	};
	$(function() {

		// Initializes the jquery flip plugin
		$( '.card' ).flip({
			speed: 400,
			axis: 'x'
		});
		QSMFlashcards.totalCards = flashcardsObject.flashcards.length;
		QSMFlashcards.nextCard();

		$( '.prev-card' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMFlashcards.prevCard();
		});

		$( '.next-card' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMFlashcards.nextCard();
		});
	});
}(jQuery));