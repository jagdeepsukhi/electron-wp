/**
 * QSM - Flashcards
 */

var QSMFlashcards;
(function ($) {
QSMFlashcards = {
	totalFlashcards: 0,
	deleteFlashcards: function( flashcards_id ) {
		$( '#delete_flashcard_id' ).val( flashcards_id );
		$( '#delete_flashcards' ).dialog( 'open' );
	},
	loadFlashcards: function() {
		$.each( flashcardsObject.flashcards, function( i, val ) {
			QSMFlashcards.addFlashcard( val );
		});
	},
	addFlashcard: function( values ) {
		QSMFlashcards.totalFlashcards++;
		cardID = QSMFlashcards.totalFlashcards;
		var template = wp.template( 'flashcard' );
		var templateValues = {
			'cardID': cardID,
			'frontContent': values.front,
			'backContent': values.back
		};
		$( '#flashcards' ).append( template( templateValues ) );
		var settings = {
			mediaButtons: true,
			tinymce:      {
				toolbar1: 'formatselect,bold,italic,bullist,numlist,link,blockquote,alignleft,aligncenter,alignright,strikethrough,hr,forecolor,pastetext,removeformat,codeformat,undo,redo'
			},
			quicktags:    true,
		};
		wp.editor.initialize( 'flashcard-' + cardID + '-front', settings );
		wp.editor.initialize( 'flashcard-' + cardID + '-back', settings );
		setTimeout( QSMFlashcards.removeNew, 250 );
	},
	newFlashcard: function() {
		values = {
			'front': 'Front',
			'back': 'Back'
		};
		QSMFlashcards.addFlashcard( values );
	},
	deleteFlashcard: function( $flashcardElement ) {
		$flashcardElement.addClass( 'deleting' );
		setTimeout( function() {
			$flashcardElement.remove();
		}, 250 );
	},
	saveFlashcards: function() {
		QSMFlashcards.displayAlert( 'Saving flashcards...', 'info' );
		$flashcards = $( '.flashcard' );
		flashcards = [];
		$.each( $flashcards, function( i, val ) {
			cardID = $( this ).data( 'cardid' );
			flashcard = {
				'front': wp.editor.getContent( 'flashcard-' + cardID + '-front' ),
				'back': wp.editor.getContent( 'flashcard-' + cardID + '-back' )
			};
			flashcards.push( flashcard );
		});
		var data = {
			'action': 'qsm_flashcards_save',
			'flashcards': flashcards,
			'flashcards_id': flashcardsObject.flashcards_id
		};

		$.post( ajaxurl, data, function( response ) {
			QSMFlashcards.savedFlashcards( JSON.parse( response ) );
		});
	},
	savedFlashcards: function( response ) {
		if ( response.status ) {
			QSMFlashcards.displayAlert( 'Flashcards have been saved!', 'success' );
		} else {
			QSMFlashcards.displayAlert( 'There was an error when saving the flashcards. Please try again.', 'error' );
		}		
	},
	displayAlert: function( msg, type ) {
		QSMFlashcards.clearAlerts();
		var template = wp.template( 'flashcard-message' );
		var values = {
			'msg': msg,
			'noticeClass': type
		};
		$( '.flashcard-messages' ).append( template( values ) );
	},
	clearAlerts: function() {
		$( '.flashcard-messages' ).empty();
	},
	removeNew: function() {
		$( '.flashcard' ).removeClass( 'new' );
	}
};
$(function() {

	// JS for admin page
	$( '#new_flashcards_dialog' ).dialog({
		autoOpen: false,
		buttons: {
			Cancel: function() {
				$( this ).dialog( 'close' );
			}
		}
	});
	$( '#delete_flashcards' ).dialog({
		autoOpen: false,
		buttons: {
			Cancel: function() {
				$( this ).dialog( 'close' );
			}
		}
	});
	$( '#new-flashcard' ).on( 'click', function( event ) {
		event.preventDefault();
		$( '#new_flashcards_dialog' ).dialog( 'open' );
	});
	$( '#the-list' ).on( 'click', '.flashcards-delete', function( event ) {
		event.preventDefault();
		QSMFlashcards.deleteFlashcards( $( this ).parents( 'tr' ).data( 'id' ) );
	});

	/**
	 * JS for edit page
	 */

	if ( 'undefined' != typeof( flashcardsObject ) ) {
		QSMFlashcards.loadFlashcards();
	}

	$( '.add-flashcard' ).on( 'click', function( event ) {
		event.preventDefault();
		QSMFlashcards.newFlashcard();
	});

	$( '.save-flashcards' ).on( 'click', function( event ) {
		event.preventDefault();
		QSMFlashcards.saveFlashcards();
	});

	$( '#flashcards' ).on( 'click', '.delete-flashcard', function( event ) {
		event.preventDefault();
		QSMFlashcards.deleteFlashcard( $( this ).parents( '.flashcard' ) );
	});
});
}(jQuery));
