<?php
// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

// Cycle through custom post type array, retreive all posts, delete each one
$qsm_flashcard_post_types = array( 'qsm_flashcards' );
foreach ( $qsm_flashcard_post_types as $post_type ) {
	$items = get_posts( array( 'post_type' => $post_type, 'post_status' => 'any', 'numberposts' => -1, 'fields' => 'ids' ) );
	if ( $items ) {
		foreach ( $items as $item ) {
			wp_delete_post( $item, true);
		}
	}
}

delete_option( 'qsm_addon_flashcard_settings' );
?>
