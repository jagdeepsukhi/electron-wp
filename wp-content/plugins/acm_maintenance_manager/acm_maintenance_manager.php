<?php

/*
Plugin Name: ACM Maintenance Manager
Plugin URI: 
Description: ACM Maintenance Manager application (This plugin only supports to wpdatatable version 2.1)
Version: 2.1
Author: MB
Author URI:  
*/

define("ACM_PLUGIN_PATH", get_bloginfo('url')."/wp-content/plugins/acm_maintenance_manager/");

include(dirname(__FILE__)."/acm_functions.php");
include(dirname(__FILE__)."/acm_templates.php");

$en_mb_datatabels = array(); 
$en_mb_datatabels ['_en_mb_wp_datatable_1'] 	= "Facility Equipment Tracker";
$en_mb_datatabels ['_en_mb_wp_datatable_2'] 	= "Equipment Types Master Table"; 
$en_mb_datatabels ['_en_mb_wp_datatable_3'] 	= "Facility Equipment Master Table"; 
$en_mb_datatabels ['_en_mb_wp_datatable_4'] 	= "Services Types Master Table"; 
$en_mb_datatabels ['_en_mb_wp_datatable_5'] 	= "WP Users Master Table";
$en_mb_datatabels ['_en_mb_wp_datatable_6'] 	= "Maintenance Records Master Table"; 
$en_mb_datatabels ['_en_mb_wp_datatable_7'] 	= "Maintenance Records Details (Automobile/Fueled Support Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_8'] 	= "Maintenance Records Details ( Inspected/Instrumentation/Non Calibrated Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_9'] 	= "Facility Equipment Status Master Table";
$en_mb_datatabels ['_en_mb_wp_datatable_10'] 	= "Facility Equipment Description Master Table";
$en_mb_datatabels ['_en_mb_wp_datatable_11']	= "Service Explanation Master Table";
$en_mb_datatabels ['_en_mb_wp_datatable_12'] 	= "Calibration Records Details (Instrumentation/Inspacted Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_13'] 	= "Equipment Detail (Automobile)";
$en_mb_datatabels ['_en_mb_wp_datatable_14'] 	= "Equipment Detail (Fueled Support Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_15'] 	= "Equipment Detail (Non Calibrated Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_16'] 	= "Equipment Detail (Inspected Equipment)";
$en_mb_datatabels ['_en_mb_wp_datatable_17']	= "Equipment Detail (Instrumentation)";
$en_mb_datatabels ['_en_mb_wp_datatable_18'] 	= "Equipment Group Templates Shortcode Settings";
$en_mb_datatabels ['_en_mb_wp_datatable_19'] 	= "All WP Data Tables";
$en_mb_datatabels ['_en_mb_wp_datatable_20'] 	= "Tracking Active Maintenance Items";
$en_mb_datatabels ['_en_mb_wp_datatable_21'] 	= "Tracking Active Calibration Items";
$en_mb_datatabels ['_en_mb_wp_datatable_22'] 	= "Maintenance Equipment History";
define("EN_MB_ALL_TABLES", $en_mb_datatabels);

$en_mb_all_pages = array(); 
$en_mb_all_pages ['_en_mb_page_1'] 	= "ACM Facility Equipment Tracker";
$en_mb_all_pages ['_en_mb_page_2'] 	= "ACM Facility Equipment Detail"; 
$en_mb_all_pages ['_en_mb_page_3'] 	= "ACM Maintenance Records"; 
$en_mb_all_pages ['_en_mb_page_4'] 	= "ACM Calibration Records"; 
$en_mb_all_pages ['_en_mb_page_5'] 	= "ACM Active Maintenance Alert";
$en_mb_all_pages ['_en_mb_page_6'] 	= "ACM Active Calibration/Inspection Alert"; 
$en_mb_all_pages ['_en_mb_page_7'] 	= "ACM Equipment History Audit";
$en_mb_all_pages ['_en_mb_page_8'] 	= "ACM Equipment Manager Settings";
$en_mb_all_pages ['_en_mb_page_9'] 	= "ACM Equipment Type Relation Settings";
$en_mb_all_pages ['_en_mb_page_10'] = "ACM Equipment Types";
$en_mb_all_pages ['_en_mb_page_11']	= "ACM Services Types";
$en_mb_all_pages ['_en_mb_page_12'] = "ACM Equipment Descriptions";
$en_mb_all_pages ['_en_mb_page_13'] = "ACM Equipment Status";
$en_mb_all_pages ['_en_mb_page_14'] = "ACM Service Explanation";
$en_mb_all_pages ['_en_mb_page_15'] = "Fuel Log";
define("EN_MB_ALL_PAGES", $en_mb_all_pages); 

$en_mb_all_pages_template = array();
$en_mb_all_pages_template ['_en_mb_page_1'] 	= "page-templates/facility-equipment.php";
$en_mb_all_pages_template ['_en_mb_page_2'] 	= "page-templates/equipment-detail-page.php"; 
$en_mb_all_pages_template ['_en_mb_page_3'] 	= "page-templates/mantenance-records-details.php"; 
$en_mb_all_pages_template ['_en_mb_page_4'] 	= "page-templates/calibration-records-details.php"; 
$en_mb_all_pages_template ['_en_mb_page_5'] 	= "page-templates/active-alert-equipment-maintenance-Items.php";
$en_mb_all_pages_template ['_en_mb_page_6'] 	= "page-templates/active-alert-calibration-Inspection-Items.php"; 
$en_mb_all_pages_template ['_en_mb_page_7'] 	= "page-templates/mantenance-history-template.php";
$en_mb_all_pages_template ['_en_mb_page_8'] 	= "page-templates/equipment-manager-settings.php";
$en_mb_all_pages_template ['_en_mb_page_9'] 	= "page-templates/equipment-type-relation-settings.php";
$en_mb_all_pages_template ['_en_mb_page_10'] 	= "page-templates/equipment-types-template.php";
$en_mb_all_pages_template ['_en_mb_page_11']	= "page-templates/service-types-template.php";
$en_mb_all_pages_template ['_en_mb_page_12'] 	= "page-templates/equipment-description-template.php";
$en_mb_all_pages_template ['_en_mb_page_13'] 	= "page-templates/equipment-status-template.php";
$en_mb_all_pages_template ['_en_mb_page_14'] 	= "page-templates/service-explanation-template.php";
define("EN_MB_ALL_PAGES_TEMPLATES", $en_mb_all_pages_template);

$en_mb_all_templates = array();
$en_mb_all_templates ['page-templates/active-alert-calibration-Inspection-Items.php'] 	= 'EM Active Alert Calibration/Inspection';
$en_mb_all_templates ['page-templates/active-alert-equipment-maintenance-Items.php'] 	= 'EM Active Alert Equipment Maintenance Items';
$en_mb_all_templates ['page-templates/calibration-records-details.php']  				= 'EM Calibration Record Details';
$en_mb_all_templates ['page-templates/mantenance-records-details.php']  				= 'EM Maintenance Records Details';
$en_mb_all_templates ['page-templates/equipment-detail-page.php']  						= 'EM Equimpent Detail Page';
$en_mb_all_templates ['page-templates/equipment-manager-settings.php']  				= 'EM Equipment Manager Settings';
$en_mb_all_templates ['page-templates/equipment-type-relation-settings.php']  			= 'EM Equipment Type Relation Settings';
$en_mb_all_templates ['page-templates/facility-equipment.php']  						= 'EM Facility Equipment';
$en_mb_all_templates ['page-templates/mantenance-history-template.php']  				= 'EM Maintenance History Audit';
$en_mb_all_templates ['page-templates/equipment-types-template.php']  					= 'EM Equipment Types';
$en_mb_all_templates ['page-templates/service-types-template.php']  					= 'EM Service Types';
$en_mb_all_templates ['page-templates/equipment-description-template.php']  			= 'EM Equipment Description';
$en_mb_all_templates ['page-templates/equipment-status-template.php']  					= 'EM Equipment Status';
$en_mb_all_templates ['page-templates/service-explanation-template.php']  				= 'EM Service Explanation'; 
define("EN_MB_ALL_TEMPLATES", $en_mb_all_templates); 


////////////////////////////////////////////////////////////////////////*/
/*************Register admin menu setting page**************************/
//////////////////////////////////////////////////////////////////////*/

function en_mb_admin_menu()
{
    //add_menu_page('Electron', 'Electron', 'manage_options', 'en-admin');
    add_submenu_page('en-admin', 'ACM WP Data Tables Relation', 'ACM Equipment Manager', 'manage_options', 'acm','en_mb_admin_setting_page');
}

if ( is_admin() ) {
    add_action('admin_menu', 'en_mb_admin_menu',11);
}

function en_mb_admin_setting_page(){
 
	if(!empty($_POST) && isset($_POST['submit_tables'])){

		foreach (EN_MB_ALL_TABLES as $slug => $tableName) {
			update_option( $slug, $_POST[$slug]);
		}
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}

	if(!empty($_POST) && isset($_POST['submit_pages'])){
		
		$optionFlag=false;

		$TemplatesArr=EN_MB_ALL_PAGES_TEMPLATES;

		foreach (EN_MB_ALL_PAGES as $option_key => $page) {
			
			update_option( $option_key, $_POST[$option_key]);
			
			if(! empty($_POST[$option_key]) && $option_key!='_en_mb_page_15' ){

				update_post_meta( $_POST[$option_key], '_wp_page_template', $TemplatesArr[$option_key] );

			
			}

			if(empty($_POST[$option_key]) ){
			
				$optionFlag=true;
			
			}
		}

		if($optionFlag!=true){
		
			update_option( '_en_mb_page_configrations', 1);

		}else{
			
			update_option( '_en_mb_page_configrations', 0);
		
		}
		
		$mess="<p style='color:green;font-weight:bold;'>Settings has been saved.</p>";
	}

	if($_GET['action']=='shortcode_mapping'){

		$response = en_mb_shortcode_mapping();
		
		include(dirname(__FILE__)."/admin-templates/acm_admin_shortcode_mapping.php");

	}elseif($_GET['action']=='wpdatatable_import'){

		$response = en_mb_import_wp_data_tables();
		
		include(dirname(__FILE__)."/admin-templates/acm_admin_import_wpdatatables.php");

	}elseif($_GET['action']=='refresh_wpdatatables'){

		$response = en_mb_refresh_wp_data_tables();
		
		include(dirname(__FILE__)."/admin-templates/acm_admin_refresh_wpdatatables.php");

	}elseif($_GET['action']=='create_pages'){

		$response = en_mb_create_required_pages();
		
		include(dirname(__FILE__)."/admin-templates/acm_create_pages.php");

	}elseif($_GET['action']=='import_source_tables'){

		$response = en_mb_import_source_tables();
		
		include(dirname(__FILE__)."/admin-templates/acm_import_source_tables.php");

	}elseif($_GET['action']=='create_menu'){

		$response =  en_mb_create_frontend_menu();
		
		include(dirname(__FILE__)."/admin-templates/acm_create_menu.php");

	}elseif($_GET['action']=='create_caps'){

		$response = en_mb_create_roles();

		$response = en_mb_add_caps_for_all_roles();
		
		include(dirname(__FILE__)."/admin-templates/acm_create_caps.php");

	}elseif($_GET['action']=='remove_config'){

		$response = en_mb_remove_all_configrattions();
		
		include(dirname(__FILE__)."/admin-templates/acm_remove_config.php");

	}else{

		include(dirname(__FILE__)."/admin-templates/acm_admin_settings.php");
		
	}

}

function en_mb_get_pages_drop_down($opt_name){
	 
	 $args = array(
			'sort_order' 	=> 'ASC',
			'sort_column' 	=> 'post_title',
			'hierarchical' 	=> 1,
			'parent' 		=> -1,
			'exclude_tree' 	=> '',
			'number' 		=> '',
			'offset' 		=> 0,
			'post_type' 	=> 'page',
			'post_status' 	=> 'publish'
			); 
	$pages = get_pages($args); 

	$selectbox ='<select name="'.$opt_name.'">';
	$option ="<option value=''>Select page</option>";
	foreach ($pages as $key => $value) {
		if(get_option($opt_name)==$value->ID){
			$select="selected";
		}else{
			$select="";
		}
		$option.='<option value="'.$value->ID.'" '.$select.'>'.$value->post_title.'</option>';
	}
	$selectbox .= $option.'</select>';
	return $selectbox;

}

/*****************Import wp data tables**********************/

function en_mb_search_meta($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, en_mb_search_meta($subarray, $key, $value));
        }
    }

    return $results;
}

function en_mb_create_data_types($count){
	$data_type=array();
	for ($i=0; $i <= $count; $i++) { 
		 $data_type[]='%s';
	}
	return $data_type;
}

function en_mb_import_wp_data_tables(){
	$response=array();

	if(get_option( '_en_mb_import_wp_data_tables')==1){
		$response[] = "<p><strong style='color:red'>Wp Data tables already imported.</strong><p>";
		return $response;
	}

	$wpdatatables 				= file_get_contents(dirname(__FILE__).'/json/wp_wpdatatables.json');
	$wpdatatablesArr			= json_decode($wpdatatables, true); 
	
	$wpdatatables_columns 		= file_get_contents(dirname(__FILE__).'/json/wp_wpdatatables_columns.json');
	$wpdatatables_columnsArr	= json_decode($wpdatatables_columns, true); 
	
	global $wpdb;

	$table_number=1;
	foreach ($wpdatatablesArr as $dtkey => $table) {

		$old_table_id 	= $table['id'];

		$table_name 	= $table['title'];

		unset($table['id']);

		$dataType= en_mb_create_data_types(count($table));

		$contactus_table = $wpdb->prefix."wpdatatables";

		if($wpdb->insert( $contactus_table, $table,$dataType)){
			$new_table_id = $wpdb->insert_id;

			$meta_columns = en_mb_search_meta($wpdatatables_columnsArr, 'table_id',$old_table_id);

			en_mb_insert_wpdatatables_columns($meta_columns,$new_table_id);

			update_option( '_en_mb_wp_datatable_'.$table_number, $new_table_id );

			$response[] = "<p><strong style='color:green'>Table Name: $table_name, Old Table ID: $old_table_id,  New Table ID: $new_table_id : Configured successfully.</strong><p>";
		}else {
			$response[] = "<p><strong style='color:red'>Table Name: $table_name, Old Table ID: $old_table_id : Error in configuration.</strong><p>";
		}
		$table_number++;  
		 
	}
	update_option( '_en_mb_import_wp_data_tables', 1 );
	return $response;
}

function en_mb_insert_wpdatatables_columns($meta_columns,$table_id){
	global $wpdb;
	foreach ($meta_columns as $mkey => $metaArr) {
		
		unset($metaArr['id']);

		$metaArr['table_id']=$table_id;

		$dataType = en_mb_create_data_types(count($metaArr));

		$wpdatatables_columns		= $wpdb->prefix."wpdatatables_columns"; 
		$wpdb->insert( $wpdatatables_columns, $metaArr,$dataType);  

	}
}

function is_en_mb_tables_exist($table){
	global $wpdb;
	if($wpdb->get_var("SHOW TABLES LIKE '$table'") ==$table) {
	 	return true;
	}
	else{
		return false;
	}
}

/***********************************Shortcode Mapping**********************************************/

function en_mb_shortcode_mapping(){
	
	global $wpdb;

	$response=array();

	if(get_option('_en_mb_shortcode_mapping')==1){
		 
		$response[] = "<p><strong style='color:red'>Shortcode Mapping already done.</strong><p>";
		return $response;
	}

	if(is_en_mb_tables_exist('en_mb_equipment_types')!=true || is_en_mb_tables_exist('en_mb_equipment_group_templates')!=true){

		$response[] = "<p><strong style='color:red'>It seems that source tables are not imported. Table: en_mb_equipment_types or en_mb_equipment_group_templates is missing.</strong><p>";
		return $response;
	}
	
	$results = $wpdb->get_results("SELECT * FROM en_mb_equipment_types order by id ASC");
 
	foreach ($results as $dtkey => $e_type) {

		$equipment_type_id 	= $e_type->id;
		$equipment_type 	= $e_type->equipment_type;

		if($equipment_type_id ==1){

			$pages['facility_equipment']					= get_option('_en_mb_wp_datatable_1');
			$pages['detail_view']							= get_option('_en_mb_wp_datatable_13');
			$pages['fuel_tracker']							= '';
			$pages['maintenance_records']					= get_option('_en_mb_wp_datatable_7');
			$pages['calibration_inspection_records']		= '';
			$pages['active_maintenance_items']				= get_option('_en_mb_wp_datatable_20');
			$pages['active_calibration_inspection_items']	= '';

		}else if($equipment_type_id ==2){

			$pages['facility_equipment']					= get_option('_en_mb_wp_datatable_1');
			$pages['detail_view']							= get_option('_en_mb_wp_datatable_14');
			$pages['fuel_tracker']							= '';
			$pages['maintenance_records']					= get_option('_en_mb_wp_datatable_7');
			$pages['calibration_inspection_records']		= '';
			$pages['active_maintenance_items']				= get_option('_en_mb_wp_datatable_20');
			$pages['active_calibration_inspection_items']	= '';

		}else if($equipment_type_id ==3){

			$pages['facility_equipment']					= get_option('_en_mb_wp_datatable_1');
			$pages['detail_view']							= get_option('_en_mb_wp_datatable_17');
			$pages['fuel_tracker']							= '';
			$pages['maintenance_records']					= get_option('_en_mb_wp_datatable_8');
			$pages['calibration_inspection_records']		= get_option('_en_mb_wp_datatable_12');
			$pages['active_maintenance_items']				= get_option('_en_mb_wp_datatable_20');
			$pages['active_calibration_inspection_items']	= get_option('_en_mb_wp_datatable_21');
			
		}else if($equipment_type_id ==4){

			$pages['facility_equipment']					= get_option('_en_mb_wp_datatable_1');
			$pages['detail_view']							= get_option('_en_mb_wp_datatable_15');
			$pages['fuel_tracker']							= '';
			$pages['maintenance_records']					= get_option('_en_mb_wp_datatable_8');
			$pages['calibration_inspection_records']		= '';
			$pages['active_maintenance_items']				= get_option('_en_mb_wp_datatable_20');
			$pages['active_calibration_inspection_items']	= '';
			
		}else if($equipment_type_id ==6){

			$pages['facility_equipment']					= get_option('_en_mb_wp_datatable_1');
			$pages['detail_view']							= get_option('_en_mb_wp_datatable_16');
			$pages['fuel_tracker']							= '';
			$pages['maintenance_records']					= get_option('_en_mb_wp_datatable_8');
			$pages['calibration_inspection_records']		= get_option('_en_mb_wp_datatable_12');
			$pages['active_maintenance_items']				= get_option('_en_mb_wp_datatable_20');
			$pages['active_calibration_inspection_items']	= get_option('_en_mb_wp_datatable_21');
			
		}

		// Set Last Due Date in planner.
		$wpdb->update('en_mb_equipment_group_templates',$pages,array('group_template'=>$equipment_type_id));
 
		$response[] = "<p><strong style='color:green'>Shortcode Mapping done for Equipment type: $equipment_type & all pages.</strong><p>";
		 	 
	}
	update_option( '_en_mb_shortcode_mapping', 1 );
	return $response;
}

/***********************************en_mb_refresh_wp_data_tables**********************************************/
function en_mb_refresh_wp_data_tables(){
	
	global $wpdb;
	$response=array();
	if(get_option('_en_mb_refresh_wp_data_tables_columns')==1){
		$response[] = "<strong style='color:red'>wpdatatables already refreshed.</strong>";
		return $response;
	}

	if(get_option('_en_mb_import_wp_data_tables')!=1){
		$response[] = "<strong style='color:red'>Import the wpdatatables before refresh the configurations.</strong>";
		return $response;
	}

	//Configurations for Facility Equipment Tracker Main Table
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_1'),get_option('_en_mb_wp_datatable_2'),'equipment_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_1'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_1'),get_option('_en_mb_wp_datatable_9'),'status');

	//Configurations for Facility Equipment Tracker Main Table
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_3'),get_option('_en_mb_wp_datatable_2'),'equipment_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_3'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_3'),get_option('_en_mb_wp_datatable_9'),'status');

	//Maintenance Records Details (Automobile/Fueled Support Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_7'),get_option('_en_mb_wp_datatable_4'),'service_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_7'),get_option('_en_mb_wp_datatable_5'),'recorded_by');	

	//Maintenance Records Details ( Inspected/Instrumentation/Non Calibrated Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_8'),get_option('_en_mb_wp_datatable_4'),'service_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_8'),get_option('_en_mb_wp_datatable_5'),'recorded_by');

	//Calibration Records Details (Instrumentation/Inspacted Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_12'),get_option('_en_mb_wp_datatable_5'),'recorded_by');

	//Equipment Detail (Automobile)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_13'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_13'),get_option('_en_mb_wp_datatable_9'),'status');

	//Equipment Detail (Fueled Support Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_14'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_14'),get_option('_en_mb_wp_datatable_9'),'status');	

	//Equipment Detail (Non Calibrated Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_15'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_15'),get_option('_en_mb_wp_datatable_9'),'status');

	//Equipment Detail (Inspected Equipment)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_16'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_16'),get_option('_en_mb_wp_datatable_9'),'status');

	//Equipment Detail (Instrumentation)
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_17'),get_option('_en_mb_wp_datatable_10'),'equipment_description');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_17'),get_option('_en_mb_wp_datatable_9'),'status');

	//Equipment Group Templates Shortcode Settings
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_2'),'group_template');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'facility_equipment');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'detail_view');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'fuel_tracker');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'maintenance_records');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'calibration_inspection_records');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'active_maintenance_items');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_18'),get_option('_en_mb_wp_datatable_19'),'active_calibration_inspection_items');

	//Tracking Active Maintenance Items
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_20'),get_option('_en_mb_wp_datatable_2'),'equipment_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_20'),get_option('_en_mb_wp_datatable_9'),'activity_status');


	//Tracking Active Calibration Items
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_21'),get_option('_en_mb_wp_datatable_2'),'equipment_type');
	en_mb_update_column_config(get_option('_en_mb_wp_datatable_21'),get_option('_en_mb_wp_datatable_9'),'activity_status');

	//Update existing Facility equipment items sub links

	$etsql		="select * from en_mb_equipment_types";
	$e_t_resuls	= $wpdb->get_results($etsql);
	foreach ($e_t_resuls as $key => $et) {

		$type_id= $et->id;
		$action_links 	= get_action_links_trigger($type_id);
		$hiddenPre		= '<input class="action_asset_id" type="hidden" value="';
		$hiddenPost 	= '">';
		$assetSQL 		= "CONCAT('".$hiddenPre."',en_mb_facility_equipment.asset_id,'".$hiddenPost."')"; 
		$actionsql 		= "UPDATE `en_mb_facility_equipment` SET action=CONCAT('".$action_links."',".$assetSQL.") WHERE equipment_type ='".$type_id."'";
		$wpdb->query($actionsql);

	}

	//update existing links

	$sql = "SELECT id FROM en_mb_calibrations";
	$calibrationRecords = $wpdb->get_results($sql);
	foreach ($calibrationRecords  as $key => $record) {
		$wpdb->update(
						'en_mb_calibrations', 
						array(
							 
							'calibration_documentations'=>"<a class='documents-btn' title='Documents' rel='".$record->id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						),   
						array('id'=>$record->id)
					);
	}



	$sql = "SELECT id FROM en_mb_facility_equipment";
	$equipmentRecords = $wpdb->get_results($sql);
	foreach ($equipmentRecords  as $key => $record) {
		$wpdb->update(
						'en_mb_facility_equipment', 
						array(
							 
							'general_documentation'=>"<a class='documents-btn' title='Documents' rel='".$record->id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						),   
						array('id'=>$record->id)
					);
	}

	$sql = "SELECT id FROM en_mb_maintenance_records";
	$maintenanceRecords = $wpdb->get_results($sql);
	foreach ($maintenanceRecords  as $key => $record) {
		$wpdb->update(
						'en_mb_maintenance_records', 
						array(
							
							'service_documentations'=>"<a class='documents-btn' title='Documents' rel='".$record->id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						),   
						array('id'=>$record->id)
					);
	}

	$response[] = "<p><strong style='color:green'>Wpdatatables configurations refreshed.</strong><p>";
	update_option( '_en_mb_refresh_wp_data_tables_columns', 1 );
	return $response;
}

function en_mb_get_old_column_config($table_id,$column){
	global $wpdb;
	
	$advanced_settings_Arr= array();

	$tableName=$wpdb->prefix."wpdatatables_columns";
	
	$sql="SELECT advanced_settings from $tableName WHERE table_id='$table_id' AND orig_header='$column'";
	
	$advanced_settings =$wpdb->get_row($sql);
	
	if(!empty($advanced_settings->advanced_settings)){
		$advanced_settings_Arr= json_decode($advanced_settings->advanced_settings);
	}
	
	return $advanced_settings_Arr;
	
}

function en_mb_set_new_column_config($table_id,$column,$config){
	
	global $wpdb;
	
	$configJson = json_encode($config); 
	$tableName 	= $wpdb->prefix."wpdatatables_columns";
	$data 		= array('advanced_settings'=> $configJson);
 	$where 		= array('table_id'=>$table_id,'orig_header'=>$column);	
	$wpdb->update($tableName,$data,$where); 
 	
 	return true;
	
}

function en_mb_get_new_column_id($table_id,$column){
	
	global $wpdb;
	
	$new_id= '';

	$tableName=$wpdb->prefix."wpdatatables_columns";
	
	$sql="SELECT id from $tableName WHERE table_id='$table_id' AND orig_header='$column'";
	
	$advanced_settings =$wpdb->get_row($sql);
	
	if(!empty($advanced_settings->id)){
		$new_id = $advanced_settings->id;
	}
	
	return $new_id;
	
}

function en_mb_update_column_config($table_id,$forgin_table_id,$column){
	global $wpdb;

	$tableInfo = $wpdb->get_row("SELECT title from ".$wpdb->prefix."wpdatatables WHERE id='".$forgin_table_id."'" );
	
	$advanced_settings_Arr 		= en_mb_get_old_column_config($table_id,$column);

 	$cofig 						= array();
 	$cofig['tableId'] 			= $forgin_table_id;
 	$cofig['tableName'] 		= $tableInfo->title.' (id: '.$forgin_table_id.')';
 	$cofig['displayColumnId'] 	= en_mb_get_new_column_id($forgin_table_id,$advanced_settings_Arr->foreignKeyRule->displayColumnName);
 	$cofig['displayColumnName'] = $advanced_settings_Arr->foreignKeyRule->displayColumnName;
 	$cofig['storeColumnId'] 	= en_mb_get_new_column_id($forgin_table_id,$advanced_settings_Arr->foreignKeyRule->storeColumnName);
 	$cofig['storeColumnName'] 	= $advanced_settings_Arr->foreignKeyRule->storeColumnName;

 	$cofigObj 					= (object) $cofig;

 	$advanced_settings_Arr->foreignKeyRule = $cofigObj;

 	en_mb_set_new_column_config($table_id,$column,$advanced_settings_Arr);
 	return $column." updated <br>";
}

function en_mb_create_frontend_menu(){

	if(get_option("_en_mb_menu_activate")=='active'){

		$response[] = "<p><strong style='color:red'>Menu already activated.</strong><p>";
		return $response;
		
	}	


	$menuLocations 		= get_nav_menu_locations();
	$primary_menu_id 	= $menuLocations['primary-menu']; 

	$main_item_id 		= wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Maintenance Manager'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_en_mb_page_1')), 
								        'menu-item-status' 		=> 'publish',
								        'menu-item-position' 	=> 5
								    	)
						);

	update_option("_en_mb_menu_item_1",$main_item_id);
	// Sub menu Items	
	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Maintenance Equipment History'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_en_mb_page_7')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> $main_item_id
								    	)
							);

	update_option("_en_mb_menu_item_2",$main_sub_item_id);

	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Active Maintenance Alert'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_en_mb_page_5')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> $main_item_id
								    	)
							);

	update_option("_en_mb_menu_item_3",$main_sub_item_id);

	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Active Calibration Alert'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_en_mb_page_6')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> $main_item_id
								    	)
							);

	update_option("_en_mb_menu_item_4",$main_sub_item_id);

	$main_sub_item_id = wp_update_nav_menu_item($primary_menu_id, 0, array(
								        'menu-item-title' 		=>  __('Equipment Manager Settings'),
								        'menu-item-classes' 	=> '',
								        'menu-item-url' 		=> get_permalink(get_option('_en_mb_page_8')),
								        'menu-item-status' 		=> 'publish',
								        'menu-item-parent-id' 	=> $main_item_id
								    	)
							);

	update_option("_en_mb_menu_item_5",$main_sub_item_id);

	update_option("_en_mb_menu_activate",'active');

	$response[] = "<p><strong style='color:green'>Menu has been created successfully.</strong><p>";
	return $response;

}
 
function en_mb_delete_frontend_menu(){ 
	wp_delete_post(get_option("_en_mb_menu_item_1"),true);
	wp_delete_post(get_option("_en_mb_menu_item_2"),true);
	wp_delete_post(get_option("_en_mb_menu_item_3"),true);
	wp_delete_post(get_option("_en_mb_menu_item_4"),true);
	wp_delete_post(get_option("_en_mb_menu_item_5"),true);
	delete_option("_en_mb_menu_item_1");
	delete_option("_en_mb_menu_item_2");
	delete_option("_en_mb_menu_item_3");
	delete_option("_en_mb_menu_item_4");
	delete_option("_en_mb_menu_item_5");
	update_option("_en_mb_menu_activate",'inactive');

}

/***********************Remove all configrations************************/
function en_mb_remove_all_configrattions(){
	global $wpdb;

	en_mb_delete_frontend_menu();

	en_mb_remove_all_capability();

	delete_option("_en_mb_menu_activate");


	// Delete all wpdatatables
	foreach (EN_MB_ALL_TABLES as $tablekey => $value) {
		
		$tableName = $wpdb->prefix."wpdatatables";
		$wpdb->query('Delete from '.$tableName.' WHERE id ='.get_option($tablekey));

		$tableName = $wpdb->prefix."wpdatatables_columns";
		$wpdb->query('Delete from '.$tableName.' WHERE table_id ='.get_option($tablekey));

		delete_option( $tablekey);
		
	}

	// Delete all pages
	foreach (EN_MB_ALL_PAGES as $pagekey => $value) {

		if( $pagekey!='_en_mb_page_15' ){

			wp_delete_post(get_option($pagekey),true);	
		}	

		delete_option($pagekey);
	}

	// Delete source tables
	$wpdb->query('DROP TABLE IF EXISTS en_mb_calibrations');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_calibrations_plans');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipments_explanation_relation');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipment_gsroup_templates');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipment_manager_settings');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipment_types');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipment_type_relation_settings');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_facility_description');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_facility_equipment');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_facility_equipment_status');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_maintenance_history');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_maintenance_plans');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_maintenance_plans_meta');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_maintenance_records');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_services_types');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_service_explanation');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_tracking_active_items');
	$wpdb->query('DROP TABLE IF EXISTS en_mb_equipment_group_templates');

	$tableName 	= $wpdb->prefix."options";
	$wpdb->query('Delete from '.$tableName.' WHERE option_name LIKE "%_en_mb_%"');
  
	$response[] = "<p><strong style='color:green'>All configurations has been removed.</strong><p>";
	return $response;

}

/***********************Plugin Activation/Deactivation hook************************/
function en_mb_plugin_activate() {
 
	if(get_option("_en_mb_menu_activate")=='inactive'){
		en_mb_create_frontend_menu();
	}	
    
}
register_activation_hook( __FILE__, 'en_mb_plugin_activate' );

function en_mb_plugin_deactivation() {
	en_mb_delete_frontend_menu();
}
register_deactivation_hook( __FILE__, 'en_mb_plugin_deactivation' );

/***********************Create required Pages************************/
function en_mb_create_required_pages(){
    global $wpdb;

    $response = array();

	if(get_option('_en_mb_create_page')==1){

		$response[] = "<strong style='color:red'>Required pages already created.</strong>";
		return $response;

	}

	$TemplatesArr = EN_MB_ALL_PAGES_TEMPLATES;

	foreach (EN_MB_ALL_PAGES as $option_key => $pageName) {
		
		if( $option_key=='_en_mb_page_15' ){

			continue;

		}	
		
		$query=$wpdb->prepare('SELECT ID FROM '. $wpdb->posts .' WHERE post_status="publish" AND post_title = %s AND post_type = \'page\'',$pageName);

		$wpdb->query( $query );

		if ( $wpdb->num_rows ) {
		  
		    $page_id = $wpdb->get_var( $query );

		    update_post_meta( $page_id, '_wp_page_template', $TemplatesArr[$option_key] );

		    update_post_meta( $page_id, '_et_pb_page_layout', 'et_full_width_page' );

		    update_option( $option_key, $page_id);

		    $response[] = "<strong style='color:green'>Page Name: $pageName already exists and upated.</strong>";
		    
		} else{

		   	$new_post = array(
		       'post_title' 	=> $pageName,
		       'post_content' 	=> '',
		       'post_status' 	=> 'publish',
		       'post_author' 	=> get_current_user_id(),
		       'post_type' 		=> 'page',
		       'post_category' 	=> array(0)
		  	);

	       $page_id = wp_insert_post($new_post);

	       update_post_meta( $page_id, '_wp_page_template', $TemplatesArr[$option_key] );

	       update_post_meta( $page_id, '_et_pb_page_layout', 'et_full_width_page' );

	       update_option( $option_key, $page_id);

	       $response[] = "<strong style='color:green'>Page Name: $pageName created.</strong>";

		}
 
	}

	update_option( '_en_mb_create_page', 1);
 	
 	$response[] = "<strong style='color:green'>All required pages has been created.</strong>";
	
	return $response;
	 
}

 /**
 * Import SQL from file
 *
 * @param string path to sql file
 */
function sqlImport($file)
{

    $delimiter 			= ';';
    $file 				= fopen($file, 'r');
    $isFirstRow 		= true;
    $isMultiLineComment = false;
    $sql 				= '';

    while (!feof($file)) {

        $row = fgets($file);

        // remove BOM for utf-8 encoded file
        if ($isFirstRow) {
            $row = preg_replace('/^\x{EF}\x{BB}\x{BF}/', '', $row);
            $isFirstRow = false;
        }

        // 1. ignore empty string and comment row
        if (trim($row) == '' || preg_match('/^\s*(#|--\s)/sUi', $row)) {
            continue;
        }

        // 2. clear comments
        $row = trim(clearSQL($row, $isMultiLineComment));

        // 3. parse delimiter row
        if (preg_match('/^DELIMITER\s+[^ ]+/sUi', $row)) {
            $delimiter = preg_replace('/^DELIMITER\s+([^ ]+)$/sUi', '$1', $row);
            continue;
        }

        // 4. separate sql queries by delimiter
        $offset = 0;
        while (strpos($row, $delimiter, $offset) !== false) {
            $delimiterOffset = strpos($row, $delimiter, $offset);
            if (isQuoted($delimiterOffset, $row)) {
                $offset = $delimiterOffset + strlen($delimiter);
            } else {
                $sql = trim($sql . ' ' . trim(substr($row, 0, $delimiterOffset)));
                query($sql);

                $row = substr($row, $delimiterOffset + strlen($delimiter));
                $offset = 0;
                $sql = '';
            }
        }
        $sql = trim($sql . ' ' . $row);
    }
    if (strlen($sql) > 0) {
        query($row);
    }
    fclose($file);
}

/**
 * Remove comments from sql
 *
 * @param string sql
 * @param boolean is multicomment line
 * @return string
 */
function clearSQL($sql, &$isMultiComment)
{
    if ($isMultiComment) {
        
        if (preg_match('#\*/#sUi', $sql)) {
            $sql = preg_replace('#^.*\*/\s*#sUi', '', $sql);
            $isMultiComment = false;
        } else {
            $sql = '';
        }
        if(trim($sql) == ''){
            return $sql;
        }

    }

    $offset = 0;
    while (preg_match('{--\s|#|/\*[^!]}sUi', $sql, $matched, PREG_OFFSET_CAPTURE, $offset)) {
        list($comment, $foundOn) = $matched[0];
        if (isQuoted($foundOn, $sql)) {
            $offset = $foundOn + strlen($comment);
        } else {
            if (substr($comment, 0, 2) == '/*') {
                $closedOn = strpos($sql, '*/', $foundOn);
                if ($closedOn !== false) {
                    $sql = substr($sql, 0, $foundOn) . substr($sql, $closedOn + 2);
                } else {
                    $sql = substr($sql, 0, $foundOn);
                    $isMultiComment = true;
                }
            } else {
                $sql = substr($sql, 0, $foundOn);
                break;
            }
        }
    }
    return $sql;
}

/**
 * Check if "offset" position is quoted
 *
 * @param int $offset
 * @param string $text
 * @return boolean
 */
function isQuoted($offset, $text)
{
    if ($offset > strlen($text))
        $offset = strlen($text);

    $isQuoted = false;
    for ($i = 0; $i < $offset; $i++) {
        if ($text[$i] == "'")
            $isQuoted = !$isQuoted;
        if ($text[$i] == "\\" && $isQuoted)
            $i++;
    }
    return $isQuoted;
}

function query($sql)
{

    global $wpdb;
    $wpdb->query($sql);
}

/***********************Import source tables************************/
function en_mb_import_source_tables(){

	global $wpdb;
	$response = array();

	if(get_option('_en_mb_import_source_tables')==1){

		$response[] = "<strong style='color:red'>Source tables already exist.</strong>";
		return $response;

	}

	// Name of the file
	$filename = dirname(__FILE__).'/json/source-tables.sql';
	set_time_limit(0);
	header('Content-Type: text/html;charset=utf-8');
	sqlImport($filename);

	echo "Peak MB: ", memory_get_peak_usage(true)/1024/1024;

	update_option('_en_mb_import_source_tables', 1);
	$response[] = "<strong style='color:green'>All source tables has been imported.</strong>";
	
	return $response;
}


/********************Add capabilities for all roles****************************************/
function en_mb_add_caps_for_all_roles(){
	$response=array();

	/*if(get_option( '_en_mb_capabilities')==1){
		$response[] = "<p><strong style='color:red'>Capabilities already imported.</strong><p>";
		return $response;
	}*/

	en_mb_remove_all_capability();

	$response[] = en_mb_add_capability_for_single_role('administrator');
	$response[] = en_mb_add_capability_for_single_role('maintenance_manager');
	$response[] = en_mb_add_capability_for_single_role('maintenance_role');
	$response[] = en_mb_add_capability_for_single_role('instrumentation_manager');
   	$response[] = en_mb_add_capability_for_single_role('instrumentation_role');
	 
	update_option( '_en_mb_capabilities', 1 );
	return $response;
}



function en_mb_add_capability_for_single_role($roleName){

	$role = get_role( $roleName );

	if(!empty($role)){
		$role_caps 		= file_get_contents(dirname(__FILE__).'/json/'.$roleName.'-caps.json');
		$role_caps_arr	= json_decode($role_caps, true); 

		foreach ($role_caps_arr as $capability => $status) {
			if($status==1){
				$role->add_cap( $capability );
			}	 
		}
		$response = "<p><strong style='color:green'>All Capabilities imported/refreshed for $roleName.</strong><p>";
	}else{
		$response = "<p><strong style='color:red'>All Capabilities couldn't import for $roleName. Role does not exist.</strong><p>";
	}
	return $response;
	 
}


/************Remove all capabilities**************/
function en_mb_remove_all_capability(){

	$response[] = en_mb_remove_single_capability('administrator');
	$response[] = en_mb_remove_single_capability('maintenance_manager');
	$response[] = en_mb_remove_single_capability('maintenance_role');
	$response[] = en_mb_remove_single_capability('instrumentation_manager');
   	$response[] = en_mb_remove_single_capability('instrumentation_role');

   	update_option( '_en_mb_capabilities', 0 );
   	return $response;
}

function en_mb_remove_single_capability($roleName){
	global $wp_roles; 
	 
	$role_caps 		= file_get_contents(dirname(__FILE__).'/json/administrator-caps.json');
	$role_caps_arr	= json_decode($role_caps, true); 

	foreach ($role_caps_arr as $capability => $status) {

		$wp_roles->remove_cap($roleName, $capability);

	}

	$response = "<p><strong style='color:green'>All Capabilities removed for $roleName.</strong><p>";

	return $response;
	 
}

/*************************create roles if not exist******************************/
function en_mb_create_roles(){

	if(add_role( 'maintenance_manager', 'Maintenance Manager')){
		$response[] = "<p><strong style='color:green'>Maintenance Manager create.</strong><p>";
	}else{
		$response[] = "<p><strong style='color:red'>Maintenance Manager already exist.</strong><p>";
	}

	if(add_role( 'maintenance_role', 'Maintenance Role')){
		$response[] = "<p><strong style='color:green'>Maintenance Role create.</strong><p>";
	}else{
		$response[] = "<p><strong style='color:red'>Maintenance Role already exist.</strong><p>";
	}

	if(add_role( 'instrumentation_manager', 'Instrumentation Manager')){
		$response[] = "<p><strong style='color:green'>Instrumentation Manager create.</strong><p>";
	}else{
		$response[] = "<p><strong style='color:red'>Instrumentation Manager already exist.</strong><p>";
	}

	if(add_role( 'instrumentation_role', 'Instrumentation Role')){
		$response[] = "<p><strong style='color:green'>Instrumentation Role create.</strong><p>";
	}else{
		$response[] = "<p><strong style='color:red'>Instrumentation Role already exist.</strong><p>";
	}
 	return $response;
}