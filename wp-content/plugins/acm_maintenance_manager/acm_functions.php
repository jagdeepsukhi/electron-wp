<?php


function add_equipment_scripts() {
 
  wp_enqueue_style( 'bootsrap_equipment',ACM_PLUGIN_PATH.'css/bootstrap.min.css', array(), '1.1', 'all');
  wp_enqueue_style( 'font_awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(),  'font_awesome');

  wp_enqueue_style( 'toastr',ACM_PLUGIN_PATH.'css/toastr.css', array(),  'toastr');
  wp_enqueue_style( 'acm-style',ACM_PLUGIN_PATH.'css/acm-style.css', array(),  'acm-style');

  wp_enqueue_script( 'toastr-js', ACM_PLUGIN_PATH.'js/toastr.js', array (), 1.1, true);
   
}
add_action( 'wp_enqueue_scripts', 'add_equipment_scripts' );

// equipment manager settings
if ( ! function_exists( 'equipment_manager_settings' ) ):
	function equipment_manager_settings(){
		global $wpdb;

		$equipment_type_id 	= (isset($_POST['equipment_type_id'])) ? $_POST['equipment_type_id'] : '';
		$emailsData			= (isset($_POST['email'])) ? $_POST['email'] : '';
		$statusData 		= (isset($_POST['status'])) ? $_POST['status'] : '';
		$everyData 			= (isset($_POST['every'])) ? $_POST['every'] : '';
		$weekdayData 		= (isset($_POST['weekday'])) ? $_POST['weekday'] : '';
		$i=0;
		foreach ($equipment_type_id as $key => $type_id) {

			$emailArr 	= $emailsData[$i];
			$statusArr 	= $statusData[$i];
			$every 		= $everyData[$i];
			$weekdayArr = $weekdayData[$i];

			$email 		= implode(',',$emailArr);
			$status 	= implode(',',$statusArr);
			$weekday 	= implode(',',$weekdayArr);

			$checkEquipmentType = $wpdb->get_row("select equipment_type from en_mb_equipment_manager_settings where equipment_type='".$type_id."'", OBJECT );
			if(!empty($checkEquipmentType)){
				$data=array(
						'emails' 		=> $email,
						'status'		=> $status,
						'every' 		=> $every,
						'weekday' 		=> $weekday,
						'recorded_by' 	=> get_current_user_id()

					);
			   $wpdb->update('en_mb_equipment_manager_settings',$data,array('equipment_type'=>$type_id));
			}else{

				$data=array(
							'equipment_type' 	=> $type_id,
							'emails' 			=> $email,
							'status'	 		=> $status,
							'every' 			=> $every,
							'weekday' 			=> $weekday,
							'recorded_by'		=> get_current_user_id()

							);
				$format=$format = array('%s','%s','%s','%s','%s','%s');
				$wpdb->insert('en_mb_equipment_manager_settings',$data,$format);
		
			}
			$i++;
		}

		echo json_encode(array("code"=>200, "message"=>"Settings has been saved sucessfully.")); die;

	}
	add_action( 'wp_ajax_equipment_manager_settings', 'equipment_manager_settings' );
	add_action( 'wp_ajax_nopriv_equipment_manager_settings', 'equipment_manager_settings' );
endif;

// equipment manager settings
if ( ! function_exists( 'equipment_status_relation' ) ):
	function equipment_status_relation(){
		global $wpdb;
 

		$equipment_type_id 	= (isset($_POST['equipment_type_id'])) ? $_POST['equipment_type_id'] : '';
		$statusData 		= (isset($_POST['status'])) ? $_POST['status'] : '';
		$action_linksData 	= (isset($_POST['action_links'])) ? $_POST['action_links'] : '';
		$explanationData 	= (isset($_POST['explanation'])) ? $_POST['explanation'] : '';
		$serviceData 		= (isset($_POST['service'])) ? $_POST['service'] : '';
		$descriptionData 	= (isset($_POST['description'])) ? $_POST['description'] : '';

		$i=0;

			
		foreach ($equipment_type_id as $key => $type_id) {
			$statusArr 		= $statusData[$i];
			 
			
			$status 		= (!empty($statusArr)) ? implode(',',$statusArr) : '';
			$action_links 	= (!empty($action_linksData)) ? implode(',',$action_linksData[$i]) : '';
			$explanations 	= (!empty($explanationData)) ? implode(',',$explanationData[$i]) : '';
			$services 		= (!empty($serviceData)) ? implode(',',$serviceData[$i]) : '';
			$descriptions 	= (!empty($descriptionData)) ? implode(',',$descriptionData[$i]) : '';

			$checkEquipmentType = $wpdb->get_row("select equipment_type from en_mb_equipment_type_relation_settings where equipment_type='".$type_id."'", OBJECT );
			 
			
			if(!empty($checkEquipmentType)){
				$data=array(
						'status'				=> $status,
						'action_links'			=> $action_links,
						'explanations'			=> $explanations,
						'services'				=> $services,
						'facility_descriptions'	=> $descriptions,
						'recorded_by' 			=> get_current_user_id()
					);
			   $wpdb->update('en_mb_equipment_type_relation_settings',$data,array('equipment_type'=>$type_id));
			}else{

				$data=array(
							'equipment_type' 		=> $type_id,
							'status'	 			=> $status,
							'action_links'			=> $action_links,
							'explanations'			=> $explanations,
							'services'				=> $services,
							'facility_descriptions'	=> $descriptions,
							'recorded_by'			=> get_current_user_id()

							);
									
				$format = array('%s','%s','%s','%s','%s','%s');
				$wpdb->insert('en_mb_equipment_type_relation_settings',$data,$format);
		
			}

			$action_links 	= get_action_links_trigger($type_id); 
			$hiddenPre		= '<input class="action_asset_id" type="hidden" value="';
			$hiddenPost 	= '">';
			$assetSQL 		= "CONCAT('".$hiddenPre."',en_mb_facility_equipment.asset_id,'".$hiddenPost."')"; 
			$actionsql 		= "UPDATE `en_mb_facility_equipment` SET action=CONCAT('".$action_links."',".$assetSQL.") WHERE equipment_type ='".$type_id."'";
			$wpdb->query($actionsql);
			
			$i++;
		}

		echo json_encode(array("code"=>200, "message"=>"Settings has been saved sucessfully.")); die;

	}
	add_action( 'wp_ajax_equipment_status_relation', 'equipment_status_relation' );
	add_action( 'wp_ajax_nopriv_equipment_status_relation', 'equipment_status_relation' );
endif;



if ( ! function_exists( 'create_maintenance_plan' ) ):
function create_maintenance_plan(){
	 
	global $wpdb;

	$asset_id 			= (isset($_POST['asset_id'])) ? $_POST['asset_id'] : '';
	$scheduled_date		= (!empty($_POST['scheduled_date'])) ? date_format(date_create($_POST['scheduled_date']),"Y-m-d") : '';
	$reoccurring 		= (isset($_POST['reoccurring'])) ? $_POST['reoccurring'] : '0';
	$period 			= (isset($_POST['period'])) ? $_POST['period'] : '';
	$instructions 		= (isset($_POST['instructions'])) ? $_POST['instructions'] : '';
	$checkPlanID=$wpdb->get_row("select id from en_mb_maintenance_plans where asset_id='".$asset_id."'", OBJECT );
	if(empty($checkPlanID)){
		$data = array(
					'asset_id' 			=> $asset_id, 
					'scheduled_date' 	=> $scheduled_date,
					'reoccurring'		=> $reoccurring,
					'period'			=> ($reoccurring==1) ? $period : '',
					'instructions'		=> $instructions,
					'recorded_by'		=> get_current_user_id()
					);

		$format = array('%s','%s','%d','%s','%s','%d');
		$wpdb->insert('en_mb_maintenance_plans',$data,$format);
		$plan_id = $wpdb->insert_id;

	}else{
		$plan_id = $checkPlanID->id;

		$data=array();
		$data['asset_id'] 		= $asset_id;
		if(!empty($scheduled_date)){
			$data['scheduled_date'] = $scheduled_date;
		}
	 	$data['reoccurring'] = $reoccurring;
		$data['period']			= ($reoccurring==1) ? $period : '';
		$data['instructions']	= $instructions;
		$data['recorded_by']	= get_current_user_id();
		
		$wpdb->update('en_mb_maintenance_plans',$data,array('id'=>$plan_id));

	}

	if(!empty($plan_id )){
		

		if(!empty($_POST['choice_name']))	{
			//$wpdb->delete( 'en_mb_maintenance_plans_meta', array( 'plan_id' => $plan_id ) );
			$i=0;
			foreach ($_POST['choice_name'] as $key => $choice_name) {

				$plan_meta_id = (isset($_POST['plan_meta_id'])) ? $_POST['plan_meta_id'][$i] : '';

				$data= array();
				$data['plan_id']			= $plan_id;
				$data['asset_id']			= $asset_id;
				$data['maintenance_type']	= 'Usage Based';
				$data['choice_name']		= $choice_name;
				$data['criteria']			= (isset($_POST['criteria'])) ? $_POST['criteria'][$i] : '';
				$data['benchmark_odometer']	= (isset($_POST['benchmark_odometer'])) ? $_POST['benchmark_odometer'][$i] : '';
				$data['criteria_type']		= (isset($_POST['maintenance_criteria'])) ? $_POST['maintenance_criteria'] : '';
				$data['recorded_by']		= get_current_user_id();

				if(empty($plan_meta_id)){
					$format = array('%d','%s','%s','%s','%s','%s','%s','%d');
					$wpdb->insert('en_mb_maintenance_plans_meta',$data,$format);
				}else{
					$wpdb->update('en_mb_maintenance_plans_meta',$data,array('id'=>$plan_meta_id));
				}
				
				$i++;
			}
		}

		if(isset($_POST['distance']) && !empty($_POST['distance']))	{
			$dueCount=0;

			foreach ($_POST['distance'] as $key => $distance) {
				$plan_meta_id 				= (isset($_POST['distance_meta_id'])) ? $_POST['distance_meta_id'][$dueCount] : '';
				$data 						= array();
				$data['plan_id']			= $plan_id;
				$data['asset_id']			= $asset_id;
				$data['maintenance_type']	= 'Distance Based';
				$data['distance']			= $distance;
				$data['due_status']			= (isset($_POST['due_status'][$dueCount])) ? $_POST['due_status'][$dueCount] : 0;
				$data['criteria_type']		= (isset($_POST['maintenance_criteria'])) ? $_POST['maintenance_criteria'] : '';
				$data['recorded_by']		= get_current_user_id();
				if(empty($plan_meta_id)){

					$format = array('%d','%s','%s','%s','%d','%s','%d');
					$wpdb->insert('en_mb_maintenance_plans_meta',$data,$format);

				}else{
					$wpdb->update('en_mb_maintenance_plans_meta',$data,array('id'=>$plan_meta_id));
				}
				$dueCount++;
			}

		}
	}
	echo json_encode(array("code"=>200, "message"=>"Maintenance plan has been saved sucessfully.")); die;
}
add_action( 'wp_ajax_create_maintenance_plan', 'create_maintenance_plan' );
add_action( 'wp_ajax_nopriv_create_maintenance_plan', 'create_maintenance_plan' );
endif;



if ( ! function_exists( 'remove_maintence_meta_by_meta_id' ) ):
function remove_maintence_meta_by_meta_id(){
	 
	global $wpdb;

	$meta_id 			= (isset($_POST['meta_id'])) ? $_POST['meta_id'] : '';
	
	if(!empty($meta_id)){
		 $wpdb->delete( 'en_mb_maintenance_plans_meta', array( 'id' => $meta_id ) );
		 echo json_encode(array("code"=>200, "message"=>"Item has been deleted sucessfully.")); die;
	}else{
		echo json_encode(array("code"=>202, "message"=>"Item could not delete!")); die;
	}

}
add_action( 'wp_ajax_remove_maintence_meta_by_meta_id', 'remove_maintence_meta_by_meta_id' );
add_action( 'wp_ajax_nopriv_remove_maintence_meta_by_meta_id', 'remove_maintence_meta_by_meta_id' );
endif;



// Create calibration plan
if ( ! function_exists( 'create_calibration_plan' ) ):
function create_calibration_plan(){
	global $wpdb;

	$asset_id 		= (isset($_POST['asset_id'])) ? $_POST['asset_id'] : '';
	$scheduled_date	= (!empty($_POST['scheduled_date'])) ? date_format(date_create($_POST['scheduled_date']),"Y-m-d") : '';
	$nextCalibrate 	= (isset($_POST['next_calibrate'])) ? $_POST['next_calibrate'] : '0';
	$reoccurring 	= (isset($_POST['reoccurring'])) ? $_POST['reoccurring'] : '0';
	$period 		= (isset($_POST['period'])) ? $_POST['period'] : '';
	$instructions 	= (isset($_POST['instructions'])) ? $_POST['instructions'] : '';
	$checkPlanID=$wpdb->get_row("select id,next_calibrate from en_mb_calibrations_plans where asset_id='".$asset_id."'", OBJECT );
	if(empty($checkPlanID)){
		$data = array(
					'asset_id' 					=> $asset_id, 
					'scheduled_calibration' 	=> ($nextCalibrate==0) ? $scheduled_date : "",
					'next_calibrate'			=> $nextCalibrate,
					'reoccurring'				=> $reoccurring,
					'period'					=> ($reoccurring==1) ? $period : '',
					'calibration_instructions'	=> $instructions,
					'recorded_by'				=> get_current_user_id()
					);

		$format = array('%s','%s','%d','%d','%s','%s','%d');
		$wpdb->insert('en_mb_calibrations_plans',$data,$format);
		$plan_id = $wpdb->insert_id;
	}else{
		$plan_id = $checkPlanID->id;
		$data = array(
					'asset_id' 					=> $asset_id, 
					'scheduled_calibration' 	=> ($nextCalibrate==0) ? $scheduled_date : "",
					'next_calibrate'			=> $nextCalibrate,
					'reoccurring'				=> $reoccurring,
					'period'					=> ($reoccurring==1) ? $period : '',
					'calibration_instructions'	=> $instructions,
					'recorded_by'				=> get_current_user_id()
					);
		
		$wpdb->update('en_mb_calibrations_plans',$data,array('id'=>$plan_id));

	}

	if(isset($nextCalibrate) && !empty($nextCalibrate) && empty($checkPlanID->next_calibrate)){
			$due_date = date("Y-m-d");
			update_tracking_active_items_calibration($plan_id,$asset_id,$due_date,$instructions,'Calibrate/Inspect on next use',11);	
			update_facility_equipment_main_status($asset_id);
	}

	echo json_encode(array("code"=>200, "message"=>"Calibration plan has been saved sucessfully.")); die;
}
add_action( 'wp_ajax_create_calibration_plan', 'create_calibration_plan' );
add_action( 'wp_ajax_nopriv_create_calibration_plan', 'create_calibration_plan' );
endif;


if ( ! function_exists( 'wpdatatables_after_frontent_edit_row_callback' ) ):
function wpdatatables_after_frontent_edit_row_callback( $formdata){
	global $wpdb;
	$formdata = $_POST['formdata'];
	$table_id = $formdata['table_id'];
	$asset_id = $formdata['asset_id'];
	

	/*********************************************************************************************/
	// Update action links for equipments
	if(
		$table_id==get_option( '_en_mb_wp_datatable_1' ) || 
		$table_id==get_option( '_en_mb_wp_datatable_13') || 
		$table_id==get_option( '_en_mb_wp_datatable_14') || 
		$table_id==get_option( '_en_mb_wp_datatable_15') || 
		$table_id==get_option( '_en_mb_wp_datatable_16') || 
		$table_id==get_option( '_en_mb_wp_datatable_17')
	){

		$equipment_type_id = $formdata['equipment_type'];
		
		// $action_links = get_action_links_for_equipment($equipment_type_id,$asset_id);  
		$action_links = get_action_links_trigger($equipment_type_id);  
		

		if(!empty($formdata['id'])){
			$action_links  .= "<input class='action_asset_id' type='hidden' value='".$asset_id."'>";
			$wpdb->update(
						'en_mb_facility_equipment', 
						array('action'=>$action_links),  
						array('id'=>$formdata['id'])
					);
		}else{
			$last_sql 	 	= "SELECT id, asset_id FROM en_mb_facility_equipment ORDER BY id DESC LIMIT 0,1";
			$last_arr 	 	= $wpdb->get_row($last_sql);
			$last_arr_id 	= $last_arr->id;
			$asset_id 		= $last_arr->asset_id;
			$action_links  .= "<input class='action_asset_id' type='hidden' value='".$asset_id."'>";  
			$wpdb->update(
						'en_mb_facility_equipment', 
						array('action'=>$action_links),  
						array('id'=>$last_arr_id)
					);

		}
	}


	/*********************************************************************************************/
	// Action hook for update Facility Equipment Table Status on update Active Maintenance Items
	if(	$table_id==get_option( '_en_mb_wp_datatable_20') || $table_id==get_option( '_en_mb_wp_datatable_21')){
 		 
		update_facility_equipment_main_status($asset_id);
		
	}
	/*********************************************************************************************/
	// Facility Equipment Main Page | Prevent user to select "In Service" status if any Maintenance/Calibration is due.
	if($table_id==get_option( '_en_mb_wp_datatable_1')){

		$status = $formdata['status']; 
		if($status==1 || $status==2 || $status==3 || $status==4){
			prevent_facility_equipment_main_status_for_in_service($asset_id);
		}
	}

	/*********************************************************************************************/
	// /////// Update Benchmark Odometer reading on status "Return from servicing" ///////
	if($table_id==get_option( '_en_mb_wp_datatable_20')){	
		 
		
		if($formdata['activity_status']==10 && $formdata['cron_type']=='Usage Based' ){
 	
			$last_odometer 	= $formdata['last_odometer'];
			$description 	= $formdata['description'];

			 
			// Set Last Due Date in planner.
			$wpdb->update(
				'en_mb_maintenance_plans_meta', 
				array('benchmark_odometer'=>$last_odometer,'due_status'=>0), 
				array('choice_name'=>$description,'asset_id'=>$asset_id)
			);

		}

	}

	/*********************************************************************************************/
	// /////// Update Benchmark Odometer reading on status "Return from servicing" ///////
	if($table_id==get_option( '_en_mb_wp_datatable_11')){	
		 
			
		$exsql="UPDATE wp_wpdatatables_columns set possible_values=( SELECT GROUP_CONCAT(`service_explanation` separator '|') as service_explanations from en_mb_service_explanation ) WHERE (wp_wpdatatables_columns.table_id =".get_option( '_en_mb_wp_datatable_7')."  AND wp_wpdatatables_columns.orig_header='explanation_of_service') OR (wp_wpdatatables_columns.table_id ='".get_option( '_en_mb_wp_datatable_8')."'  AND wp_wpdatatables_columns.orig_header='explanation_of_service')";

		$wpdb->query($exsql);



	}


	/*********************************************************************************************/
	// /////// Update Current User ID for Edit records Maintenance Records Table" ///////
	if(	
		$table_id==get_option( '_en_mb_wp_datatable_1')  || 
		$table_id==get_option( '_en_mb_wp_datatable_13') || 	
		$table_id==get_option( '_en_mb_wp_datatable_14') || 
		$table_id==get_option( '_en_mb_wp_datatable_15') || 	
		$table_id==get_option( '_en_mb_wp_datatable_16') ||
		$table_id==get_option( '_en_mb_wp_datatable_17')	
	){ 

  	  	if(empty($formdata['id'])){

  	  		$last_sql = "SELECT asset_id FROM en_mb_facility_equipment WHERE equipment_type='".$formdata['equipment_type']."' AND status='".$formdata['status']."' ORDER BY id DESC LIMIT 1";
			$last_arr= $wpdb->get_row($last_sql);
			 
			$wpdb->update(
						'en_mb_facility_equipment', 
						array(
							'general_documentation'=>"<a class='documents-btn' title='Documents' rel='".$asset_id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						 ),  
						array('asset_id'=>$last_arr->asset_id)
					);
		}

	}


	/*********************************************************************************************/
	// /////// Update Current User ID for Edit records Maintenance Records Table" ///////
	if(	$table_id==get_option( '_en_mb_wp_datatable_7') || 	$table_id==get_option( '_en_mb_wp_datatable_8')	){	


  	  	if(!empty($formdata['id'])){
			$wpdb->update(
						'en_mb_maintenance_records', 
						array('recorded_by'=>get_current_user_id()),  
						array('id'=>$formdata['id'])
					);
		}else{
			$last_sql = "SELECT id FROM en_mb_maintenance_records WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
			$last_arr= $wpdb->get_row($last_sql);
			$last_arr_id=$last_arr->id;
			$wpdb->update(
						'en_mb_maintenance_records', 
						array(
							'recorded_by'=>get_current_user_id(),
							'service_documentations'=>"<a class='documents-btn' title='Documents' rel='".$last_arr->id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						 ),  
						array('id'=>$last_arr_id)
					);

		}

	}

	/*********************************************************************************************/
	// /////// Update Current User ID for Edit records Calibration Records Table" ///////
	if($table_id==get_option( '_en_mb_wp_datatable_12')){	

		if(!empty($formdata['id'])){
			/*$wpdb->update(
						'en_mb_calibrations', 
						array('recorded_by'=>get_current_user_id()),  
						array('id'=>$formdata['id'])
					);*/
		}else{
			$last_sql = "SELECT id FROM en_mb_calibrations WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
			$last_arr= $wpdb->get_row($last_sql);
			$last_arr_id=$last_arr->id;
			$wpdb->update(
						'en_mb_calibrations', 
						array(
							//'recorded_by'=>get_current_user_id(),
							'calibration_documentations'=>"<a class='documents-btn' title='Documents' rel='".$last_arr_id."' href='#'><img src='".ACM_PLUGIN_PATH."icons/folder.png'></a>",
						),   
						array('id'=>$last_arr_id)
					);
		}




		$max_sql = "SELECT id FROM en_mb_calibrations WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
		$max_id_arr= $wpdb->get_row($max_sql);
		$max_id=$max_id_arr->id;
 
		if( ($max_id==$formdata['id'] or empty($formdata['id'])) ){
			update_facility_equipment_main_status($asset_id);
		}
	}

}

add_action( 'wpdatatables_after_frontent_edit_row', 'wpdatatables_after_frontent_edit_row_callback' );
endif;


function wpdatatables_before_delete_row_callback($rowId){
 	global $wpdb;
	 
	$table_id = $_POST['table_id'];
	$sql = "SELECT mysql_table_name FROM wp_wpdatatables WHERE id=$table_id";
	$res = $wpdb->get_row($sql);

	if(!empty($res->mysql_table_name)){
		$uptSql="Update $res->mysql_table_name SET recorded_by='".get_current_user_id()."' WHERE id='".$rowId."'"; 

		$wpdb->query($uptSql);
	}


	/*********************************************************************************************/
	// /////// Update Benchmark Odometer reading on status "Return from servicing" ///////
	if($table_id==get_option( '_en_mb_wp_datatable_11')){	
		 
		$delete_id_val=$_POST['id_val'];
				
		$exsql="UPDATE wp_wpdatatables_columns set possible_values=( SELECT GROUP_CONCAT(`service_explanation` separator '|') as service_explanations from en_mb_service_explanation where id !=".$delete_id_val." ) WHERE (wp_wpdatatables_columns.table_id =".get_option( '_en_mb_wp_datatable_7')."  AND wp_wpdatatables_columns.orig_header='explanation_of_service') OR (wp_wpdatatables_columns.table_id ='".get_option( '_en_mb_wp_datatable_8')."'  AND wp_wpdatatables_columns.orig_header='explanation_of_service')";

		$wpdb->query($exsql);
 
	}

}
add_action( 'wpdatatables_before_delete_row', 'wpdatatables_before_delete_row_callback' );




if ( ! function_exists( 'prevent_facility_equipment_main_status_for_in_service' ) ):
	function prevent_facility_equipment_main_status_for_in_service($asset_id){
		global $wpdb;


		 
		$pending_items_sql = "SELECT activity_status,description FROM en_mb_tracking_active_items WHERE asset_id='".$asset_id."' AND activity_status NOT IN (10,12)"; 
		$pending_items= $wpdb->get_results($pending_items_sql);

		$max_sql = "SELECT calibration_status FROM en_mb_calibrations WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
		$last_calibration_arr = $wpdb->get_row($max_sql);

		if(count($pending_items) > 0 || $last_calibration_arr->calibration_status=="Fail" ){

			update_facility_equipment_main_status($asset_id);

			$historysql="Delete from en_mb_maintenance_history where user_id='".get_current_user_id()."' ORDER BY equipment_record_id DESC LIMIT 2";
 
			$wpdb->query($historysql);

			echo json_encode(array('error'=>"You can't change the status. Check pending active items or last calibration status"));
			die();
		}
			
	}
endif;


if ( ! function_exists( 'update_facility_equipment_main_status' ) ):
function update_facility_equipment_main_status($asset_id){
	global $wpdb;
	 
	$pending_items_sql = "SELECT activity_status,description FROM en_mb_tracking_active_items WHERE asset_id='".$asset_id."' AND activity_status NOT IN (10,12)";
	$pending_items= $wpdb->get_results($pending_items_sql);
	$facility_status 			= array();
	if(count($pending_items)>0){
		foreach ($pending_items as $key => $item) {
			$facility_status[]=$item->description;
		}
	}
	

	// Check Calibration record if last status is failed.
	$max_sql ="SELECT calibration_status FROM en_mb_calibrations WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
	$last_calibration_arr 		= $wpdb->get_row($max_sql);
	if(count($last_calibration_arr) > 0 ){

		$last_calibration_status 	= $last_calibration_arr->calibration_status; 
		if($last_calibration_status=="Fail"){
			$facility_status[]="Failed Calibration/Inspection";
		} 

	} 

	// Check Calibration record if last status is failed.
	$previous_status_sql = "SELECT status FROM en_mb_facility_equipment WHERE asset_id='".$asset_id."' ORDER BY id DESC LIMIT 1";
	$previous_status_arr = $wpdb->get_row($previous_status_sql);

	if(!in_array($previous_status_arr->status,array(2,3,4))){
		if(empty($facility_status)){
		// Set Equipment Status "In Service" in main table.
		$wpdb->update(
						'en_mb_facility_equipment', 
						array('status'=>1),  
						array('asset_id'=>$asset_id)
					);
		}else{
			$wpdb->update(
							'en_mb_facility_equipment', 
							array('status'=>implode(',', $facility_status)),  
							array('asset_id'=>$asset_id)
						);
		}
	} 
			
}

endif;

if ( ! function_exists( 'maintenance_status_list' ) ):
	function maintenance_status_list(){
		global $wpdb;
		$statussql = "SELECT equipment_status FROM en_mb_facility_equipment_status";
		$status= $wpdb->get_results($statussql);
		$facility_status=array();
		foreach ($status as $key => $item) {
			$facility_status[]=$item->equipment_status;
		}
		return $facility_status;
			
	}
endif;

if ( ! function_exists( 'service_explanations_list' ) ):
	function service_explanations_list($equipment_type){
		global $wpdb;
		$explanationSql = "SELECT explanations FROM `en_mb_equipment_type_relation_settings` WHERE equipment_type='".$equipment_type."'"; 
		$explanation = $wpdb->get_row($explanationSql);
		
		if(!empty($explanation->explanations)){
			$explanationSql = "SELECT service_explanation FROM `en_mb_service_explanation` where id IN (".$explanation->explanations.")";
			$status= $wpdb->get_results($explanationSql);
			$facility_status=array();
			foreach ($status as $key => $item) {
				$facility_status[]=$item->service_explanation;
			}
			return $facility_status;
		}else{
			return false;
		}
	          
	}
endif;
if ( ! function_exists( 'all_service_explanations_list' ) ):
	function all_service_explanations_list(){
		global $wpdb;
		$explanationSql = "SELECT service_explanation FROM `en_mb_service_explanation`";
		$status= $wpdb->get_results($explanationSql);
		$facility_status=array();
		foreach ($status as $key => $item) {
			$facility_status[]=$item->service_explanation;
		}
		return $facility_status;
			
	}
endif;


if ( ! function_exists( 'all_service_types_list' ) ):
	function all_service_types_list(){
		global $wpdb;
		$services_typesSql = "SELECT service_type FROM `en_mb_services_types`";
		$status= $wpdb->get_results($services_typesSql);
		$facility_status=array();
		foreach ($status as $key => $item) {
			$service_types[]=$item->service_type;
		}
		return $service_types;
			
	}
endif;

if ( ! function_exists( 'service_types_list' ) ):
	function service_types_list($equipment_type){
		global $wpdb;
		$service_typesSql = "SELECT services FROM `en_mb_equipment_type_relation_settings` WHERE equipment_type='".$equipment_type."'"; 
		$service_types = $wpdb->get_row($service_typesSql);
		 
		if(!empty($service_types->services)){
			$explanationSql = "SELECT service_type FROM `en_mb_services_types` where id IN (".$service_types->services.")";
			$status= $wpdb->get_results($explanationSql);
			$facility_status=array();
			foreach ($status as $key => $item) {
				$facility_status[]=$item->service_type;
			}
			return $facility_status;
		}else{
			return false;
		}
	          
	}
endif;



/*//////////////////////////////////////////////////////////////////////////////////////*/
/****************************1 Calibration Notification Send *****************************/ 
//////////////////////////////////////////////////////////////////////////////////////*/

if ( ! wp_next_scheduled( 'acm_cron_send_notifications_calibration' ) ) {
  wp_schedule_event( time(), 'daily', 'acm_cron_send_notifications_calibration' );
}
add_action( 'acm_cron_send_notifications_calibration', 'acm_cron_send_notifications_calibration' );

if ( ! function_exists( 'acm_cron_send_notifications_calibration' ) ):

	function acm_cron_send_notifications_calibration(){
		global $wpdb;
		$manager_settings_sql = "SELECT * FROM en_mb_equipment_manager_settings";
		$manager_settings 	  = $wpdb->get_results($manager_settings_sql);
		 

		if($manager_settings){
		 	$emailsData = array();
			foreach ($manager_settings as $key => $settings) {
				// echo $settings->every."<br>";
				switch ($settings->every) {
					    case 'Week':
					    	$weekdays = $settings->weekday;
					    	if(!empty($weekdays)){
					    		foreach (explode(',', $weekdays) as $daykey => $day) { 
					    			$today = date('l');
					    			if($day==$today){	 
 										 
					    				$emailsData = prepare_notification_data_calibration($settings,$emailsData);
					    			}
					    		}
					    	}
					        break;

					    case 'Month':
								 
								$todayDate = date('d');
								if($todayDate==03){
								$emailsData = prepare_notification_data_calibration($settings,$emailsData);
							}
					        break;

					    case 'Quarter':
					    	$todayDate  = date('d');
					    	$todayMonth = date('m');
							
							if(
									($todayDate==03 && $todayMonth==01) || 
									($todayDate==03 && $todayMonth==04) || 
									($todayDate==03 && $todayMonth==07) || 
									($todayDate==03 && $todayMonth==10)
							){
								$emailsData = prepare_notification_data_calibration($settings,$emailsData);
							}
					         
					        break;

					    default:
					    	continue;       
					}

					
			} // End foreach
 
			 
			if(!empty($emailsData)){
		 
			  	$emailTemplate= dirname(__FILE__)."/cron-jobs/email-template-calibration.php";
			   	$subject= "Calibration/Inspection Items Status Report";
				cron_send_emails($emailsData,$emailTemplate,$subject);
			}else{
				wp_die("No Email Set.");
			}
			
		} else{
			wp_die("No Settings Set.");
		}
	}

endif;

if ( ! function_exists( 'prepare_notification_data_calibration' ) ):
	function prepare_notification_data_calibration($settings,$emailsData){
		global $wpdb;
		$equipment_type = $settings->equipment_type;
		$emails 		= $settings->emails;
		$status 		= $settings->status;

		$getrecordssql= "SELECT 
						tracking.*,
						equipment.make,
						equipment.model,
						equipment.year ,
						types.equipment_type,
						status.equipment_status
						FROM en_mb_tracking_active_items as tracking
						LEFT JOIN en_mb_facility_equipment as equipment ON tracking.asset_id=equipment.asset_id   
						LEFT JOIN en_mb_facility_equipment_status as status ON status.id=tracking.activity_status 
						LEFT JOIN en_mb_equipment_types as types ON types.id=equipment.equipment_type 
						WHERE tracking.activity_status IN (".$status.") AND tracking.item_type ='Calibration' AND equipment.equipment_type ='".$equipment_type."'";

						  
		$recorsDataArr = $wpdb->get_results($getrecordssql,ARRAY_A);
	 
		if(!empty($emails) && !empty($recorsDataArr)){
			foreach (explode(',', $emails) as $emailkey => $email) {
				
				if(array_key_exists($email, $emailsData)){
					foreach ($recorsDataArr as $recorsDatakey => $recorsData) {
						array_push($emailsData[$email], $recorsData);
					}
					
				}else{
					$emailsData[$email]= array();
					foreach ($recorsDataArr as $recorsDatakey => $recorsData) {
						array_push($emailsData[$email], $recorsData);
					}
				}

			}
		}

		return $emailsData;

	}
endif;


//////////////////////////////////////////////////////////////////////////////////////*/
/**************************** 2 Maintenance Notification Send *****************************/
//////////////////////////////////////////////////////////////////////////////////////*/

if ( ! wp_next_scheduled( 'acm_cron_send_notifications_maintenance' ) ) {
  wp_schedule_event( time(), 'daily', 'acm_cron_send_notifications_maintenance' );
}
add_action( 'acm_cron_send_notifications_maintenance', 'acm_cron_send_notifications_maintenance' );

if ( ! function_exists( 'acm_cron_send_notifications_maintenance' ) ):

	function acm_cron_send_notifications_maintenance(){
		global $wpdb;
		$manager_settings_sql = "SELECT * FROM en_mb_equipment_manager_settings";
		$manager_settings 	  = $wpdb->get_results($manager_settings_sql);
		 
		if($manager_settings){
		 	$emailsData = array();
			foreach ($manager_settings as $key => $settings) {

				switch ($settings->every) {
					    case 'Week':
					    	$weekdays = $settings->weekday;
					    	if(!empty($weekdays)){
					    		foreach (explode(',', $weekdays) as $daykey => $day) { 
					    			$today = date('l');
					    			if($day==$today){	 
					    				$emailsData = prepare_notification_data($settings,$emailsData);
					    			}
					    		}
					    	}
					        break;

					    case 'Month':
								 
								$todayDate = date('d');
								if($todayDate==03){
								$emailsData = prepare_notification_data($settings,$emailsData);
							}
					        break;

					    case 'Quarter':
					    	$todayDate  = date('d');
					    	$todayMonth = date('m');
							
							if(
									($todayDate==03 && $todayMonth==01) || 
									($todayDate==03 && $todayMonth==04) || 
									($todayDate==03 && $todayMonth==07) || 
									($todayDate==03 && $todayMonth==10)
							){
								$emailsData = prepare_notification_data($settings,$emailsData);
							}
					         
					        break;

					    default:
					    	continue;       
					}

					
			} // End foreach
 

			 
			if(!empty($emailsData)){
				 
			    $emailTemplate= dirname(__FILE__)."/cron-jobs/email-template-maintenance.php";
			   	$subject= "Active Maintenance Items Status Report";
				cron_send_emails($emailsData,$emailTemplate,$subject);
			}else{
				wp_die("No Email Set.");
			}

		} else{
			wp_die("No Settings Set.");
		}

	}
endif;


if ( ! function_exists( 'prepare_notification_data' ) ):

	function prepare_notification_data($settings,$emailsData){
		global $wpdb;
		$equipment_type = $settings->equipment_type;
		$emails 		= $settings->emails;
		$status 		= $settings->status;

		echo $getrecordssql= "SELECT 
						tracking.*,
						equipment.make,
						equipment.model,
						equipment.year ,
						types.equipment_type,
						status.equipment_status
						FROM en_mb_tracking_active_items as tracking
						LEFT JOIN en_mb_facility_equipment as equipment ON tracking.asset_id=equipment.asset_id   
						LEFT JOIN en_mb_facility_equipment_status as status ON status.id=tracking.activity_status 
						LEFT JOIN en_mb_equipment_types as types ON types.id=equipment.equipment_type 
						WHERE tracking.activity_status IN (".$status.") AND tracking.item_type ='Maintenance' AND equipment.equipment_type ='".$equipment_type."'";


		$recorsDataArr = $wpdb->get_results($getrecordssql,ARRAY_A);
	 
		if(!empty($emails) && !empty($recorsDataArr)){
			foreach (explode(',', $emails) as $emailkey => $email) {
				
				if(array_key_exists($email, $emailsData)){
					foreach ($recorsDataArr as $recorsDatakey => $recorsData) {
						array_push($emailsData[$email], $recorsData);
					}
					
				}else{
					$emailsData[$email]= array();
					foreach ($recorsDataArr as $recorsDatakey => $recorsData) {
						array_push($emailsData[$email], $recorsData);
					}
				}

			}
		}

		return $emailsData;

	}

endif;


if ( ! function_exists( 'cron_send_emails' ) ):

	function cron_send_emails($emailsData,$emailTemplate,$subject) {

		if(!empty($emailsData)){
			foreach ($emailsData as $toEmail => $data) {

				 if(!empty($data)){

				 	ob_start();
			    	require $emailTemplate;
			  		$message = ob_get_clean();	
					$headers = array('Content-Type: text/html; charset=UTF-8');
					if(!wp_mail( $toEmail, $subject, $message, $headers )) {
					    echo "Mailer Error: " . $toEmail;
					} else {
					    echo "Message has been sent to : $toEmail";
					}
					
				 }else{
				 	continue;
				 }
		 
			}
		} 

	}

endif;




//////////////////////////////////////////////////////////////////////////////////////*/
/********************3 Cron Maintenance Planner Criteria Type Usage Based****************/
//////////////////////////////////////////////////////////////////////////////////////*/

if ( ! wp_next_scheduled( 'acm_cron_maintenance_planner_CT_usage_based' ) ) {
  wp_schedule_event( time(), 'daily', 'acm_cron_maintenance_planner_CT_usage_based' );
}

add_action( 'acm_cron_maintenance_planner_CT_usage_based', 'acm_cron_maintenance_planner_CT_usage_based' );


if ( ! function_exists( 'acm_cron_maintenance_planner_CT_usage_based' ) ):

	function acm_cron_maintenance_planner_CT_usage_based() {

		global $wpdb;
		$get_plan_sql= "SELECT 
						en_mb_maintenance_plans.id,
						en_mb_maintenance_plans.asset_id,
						en_mb_maintenance_plans.reoccurring,
						en_mb_maintenance_plans.period,
						en_mb_maintenance_plans.instructions,
						en_mb_maintenance_plans.scheduled_date,
						(SELECT odometer FROM en_avte_fuel WHERE en_avte_fuel.car_id=en_mb_maintenance_plans.asset_id ORDER BY en_avte_fuel.id DESC LIMIT 1) as last_odometer_reading,

						(SELECT en_mb_maintenance_records.instructions_for_next_visit FROM en_mb_maintenance_records WHERE en_mb_maintenance_records.asset_id=en_mb_maintenance_plans.asset_id ORDER BY en_mb_maintenance_records.id DESC LIMIT 1) as instructions_for_next_visit

						FROM en_mb_maintenance_plans 
						LEFT JOIN en_mb_facility_equipment 
						ON en_mb_facility_equipment.asset_id=en_mb_maintenance_plans.asset_id 
						WHERE en_mb_facility_equipment.maintenance_criteria IN ('Hours','Miles','Kilometers') AND en_mb_facility_equipment.status NOT IN (2,3)";
		$get_plans= $wpdb->get_results($get_plan_sql);

		if($get_plans){
			foreach ($get_plans as $key => $plan) {
				
				if ( function_exists( 'lm_get_latest_odometer_reading' ) ){
	 				$last_odometer_reading  = lm_get_latest_odometer_reading($plan->asset_id);
	 			}else{
	 				$last_odometer_reading  = $plan->last_odometer_reading;
	 			}

				if(empty($last_odometer_reading)){
					echo "Equipment ID :".$plan->asset_id." | Not has Last Odometer Reading <br>";
		 			continue;
		 		}
				$plan_meta_descriptions=$wpdb->get_results("SELECT * from en_mb_maintenance_plans_meta WHERE maintenance_type='Usage Based' AND plan_id='".$plan->id."'");
				if(!empty($plan_meta_descriptions)){
				 	foreach ($plan_meta_descriptions as $metakey => $description) {
					 		if($description->benchmark_odometer==""){
					 			echo "Equipment ID :".$plan->asset_id." | Not has Benchmark Odometer Reading <br>";
					 			continue;
					 		}
				 			$benchmark_odometer 	= $description->benchmark_odometer;
				 			

				 			$criteria 				= $description->criteria;
				 			$odometer_difference 	= ($last_odometer_reading-$benchmark_odometer);
				 			
				 			if($odometer_difference >= $criteria){
				 				$sql = 	"INSERT INTO en_mb_tracking_active_items
					 				 (
					 				 	asset_id,
					 				 	due_date,
					 				 	description,
					 				 	activity_status,
					 				 	item_type,
					 				 	cron_type, 
					 				 	instruction,
					 				 	instructions_for_next_visit
					 				 )
					 				 VALUES 
					 				 (
					 				 	'".$plan->asset_id."',
					 				 	'".date("Y-m-d")."',
					 				 	'".$description->choice_name."',
					 				 	'7',
					 				 	'Maintenance',
					 				 	'Usage Based',
					 				 	'".$plan->instructions."',
					 				 	'".$plan->instructions_for_next_visit."'
					 				 )";
					 				  
			         			$wpdb->query($sql);
			         			// Set Last Due staus 1 in plan meta.
								$wpdb->update(
											'en_mb_maintenance_plans_meta', 
											array('benchmark_odometer'=>$last_odometer_reading), 
											array('id'=>$description->id)
										);
								update_facility_equipment_main_status($plan->asset_id);
			         			echo "Equipment ID :".$plan->asset_id." | ".$description->choice_name." | Maintenance Due <br>";
				 			}else{
				 				echo "Equipment ID :".$plan->asset_id." | ".$description->choice_name." | Maintenance Not Due Yet<br>";
				 			}
					 		
					 	} //End mete forech
				}else{
					echo "Equipment ID :".$plan->asset_id." | No Active Maintenance Item <br>";
				}	
			} // End foreach

		} else{
			wp_die("No epuipment has plan.");
		}
	}
endif;



/////////////////////////////////////////////////////////////////////////////////////////*/
/********************4 Cron Maintenance Planner Criteria Type Distance Based****************/
/////////////////////////////////////////////////////////////////////////////////////////*/

add_action( 'acm_cron_maintenance_planner_CT_distance_based', 'acm_cron_maintenance_planner_CT_distance_based' );
if ( ! wp_next_scheduled( 'acm_cron_maintenance_planner_CT_distance_based' ) ) {
  wp_schedule_event( time(), 'daily', 'acm_cron_maintenance_planner_CT_distance_based' );
}

if ( ! function_exists( 'acm_cron_maintenance_planner_CT_distance_based' ) ):
function acm_cron_maintenance_planner_CT_distance_based(){
		global $wpdb;
		$get_plan_sql= "SELECT 
						en_mb_maintenance_plans.id,
						en_mb_maintenance_plans.asset_id,
						en_mb_maintenance_plans.reoccurring,
						en_mb_maintenance_plans.period,
						en_mb_maintenance_plans.instructions,
						en_mb_maintenance_plans.scheduled_date,
						(SELECT odometer FROM en_avte_fuel WHERE en_avte_fuel.car_id=en_mb_maintenance_plans.asset_id ORDER BY en_avte_fuel.id DESC LIMIT 1) as last_odometer_reading,

						(SELECT en_mb_maintenance_records.instructions_for_next_visit FROM en_mb_maintenance_records WHERE en_mb_maintenance_records.asset_id=en_mb_maintenance_plans.asset_id ORDER BY en_mb_maintenance_records.id DESC LIMIT 1) as instructions_for_next_visit
						FROM en_mb_maintenance_plans 
						LEFT JOIN en_mb_facility_equipment 
						ON en_mb_facility_equipment.asset_id=en_mb_maintenance_plans.asset_id 
						WHERE en_mb_facility_equipment.maintenance_criteria IN ('Hours','Miles','Kilometers')";
		$get_plans= $wpdb->get_results($get_plan_sql);

		if($get_plans){
			foreach ($get_plans as $key => $plan) {
				if ( function_exists( 'lm_get_latest_odometer_reading' ) ){
	 				$last_odometer_reading  = lm_get_latest_odometer_reading($plan->asset_id);
	 			}else{
	 				$last_odometer_reading  = $plan->last_odometer_reading;
	 			}



				if(empty($last_odometer_reading)){
					echo "Equipment ID :".$plan->asset_id." | Not has Last Odometer Reading <br>";
		 			continue;
		 		}
				$plan_meta_descriptions=$wpdb->get_results("SELECT * from en_mb_maintenance_plans_meta WHERE maintenance_type='Distance Based' AND due_status = 0 AND plan_id='".$plan->id."'");

		 
				if(!empty($plan_meta_descriptions)){
				 	foreach ($plan_meta_descriptions as $metakey => $description) {
					 	
				 			$distance 				= $description->distance;
				 			//$last_odometer_reading  = $plan->last_odometer_reading;
				 		 
				 			if(!empty($distance ) && $last_odometer_reading >= $distance){
				 				 $sql = 	"INSERT INTO en_mb_tracking_active_items
					 				 (
					 				 	asset_id,
					 				 	due_date,
					 				 	description,
					 				 	activity_status,
					 				 	item_type,
					 				 	cron_type, 
					 				 	instruction,
					 				 	instructions_for_next_visit
					 				 )
					 				 VALUES 
					 				 (
					 				 	'".$plan->asset_id."',
					 				 	'".date("Y-m-d")."',
					 				 	'".$description->distance." ".$description->criteria_type." ',
					 				 	'7',
					 				 	'Maintenance',
					 				 	'Distance Based',
					 				 	'".$plan->instructions."',
					 				 	'".$plan->instructions_for_next_visit."'
					 				 )";

			         			$wpdb->query($sql);


			         			// Set Last Due staus 1 in plan meta.
								$wpdb->update(
											'en_mb_maintenance_plans_meta', 
											array('due_status'=>1), 
											array('id'=>$description->id)
										);
								update_facility_equipment_main_status( $plan->asset_id);
								
			         			echo "Equipment ID :".$plan->asset_id." | ".$description->distance." ".$description->criteria_type." | Maintenance Due <br>";
				 			}else{
				 				echo "Equipment ID :".$plan->asset_id." | ".$description->distance." | Maintenance Not Due Yet<br>";
				 			}
					 		
					 	} //End mete forech
				}else{
					echo "Equipment ID :".$plan->asset_id." | No Active Maintenance Item <br>";
				}	
			} // End foreach

		} else{
			wp_die("No epuipment has plan.");
		}
	}
endif;	


/////////////////////////////////////////////////////////////////////////////////////////*/
/********************5 Cron Maintenance Planner Criteria Type Date Based****************/////
/////////////////////////////////////////////////////////////////////////////////////////*/


if ( ! wp_next_scheduled( 'acm_cron_maintenance_planner_CT_date_based' ) ) {
  wp_schedule_event( time(), 'daily', 'acm_cron_maintenance_planner_CT_date_based' );
}

add_action( 'acm_cron_maintenance_planner_CT_date_based', 'acm_cron_maintenance_planner_CT_date_based' );
if ( ! function_exists( 'acm_cron_maintenance_planner_CT_date_based' ) ):  
	function acm_cron_maintenance_planner_CT_date_based(){
		global $wpdb;
		$get_plan_sql= "SELECT 
						en_mb_maintenance_plans.id,
						en_mb_maintenance_plans.asset_id,
						en_mb_maintenance_plans.reoccurring,
						en_mb_maintenance_plans.period,
						en_mb_maintenance_plans.instructions,
						en_mb_maintenance_plans.scheduled_date,

						(SELECT en_mb_maintenance_records.instructions_for_next_visit FROM en_mb_maintenance_records WHERE en_mb_maintenance_records.asset_id=en_mb_maintenance_plans.asset_id ORDER BY en_mb_maintenance_records.id DESC LIMIT 1) as instructions_for_next_visit

						FROM en_mb_maintenance_plans 
						LEFT JOIN en_mb_facility_equipment 
						ON en_mb_facility_equipment.asset_id=en_mb_maintenance_plans.asset_id 
						WHERE en_mb_facility_equipment.maintenance_criteria!='None' AND en_mb_maintenance_plans.status='1' ";


		$get_plans= $wpdb->get_results($get_plan_sql);

		if($get_plans){
			foreach ($get_plans as $key => $plan) {

				if(empty($plan->scheduled_date)){
					echo "Equipment ID :".$plan->asset_id." | Secheduled date not set Yet<br>";
					continue;
				}
			 
				if($plan->reoccurring==0 && strtotime($plan->scheduled_date)<=strtotime(date("Y-m-d"))){
					/*---If criteria type is Date and reoccurring if off-----*/
					$description="One-Time Maintenance Check";
	
					update_tracking_active_items_for_maintenance(
																	$plan->id,
																	$plan->asset_id,
																	"",
																	$description,
																	'Date Based',
																	$plan->instructions,
																	$plan->instructions_for_next_visit
																);



					update_facility_equipment_main_status($plan->asset_id);

				}else if($plan->reoccurring==1 ) { 
					/*---If criteria type is Date and reoccurring if on-----*/

					$initialDate= $plan->scheduled_date;
					switch ($plan->period) {
					    case 'Daily':

							$due_date = date('Y-m-d', strtotime('1 days', strtotime(date("Y-m-d"))));
					        break;

					    case 'Weekly':
					       	
							$due_date = date('Y-m-d', strtotime('7 days', strtotime($initialDate)));
					        break;

					    case 'Monthly':

					        $due_date = date('Y-m-d', strtotime('1 months', strtotime($initialDate)));
					        
					        break;

					    case 'Quarterly':

					        $due_date = date('Y-m-d', strtotime('3 months', strtotime($initialDate)));
					        break;

					    case 'Semi Annually':

					        $due_date = date('Y-m-d', strtotime('6 months', strtotime($initialDate)));
					        break;

					    case 'Annually':

					        $due_date = date('Y-m-d', strtotime('12 months', strtotime($initialDate)));
					        break;

					    default:
					    	continue;       
					}

					if(strtotime($initialDate)<=strtotime(date("Y-m-d"))){
						$description=$plan->period." Maintenance";
						update_tracking_active_items_for_maintenance(
																		$plan->id,
																		$plan->asset_id,
																		$due_date,
																		$description,
																		'Date Based',
																		$plan->instructions,
																		$plan->instructions_for_next_visit
																	);
						update_facility_equipment_main_status($plan->asset_id);
						
					}else{
						echo "Equipment ID :".$plan->asset_id." | Maintenance Not Due Yet<br>";
					}	

				} else{
					echo "Equipment ID :".$plan->asset_id." | Maintenance Not Due Yet<br>";
				} // End If
				 
			} // End foreach

		} else{
			wp_die("No equipment has plan.");
		}
	}
endif;


/////////////////////////////////////////////////////////////////////////////////*/
/********************Update Tracking Active Table Status ************************/
/////////////////////////////////////////////////////////////////////////////////*/

if ( ! function_exists( 'update_tracking_active_items_for_maintenance' ) ):  
	function update_tracking_active_items_for_maintenance($plan_id,$asset_id,$due_date,$description,$cron_type,$genral_instructions,$instructions_for_next_visit){
		global $wpdb;
		// Set data in en_mb_tracking_active_items table

		$status_facility_equipment= array();
		$sql = "INSERT INTO en_mb_tracking_active_items 
					(
						asset_id, 
						due_date, 
						description,
						activity_status, 
						item_type, 
						cron_type, 
						instruction,
						instructions_for_next_visit
					) 
					VALUES 
					(
						'".$asset_id."', 
						'".date("Y-m-d")."', 
						'".$description."', 
						'7', 
						'Maintenance', 
						'".$cron_type."',
						'".$genral_instructions."',
						'".$instructions_for_next_visit."'
					)";
					  
	     		$wpdb->query($sql);

		// Set Last Due Date in planner.
		$wpdb->update(
						'en_mb_maintenance_plans', 
						array('scheduled_date'=>$due_date), 
						array('id'=>$plan_id)
					);

		// Set Maintenance Due for this equipment in main table.
		$pending_items_sql 	= "SELECT
							  activity_status,
							  description 
							  FROM en_mb_tracking_active_items 
							  WHERE asset_id='".$asset_id."' AND activity_status NOT IN (10,12)";
		$pending_items 		= $wpdb->get_results($pending_items_sql);

		foreach ($pending_items as $key => $item) {
			$status_facility_equipment[]=$item->description;
		}

		$wpdb->update(
						'en_mb_facility_equipment', 
						array('status'=>implode(",", $status_facility_equipment)), 
						array('asset_id'=>$asset_id)
					);

		echo "Equipment ID :".$asset_id." | Maintenance Due <br>";
	}
endif;	





//////////////////////////////////////////////////////////////////////////////////////////*/
/*********************************6 Cron calibration Planner Set ***************************/
////////////////////////////////////////////////////////////////////////////////////////*/

if ( ! wp_next_scheduled( 'acm_cron_calibration_planner' ) ) {
  	wp_schedule_event( time(), 'daily', 'acm_cron_calibration_planner' );
} 
add_action( 'acm_cron_calibration_planner', 'acm_cron_calibration_planner' );


if ( ! function_exists( 'acm_cron_calibration_planner' ) ):  
	function acm_cron_calibration_planner(){

		global $wpdb;
		$get_plan_sql= "SELECT 
						en_mb_calibrations_plans.*			 
						FROM en_mb_calibrations_plans 
						WHERE en_mb_calibrations_plans.status='1' ";
		$get_plans= $wpdb->get_results($get_plan_sql);
	
		if($get_plans){
			foreach ($get_plans as $key => $plan) {

				if(empty($plan->scheduled_calibration)){
					echo "Equipment ID :".$plan->asset_id." | Secheduled date not set Yet<br>";
					continue;
				}
			 
			 
				if($plan->reoccurring==0 && strtotime($plan->scheduled_calibration)<=strtotime(date("Y-m-d"))){
					/*---If reoccurring if off-----*/
					update_tracking_active_items_calibration($plan->id,$plan->asset_id,'',$plan->calibration_instructions,'Calibration/Inspection');
					update_facility_equipment_main_status($plan->asset_id);

					echo "Equipment ID :".$plan->asset_id." | One time calibration due<br>";

				}else if($plan->reoccurring==1 ) { 
					/*---Reoccurring if on-----*/

					$initialDate 	= $plan->scheduled_calibration; 
					$period 		= $plan->period." Calibration/Inspection";
					switch ($plan->period) {
					    
					    case 'Monthly':

					        $due_date = date('Y-m-d', strtotime('1 months', strtotime($initialDate)));
					        break;

					    case 'Quarterly':

					        $due_date = date('Y-m-d', strtotime('3 months', strtotime($initialDate)));
					        break;

						case 'Semi­-Annually':

					        $due_date = date('Y-m-d', strtotime('6 months', strtotime($initialDate)));
					        break;

					    case 'Annually':

					        $due_date = date('Y-m-d', strtotime('12 months', strtotime($initialDate)));
					        break; 

					    case 'Bi­-Annually':

					        $due_date = date('Y-m-d', strtotime('24 months', strtotime($initialDate)));
					        break;

					    default:
					    
					    	continue;       
					}
				 

					if(strtotime($initialDate)<=strtotime(date("Y-m-d"))){

						update_tracking_active_items_calibration($plan->id,$plan->asset_id,$due_date,$plan->calibration_instructions,$period);	
						update_facility_equipment_main_status($plan->asset_id);

						echo "Equipment ID :".$plan->asset_id." | ".$plan->period." Calibration Due<br>";
						
					}else{
						echo "Equipment ID :".$plan->asset_id." | Calibration Not Due Yet<br>";
					}	

				} else{
					echo "Equipment ID :".$plan->asset_id." | Calibration Not Due Yet<br>";
				} // End If
				 
			} // End foreach

		} else{
			wp_die("No epuipment has plan.");
		}

	}
endif;


/////////////////////////////////////////////////////////////////////////////////////////*/
/********************Cron Maintenance Planner Criteria Type Date Based*******************/

if ( ! function_exists( 'update_tracking_active_items_calibration' ) ):  
	 
	function update_tracking_active_items_calibration($plan_id,$asset_id,$due_date,$instructions,$description,$due_status=5){
		global $wpdb;
		$status_facility_equipment= array();
		
		$sql = "INSERT INTO en_mb_tracking_active_items 
					(
						asset_id, 
						due_date, 
						description,
						activity_status, 
						item_type, 
						cron_type, 
						instruction
					) 
					VALUES 
					(
						'".$asset_id."', 
						'".date("Y-m-d")."', 
						'".$description."',
						'".$due_status."', 
						'Calibration', 
						'Calibration Based', 
						'".$instructions."'
					)";
	     $wpdb->query($sql);
		
	     if($description!="Calibrate/Inspect on next use"){
	     	// Set Last Due Date in planner.
			$wpdb->update(
						'en_mb_calibrations_plans', 
						array('scheduled_calibration'=>$due_date), 
						array('id'=>$plan_id)
					);
	     }
		

		// Set Calibration Due for this equipment in main table.
		$pending_items_sql 	="SELECT
							  activity_status,
							  description 
							  FROM en_mb_tracking_active_items 
							  WHERE asset_id='".$asset_id."' AND activity_status NOT IN (10,12)";
		$pending_items 		= $wpdb->get_results($pending_items_sql);

		foreach ($pending_items as $key => $item) {
			$status_facility_equipment[]=$item->description;
		}

		$wpdb->update(
						'en_mb_facility_equipment', 
						array('status'=>implode(",", $status_facility_equipment)), 
						array('asset_id'=>$asset_id)
					);

		//echo "Equipment ID :".$asset_id." | Calibration Due <br>";
	}
endif;



if ( ! function_exists( 'get_action_links_for_equipment' ) ):  
	function get_action_links_for_equipment($equipment_type_id,$asset_id){
		global $wpdb;

		$linksData = $wpdb->get_row("select * from en_mb_equipment_type_relation_settings where equipment_type='".$equipment_type_id."'", OBJECT );
		$action_linksArr = explode(",",$linksData->action_links);

		$action_links="";
		if(in_array('Fuel Tracker', $action_linksArr )){

			$action_links.="<a title='Fuel Tracker' class='action-links' href='".get_permalink(get_option('_en_mb_page_15'))."?asset_id=".$asset_id."'><img src='".ACM_PLUGIN_PATH."icons/icon-4.png'> </a>";
		}
		if(in_array('Maintenance Records', $action_linksArr )){

			$action_links.="<a title='Maintenance Records' class='action-links' href='".get_permalink(get_option('_en_mb_page_3'))."?asset_id=".$asset_id."'><img src='".ACM_PLUGIN_PATH."icons/icon-2.png'> </a>";
		}

		if(in_array('Calibration Records', $action_linksArr )){

			$action_links.="<a title='Calibration/Inspection' class='action-links' href='".get_permalink(get_option('_en_mb_page_4'))."?asset_id=".$asset_id."'><img src='".ACM_PLUGIN_PATH."icons/icon1.png'> </a>";

			 
		}

		if(in_array('Equipment Details', $action_linksArr )){

			$action_links.="<a title='Calibration/Inspection' class='action-links' href='".get_permalink(get_option('_en_mb_page_2'))."?asset_id=".$asset_id."'><img src='".ACM_PLUGIN_PATH."icons/icon-3.png'> </a>";

			 
		}

		return $action_links;
	}
endif;

/////////////////////////////////////////////////////////////////////////////////////////*/


if ( ! function_exists( 'get_action_links_trigger' ) ):  
	function get_action_links_trigger($equipment_type_id){
		global $wpdb;

		$linksData = $wpdb->get_row("select * from en_mb_equipment_type_relation_settings where equipment_type='".$equipment_type_id."'", OBJECT );
		$action_linksArr = explode(",",$linksData->action_links);

		$action_links="";
		if(in_array('Fuel Tracker', $action_linksArr )){

			$action_links.='<a title="Fuel Tracker" class="action-links action-fuel-tracker" data-href="'.get_permalink(get_option('_en_mb_page_15')).'?asset_id="><img class="action-icon"  src="'.ACM_PLUGIN_PATH.'icons/icon-4.png"></a>';
		}
		if(in_array('Maintenance Records', $action_linksArr )){

			$action_links.='<a title="Maintenance Records" class="action-links action-maintenance-records" data-href="'.get_permalink(get_option('_en_mb_page_3')).'?asset_id="><img class="action-icon" src="'.ACM_PLUGIN_PATH.'icons/icon-2.png"> </a>';

		}

		if(in_array('Calibration Records', $action_linksArr )){

			 
			$action_links.='<a title="Calibration/Inspection" class="action-links action-calibration-records" data-href="'.get_permalink(get_option('_en_mb_page_4')).'?asset_id="><img class="action-icon" src="'.ACM_PLUGIN_PATH.'icons/icon1.png"> </a>';

			 
		}

		if(in_array('Equipment Details', $action_linksArr )){

			$action_links.='<a title="Equipment Details" class="action-links action-equipment-details" data-href="'.get_permalink(get_option('_en_mb_page_2')).'?asset_id="><img class="action-icon" src="'.ACM_PLUGIN_PATH.'icons/icon-3.png"> </a>'; 
		}

		return $action_links;
		 
	}
endif;



?>