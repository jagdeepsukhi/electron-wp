<?php
/*
* Template Name: Active Alert Calibration/Inspection
* Description: This template is only of Active Calibration alert page. Wpdatatable shortcode dynamically added into this template according to capabilities. 
*/
get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 


if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}



/* ************************Get All Equipments************************ */
$page_slug					= 'calibration-inspection-records';
$equipmentData				= array();
$equipment_type_ids_array	= array();
$equipment_sql				= "SELECT equipment_type,id FROM en_mb_equipment_types";
$equipmentArr 				= $wpdb->get_results( $equipment_sql, OBJECT );
foreach ($equipmentArr as $key => $equipment) {
	$equipmentData[str_replace(' ', '-', strtolower($equipment->equipment_type))]=$equipment->id;
}

/* *******************Get Capebilitiies for this Page***************** */
$userData = get_userdata( get_current_user_id() );
if ( is_object( $userData) ) {
    $current_user_caps = $userData->allcaps; 
	$current_user_roles = $userData->roles; 

	$current_user_caps_custom = array();
	foreach ($current_user_roles as $kkey => $role) {
	    $roleData=get_role($role);
	    $capabilities = $roleData->capabilities;

	    
	    foreach ($capabilities as $ckey => $capValue) {
	        //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

	        if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
	                continue;
	        }else{

	                $current_user_caps_custom[$ckey]=$capValue;
	        }
	    }


	}
	   
	foreach ($current_user_caps_custom as $capability => $status) { 
   // foreach ($current_user_caps as $capability => $status) {
    	if($status==1){
    		$capabilityString_Arr= explode("_",$capability);
    		$equipment_type_name=$capabilityString_Arr[1];
    		$is_edit=$capabilityString_Arr[3];
    		if(in_array($page_slug, $capabilityString_Arr)) {
    			$equipment_type_ids_array[]=$equipmentData[$equipment_type_name];
    			if($is_edit=="edit"){
    				$equipment_type_allowed_edit_ids[]=$equipmentData[$equipment_type_name];
    			}
    			
    		}
    		
    	}
    }
}

if(!empty(array_unique($equipment_type_ids_array))){
	$equipment_types_string=implode(",", array_filter(array_unique($equipment_type_ids_array))); 
}else{
	wp_die( "You do not have permission to view this page.");
}
/* *******************Get Shortcode based on cabeblity***************** */
	$shortcode_sql	= "SELECT active_calibration_inspection_items FROM en_mb_equipment_group_templates Where group_template IN (".$equipment_types_string.") GROUP BY active_calibration_inspection_items Limit 1";

	$shortcodeArr 	= $wpdb->get_row( $shortcode_sql, OBJECT );
	if(!empty($shortcodeArr) && !empty($shortcodeArr->active_calibration_inspection_items)){
	 	$wpTable_Shortcode = "[wpdatatable id=".$shortcodeArr->active_calibration_inspection_items." VAR1='(".$equipment_types_string.")']";
	}else{
		wp_die( "No WP Datatable set for this page.");
	}
 
?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">

					<?php echo do_shortcode($wpTable_Shortcode); ?>	

					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->


<!-- Prepaer Asset ID drop down -->
<?php 
	$equip_sql	= "SELECT asset_id FROM `en_mb_facility_equipment` where equipment_type IN ($equipment_types_string) ORDER BY `asset_id`  ASC";
	$equip_sqlArr 	= $wpdb->get_results( $equip_sql, OBJECT );
	$options ="<option value=''></option>";
	$optionsli = '<li data-original-index="0" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text"></span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>';
	$index=1;

	foreach ($equip_sqlArr as $Ekey => $equip) {

		$options.="<option value='".$equip->asset_id."'>".$equip->asset_id."</option>";
		$optionsli .= '<li data-original-index="'.$index.'"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">'.$equip->asset_id.'</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>';
		# code...
		$index++;
	}
 
?>
 

<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery("body").on('click', 'a.action-links',function(){
		     window.location = jQuery(this).attr("href");    
		});
		jQuery("body").on('click', '.DTTT_button_edit',function(){  
			jQuery("#table_1_asset_id").prev().prev().removeAttr('data-toggle');
			jQuery("#table_1_recorded_by").parent().parent().parent().hide();
 			jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");
 			
			var maintenance_status_list=<?php echo json_encode(maintenance_status_list()); ?>; 
			jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
				var text= jQuery( this ).text();
				if(jQuery.inArray( text, maintenance_status_list ) > -1)
				{
				 	jQuery( this ).hide();
				} 
			}); 
		    jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
				 var text= jQuery( this ).text();
				 
				 if(text=="Calibration/Inspection Due" || text=="Returned from Calibration/Inspection" ||  text=="" || text=="Out for Calibration/Inspection" || text=="Calibrate/Inspect on next use" ){
				 	jQuery( this ).show();
				 }  
				 
			}); 


		    jQuery("#table_1_recorded_by").parent().parent().parent().hide();
 			jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");	
 			

 			jQuery("#table_1_item_type").parent().parent().parent().hide();
 			jQuery("#table_1_item_type").val("Calibration");

		});
		jQuery("body").on('click', '.DTTT_button_new',function(){

			jQuery("#table_1_asset_id").prev().prev().attr('data-toggle','dropdown');
			 /*Prepaer Asset ID drop down*/
		    jQuery("#table_1_asset_id").html("<?php echo $options;?>");
		    jQuery("#table_1_asset_id").prev().children('ul').html('<?php echo $optionsli; ?>');

		    jQuery("#table_1_recorded_by").parent().parent().parent().hide();
 			jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");	

 			jQuery("#table_1_item_type").parent().parent().parent().hide();
 			jQuery("#table_1_item_type").val("Calibration");	


 			var maintenance_status_list=<?php echo json_encode(maintenance_status_list()); ?>; 
			jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
				var text= jQuery( this ).text();
				if(jQuery.inArray( text, maintenance_status_list ) > -1)
				{
				 	jQuery( this ).hide();
				} 
			}); 
		    jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
				 var text= jQuery( this ).text();
				 
				 if(text=="Calibration/Inspection Due" || text=="Returned from Calibration/Inspection" ||  text=="" || text=="Out for Calibration/Inspection" || text=="Calibrate/Inspect on next use" ){
				 	jQuery( this ).show();
				 }  
				 
			}); 
		});
	});
</script>
<style type="text/css">
 
 
.DTTT_button_delete {
    display: none;
}
button#table_1_apply_edit_dialog {
    display: none;
}
#table_1_filter{display: none;}
</style>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		 
		var equipmentData = <?php echo json_encode($equipmentData); ?>;
		var equipment_type_allowed_edit_ids = <?php echo json_encode($equipment_type_allowed_edit_ids); ?>;
		jQuery("body").on('click', '#table_1 tr',function(event){
			var equipment_type = jQuery(this).find(".equipment_type").text();
			if(equipment_type!=""){
				equipment_type = equipment_type.toLowerCase();
				equipment_type = equipment_type.split(' ').join('-');
				 
				if ( jQuery.inArray(equipmentData[equipment_type], equipment_type_allowed_edit_ids) == -1 ) {
					 
                    setTimeout(function() 
					{
					    jQuery('.wpDataTablesPopover.editTools').hide();
					    //jQuery("#editNot-allowed").modal("show");
					    if(event.target.tagName.toLowerCase() == 'td' ){
						    	 jQuery("#edit-allow").modal("show");
						   		
						}
					      
					}, 40);
				}  
			}    
		});	
	 
	});
	 
</script>
<!-- Modal -->
<div id="edit-allow" class="modal fade" role="dialog" style="z-index: 9999;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alert</h4>
      </div>
      <div class="modal-body">
		<div class="alert alert-danger">
		   <p>You do not have permission to edit this record.</p>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.modal-backdrop.fade.in {
    z-index: 999;
}
</style>
<?php
get_footer();
