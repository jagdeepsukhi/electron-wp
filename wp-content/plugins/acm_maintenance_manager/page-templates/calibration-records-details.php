<?php
/*
Template Name: Calibration Record Details 
*/

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 

if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}


$user = wp_get_current_user();
if(!empty($user) && isset($_GET['asset_id'])){
  $asset_id=$_GET['asset_id'];
  global $wpdb;
  $equipmentDetail=$wpdb->get_row("SELECT en_mb_equipment_types.id , en_mb_equipment_types.equipment_type FROM en_mb_equipment_types JOIN en_mb_facility_equipment ON en_mb_facility_equipment.equipment_type=en_mb_equipment_types.id WHERE en_mb_facility_equipment.asset_id='".$asset_id."'", OBJECT );

  /* ************************Set variables************************ */
  $page_slug                  = 'calibration-inspection-records';

  $plan_slug                  = 'calibration-plan'; // Parmission should be same for maintenance record and active maintenance

  $equipment_type_string      = str_replace(' ', '-', strtolower($equipmentDetail->equipment_type));
  $capability_for_page_view   = trim("enet_".$equipment_type_string."_".$page_slug."_view"); 
  $capability_for_page_edit   = trim("enet_".$equipment_type_string."_".$page_slug."_edit");

  $capability_for_plan_view   = trim("enet_".$equipment_type_string."_".$plan_slug."_view"); 
  $capability_for_plan_edit   = trim("enet_".$equipment_type_string."_".$plan_slug."_edit");

  $capability_for_docs_upload = trim("enet_calibration_documents_upload");

  $allow_view         = 0;
  $allow_edit         = 0;

  $allow_plan_view    = 0;
  $allow_plan_edit    = 0;

  $allow_docs_upload  = 0;
    
  /* *******************Get Capebilitiies for this Page***************** */
  $userData = get_userdata( get_current_user_id() );
  if ( is_object( $userData) ) {
      $current_user_caps = $userData->allcaps; 

      $current_user_roles = $userData->roles; 
      $current_user_caps_custom = array();
      
      foreach ($current_user_roles as $kkey => $role) {
            $roleData=get_role($role);
            $capabilities = $roleData->capabilities;
      
            
            foreach ($capabilities as $ckey => $capValue) {
                //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

                if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
                        continue;
                }else{

                        $current_user_caps_custom[$ckey]=$capValue;
                }
            }
     
      
        }
           
      foreach ($current_user_caps_custom as $capability => $status) {  
        if($capability!==0){
          
          if($status==1 && (trim($capability)==$capability_for_page_view) ){  
            $allow_view =1;   
          }
          if($status==1 && $capability==$capability_for_page_edit){  
            $allow_edit =1;  
          }

           // Check plan edit/view permission
            if($status==1 && (trim($capability)==$capability_for_plan_view) ){  
                $allow_plan_view =1;     
            }
            if($status==1 && (trim($capability)==$capability_for_plan_edit) ){  
                $allow_plan_edit =1;     
            }


          if($status==1 && $capability==$capability_for_docs_upload){  
            $allow_docs_upload =1;  
          }
        }
      }
  }
  

  /* *******************Set Edit capability***************** */
  function wpdatatables_allow_edit_table_callback( $edit ) {
      global $allow_edit;
      return $allow_edit;  
  }

  add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );
 
  /* *******************Get Shortcode based on capability***************** */
  if($allow_view==1 || $allow_edit  ==1){
      $shortcode_sql  = "SELECT calibration_inspection_records FROM en_mb_equipment_group_templates where group_template ='".$equipmentDetail->id."' Limit 1";

      $shortcodeArr   = $wpdb->get_row( $shortcode_sql, OBJECT );
      if(!empty($shortcodeArr)  && !empty($shortcodeArr->calibration_inspection_records)){
        $wpTable_Shortcode = "[wpdatatable id=".$shortcodeArr->calibration_inspection_records." Var1='".$asset_id."']";
        $assetSql="select asset_id from en_mb_facility_equipment where equipment_type = '".$equipmentDetail->id."' ";
        $assetArr = $wpdb->get_results( $assetSql, OBJECT );
      }else{
        wp_die( "WP Datatable not available for this page.");
      }
  }else {
    wp_die( "You do not have permission to view this page.");
  }
 
}else {
    wp_die( "You do not have permission to view this page.");
}

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?> for: <?php echo $asset_id; ?></h1>
					<div class="wpDataTableFilterSection" id="table_1_3_filter_sections">
          <?php if($allow_plan_view == 1 || $allow_plan_edit == 1) {?>
						<!-- Trigger the modal with a button -->
						<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#calibration_plan_popup">Create Calibration Plan</button>
            <?php } ?>
					</div>

					<div class="wpDataTableFilterSection" id="table_1_3_filter_sections">
					<form action="" method="get" id="asset_filter_form">
                        <select name="asset_id" id="asset_id_select">
                        	<option>Find a different asset</option>
                        	<?php foreach ($assetArr as $key => $asset) { ?>
                        		<option value="<?php echo $asset->asset_id; ?>"><?php echo $asset->asset_id; ?></option>
                        	<?php } ?>
                        	 
                        </select>
                    </form>     	
                    </div>

				<?php endif; ?>

					<div class="entry-content">

					<?php echo do_shortcode($wpTable_Shortcode); ?>	


					<?php the_content(); ?>
					</div> <!-- .entry-content -->

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<div id="calibration_plan_popup" class="modal fade" role="dialog" style="z-index: 99999;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <form action="" method="post" id="create-calibration-plan-form">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Calibration/Inspection Plan for Asset: <?php echo $asset_id; ?></h4>
      </div>
  <?php 
  $planDetails 	=$wpdb->get_row("select * from en_mb_calibrations_plans where asset_id='".$asset_id."'", OBJECT );
  ?>
      <div class="modal-body row">
         <div class="col-md-12 align-center">
         	<h5 class="in-heading">Calibration Schedule</h5>
         	
         	<ul class="scheduled_date_based">
         	<?php 
         		$scheduled_date=(!empty($planDetails->scheduled_calibration)) ? date_format(date_create($planDetails->scheduled_calibration),"d M Y") : '';


         	?>

          <?php if($planDetails->next_calibrate==1) { $datedisabled= "disabled"; } else { $datedisabled= "";} ?>

         		<li>Next Scheduled Calibration:&nbsp;<input type="text" value="<?php echo $scheduled_date;?>" name="scheduled_date" id="scheduled_date" class="wdt-datepicker" <?php echo $datedisabled;?>>&nbsp;<i class="fa fa-calendar" ></i>


            <?php if(!empty($scheduled_date)) { $disabled= "disabled"; } else { $disabled= "";} ?>



            &nbsp;&nbsp;OR Calibrate/Inspect on next use <input type="checkbox" value="1" name="next_calibrate" id="next_calibrate" <?php if($planDetails->next_calibrate==1){echo "checked"; } ?> <?php echo $disabled;?>>
            </li>

           
         		<li>Calibration is reocurring:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="reoccurring" value="1" id="reoccurring" class="" <?php if($planDetails->reoccurring==1) { echo "checked"; } ?>></li>
         		<li>
         		 
         			<?php if($planDetails->reoccurring!=1) { $disabled= "disabled"; } else { $disabled= "";} ?>

         			Monthly <input type="radio" name="period" value="Monthly" class="period" <?php echo $disabled ?> <?php if($planDetails->period=='Monthly') { echo "checked"; } ?>>
         			&nbsp;Quarterly <input type="radio" name="period" value="Quarterly" class="period" <?php echo $disabled ?> <?php if($planDetails->period=='Quarterly') { echo "checked"; } ?>>
         			&nbsp;Semi­-Annually <input type="radio" name="period" value="Semi­-Annually" class="period" <?php echo $disabled ?> <?php if($planDetails->period=='Semi­-Annually') { echo "checked"; } ?>>
         			&nbsp;Annually <input type="radio" name="period" value="Annually" class="period" <?php echo $disabled ?> <?php if($planDetails->period=='Annually') { echo "checked"; } ?>>
         			&nbsp;Bi­-Annually <input type="radio" name="period" value="Bi­-Annually" class="period" <?php echo $disabled ?> <?php if($planDetails->period=='Bi­-Annually') { echo "checked"; } ?>>

         		</li>

         	</ul>

         </div>

         
 		<div class="col-md-12">
 				<hr>
 				<h5 style="text-align: center;" class="in-heading" >Calibration Instructions</h5> 
 				<textarea id="instructions" name="instructions"><?php echo $planDetails->calibration_instructions;?></textarea>
 			</div>
      </div>
      <div class="modal-footer">
      	<input type="hidden" name="asset_id" value="<?php echo $asset_id ;?>">
        <input type="hidden" name="action" value="create_calibration_plan">
       <?php if( $allow_plan_edit == 1) {?>
      	<button type="submit" class="btn btn-success ">Save & Exit</button>
        <?php } ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>

  </div>
</div>



<style type="text/css">
input#scheduled_date {width: 30%;}
#calibration_plan_popup .modal-dialog {width: 780px;}	
input#criteria {width: 53px;}
h5.in-heading {font-weight: bold;}
input#distance {width: 80%;}
.border-right {border-right: 1px solid #ccc;}
button.btn.btn-info.btn-lg.waves-effect {margin-bottom: 14px; border-radius: 0px;}
.fa-close {color:red; cursor: pointer;}
.fa-plus {color:green; cursor: pointer;}
.distance_based li, .usage_based li, .scheduled_date_based li { margin-bottom: 10px; }
textarea#instructions {width: 100%;}
input#choice_name {width: 40%;}
.gray-bg {background: #eee !important;}
.align-center {text-align: center;}
#table_1_filter{display: none;}
</style>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php get_footer();?>
<link href="<?php echo ACM_PLUGIN_PATH ;?>/css/toastr.css" rel="stylesheet"/>
<script src="<?php echo ACM_PLUGIN_PATH  ;?>/js/toastr.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){

    //var dateToday = new Date();
    //jQuery( ".en-mb-wdt-datepicker" ).datepicker({  dateFormat: 'dd M yy', minDate:dateToday}); 
     
    jQuery("#asset_id_select").on("change",function(){
      jQuery("#asset_filter_form").submit();
    });
  
    jQuery("body").on("click","#reoccurring",function(){
      if (jQuery(this).is(':checked')) {
        jQuery(".period").removeAttr("disabled");
      }else{
        jQuery(".period").attr("disabled","disabled");
      }
    });
      jQuery('#create-calibration-plan-form').on('submit', function (e) { 
        e.preventDefault();
        var url= "<?php echo admin_url('admin-ajax.php');?>";

        jQuery.ajax({
          type: 'post',
          url: url,
          data: jQuery('#create-calibration-plan-form').serialize(), 
          success: function (res) {
            if(IsJsonString(res)){
                  var response = jQuery.parseJSON(res);
                  if(response.code ==200){
                    toastr.success(response.message );  
                    jQuery("#calibration_plan_popup").modal("hide");
                    //window.location.reload(true);
                  }else{
                    toastr.error("Settings couldn't save.");  
                 
                  }
               }else{
                  toastr.error("Settings couldn't save.");  
                 
               }
          }
        });

      });     

});
function IsJsonString(str) {
      try {
          JSON.parse(str);
      } catch (e) {
          return false;
      }
      return true;
  }
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery("#scheduled_date").on('blur',function(){
      
      if(jQuery(this).val().length > 0){
        jQuery("#next_calibrate").attr("disabled","disabled");
      }else{
        jQuery("#next_calibrate").removeAttr("disabled");
      }
    });

    jQuery("#next_calibrate").on('change',function(){
      if (jQuery(this).is(':checked')) {

         jQuery("#scheduled_date").attr("disabled","disabled");
      }else{
        jQuery("#scheduled_date").removeAttr("disabled");
      }
    });    

  });

</script>
<script type="text/javascript">
  jQuery(document).ready(function(){ 
    jQuery("body").on('click', 'a.action-links',function(){
         window.location = jQuery(this).attr("href");    
    });

    jQuery("body").on('click', '.DTTT_button_edit',function(){
         
        var maintenance_status_list=<?php echo json_encode(maintenance_status_list()); ?>; 
        jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
          var text= jQuery( this ).text();
          if(jQuery.inArray( text, maintenance_status_list ) > -1)
          {
            jQuery( this ).hide();
          } 
        }); 
        jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
         var text= jQuery( this ).text();
         
         if(text=="Calibration/Inspection Due" || text=="In Service" || text=="" || text=="Out for Calibration / Inspection" ){
          jQuery( this ).show();
         } 
         
        }); 

        jQuery("#table_1_asset_id").attr("disabled","disabled");
        jQuery("#table_1_recorded_by").parent().parent().parent().parent().hide();
        jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");  
    });

   
    jQuery("body").on('click', '.DTTT_button_new',function(){
      jQuery("#table_1_asset_id").attr("disabled","disabled");
      jQuery("#table_1_recorded_by").parent().parent().parent().parent().hide();
      jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");  
    });


  });

</script>

<!-- Files Upload Start-->

<!-- Generic page styles -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/css/jquery.fileupload-ui-noscript.css"></noscript>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Documents</h4>
      </div>
      <div class="modal-body">
           <!-- The file upload form used as target for the file upload widget -->
            <form id="fileupload" action="" method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <noscript><input type="hidden" name="redirect" value=""></noscript>
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                <?php if ( $allow_docs_upload==1 ) { ?>
                    <div class="col-lg-12 col-md-12">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Add files...</span>
                            <input type="file" name="files[]" multiple>
                        </span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <button type="button" class="btn btn-danger delete">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <!-- The global file processing state -->
                        <span class="fileupload-process"></span>
                    </div>
                     <?php } else { ?>
                        <div class="col-lg-12 col-md-12">
                            <p><strong>You have documents view only permission.</strong></p>
                         </div>
                    <?php } ?>
                    <!-- The global progress state -->
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
            </form>

        <!-- The blueimp Gallery widget -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

             </div> <!--modal body end -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         
      </div>
    </div>
  </div>
</div>


<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td class="delete-files">
            <?php if ( $allow_docs_upload==1 ) { ?>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}

            <?php } ?>
        </td>
    </tr>
{% } %}
</script>
 

<style type="text/css">
.modal-backdrop.fade.in {
    z-index: 999;
}
</style>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-ui.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery("body").on("click",".documents-btn",function(){
    jQuery("table tbody.files").empty();
    var id= jQuery(this).attr("rel");
    var setPath= "path=calibration/<?php echo $asset_id; ?>/"+id; 
    'use strict';
    var createPath ='<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/server/php/?'+setPath;
   
    // Initialize the jQuery File Upload widget:
    jQuery('#fileupload').fileupload({
        url: createPath,
         acceptFileTypes: /(\.|\/)(gif|jpe?g|png|xlsx|docx|csv|xls|pdf|txt|plain|rtf|doc)$/i
    });

    // Enable iframe cross-domain access via redirect option:
    jQuery('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Load existing files:
   jQuery('#fileupload').addClass('fileupload-processing');
    jQuery.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: jQuery('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: jQuery('#fileupload')[0],
         acceptFileTypes: /(\.|\/)(gif|jpe?g|png|xlsx|docx|csv|xls|pdf|txt|plain|rtf|doc)$/i
    }).always(function () {
        jQuery(this).removeClass('fileupload-processing');
    }).done(function (result) {
        jQuery(this).fileupload('option', 'done').call(this, jQuery.Event('done'), {result: result});
    });

    jQuery("#exampleModal").modal('show');
  });

});

</script>
 