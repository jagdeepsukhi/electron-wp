<?php
/*
Template Name: Equimpent Detail Page
*/

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 

if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}

$user = wp_get_current_user();
if(!empty($user) && isset($_GET['asset_id'])){
	$asset_id=$_GET['asset_id'];
	global $wpdb;
	$equipmentDetail=$wpdb->get_row("SELECT en_mb_equipment_types.id , en_mb_equipment_types.equipment_type FROM en_mb_equipment_types JOIN en_mb_facility_equipment ON en_mb_facility_equipment.equipment_type=en_mb_equipment_types.id WHERE en_mb_facility_equipment.asset_id='".$asset_id."'", OBJECT );

	/* ************************Set variables************************ */
	$page_slug					= 'detail';
	$equipment_type_string		= str_replace(' ', '-', strtolower($equipmentDetail->equipment_type));
	$capability_for_page_view 	= trim("enet_".$equipment_type_string."_detail_view"); 
	$capability_for_page_edit 	= trim("enet_".$equipment_type_string."_detail_edit");
	$allow_view					= 0;
	$allow_edit					= 0;
		
	/* *******************Get Capebilitiies for this Page***************** */
	$userData = get_userdata( get_current_user_id() );
	if ( is_object( $userData) ) {
	    $current_user_caps = $userData->allcaps; 
	  	$current_user_roles = $userData->roles; 

		$current_user_caps_custom = array();
		foreach ($current_user_roles as $kkey => $role) {
		    $roleData=get_role($role);
		    $capabilities = $roleData->capabilities;

		    
		    foreach ($capabilities as $ckey => $capValue) {
		        //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

		        if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
		                continue;
		        }else{

		                $current_user_caps_custom[$ckey]=$capValue;
		        }
		    }


		}
		   
		foreach ($current_user_caps_custom as $capability => $status) { 
	    //foreach ($current_user_caps as $capability => $status) {  
	    	if($capability!==0){
		    	
		    	if($status==1 && (trim($capability)==$capability_for_page_view) ){	
		    		$allow_view	=1;		
		    	}
		    	if($status==1 && $capability==$capability_for_page_edit){	 
		    		$allow_edit	=1;	 
		    	}
	    	}
	    }
	}
	

	/* *******************Set Edit capability***************** */
	function wpdatatables_allow_edit_table_callback( $edit ) {
		global $allow_edit;
	   	return $allow_edit;	 
	}

	add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );
	
	/* *******************Get Shortcode based on capability***************** */
	if($allow_view==1 || $allow_edit	==1){
		$shortcode_sql	= "SELECT detail_view FROM en_mb_equipment_group_templates where group_template ='".$equipmentDetail->id."' Limit 1";

		$shortcodeArr 	= $wpdb->get_row( $shortcode_sql, OBJECT );
		if(!empty($shortcodeArr)){
			$wpTable_Shortcode = "[wpdatatable id=".$shortcodeArr->detail_view." Var1='".$asset_id."']";
		}
	}else {
		wp_die( "You do not have permission to view this page.");
	}
 
}else {
		wp_die( "You do not have permission to view this page.");
}


?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title">Equipment Type : <?php echo $equipmentDetail->equipment_type ?> </h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">

					<?php echo do_shortcode($wpTable_Shortcode); ?>	
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->
<style>
.entry-content h2{display: none;}
a.dt-button.new_table_entry.DTTT_button.DTTT_button_new { display: none; }
#table_1_filter{display: none;}
</style>

<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery("body").on('click', 'a.action-links',function(){
			var asset_id 	= jQuery(this).parent().children(".action_asset_id").val();	 
		    var href 		= jQuery(this).data("href");    
 			var asset_id 	= jQuery(this).parent().children(".action_asset_id").val();	 
		    var href 	 	= jQuery(this).data("href"); 
		    jQuery(this).attr("href",href+asset_id);
		    window.location = href+asset_id;    
		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery("body").on('click', '.DTTT_button_edit',function(){
 			jQuery("#table_1_recorded_by").parent().parent().parent().hide();
 			jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");			
		});
		jQuery("body").on('click', '.DTTT_button_new',function(){
 			jQuery("#table_1_recorded_by").parent().parent().parent().hide();
 			jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");			
		});
	});
</script>

 
<?php
get_footer();



?>












<!-- Files Upload Start-->

<!-- Generic page styles -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/jquery.fileupload.css">
<link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH; ?>Upload-master/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/css/jquery.fileupload-ui-noscript.css"></noscript>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Documents</h4>
      </div>
      <div class="modal-body">
           <!-- The file upload form used as target for the file upload widget -->
            <form id="fileupload" action="" method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <noscript><input type="hidden" name="redirect" value=""></noscript>
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
                <?php if ( $allow_edit==1 ) { ?>
                    <div class="col-lg-12 col-md-12">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Add files...</span>
                            <input type="file" name="files[]" multiple>
                        </span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="glyphicon glyphicon-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-warning cancel">
                            <i class="glyphicon glyphicon-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <button type="button" class="btn btn-danger delete">
                            <i class="glyphicon glyphicon-trash"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <!-- The global file processing state -->
                        <span class="fileupload-process"></span>
                    </div>
                     <?php } else { ?>
                        <div class="col-lg-12 col-md-12">
                            <p><strong>You have documents view only permission.</strong></p>
                         </div>
                    <?php } ?>
                    <!-- The global progress state -->
                    <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
            </form>

        <!-- The blueimp Gallery widget -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

             </div> <!--modal body end -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         
      </div>
    </div>
  </div>
</div>


<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td class="delete-files">
            <?php if ( $allow_edit==1 ) { ?>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}

            <?php } ?>
        </td>
    </tr>
{% } %}
</script>
 

<style type="text/css">
.modal-backdrop.fade.in {
    z-index: 999;
}
</style>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?php echo ACM_PLUGIN_PATH;?>js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo ACM_PLUGIN_PATH;?>Upload-master/js/jquery.fileupload-ui.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
  jQuery("body").on("click",".documents-btn",function(){
    jQuery("table tbody.files").empty();
    var asset_id = jQuery(this).attr("rel");
    var setPath  = "path=general_documentation/"+asset_id; 
    'use strict';
    var createPath ='<?php echo ACM_PLUGIN_PATH  ;?>Upload-master/server/php/?'+setPath;
   
    // Initialize the jQuery File Upload widget:
    jQuery('#fileupload').fileupload({
        url: createPath,
         acceptFileTypes: /(\.|\/)(gif|jpe?g|png|xlsx|docx|csv|xls|pdf|txt|plain|rtf|doc)$/i
    });

    // Enable iframe cross-domain access via redirect option:
    jQuery('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Load existing files:
   jQuery('#fileupload').addClass('fileupload-processing');
    jQuery.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: jQuery('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: jQuery('#fileupload')[0],
         acceptFileTypes: /(\.|\/)(gif|jpe?g|png|xlsx|docx|csv|xls|pdf|txt|plain|rtf|doc)$/i
    }).always(function () {
        jQuery(this).removeClass('fileupload-processing');
    }).done(function (result) {
        jQuery(this).fileupload('option', 'done').call(this, jQuery.Event('done'), {result: result});
    });

    jQuery("#exampleModal").modal('show');
  });

});

</script>
