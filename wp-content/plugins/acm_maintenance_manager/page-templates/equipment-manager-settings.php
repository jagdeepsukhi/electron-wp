<?php
/*
Template Name:  Equipment Manager Settings
*/
get_header(); 

if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}

$user = wp_get_current_user();
if(!empty($user)){
  $asset_id=$_GET['asset_id'];
  global $wpdb;
  $equipmentDetail=$wpdb->get_row("SELECT en_mb_equipment_types.id , en_mb_equipment_types.equipment_type FROM en_mb_equipment_types JOIN en_mb_facility_equipment ON en_mb_facility_equipment.equipment_type=en_mb_equipment_types.id WHERE en_mb_facility_equipment.asset_id='".$asset_id."'", OBJECT );

  /* ************************Set variables************************ */

  $capability_page_view = trim("enet_equipment-manager-settings_view");
  $capability_page_edit = trim("enet_equipment-manager-settings_edit");
 
  $allow_view         = 0;
  $allow_edit         = 0;

 

    
  /* *******************Get Capebilitiies for this Page***************** */
  $userData = get_userdata( get_current_user_id() );
  if ( is_object( $userData) ) {
      $current_user_caps = $userData->allcaps; 
     	
    	 $current_user_roles = $userData->roles; 

		$current_user_caps_custom = array();
		foreach ($current_user_roles as $kkey => $role) {
		    $roleData=get_role($role);
		    $capabilities = $roleData->capabilities;

		    
		    foreach ($capabilities as $ckey => $capValue) {
		        //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

		        if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
		                continue;
		        }else{

		                $current_user_caps_custom[$ckey]=$capValue;
		        }
		    }


		}
		   
		foreach ($current_user_caps_custom as $capability => $status) { 
      //foreach ($current_user_caps as $capability => $status) {  
        if($capability!==0){
          
          if($status==1 && (trim($capability)==$capability_page_view) ){  
            $allow_view =1;   
          }
          if($status==1 && $capability==$capability_page_edit){  
            $allow_edit =1;  
          }

          
        }
      }
  }
  
 
}else {
    wp_die( "You are not logged in.");
}



if($allow_view==0 && $allow_edit==0){
	wp_die( "You do not have permission to view this page.");
}

?>
<div id="main-content">
<?php if ( ! $is_page_builder_used ) : ?>
	<div class="container en-mb-container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>

				<?php endif; ?>

				<div class="entry-content">
						<?php if($allow_edit==1){ ?>
						<div class="row">
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_10')) ;?>">Equipment Types</a>
							</div>
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_11')) ;?>">Service Types</a>
							</div>
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_12')) ;?>">Equipment Descriptions</a>
							</div>
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_13')) ;?>">Equipment Status</a>
							</div>
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_14')) ;?>">Service Explanation </a>
							</div>
							<div class="col-md-2">
								<a class="heading-butn" href="<?php echo get_permalink(get_option('_en_mb_page_9')) ;?>">Equipment Type Relations</a>
							</div>
						</div>
					<?php } ?>

				<form action="" method="post" id="equipment-manager-settings-form">
					<?php global $wpdb;
					$equipmenttypes=$wpdb->get_results("SELECT id,equipment_type FROM en_mb_equipment_types");
					$i=0;
					foreach ($equipmenttypes as $key => $equipmenttype) { ?>
					<?php $settingData 	=$wpdb->get_row("select * from en_mb_equipment_manager_settings where equipment_type='".$equipmenttype->id."'", OBJECT );
						if(!empty($settingData->emails)) {
							$emailArr = explode(",",$settingData->emails); 	
						}else {
							$emailArr = array();	
						}
						$statusArr = explode(",",$settingData->status);
						$weekdayArr = explode(",",$settingData->weekday);		
					?>
						<section  class="setting-section" id="Email Notifications">	
							<div class="row">
								<h5 class="equipment-title"><?php echo $equipmenttype->equipment_type; ?></h5>
									<div class="col-md-4">
										<div><label class="email">Email to:</label></div>
								<table class="emails-table">
									<?php
									if(!empty($emailArr)){
										$j=0;
										foreach ($emailArr as $key => $email) { if(empty($email)){ continue; }?>
									<tr class="emails">
  										<td><input type="text" class="email-text" name="email[<?php echo $i; ?>][]" value="<?php echo $email;?>"></td>
  										<td>
  											<?php if($j==0){?>
  											<i class="fa fa-plus add-more-emails"></i>
  										<?php }else{?>
  											<i class="fa fa-close remove-emails"></i>
  										<?php } ?>
  										</td>
  									</tr>
											
									<?php
									$j++;
									}

									}else {	
									?>
										<tr class="emails">
  										<td><input type="text" class="email-text" name="email[<?php echo $i; ?>][]" value=""></td>
  										<td>

  											<i class="fa fa-plus add-more-emails"></i>
  										</td>
  									</tr>
								<?php } ?>
  								</table>
  										<input type="hidden" class="email-text" name="equipment_type_id[]" value="<?php echo $equipmenttype->id; ?>">
  										
									</div>

									<?php $relation = $wpdb->get_row("select status from en_mb_equipment_type_relation_settings where equipment_type='".$equipmenttype->id."'", OBJECT ); ?>

									<?php if(!empty($relation->status)) { 
											$Equipmentstatus = $wpdb->get_results("select * from en_mb_facility_equipment_status where id IN (".$relation->status.") order by equipment_status ASC", OBJECT );
										?>
									 
									<div class="col-md-3">
										<div><label class="status">when status is</label></div>
										 <?php 
							
										 foreach ($Equipmentstatus as $skey => $status) {
										 	
										  ?>
											<input type="checkbox" name="status[<?php echo $i; ?>][]" value="<?php echo $status->id ;?>" <?php if(in_array($status->id,$statusArr)){echo "checked";} ?>><?php echo $status->equipment_status ;?><br>

										<?php } ?>
									<?php } ?>

									
									</div>
									<div class="col-md-2">
										
										<div><label class="every">every</label></div>
										
										<input type="radio" id="rad-week-<?php echo $equipmenttype->id; ?>" class="text-every" name="every[<?php echo $i; ?>]" value="Week" <?php if (isset($settingData->every) && $settingData->every=="Week") echo "checked";?>>Week<br>
										<input type="radio" id="rad-month-<?php echo $equipmenttype->id; ?>" class="text-every" name="every[<?php echo $i; ?>]" value="Month" <?php if (isset($settingData->every) && $settingData->every=="Month") echo "checked";?>>Month<br>
										<input type="radio" id="rad-quarter" class="text-every" name="every[<?php echo $i; ?>]" value="Quarter" <?php if (isset($settingData->every) && $settingData->every=="Quarter") echo "checked";?>>Quarter<br>
										<!-- <input type="text" class="text-days" name="days-<?php echo $equipmenttype->id; ?>" value="<?php echo $settingData->days ?>"> -->
										<!-- <label>days</label> -->
									</div>
									<div class="col-md-3">
										
					
										<?php if($settingData->every!= "Week") { $disabled="disabled"; }else{$disabled="";} ?>
										<div><label class="on">on</label></div>
										<div class="days-checkboxes">
											 
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Sunday" <?php if(in_array("Sunday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Sunday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Monday" <?php if(in_array("Monday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Monday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Tuesday" <?php if(in_array("Tuesday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Tuesday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Wednesday" <?php if(in_array("Wednesday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Wednesday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Thursday" <?php if(in_array("Thursday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Thursday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Friday" <?php if(in_array("Friday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Friday<br>
											<input type="checkbox" class="text-weekday" name="weekday[<?php echo $i; ?>][]" value="Saturday" <?php if(in_array("Saturday",$weekdayArr)){echo "checked";} ?> <?php echo $disabled; ?>>Saturday<br>
										</div>
										
									</div>

				
							</div>
						</section>
					<?php $i++; } ?>
						<input type="hidden" name="action" value="equipment_manager_settings">
						<?php if( $allow_edit==1){ ?>
							<button type="submit" class="btn btn-success save ">Save</button>
						<?php } ?>
								
				</form>
				</div>
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if (! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<style type="text/css">
.setting-section {border-bottom: 1px dotted #A9A9A9;}
.equipment-title{font-weight: bold;}
.equipment-title {font-size: 16px;margin-left: 20px;}
/*.email-text{height: 80px;width: 215px;}*/
.heading-butn{display: block; 
	width: 160px; 
	height: 48px; 
	background: #ccc; 
	padding: 5px; 
	text-align: center; 
	color: #000;}
.fa-close {color:red; cursor: pointer;}
.fa-plus {color:green; cursor: pointer;}
.heading-butn:hover{text-decoration: none; color: #000;}
.save{text-align: right;
    margin-top: 10px;
}
.days-checkboxes {
    float: left;
}
input.email-text {
    width: 100%;
}


</style>
 <script>
 	function IsJsonString(str) {
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}
 	jQuery(document).ready(function(){
 		jQuery('#equipment-manager-settings-form').on('submit', function (e) { 
	        e.preventDefault();
	        var url= "<?php echo admin_url('admin-ajax.php');?>";
	       
	        jQuery.ajax({
	          type: 'post',
	          url: url,
	          data: jQuery('#equipment-manager-settings-form').serialize(),
	          beforeSend: function() { 
	      		jQuery(".save").prop('disabled',true);
	    	}, 
	         success: function (res) {
		          	if(IsJsonString(res)){
			          	var response = jQuery.parseJSON(res);
			          	if(response.code ==200){
			          		toastr.success(response.message );	
			         	 	jQuery(".save").prop('disabled',false);
			          	}else{
							toastr.error("Settings couldn't save.");	
			         	 	jQuery(".save").prop('disabled',false);
			          	}
		           }else{
		           		toastr.error("Settings couldn't save.");	
			         	 	jQuery(".save").prop('disabled',false);
		           }
	          	   
	          	}
	        });

      });

     jQuery("body").on("click",".add-more-emails",function(){
     	var name= jQuery(this).parent().parent().children().children().attr("name");
     	var html='<tr class="emails"><td><input type="text" class="email-text" name="'+name+'" value=""></td><td><i class="fa fa-close remove-emails"></i></td></tr>';
     	jQuery(this).parent().parent().parent().append(html);
	});
    jQuery(".emails-table").on("click",".remove-emails",function(){
			jQuery(this).parent().parent().remove();
	});


	jQuery('.text-every').change(function() { 
 			var text_every= jQuery(this).val(); 
 			if(text_every=="Week"){
 				jQuery(this).parent().next().find(".text-weekday").removeAttr("disabled");
 			}else{
 				jQuery(this).parent().next().find(".text-weekday").attr("disabled","disabled");
 			}
 	});

 });

 </script>

<?php

get_footer();
