<?php
/*
Template Name: Equipment Templates
*/

get_header();

if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 
$user = wp_get_current_user();
if(!empty($user)){
  $asset_id=$_GET['asset_id'];
  global $wpdb;
  $equipmentDetail=$wpdb->get_row("SELECT en_mb_equipment_types.id , en_mb_equipment_types.equipment_type FROM en_mb_equipment_types JOIN en_mb_facility_equipment ON en_mb_facility_equipment.equipment_type=en_mb_equipment_types.id WHERE en_mb_facility_equipment.asset_id='".$asset_id."'", OBJECT );

  /* ************************Set variables************************ */

  $capability_page_view = trim("enet_equipment_types-edit");
  $capability_page_edit = trim("enet_equipment_types-view");
 
  $allow_view         = 0;
  $allow_edit         = 0;

 

    
  /* *******************Get Capebilitiies for this Page***************** */
  $userData = get_userdata( get_current_user_id() );
  if ( is_object( $userData) ) {
      $current_user_caps = $userData->allcaps; 
     
      $current_user_roles = $userData->roles; 

      $current_user_caps_custom = array();
      foreach ($current_user_roles as $kkey => $role) {
          $roleData=get_role($role);
          $capabilities = $roleData->capabilities;

          
          foreach ($capabilities as $ckey => $capValue) {
              //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

              if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
                      continue;
              }else{

                      $current_user_caps_custom[$ckey]=$capValue;
              }
          }


      }
         
      foreach ($current_user_caps_custom as $capability => $status) { 
      //foreach ($current_user_caps as $capability => $status) {  
        if($capability!==0){
          
          if($status==1 && (trim($capability)==$capability_page_view) ){  
            $allow_view =1;   
          }
          if($status==1 && $capability==$capability_page_edit){  
            $allow_edit =1;  
          }

          
        }
      }
  }
  
 
}else {
    wp_die( "You are not logged in.");
}



if($allow_view==0 && $allow_edit==0){
	wp_die( "You do not have permission to view this page.");
}


$wpTable_Shortcode = "[wpdatatable id=".get_option('_en_mb_wp_datatable_2')."]";



/* *******************Set Edit capability***************** */
function wpdatatables_allow_edit_table_callback( $edit ) {
    global $allow_edit;
    return $allow_edit;  
}

add_filter( 'wpdatatables_allow_edit_table', 'wpdatatables_allow_edit_table_callback' );

?>
 
<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?> </h1>


				<?php endif; ?>

					<div class="entry-content">
						<?php echo do_shortcode($wpTable_Shortcode); ?>	
						<?php //the_content(); ?>
					</div> <!-- .entry-content -->

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

 
<?php get_footer(); ?>
<script type="text/javascript">

jQuery(document).ready(function(){ 

  jQuery("body").on('click', '.DTTT_button_edit',function(){   
    	jQuery("#table_1_recorded_by").parent().parent().parent().hide();
      jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");    
  });

	jQuery("body").on('click', '.DTTT_button_new',function(){
		jQuery("#table_1_recorded_by").parent().parent().parent().hide();
    jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");  

	});
});
</script>

<style type="text/css">
.modal-backdrop.fade.in {
    z-index: 999;
}
#table_1_filter{
	display: none;
}
a.dt-button.delete_table_entry.DTTT_button.DTTT_button_delete {
    display: none !important;
}
</style>