<?php
/*
Template Name: Facility Equipment 

*/

get_header();

if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() ); 
 
/* ************************Get All Equipments************************ */
$page_slug					= 'facility-equipment';
$equipmentData				= array();
$equipment_type_ids_array	= array();
$all_equipment_types_name	= array();
 
$equipment_sql				= "SELECT equipment_type,id FROM en_mb_equipment_types";
$equipmentArr 				= $wpdb->get_results( $equipment_sql, OBJECT );

foreach ($equipmentArr as $key => $equipment) {
	$equipmentData[str_replace(' ', '-', strtolower($equipment->equipment_type))]=$equipment->id;
	$all_equipment_types_name[]=$equipment->equipment_type;
}


/* *******************Get Capebilitiies for this Page***************** */
$userData = get_userdata( get_current_user_id() );
if ( is_object( $userData) ) {
    $current_user_caps = $userData->allcaps; 
    $current_user_roles = $userData->roles; 

 	$current_user_caps_custom = array();
    foreach ($current_user_roles as $kkey => $role) {
    	$roleData=get_role($role);
    	$capabilities = $roleData->capabilities;
  		

    	$current_user_roles = $userData->roles; 

		$current_user_caps_custom = array();
		foreach ($current_user_roles as $kkey => $role) {
		    $roleData=get_role($role);
		    $capabilities = $roleData->capabilities;

		    
		    foreach ($capabilities as $ckey => $capValue) {
		        //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

		        if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
		                continue;
		        }else{

		                $current_user_caps_custom[$ckey]=$capValue;
		        }
		    }


		}
		   
		foreach ($current_user_caps_custom as $capability => $status) { 
    	//foreach ($capabilities as $ckey => $capValue) {
    		//echo "<br>Cap: ".$ckey. " Status: ".$capValue;

    		if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
    				continue;
    		}else{

    		 		$current_user_caps_custom[$ckey]=$capValue;
    		}
    	}
 
  
    }
    
    foreach ($current_user_caps_custom as $capability => $status) {
    	if($status==1){
    		$capabilityString_Arr= explode("_",$capability);
    		$equipment_type_name=$capabilityString_Arr[1];
    		$is_edit=$capabilityString_Arr[3];
    		if(in_array($page_slug, $capabilityString_Arr)) {
    			$equipment_type_ids_array[]=$equipmentData[$equipment_type_name];
    			if($is_edit=="edit"){
    				 
    				$equipment_type_allowed_edit_ids[]=$equipmentData[$equipment_type_name];
    				$equipment_type_allowed_edit_name[]=str_replace('-', ' ', strtolower($equipment_type_name));
    			}
    			 
    		}
    		
    	}
    }
}
 
 
if(!empty(array_unique($equipment_type_ids_array))){
	$equipment_type_ids_array= array_unique($equipment_type_ids_array);

	$equipment_types_string=implode(",", array_filter($equipment_type_ids_array));
}

if(!empty($_GET['status_filter']) && $_GET['status_filter'] =="1" ){

	$sql_string= "var2 = 'AND status IN (".$_GET['status_filter'].")'";

}else if(!empty($_GET['status_filter']) && $_GET['status_filter'] =="2" ){

	$sql_string= "var2 = 'AND status IN (".$_GET['status_filter'].")'";

} else if(!empty($_GET['status_filter']) && $_GET['status_filter'] =="3" ){

	$sql_string= "var2 = 'AND status IN (".$_GET['status_filter'].")'";

} else if(!empty($_GET['status_filter']) && $_GET['status_filter'] =="4" ){

	$sql_string= "var2 = 'AND status IN (".$_GET['status_filter'].")'";

} else if(!empty($_GET['status_filter']) && $_GET['status_filter'] =="All" ){

	$sql_string='';

} else if(!empty($_GET['status_filter']) ){

	$sql_string= "var2 = 'AND status NOT IN (".$_GET['status_filter'].")'";

}else{
	$sql_string= "var2 = 'AND status NOT IN (9,3,2)'";
}

if(empty($equipment_types_string)){
	wp_die("It seems that you do not have any assigned equipment type!");
}

/* *******************Get Shortcode based on cabablity***************** */
$shortcode_sql	= "SELECT facility_equipment FROM en_mb_equipment_group_templates GROUP BY facility_equipment Limit 1";
$shortcodeArr 	= $wpdb->get_row( $shortcode_sql, OBJECT );
if(!empty($shortcodeArr)){
 	$wpTable_Shortcode = "[wpdatatable id=".$shortcodeArr->facility_equipment." var1='(".$equipment_types_string.")' ".$sql_string."]";
}
 
?>
<style type="text/css">
.modal-backdrop.fade.in {
    z-index: 999;
}
#table_1 tr.odd td{
	background-color: red;
}
#table_1 tr.even td{
	background-color: red;
}
select#status_filter {
    background: #5bc0de;
    padding: 10px 16px;
    color: #fff;
    border: 1px solid #5bc0de;
    z-index: 999;
    font-size: 17px;
}
div#table_1_filter {
    display: none;
}
 
</style>   
<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

			<h1 class="main_title"><?php the_title(); ?></h1>
			
 
				<?php
					$thumb 		= '';

					$width 		= (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height 	= (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext 	= 'et_featured_image';
					$titletext 	= get_the_title();
					$thumbnail 	= get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
						<form method="get" id="status_filter_form">
							<select name="status_filter" id="status_filter">
								<option value="9,3,2" <?php if($_GET['status_filter']=='9,3,2') { echo "selected";} ?>>See Active Equipment</option>
								<option value="All" <?php if($_GET['status_filter']=='All') { echo "selected"; } ?>>See All Equipment</option>
								<option value="1" <?php if($_GET['status_filter']==1) { echo "selected"; } ?>>In Service</option>s	
								<option value="2" <?php if($_GET['status_filter']==2) { echo "selected"; } ?>>Totaled</option>
								<option value="3" <?php if($_GET['status_filter']==3) { echo "selected"; } ?>>Sold</option>
								<option value="4" <?php if($_GET['status_filter']==4) { echo "selected"; } ?>>Out for Repair</option>
							</select>
						</form>

					<?php echo do_shortcode($wpTable_Shortcode); ?>	



					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		jQuery("body").on('click', 'a.action-links',function(){
			var asset_id 	= jQuery(this).parent().children(".action_asset_id").val();	 
		    var href 		= jQuery(this).data("href");    
		    window.location = href+asset_id;    
		});
		jQuery("body").on('mouseover', 'a.action-links',function(){
			var asset_id =jQuery(this).parent().children(".action_asset_id").val();	 
		    var href 	 = jQuery(this).data("href"); 
		    jQuery(this).attr("href",href+asset_id);   
		});
		var equipmentData = <?php echo json_encode($equipmentData); ?>;
		var equipment_type_allowed_edit_ids = <?php echo json_encode($equipment_type_allowed_edit_ids); ?>;
		jQuery("body").on('click', '#table_1 tr',function(event){
				
				if(jQuery(this).find("th").hasClass("wdtheader")){
					return false;
				}

				if(jQuery(this).find("td").hasClass("dataTables_empty")){
					return true;
				}



				var equipment_type = jQuery(this).find(".equipment_type").text();
					equipment_type = equipment_type.toLowerCase();
					equipment_type = equipment_type.split(' ').join('-');
					if ( jQuery.inArray(equipmentData[equipment_type], equipment_type_allowed_edit_ids) == -1 ) {
	                    setTimeout(function() 
						{
						    jQuery('.wpDataTablesPopover.editTools').hide();
						    if(jQuery(event.target).attr('class')!="action-icon"){
						   		 jQuery("#myModal").modal("show");
						    } 
						      
						}, 40);
					} 
		});	
	});

</script>

<script type="text/javascript">
function  equipment_type_filter(all_equipment_types,show_equipment_types ){
    	jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
            var text = jQuery( this ).text();

            if(jQuery.inArray( text, all_equipment_types ) > -1)
            { 
                jQuery( this ).hide();
            } 
        });
       	jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
            var text = jQuery( this ).text();
            text= text.toLowerCase();
            if(jQuery.inArray( text, show_equipment_types ) > -1)
            {
                jQuery( this ).show();
            } 
        }); 
}
jQuery(document).ready(function(){ 
	var show_equipment_types = <?php echo json_encode($equipment_type_allowed_edit_name); ?>;
	var all_equipment_types  = <?php echo json_encode(array_unique($all_equipment_types_name)); ?>;

	

	jQuery("body").on('click', '.DTTT_button_edit',function(){

		equipment_type_filter(all_equipment_types,show_equipment_types);
		 
		jQuery("#table_1_recorded_by").parent().parent().parent().hide();
		jQuery("#table_1_recorded_by").val("<?php echo get_current_user_id();?>");
		var maintenance_status_list=<?php echo json_encode(maintenance_status_list()); ?>; 
		jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
			var text= jQuery( this ).text();
			if(jQuery.inArray( text, maintenance_status_list ) > -1)
			{
			 	jQuery( this ).hide();
			} 
		}); 
	    jQuery(".editDialogInput  div.dropdown-menu ul.dropdown-menu > li ").each(function( index ) {
			 var text= jQuery( this ).text();
 
				if(text=="Totaled" || text=="Out for Repair" || text=="Sold" || text=="" || text=="In Service" ){
			 		jQuery( this ).show();
				}
		}); 

	});
	jQuery("body").on('click', '.DTTT_button_new',function(){
		jQuery("#table_1_recorded_by").parent().parent().parent().hide();
		equipment_type_filter(all_equipment_types,show_equipment_types);
	});

	jQuery("#status_filter").change(function(){
		jQuery("#status_filter_form").submit();
	});

});
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="z-index: 9999;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
        	<h4 class="modal-title">Alert</h4>
      </div>
      <div class="modal-body">
		<div class="alert alert-danger">
		   <p>You do not have permission to edit this record.</p>
		</div>
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 
<?php

get_footer();
