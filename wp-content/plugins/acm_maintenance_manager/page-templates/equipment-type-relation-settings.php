<?php
/*
Template Name:  Equipment Type Relation Settings
*/
get_header(); 


if(!is_user_logged_in()){
  wp_die( "You do not have permission to view this page. Please login!");
}
$user = wp_get_current_user();
if(!empty($user)){
  $asset_id=$_GET['asset_id'];
  global $wpdb;
  $equipmentDetail=$wpdb->get_row("SELECT en_mb_equipment_types.id , en_mb_equipment_types.equipment_type FROM en_mb_equipment_types JOIN en_mb_facility_equipment ON en_mb_facility_equipment.equipment_type=en_mb_equipment_types.id WHERE en_mb_facility_equipment.asset_id='".$asset_id."'", OBJECT );

  /* ************************Set variables************************ */

  $capability_page_view = trim("enet_equipment-manager-settings_view");
  $capability_page_edit = trim("enet_equipment-manager-settings_edit");
 
  $allow_view         = 0;
  $allow_edit         = 0;

 

    
  /* *******************Get Capebilitiies for this Page***************** */
  $userData = get_userdata( get_current_user_id() );
  if ( is_object( $userData) ) {
      $current_user_caps = $userData->allcaps; 
     	$current_user_roles = $userData->roles; 

		$current_user_caps_custom = array();
		foreach ($current_user_roles as $kkey => $role) {
		    $roleData=get_role($role);
		    $capabilities = $roleData->capabilities;

		    
		    foreach ($capabilities as $ckey => $capValue) {
		        //echo "<br>Cap: ".$ckey. " Status: ".$capValue;

		        if (array_key_exists($ckey,$current_user_caps_custom) && $current_user_caps_custom[$ckey]==1) {
		                continue;
		        }else{

		                $current_user_caps_custom[$ckey]=$capValue;
		        }
		    }


		}
	   
	foreach ($current_user_caps_custom as $capability => $status) { 	
     
      //foreach ($current_user_caps as $capability => $status) {  
        if($capability!==0){
          
          if($status==1 && (trim($capability)==$capability_page_view) ){  
            $allow_view =1;   
          }
          if($status==1 && $capability==$capability_page_edit){  
            $allow_edit =1;  
          }

          
        }
      }
  }
  
 
}else {
    wp_die( "You are not logged in.");
}



if($allow_view==0 && $allow_edit==0){
	wp_die( "You do not have permission to view this page.");
}

?>
<div id="main-content">
<?php if ( ! $is_page_builder_used ) : ?>
	<div class="container en-mb-container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="main_title"><?php the_title(); ?></h1>
				<?php endif; ?>
				<div class="entry-content">		 
					<form action="" method="post" id="equipment-manager-settings-form">
						<?php global $wpdb;
						$equipmenttypes  = $wpdb->get_results("SELECT id,equipment_type FROM en_mb_equipment_types");
						$equipmentstatus = $wpdb->get_results("SELECT * FROM en_mb_facility_equipment_status order by equipment_status ASC");
						$explanations = $wpdb->get_results("SELECT * FROM en_mb_service_explanation order by service_explanation ASC");
						$services = $wpdb->get_results("SELECT * FROM en_mb_services_types order by service_type ASC");

						$descriptions = $wpdb->get_results("SELECT * FROM en_mb_facility_description order by facility_description ASC");
						$i=0;
						foreach ($equipmenttypes as $key => $equipmenttype) { ?>
						<?php $settingData 	    = $wpdb->get_row("select * from en_mb_equipment_type_relation_settings where equipment_type='".$equipmenttype->id."'", OBJECT );
							$statusArr 			= explode(",",$settingData->status);
							$action_linksArr 	= explode(",",$settingData->action_links);
							$explanationsArr 	= explode(",",$settingData->explanations);
							$servicesArr 		= explode(",",$settingData->services);
							$descriptionsArr 	= explode(",",$settingData->facility_descriptions);
						?>
							<section  class="setting-section" id="Email Notifications">	
								<div class="row">
									<h4 class="equipment-title"><?php echo $equipmenttype->equipment_type; ?></h4>
									
									<input type="hidden" name="equipment_type_id[]" value="<?php echo $equipmenttype->id; ?>">
										<div class="col-md-3">	
											<h5 class="relation-title">Status</h5>									
											<?php foreach ($equipmentstatus as $tkey => $status) { ?>
												<input type="checkbox" name="status[<?php echo $i; ?>][]" value="<?php echo $status->id; ?>" <?php if(in_array($status->id,$statusArr)){echo "checked";} ?>>&nbsp;<?php echo $status->equipment_status; ?><br>

											<?php $count ++;} ?> 
										</div>

										<div class="col-md-2">	
											<h5 class="relation-title">Explanations</h5>									
											<?php foreach ($explanations as $Ekey => $explanation) { ?>
												<input type="checkbox" name="explanation[<?php echo $i; ?>][]" value="<?php echo $explanation->id; ?>" <?php if(in_array($explanation->id,$explanationsArr)){echo "checked";} ?>>&nbsp;<?php echo $explanation->service_explanation; ?><br>

											<?php $count ++;} ?> 
										</div>

										<div class="col-md-2">	
											<h5 class="relation-title">Service Type</h5>									
											<?php foreach ($services as $Skey => $service) { ?>
												<input type="checkbox" name="service[<?php echo $i; ?>][]" value="<?php echo $service->id; ?>" <?php if(in_array($service->id,$servicesArr)){echo "checked";} ?>>&nbsp;<?php echo $service->service_type; ?><br>

											<?php $count ++;} ?> 
										</div>
										<!-- <div class="col-md-2">	
											<h5 class="relation-title">Description</h5>									
											<?php foreach ($descriptions as $Dkey => $description) { ?>
												<input type="checkbox" name="description[<?php echo $i; ?>][]" value="<?php echo $description->id; ?>" <?php if(in_array($description->id,$descriptionsArr)){echo "checked";} ?>>&nbsp;<?php echo $description->facility_description; ?><br>

											<?php $count ++;} ?> 
										</div> -->


										<div class="col-md-3">
										<h5 class="relation-title">Action Links</h5>										 
												<input type="checkbox" name="action_links[<?php echo $i; ?>][]" value="Fuel Tracker"  <?php if(in_array('Fuel Tracker',$action_linksArr)){echo "checked";} ?>>&nbsp;<?php echo "Fuel Tracker"; ?><br>
												 
												<input type="checkbox" name="action_links[<?php echo $i; ?>][]" value="Maintenance Records" <?php if(in_array('Maintenance Records',$action_linksArr)){echo "checked";} ?>>&nbsp;<?php echo "Maintenance Records"; ?><br>
												 
												<input type="checkbox" name="action_links[<?php echo $i; ?>][]" value="Calibration Records" <?php if(in_array('Calibration Records',$action_linksArr)){echo "checked";} ?>>&nbsp;<?php echo "Calibration Records"; ?><br>
												 
												<input type="checkbox" name="action_links[<?php echo $i; ?>][]" value="Equipment Details" <?php if(in_array('Equipment Details',$action_linksArr)){echo "checked";} ?>>&nbsp;<?php echo "Equipment Details"; ?><br>
					
										</div>											
								</div>
							</section>
						<?php $i++; } ?>
							<input type="hidden" name="action" value="equipment_status_relation">
							<?php if($allow_edit==1){?>
							<button type="submit" class="btn btn-success save ">Save</button>
						<?php } ?>
									
					</form>
				</div>
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<style type="text/css">
.setting-section {
    border-bottom: 1px dotted #A9A9A9;
    padding: 10px 0px;
}
.relation-title{ margin-bottom: 0; font-weight: bold; }
.equipment-title{font-weight: bold;}
.equipment-title {font-size: 16px;
    margin-left: 14px;
    margin-bottom: 0px;}
/*.email-text{height: 80px;width: 215px;}*/
.heading-butn{display: block; 
	width: 160px; 
	height: 48px; 
	background: #ccc; 
	padding: 5px; 
	text-align: center; 
	color: #000;}
.fa-close {color:red; cursor: pointer;}
.fa-plus {color:green; cursor: pointer;}
.heading-butn:hover{text-decoration: none; color: #000;}
.save{text-align: right;
    margin-top: 10px;
}
.days-checkboxes {
    float: left;
}
input.email-text {
    width: 100%;
}
#table_1_filter{display: none;}

</style>
<link href="<?php echo get_stylesheet_directory_uri();?>/css/toastr.css" rel="stylesheet"/>
<script src="<?php echo get_stylesheet_directory_uri();?>/js/toastr.js"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/css/bootstrap.min.css">    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
 	function IsJsonString(str) {
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}
 	jQuery(document).ready(function(){
 		jQuery('#equipment-manager-settings-form').on('submit', function (e) { 
	        e.preventDefault();
	        var url= "<?php echo admin_url('admin-ajax.php');?>";
	       
	        jQuery.ajax({
	          type: 'post',
	          url: url,
	          data: jQuery('#equipment-manager-settings-form').serialize(),
	          beforeSend: function() { 
	      		jQuery(".save").prop('disabled',true);
	    		}, 
	          success: function (res) {
		          	if(IsJsonString(res)){
			          	var response = jQuery.parseJSON(res);
			          	if(response.code ==200){
			          		toastr.success(response.message );	
			         	 	jQuery(".save").prop('disabled',false);
			          	}else{
							toastr.error("Settings couldn't save.");	
			         	 	jQuery(".save").prop('disabled',false);
			          	}
		           }else{
		           		toastr.error("Settings couldn't save.");	
			         	 	jQuery(".save").prop('disabled',false);
		           }
	          	   
	          	}
	        });

      });


 });

 </script>

<?php

get_footer();
