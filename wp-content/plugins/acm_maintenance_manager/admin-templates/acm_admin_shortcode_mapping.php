<?php
/*
 *  
 * Description: A Page Template with a darker design.
 */
?>
<div class = "wrap">
 	<h1 class = "wp-heading-inline">ACM Maintenance Manager Shortcodes Mapping</h1>

	<table class="form-table em-tab-content" id="em-instructions-steps">
 		<tbody>
 			<?php foreach ($response as $value) { ?>
		 		<tr>
		 			<td scope="row" ><?php echo $value; ?></td>	
	            </tr>
       		<?php } ?>

		</tbody>
	</table>
	<div>
		<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm" class="button button-primary button-large"><< Back to Instructions & Steps</a>
		<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=wpdatatables-constructor&source&table_id=<?php echo get_option('_en_mb_wp_datatable_18'); ?>" class="button button-primary button-large" target="_blank">View mapping >></a>
	</div>

 </div>
