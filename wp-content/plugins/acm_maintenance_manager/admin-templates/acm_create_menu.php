<?php
/*
 *  
 * Description: A Page Template with a darker design.
 */
?>
<div class = "wrap">
 	<h1 class = "wp-heading-inline">ACM Maintenance Manager Configrations</h1>

	<table class="form-table em-tab-content" id="em-instructions-steps">
 		<tbody>
 			<?php foreach ($response as $value) { ?>
		 		<tr>
		 			<td scope="row" ><?php echo $value; ?></td>	
	            </tr>
       		<?php } ?>

		</tbody>
	</table>
	<div><a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm" class="button button-primary button-large"><< Back to Instructions & Steps</a></div>
 	
 	 
 </div>
