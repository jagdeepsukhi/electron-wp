<?php
/*
 *  
 * Description: A Page Template with a darker design.
 */

global $wpdb;

//$sql ="SELECT * FROM `wp_wpdatatables` ORDER BY `id` ASC";
//$sql ="SELECT * FROM `wp_wpdatatables_columns` ORDER BY `wp_wpdatatables_columns`.`table_id` ASC";

//$result = $wpdb->get_results($sql);

//echo json_encode($result);
 

?>

 
<div class = "wrap">
 	<h1 class = "wp-heading-inline">ACM Maintenance Manager Configrations</h1>
 	<?php echo @$mess; ?>

 	<h2 class="nav-tab-wrapper wp-clearfix em-tabs">
	<a href="" class="nav-tab en-mb-tabs nav-tab-active" data-id="em-instructions-steps">Instructions & Steps</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-page-config">Page Configrations</a>
	<a href="" class="nav-tab en-mb-tabs" data-id="em-datatables-config">WP Datatables ID</a>
	 
	</h2>


	<table class="form-table em-tab-content" id="em-instructions-steps">
 		<tbody>
	 		
            <tr>
	 			<th scope="row" style="width:50px; ">Step 1</th>	
	 			<th scope="row">Import all source tables</th>
	 			<?php if(is_en_mb_tables_exist('en_mb_calibrations')==true && is_en_mb_tables_exist('en_mb_tracking_active_items')==true) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=import_source_tables" onclick="en_mb_show_loader()">Import Now >></a>
	 					OR 
	 					<a href="<?php bloginfo('url'); ?>/wp-content/plugins/acm_maintenance_manager/json/source-tables.sql" download>Download & Import Manually</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

            <tr>
	 			<th scope="row" style="width:50px; ">Step 2</th>	
	 			<th scope="row">Import wpdatatables</th>
	 			<?php if(get_option('_en_mb_import_wp_data_tables')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=wpdatatable_import" onclick="en_mb_show_loader()">Import Now >></a>
	 				</th>
	 			<?php } ?>

            </tr>
 
             <tr>
	 			<th scope="row" style="width:50px; ">Step 3</th>	
	 			<th scope="row">Create Pages Dynamically</th>
	 			<?php if(get_option('_en_mb_create_page')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=create_pages" onclick="en_mb_show_loader()">Create Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>

             <tr>
	 			<th scope="row" style="width:50px; ">Step 4</th>	
	 			<th scope="row">Pages Configurations</th>
	 			<?php if(get_option('_en_mb_page_configrations')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php
	 			 } 
	 			else 
	 				{ ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="" class="en-mb-tabs en-mb-page-config" data-id="em-page-config" >Configure Now</a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>
            <tr>
	 			<th scope="row" style="width:50px; ">Step 5</th>	
	 			<th scope="row">Refresh wpdatatables columns Configrations</th>
	 			<?php if(get_option('_en_mb_refresh_wp_data_tables_columns')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=refresh_wpdatatables" onclick="en_mb_show_loader()">Refresh Now >></a>
	 				</th>
	 			<?php } ?>

            </tr>
            <tr>
	 			<th scope="row" style="width:50px; ">Step 6</th>	
	 			<th scope="row">Data tables shortcode mapping</th>
	 			<?php if(get_option('_en_mb_shortcode_mapping')==1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=shortcode_mapping" onclick="en_mb_show_loader()">Map Now >></a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>
            <tr>
	 			<th scope="row" style="width:50px; ">Step 7</th>	
	 			<th scope="row">Create Frontend menu items</th>
	 			<?php if( get_option('_en_mb_menu_activate') =="active" ) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons dashicons-no" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=create_menu" onclick="en_mb_show_loader()">Create Now >></a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>
            <tr>
	 			<th scope="row" style="width:50px; ">Step 8</th>	
	 			<th scope="row">Refresh Capabilities & Relation <br><small><i style="font-weight: normal;"> It will reset all previous capabilities related to ACM Manager Application</i></small></th>
	 			<?php if(1!=1) { ?>
	 				<th><span class="dashicons dashicons-yes" style="color: green;"></span> </th>
	 			<?php } else { ?>
	 				<th>
	 					<span class="dashicons  dashicons-update" style="color: red;"></span>
	 					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=create_caps" onclick="en_mb_show_loader()">Refresh Now >></a>
	 					 
	 				</th>
	 			<?php } ?>

            </tr>
           
          <!--   <tr>
	 			<th scope="row" style="width:50px; ">Step 7</th>	
	 			<th scope="row">Import AAM configrations manually.<br><i> <small> Go to AAM >> SETTINGS >> Tools >> Import AAM Settings</small></i> </th>

	 			<th>
	 				<span class="dashicons dashicons-smiley"></span>

					 &nbsp;<a href="<?php bloginfo('url'); ?>/wp-content/plugins/acm_maintenance_manager/json/aam-export.json" download>Download file</a>
	 			</th>

            </tr> -->

             <tr> 

	 			<th colspan="3">
	 				<hr>
	 				<p >Remove All Configurations:<i style="font-weight: normal;"> Type DELETE in below text box and click on "Delete" button to remove all configurations & source data related to ACM Maintenance Manager application (Including wpdatatables, Source tables, Pages, Menu Items etc).</i> </p><br> 
	 				
	 				&nbsp;<input type="text" name="" id="delete-conf" placeholder="Type DELETE here">
					&nbsp;<span class="dashicons dashicons-trash" style="color:red;"></span>
					<a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=acm&action=remove_config" onclick="return en_mb_delete_confirm()" style="color:red;">Delete</a>
					
	 			</th>

            </tr>

		</tbody>
	</table>

 	<form name="form-pages" method="post">	 
 		<table class="form-table em-tab-content" id="em-page-config" style="display: none;">
	 		<tbody>
	 			<tr>
			 		 
			 		<th colspan="2">
			 			<i> Note: All page will auto created on step 3. You just need to assign Fuel Log page from here. In case of duplicate selection it will overwrite the previous template & setting.</i> 
		            </th>
	            </tr>

	            <?php foreach (EN_MB_ALL_PAGES as $option_key => $page) { ?>
 				<tr id="<?php echo $option_key; ?>">
			 		<th scope="row" style="width: 300px;"><?php echo $page; ?></th>
			 		<td class="en-mb-select">
			 			<?php echo en_mb_get_pages_drop_down($option_key); ?>	
		            </td>
	            </tr>
 				<?php } ?>

	             <tr>
	            	<td><input name="submit_pages" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
	            </tr>

			</tbody>
		</table>
 	</form>	


 	<form name="form-tables" method="post">	  
 		<table class="form-table em-tab-content" id="em-datatables-config" style="display: none;">
 		<tbody>
 				<tr>
			 		<th colspan="2">
			 			<i> Once you done with import wpdatatables (Step 3), all the following table ids will automatically populate. You do not need to fill these manually. </i> 
		            </th>
	            </tr>
 			<?php foreach (EN_MB_ALL_TABLES as $slug => $tableName) { ?>
 				<tr>
			 		<th scope="row" style="width: 300px;"><?php echo $tableName; ?></th>
			 		<td>
			 			<input name="<?php echo $slug; ?>" type="number" value="<?php echo get_option($slug);?>" style="width: 70px;" required>
		            </td>
	            </tr>
 			<?php } ?>
	 		
            <tr>
            	<td><input name="submit_tables" class="button button-primary button-large" type="submit" value="Save" onclick="en_mb_show_loader()"></td>
            </tr>
        </tbody>
        </table>
        <?php echo wp_nonce_field();?>
        <hr>  
 	
 	<div id="em-datatables-shortcodes" class="em-tab-content" style="display: none;">

 		
 	</div>
 </div>

<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery("body").on("click",".en-mb-tabs",function(e){
			e.preventDefault();
			jQuery(".em-tabs .nav-tab").removeClass("nav-tab-active");
			jQuery(this).addClass("nav-tab-active");
			jQuery(".em-tab-content").hide();
			jQuery("#"+jQuery(this).data("id")).show();
			
		});

		jQuery("body").on("click",".en-mb-page-config",function(e){
			e.preventDefault();
			jQuery('html,body').animate({
		        scrollTop: jQuery("#_en_mb_page_15").offset().top},
		        'slow');

			    jQuery("#_en_mb_page_15 select").css("border","1px solid red");
			
		});


		jQuery('.en-mb-select select').on('change', function() {
		  jQuery('option').prop('disabled', false);
		  jQuery('.en-mb-select select').each(function() {
		    var val = this.value;
		    if(val==''){
		    	return;
		    }
		    jQuery('.en-mb-select select').not(this).find('option').filter(function() {
		      return this.value === val;
		    }).prop('disabled', true);
		  });
		}).change();


	});


function en_mb_delete_confirm() {

	var delete_conf=jQuery("#delete-conf").val();	
	if(delete_conf!="DELETE"){
			jQuery("#delete-conf").css("border","1px solid red");
			return false;
	}

	if(confirm("It will remove all configurations & source data related to ACM Maintenance Manage application (Including wpdatatables, Source tables, Pages, Menu Items etc). Are you sure to delete plugin configurations?")){
		en_mb_show_loader();
		return true;
	}else{
		return  false; 
	}
   	
   		 
}
function en_mb_show_loader() {

	 jQuery(".mb-loader").show(); 
   		 
}
</script>
<div class="mb-loader" style="display: none;"><img src="<?php echo ACM_PLUGIN_PATH; ?>img/material-loader.gif"></div>
<style type="text/css">
.mb-loader {
    position: fixed;
    top: 0;
    left: 0;
    background: rgba(255,255,255, 0.6);
    width: 100%;
    padding: 15% 50%;
    height: 100%;
}
.mb-loader img {
    width: 100px;
}
</style>