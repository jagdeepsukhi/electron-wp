
<?php
  $logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
    ? $user_logo
    : $template_directory_uri . '/images/logo.png';
?>
<div style="text-align: center;">
<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo"  style="width: 150px"/>
</div>
<h4>The following equipment is in need of calibration or is activily being serviced:</h4>
<table style="font-family: arial, sans-serif; border-collapse: collapse; width: 100%;">
  <tr>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Asset ID</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Type</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Make</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Model</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Year</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Due Date</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Description</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Status</th>
    <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;" >Maintenance Instructions</th>
  </tr>
  <?php foreach ($data as $key => $value) { ?>
     <?php if (strpos($value['equipment_status'], 'Due') !== false) { $color="red"; } else { $color=""; } ?>
    <tr style="background: <?php echo $color; ?>">
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['asset_id'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['equipment_type'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['make'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['model'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['year'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['due_date'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['description'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['equipment_status'];?></td>
      <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;" ><?php echo $value['instruction'];?></td>
    </tr>
<?php } ?>
</table>
<h4>Click here to update the status of any of these pieces of equipment.</h4>
<a href="<?php echo get_permalink(get_option('_en_mb_page_6')) ;?>"><?php echo get_permalink(get_option('_en_mb_page_6')) ; ?></a>