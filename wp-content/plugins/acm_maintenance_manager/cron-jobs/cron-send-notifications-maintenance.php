<?php 
/*
*	Cron job for maintenance due if citeria type id Date based.
*	This cron will also check if maintenance plan is reoccurring.
*
*/

require (dirname(dirname(dirname(dirname(dirname(__FILE__)))))."/wp-load.php");

do_action('acm_cron_send_notifications_maintenance');

?>