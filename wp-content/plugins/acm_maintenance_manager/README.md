<h2>ACM Maintenance Manager</h2>

**NOTICE: This version of ACM Maintenance Manager only compatible with wpdatatable version 2.1**

**REQUIRED STEPS FOR INSTALLATION**

Make sure follow the steps in sequence as they defined.

STEP 1:
	Import all source tables by click the button. In this step system will import all source tables with basic data required to start application.

STEP 2:
	Import wpdatatables by click the button. This step import all the wp tables into exisitng DB table of wpdatatable Plugin. It will not effect on existing tables in the database. All new created ids will save in wp_options table with thier associated keys.

	Keys are mentioned in this list are associated with given WP tables: These keys containn the wp data tables ids in wp_options tables.

	_en_mb_wp_datatable_1	= "Facility Equipment Tracker";
	_en_mb_wp_datatable_2	= "Equipment Types Master Table"; 
	_en_mb_wp_datatable_3	= "Facility Equipment Master Table"; 
	_en_mb_wp_datatable_4	= "Services Types Master Table"; 
	_en_mb_wp_datatable_5 	= "WP Users Master Table";
	_en_mb_wp_datatable_6 	= "Maintenance Records Master Table"; 
	_en_mb_wp_datatable_7	= "Maintenance Records Details (Automobile/Fueled Support Equipment)";
	_en_mb_wp_datatable_8	= "Maintenance Records Details ( Inspected/Instrumentation/Non Calibrated Equipment)";
	_en_mb_wp_datatable_9	= "Facility Equipment Status Master Table";
	_en_mb_wp_datatable_10 	= "Facility Equipment Description Master Table";
	_en_mb_wp_datatable_11	= "Service Explanation Master Table";
	_en_mb_wp_datatable_12	= "Calibration Records Details (Instrumentation/Inspacted Equipment)";
	_en_mb_wp_datatable_13	= "Equipment Detail (Automobile)";
	_en_mb_wp_datatable_14	= "Equipment Detail (Fueled Support Equipment)";
	_en_mb_wp_datatable_15	= "Equipment Detail (Non Calibrated Equipment)";
	_en_mb_wp_datatable_16	= "Equipment Detail (Inspected Equipment)";
	_en_mb_wp_datatable_17	= "Equipment Detail (Instrumentation)";
	_en_mb_wp_datatable_18	= "Equipment Group Templates Shortcode Settings";
	_en_mb_wp_datatable_19	= "All WP Data Tables";
	_en_mb_wp_datatable_20	= "Tracking Active Maintenance Items"
	_en_mb_wp_datatable_21 	= "Tracking Active Calibration Items"
	_en_mb_wp_datatable_22	= "Maintenance Equipment History"

STEP 3:	
	Create Pages Dynamically: In this step you just need to click on create now button, system will auto genrate all required pages and assign templates to the pages. Plugin will look for the page with defined names, If not found then it will create the pages and assign the related templates. All pages ids will save into wp_options table with thier keys.

	Keys are mentioned in this list are associated with Pages: These keys containn the Page ids in wp_options tables. 

	_en_mb_page_1 	= "ACM Facility Equipment Tracker
	_en_mb_page_2	= "ACM Facility Equipment Detail
	_en_mb_page_1 	= "ACM Maintenance Records
	_en_mb_page_4	= "ACM Calibration Records
	_en_mb_page_5	= "ACM Active Maintenance Alert
	_en_mb_page_6	= "ACM Active Calibration/Inspection Alert
	_en_mb_page_7	= "ACM Equipment History Audit
	_en_mb_page_8	= "ACM Equipment Manager Settings
	_en_mb_page_9	= "ACM Equipment Type Relation Settings
	_en_mb_page_10 	= "ACM Equipment Types
	_en_mb_page_11	= "ACM Services Types
	_en_mb_page_12 	= "ACM Equipment Descriptions
	_en_mb_page_13 	= "ACM Equipment Status
	_en_mb_page_14 	= "ACM Service Explanation
	_en_mb_page_15 	= "Fuel Log

STEP 4:
	Page Configrations : There is one existing page for FUEL LOG page that Plugin can not trigger so that you need to choose page for FULE LOG from dropdown and save. You do not need to change anything from all other dropdown as you aleady created all reuired pages in step 3 and assigned automatically.

STEP 5:	
	Refresh Wpdatatables columns Configrations: In this step you just need to click on Refresh button. It will update the intenal linking of wpdatables accroding new created ids.


STEP 6:
	Data tables shortcode mapping : Click on the Map now button to refresh the shortcode mapping.In this step, Plugin update the old ids of tables with new created ids of wpdatatables. The table "Equipment Group Templates Shortcode Settings" is use to define the relation between pages and wpdatatables.

STEP 7: 
	Create Frontend menu items: When you first time install this plugin or configure this plugin, in this step pluign will create menu items for frontend. If you deactivate the plugin without deleting CONFIGATIONS, menu will be remove and when you again activate the (if all configartions exists) menu will appear.

STEP 8: 
	Refresh Capabilities & Relation : This step is plugin will create all required roles (4 roles) & create capabilities and assign to roles according to mapping provide by JON.
	If you 2nd time click on this button, it will reset all the caps again according to map. 

****************************************************************************************************************************
****************************************************************************************************************************


**Create New Equipment Type**
* Everthing is depends upon "Equipment Type", So whenever you go to add/insert new equipment make sure it should be Unique.
* Do not delete any euipment type if not necessary.
* Whenever you will add new Equipment type, create capabilites with defined structure for each page.
* After creating a Equipment Type, first assing the status, descriptions etc from "Equipment Types Relation" setting tab.
* Add capabilities for all pages in this format

Eg:  enet_inspected-equipment_facility-equipment_edit ---> enet_[Equipment Type]_[PageName]_[view/edit]

**************************************************************************************************************************

**CRON JOBS**

There total 6 cron jobs to perform individual task defined below:

1) **Cron Maintenance Planner CT Date Based**: This cron event works for maintenance planner, It triggers all the pending Maintenance item based upon schudled Date.

2) **Cron Maintenance Planner CT Usage Based**: This cron event works for maintenance planner, It triggers all the pending Maintenance item based upon usage item eg: Break check on every 100 Miles.

3) **Cron Maintenance Planner CT Distance Based**: This cron event works for maintenance planner, It triggers all the pending Maintenance item based upon overall distance item.

4) **Cron Calibration Planner**: This cron event works for Calibration planner, It triggers all the pending Calibration item based upon overall distance item.

5) **Cron Send Notification Calibration Planner**: This cron event works for Calibration planner, It send email to all person those are assinged from Maintenance Manager Settings for this notification.

6) **Cron Send Notification Maintenance Planner**: This cron event works for Maintenance planner, It send email to all person those are assinged from Maintenance Manager Settings for this notification.


**************************************************************************************************************************

**CAPABILITIES LOGIC**

Capabilities logic implemented just before load the page. Scripts are written in top of temaplate file of each page. You can find the template files under template directory.


**************************************************************************************************************************

**How installation logic works**

*We have placed the json/sql files for basic data under Json directory.
*When you follow the steps from setting in admin panel. Each file read one by one and import in database.


**************************************************************************************************************************






