-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 17, 2018 at 12:33 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.2.9-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electron`
--

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_calibrations`
--

CREATE TABLE `en_mb_calibrations` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `calibration_date` varchar(255) NOT NULL,
  `calibration_provider` varchar(255) NOT NULL,
  `calibration_technician` varchar(255) NOT NULL,
  `calibration_status` varchar(11) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `calibration_documentations` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_calibrations`
--

INSERT INTO `en_mb_calibrations` (`id`, `asset_id`, `calibration_date`, `calibration_provider`, `calibration_technician`, `calibration_status`, `cost`, `calibration_documentations`, `remarks`, `recorded_by`, `date_created`) VALUES
(1, 'F4E3297', '01 May 2018', 'Cal  Company gfhfgh', 'Bob', 'Pass', '1', '<a class=\"documents-btn\" rel=\"1\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'Text Here', 1, '2018-06-27 09:58:02'),
(12, 'F4E3290', '08 May 2018', 'XYZ', 'Bob', 'Fail', '23.23', '<a class=\"documents-btn\" rel=\"12\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'Test', 1, '2018-07-09 05:30:25'),
(13, 'F4E3212', '12 Jun 2018', 'asd', 'asd', 'Pass', '2.34', '<a class=\"documents-btn\" rel=\"13\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'asd', 1, '2018-08-16 05:29:34'),
(36, 'F4E3297', '22 Aug 2018', 'DAS', 'daasdas', 'Pass', '23.44', '<a class=\'documents-btn\' title=\'Documents\' rel=\'36\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', 'adasdasdasdasd', 149, '2018-08-22 06:43:29'),
(37, 'F4E3297', '23 Aug 2018', 'TTTT', 'TTT', 'Pass', '4234.23', '<a class=\'documents-btn\' title=\'Documents\' rel=\'37\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', '224234234234werwerwerw', 149, '2018-08-22 06:48:01'),
(38, 'F4E3297', '30 Aug 2018', 'rrrrr', 'rrrrrrrrrr', 'Pass', '324.23', '<a class=\'documents-btn\' title=\'Documents\' rel=\'38\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', 'rrrrrrrrr', 149, '2018-08-22 06:49:29');

--
-- Triggers `en_mb_calibrations`
--
DELIMITER $$
CREATE TRIGGER `Calibration History Audit On Delete` BEFORE DELETE ON `en_mb_calibrations` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'calibration_date',OLD.calibration_date,'','delete'), (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'calibration_provider',OLD.calibration_provider,'','delete'), (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'calibration_technician',OLD.calibration_technician,'','delete'), (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'calibration_status',OLD.calibration_status,'','delete'), (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'cost',OLD.cost,'','delete'), (OLD.asset_id, CONCAT('C',OLD.id), OLD.recorded_by,'remarks',OLD.remarks,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Calibration History Audit On Insert` AFTER INSERT ON `en_mb_calibrations` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_date','',NEW.calibration_date,'add'), (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_provider','',NEW.calibration_provider,'add'), (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_technician','',NEW.calibration_technician,'add'), (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_status','',NEW.calibration_status,'add'), (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'cost','',NEW.cost,'add'), (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'remarks','',NEW.remarks,'add'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Calibration History Audit on Update` AFTER UPDATE ON `en_mb_calibrations` FOR EACH ROW BEGIN IF (OLD.calibration_date <> NEW.calibration_date) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_date',OLD.calibration_date,NEW.calibration_date,'edit'); END IF; IF (OLD.calibration_provider <> NEW.calibration_provider) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_provider',OLD.calibration_provider,NEW.calibration_provider,'edit'); END IF; IF OLD.calibration_technician <> NEW.calibration_technician THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_technician',OLD.calibration_technician,NEW.calibration_technician,'edit'); END IF; IF OLD.calibration_status <> NEW.calibration_status THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'calibration_status',OLD.calibration_status,NEW.calibration_status,'edit'); END IF; IF OLD.cost <> NEW.cost THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'cost',OLD.cost,NEW.cost,'edit'); END IF; IF OLD.remarks <> NEW.remarks THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('C',NEW.id), NEW.recorded_by,'remarks',OLD.remarks,NEW.remarks,'edit'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Documents Links Calibration` BEFORE INSERT ON `en_mb_calibrations` FOR EACH ROW SET NEW.calibration_documentations = CONCAT("<a class='documents-btn' title='Documents' rel='",NEW.id,"' href='#'><img src='http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png'></a>")
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_calibrations_plans`
--

CREATE TABLE `en_mb_calibrations_plans` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `scheduled_calibration` varchar(100) NOT NULL,
  `next_calibrate` int(11) NOT NULL,
  `reoccurring` int(11) NOT NULL,
  `period` varchar(255) NOT NULL,
  `calibration_instructions` longtext NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_calibrations_plans`
--

INSERT INTO `en_mb_calibrations_plans` (`id`, `asset_id`, `scheduled_calibration`, `next_calibrate`, `reoccurring`, `period`, `calibration_instructions`, `recorded_by`, `created_date`, `status`) VALUES
(1, 'F4E3297', '2018-07-19', 0, 1, 'Semi­-Annually', 'You can type something here if there is a note you need to be part of the\r\nnotification. For example if we need to provide the calibration house with any sdf\r\nsupport equipment in order to perform thier .', 149, '2018-08-22 06:40:49', 1),
(2, 'F4E3290', '2018-09-06', 0, 1, 'Monthly', 'werwerwerwr', 1, '2018-08-06 06:04:43', 1),
(3, 'F4E3212', '2018-06-28', 0, 1, 'Monthly', 'zadasdas', 1, '2018-06-18 07:38:37', 1);

--
-- Triggers `en_mb_calibrations_plans`
--
DELIMITER $$
CREATE TRIGGER `Calibration Plan History Audit On Delete` AFTER DELETE ON `en_mb_calibrations_plans` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('CP',OLD.id), OLD.recorded_by,'scheduled_calibration',OLD.scheduled_calibration,'','delete'), (OLD.asset_id, CONCAT('CP',OLD.id), OLD.recorded_by,'next_calibrate',OLD.next_calibrate,'','delete'), (OLD.asset_id, CONCAT('CP',OLD.id), OLD.recorded_by,'reoccurring',OLD.reoccurring,'','delete'), (OLD.asset_id, CONCAT('CP',OLD.id), OLD.recorded_by,'period',OLD.period,'','delete'), (OLD.asset_id, CONCAT('CP',OLD.id), OLD.recorded_by,'calibration_instructions',OLD.calibration_instructions,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Calibration Plan History Audit On Insert` AFTER INSERT ON `en_mb_calibrations_plans` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'scheduled_calibration','',NEW.scheduled_calibration,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'next_calibrate','',NEW.next_calibrate,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'reoccurring','',NEW.reoccurring,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'period','',NEW.period,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'calibration_instructions','',NEW.calibration_instructions,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Calibration Plan History Audit on Update` AFTER UPDATE ON `en_mb_calibrations_plans` FOR EACH ROW BEGIN IF (OLD.scheduled_calibration <> NEW.scheduled_calibration) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'scheduled_calibration',OLD.scheduled_calibration,NEW.scheduled_calibration,'edit'); END IF; IF (OLD.next_calibrate <> NEW.next_calibrate) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'next_calibrate',OLD.next_calibrate,NEW.next_calibrate,'edit'); END IF; IF OLD.reoccurring <> NEW.reoccurring THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'reoccurring',OLD.reoccurring,NEW.reoccurring,'edit'); END IF; IF OLD.period <> NEW.period THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'period',OLD.period,NEW.period,'edit'); END IF; IF OLD.calibration_instructions <> NEW.calibration_instructions THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'calibration_instructions',OLD.calibration_instructions,NEW.calibration_instructions,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_equipments_explanation_relation`
--

CREATE TABLE `en_mb_equipments_explanation_relation` (
  `id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `explanation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_equipments_explanation_relation`
--

INSERT INTO `en_mb_equipments_explanation_relation` (`id`, `equipment_id`, `explanation_id`) VALUES
(1, 1, 1),
(2, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_equipment_group_templates`
--

CREATE TABLE `en_mb_equipment_group_templates` (
  `id` int(11) NOT NULL,
  `group_template` varchar(255) NOT NULL,
  `facility_equipment` varchar(255) NOT NULL,
  `detail_view` varchar(255) NOT NULL,
  `fuel_tracker` varchar(255) NOT NULL,
  `maintenance_records` varchar(255) NOT NULL,
  `calibration_inspection_records` varchar(255) NOT NULL,
  `active_maintenance_items` varchar(255) NOT NULL,
  `active_calibration_inspection_items` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_equipment_group_templates`
--

INSERT INTO `en_mb_equipment_group_templates` (`id`, `group_template`, `facility_equipment`, `detail_view`, `fuel_tracker`, `maintenance_records`, `calibration_inspection_records`, `active_maintenance_items`, `active_calibration_inspection_items`, `recorded_by`) VALUES
(1, '1', '177', '189', '', '183', '', '196', '', 149),
(2, '2', '177', '190', '', '183', '', '196', '', 1),
(3, '6', '177', '192', '', '184', '188', '196', '197', 1),
(4, '3', '177', '193', '', '184', '188', '196', '197', 1),
(5, '4', '177', '191', '', '184', '', '196', '', 1);

--
-- Triggers `en_mb_equipment_group_templates`
--
DELIMITER $$
CREATE TRIGGER `Management Equipment Groups History Audit Delete` BEFORE DELETE ON `en_mb_equipment_group_templates` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',OLD.id), OLD.recorded_by,'group_template',OLD.group_template,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'facility_equipment',OLD.facility_equipment,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'detail_view',OLD.detail_view,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'fuel_tracker',OLD.fuel_tracker,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'maintenance_records',OLD.maintenance_records,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'calibration_inspection_records',OLD.calibration_inspection_records,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'active_maintenance_items',OLD.active_maintenance_items,'','delete'), ('', CONCAT('G',OLD.id), OLD.recorded_by,'active_calibration_inspection_items',OLD.active_calibration_inspection_items,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Management Equipment Groups History Audit Insert` AFTER INSERT ON `en_mb_equipment_group_templates` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'group_template','',NEW.group_template,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'facility_equipment','',NEW.facility_equipment,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'detail_view','',NEW.detail_view,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'fuel_tracker','',NEW.fuel_tracker,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'maintenance_records','',NEW.maintenance_records,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'calibration_inspection_records','',NEW.calibration_inspection_records,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'active_maintenance_items','',NEW.active_maintenance_items,'add'), ('', CONCAT('G',NEW.id), NEW.recorded_by,'active_calibration_inspection_items','',NEW.active_calibration_inspection_items,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Management Equipment Groups History Audit Update` AFTER UPDATE ON `en_mb_equipment_group_templates` FOR EACH ROW BEGIN IF (OLD.group_template <> NEW.group_template) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'group_template',OLD.group_template,NEW.group_template,'edit'); END IF; IF (OLD.facility_equipment <> NEW.facility_equipment) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'facility_equipment',OLD.facility_equipment,NEW.facility_equipment,'edit'); END IF; IF OLD.detail_view <> NEW.detail_view THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'detail_view',OLD.detail_view,NEW.detail_view,'edit'); END IF; IF OLD.fuel_tracker <> NEW.fuel_tracker THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'fuel_tracker',OLD.fuel_tracker,NEW.fuel_tracker,'edit'); END IF; IF OLD.maintenance_records <> NEW.maintenance_records THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'maintenance_records',OLD.maintenance_records,NEW.maintenance_records,'edit'); END IF; IF OLD.calibration_inspection_records <> NEW.calibration_inspection_records THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'calibration_inspection_records',OLD.calibration_inspection_records,NEW.calibration_inspection_records,'edit'); END IF; IF OLD.active_maintenance_items <> NEW.active_maintenance_items THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'active_maintenance_items',OLD.active_maintenance_items,NEW.active_maintenance_items,'edit'); END IF; IF OLD.active_calibration_inspection_items <> NEW.active_calibration_inspection_items THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('G',NEW.id), NEW.recorded_by,'active_calibration_inspection_items',OLD.active_calibration_inspection_items,NEW.active_calibration_inspection_items,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_equipment_manager_settings`
--

CREATE TABLE `en_mb_equipment_manager_settings` (
  `id` int(11) NOT NULL,
  `equipment_type` varchar(255) NOT NULL,
  `emails` longtext NOT NULL,
  `status` varchar(255) NOT NULL,
  `every` varchar(255) NOT NULL,
  `weekday` varchar(255) NOT NULL,
  `recorded_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_equipment_manager_settings`
--

INSERT INTO `en_mb_equipment_manager_settings` (`id`, `equipment_type`, `emails`, `status`, `every`, `weekday`, `recorded_by`) VALUES
(22, '1', 'www.webexpert@gmail.com', '7,4,9', 'Week', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1'),
(23, '2', 'www.webexpert@gmail.com', '7,4,9', 'Week', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1'),
(24, '3', 'www.webexpert@gmail.com', '5,7,6,4,9', 'Week', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1'),
(25, '4', 'www.webexpert@gmail.com', '7,4,9', 'Week', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1'),
(26, '6', 'www.webexpert@gmail.com', '5,6,4,9', 'Week', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1'),
(27, '7', '', '11,1', '', '', '1');

--
-- Triggers `en_mb_equipment_manager_settings`
--
DELIMITER $$
CREATE TRIGGER `Equipment Manager History Audit On Delete` AFTER DELETE ON `en_mb_equipment_manager_settings` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',OLD.id), OLD.recorded_by,'emails',OLD.emails,'','delete'), ('', CONCAT('ES',OLD.id), OLD.recorded_by,'status',OLD.status,'','delete'), ('', CONCAT('ES',OLD.id), OLD.recorded_by,'every',OLD.every,'','delete'), ('', CONCAT('ES',OLD.id), OLD.recorded_by,'weekday',OLD.weekday,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Manager History Audit On Insert` AFTER INSERT ON `en_mb_equipment_manager_settings` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'emails','',NEW.emails,'add'), ('', CONCAT('ES',NEW.id), NEW.recorded_by,'status','',NEW.status,'add'), ('', CONCAT('ES',NEW.id), NEW.recorded_by,'every','',NEW.every,'add'), ('', CONCAT('ES',NEW.id), NEW.recorded_by,'weekday','',NEW.weekday,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Manager Settings History Audit` AFTER UPDATE ON `en_mb_equipment_manager_settings` FOR EACH ROW BEGIN IF (OLD.emails <> NEW.emails) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'emails',OLD.emails,NEW.emails,'edit'); END IF; IF (OLD.status <> NEW.status) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'status',OLD.status,NEW.status,'edit'); END IF; IF OLD.every <> NEW.every THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'every',OLD.every,NEW.every,'edit'); END IF; IF OLD.weekday <> NEW.weekday THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'weekday',OLD.weekday,NEW.weekday,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_equipment_types`
--

CREATE TABLE `en_mb_equipment_types` (
  `id` int(11) NOT NULL,
  `equipment_type` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_equipment_types`
--

INSERT INTO `en_mb_equipment_types` (`id`, `equipment_type`, `recorded_by`, `date`) VALUES
(1, 'Automobile', 1, '2018-05-23 08:19:35'),
(2, 'Fueled Support Equipment', 1, '2018-06-08 10:14:39'),
(3, 'Instrumentation', 1, '2018-07-05 15:33:27'),
(4, 'Non Calibrated Equipment', 1, '2018-05-23 08:29:59'),
(6, 'Inspected Equipment', 1, '2018-06-08 09:41:05');

--
-- Triggers `en_mb_equipment_types`
--
DELIMITER $$
CREATE TRIGGER `Equipment Type History Audit on Delete` BEFORE DELETE ON `en_mb_equipment_types` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ET',OLD.id), OLD.recorded_by,'equipment_type',OLD.equipment_type,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Type History Audit on Insert` AFTER INSERT ON `en_mb_equipment_types` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ET',NEW.id), NEW.recorded_by,'equipment_type','',NEW.equipment_type,'add'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Type History Audit on Update` AFTER UPDATE ON `en_mb_equipment_types` FOR EACH ROW BEGIN IF (OLD.equipment_type <> NEW.equipment_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ET',NEW.id), NEW.recorded_by,'equipment_type',OLD.equipment_type,NEW.equipment_type,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_equipment_type_relation_settings`
--

CREATE TABLE `en_mb_equipment_type_relation_settings` (
  `id` int(11) NOT NULL,
  `equipment_type` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `action_links` varchar(255) NOT NULL,
  `explanations` varchar(200) NOT NULL,
  `services` varchar(200) NOT NULL,
  `facility_descriptions` varchar(200) NOT NULL,
  `recorded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_equipment_type_relation_settings`
--

INSERT INTO `en_mb_equipment_type_relation_settings` (`id`, `equipment_type`, `status`, `action_links`, `explanations`, `services`, `facility_descriptions`, `recorded_by`) VALUES
(1, 6, '11,5,1,6,4,9', 'Maintenance Records,Calibration Records,Equipment Details', '1,7', '17,1', '', 1),
(2, 1, '7,8,4,9', 'Fuel Tracker,Maintenance Records,Equipment Details', '11,1,7,12,8', '19,17,1,20', '', 1),
(3, 2, '7,6,8,4,9,12,10', 'Fuel Tracker,Maintenance Records,Equipment Details', '11,1,7,8', '19,17,1,20', '', 1),
(4, 3, '5,7,6,8,4,9,12,10', 'Fuel Tracker,Maintenance Records,Calibration Records,Equipment Details', '1,7', '19,17', '', 1),
(5, 4, '7,8,4,9', 'Fuel Tracker,Maintenance Records,Equipment Details', '1,8', '19,17,1', '', 1),
(6, 7, '', 'Fuel Tracker,Maintenance Records,Calibration Records', '', '19,17', '', 1),
(7, 12, '11,1,6,4,12,3', '', '', '', '', 1);

--
-- Triggers `en_mb_equipment_type_relation_settings`
--
DELIMITER $$
CREATE TRIGGER `Relation Settings History Audit Insert` AFTER INSERT ON `en_mb_equipment_type_relation_settings` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'equipment_type','',NEW.equipment_type,'add'), ('', CONCAT('RS',NEW.id), NEW.recorded_by,'status','',NEW.status,'add'), ('', CONCAT('RS',NEW.id), NEW.recorded_by,'action_links','',NEW.action_links,'add'), ('', CONCAT('RS',NEW.id), NEW.recorded_by,'services','',NEW.services,'add'), ('', CONCAT('RS',NEW.id), NEW.recorded_by,'facility_descriptions','',NEW.facility_descriptions,'add'), ('', CONCAT('RS',NEW.id), NEW.recorded_by,'explanations','',NEW.explanations,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Relation Settings History Audit Update` AFTER UPDATE ON `en_mb_equipment_type_relation_settings` FOR EACH ROW BEGIN IF (OLD.equipment_type <> NEW.equipment_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'equipment_type',OLD.equipment_type,NEW.equipment_type,'edit'); END IF; IF (OLD.status <> NEW.status) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'status',OLD.status,NEW.status,'edit'); END IF; IF (OLD.action_links <> NEW.action_links) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'action_links',OLD.action_links,NEW.action_links,'edit'); END IF; IF (OLD.services <> NEW.services) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'services',OLD.services,NEW.services,'edit'); END IF; IF (OLD.facility_descriptions <> NEW.facility_descriptions) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'facility_descriptions',OLD.facility_descriptions,NEW.facility_descriptions,'edit'); END IF; IF (OLD.explanations <> NEW.explanations) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',NEW.id), NEW.recorded_by,'explanations',OLD.explanations,NEW.explanations,'edit'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Relation Settings History Audit on Delete` AFTER DELETE ON `en_mb_equipment_type_relation_settings` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('RS',OLD.id), OLD.recorded_by,'equipment_type',OLD.equipment_type,'','delete'), ('', CONCAT('RS',OLD.id), OLD.recorded_by,'status',OLD.status,'','delete'), ('', CONCAT('RS',OLD.id), OLD.recorded_by,'action_links','',OLD.action_links,'','delete'), ('', CONCAT('RS',OLD.id), OLD.recorded_by,'services','',OLD.services,'','delete'), ('', CONCAT('RS',OLD.id), OLD.recorded_by,'facility_descriptions','',OLD.facility_descriptions,'','delete'), ('', CONCAT('RS',OLD.id), OLD.recorded_by,'explanations','',OLD.explanations,'','delete')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_facility_description`
--

CREATE TABLE `en_mb_facility_description` (
  `id` int(11) NOT NULL,
  `facility_description` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_facility_description`
--

INSERT INTO `en_mb_facility_description` (`id`, `facility_description`, `recorded_by`, `created_on`) VALUES
(2, 'Data Logger', 1, '2018-08-20 07:26:03'),
(3, 'Drill Press', 0, '2018-05-22 12:48:42'),
(4, 'Fork Lift', 0, '2018-05-22 12:48:54'),
(5, 'Lawn Mower', 0, '2018-05-22 12:49:16'),
(6, 'Safety Hook', 0, '2018-05-22 12:49:53');

--
-- Triggers `en_mb_facility_description`
--
DELIMITER $$
CREATE TRIGGER `Facility Descrption History Audit on Delete` AFTER DELETE ON `en_mb_facility_description` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('FD',OLD.id), OLD.recorded_by,'facility_description',OLD.facility_description,'','delete'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Facility Descrption History Audit on Insert` AFTER INSERT ON `en_mb_facility_description` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('FD',NEW.id), NEW.recorded_by,'facility_description','',NEW.facility_description,'add'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Facility Descrption History Audit on Update` AFTER UPDATE ON `en_mb_facility_description` FOR EACH ROW BEGIN IF (OLD.facility_description <> NEW.facility_description) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('FD',NEW.id), NEW.recorded_by,'facility_description',OLD.facility_description,NEW.facility_description,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_facility_equipment`
--

CREATE TABLE `en_mb_facility_equipment` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `equipment_type` varchar(255) NOT NULL,
  `equipment_description` varchar(255) NOT NULL,
  `make` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `year` year(4) NOT NULL,
  `VIN` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `fuel_type` varchar(255) NOT NULL,
  `maintenance_criteria` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `aquisition_method` varchar(255) NOT NULL,
  `aquisition_odometer` varchar(255) NOT NULL,
  `aquisition_hours` int(11) NOT NULL,
  `aquisition_date` date NOT NULL,
  `aquisition_from` varchar(255) NOT NULL,
  `cost` float NOT NULL,
  `resale_price` float NOT NULL,
  `warranty` varchar(255) NOT NULL,
  `liscence_plate` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `action` longtext NOT NULL,
  `general_documentation` longtext NOT NULL,
  `recorded_by` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_facility_equipment`
--

INSERT INTO `en_mb_facility_equipment` (`id`, `asset_id`, `equipment_type`, `equipment_description`, `make`, `model`, `status`, `year`, `VIN`, `serial_number`, `fuel_type`, `maintenance_criteria`, `color`, `aquisition_method`, `aquisition_odometer`, `aquisition_hours`, `aquisition_date`, `aquisition_from`, `cost`, `resale_price`, `warranty`, `liscence_plate`, `notes`, `action`, `general_documentation`, `recorded_by`) VALUES
(1, 'F4E3295', '1', '4', 'toyota', 'Camery', 'Oil Change,Break Check', 2014, '451BF2FK4EU384043', '451BF2FK4EU384043', 'Diesel', 'Kilometers', 'Black', 'Purchase', '2000', 0, '2018-05-22', 'Bell Honda', 10000, 0, '2021-05-21', 'DFG 5656', 'Text Here note', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3295\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3295\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(2, 'F4E3297', '3', '2', 'toyote', 'Camery', 'Wire Change', 2014, '451BF2FK4EU384043', '451BF2FK4EU384043 ', 'Gas', 'Date', 'Black', 'Purchase', '2000', 0, '2018-05-21', 'Bell Honda ', 10000, 123123, '2021-05-21', '', 'hgfhgfhgfh', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Calibration/Inspection\" class=\"action-links action-calibration-records\" data-href=\"https://localhost/electron/acm-calibration-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon1.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3297\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3297\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(4, 'F4E3290', '6', '2', 'Honda', '2015', '1', 2015, '451BF2FK4EU384043', '465464645645645', '', 'Date', '', 'Lease', '', 0, '2018-07-10', '456', 560.04, 4.56, 'rty', '', '45645', '<a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Calibration/Inspection\" class=\"action-links action-calibration-records\" data-href=\"https://localhost/electron/acm-calibration-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon1.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3290\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3290\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(5, 'F4E3298', '4', '2', 'Honda', '2015', '9', 0000, '451BF2FK4EU384043', '', '', 'Miles', '', 'Purchase', '', 0, '2018-05-09', '234234', 234.23, 2.34, 'werwer', '', 'wrewr', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3298\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3298\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(6, 'F4E3455', '2', '2', 'Honda', '2015', '2', 0000, '451BF2FK4EU384043', '', 'Diesel', 'Kilometers', 'Red', 'Purchase', '345', 0, '2018-05-09', '345', 3.45, 50.04, 'fh', '', '345345', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3455\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3455\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(7, 'F4E3212', '6', '4', 'toyota', 'Camery', '1', 2014, '451BF2FK4EU384043', '451BF2FK4EU384043', 'Diesel', 'Miles', 'Black', 'Purchase', '2000', 0, '2018-05-22', 'Bell Honda', 10000, 0, '2021-05-21', 'DFG 5656', 'Text Here note', '<a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Calibration/Inspection\" class=\"action-links action-calibration-records\" data-href=\"https://localhost/electron/acm-calibration-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon1.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3212\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3212\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(11, 'ADSD2423', '2', '2', 'Suzuki', '234', '1', 0000, '', '', '', '', '', '', '', 0, '0000-00-00', '', 0, 0, '', '', '', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"ADSD2423\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'ADSD2423\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(12, 'SDA23423', '6', '2', 'FD', '3333', '1', 0000, '', '', '', '', '', '', '', 0, '0000-00-00', '', 0, 0, '', '', '', '<a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Calibration/Inspection\" class=\"action-links action-calibration-records\" data-href=\"https://localhost/electron/acm-calibration-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon1.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"SDA23423\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'SDA23423', 1),
(3236, 'F4E3236', '1', '2', 'adasd', '234234', '1', 0000, '', '', '', '', '', '', '', 0, '0000-00-00', '', 0, 0, '', '', '', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3236\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3236\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(3237, 'F4E3237', '1', '2', 'yrty', 'rty', '4', 0000, '', '', '', '', '', '', '', 0, '0000-00-00', '', 0, 0, '', '', '', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\"action_asset_id\" type=\"hidden\" value=\"F4E3237\">', '<a class=\'documents-btn\' title=\'Documents\' rel=\'F4E3237\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1),
(3241, 'ACM3241', '1', '2', 'SA', 'SAS', '1', 0000, '', '', '', '', '', '', '', 0, '0000-00-00', '', 0, 0, '', '', '', '<a title=\"Fuel Tracker\" class=\"action-links action-fuel-tracker\" data-href=\"https://localhost/electron/chain-of-custody/history-of-changes/?asset_id=\"><img class=\"action-icon\"  src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-4.png\"></a><a title=\"Maintenance Records\" class=\"action-links action-maintenance-records\" data-href=\"https://localhost/electron/acm-maintenance-records/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-2.png\"> </a><a title=\"Equipment Details\" class=\"action-links action-equipment-details\" data-href=\"https://localhost/electron/acm-facility-equipment-detail/?asset_id=\"><img class=\"action-icon\" src=\"https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/icon-3.png\"> </a><input class=\'action_asset_id\' type=\'hidden\' value=\'ACM3241\'>', '<a class=\'documents-btn\' title=\'Documents\' rel=\'ACM3241\' href=\'#\'><img src=\'https://localhost/electron/wp-content/plugins/acm_maintenance_manager//icons/folder.png\'></a>', 1);

--
-- Triggers `en_mb_facility_equipment`
--
DELIMITER $$
CREATE TRIGGER `Delete All Records` AFTER DELETE ON `en_mb_facility_equipment` FOR EACH ROW BEGIN DELETE FROM en_mb_calibrations WHERE asset_id = OLD.asset_id; DELETE FROM en_mb_maintenance_records WHERE asset_id = OLD.asset_id; DELETE FROM en_mb_tracking_active_items WHERE asset_id = OLD.asset_id; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Facility Equipment History Audit On Delete` BEFORE DELETE ON `en_mb_facility_equipment` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'equipment_type',OLD.equipment_type,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'make',OLD.make,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'model',OLD.model,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'status',OLD.status,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'year',OLD.year,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'VIN',OLD.VIN,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'serial_number',OLD.serial_number,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'fuel_type',OLD.fuel_type,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'maintenance_criteria',OLD.maintenance_criteria,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'color',OLD.color,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'aquisition_method',OLD.aquisition_method,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'aquisition_odometer',OLD.aquisition_odometer,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'aquisition_hours',OLD.aquisition_hours,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'aquisition_date',OLD.aquisition_date,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'aquisition_from',OLD.aquisition_from,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'cost',OLD.cost,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'resale_price',OLD.resale_price,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'warranty',OLD.warranty,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'liscence_plate',OLD.liscence_plate,'','delete'), (OLD.asset_id, CONCAT('F',OLD.id), OLD.recorded_by,'notes',OLD.notes,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Facility Equipment History Audit On Insert` AFTER INSERT ON `en_mb_facility_equipment` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'equipment_type','',NEW.equipment_type,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'make','',NEW.make,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'model','',NEW.model,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'status','',NEW.status,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'year','',NEW.year,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'VIN','',NEW.VIN,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'serial_number','',NEW.serial_number,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'fuel_type','',NEW.fuel_type,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'maintenance_criteria','',NEW.maintenance_criteria,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'color','',NEW.color,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_method','',NEW.aquisition_method,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_odometer','',NEW.aquisition_odometer,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_hours','',NEW.aquisition_hours,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_date','',NEW.aquisition_date,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_from','',NEW.aquisition_from,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'cost','',NEW.cost,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'resale_price','',NEW.resale_price,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'warranty','',NEW.warranty,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'liscence_plate','',NEW.liscence_plate,'Add'), (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'notes','',NEW.notes,'Add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Facility Equipment History Audit On Update` AFTER UPDATE ON `en_mb_facility_equipment` FOR EACH ROW BEGIN IF (OLD.equipment_type <> NEW.equipment_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'equipment_type',OLD.equipment_type,NEW.equipment_type,'edit'); END IF; IF (OLD.equipment_description <> NEW.equipment_description) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'equipment_description',OLD.equipment_description,NEW.equipment_description,'edit'); END IF; IF (OLD.make <> NEW.make) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'make',OLD.make,NEW.make,'edit'); END IF; IF (OLD.model <> NEW.model) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'model',OLD.model,NEW.model,'edit'); END IF; IF (OLD.status <> NEW.status) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'status',OLD.status,NEW.status,'edit'); END IF; IF (OLD.year <> NEW.year) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'year',OLD.year,NEW.year,'edit'); END IF; IF (OLD.VIN <> NEW.VIN) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'VIN',OLD.VIN,NEW.VIN,'edit'); END IF; IF (OLD.serial_number <> NEW.serial_number) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'serial_number',OLD.serial_number,NEW.serial_number,'edit'); END IF; IF (OLD.fuel_type <> NEW.fuel_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'fuel_type',OLD.fuel_type,NEW.fuel_type,'edit'); END IF; IF (OLD.maintenance_criteria <> NEW.maintenance_criteria) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'maintenance_criteria',OLD.maintenance_criteria,NEW.maintenance_criteria,'edit'); END IF; IF (OLD.color <> NEW.color) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'color',OLD.color,NEW.color,'edit'); END IF; IF (OLD.aquisition_method <> NEW.aquisition_method) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_method',OLD.aquisition_method,NEW.aquisition_method,'edit'); END IF; IF (OLD.aquisition_odometer <> NEW.aquisition_odometer) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_odometer',OLD.aquisition_odometer,NEW.aquisition_odometer,'edit'); END IF; IF (OLD.aquisition_hours <> NEW.aquisition_hours) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_hours',OLD.aquisition_hours,NEW.aquisition_hours,'edit'); END IF; IF (OLD.aquisition_date <> NEW.aquisition_date) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_date',OLD.aquisition_date,NEW.aquisition_date,'edit'); END IF; IF (OLD.aquisition_from <> NEW.aquisition_from) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'aquisition_from',OLD.aquisition_from,NEW.aquisition_from,'edit'); END IF; IF (OLD.cost <> NEW.cost) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'cost',OLD.cost,NEW.cost,'edit'); END IF; IF (OLD.resale_price <> NEW.resale_price) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'resale_price',OLD.resale_price,NEW.resale_price,'edit'); END IF; IF (OLD.warranty <> NEW.warranty) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'warranty',OLD.warranty,NEW.warranty,'edit'); END IF; IF (OLD.liscence_plate <> NEW.liscence_plate) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'liscence_plate',OLD.liscence_plate,NEW.liscence_plate,'edit'); END IF; IF (OLD.notes <> NEW.notes) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('F',NEW.id), NEW.recorded_by,'notes',OLD.notes,NEW.notes,'edit'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Generate Asset ID` BEFORE INSERT ON `en_mb_facility_equipment` FOR EACH ROW BEGIN SET NEW.asset_id= concat('ACM',(SELECT AUTO_increment FROM information_schema.tables WHERE table_name = 'en_mb_facility_equipment' ORDER BY AUTO_increment DESC LIMIT 1) ); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Update_Event` BEFORE UPDATE ON `en_mb_facility_equipment` FOR EACH ROW BEGIN IF( NEW.status='') THEN SET  NEW.status=OLD.status; END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_facility_equipment_status`
--

CREATE TABLE `en_mb_facility_equipment_status` (
  `id` int(11) NOT NULL,
  `equipment_status` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_facility_equipment_status`
--

INSERT INTO `en_mb_facility_equipment_status` (`id`, `equipment_status`, `recorded_by`, `created_on`) VALUES
(1, 'In Service', 1, '2018-08-22 07:15:01'),
(2, 'Totaled', 1, '2018-08-22 07:15:16'),
(3, 'Sold', 1, '2018-08-22 07:15:21'),
(4, 'Out for Repair', 1, '2018-08-22 07:15:29'),
(5, 'Calibration/Inspection Due', 1, '2018-08-22 07:15:56'),
(6, 'Out for Calibration/Inspection', 1, '2018-08-22 07:15:55'),
(7, 'Maintenance Due', 1, '2018-08-22 07:15:54'),
(8, 'Out for Maintenance', 1, '2018-08-22 07:15:52'),
(9, 'Out of service', 1, '2018-08-22 07:15:50'),
(10, 'Returned from Servicing', 1, '2018-08-22 07:15:48'),
(11, 'Calibrate/Inspect on next use', 1, '2018-08-22 07:15:46'),
(12, 'Returned from Calibration/Inspection', 1, '2018-08-22 07:15:43');

--
-- Triggers `en_mb_facility_equipment_status`
--
DELIMITER $$
CREATE TRIGGER `Equipment Status Type History Audit Delete` AFTER DELETE ON `en_mb_facility_equipment_status` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',OLD.id), OLD.recorded_by,'equipment_status',OLD.equipment_status,'','delete'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Status Type History Audit Insert` AFTER INSERT ON `en_mb_facility_equipment_status` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'equipment_status','',NEW.equipment_status,'add'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Equipment Status Type History Audit Update` AFTER UPDATE ON `en_mb_facility_equipment_status` FOR EACH ROW BEGIN IF (OLD.equipment_status <> NEW.equipment_status) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ES',NEW.id), NEW.recorded_by,'equipment_status',OLD.equipment_status,NEW.equipment_status,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_maintenance_history`
--

CREATE TABLE `en_mb_maintenance_history` (
  `equipment_record_id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `before_value` longtext NOT NULL,
  `after_value` longtext,
  `source` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_maintenance_history`
--

INSERT INTO `en_mb_maintenance_history` (`equipment_record_id`, `asset_id`, `date_time`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES
(1, '', '2018-09-17 06:45:54', 'G1', 149, 'facility_equipment', '23', '177', 'edit'),
(2, '', '2018-09-17 06:45:54', 'G1', 149, 'detail_view', '35', '189', 'edit'),
(3, '', '2018-09-17 06:45:54', 'G1', 149, 'maintenance_records', '29', '183', 'edit'),
(4, '', '2018-09-17 06:45:54', 'G1', 149, 'active_maintenance_items', '42', '196', 'edit'),
(5, '', '2018-09-17 06:45:54', 'G2', 1, 'facility_equipment', '23', '177', 'edit'),
(6, '', '2018-09-17 06:45:54', 'G2', 1, 'detail_view', '36', '190', 'edit'),
(7, '', '2018-09-17 06:45:54', 'G2', 1, 'maintenance_records', '29', '183', 'edit'),
(8, '', '2018-09-17 06:45:54', 'G2', 1, 'active_maintenance_items', '42', '196', 'edit'),
(9, '', '2018-09-17 06:45:55', 'G4', 1, 'facility_equipment', '23', '177', 'edit'),
(10, '', '2018-09-17 06:45:55', 'G4', 1, 'detail_view', '39', '193', 'edit'),
(11, '', '2018-09-17 06:45:55', 'G4', 1, 'maintenance_records', '30', '184', 'edit'),
(12, '', '2018-09-17 06:45:55', 'G4', 1, 'calibration_inspection_records', '34', '188', 'edit'),
(13, '', '2018-09-17 06:45:55', 'G4', 1, 'active_maintenance_items', '42', '196', 'edit'),
(14, '', '2018-09-17 06:45:55', 'G4', 1, 'active_calibration_inspection_items', '43', '197', 'edit'),
(15, '', '2018-09-17 06:45:55', 'G5', 1, 'facility_equipment', '23', '177', 'edit'),
(16, '', '2018-09-17 06:45:55', 'G5', 1, 'detail_view', '37', '191', 'edit'),
(17, '', '2018-09-17 06:45:55', 'G5', 1, 'maintenance_records', '30', '184', 'edit'),
(18, '', '2018-09-17 06:45:55', 'G5', 1, 'active_maintenance_items', '42', '196', 'edit'),
(19, '', '2018-09-17 06:45:55', 'G3', 1, 'facility_equipment', '23', '177', 'edit'),
(20, '', '2018-09-17 06:45:55', 'G3', 1, 'detail_view', '38', '192', 'edit'),
(21, '', '2018-09-17 06:45:55', 'G3', 1, 'maintenance_records', '30', '184', 'edit'),
(22, '', '2018-09-17 06:45:55', 'G3', 1, 'calibration_inspection_records', '34', '188', 'edit'),
(23, '', '2018-09-17 06:45:55', 'G3', 1, 'active_maintenance_items', '42', '196', 'edit'),
(24, '', '2018-09-17 06:45:55', 'G3', 1, 'active_calibration_inspection_items', '43', '197', 'edit'),
(25, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'equipment_type', '', '1', 'Add'),
(26, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'make', '', 'SA', 'Add'),
(27, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'model', '', 'SAS', 'Add'),
(28, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'status', '', '1', 'Add'),
(29, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'year', '', '0000', 'Add'),
(30, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'VIN', '', '', 'Add'),
(31, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'serial_number', '', '', 'Add'),
(32, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'fuel_type', '', '', 'Add'),
(33, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'maintenance_criteria', '', '', 'Add'),
(34, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'color', '', '', 'Add'),
(35, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'aquisition_method', '', '', 'Add'),
(36, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'aquisition_odometer', '', '', 'Add'),
(37, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'aquisition_hours', '', '0', 'Add'),
(38, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'aquisition_date', '', '0000-00-00', 'Add'),
(39, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'aquisition_from', '', '', 'Add'),
(40, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'cost', '', '0', 'Add'),
(41, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'resale_price', '', '0', 'Add'),
(42, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'warranty', '', '', 'Add'),
(43, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'liscence_plate', '', '', 'Add'),
(44, 'ACM3241', '2018-09-17 06:57:14', 'F3241', 1, 'notes', '', '', 'Add');

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_maintenance_plans`
--

CREATE TABLE `en_mb_maintenance_plans` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `scheduled_date` varchar(100) NOT NULL,
  `reoccurring` int(11) NOT NULL,
  `period` varchar(255) NOT NULL,
  `instructions` longtext NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recorded_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_maintenance_plans`
--

INSERT INTO `en_mb_maintenance_plans` (`id`, `asset_id`, `scheduled_date`, `reoccurring`, `period`, `instructions`, `created_date`, `recorded_by`, `status`) VALUES
(1, 'F4E3295', '2018-09-14', 1, 'Monthly', 'First, login to your site and visit the Pages menu in your admin dashboard. Then, click on the Page that you need an ID fo', '2018-09-10 11:09:33', 1, 1),
(2, 'F4E3297', '2018-06-22', 0, '', 'Maintenance is reoccurring dfg ', '2018-06-22 12:17:37', 1, 1),
(3, 'F4E3290', '', 0, '', '', '2018-06-04 05:59:28', 150, 1),
(4, 'F4E3298', '2018-06-20', 1, 'Quarterly', 'aweaweqe', '2018-06-13 10:34:06', 1, 1),
(5, 'F4E3240', '2018-08-27', 1, 'Weekly', 'Test asdfsdfsdf', '2018-08-20 05:18:59', 1, 1),
(6, 'F4E3238', '2018-08-11', 1, 'Daily', 'asdasdd', '2018-08-10 04:51:18', 1, 1);

--
-- Triggers `en_mb_maintenance_plans`
--
DELIMITER $$
CREATE TRIGGER `Maintenance Plan History Audit On Insert` AFTER INSERT ON `en_mb_maintenance_plans` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'scheduled_date','',NEW.scheduled_date,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'reoccurring','',NEW.reoccurring,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'period','',NEW.period,'add'), (NEW.asset_id, CONCAT('CP',NEW.id), NEW.recorded_by,'instructions','',NEW.instructions,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Maintenance Plan History Audit on Update` AFTER UPDATE ON `en_mb_maintenance_plans` FOR EACH ROW BEGIN IF (OLD.scheduled_date <> NEW.scheduled_date) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP',NEW.id), NEW.recorded_by,'scheduled_date',OLD.scheduled_date,NEW.scheduled_date,'edit'); END IF; IF (OLD.period <> NEW.period) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP',NEW.id), NEW.recorded_by,'period',OLD.period,NEW.period,'edit'); END IF; IF OLD.instructions <> NEW.instructions THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP',NEW.id), NEW.recorded_by,'instructions',OLD.instructions,NEW.instructions,'edit'); END IF; IF OLD.reoccurring <> NEW.reoccurring THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP',NEW.id), NEW.recorded_by,'reoccurring',OLD.reoccurring,NEW.reoccurring,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_maintenance_plans_meta`
--

CREATE TABLE `en_mb_maintenance_plans_meta` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `maintenance_type` varchar(255) NOT NULL,
  `distance` varchar(255) NOT NULL,
  `choice_name` varchar(255) NOT NULL,
  `criteria` varchar(255) NOT NULL,
  `criteria_type` varchar(255) NOT NULL,
  `benchmark_odometer` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recorded_by` int(11) NOT NULL,
  `due_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_maintenance_plans_meta`
--

INSERT INTO `en_mb_maintenance_plans_meta` (`id`, `plan_id`, `asset_id`, `maintenance_type`, `distance`, `choice_name`, `criteria`, `criteria_type`, `benchmark_odometer`, `created_on`, `recorded_by`, `due_status`) VALUES
(545, 5, 'F4E3240', 'Usage Based', '', '', '', 'Miles', '', '2018-09-10 10:34:14', 0, 0),
(546, 5, 'F4E3240', 'Distance Based', '', '', '', 'Miles', '', '2018-09-10 10:34:10', 0, 0),
(634, 1, 'F4E3295', 'Distance Based', '10500', 'Oil Change', '100', 'Kilometers', '11500', '2018-09-10 10:34:27', 1, 1),
(635, 1, 'F4E3295', 'Distance Based', '10500', 'Break Check', '100', 'Kilometers', '11500', '2018-09-10 10:30:52', 1, 1),
(655, 1, 'F4E3295', 'Usage Based', '', 'Oil Change', '100', 'Kilometers', '23000', '2018-09-10 11:00:31', 1, 0),
(657, 1, 'F4E3295', 'Distance Based', '13000', '', '', 'Kilometers', '', '2018-09-10 10:36:50', 1, 0),
(658, 1, 'F4E3295', 'Usage Based', '', 'Break Check', '200', 'Kilometers', '23000', '2018-09-10 11:40:53', 1, 0),
(659, 1, 'F4E3295', 'Distance Based', '15000', '', '', 'Kilometers', '', '2018-09-10 11:40:53', 1, 0);

--
-- Triggers `en_mb_maintenance_plans_meta`
--
DELIMITER $$
CREATE TRIGGER `Audit History On Insert` AFTER INSERT ON `en_mb_maintenance_plans_meta` FOR EACH ROW BEGIN IF(NEW.distance!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'distance','',NEW.distance,'add'); END IF; IF(NEW.choice_name!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'choice_name','',NEW.choice_name,'add'); END IF; IF(NEW.criteria!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'criteria','',NEW.criteria,'add'); END IF; IF(NEW.benchmark_odometer!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES(NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'benchmark_odometer','',NEW.benchmark_odometer,'add'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Maintenance Plan Meta History Audit on Delete` BEFORE DELETE ON `en_mb_maintenance_plans_meta` FOR EACH ROW BEGIN IF(OLD.distance!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('MP-META-',OLD.id), OLD.recorded_by,'distance',OLD.distance,'','delete'); END IF; IF(OLD.choice_name!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES(OLD.asset_id, CONCAT('MP-META-',OLD.id), OLD.recorded_by,'choice_name',OLD.choice_name,'','delete'); END IF; IF(OLD.criteria!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('MP-META-',OLD.id), OLD.recorded_by,'criteria',OLD.criteria,'','delete'); END IF; IF(OLD.benchmark_odometer!="") THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('MP-META-',OLD.id), OLD.recorded_by,'benchmark_odometer',OLD.benchmark_odometer,'','delete'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Maintenance Plan Meta History audit On Update` AFTER UPDATE ON `en_mb_maintenance_plans_meta` FOR EACH ROW BEGIN IF (OLD.distance <> NEW.distance)  THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'distance',OLD.distance,NEW.distance,'update'); END IF; IF (OLD.choice_name <> NEW.choice_name)  THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'choice_name',OLD.choice_name,NEW.choice_name,'update'); END IF; IF (OLD.criteria <> NEW.criteria)  THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'criteria',OLD.criteria,NEW.criteria,'update'); END IF; IF (OLD.benchmark_odometer <> NEW.benchmark_odometer)  THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES(NEW.asset_id, CONCAT('MP-META-',NEW.id), NEW.recorded_by,'benchmark_odometer',OLD.benchmark_odometer,NEW.benchmark_odometer,'update'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_maintenance_records`
--

CREATE TABLE `en_mb_maintenance_records` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(255) NOT NULL,
  `service_type` int(11) NOT NULL,
  `repair_date` date NOT NULL,
  `odometer_hrs_used` float NOT NULL,
  `explanation_of_service` varchar(255) NOT NULL,
  `under_warranty` varchar(255) NOT NULL,
  `repair_facility` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `service_documentations` varchar(255) NOT NULL,
  `instructions_for_next_visit` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_maintenance_records`
--

INSERT INTO `en_mb_maintenance_records` (`id`, `asset_id`, `service_type`, `repair_date`, `odometer_hrs_used`, `explanation_of_service`, `under_warranty`, `repair_facility`, `cost`, `service_documentations`, `instructions_for_next_visit`, `recorded_by`, `date_created`) VALUES
(1, 'F4E3295', 17, '2018-05-15', 700, 'Engine Work, Transmission Repair, Vehicle Check up', 'No', 'Here is text', '2', '<a class=\"documents-btn\" rel=\"1\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'Instrictions', 1, '2018-07-12 10:31:32'),
(9, 'F4E3295', 1, '2018-05-15', 45, 'Transmission Repair', 'Yes', 'ertet', '1', '<a class=\"documents-btn\" rel=\"9\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'dfgdfg', 1, '2018-06-05 09:17:23'),
(10, 'F4E3297', 17, '2018-05-22', 0, 'Engine Work, Transmission Repair', 'Yes', 'asd', '3.24', '<a class=\"documents-btn\" rel=\"10\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'asd', 150, '2018-06-05 09:17:39'),
(11, 'F4E3299', 17, '2018-06-15', 200, 'Engine Work, Transmission Repair', 'Yes', 'Test Mechanic', '2', '<a class=\"documents-btn\" rel=\"11\" href=\"#\"><img src=\"http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\"></a>', 'Testr sdf sdfsd', 149, '2018-06-05 09:17:33'),
(12, 'F4E3298', 19, '2018-06-13', 234, 'Engine Work', 'Yes', 'ert', '3.45', '<a class=\'documents-btn\' title=\'Documents\' rel=\'0\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', 'wr', 146, '2018-06-12 07:25:01'),
(16, 'F4E3295', 19, '2018-07-18', 234234, 'Transmission Repair, Vehicle Check up', 'Yes', '234', '234', '<a class=\'documents-btn\' title=\'Documents\' rel=\'0\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', '23424', 1, '2018-07-18 12:12:18'),
(25, 'F4E3295', 19, '2018-07-18', 0, 'Vehicle Check up', 'Yes', 'sdfsd', 'sffsf', '<a class=\'documents-btn\' title=\'Documents\' rel=\'25\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', 'sdfsd', 1, '2018-07-18 12:47:50'),
(30, 'F4E3295', 19, '2018-07-23', 34234, 'Engine Work', 'Yes', 'asdasd', '12342.34', '<a class=\'documents-btn\' title=\'Documents\' rel=\'30\' href=\'#\'><img src=\'http://electron.mobilytedev.com/wp-content/uploads/2018/06/folder.png\'></a>', 'sdfsdf', 1, '2018-07-30 06:06:34');

--
-- Triggers `en_mb_maintenance_records`
--
DELIMITER $$
CREATE TRIGGER `Maintenance Records History Audit On Delete` BEFORE DELETE ON `en_mb_maintenance_records` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'odometer_hrs_used',OLD.odometer_hrs_used,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'service_type',OLD.service_type,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'repair_date',OLD.repair_date,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'explanation_of_service',OLD.explanation_of_service,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'under_warranty',OLD.under_warranty,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'repair_facility',OLD.repair_facility,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'cost',OLD.cost,'','delete'), (OLD.asset_id, CONCAT('MR',OLD.id), OLD.recorded_by,'instructions_for_next_visit',OLD.instructions_for_next_visit,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Maintenance Records History Audit On Insert` AFTER INSERT ON `en_mb_maintenance_records` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'odometer_hrs_used','',NEW.odometer_hrs_used,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'service_type','',NEW.service_type,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'repair_date','',NEW.repair_date,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'explanation_of_service','',NEW.explanation_of_service,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'under_warranty','',NEW.under_warranty,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'repair_facility','',NEW.repair_facility,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'cost','',NEW.cost,'Add'), (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'instructions_for_next_visit','',NEW.instructions_for_next_visit,'Add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Maintenance Records History Audit On Update` AFTER UPDATE ON `en_mb_maintenance_records` FOR EACH ROW BEGIN IF (OLD.odometer_hrs_used <> NEW.odometer_hrs_used) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'odometer_hrs_used',OLD.odometer_hrs_used,NEW.odometer_hrs_used,'edit'); END IF; IF (OLD.service_type <> NEW.service_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'service_type',OLD.service_type,NEW.service_type,'edit'); END IF; IF OLD.repair_date <> NEW.repair_date THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'repair_date',OLD.repair_date,NEW.repair_date,'edit'); END IF; IF OLD.explanation_of_service <> NEW.explanation_of_service THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'explanation_of_service',OLD.explanation_of_service,NEW.explanation_of_service,'edit'); END IF; IF OLD.under_warranty <> NEW.under_warranty THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'under_warranty',OLD.under_warranty,NEW.under_warranty,'edit'); END IF; IF OLD.repair_facility <> NEW.repair_facility THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'repair_facility',OLD.repair_facility,NEW.repair_facility,'edit'); END IF; IF OLD.cost <> NEW.cost THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'cost',OLD.cost,NEW.cost,'edit'); END IF; IF OLD.instructions_for_next_visit <> NEW.instructions_for_next_visit THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('MR',NEW.id), NEW.recorded_by,'instructions_for_next_visit',OLD.instructions_for_next_visit,NEW.instructions_for_next_visit,'edit'); END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_services_types`
--

CREATE TABLE `en_mb_services_types` (
  `id` int(11) NOT NULL,
  `service_type` varchar(255) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_services_types`
--

INSERT INTO `en_mb_services_types` (`id`, `service_type`, `recorded_by`, `created_on`) VALUES
(1, 'Schedule Maintenance', 1, '2018-08-20 07:20:57'),
(17, 'Repair', 1, '2018-05-22 06:55:08'),
(19, 'Accident', 1, '2018-05-29 07:38:59'),
(20, 'Upgrade', 1, '2018-05-29 07:39:08');

--
-- Triggers `en_mb_services_types`
--
DELIMITER $$
CREATE TRIGGER `Service Type History Audit Insert` BEFORE INSERT ON `en_mb_services_types` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ST',NEW.id), NEW.recorded_by,'service_type','',NEW.service_type,'add'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Service Type History Audit On Update` AFTER UPDATE ON `en_mb_services_types` FOR EACH ROW BEGIN IF (OLD.service_type <> NEW.service_type) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ST',NEW.id), NEW.recorded_by,'service_type',OLD.service_type,NEW.service_type,'edit'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Service Type History Audit on Delete` BEFORE DELETE ON `en_mb_services_types` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('ST',OLD.id), OLD.recorded_by,'service_type',OLD.service_type,'','delete'); END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_service_explanation`
--

CREATE TABLE `en_mb_service_explanation` (
  `id` int(11) NOT NULL,
  `service_explanation` varchar(255) NOT NULL,
  `equipment_type` varchar(255) DEFAULT NULL,
  `recorded_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `en_mb_service_explanation`
--

INSERT INTO `en_mb_service_explanation` (`id`, `service_explanation`, `equipment_type`, `recorded_by`, `created_on`) VALUES
(1, 'Engine Works', 'Instrumentation, Non Calibrated Equipment, Inspected Equipment', 1, '2018-08-27 07:40:16'),
(7, 'Transmission Repairs', 'Automobile, Instrumentations', 1, '2018-08-27 07:43:10'),
(8, 'Vehicle Check up', 'Automobile', 0, '2018-05-23 10:08:46');

--
-- Triggers `en_mb_service_explanation`
--
DELIMITER $$
CREATE TRIGGER `Explanation History Audit on Delete` AFTER DELETE ON `en_mb_service_explanation` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('SE',OLD.id), OLD.recorded_by,'service_explanation',OLD.service_explanation,'','delete'); END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Explanation History Update` AFTER UPDATE ON `en_mb_service_explanation` FOR EACH ROW BEGIN IF (OLD.service_explanation <> NEW.service_explanation) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('SE',NEW.id), NEW.recorded_by,'service_explanation',OLD.service_explanation,NEW.service_explanation,'edit'); END IF; END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Insert Explanation History Insert` AFTER INSERT ON `en_mb_service_explanation` FOR EACH ROW BEGIN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES ('', CONCAT('SE',NEW.id), NEW.recorded_by,'service_explanation','',NEW.service_explanation,'add'); END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `en_mb_tracking_active_items`
--

CREATE TABLE `en_mb_tracking_active_items` (
  `id` int(11) NOT NULL,
  `asset_id` varchar(100) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `last_odometer` varchar(255) NOT NULL DEFAULT '0',
  `activity_status` int(11) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `cron_type` varchar(255) NOT NULL,
  `instruction` varchar(255) NOT NULL,
  `instructions_for_next_visit` longtext NOT NULL,
  `recorded_by` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `en_mb_tracking_active_items`
--
DELIMITER $$
CREATE TRIGGER `Tracking table History Audit on Delete` AFTER DELETE ON `en_mb_tracking_active_items` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'due_date',OLD.due_date,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'description',OLD.description,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'last_odometer',OLD.last_odometer,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'activity_status',OLD.activity_status,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'item_type',OLD.item_type,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'cron_type',OLD.cron_type,'','delete'), (OLD.asset_id, CONCAT('T',OLD.id), OLD.recorded_by,'instruction',OLD.instruction,'','delete')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Tracking table History Audit on Insert` AFTER INSERT ON `en_mb_tracking_active_items` FOR EACH ROW INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'due_date','',NEW.due_date,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'description','',NEW.description,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'last_odometer','',NEW.last_odometer,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'activity_status','',NEW.activity_status,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'item_type','',NEW.item_type,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'cron_type','',NEW.cron_type,'add'), (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'instruction','',NEW.instruction,'add')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Tracking table History Audit on Update` AFTER UPDATE ON `en_mb_tracking_active_items` FOR EACH ROW BEGIN IF (OLD.due_date <> NEW.due_date) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'due_date',OLD.due_date,NEW.due_date,'edit'); END IF; IF (OLD.description <> NEW.description) THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'description',OLD.description,NEW.description,'edit'); END IF; IF OLD.last_odometer <> NEW.last_odometer THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'last_odometer',OLD.last_odometer,NEW.last_odometer,'edit'); END IF; IF OLD.activity_status <> NEW.activity_status THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'activity_status',OLD.activity_status,NEW.activity_status,'edit'); END IF; IF OLD.item_type <> NEW.item_type THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'item_type',OLD.item_type,NEW.item_type,'edit'); END IF; IF OLD.cron_type <> NEW.cron_type THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'cron_type',OLD.cron_type,NEW.cron_type,'edit'); END IF; IF OLD.instruction <> NEW.instruction THEN INSERT INTO en_mb_maintenance_history (`asset_id`, `record_id`, `user_id`, `field`, `before_value`, `after_value`, `source`) VALUES (NEW.asset_id, CONCAT('T',NEW.id), NEW.recorded_by,'instruction',OLD.instruction,NEW.instruction,'edit'); END IF; END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `en_mb_calibrations`
--
ALTER TABLE `en_mb_calibrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `en_mb_calibrations_plans`
--
ALTER TABLE `en_mb_calibrations_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_equipments_explanation_relation`
--
ALTER TABLE `en_mb_equipments_explanation_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_equipment_group_templates`
--
ALTER TABLE `en_mb_equipment_group_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_equipment_manager_settings`
--
ALTER TABLE `en_mb_equipment_manager_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_equipment_types`
--
ALTER TABLE `en_mb_equipment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_equipment_type_relation_settings`
--
ALTER TABLE `en_mb_equipment_type_relation_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_facility_description`
--
ALTER TABLE `en_mb_facility_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_facility_equipment`
--
ALTER TABLE `en_mb_facility_equipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_id` (`asset_id`);

--
-- Indexes for table `en_mb_facility_equipment_status`
--
ALTER TABLE `en_mb_facility_equipment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_maintenance_history`
--
ALTER TABLE `en_mb_maintenance_history`
  ADD PRIMARY KEY (`equipment_record_id`);

--
-- Indexes for table `en_mb_maintenance_plans`
--
ALTER TABLE `en_mb_maintenance_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_maintenance_plans_meta`
--
ALTER TABLE `en_mb_maintenance_plans_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_maintenance_records`
--
ALTER TABLE `en_mb_maintenance_records`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `en_mb_services_types`
--
ALTER TABLE `en_mb_services_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_service_explanation`
--
ALTER TABLE `en_mb_service_explanation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `en_mb_tracking_active_items`
--
ALTER TABLE `en_mb_tracking_active_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `en_mb_calibrations`
--
ALTER TABLE `en_mb_calibrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `en_mb_calibrations_plans`
--
ALTER TABLE `en_mb_calibrations_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `en_mb_equipments_explanation_relation`
--
ALTER TABLE `en_mb_equipments_explanation_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `en_mb_equipment_group_templates`
--
ALTER TABLE `en_mb_equipment_group_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `en_mb_equipment_manager_settings`
--
ALTER TABLE `en_mb_equipment_manager_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `en_mb_equipment_types`
--
ALTER TABLE `en_mb_equipment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `en_mb_equipment_type_relation_settings`
--
ALTER TABLE `en_mb_equipment_type_relation_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `en_mb_facility_description`
--
ALTER TABLE `en_mb_facility_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `en_mb_facility_equipment`
--
ALTER TABLE `en_mb_facility_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3242;
--
-- AUTO_INCREMENT for table `en_mb_facility_equipment_status`
--
ALTER TABLE `en_mb_facility_equipment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `en_mb_maintenance_history`
--
ALTER TABLE `en_mb_maintenance_history`
  MODIFY `equipment_record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `en_mb_maintenance_plans`
--
ALTER TABLE `en_mb_maintenance_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `en_mb_maintenance_plans_meta`
--
ALTER TABLE `en_mb_maintenance_plans_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=660;
--
-- AUTO_INCREMENT for table `en_mb_maintenance_records`
--
ALTER TABLE `en_mb_maintenance_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `en_mb_services_types`
--
ALTER TABLE `en_mb_services_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `en_mb_service_explanation`
--
ALTER TABLE `en_mb_service_explanation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `en_mb_tracking_active_items`
--
ALTER TABLE `en_mb_tracking_active_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
