<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Registers your tab in the quiz settings page
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_logic_register_quiz_settings_tabs() {
  global $mlwQuizMasterNext;
  if ( ! is_null( $mlwQuizMasterNext ) && ! is_null( $mlwQuizMasterNext->pluginHelper ) && method_exists( $mlwQuizMasterNext->pluginHelper, 'register_quiz_settings_tabs' ) ) {
    $mlwQuizMasterNext->pluginHelper->register_quiz_settings_tabs( "Logic", 'qsm_addon_logic_quiz_settings_tabs_content' );
  }
}

/**
 * Generates the content for your quiz settings tab
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_logic_quiz_settings_tabs_content() {
  global $wpdb;
	global $mlwQuizMasterNext;
	$quiz_id = $_GET["quiz_id"];
  $questions = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "mlw_questions WHERE quiz_id=%d AND deleted='0' ORDER BY question_order ASC", $quiz_id ) );
  $question_data_json = array();
  foreach ( $questions as $question ) {
    $question_array = array(
      "id" => $question->question_id,
      "question" => $question->question_name,
      "answers" => @unserialize( $question->answer_array ),
      'type' => $question->question_type_new
    );
    $question_data_json[] = $question_array;
  };

  $logic_rules = $mlwQuizMasterNext->pluginHelper->get_quiz_setting( "logic_rules" );

  wp_enqueue_script( 'qsm_logic_admin_script', plugins_url( '../js/qsm-logic-admin.js' , __FILE__ ), array( 'jquery' ), date("YmdHis") );
  wp_localize_script( 'qsm_logic_admin_script', 'qsm_logic_object', array( 'logic_rules' => unserialize( $logic_rules ), 'question_data' => $question_data_json, 'quiz_id' => $quiz_id, 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_style( 'qsm_logic_admin_style', plugins_url( '../css/qsm-logic-admin.css' , __FILE__ ) );
  ?>
  <h2>Logic</h2>
  <div class="logic-rule-message"></div>
  <a class="save-rules button-primary">Save Rules</a> <a class="add-rule button-primary">Add Rule</a>
  <div class="logic-rules"></div>
  <a class="save-rules button-primary">Save Rules</a>
  <?php
}
?>
