<?php
/**
 * Adds the logic JavaScript function to the enqueue in the quiz
 *
 * @since 0.1.0
 * @return string The HTML of the quiz
 */
function qsm_addon_logic_script_integration( $display, $qmn_quiz_options, $qmn_array_for_variables ) {
  global $mlwQuizMasterNext;
  $logic_rules = $mlwQuizMasterNext->pluginHelper->get_quiz_setting( "logic_rules" );
  wp_enqueue_script( 'qsm_logic_quiz_script', plugins_url( '../js/qsm-logic-quiz.js' , __FILE__ ), array( 'jquery' ), '1.0.1' );
  wp_localize_script( 'qsm_logic_quiz_script', 'qsm_logic_object', unserialize( $logic_rules ) );
  return $display;
}
?>
