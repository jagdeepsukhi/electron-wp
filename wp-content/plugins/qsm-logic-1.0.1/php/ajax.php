<?php
/**
 * Saves the logic rules from the quiz settings tab
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_logic_ajax() {
  global $wpdb;
  global $mlwQuizMasterNext;
  $mlwQuizMasterNext->quizCreator->set_id( intval( $_POST["quiz_id"] ) );
  $logic_ajax["status"] = $mlwQuizMasterNext->pluginHelper->update_quiz_setting( "logic_rules", serialize( $_POST["logic_rules"] ) );
  echo json_encode( $logic_ajax );
  die();
}

?>
