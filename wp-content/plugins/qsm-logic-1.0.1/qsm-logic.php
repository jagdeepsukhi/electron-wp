<?php
/**
 * Plugin Name: QSM - Logic
 * Plugin URI: https://quizandsurveymaster.com
 * Description: Allows you to show and hide different questions based on previous answers
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com
 * Version: 1.0.1
 *
 * @author Frank Corso
 * @version 1.0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit;


/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 0.1.0
  */
class QSM_Logic {

    /**
     * Version Number
     *
     * @var string
     * @since 0.1.0
     */
    public $version = '1.0.1';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 0.1.0
  	  * @uses QSM_Logic::load_dependencies() Loads required filed
  	  * @uses QSM_Logic::add_hooks() Adds actions to hooks and filters
      * @uses QSM_Logic::check_license() Checks if license is active and if updates are available
  	  * @return void
  	  */
    function __construct() {
      $this->load_dependencies();
      $this->add_hooks();
      $this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 0.1.0
  	  * @return void
  	  */
    public function load_dependencies() {
      include( "php/addon-settings-tab-content.php" );
      include( "php/quiz-settings-tab-content.php" );
      include( "php/ajax.php" );
      include( "php/qsm-intergration.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 0.1.0
  	  * @return void
  	  */
    public function add_hooks() {
      add_action( 'admin_init', 'qsm_addon_logic_register_quiz_settings_tabs' );
      add_action( 'admin_init', 'qsm_addon_logic_register_addon_settings_tabs' );
      add_action( 'wp_ajax_qsm_save_logic', 'qsm_addon_logic_ajax' );
      add_action( 'wp_ajax_nopriv_qsm_save_logic', 'qsm_addon_logic_ajax' );
      add_filter( 'qmn_begin_quiz', 'qsm_addon_logic_script_integration', 10, 3 );
    }

    /**
     * Checks license
     *
     * Checks to see if license is active and, if so, checks for updates
     *
     * @since 1.3.0
     * @return void
     */
     public function check_license() {

       if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {

       	// load our custom updater
       	include( 'php/EDD_SL_Plugin_Updater.php' );
       }

      // retrieve our license key from the DB
      $logic_data = get_option( 'qsm_addon_logic_settings', '' );
      if ( isset( $logic_data["license_key"] ) ) {
        $license_key = trim( $logic_data["license_key"] );
      } else {
        $license_key = '';
      }

     	// setup the updater
     	$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
     			'version' 	=> $this->version, 				// current version number
     			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
     			'item_name' => 'Logic', 	// name of this plugin
     			'author' 	=> 'Frank Corso'  // author of this plugin
     		)
     	);
     }
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 0.1.0
 * @return void
 */
function qsm_addon_logic_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_logic = new QSM_Logic();
	} else {
		add_action( 'admin_notices', 'qsm_addon_logic_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_logic_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       0.1.0
 * @return      string The notice to display
 */
function qsm_addon_logic_missing_qsm() {
  echo '<div class="error"><p>QSM - Logic requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
