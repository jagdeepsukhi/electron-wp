/**
 * QSM - Logic v0.1.0
 */

var QSM_Logic_Quiz;
(function ($) {
  QSM_Logic_Quiz = {
    VERSION : '1.0.1',
    DEFAULTS : {},
    checkRule : function( rule ) {
      var conditionsTrue = 0;
      var totalIf = 0;
      $.each( rule.if, function( i, val ) {
        totalIf++;
        var $question = $( "input[name=question" + val.question + "]:checked, select[name=question" + val.question + "]" );
        if ( "No Answer Provided" != $question.val() ) {
          switch ( val.condition ) {
            case 'is equal to':
              if ( $question.val() === val.answer ) {
                conditionsTrue++;
              }
              break;
            case 'is not equal to':
              if ( $question.val() !== val.answer ) {
                conditionsTrue++;
              }
              break;
            case 'is greater than':
              if ( $question.val() > val.answer ) {
                conditionsTrue++;
              }
              break;
            case 'is less than':
              if ( $question.val() < val.answer ) {
                conditionsTrue++;
              }
              break;
            default:
              // default
          }
        } else {
          // Default answer
        }
      });
      if ( conditionsTrue === totalIf ) {
        $.each( rule.then, function( i, val ) {
          if ( 'Show' === val.condition) {
            $( '.question-section-id-' + val.question ).show();
          } else {
            $( '.question-section-id-' + val.question ).hide();
          }
        });
      } else {
        $.each( rule.then, function( i, val ) {
          if ( 'Show' !== val.condition) {
            $( '.question-section-id-' + val.question ).show();
          } else {
            $( '.question-section-id-' + val.question ).hide();
          }
        });
      }
    }
  };
  $(function() {
    $.each( qsm_logic_object, function( i, val ) {
      QSM_Logic_Quiz.checkRule( qsm_logic_object[i] );
      $.each( val.if, function( j, vals ) {
        $( "input[name=question" + vals.question + "], select[name=question" + vals.question + "]" ).on( "change", function() {
          QSM_Logic_Quiz.checkRule( qsm_logic_object[i] );
        });
      });
    });
  });
}(jQuery));
