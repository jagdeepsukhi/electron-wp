/**
 * QSM - Logic v0.1.0
 */

var QSM_Logic;
(function ($) {
  var question_options = '';
  $.each( qsm_logic_object.question_data, function( i, val ) {
    question_options += '<option value="' + val.id + '">' + val.question + '</option>';
  });
  QSM_Logic = {
    VERSION : '0.1.0',
    DEFAULTS : {},
    question_option_string : question_options,
    add_rule : function() {
      $( '.logic-rules' ).append(
        '<div class="logic-rule-single">' +
          '<div class="logic-rule-single-body">' +
            '<div class="logic-rule-single-if">' +
              '<div class="logic-rule-single-if-row">When this happens...</div>' +
              '<div class="logic-rule-single-if-row">' +
                '<select class="if-question">' +
                  '<option value="none">Select a question</option>' +
                  question_options +
                '</select>' +
                '<select class="if-condition">' +
                  '<option>is equal to</option>' +
                  '<option>is not equal to</option>' +
                  '<option>is greater than</option>' +
                  '<option>is less than</option>' +
                '</select>' +
                '<select class="if-answer">' +
                  '<option>Select an answer</option>' +
                '</select>' +
              '</div>' +
              '<div class="logic-rule-single-if-row">' +
                '<span class="button add-new-if">Add additional condition</span>' +
              '</div>' +
            '</div>' +
            '<div class="logic-rule-single-arrow">' +
              '<span class="dashicons dashicons-arrow-right-alt"></span>' +
            '</div>' +
            '<div class="logic-rule-single-then">' +
              '<div class="logic-rule-single-then-row">Do this</div>' +
              '<div class="logic-rule-single-then-row">' +
                '<select class="then-condition">' +
                  '<option>Show</option>' +
                  '<option>Hide</option>' +
                '</select>' +
                '<select class="then-question">' +
                  '<option>Select a question</option>' +
                  question_options +
                '</select>' +
              '</div>' +
              '<div class="logic-rule-single-then-row">' +
                '<span class="button add-new-then">Add additional action</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="logic-rule-single-footer">' +
            '<span class="button delete-rule">Delete Rule</span>' +
          '</div>' +
        '</div>'
      );
    },
    deleteRule : function( $deleteElement ) {
      $deleteElement.parent().parent().remove();
    },
    addCondition : function( $ifElement ) {
      $ifElement.append(
        '<div class="logic-rule-single-if-row">AND</div>' +
        '<div class="logic-rule-single-if-row">' +
          '<select class="if-question">' +
            '<option>Select a question</option>' +
            QSM_Logic.question_option_string +
          '</select>' +
          '<select class="if-condition">' +
            '<option>is equal to</option>' +
            '<option>is not equal to</option>' +
            '<option>is greater than</option>' +
            '<option>is less than</option>' +
          '</select>' +
          '<select class="if-answer">' +
            '<option>Select an answer</option>' +
          '</select>' +
          '<span class="button delete-if-row">Delete</span>' +
        '</div>' +
        '<div class="logic-rule-single-if-row">' +
          '<span class="button add-new-if">Add additional condition</span>' +
        '</div>'
      );
    },
    deleteCondition : function( $deleteElement ) {
      if ( $deleteElement.parent().parent().children().length > 3 ) {
        $deleteElement.parent().prev().remove();
      }
      $deleteElement.parent().remove();
    },
    addTrigger : function( $thenElement ) {
      $thenElement.append(
        '<div class="logic-rule-single-then-row">AND</div>' +
        '<div class="logic-rule-single-then-row">' +
          '<select class="then-condition">' +
            '<option>Show</option>' +
            '<option>Hide</option>' +
          '</select>' +
          '<select class="then-question">' +
            '<option>Select a question</option>' +
            QSM_Logic.question_option_string +
          '</select>' +
          '<span class="button delete-then-row">Delete</span>' +
        '</div>' +
        '<div class="logic-rule-single-then-row">' +
          '<span class="button add-new-then">Add additional action</span>' +
        '</div>'
      );
    },
    deleteTrigger : function( $deleteElement ) {
      if ( $deleteElement.parent().parent().children().length > 3 ) {
        $deleteElement.parent().prev().remove();
      }
      $deleteElement.parent().remove();
    },
    changeQuestion : function( question, $questionElement ) {
      $questionElement.siblings( '.if-answer' ).remove();
      $questionElement.siblings( '.delete-if-row' ).remove();
      $.each( qsm_logic_object.question_data, function( i, val ) {
        if ( val.id === question ) {
          switch ( val.type ) {
            case "0":
            case "1":
            case "2":
            case "4":
            case "10":
              $questionElement.parent().append( '<select class="if-answer"></select>');
              $.each( val.answers, function( j, answer ) {
                $questionElement.siblings( '.if-answer' ).append( '<option>' + answer[0] + '</option>' );
              });
              break;
            case "3":
            case "5":
            case "7":
              $questionElement.parent().append( '<input type="text" class="if-answer" />');
              break;
            default:
              $questionElement.parent().append( '<input type="text" class="if-answer" />');
          }
        }
      });
      if ( $questionElement.parent().parent().children().length > 3 ) {
        $questionElement.parent().append( '<span class="button delete-if-row">Delete</span>' );
      }
    },
    load : function() {
      var $logic_rule = '';
      $.each( qsm_logic_object.logic_rules, function( i, val ) {
        $logic_rule = $( '<div class="logic-rule-single ' + i + '">' +
          '<div class="logic-rule-single-body">' +
            '<div class="logic-rule-single-if">' +
              '<div class="logic-rule-single-if-row">When this happens...</div>' +
            '</div>' +
            '<div class="logic-rule-single-arrow">' +
              '<span class="dashicons dashicons-arrow-right-alt"></span>' +
            '</div>' +
            '<div class="logic-rule-single-then">' +
              '<div class="logic-rule-single-then-row">Do this</div>' +
            '</div>' +
          '</div>' +
          '<div class="logic-rule-single-footer">' +
            '<span class="button delete-rule">Delete Rule</span>' +
          '</div>' +
        '</div>' );
        $.each( val.if, function( j, ifVal ) {
          if ( 0 !== j ) {
            $logic_rule.find( '.logic-rule-single-if' ).append( '<div class="logic-rule-single-if-row">AND</div>');
          }
          $logic_rule.find( '.logic-rule-single-if' ).append(
            '<div class="logic-rule-single-if-row ' + j + '">' +
              '<select class="if-question">' +
                '<option>Select a question</option>' +
                QSM_Logic.question_option_string +
              '</select>' +
              '<select class="if-condition">' +
                '<option>is equal to</option>' +
                '<option>is not equal to</option>' +
                '<option>is greater than</option>' +
                '<option>is less than</option>' +
              '</select>' +
              '<input type="text" class="if-answer" />' +
            '</div>'
          );
          if ( 0 !== j ) {
            $logic_rule.find( '.logic-rule-single-if .logic-rule-single-if-row.' + j ).append( '<span class="button delete-if-row">Delete</span>' );
          }
          $logic_rule.find( '.logic-rule-single-if .logic-rule-single-if-row.' + j + ' .if-question' ).val( ifVal.question );
          $logic_rule.find( '.logic-rule-single-if .logic-rule-single-if-row.' + j + ' .if-condition' ).val( ifVal.condition );
          $logic_rule.find( '.logic-rule-single-if .logic-rule-single-if-row.' + j + ' .if-answer' ).val( ifVal.answer );
        });
        $logic_rule.find( '.logic-rule-single-if' ).append(
          '<div class="logic-rule-single-if-row">' +
            '<span class="button add-new-if">Add additional condition</span>' +
          '</div>'
        );
        $.each( val.then, function( j, thenVal ) {
          if ( 0 !== j ) {
            $logic_rule.find( '.logic-rule-single-then' ).append( '<div class="logic-rule-single-then-row">AND</div>');
          }
          $logic_rule.find( '.logic-rule-single-then' ).append(
            '<div class="logic-rule-single-then-row ' + j + '">' +
              '<select class="then-condition">' +
                '<option>Show</option>' +
                '<option>Hide</option>' +
              '</select>' +
              '<select class="then-question">' +
                '<option>Select a question</option>' +
                QSM_Logic.question_option_string +
              '</select>' +
            '</div>'
          );
          if ( 0 !== j ) {
            $logic_rule.find( '.logic-rule-single-then .logic-rule-single-then-row.' + j ).append( '<span class="button delete-then-row">Delete</span>' );
          }
          $logic_rule.find( '.logic-rule-single-then .logic-rule-single-then-row.' + j + ' .then-question' ).val( thenVal.question );
          $logic_rule.find( '.logic-rule-single-then .logic-rule-single-then-row.' + j + ' .then-condition' ).val( thenVal.condition );
        });
        $logic_rule.find( '.logic-rule-single-then' ).append(
          '<div class="logic-rule-single-then-row">' +
            '<span class="button add-new-then">Add additional action</span>' +
          '</div>'
        );
        $( '.logic-rules' ).append( $logic_rule );
      });
    },
    save : function() {
      $( '.logic-rule-message' ).empty();
      var logic_rule_array = [];
      var logic_rule_single = {};
      $( '.logic-rule-single' ).each( function() {
        logic_rule_single = {
          if : [],
          then : []
        };
        $( this ).find( '.logic-rule-single-if-row:nth-child(even)' ).each( function() {
          logic_rule_single.if.push({
            question : $( this ).children( '.if-question' ).val(),
            condition : $( this ).children( '.if-condition' ).val(),
            answer : $( this ).children( '.if-answer' ).val()
          });
        });
        $( this ).find( '.logic-rule-single-then-row:nth-child(even)' ).each( function() {
          logic_rule_single.then.push({
            question : $( this ).children( '.then-question' ).val(),
            condition : $( this ).children( '.then-condition' ).val(),
          });
        });
        logic_rule_array.push( logic_rule_single );
      });

      var data = {
    		action: 'qsm_save_logic',
    		logic_rules: logic_rule_array,
        quiz_id : qsm_logic_object.quiz_id
    	};

    	jQuery.post( qsm_logic_object.ajaxurl, data, function( response ) {
    		QSM_Logic.saved( JSON.parse( response ) );
    	});
    },
    saved : function( response ) {
      $( '.logic-rule-message' ).removeClass( 'updated' ).removeClass( 'error' );
      if ( response.status ) {
        $( '.logic-rule-message' ).addClass( 'updated' );
        $( '.logic-rule-message' ).append( '<p><strong>Success</strong> Your rules have been saved!</p>' );
      } else {
        $( '.logic-rule-message' ).addClass( 'error' );
        $( '.logic-rule-message' ).append( '<p><strong>Error</strong> There was an error encountered when saving your rules. Please try again.</p>' );
      }
    }
  };
  $(function() {
    QSM_Logic.load();
    $( '.add-rule' ).on( 'click', function() {
      QSM_Logic.add_rule();
    });
    $( '.save-rules' ).on( 'click', function() {
      QSM_Logic.save();
    });
    $( '.logic-rules' ).on( 'click', '.add-new-if', function() {
      var $ifElement = $( this ).parent().parent();
      $( this ).parent().remove();
      QSM_Logic.addCondition( $ifElement );
    });
    $( '.logic-rules' ).on( 'click', '.add-new-then', function() {
      var $thenElement = $( this ).parent().parent();
      $( this ).parent().remove();
      QSM_Logic.addTrigger( $thenElement );
    });
    $( '.logic-rules' ).on( 'click', '.delete-if-row', function() {
      var $deleteElement = $( this );
      QSM_Logic.deleteCondition( $deleteElement );
    });
    $( '.logic-rules' ).on( 'click', '.delete-then-row', function() {
      var $deleteElement = $( this );
      QSM_Logic.deleteTrigger( $deleteElement );
    });
    $( '.logic-rules' ).on( 'click', '.delete-rule', function() {
      var $deleteElement = $( this );
      QSM_Logic.deleteRule( $deleteElement );
    });
    $( '.logic-rules' ).on( 'change', '.if-question', function() {
      QSM_Logic.changeQuestion( $( this ).val(), $( this ) );
    });
  });
}(jQuery));
