var delete_record_call;
var bulkRowAdd;
var en_coc_unique_cellRender;

(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_coc_unique_grid'),
    en_coc_unique_searchgrid = $$('en_coc_unique_searchgrid'),
    en_coc_unique_paginggrid = $$('en_coc_unique_paginggrid'),
    en_coc_unique_hot,
    // rowmap ={},
    firstInsert= 0,
    filtering=0,
    myData = [],
    noOfRowstoShow = 10000,
    // myData_charge_event_col_num = 0,
    recordnum = [];
    newrowArray = [],
    custodian_select_array = [];
    idsToAddBackIn = [];


jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_coc_unique',
        samplegroupid: urlParam('mysamplegroup')

    },
    function (res) {

        var data = JSON.parse(res);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. \n\nError: " + JSON.stringify(data.errors));
        } else {
            myData = data.allcols;
            buildHOT(data.colInfo);
            getgridData(myData, "1", 1000);
        }
        custodian_select_array = data.custodian_select_array;
        custodian_select_array[custodian_select_array.length] = "On Shelf";
    }
);

function buildHOT(icols) {
    en_coc_unique_hot = new Handsontable(container, {
        startRows: 0,
        startCols: 0,
        // fixedRowsTop: 2,
        autoRowSize: true,
        manualColumnResize: true,
        columns: GetColumnMeta(icols),
        colHeaders: GetHeaderNames(icols),
        manualRowResize: true,
        contextMenu: ['remove_row', 'undo', 'redo', 'row_above', 'row_below'],
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        className: "htCenter",
        fillHandle: true,
        // minSpareRows: 1,

        cells: function (row, col, prop) {
            return en_coc_unique_cellRender({row: row, col: col, prop: prop, myhot: this.instance});
        },
        beforeUndo:function(action){

            idsToAddBackIn = getIdsToUndo(action, en_coc_unique_hot);
        },

        beforeChange: function (changes, source) {
            // if user makes a gange that is equal to the current value do not bother making change... does NOT work for bulk changes
            if ((changes[0][2] == changes[0][3]) && (changes.length == 1)) {
                return false;
            }
            //we were getting an error when a blank was entered into the sample recipt dat time field. so if the entry is blank do not make change
            if (((changes[0][1].indexOf('time') >= 0) || (changes[0][1].indexOf('date') >= 0)) && (changes[0][3] == "")) {
                return false;
            }
        },

        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 0) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(en_coc_unique_hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                // if there is an instance where a client was created and not named.  Change myclient to '' so it can be found in the database

                jQuery.ajax({
                    url: ajax_sttrackorder_object.ajaxurl,
                    dataType: 'json',
                    async: false,
                    type: 'post',
                    data: {
                        action: "ajaxen_coc_unique_new",
                        amount: amount,
                        samplegroupid: urlParam('mysamplegroup'),
                        'addType':action,
                        'idsToAddBackIn': idsToAddBackIn,
                    },
                    success: function (data) {
                        if (data.error == true) {
                            alert("Sorry, we can't add a new row.  Upon refeshing the added record will NOT be present. \n\nError: " + JSON.stringify(data.errors));
                            hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                        } else {
                            for (var i = 0; i < data.newid.length; i++) {
                                //set email to R/W
                                //hot.setCellMeta(index + i - row_offset, 'user_email', 'valid', 'false');
                                en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                var newrow = {id: data.newid[i]};
                                myData.splice(myData.length, 0, newrow);
                                newSampleNumber = data.current_sample_number + i + 1;

                                if ((action == 'bulk_row_add')) {
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_reciept_dat_time', data.cur_time, 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'condition', '', 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index +1 - row_offset, 'id', data.newid[i], 'loadData');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_reciept_dat_time', data.cur_time, 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'custodian', data.user, 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_group_id', urlParam('mysamplegroup'), 'loadData');
                                    en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_number', String(newSampleNumber), 'auto_edit');
                            }
                                else if ((action != 'UndoRedo.undo')) {
                                    // en_coc_unique_hot.setDataAtRowProp(index, 'id', data.newid[i], 'loadData');
                                    en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_reciept_dat_time', data.cur_time, 'auto_edit');
                                    en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'custodian', data.user, 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_reciept_dat_time', '', 'auto_edit');
                                    // en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_group_id', urlParam('mysamplegroup'), 'loadData');
                                    en_coc_unique_hot.setDataAtRowProp(index + i - row_offset, 'sample_number', newSampleNumber, 'auto_edit');
                                }
                            }
                            hot_console.innerText = 'Row Inserted in DB';
                        }
                    }
                });
            }
            firstInsert++;
        },
        afterChange: function (change, source) {
            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }
            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=en_coc_unique_hot.getDataAtRowProp(change[i][0],'id');
                // change[i][5]=en_coc_main_hot.getDataAtRowProp(change[i][0],'user_id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_coc_unique_change',
                    'data': change,
                    'source': source,
                    samplegroupid: urlParam('mysamplegroup')
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the requested change. Upon refeshing the change you made will NOT be present. \n\nError: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';
                        }
                    }
                }
            });
        },

        beforeRemoveRow: function (index, amount) {

            // lastRow = en_coc_unique_hot.countRows() -1;
            // if (((index == lastRow) || (index + amount -1 == lastRow)) && (en_coc_unique_hot.getDataAtRowProp(lastRow, 'id') == null)) {
            //     alert("You CANNOT delete the blank entry row (last row)."); return false;}

            // index the row(s) to be deleted with the record ID
            recordnum = [];
            // if (index > 1) {
                i = 0;
                for (row = index; row <= index + (amount - 1); row++) {
                    recordnum[i] = en_coc_unique_hot.getDataAtRowProp(row, 'id');
                    i++;
                }
            shouldWeDeleteTheRow = delete_record(index, amount, recordnum);
            // }
        }
    });

}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_coc_unique_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            en_coc_unique_hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_coc_unique_paginggrid).append(element);
    }
    en_coc_unique_hot.loadData(part);
    return part;
}

en_coc_unique_cellRender  = function en_coc_unique_cellRender(parameters) {
    var row = parameters.row;
    // var col = parameters.col;
    var prop = parameters.prop;
    // var myhot = parameters.myhot;
    var cellProperties = {};

    //hide group row.  Used for coloring and permissions.
    // if (row === 0) {cellProperties.type = { renderer: BlackCellRenderer }; }

        switch(prop) {
            case "id":
                cellProperties.readOnly = true;
                break;
            case "sample_group_id":
                cellProperties.readOnly = true;
                break;
            case "sample_number":
                // cellProperties.readOnly = true; I learned this kills undo for this column
                break;
            case "condition":
            case "disposition_details":
                cellProperties.type = 'text';
                cellProperties.renderer = customCellRenderer;
                break;
            case "custodian":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source = custodian_select_array;
                break;
            case "location":
                //http://utilitymill.com/
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Sample Storage Room','Main Facility','Nissan Proving Grounds','Ford Proving Grounds', 'See Disposition Details'];
                break;

            case "item_description":
                //http://utilitymill.com/
                // utility/Regex_For_Range
                cellProperties.type = 'text';
                break;

            case "status":
                //http://utilitymill.com/
                // utility/Regex_For_Range
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Test not started', 'Testing Paused', 'In test', 'Testing Concluded'];
                break;

            default:
        }
        //Take care of Date columns
        if(typeof(prop) === 'string') {
            if (prop != null) {
                if (prop.indexOf("time") > -1) {
                    cellProperties.type = 'date';
                    cellProperties.dateFormat = 'M-D-YYYY h:mm a';
                    cellProperties.correctFormat = true;
                    cellProperties.allowInvalid = false;
                } else if (prop.indexOf("date") > -1) {
                    cellProperties.type = 'date';
                    cellProperties.dateFormat = 'MM-DD-YYYY';
                    cellProperties.correctFormat = true;
                }
            }


                if (prop != null) {
                    if ((prop.indexOf("collection") > -1) ||
                        (prop.indexOf("accession") > -1)){
                        cellProperties.readOnly = true;
                    }
                }
            }

    return cellProperties;
}

//Ajax call to server for record delete
delete_record_call = function delete_record_call(index, amount, recordnum) {
    return jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        type: "POST",
        async: false,
        data: {
            action: "ajaxen_coc_unique_deleterecord",
            amount: amount,
            index: index,
            id: recordnum,
            samplegroupid: urlParam('mysamplegroup')
        }
    }).responseText;
}

bulkRowAdd = function bulkRowAdd (){
    numberOfRowsToAdd = prompt("Please Specify the number of rows you would like to add.","0");
    if(numberOfRowsToAdd > 10){alert("The \"Bulk Row Add\" tool is limited to 10 rows at a time.")
    return false}

    if (numberOfRowsToAdd != null){en_coc_unique_hot.alter('insert_row', null, numberOfRowsToAdd, 'bulk_row_add');}

    }

})( jQuery );