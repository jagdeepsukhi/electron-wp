<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-coc-unique-form.php";
    wp_register_script('en-coc-unique-js', ISTO_URL . "/en-coc-unique/en-coc-unique.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-coc-unique/en-coc-unique.js'));
    wp_register_script('en-coc-unique-mobile-js', ISTO_URL . "/en-coc-unique/en-coc-unique-mobile.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-coc-unique/en-coc-unique-mobile.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_coc_unique_new', 'ajax_en_coc_unique_new');
    add_action('wp_ajax_ajaxen_coc_unique_change', 'ajax_en_coc_unique_change');
    add_action('wp_ajax_ajaxen_coc_unique', 'ajax_en_coc_unique');
    add_action('wp_ajax_ajaxen_coc_unique_mobile', 'ajax_en_coc_unique_mobile');
    add_action('wp_ajax_ajaxen_coc_unique_deleterecord', 'ajax_en_coc_unique_deleterecord');

    add_shortcode('en_coc_unique', 'en_coc_unique');
    function en_coc_unique($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');

        $output = en_coc_unique_form();
        return $output;
    }

function ajax_en_coc_unique(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("cc", "administrator"), $user->roles)) > 0) {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_coc_unique'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }

try {
	$wpdb->flush();
    $default_cols = $wpdb->get_results( $sql, OBJECT );
	if (false === $wpdb->result) {
	    $json['errors']['lookup'] = "Default Columns:  ".$wpdb->last_error;
	} else {

		//Get table data
		$sql    = "SELECT
              en_coc_unique.*
            FROM en_coc_unique
            WHERE en_coc_unique.sample_group_id = '%s'";
		$dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['samplegroupid'] ), OBJECT );
		$dbdata = make_date( $dbdata );

		if (false === $wpdb->result) {$json['errors']['lookup'] = "Dbdata Table Query Problem:  ".$wpdb->last_error;}

		// query selectable user names list for user name dropdown menu
		$sqlSelectUserName = "SELECT
                                      wp_users.display_name
                                    FROM wp_users
                                    WHERE wp_users.user_email LIKE '%intertek%'
                                    AND wp_users.user_email NOT LIKE '%DISABLED-USER%'
                                    ORDER BY wp_users.display_name";

		$custodian_select = $wpdb->get_results( $sqlSelectUserName, OBJECT );
		$custodian_select = make_date( $custodian_select );

		if (false === $wpdb->result) {$json['errors']['lookup'] = "Custodian_Select Query Problem:  ".$wpdb->last_error;}

		//convert $maintenance_items_selection into an array form that can be used in the cell render on the javasctipt side
		$itemrow                = 0;
		$custodian_select_array = [];
		foreach ( $custodian_select as $item ) {
			$custodian_select_array[ $itemrow ] = $item->display_name;
			$itemrow ++;
		}
	}
} catch (Exception $e) {
	$json['errors']['initial'] = $e->getMessage();
}

    //Send Response
	$json['colInfo'] = $default_cols;
    $json['allcols'] = $dbdata;
    $json['custodian_select_array'] = $custodian_select_array;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_coc_unique_mobile(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    global $current_user;
    $lastOdo=$current_user;

    $user = $current_user->data->display_name;

    // Get current sample information
    $sql = "SELECT
                  en_coc_unique.custodian,
                  en_coc_unique.location,
                  en_coc_unique.condition,
                  en_coc_unique.status
                FROM en_coc_unique
                WHERE en_coc_unique.id = '%s'";

    $currentSampleStatusInfo = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['uniquesampleid']), OBJECT );

    $json['custodian'] =  $user;
    $json['prior_custodian'] =  $currentSampleStatusInfo[0]->custodian;
    $json['sample_location'] =  $currentSampleStatusInfo[0]->location;
    $json['prior_condition'] =  $currentSampleStatusInfo[0]->condition;
    $json['status'] =  $currentSampleStatusInfo[0]->status;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_coc_unique_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );
    //Handle each change
    $cur_time = current_time( 'mysql' );


    //Go get the highest sample ID # so you you can assign sample IDs to the new samples being added
    $sql = "SELECT
                MAX(en_coc_unique.sample_number) AS MaxSampleNumber
                FROM en_coc_unique
                WHERE en_coc_unique.sample_group_id = '%s'";
    $currentMaxSampleNumber = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['samplegroupid'], OBJECT ))[0]->MaxSampleNumber;

	if (false === $wpdb->result) {$json['errors']['lookup'] = "Last Sample Number query had an issue:  ".$wpdb->last_error;}

	//As a security measure make sure someone is NOT trying to crash the DB with bulk entries
	$sqlEntryRate = "SELECT
                COUNT(en_coc_history.field) AS entryRate
                FROM en_coc_history
                WHERE en_coc_history.date >= DATE_SUB(NOW(), INTERVAL 60 SECOND)
                AND en_coc_history.after = 'NEW'
                AND (en_coc_history.source = 'auto'
                OR en_coc_history.source = 'Autofill.fill')";
	$entryRate = $wpdb->get_results( $sqlEntryRate, OBJECT );
	if ($wpdb->result === false) {
		$json['errors']['lookup'] = "Entry Rate Checker had an issue:  ".$wpdb->last_error;
	}

    for ($i = 0; $i < $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
            // Limit number of rows endered at any one time to 100.
            if (($_POST['amount'] > 100) || $entryRate[0]->entryRate > 25 || count($json['errors']) > 0 ) {
                $dbresult = false;
            }else {
                if (($_POST['addType'] != "UndoRedo.undo")) {
                    $dbresult = $wpdb->insert('en_coc_unique',
                        array(
//                        'custodian' => $current_user->data->display_name,
//                        'sample_reciept_dat_time' => $cur_time,
                            'sample_group_id' => (int)$_POST['samplegroupid']
//                        'sample_number' => $sampleNumberAssignment
                        )
                    );
                }else {
                    $dbresult = $wpdb->insert('en_coc_unique',
                        array(
                            'sample_group_id' => (int)$_POST['samplegroupid'],
                            'id' => $_POST['idsToAddBackIn'][$i]
                        )
                    );
                }

                $new_id[] = $wpdb->insert_id;

//                Check DB results
	            if ((0 === $dbresult) || (false === $dbresult) ||  $wpdb->result === false) {
		            $json['errors']['lookup'] = "Insert Row: ".$wpdb->last_error;
		            $json[errors][$i] = true;
	            }
            }

	        if (count($json['errors']) == 0){
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $new_id[$i],
                    'source_table' => 'Unique Sample',
                    'group_id' => $_POST['samplegroupid']
                );
                $wpdb->insert( 'en_coc_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['newid'] = $new_id;
    $json['user'] = $current_user->data->display_name;
    $json['current_sample_number'] = (int)$currentMaxSampleNumber;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_coc_unique_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );

//    // First check the nonce, if it fails the function will break
//    check_ajax_referer( 'ajax-steditorderdetails-nonce', 'security' );

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_coc_unique'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    $dbindex = 0;
//See if they are allowed to delete this record
////Make the deletion

    foreach ($_POST['id'] as $id) {

        if ((allowed_to_change('id', $rolemap, $user->roles))){
            try {
                $wpdb->flush();
                $sql = "DELETE FROM en_coc_unique
	            WHERE id = '%s'";
                $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
	            if ((0 === $dbresult) || (false === $dbresult) ||  $wpdb->result === false) {
		            $json['errors']['lookup'] = "DbResult query had an issue:  ".$wpdb->last_error;
                    $json[errors][$dbindex] = true;
                }
	            //Log the deletion if no errors were detected
	            if (count($json['errors']) == 0) {
	                $dbdata = array(
		                'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
		                'field' => 'RECORD DELETED',
		                'before' => 'Active',
		                'after' => 'Deleted',
		                'source' => 'Delete',
		                'table_id' => $id,
		                'source_table' => 'Unique Sample',
		                'group_id' => $_POST['samplegroupid']

	                );
	                $wpdb->insert('en_coc_history', $dbdata);
                }
            } catch (Exception $e) {
                $json[errors][$dbindex] = $e->getMessage();
            }
        }
        else {
            //user did not have permissiong to make deletion
            $json[errors][$dbindex] = 'role';
        }

        $dbindex++;
    }

    //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}


function ajax_en_coc_unique_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_coc_unique'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {


        $logit = true;
        //skip ID change column notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles)) ){
                //Make the change
                try {
                    $wpdb->flush();
                    $table = "en_coc_unique";
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]));
	                if ((false === $dbresult) ||  $wpdb->result === false) {
		                $json['errors']['lookup'] = "DbResult Update Problem:  ".$wpdb->last_error;
                        $json[errors][$dbindex] = true;
                    } else {
	                    //Log the change
	                    if ( $logit) {
		                    $dbdata = array(
			                    'user'         => $current_user->user_firstname . ' ' . $current_user->user_lastname,
			                    'field'        => $change[1],
			                    'before'       => $change[2],
			                    'after'        => $change[3],
			                    'source'       => $_POST['source'],
			                    'table_id'     => $change[4],
			                    'source_table' => 'Unique Sample',
			                    'group_id'     => $_POST['samplegroupid']
		                    );
		                    $wpdb->insert( 'en_coc_history', $dbdata );
	                    }
                    }
                } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = 'role';
            }
        }
        //user tried to change record "id" which is NEVER permissible unless its performed by "Undo"
        else{
	        //undo needs to be able to add the deleated ID back in and therefore needs access to change it
	        if ($_POST['source'] != 'UndoRedo.undo'){
		        $json[errors][$dbindex] = true;
            }
        }
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>
