<?php


function en_coc_unique_form()
{
    global $current_user;
    global $wpdb;
    $sampleGroupID = $_GET['mysamplegroup'];
    $myprojectid = $_GET["projectID"];
  //  $myclient = $_GET["myclient"];
    $uniqueSampleID = $_GET["uniquesampleid"];
    ob_start();

    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'cc'))) {
                if ( wp_is_mobile() == false ){
                    wp_enqueue_script('en-coc-unique-js');

                    ?>
                    <h3>Unique Items</h3>
                    <h5></h5>
                    <h5>Project ID:  <?php echo $myprojectid; ?></h5>
                    <h5>Group ID:  <?php echo $sampleGroupID; ?></h5>
                    <button type="button" name="row_add_button" id = "row_add_button" value = "true" onclick="bulkRowAdd()">Bulk Row Add</button><BR>
                    <div class="en_coc_unique_form">
                      <!--  Search: <input id="en_coc_unique_searchgrid" type="text" />  -->
                        <div id="en_coc_unique_grid" class="dataTable"></div>
                       <!-- <div id="en_coc_unique_paginggrid" class="pagination"></div> -->
                    </div> <!--en_coc_unique_form-->

                    <a name="BOTTOM"></a>
                    <?php

                }
                else {
                    //Go get project id and client name to confirm they are recognized in the db.  also this info is used to populat the header in the mobile form
                    $sql = "SELECT
                                  en_coc_unique.custodian,
                                  en_coc_unique.location,
                                  en_coc_unique.status,
                                  en_coc_unique.item_description,
                                  en_coc_main.project_id,
                                  en_coc_main.client_name,
                                  en_coc_unique.sample_number,
                                  en_coc_main.id 
                                FROM en_coc_main
                                  INNER JOIN en_coc_unique
                                    ON en_coc_main.id = en_coc_unique.sample_group_id
                                WHERE en_coc_unique.id = '%s'";
                    $samlpeInfo = $wpdb->get_results( $wpdb->prepare( $sql, $uniqueSampleID, OBJECT ));

                    if(($samlpeInfo[0]->project_id == null)||($samlpeInfo[0]->client_name == null)){ ?>
                        <h3>Error!!!!</h3><br>
                        <h5>This item is NOT associated with a Project ID or a Client or both.<br><br>  Check that this
                        item is assigned to the proper client and project.</h5>
                    <?php } else {

                        ?>
                        <h3>Update Sample Status</h3>
                        <h5></h5>
                        <h5>Client: <?php echo $samlpeInfo[0]->client_name; ?></h5>
                        <h5>Project ID: <?php echo $samlpeInfo[0]->project_id; ?></h5>
                        <h5>Group #: <?php echo $samlpeInfo[0]->id; ?></h5>
                        <h5>Item #: <?php echo $samlpeInfo[0]->sample_number; ?></h5>
<!--                        "Manufacturer ID" formatted in "st-Track-order.css-file-->
                        <p id="field">Manufacturer ID: <span id='secondText'><?php echo $samlpeInfo[0]->item_description; ?></span></p>
                        <?php
                        wp_enqueue_script('en-coc-unique-mobile-js');
                        wd_form_maker(13);
                    }

                }
            }
            else {
                ?>
                <div class="en_coc_unique_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_coc_unique_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_coc_unique_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_coc_unique_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

?>