var user;
var

    $$ = function(id) {
        return document.getElementById(id);
    },
    en_coc_unique_container = $$('en_coc_unique_mobile_grid'),
    en_coc_unique_searchgrid = $$('en_coc_unique_searchgrid'),
    en_coc_unique_paginggrid = $$('en_coc_unique_paginggrid'),
    //autosave = $$('autosave'),
    en_coc_unique_autosaveNotification,
    en_coc_unique_hot,
    wait_for_data,
    rowmap ={},
    firstInsert= 0,
    lowCoveredMiles, highCoveredMiles;
    myData = [],
    noOfRowstoShow = 5,


jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_coc_unique_mobile',
        'uniquesampleid': urlParam('uniquesampleid')
    },
    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
         var custodian = data.custodian;
         var sample_location = data.sample_location;
         var prior_custodian = data.prior_custodian;
        var prior_condition = data.prior_condition;
         var status = data.status;
        unique_sample_id = data.unique_sample_id;


        /* The jQuery below triggers upon completion of the mobile web page load. "myselect" is populated with the object in the
         * vehicleID field.  myselect.value is then populated with the vehicle's id number. This populates vehicle id and
         * record id in the mobile app so it can be recorded upon uploading event data*/

       jQuery(document).ready(function($) {
          var myUniqueSampleId = document.getElementsByName('unique_sample_id')[0];
            if(typeof myUniqueSampleId !== 'undefined') {

                myUniqueSampleId.value = urlParam('uniquesampleid');
                    jQuery("select[electronid='sample_disposition_status']").val(status);
                    jQuery("select[electronid='sample_location']").val(sample_location);
                    jQuery("select[electronid='custodian']").append( new Option(custodian)).val(custodian);
                    jQuery("input[electronid='prior_custodian']").val(prior_custodian);
                    jQuery("input[electronid='prior_location']").val(sample_location);
                    jQuery("input[electronid='prior_status']").val(status);
                    jQuery("input[electronid='prior_condition']").val(prior_condition);
                    jQuery("textarea[electronid='condition']").val(prior_condition);
                   }
       });
    }
);

function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}
