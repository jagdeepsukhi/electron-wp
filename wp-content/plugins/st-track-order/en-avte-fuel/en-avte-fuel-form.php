<?php


function en_avte_fuel_form()
{
    global $wpdb;
    global $current_user;
    $car = $_GET['mycar'];
    ob_start();

    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) {
                if ( wp_is_mobile() == false ){
                    wp_enqueue_script('en-avte-fuel-js');

                    //Go get the vehicle id for each fueled vechicle
                    $sql = "SELECT
                              en_avte_cars.vehicle_id
                            FROM en_avte_cars
                            WHERE en_avte_cars.TYPE = 'PHEV'
                            OR en_avte_cars.TYPE = 'Gas'
                            OR en_avte_cars.TYPE = 'CNG'
                            OR en_avte_cars.TYPE = 'Diesel'
                            OR en_avte_cars.TYPE = 'HEV'
                            OR en_avte_cars.TYPE = 'Gasoline/CNG'
                            ORDER BY en_avte_cars.vehicle_id";
                    $chargeable_vehicles = $wpdb->get_results( $sql, OBJECT );

                    //Build vehicle list for "Find Another Vechile" Field
                    $i=0;
                    $vehicleHtmlString = "";
                    foreach($chargeable_vehicles AS $vehiclehtml) {
                        if ($i != 0){$vehicleHtmlString= $vehicleHtmlString . ", ";
                        }
                        $vehicleHtmlString = $vehicleHtmlString . "'" . $vehiclehtml->vehicle_id . "'";
                        $i++;
                    }

                    ?>
                    <!--Form Heaher-->
                    <h3><?php echo $car; ?></h3>
                    <A HREF="#BOTTOM">Go to bottom of page</A><BR>

                    <!--"Find Another Vehicle" Text Box-->
                      <meta charset="utf-8">
                      <meta name="viewport" content="width=device-width, initial-scale=1">
                      <title>jQuery UI Autocomplete - Default functionality</title>
                      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<!--                      <link rel="stylesheet" href="/resources/demos/style.css">-->
                      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                      <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                      <script>
                          $( function() {
                            var availableTags =

                           [ <?php echo $vehicleHtmlString; ?> ]

                            $( "#tags" ).autocomplete({
                              source: availableTags
                            });
                          } );
                      </script>

                      <div class="ui-widget">
                        <label for="tags">Find Another Vehicle: </label>
                        <input id="tags" onKeyDown="if(event.keyCode==13);" value = "" placeholder = "Type Here">
                      </div>

                    </select>
                    <div class="en_avte_fuel_form">
                      <!--  Search: <input id="en_avte_fuel_searchgrid" type="text" />  -->
                        <div id="en_avte_fuel_grid" class="dataTable"></div>
                       <!-- <div id="en_avte_fuel_paginggrid" class="pagination"></div> -->
                    </div> <!--en_avte_fuel_form-->


                    <h3 style="margin-top: 200px;"><?php echo $car; ?></h3>

                    <a name="BOTTOM"></a>
                    <?php

                }
                else {
                    ?>
                    <h3> Vehicle ID: <?php echo $car; ?></h3>
                    <?php
                    wp_enqueue_script('en-avte-fuel-mobile-js');
                    wd_form_maker(10);
                    ?>
                    <h3><?php echo $car; ?></h3>
                    <div id="en_avte_fuel_mobile_grid" class="dataTable"></div>
                    <h3 style="margin-top: 50px;"><?php echo $car; ?></h3>
                    <?php
                }

            } else {
                ?>
                <div class="en_avte_fuel_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_fuel_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_fuel_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_fuel_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>