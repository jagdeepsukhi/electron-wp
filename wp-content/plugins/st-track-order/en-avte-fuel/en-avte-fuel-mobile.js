
(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_fuel_mobile_grid'),
    en_avte_fuel_searchgrid = $$('en_avte_fuel_searchgrid'),
    en_avte_fuel_paginggrid = $$('en_avte_fuel_paginggrid'),
    hot,
    lowmpg, highmpg, newid;
    myData = [],
    noOfRowstoShow = 5,



jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_fuel_mobile',
        'car': urlParam('mycar')
    },

        function (res) {
            //alert('The server responded: ' + res);  //for debug - only works in google chrome
            var data = JSON.parse(res);
            if (data.error == true) {
                alert("Sorry, there were errors loading the table. Error: " + JSON.stringify(data.errors));
            } else {
                var data = JSON.parse(res);
                myData = ObjToArr(data.allcols);
                lowmpg = Number(data.lowmpg);
                highmpg = Number(data.highmpg);
                newid = data.newid;
                lastodo = data.lastodo;
                currentdate = data.currentdate;
                vehicleFuelType = data.vehicleFuelType;
                buildHOT(data.allcols);
                getgridData(data.allcols, "1", 7);
            }

        /* The jQuery below triggers upon completion of the mobile web page load. "myselect" is populated with the object in the
         * vehicleID field.  myselect.value is then populated with the vehicle's id number. This populates vehicle id and
         * record id in the mobile app so it can be recorded upon uploading event data*/

        jQuery(document).ready(function($) {
            var myselectVehicleId = document.getElementsByName('vehicleID')[0];
                if(typeof myselectVehicleId !== 'undefined') {

                    myselectVehicleId.value = urlParam('mycar');
                    var myselectChargeDate =jQuery("input[class='wdform-date']").val();
                    jQuery("input[class='wdform-date']").val(currentdate);
                    var myselectCurrnetDate =jQuery("input[electronid='current_date']").val();
                    jQuery("input[electronid='current_date']").val(currentdate);
                    var myselectLastOdo = document.getElementsByName('last_odometer_for_mobile_functionality')[0];
                    myselectLastOdo.value = lastodo;
                    var myselecthigh_mpg = document.getElementsByName('high_mpg')[0];
                    myselecthigh_mpg.value = highmpg;
                    var myselectlow_mpg = document.getElementsByName('low_mpg')[0];
                    myselectlow_mpg.value = lowmpg;
                    var myselectverificationtype = document.getElementsByName('verification_type')[0];
                    myselectverificationtype.value = 'fuel';
                }

        });

        // Auto populate fuel for each vehicle and hide all fields if vehicle in an "EV" ... Could not get "switch statement to work, so used conditionals.
                    if((vehicleFuelType == "Gas")|| (vehicleFuelType == "PHEV")||(vehicleFuelType == "HEV")) {
                       // jQuery("#wdform_5_element10").val('Gas');
                        jQuery("select[electronid='fueltype']").val('Gas');
                        jQuery("option[value='CNG']").hide();
                        jQuery("option[value='Diesel']").hide();
                    }
                    if((vehicleFuelType == "CNG")) {
                        jQuery("select[electronid='fueltype']").val(vehicleFuelType);
                        jQuery("option[value='Gas']").hide();
                        jQuery("option[value='Diesel']").hide();
                    }
                    if((vehicleFuelType == "Diesel")) {
                        jQuery("select[electronid='fueltype']").val(vehicleFuelType);
                        jQuery("option[value='Gas']").hide();
                        jQuery("option[value='CNG']").hide();
                    }
                    if((vehicleFuelType == "EV")) {
                        jQuery("div[class='wdform_section']").hide();
                        alert("Fuel form is not applicable to this vechicle.")
                    }
                    if((vehicleFuelType == "Gasoline/CNG")) {
                        jQuery("option[value='Diesel']").hide();
                    }

    }
);

function buildHOT(idata) {
    hot = new Handsontable(container, {
        columns: GetHeaders(idata),
        startRows: 0,
        startCols: 0,
        fixedRowsTop: 2,
        readOnly: true,
        //stretchH: 'all',
        cells: function (row, col, prop) {
            return en_avte_fuel_cellRender(row, col, prop, this.instance);
        },
     });
}


function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_fuel_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_fuel_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_fuel_cellRender(row, col, prop, myhot) {
    var cellProperties = {};

    //hide columns
    if ((myhot.getDataAtCell(1, col) == "id") ||
        (myhot.getDataAtCell(1, col) == "Vehicle ID"))
    { cellProperties.type = {renderer: hiddenRowRender} }

    //hide group row.  Used for coloring and permissions.
    if (row === 0) {cellProperties.type = { renderer: hiddenRowRender }; }

    //color the second row, which is the friendly field name
    else if ((row == 1) && !((myhot.getDataAtCell(1, col) == "id") ||
        (myhot.getDataAtCell(1, col) == "Vehicle ID"))) {
        if (myhot.getDataAtCell(0, col) == "dr"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }

    else {
        switch(prop) {
            case "fuel_type":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Gas','CNG','Diesel',''];
                break;
            case "mpg":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.readOnly = true;
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0.0';
                // cellProperties.validator = mpgValidator;
                cellProperties.allowInvalid = true;
                cellProperties.renderer = mpgRenderer;
                break;
            case "fuel_cost":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '$0,0.00';
                cellProperties.validator = /^(\s*|(^[0-9]+(\.[0-9]{1,2})?$)|(^(\.[0-9]{1,2})?$))?$/;
                cellProperties.allowInvalid = false;
                break;
            case "gallons":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0.000';
                cellProperties.validator = /^(\s*|(^[0-9]+(\.[0-9]{1,3})?$)|(^(\.[0-9]{1,3})?$))?$/;
                cellProperties.allowInvalid = false;
                break;
            case "covered_miles":
                cellProperties.readOnly = true;

            case "odometer":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/ ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;

            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                //cellProperties.type = 'date';
                //cellProperties.dateFormat = 'MM-DD-YYYY HH:MM:SS';
                //cellProperties.correctFormat = false;
                //cellProperties.defaultDate = '01/18/2020';
            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = false;
            }
        }
        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }
    }

    return cellProperties;
}

function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}

// mpgValidator = function (value, callback) {
//     setTimeout(function(){
//         //change value to lowmpg and highmpg
//         if ((Number(value) >= (lowmpg)) && (Number(value) <= (highmpg))) {
//             callback(true);
//             }
//         else {
//
//            callback(false);
//             }
//         }, 1000);
//     }

//render the cell contents for MPG.  If the mpg is out of range the cell contents are red and bold.  If within range the contents are black and non-bold
function mpgRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);

    if( (value !== null)) {
        if (((Number(value) <= (lowmpg)) || (Number(value) >= (highmpg))) || (Number(value) <= 0)) {

            td.style.fontWeight = 'bold';
            td.style.color = 'blue';
            td.style.background = 'Red';
        }
    }
}

})( jQuery );
