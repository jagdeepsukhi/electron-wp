<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-fuel-form.php";
    wp_register_script('en-avte-fuel-js', ISTO_URL . "/en-avte-fuel/en-avte-fuel.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-fuel/en-avte-fuel.js'));
    wp_register_script('en-avte-fuel-mobile-js', ISTO_URL . "/en-avte-fuel/en-avte-fuel-mobile.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-fuel/en-avte-fuel-mobile.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_avte_fuel_new', 'ajax_en_avte_fuel_new');
    add_action('wp_ajax_ajaxen_avte_fuel_change', 'ajax_en_avte_fuel_change');
    add_action('wp_ajax_ajaxen_avte_fuel', 'ajax_en_avte_fuel');
    add_action('wp_ajax_ajaxen_avte_fuel_mobile', 'ajax_en_avte_fuel_mobile');
    add_action('wp_ajax_ajaxen_avte_fuel_deleterecord', 'ajax_en_avte_fuel_deleterecord');

    add_shortcode('en_avte_fuel', 'en_avte_fuel');
    function en_avte_fuel($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');

        $output = en_avte_fuel_form();
        return $output;
    }

function ajax_en_avte_fuel(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_fuel'
                    ORDER BY st_default_cols.`order`";
            }

            else {
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_fuel' AND st_default_cols.group = 'DR'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT
                en_avte_fuel.id,
                en_avte_fuel.car_id,
                en_avte_fuel.fuel_date,
                en_avte_fuel.gallons,
                en_avte_fuel.odometer,
                @lastOdo AS Last_Odo,
              IF(en_avte_fuel.driver_id = 'Admin',NULL,CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED)) AS covered_miles,
              IF(en_avte_fuel.driver_id = 'Admin',NULL, ROUND((CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED))/en_avte_fuel.gallons,1)) AS mpg,
                IF(en_avte_fuel.driver_id = 'Admin',@lastOdo = @lastOdo,@lastOdo:= en_avte_fuel.odometer),
                en_avte_fuel.fuel_type,
                en_avte_fuel.fuel_cost,
                en_avte_fuel.driver_id,
                en_avte_fuel.verify_date,
                en_avte_fuel.notes,
                en_avte_fuel.create_time
              FROM en_avte_fuel,
                ( select @lastOdo := 0) SQLVar
              WHERE en_avte_fuel.car_id = '%s'
              ORDER BY en_avte_fuel.fuel_date, en_avte_fuel.odometer";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );
    $dbdata = make_date($dbdata);
    $col_plus_data = array_merge ($transposed, $dbdata);

    // Get acceptable mpg range for vehicle w/in +/-2 stddev
    $sql = "SELECT
              AVG(STDCalc.mpg) AS Average,
              AVG(STDCalc.mpg) + 2 * STDDEV(STDCalc.mpg) as highmpg,
              AVG(STDCalc.mpg) - 2 * STDDEV(STDCalc.mpg) as lowmpg
              FROM(
            SELECT
                 @lastOdo AS Last_Odo,
              CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED) AS covered_miles,
              (CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED))/en_avte_fuel.gallons AS mpg,
                @lastOdo:= en_avte_fuel.odometer

              FROM en_avte_fuel,
                ( select @lastOdo := 0) SQLVar
              WHERE en_avte_fuel.car_id = '%s'
              AND en_avte_fuel.driver_id <> 'Admin'
              ORDER BY en_avte_fuel.fuel_date, en_avte_fuel.odometer) STDCalc";
    $mpgrange = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );


    //Send Response
    $json['lowmpg'] =  $mpgrange[0]->lowmpg;
    $json['currentUser'] =  $user->roles;
    $json['highmpg'] = $mpgrange[0]->highmpg;
    $json['allcols'] =$col_plus_data;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_fuel_mobile(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    global $current_user;
    $lastOdo=0;


    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0)  {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_fuel'
                    ORDER BY st_default_cols.`order`";
            }

            else {
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_fuel' AND st_default_cols.group = 'DR'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT*
                FROM(SELECT *
                FROM(SELECT
                en_avte_fuel.id,
                en_avte_fuel.car_id,
                en_avte_fuel.fuel_date,
                en_avte_fuel.gallons,
                en_avte_fuel.odometer,
                @lastOdo AS Last_Odo,
              CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED) AS covered_miles,
                ROUND((CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED))/en_avte_fuel.gallons,1) AS mpg,
                IF(en_avte_fuel.driver_id = 'Admin',@lastOdo = @lastOdo,@lastOdo:= en_avte_fuel.odometer),
                en_avte_fuel.fuel_type,
                en_avte_fuel.fuel_cost,
                en_avte_fuel.driver_id,
                en_avte_fuel.verify_date,
                en_avte_fuel.notes,
                en_avte_fuel.create_time
              FROM en_avte_fuel,
                ( select @lastOdo := 0) SQLVar
              WHERE en_avte_fuel.car_id = '%s'
              ORDER BY en_avte_fuel.fuel_date, en_avte_fuel.odometer) AllFuel
              ORDER BY AllFuel.fuel_date Desc, AllFuel.odometer DESC LIMIT 5) last_5
              ORDER BY last_5.fuel_date, last_5.odometer";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );
    $dbdata = make_date($dbdata);
    $lastOdo = end($dbdata)->odometer;
    $col_plus_data = array_merge ($transposed, $dbdata);


    // Get acceptable mpg range for vehicle w/in +/-2 stddev
    $sql = "SELECT
              AVG(STDCalc.mpg) AS Average,
              AVG(STDCalc.mpg) + 2 * STDDEV(STDCalc.mpg) as highmpg,
              AVG(STDCalc.mpg) - 2 * STDDEV(STDCalc.mpg) as lowmpg
              FROM(
            SELECT
                 @lastOdo AS Last_Odo,
              CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED) AS covered_miles,
              (CAST(en_avte_fuel.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED))/en_avte_fuel.gallons AS mpg,
                @lastOdo:= en_avte_fuel.odometer

              FROM en_avte_fuel,
                ( select @lastOdo := 0) SQLVar
              WHERE en_avte_fuel.car_id = '%s'
              AND en_avte_fuel.driver_id <> 'Admin'
              ORDER BY en_avte_fuel.fuel_date, en_avte_fuel.odometer) STDCalc";
    $mpgrange = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );

    // Get vehicle Fuel Type
    $sql = "SELECT
              en_avte_cars.TYPE
            FROM en_avte_cars
            WHERE en_avte_cars.vehicle_id = '%s'";

    $vehicleFuelType = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );


     // finalize data points that need to be sent to the mobile app for processing
    $curtime =current_time( 'mysql' );
    $cur_date = date_format(date_create_from_format('Y-m-d H:i:s', $curtime ), 'Y-m-d');



    //Send Response
    $json['currentdate'] =  $cur_date;
    $json['vehicleFuelType'] =  $vehicleFuelType[0]->TYPE;
    $json['lastodo'] =  $lastOdo;
    $json['lowmpg'] =  $mpgrange[0]->lowmpg;
    $json['highmpg'] = $mpgrange[0]->highmpg;
    $json['allcols'] =$col_plus_data;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}


function ajax_en_avte_fuel_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

//    // First check the nonce, if it fails the function will break
//    check_ajax_referer( 'ajax-steditorderdetails-nonce', 'security' );

    //Handle each change
    $cur_time = current_time( 'mysql' );

    for ($i = 0; $i < $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
            if ($_POST['addType'] != "UndoRedo.undo") {
                $dbresult = $wpdb->insert('en_avte_fuel',
                    array(
                        'create_time' => $cur_time,
                        'car_id' => $_POST['car'],
                    )
                );
            } else{
                $dbresult = $wpdb->insert('en_avte_fuel',
                    array(
                        'id' => $_POST['idsToAddBackIn'][$i],
                        'car_id' => $_POST['car']
                    )
                );
            }

            $new_id[] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][$i] = true;
            } else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $new_id[$i],
                    'car_id' => $_POST['car']
                );
                $wpdb->insert( 'en_avte_fuel_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['newid'] = $new_id;
    $json['user'] = $current_user->data;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_fuel_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );
    $table = "fuel";

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_fuel'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    $dbindex = 0;

    //See if they are allowed to delete this record
    ////Make the deletion
    foreach ($_POST['id'] as $id) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified ($table, $id);

        if ((allowed_to_change('odometer', $rolemap, $user->roles)) && ((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")))){
            try {
                $wpdb->flush();
                $sql = "DELETE FROM en_avte_fuel
	            WHERE id = '%s'";
                $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
                if ((0 === $dbresult) || (false === $dbresult)) {

                    $json[errors][$dbindex] = true;

                }

//                //Log the deletion
            $dbdata = array(
                'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                'field' => 'RECORD DELETED',
                'before' => 'Active',
                'after' => 'Deleted',
                'source' => 'Delete',
                'table_id' => $id,
                'car_id' => $_POST['car']
            );
            $wpdb->insert('en_avte_fuel_history', $dbdata);

            } catch (Exception $e) {
                $json[errors][$dbindex] = $e->getMessage();
            }
        }

        else {
            //user did not have permissiong to make deletion
            $json[errors][$dbindex] = 'role';
        }

        $dbindex++;
    }

    //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_fuel_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );
    $table = 'fuel';
    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_fuel'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified ($table, $change[4]);

        // has the time period the user is trying to enter a record into already been reported on.  If so ONLY allow "fm" users to make the change
        $timeFrameAlreadyVerified = hasTimePeriodAlreadyBeenReported($table, $change, $current_user, $cur_time);

        $logit = true;
        //skip ID change notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles)) && (((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")) && $timeFrameAlreadyVerified == false)
                    || ((count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) && (($change[1] == "verify_date")||($change[1] == "notes"))))){
                //Make the change
                try {
                    $wpdb->flush();
                    $table = "en_avte_fuel";
                    //START transaction
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]) );
                    if ( false === $dbresult ) {
                        //throw new Exception('Division by zero.');

                            $json[errors][$dbindex] = true;

                    }


                //Log the change
                if ($logit) {
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => $change[1],
                        'before' => $change[2],
                        'after' => $change[3],
                        'source' => $_POST['source'],
                        'table_id' => $change[4],
                        'car_id' => $_POST['car']
                    );
                    $wpdb->insert('en_avte_fuel_history', $dbdata);
                }
                //log stuff goes here
                //commit
                } catch (Exception $e) {
                    //rollback
                    $json[errors][$dbindex] = $e->getMessage();
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = 'role';
            }
        }
        //user tried to change record "id" which is NEVER permissible
        else{ $json[errors][$dbindex] = true;}
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>


