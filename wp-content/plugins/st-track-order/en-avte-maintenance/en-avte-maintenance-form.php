<?php

function uploader_form() {

	ob_start();
	?>
    <div class="container">

        <!-- The file upload form used as target for the file upload widget -->
        <form id="fileupload" action="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/server/php/" method="POST" enctype="multipart/form-data">
            <!-- Redirect browsers with JavaScript disabled to the origin page -->
            <noscript><input type="hidden" name="redirect" value="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/"></noscript>
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar">
                <div class="col-lg-7">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="glyphicon glyphicon-upload"></i>
                        <span>Start upload</span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-ban-circle"></i>
                        <span>Cancel upload</span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="glyphicon glyphicon-trash"></i>
                        <span>Delete</span>
                    </button>
                    <input type="checkbox" class="toggle">
                    <!-- The global file processing state -->
                    <span class="fileupload-process"></span>
                </div>
                <!-- The global progress state -->
                <div class="col-lg-5 fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    <!-- The extended global progress state -->
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form>
    </div>
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
	<?php

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


function en_avte_maintenance_form()
{
	global $current_user;
	global $wpdb;
	$car = $_GET['mycar'];
	$uploaddoc = $_GET['uploaddoc'];
	ob_start();

	if ( is_user_logged_in() ) {
		$user = $current_user;
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			if (doesUserHavePermissionToEnterPage($user, array('administrator', 'fm'))) {
				wp_enqueue_script('en-avte-maintenance-js');
//                    wp_enqueue_script('en-avte-maintenance-ui-widget-js');
//                    wp_enqueue_script('en-avte-maintenance-iframe-transport-js');
//                    wp_enqueue_script('en-avte-maintenance-fileupload-js');

				//Go get the vehicle id for each vehicle
				$sql = "SELECT
                              en_avte_cars.vehicle_id
                            FROM en_avte_cars
                            ORDER BY en_avte_cars.vehicle_id";
				$chargeable_vehicles = $wpdb->get_results( $sql, OBJECT );

				//Build vehicle list for "Find Another Vechile" Field
				$i=0;
				$vehicleHtmlString = "";
				foreach($chargeable_vehicles AS $vehiclehtml) {
					if ($i != 0){$vehicleHtmlString= $vehicleHtmlString . ", ";
					}
					$vehicleHtmlString = $vehicleHtmlString . "'" . $vehiclehtml->vehicle_id . "'";
					$i++;
				}

				?>
                <!--Form Header-->
                <h3><?php echo $car; ?></h3>



                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Bootstrap styles -->
                <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
                <!-- Generic page styles -->
                <link rel="stylesheet" href="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/css/style.css">
                <!-- blueimp Gallery styles -->
                <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
                <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
                <link rel="stylesheet" href="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/css/jquery.fileupload.css">
                <link rel="stylesheet" href="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/css/jquery.fileupload-ui.css">
                <!-- CSS adjustments for browsers with JavaScript disabled -->
                <noscript><link rel="stylesheet" href="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/css/jquery.fileupload-noscript.css"></noscript>
                <noscript><link rel="stylesheet" href="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/css/jquery.fileupload-ui-noscript.css"></noscript>





                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
                <!-- The Templates plugin is included to render the upload/download listings -->
                <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
                <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
                <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
                <!-- The Canvas to Blob plugin is included for image resizing functionality -->
                <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
                <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
                <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
                <!-- blueimp Gallery script -->
                <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
                <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
                <!-- The basic File Upload plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload.js"></script>
                <!-- The File Upload processing plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-process.js"></script>
                <!-- The File Upload image preview & resize plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-image.js"></script>
                <!-- The File Upload audio preview plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-audio.js"></script>
                <!-- The File Upload video preview plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-video.js"></script>
                <!-- The File Upload validation plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-validate.js"></script>
                <!-- The File Upload user interface plugin -->
                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/jquery.fileupload-ui.js"></script>
                <!-- The main application script -->
<!--                <script src="/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/js/main.js"></script>-->
                <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
                <!--[if (gte IE 8)&(lt IE 10)]>
                <script src="js/cors/jquery.xdr-transport.js"></script>
                <![endif]-->




                <button type="button" name="uploaddoc_button" button style="background-color:lightgray" id = "uploaddoc_button" value = "true" onclick="document_uploader()"><?php if ($uploaddoc == "true"){ ?> Deactivate Document Uploader <?php } elseif ($uploaddoc == "false") { ?> Activate Document Uploader <?php }
                    elseif ($uploaddoc == '') { ?> Activate Document Uploader <?php } ?></button><BR>
                <BR>
                <A HREF="#BOTTOM">Go to bottom of page</A><BR>


                <!--"Find Another Vehicle" Text Box-->
                <meta charset="utf-8">
                <!--                <meta name="viewport" content="width=device-width, initial-scale=1">-->
                <title>jQuery UI Autocomplete - Default functionality</title>
                <!--                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">-->
                <!--                      <link rel="stylesheet" href="/resources/demos/style.css">-->
                <!--                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
                <!--                <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>-->


                <div class="ui-widget">
                    <label for="tags">Find Another Vehicle: </label>
                    <input id="tags" onKeyDown="if(event.keyCode==13);" value = "" placeholder = "Type Here">
                </div>

                <div class="en_avte_maintenance_form">
                    <!--  Search: <input id="en_avte_maintenance_searchgrid" type="text" />  -->
                    <div id="en_avte_maintenance_grid" class="dataTable"></div>
                    <!-- <div id="en_avte_maintenance_paginggrid" class="pagination"></div> -->
                </div> <!--en_avte_maintenance_form-->
                <h3 style="margin-top: 200px;"><?php echo $car; ?></h3>

                <a name="BOTTOM"></a>

				<?php

			} else {
				?>
                <div class="en_avte_maintenance_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_maintenance_form-->
				<?php
			}
		}
	} else {
		?>
        <div class="en_avte_maintenance_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_maintenance_form-->
		<?php
	}
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}

?>