var delete_record_call;
var document_uploader;

(function($) {

var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_maintenance_grid'),
    en_avte_maintenance_searchgrid = $$('en_avte_maintenance_searchgrid'),
    en_avte_maintenance_paginggrid = $$('en_avte_maintenance_paginggrid'),
    hot,
    firstInsert= 0,
    filtering = 0,
    myData = [],
    noOfRowstoShow = 10000,
    firsttime = true,
    recordnum = [];
    idsToAddBackIn = [],

jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_maintenance',
        'car': urlParam('mycar'),
        'uploaddoc': urlParam('uploaddoc')
    },

    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. Error: " + JSON.stringify(data.errors));
        } else {
            var data = JSON.parse(res);
            myData = ObjToArr(data.allcols);
            myInlSyncFlag = ObjToArr(data.inlSyncFlag);
            buildHOT(data.allcols);
            getgridData(data.allcols, "1", 1000);
            prepareUpload();
        }
    }
);

function prepareUpload(){
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/wp-content/plugins/st-track-order/en-avte-maintenance/jQuery-File-Upload/server/php/'
    });

    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });
}

function buildHOT(idata) {
    hot = new Handsontable(container, {
        columns: GetHeaders(idata),
        startRows: 0,
        startCols: 0,
        fixedRowsTop: 2,
        contextMenu: ['remove_row', 'undo', 'redo', 'row_above', 'row_below'],
        minSpareRows: 1,
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        className: "htCenter",

        cells: function (row, col, prop) {
            return en_avte_maintenance_cellRender({row: row, col: col, prop: prop, myhot: this.instance});

        },

        beforeUndo:function(action){

            idsToAddBackIn = getIdsToUndo(action, hot);
        },

        afterUndo:function(action){
            // reinitialize "idsToAddBackIn " array so that we can track whether to make changes to calculated fields in the "afterchange" functionality
            idsToAddBackIn = [];

        },

        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 2) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                // if there is an instance where a client was created and not named.  Change myclient to '' so it can be found in the database

                jQuery.ajax({
                    url: ajax_sttrackorder_object.ajaxurl,
                    dataType: 'json',
                    async: false,
                    type: 'post',
                    data: {
                        'action': "ajaxen_avte_maintenance_new",
                        'amount': amount,
                        'car': urlParam('mycar'),
                        'addType':action,
                        'idsToAddBackIn': idsToAddBackIn
                    },
                    success: function (data) {
                        if (data.error == true) {
                            alert("Sorry, we can't add a new row. Error: " + JSON.stringify(data.errors));
                            hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                        } else {
                            for (var i = 0; i < data.newid.length; i++) {
                                //set email to R/W
                                //hot.setCellMeta(index + i - row_offset, 'user_email', 'valid', 'false');
                                hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                var newrow = {id: data.newid[i]};
                                myData.splice(myData.length, 0, newrow);

                                if (action != 'UndoRedo.undo'){
                                    hot.setDataAtRowProp(index + i - row_offset, 'create_time', data.cur_time, 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'explanation_of_service', '<A HREF="/avte-vehicle-log/maintenance-entry/maintenance-detail/?mymaintrec='+ data.newid[i].toString() + '&mycar=' + urlParam('mycar')+ '"> Click to edit </a>', 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'user', data.user, 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'car_id', urlParam('mycar'), 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'under_warranty', 'false', 'auto_edit');
                                }

                            }
                            hot_console.innerText = 'Row Inserted in DB';
                        }
                    }
                });
            }
            firstInsert++;
        },

       beforeChange: function (changes, source) {
            // if user makes a gange that is equal to the current value do not bother making change... does NOT work for bulk changes
            if ((changes[0][2] == changes[0][3]) && (changes.length == 1)) {
                return false;
            }

            //we were getting an error when a blank was entered into the sample recipt dat time field. so if the entry is blank do not make change
            if (((changes[0][1].indexOf('time') >= 0) || (changes[0][1].indexOf('date') >= 0)) && (changes[0][1].indexOf('verify_date') == -1) && (changes[0][3] == "")) {
                return false;
            }

            // Do not let users change regords that have already been verified
            for (i = 0; i < changes.length; i++) {
                if (((hot.getDataAtRowProp(changes[i][0], "verify_date")) != "") && (hot.getDataAtRowProp(changes[i][0], "verify_date") != "11-30--0001") &&
                    ((hot.getDataAtRowProp(changes[i][0], "verify_date")) != null) && ((changes[i][1]) != "verify_date") && ((changes[i][1]) != "notes")) {
                    alert("ERROR!!! Change NOT Saved!!!  You cannot make a change to a record that has already been verified");
                    return false;
                }
            }
        },

        afterChange: function (change, source) {

            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }

            // // do not allow columns that are only caclulation that are not actually saved in the DB to be sent to the DB
            if ((change[0][1] == 'explanation_of_service') || (change[0][1] == 'cost')) {
                return false;
            }

            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=hot.getDataAtRowProp(change[i][0],'id');
                // change[i][5]=hot.getDataAtRowProp(change[i][0],'user_id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_avte_maintenance_change',
                    'data': change,
                    'source': source,
                    'car': urlParam('mycar')
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the following changes. Error: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';

                        }
                    }
                }
            });

            //Now go see if I need to do further actions
            // change the "Sync with INL" column based on the "Service Type" column
            /*only if idsToAddBackIn is null because this indicates that the change is not being triggered by undo.  Undo already has all the pertinent information for each column.
            * allowing it to make this change also confilcts with undo because this action will clear the previously undone actions in the hansdaontable functionality*/
            if (idsToAddBackIn.length == 0) {
                var key;
                for (key in change) {
                    if (change[key][1] == 'service_type') {
                        service_type = hot.getDataAtRowProp(change[key][0], 'service_type');
                        for (i = 0, r_len = myInlSyncFlag.length; i < r_len; i++) {
                            if (myInlSyncFlag[i][0] == service_type) {
                                sync_to_inl_flag = myInlSyncFlag[i][1];
                                if (sync_to_inl_flag == 1) {
                                    inl_sync_flag = 'X';
                                    hot.setDataAtRowProp(change[key][0], 'inl_sync', inl_sync_flag, 'change_' + change[key][1]);
                                    break;
                                }
                                else if (sync_to_inl_flag == 0) {
                                    inl_sync_flag = '';
                                    hot.setDataAtRowProp(change[key][0], 'inl_sync', inl_sync_flag, 'change_' + change[key][1]);
                                }
                                break;
                            }
                        }
                    }
                }
            }
        },

        beforeRemoveRow: function (index, amount) {

            //Check to make sure we are not trying to delete the blank row at the bottom
            lastRow = hot.countRows() -1;
            if (((index == lastRow) || (index + amount -1 == lastRow)) && (hot.getDataAtRowProp(lastRow, 'id') == null)) {
                alert("You CANNOT delete the blank entry row (last row)."); return false;}

            // index the row(s) to be deleted with the record ID
            recordnum = [];
            if (index > 1) {
                i = 0;
                for (row = index; row <= index + (amount - 1); row++) {
                    recordnum[i] = hot.getDataAtRowProp(row, 'id');
                    //if a record in the selection has already been verified then cancel deletion
                    if ((hot.getDataAtRowProp(row, 'verify_date') != "11-30--0001") && (hot.getDataAtRowProp(row, 'verify_date') != "")
                        && (hot.getDataAtRowProp(row, 'verify_date') != null)) {
                        alert("ERROR!!! Record(s) NOT Deleted!!! One or more of the records selected for deletion has already been verified.");
                        return false;
                    }
                    i++;
                }
                delete_record(index, amount, recordnum);
            }
        }
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_maintenance_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_maintenance_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_maintenance_cellRender(parameters) {
    var row = parameters.row;
    var col = parameters.col;
    var prop = parameters.prop;
    var myhot = parameters.myhot;
    var cellProperties = {};


    //hide group row.  Used for coloring and permissions.
    if (row === 0) {cellProperties.type = { renderer: hiddenRowRender }; }

    //color the second row, which is the friendly field name
   // (myhot.getDataAtCell(1, col) == "id") ||
    else if ((row == 1) && !((myhot.getDataAtCell(1, col) == "Vehicle ID"))) {
        if (myhot.getDataAtCell(0, col) == "fm"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }

    else {
        switch(prop) {
            case "service_documentation":
                cellProperties.renderer = customCellRenderer;
                cellProperties.readOnly = true;
                break;
            case "id":
                cellProperties.readOnly = true;
                break;
            case "under_warranty":
                cellProperties.type = 'checkbox';
                break;
            case "service_type":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Scheduled Maintenance','Repair & Wear - Normal','Repair & Wear - Incidental','OEM/Dealer Recalls', 'Service Bulletins', 'Accidents', 'Notes',''];
                break;
            case "inl_sync":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'text';
                cellProperties.validator = /^$|^[X]$/;
                cellProperties.allowInvalid = false;
                cellProperties.renderer = syncRenderer;
                break;
            case "cost":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '$0,0.00';
                cellProperties.readOnly = true;
                cellProperties.validator = /^(\s*|(^[0-9]+(\.[0-9]{1,2})?$)|(^(\.[0-9]{1,2})?$))?$/;
                cellProperties.allowInvalid = false;
                break;
            case "explanation_of_service":
                cellProperties.type = 'text';
                cellProperties.renderer = customCellRenderer;
                cellProperties.readOnly = true;
                break;
            case "repair_facility":
                cellProperties.type = 'text';
                break;
            case "notes":
                cellProperties.type = 'text';
                cellProperties.renderer = customCellRenderer;
                break;
            case "odometer":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/ ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = true;
                break;

            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                //cellProperties.type = 'date';
                //cellProperties.dateFormat = 'MM-DD-YYYY HH:MM:SS';
                //cellProperties.correctFormat = false;
                //cellProperties.defaultDate = '01/18/2020';
            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = true;
            }
        }
        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }
    }

    return cellProperties;
}

//render the cell contents for INL SYNC FLAG.  If the flag column is set to sync "X" it will be red.  If it is set to not sync "" then it will be white
function syncRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);

        if (value == ''){

            td.style.fontWeight = 'none';
            td.style.color = 'black';
            td.style.background = 'gray';
        }
        else if (value == 'X') {

            td.style.fontWeight = 'bold';
            td.style.color = 'lightblue';
            td.style.background = 'green';
        }
}

//Ajax call to server for record delete
delete_record_call = function delete_record_call(index, amount, recordnum) {
    return jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        type: "POST",
        async: false,
        data: {
            action: "ajaxen_avte_maintenance_deleterecord",
            amount: amount,
            index: index,
            id: recordnum,
            car: urlParam('mycar')
        }
    }).responseText;
}

//Pull up another vehicle using the "Find Another Vehicle" text box
// pulls up new vehicle information upon pressing enter
document.getElementById('tags').onkeydown = function(event){

    document.getElementById('tags').style.color = "green";
    var e = event || window.event;

    if(e.keyCode == 13){
        myselectCars = jQuery ("input[id='tags']").val();
        window.location.assign("?mycar=" + myselectCars);
    }
}

// Upload Documents button
document_uploader = function document_uploader(){
    if ((urlParam('uploaddoc') == null) || (urlParam('uploaddoc') == "false") ) {
        window.location.assign("?mycar=" + urlParam('mycar') + "&uploaddoc=true");
    } else if ((urlParam('uploaddoc') == "true")) {
        window.location.assign("?mycar=" + urlParam('mycar') + "&uploaddoc=false");
    }
}

})( jQuery );


