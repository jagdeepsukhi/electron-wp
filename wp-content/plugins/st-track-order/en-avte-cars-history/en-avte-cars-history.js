//http://my-waking-dream.blogspot.com/2013/12/live-search-filter-for-jquery.html
jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {'action': 'ajaxen_avte_cars_history'},
    function(res){
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        var stripObj = ObjToArr(data.allcols);
        callFunc(stripObj);
    }
);

function callFunc(iData) {
    myData = iData;
    buildHOT();
    loadData();
}

var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_cars_history_master_grid'),
    exampleConsole = $$('en_avte_cars_history_master_console'),
    searchgrid = $$('en_avte_cars_history_searchgrid'),
    hot,
    noOfRowstoShow = 100,
    myData = [];

function buildHOT() {
    hot = new Handsontable(container, {
        startRows: 1,
        startCols: 1,
        manualColumnResize: true,
        manualRowResize: true,
        rowHeaders: false,
        colHeaders: ["Date/Time", "VIN", "Record ID", "User", "Field", "Before", "After", "Source"],
        columns: [
            {data: 0, type: 'date'},
            {data: 1, type: 'text'},
            {data: 2, type: 'text'},
            {data: 3, type: 'text'},
            {data: 4, type: 'text'},
            {data: 5, type: 'text'},
            {data: 6, type: 'text'},
            {data: 7, type: 'text'}
        ],
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        columnSorting: true,
        stretchH: 'all',
        readOnly: true,
        contextMenu: false,
        afterChange: function (change, source) {
            if (source === 'edit') {
                var datarow = hot.getDataAtRow(change[0][0]);
                for (row = 0, r_len = myData.length; row < r_len; row++) {
                    for (col = 0, c_len = myData[row].length; col < c_len; col++) {
                        if (myData[row][col] == datarow[5]) {
                            myData[row][change[0][1]] = change[0][3];
                        }
                    }
                }
            }
        },
        afterCreateRow: function (index, amount) {
            var rowvalue = myData[myData.length - 1][5];
            var rowno = rowvalue.split("||");
            var newrow = ["", "", "", "", "", "R||" + (parseInt(rowno[1]) + 1)];

            myData.splice(index, 0, newrow);
            getgridData(myData, "1", noOfRowstoShow);
        },
        beforeRemoveRow: function (index, amount) {
            var removerow = hot.getDataAtRow(index);
            var flag = false;
            for (row = 0, r_len = myData.length; row < r_len; row++) {
                for (col = 0, c_len = myData[row].length; col < c_len; col++) {
                    if (myData[row][col] == removerow[5]) {

                        myData.splice(row, 1);
                        flag = true;
                    }
                    if (flag == true) {
                        break;
                    }
                }
                if (flag == true) {
                    break;
                }
            }
        }
    });
}

function loadData() {

    //jQuery.ajax({
    //    type: 'POST',
    //    dataType: 'json',
    //    url: ajax_sttrackorder_object.ajaxurl,
    //    async: false,   // this is the important line that makes the request synchronous
    //    data: 'action=ajaxstchangehistory',
    //    success: function (res) {
    //        alert('The server respondedaa: ' + res);
    //        var data = JSON.parse(res);
    //        alert('The server respondedbb: ' + data);
    //        myData = data.allcols;
    //        //hot.loadData(myData);
    //    }
    //});
    //alert('The server respondedZZZ: ' + myData);
    getgridData(myData, "1", noOfRowstoShow);
}

jQuery('#en_avte_cars_history_searchgrid').on('keyup', function (event) {
    var value = ('' + this.value).toLowerCase(), row, col, r_len, c_len, td;
    var data = myData;
    var searcharray = [];
    if (value) {
        for (row = 0, r_len = data.length; row < r_len; row++) {
            for (col = 0, c_len = data[row].length; col < c_len; col++) {
                if (data[row][col] == null) {
                    continue;
                }
                if (('' + data[row][col]).toLowerCase().indexOf(value) > -1) {
                    searcharray.push(data[row]);
                    break;
                }
                else {
                }
            }
        }
        getgridData(searcharray, "1", noOfRowstoShow);
    }
    else {
        getgridData(myData, "1", noOfRowstoShow);
    }
});

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery('#en_avte_cars_history_gridpage').empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery('#en_avte_cars_history_gridpage').append(element);
    }
    hot.loadData(part);
    return part;
}

