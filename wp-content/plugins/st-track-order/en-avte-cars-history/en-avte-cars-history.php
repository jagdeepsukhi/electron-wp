<?php

//Go make the files and variables be available
require_once realpath( dirname( __file__ ) ) . "/en-avte-cars-history-form.php";
wp_register_script('en-avte-cars-history-js', ISTO_URL."/en-avte-cars-history/en-avte-cars-history.js", array('jquery'),
    filemtime(ISTO_DIR.'/en-avte-cars-history/en-avte-cars-history.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxstchangehistory', 'ajax_stchangehistory' );
add_action( 'wp_ajax_ajaxen_avte_cars_history', 'ajax_en_avte_cars_history' );


add_shortcode( 'en_avte_cars_history', 'en_avte_cars_history' );
function en_avte_cars_history( $atts, $content = '' )
{
    wp_enqueue_script('st-hot-js');
    wp_enqueue_style('st-hot-css');
    wp_enqueue_script('st-track-order-js');
    wp_enqueue_style('st-track-order-css');
    wp_enqueue_script('en-avte-cars-history-js');

    $output = en_avte_cars_history_form();
    return $output;
}

function ajax_en_avte_cars_history(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    //Go get the columns that should be there for all Cars history
    $sql = "SELECT
              en_avte_cars_history.date,
              en_avte_cars.vehicle_id,
              en_avte_cars_history.table_id,
              en_avte_cars_history.user,
              en_avte_cars_history.field,
              en_avte_cars_history.`before`,
              en_avte_cars_history.after,
              en_avte_cars_history.source
            FROM en_avte_cars_history
              INNER JOIN en_avte_cars
                ON en_avte_cars_history.table_id = en_avte_cars.id
            ORDER BY en_avte_cars_history.id DESC";
    $myres = $wpdb->get_results( $sql, OBJECT );


    //Send Response
    $json['allcols'] = $myres;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>