<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-monthend-form.php";
    wp_register_script('en-avte-monthend-js', ISTO_URL . "/en-avte-monthend/en-avte-monthend.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-monthend/en-avte-monthend.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_avte_monthend_new', 'ajax_en_avte_monthend_new');
    add_action('wp_ajax_ajaxen_avte_monthend_change', 'ajax_en_avte_monthend_change');
    add_action('wp_ajax_ajaxen_avte_monthend', 'ajax_en_avte_monthend');
    add_action('wp_ajax_ajaxen_avte_monthend_deleterecord', 'ajax_en_avte_monthend_deleterecord');

    add_shortcode('en_avte_monthend', 'en_avte_monthend');
    function en_avte_monthend($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');


        $output = en_avte_monthend_form();
        return $output;
    }


function ajax_en_avte_monthend(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_monthend'
                    ORDER BY st_default_cols.`order`";
            }

            else {
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_monthend' AND st_default_cols.group = 'DR'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT * FROM (
        SELECT  *
        FROM    en_avte_monthend
        WHERE   car_id = '%s'
        ORDER BY year DESC,month DESC LIMIT 10000) as allMonthendEntries
        ORDER BY year ASC, month ASC";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );
    $dbdata = make_date($dbdata);
    $col_plus_data = array_merge ($transposed, $dbdata);

    //Send Response
    $json['allcols'] =$col_plus_data;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_monthend_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

    //Handle each change
    $cur_time = current_time( 'mysql' );

    for ($i = 0; $i < $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
            if ($_POST['addType'] != "UndoRedo.undo") {
                $dbresult = $wpdb->insert('en_avte_monthend',
                    array(
                    'create_time' => $cur_time,
                    'car_id' => $_POST['car'],
                    'driver_id' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    )
                );
            } else{
                $dbresult = $wpdb->insert('en_avte_monthend',
                    array(
                        'id' => $_POST['idsToAddBackIn'][$i],
                        'car_id' => $_POST['car']
                    )
                );
            }

            $json['newid'][$i] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][$i] = true;
            } else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $wpdb->insert_id,
                    'car_id' => $_POST['car']
                );
                $wpdb->insert( 'en_avte_monthend_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['user'] = $current_user->data->display_name;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_monthend_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );
    $table = "monthend";
    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_monthend'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    $dbindex = 0;

//See if they are allowed to delete this record
//Make the deletion
    foreach ($_POST['id'] as $id) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified ($table, $id);

        if ((allowed_to_change('id', $rolemap, $user->roles)) && ((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")))){
            try {
                $wpdb->flush();
                $sql = "DELETE FROM en_avte_monthend
	            WHERE id = '%s'";
                $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
                if ((0 === $dbresult) || (false === $dbresult)) {

                    $json[errors][$dbindex] = true;

                }
            } catch (Exception $e) {
                $json[errors][$dbindex] = $e->getMessage();
            }

//          Log the deletion
            $dbdata = array(
                'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                'field' => 'RECORD DELETED',
                'before' => 'Active',
                'after' => 'Deleted',
                'source' => 'Delete',
                'table_id' => $id,
                'car_id' => $_POST['car']
            );
            $wpdb->insert('en_avte_monthend_history', $dbdata);
        }

        else {
            //user did not have permissiong to make deletion
            $json[errors][$dbindex] = 'role';
        }

        $dbindex++;
    }

    //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_monthend_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );
    $table = 'monthend';

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_monthend'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified ($table, $change[4]);

        $logit = true;
        //skip ID change notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles)) && ((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")) || (($user->roles[0] =="FM" || "administrator")
                        && (($change[1] == "verify_date")||($change[1] == "notes"))))){
                //Make the change
                try {
                    $wpdb->flush();
                    //wpdb->prepare does not accept nulls, so we can't use it here.
                    $table = "en_avte_monthend";
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]) );
                    if ( false === $dbresult ) {

                            $json[errors][$dbindex] = true;

                    }
                } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }

                //Log the change
                if ($logit) {
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => $change[1],
                        'before' => $change[2],
                        'after' => $change[3],
                        'source' => $_POST['source'],
                        'table_id' => $change[4],
                        'car_id' => $_POST['car']
                    );
                    $wpdb->insert('en_avte_monthend_history', $dbdata);
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = 'role';
            }
        }
        //user tried to change record "id" which is NEVER permissible
        else{ $json[errors][$dbindex] = true;}
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>