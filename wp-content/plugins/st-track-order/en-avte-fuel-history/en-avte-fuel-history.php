<?php

//Go make the files and variables be available
require_once realpath( dirname( __file__ ) ) . "/en-avte-fuel-history-form.php";
wp_register_script('en-avte-fuel-history-js', ISTO_URL."/en-avte-fuel-history/en-avte-fuel-history.js", array('jquery'),
    filemtime(ISTO_DIR.'/en-avte-fuel-history/en-avte-fuel-history.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxstchangehistory', 'ajax_stchangehistory' );
add_action( 'wp_ajax_ajaxen_avte_fuel_history', 'ajax_en_avte_fuel_history' );


add_shortcode( 'en_avte_fuel_history', 'en_avte_fuel_history' );
function en_avte_fuel_history( $atts, $content = '' )
{
    wp_enqueue_script('st-hot-js');
    wp_enqueue_style('st-hot-css');
    wp_enqueue_script('st-track-order-js');
    wp_enqueue_style('st-track-order-css');
    wp_enqueue_script('en-avte-fuel-history-js');

    $output = en_avte_fuel_history_form();
    return $output;
}

function ajax_en_avte_fuel_history(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    //Go get the columns that should be there for all SO
    $sql = "SELECT
              date,
              car_id,
              table_id,
              user,
              field,
              `before`,
              after,
              source
            FROM en_avte_fuel_history
            ORDER BY id DESC LIMIT 30000";
    $myres = $wpdb->get_results( $sql, OBJECT );

    //Send Response
    $json['allcols'] = $myres;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>