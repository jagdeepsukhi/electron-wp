var editCarsPageContent;
var hideCars;

(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_cars_grid'),
    en_avte_cars_searchgrid = $$('en_avte_cars_searchgrid'),
    en_avte_cars_paginggrid = $$('en_avte_cars_gridpage'),
    hot,
    filtering = 0,
    rowmap ={},
    firstInsert= 0,
    myData = [],
    noOfRowstoShow = 10000


jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_cars',
        'view': jQuery("input[name='tableView']").val(),
        'hide': urlParam ('hide')
    },

    function (res) {
        var data = JSON.parse(res);
        myData = ObjToArr(data.allcols);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. Error: " + JSON.stringify(data.errors));
        } else {
        buildHOT(data.allcols);
	    getgridData(data.allcols, "1", 10000);
        }
    }

);

// function before_insert(){
//     debugger;
//     if (((jQuery("button[name='edit_button']").val()== "true") || jQuery("button[name='edit_button']").val()== undefined)) {
//         return "Insert row above";
//     } else {
//         //if in edit mode
//        return {};
//     }
//
//
// }

    var blah = {
        name: "Insert row above",
        callback: function(key, options){
            return jQuery.noop;
            if (jQuery("button[name='edit_button']").val() != "true") {
                hot.alter('insert_row', options.start.row, 1, 'row_add');
            }
        }
    };

function buildHOT(idata){
    hot = new Handsontable(container, {

        startRows: 0,
        startCols: 0,
        columns: GetHeaders(idata),
        fixedRowsTop: 2,
        autoRowSize: false,
        manualColumnResize: true,
        manualRowResize: true,
        autoWrapRow: false,
        colHeaders: false,
        wordWrap: false,
        //contextMenu: ['row_above', 'row_below'],
        contextMenu: {
            items: {
                "row_above_custom": {
                    name: "Insert row above",
                    callback: function(key, options){
                        if (jQuery("button[name='edit_button']").val() != "true") {
                            hot.alter('insert_row', options.start.row, 1, 'new_row');
                        } else {
                            setTimeout(function () {
                                // timeout is used to make sure the menu collapsed before alert is shown
                                    if (jQuery("button[name='edit_button']").val()== undefined) {
                                        alert("You do not have permission to make this change.");
                                    } else {alert("You must be in \"Edit\" mode to make this change.  Edit mode is ONLY accessible to Fleet Managers");}
                            }, 100);
                        }
                    }
                },
                "row_below_custom": {
                    name: "Insert row below",
                    callback: function(key, options){
                        if (jQuery("button[name='edit_button']").val() != "true") {
                            hot.alter('insert_row', options.end.row + 1, 1, 'new_row');
                        } else {
                            setTimeout(function () {
                                // timeout is used to make sure the menu collapsed before alert is shown
                                if (jQuery("button[name='edit_button']").val()== undefined) {
                                    alert("You do not have permission to make this change.");
                                } else {alert("You must be in \"Edit\" mode to make this change.  Edit mode is ONLY accessible to Fleet Managers");}
                            }, 100);
                        }
                    }
                }
            }

        },

        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        className: "htCenter",
        fillHandle: false,

        cells: function (row, col, prop) {
            return en_avte_cars_cellRender({row: row, col: col, prop: prop, myhot: this.instance});
        },

        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 0) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                jQuery.ajax({
                    url: ajax_sttrackorder_object.ajaxurl,
                    dataType: 'json',
                    async: false,
                    type: 'post',
                    data: {
                        'action': "ajaxen_avte_cars_new",
                        'amount': amount,
                        'addType': action,
                    },
                    success: function (data) {
                        if (data.error == true) {
                            alert("Sorry, we can't add a new row. Error: " + JSON.stringify(data.errors));
                            hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                        } else {
                            for (var i = 0; i < data.newid.length; i++) {
                                //set email to R/W
                                hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                var newrow = {id: data.newid[i]};
                                myData.splice(myData.length, 0, newrow);

                                if (action != 'UndoRedo.undo'){
                                    hot.setDataAtRowProp(index + i - row_offset, 'start_date', data.cur_time, 'auto_edit');
                                }

                            }
                            hot_console.innerText = 'Row Inserted in DB';
                        }
                    }
                });
            }
            firstInsert++;
        },

        beforeChange: function(changes, source) {
            // if user makes a gange that is equal to the current value do not bother making change... does NOT work for bulk changes
            if ((changes[0][2] == changes[0][3]) && (changes.length == 1)) {
                return false;
            }
           if ((jQuery("button[name='edit_button']").val()== "true") || jQuery("button[name='edit_button']").val()== undefined) {
               if (jQuery("button[name='edit_button']").val()== undefined) {
                   alert("You do not have permission to make this change.");
               } else {alert("You must be in \"Edit\" mode to make this change.  Edit mode is ONLY accessible to Fleet Managers");}

                return false;
            }
        },

        afterChange: function (change, source) {
            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }
            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=hot.getDataAtRowProp(change[i][0],'id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_avte_cars_change',
                    'data': change,
                    'source': source,
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the following changes. Error: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                            alert("CHANGE NOT SAVED!!!!  This is due to the user not having the correct permissions or the table is not in \"Edit\" mode");
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';
                        }
                    }
                }
            });
        },
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }
    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_cars_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_cars_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_cars_cellRender(parameters) {
    var row = parameters.row;
    var col = parameters.col;
    var prop = parameters.prop;
    var myhot = parameters.myhot;
    var cellProperties = {};

    //hide group row.  Used for coloring and permissions.
    if (row === 0) {
        cellProperties.type = { renderer: hiddenRowRender };
    }
    //color the second row, which is the friendly field name
    else if ((row == 1) && !((myhot.getDataAtCell(1, col) == "id"))) {
        if (myhot.getDataAtCell(0, col) == "dr"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }
    else {
        switch(prop) {
            case "id":
                cellProperties.renderer = whiteCellRenderer;
                break;
            case "vehicle_id":
            case "make":
            case "model":
            case "vin":
            case "color":
            case "plate_num":
            case "responsible_engineer":
            case "blink_card_num":
            case "fleet_location":
            case "insurance_owner":
            case "insurance_underwriter":
            case "insurance_ref":
            case "warranty_coverage":
                cellProperties.type = 'text';
                break;
            case "year":
                cellProperties.type = 'numeric';
                // cellProperties.renderer = customCellRenderer;
                break;
            case "start_odo":
            case "acquisition_odo":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/ ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;
            case "navigation":
                http://utilitymill.com/utility/Regex_For_Range
                    cellProperties.renderer = 'html';
                break;
            case "registration_cost":
            case "vehicle_cost":
            case "resale_price":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '$0,0.00';
                cellProperties.validator = /^(\s*|(^[0-9]+(\.[0-9]{1,2})?$)|(^(\.[0-9]{1,2})?$))?$/;
                cellProperties.allowInvalid = false;
                break;
            case "TYPE":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['EV', 'PHEV', 'HEV', 'Gas', 'CNG', 'Diesel', 'Gasoline/CNG'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "odo_type":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['Miles', 'km'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "department":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['Intertek'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "project":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['AVTE'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "fleet_owner":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['EZ Messenger', 'Sonora Quest', 'Specialized Delivery Service'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "disposition":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['', 'In Service', 'Sold', 'End Of Test',  'Totaled'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "acquisition_method":
                cellProperties.type = 'dropdown';
                cellProperties.source = ['Purchase', 'Lease', 'Loaner', 'Rent', 'Affiliate Owned'];
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                break;
            case "acquisition_date":
            case "start_date":
            case "registration_due_date":
            case "insurance_date":
            case "insurance_due_date":
            case "purchase_date":
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = true;
                break;
            case "cumulative_kWh":
                cellProperties.renderer = 'html';
                cellProperties.readonly = true;
                break;

            default:
        }
    }
    return cellProperties;
}

// this function is called by the edit button on the cars page and provides the funtionality for the user to make
    // changes on the cars page
editCarsPageContent = function editCarsPageContent(){
    if (jQuery("button[name='edit_button']").val()=="true"){ jQuery("button[name='edit_button']").val("false");
        jQuery("button[name='edit_button']").css('background-color','lightgreen');
        jQuery("button[name='edit_button']").css('color','white');
    } else {
        jQuery("button[name='edit_button']").val("true");
        jQuery("button[name='edit_button']").css('background-color','red');
    }
}

    // function called by the hide inactive vehicles button on cars page
hideCars = function hideCars() {
    if ((urlParam('hide') == "false") ) {
        window.location.assign("?myview=" + jQuery("input[name='tableView']").val() + "&hide=true");
    } else {
        window.location.assign("?myview=" + jQuery("input[name='tableView']").val() + "&hide=false");
    }
}

})( jQuery );
