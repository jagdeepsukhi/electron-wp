<?php


function en_avte_cars_form()
{
    global $current_user;
    $view = $_GET["myview"];
    $hide = $_GET['hide'];
    if ($view == ""){$view = "Main";}
    ob_start();

    if ( is_user_logged_in() ) {
        $user = $current_user;

        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) {
                    wp_enqueue_script('en-avte-cars-js');
                                if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {
                    ?>
                    <div class="en_avte_cars_form">
                        <A HREF="/vehicle-log/?myview=Main&hide=<?php echo $_GET['hide']; ?>"> Main View </a> | <A HREF="/vehicle-log/?myview=Purchase&hide=<?php echo $_GET['hide']; ?>"> Purchase View </a> |
                        <A HREF="/vehicle-log/?myview=Project&hide=<?php echo $_GET['hide']; ?>"> Project View </a> | <A HREF="/vehicle-log/?myview=Registration/Insurance&hide=<?php echo $_GET['hide']; ?>"> Registration/Insurance View </a> |
                        <A HREF="/vehicle-log/?myview=kWh&hide=<?php echo $_GET['hide']; ?>"> kWh View </a><BR>

                        <?php if ($view != "kWh"){ ?><button type="button" name="edit_button" id = "edit_button" value = "true" onclick="editCarsPageContent()" >Edit</button>
                        <?php }}else{ ?>
                        <div class="en_avte_cars_form">
                        <?php } ?>

<!--                        Show or Hide Inactive vehicles button-->
                        <button type="button" name="hide_button" id = "hide_button" value = "true" onclick="hideCars();"><?php if ($hide == "true"){ ?> Show All Vehicles <?php } elseif ($hide == "false") { ?> Show Active Vehicles ONLY <?php }
                            elseif ($hide == '') { ?> Show All Vehicles <?php } ?></button><BR>

                        <BR>

                        <h5><?php echo $view; ?> View</h5>
                        <!--  Search: <input id="en_avte_cars_searchgrid" type="text" />  -->
<!--                        Search: <input id="en_avte_cars_searchgrid" type="text" />-->
                        <div id="en_avte_cars_grid" class="dataTable"></div>
                        <pre id="en_avte_cars_master_console" class="console"></pre>
                        <!-- <div id="en_avte_cars_paginggrid" class="pagination"></div> -->
                        <div id="en_avte_cars_gridpage" class="pagination"></div>
                    </div> <!--en_avte_cars_form-->


                    <input type="hidden" name="tableView" value=<?php echo $view; ?>>
                    <?php

            } else {
                ?>
                <div class="en_avte_cars_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_cars_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_cars_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_cars_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>