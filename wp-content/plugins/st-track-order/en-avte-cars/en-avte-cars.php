<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-cars-form.php";
    wp_register_script('en-avte-cars-js', ISTO_URL . "/en-avte-cars/en-avte-cars.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-cars/en-avte-cars.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
    add_action('wp_ajax_ajaxen_avte_cars_new', 'ajax_en_avte_cars_new');
    add_action('wp_ajax_ajaxen_avte_cars_change', 'ajax_en_avte_cars_change');
    add_action('wp_ajax_ajaxen_avte_cars', 'ajax_en_avte_cars');
    add_action('wp_ajax_ajaxen_avte_cars_deleterecord', 'ajax_en_avte_cars_deleterecord');


    add_shortcode('en_avte_cars', 'en_avte_cars');
    function en_avte_cars($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');


        $output = en_avte_cars_form();
        return $output;
    }

function ajax_en_avte_cars(){

    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    $view = $_POST['view'];
    $hide = $_POST['hide'];
    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {
                //Go get the columns that should be there for all SO
                switch ($view) {
                    case "":
                    case "Main":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_avte_carsmain'
                                OR st_default_cols.table_name = 'en_avte_cars'
                                OR st_default_cols.table_name = 'en_avte_carstype'
                                ORDER BY st_default_cols.`order`";
                        break;
                    case "Purchase":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_avte_carspurchase'
                                OR st_default_cols.table_name = 'en_avte_cars'
                                ORDER BY st_default_cols.`order`";
                        break;
                    case "Project":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_avte_carsproject'
                                OR st_default_cols.table_name = 'en_avte_cars'
                                ORDER BY st_default_cols.`order`";
                        break;

                    case "Registration/Insurance":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_avte_carsreg-ins'
                                OR st_default_cols.table_name = 'en_avte_cars'
                                ORDER BY st_default_cols.`order`";
                        break;

                    case "kWh":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_avte_carskwh'
                                OR st_default_cols.table_name = 'en_avte_cars'
                                OR st_default_cols.table_name = 'en_avte_carstype'
                                ORDER BY st_default_cols.`order`";
                        break;

                    default:

                }
                $readOnly = 'false';
            } else {

                if ($view == '' || $view == 'Main') {

                    $sql = "SELECT
                          st_default_cols.field_name,
                          st_default_cols.`group`,
                          st_default_cols.name
                        FROM st_default_cols
                        WHERE st_default_cols.table_name = 'en_avte_cars' OR st_default_cols.table_name = 'en_avte_carsmain' AND st_default_cols.group = 'DR'
                        ORDER BY st_default_cols.`order`";
                    $readOnly = 'true';
                }
            }
        }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    IF ($view == 'kWh'){
        // if user has hidden or unhidden inactive vehicles from viewing with button on cars page
        IF ($hide == 'true' || $hide == "" || $hide == "0") {
            $sql = "SELECT
                      chargeagble_vehicles.vehicle_id AS vehicle_id,
                      ROUND(COALESCE(SUM(en_avte_monthend.kwh), CAST('kWh Not Found' AS char)),3) AS cumulative_kWh,
                      chargeagble_vehicles.TYPE AS TYPE
                      FROM en_avte_monthend
                      RIGHT OUTER JOIN (SELECT
                        *
                        FROM en_avte_cars
                        WHERE en_avte_cars.TYPE = 'EV'
                        OR en_avte_cars.TYPE = 'PHEV') chargeagble_vehicles
                        ON en_avte_monthend.car_id = chargeagble_vehicles.vehicle_id
                        WHERE chargeagble_vehicles.disposition = 'In Service'
                        GROUP BY chargeagble_vehicles.vehicle_id,
                             chargeagble_vehicles.TYPE";
        } else {
            $sql = "SELECT
                      chargeagble_vehicles.vehicle_id AS vehicle_id,
                      ROUND(COALESCE(SUM(en_avte_monthend.kwh), CAST('kWh Not Found' AS char)), 3) AS cumulative_kWh,
                      chargeagble_vehicles.TYPE AS TYPE
                    FROM en_avte_monthend
                      RIGHT OUTER JOIN (SELECT
                                *
                        FROM en_avte_cars
                        WHERE en_avte_cars.TYPE = 'EV'
                        OR en_avte_cars.TYPE = 'PHEV') chargeagble_vehicles
                        ON en_avte_monthend.car_id = chargeagble_vehicles.vehicle_id
                    GROUP BY chargeagble_vehicles.vehicle_id,
                             chargeagble_vehicles.TYPE";}

   }
   Else {
       // if user has hidden or unhidden inactive vehicles from viewing with button on cars page
        IF ($hide == 'true' || $hide == "" || $hide == "0"){
            $sql = "SELECT
                    *
                    FROM en_avte_cars
                    WHERE en_avte_cars.disposition = 'In Service'
                    ORDER BY en_avte_cars.vehicle_id";

        } else {
            $sql = "SELECT
                    *
                    FROM en_avte_cars
                    ORDER BY en_avte_cars.vehicle_id ASC";}
   }

$myres = $wpdb->get_results( $sql, OBJECT );
//$myres = $wpdb->get_results( $wpdb->prepare( $sql, $default_cols_string), OBJECT );
$vehicle_type = $myres->type;
$linked_dbdata = navigation_links ($myres);
$linked_dbdata = kWh_links ($linked_dbdata);
//$dbdatacount = count($linked_dbdata);
$linked_dbdata = make_date($linked_dbdata);
$col_plus_data = array_merge ($transposed, $linked_dbdata);

//Send Response
$json['allcols'] = $col_plus_data;
$json['error'] = count($json['errors']) > 0;
$json['roles'] = $user->roles;
$json['readOnly'] = $readOnly;
$json['wp_is_mobile'] = wp_is_mobile();
echo json_encode( $json );
wp_die();
}


function ajax_en_avte_cars_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

//    // First check the nonce, if it fails the function will break
//    check_ajax_referer( 'ajax-steditorderdetails-nonce', 'security' );

    //Handle each change
    $cur_time = current_time( 'mysql' );

    for ($i = 1; $i <= $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
//            if ($_POST['addType'] != "UndoRedo.undo") { ***********Undo is not currently implemented on this page******
                $dbresult = $wpdb->insert('en_avte_cars',
                    array(
                        'start_date' => $cur_time
                    )
                );
//            } else{
//                $dbresult = $wpdb->insert('en_avte_cars',
//                    array(
//                        'id' => $_POST['idsToAddBackIn'][$i]
//                    )
//                );
//            }
            $new_id[] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][$i] = true;
            } else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $new_id[$i-1]
                );
                $wpdb->insert( 'en_avte_cars_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['newid'] = $new_id;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-d-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_cars_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name LIKE '%en_avte_cars%'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

        $logit = true;
        //skip ID change notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles))){
//                 && ($_POST['readOnlyButton'] == "false")
                //Make the change
                try {
                    $wpdb->flush();
                    $table = "en_avte_cars";
                    //wpdb->prepare does not accept nulls, so we can't use it here.
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]) );
                    if  (false === $dbresult) {
                            $json[errors][$dbindex] = true;
                    }
                } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }

                //Log the change
                if ($logit) {
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => $change[1],
                        'before' => $change[2],
                        'after' => $change[3],
                        'source' => $_POST['source'],
                        'table_id' => $change[4]

                    );
                    $wpdb->insert('en_avte_cars_history', $dbdata);
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = "ERROR SAVING! You don't have permission.";
            }
        }
        //user tried to change record "id" which is NEVER permissible
        else{ if ($_POST['source'] != 'UndoRedo.undo'){
            $json[errors][$dbindex] = true;}
        }
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>