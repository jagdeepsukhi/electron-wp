<?php

//Go make the files and variables be available
require_once realpath(dirname(__file__)) . "/en-avte-vlr-form.php";
//require_once realpath(dirname(__file__)) . "/options.php";
wp_register_script('en-avte-vlr-js', ISTO_URL . "/en-avte-vlr/en-avte-vlr.js", array('jquery'),
    filemtime(ISTO_DIR . '/en-avte-vlr/en-avte-vlr.js'));
wp_register_script('en-avte-vlr-return-js', ISTO_URL . "/en-avte-vlr/en-avte-vlr-return.js", array('jquery'),
    filemtime(ISTO_DIR . '/en-avte-vlr/en-avte-vlr-return.js'));
wp_register_script('en-avte-vlr-request-js', ISTO_URL . "/en-avte-vlr/en-avte-vlr-request.js", array('jquery'),
    filemtime(ISTO_DIR . '/en-avte-vlr/en-avte-vlr-request.js'));

//Set the variables between php and js
$options = get_option('en_vlr_settings');
wp_localize_script( 'en-avte-vlr-js', 'ajax_sttrackorder_object', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    'request_checker' => 'check'.$options['request_form'],
    'return_checker' => 'check'.$options['return_form']
));

//setup settings needed for VLR, to be managed by the admin configuration screen inside wp-admin
function electron_vlr_settings()
{
    //http://ottopress.com/2009/wordpress-settings-api-tutorial/
    //all of the options will be under one single database entry, an array.
    //register new settings group 'en_vlr_settings', with single array 'en_vlr_settings' to store all settings
    register_setting( 'en_vlr_settings', 'en_vlr_settings' );
    //unique section id 'en_vlr_main', title, callback for section decription (required), and this is under submenu page 'en-vlr'
    add_settings_section('en_vlr_main', 'General Settings', 'en_vlr_admin_section_cb', 'en-vlr');
    //unique field 'en_vlr_auth_email', title, callback to display the option, under the ''en-vlr' submenu, under the 'en_vlr_main' section,
    // 'en_vlr_auth_email' array arg so we know what field to show
    add_settings_field('en_vlr_auth_email', 'Authorization emails', 'en_vlr_admin_field_cb', 'en-vlr', 'en_vlr_main',array('en_vlr_auth_email'));
    add_settings_section('en_vlr_request', 'Request Settings', 'en_vlr_admin_section_cb', 'en-vlr');
    add_settings_field('en_vlr_request_form', 'Request FormMaker ID ', 'en_vlr_admin_field_cb', 'en-vlr', 'en_vlr_request',array('en_vlr_request_form'));
    add_settings_section('en_vlr_return', 'Return Settings', 'en_vlr_admin_section_cb', 'en-vlr');
    add_settings_field('en_vlr_return_form', 'Return FormMaker ID ', 'en_vlr_admin_field_cb', 'en-vlr', 'en_vlr_return',array('en_vlr_return_form'));
}
if ( is_admin() ) {
    add_action('admin_init', 'electron_vlr_settings');
}


//Setup all the shortcodes
add_shortcode('en_avte_vlr', 'en_avte_vlr');
function en_avte_vlr($atts, $content = '')
{
    wp_enqueue_script('en-avte-vlr-js');
    wp_enqueue_script('en-avte-vlr-request-js');
    $output = en_avte_vlr_form();
    return $output;
}

add_shortcode('en_avte_vlr_list', 'en_avte_vlr_list');
function en_avte_vlr_list($atts, $content = '')
{
    $output = do_shortcode(en_avte_vlr_list_form());
    return $output;
}

add_shortcode('en_avte_vlr_auth', 'en_avte_vlr_auth');
function en_avte_vlr_auth($atts, $content = '')
{
    $output = do_shortcode(en_avte_vlr_auth_form());
    return $output;
}

add_shortcode('en_avte_vlr_return', 'en_avte_vlr_return');
function en_avte_vlr_return($atts, $content = '')
{
    wp_enqueue_script('en-avte-vlr-js');
    wp_enqueue_script('en-avte-vlr-return-js');
    $output = do_shortcode(en_avte_vlr_return_form());
    return $output;
}

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
add_action('wp_ajax_nopriv_ajaxen_avte_vlr_auth', 'ajax_en_avte_vlr_auth');
add_action('wp_ajax_ajaxen_avte_vlr_auth', 'ajax_en_avte_vlr_auth');
add_action('wp_ajax_ajaxen_avte_vlr_carlookup', 'ajax_en_avte_vlr_carlookup');
add_action('wp_ajax_ajaxen_avte_vlr_checkout', 'ajax_en_avte_vlr_checkout');
add_action('wp_ajax_ajaxen_avte_vlr_vlrlookup', 'ajax_en_avte_vlr_vlrlookup');
add_action('wp_ajax_ajaxen_avte_vlr_checkin', 'ajax_en_avte_vlr_checkin');
add_action('wp_ajax_ajaxen_avte_vlr_donext', 'ajax_en_avte_vlr_donext');
add_action('wp_ajax_nopriv_ajaxen_avte_vlr_donext', 'ajax_en_avte_vlr_donext');

//Setup all the AJAX callbacks
function ajax_en_avte_vlr_donext()
{
    //usage: http://70.42.103.171/wp-admin/admin-ajax.php?action=ajaxen_avte_vlr_donext&car=C1V0815
    global $wpdb;

    try {
        $wpdb->flush();
        $sql = "SELECT
              CASE
                WHEN CarsAlreadyCheckedOut.display_name IS NULL THEN CONCAT('/vehicle-loan-request/?mycar=', en_avte_cars.vehicle_id) ELSE CASE
                  WHEN CarsAlreadyCheckedOut.auth_status IS NULL THEN CONCAT('/vehicle-loan-request-authorization/?vlr_id=', CarsAlreadyCheckedOut.id) ELSE CONCAT('/vehicle-loan-return/?vlr_id=', CarsAlreadyCheckedOut.id)
                END
              END AS action
        FROM en_avte_cars
          LEFT OUTER JOIN (SELECT
            en_avte_vlr.car_id,
            en_avte_vlr.create_time,
            en_avte_vlr.last_modified_time,
            en_avte_vlr.auth_status,
            wp_users.display_name,
            wp_users.user_email,
            en_avte_vlr.date_to_return,
            en_avte_vlr.id
          FROM en_avte_vlr
            LEFT OUTER JOIN wp_users
              ON en_avte_vlr.driver_id = wp_users.ID
          WHERE en_avte_vlr.date_returned IS NULL AND COALESCE(en_avte_vlr.auth_status, '') <> 'denied') CarsAlreadyCheckedOut
            ON en_avte_cars.vehicle_id = CarsAlreadyCheckedOut.car_id
          WHERE en_avte_cars.vehicle_id = '%s'";
        $dbresult = $wpdb->get_var($wpdb->prepare($sql, $_REQUEST['car']));
        if ((0 === $dbresult) || (false === $dbresult) || (null === $dbresult)) {
            echo "<h1>Sorry, car not found.</h1>";
        } else {
            wp_redirect($dbresult);
        }
    } catch (Exception $e) {
        echo "<h1>Database error: '.$e->getMessage().'</h1>";
    }

    die();
}

function ajax_en_avte_vlr_auth(){
    global $wpdb;
    global $current_user;
    $ok_to_proceed = false;

    //Let's see if this process is permitted
    //First, let's see if they have an email with the link
    if (isset($_GET['nonce_name'])) {
        $ok_to_proceed = WPSimpleNonce::checkNonce($_GET['nonce_name'], $_GET['nonce_val']);
    } else {
        //Or, did they come via the website and have the role to do this?
        if (is_user_logged_in()) {
            $user = $current_user;
            if (isset($user->roles) && is_array($user->roles)) {
                $ADorg = get_user_option('ad_integration_account_suffix');
                if (
                    (doesUserHavePermissionToEnterPage($user, array('administrator', 'fm'))) &&
                    (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
                ) {
                    $ok_to_proceed = true;
                } else {
                    echo "<h1>Sorry, you don't have the right roles assigned to authorize this VLR.</h1>";
                    wp_die();
                }
            } else {
                echo "<h1>Sorry, error looking up your roles.</h1>";
                wp_die();
            }
        } else {
            echo '<h1>Sorry, you need to be logged in. You can do that <a href="/logmein/">HERE</a></h1>';
            wp_die();
        }
    }

    if ($ok_to_proceed){
        //Nonce is still valid and good to go or user has the right roles
        try {
            if (is_user_logged_in()) {
                $user = $current_user;
                $authorizer = $user->display_name;
            } else {
                $authorizer = 'email';
            }
            $wpdb->flush();
            $sql = "SELECT
                      en_avte_vlr.auth_status,
                      wp_users.user_email,
                      en_avte_cars.year,
                      en_avte_vlr.car_id,
                      en_avte_cars.make,
                      en_avte_cars.model,
                      en_avte_cars.color,
                      wp_users.display_name,
                      en_avte_vlr.date_required,
                      en_avte_vlr.date_to_return,
                      en_avte_vlr.mileage_out,
                      en_avte_vlr.fuel_out,
                      en_avte_vlr.purpose,
                      en_avte_vlr.damage
                    FROM en_avte_vlr
                      LEFT OUTER JOIN en_avte_cars
                        ON en_avte_vlr.car_id = en_avte_cars.vehicle_id
                      LEFT OUTER JOIN wp_users
                        ON en_avte_vlr.driver_id = wp_users.ID
                    WHERE en_avte_vlr.id = %d";
            $dbresult = $wpdb->get_row($wpdb->prepare($sql, $_GET['vlr_id']));
            if ((0 === $dbresult) || (false === $dbresult)) {
                echo '<h1>You should never see this... VLR not found on lookup.</h1>';
            } else {
                if ($dbresult->auth_status === null) {
                    //Save DB results for email later
                    $driver = $dbresult->user_email;
                    $param = array(
                        '%Car ID%' => $dbresult->car_id,
                        '%Color%' => $dbresult->color,
                        '%Year%' => $dbresult->year,
                        '%Make%' => $dbresult->make,
                        '%Model%' => $dbresult->model,
                        '%Date Required%' => $dbresult->date_required,
                        '%Date to be returned%' => $dbresult->date_to_return,
                        '%Current Mileage%' => $dbresult->mileage_out,
                        '%Current Fuel Level%' => $dbresult->fuel_out,
                        '%Reason for Vehicle Loan Request%' => $dbresult->purpose,
                        '%vlr_id%' => $_GET['vlr_id'],
                        '%action_message%' => '<br>',
                        '%damage%' => '<br>',
                        '<li>Damage: </li>' => ''
                    );
                    try {
                        $wpdb->flush();
                        $sql = "UPDATE en_avte_vlr
                                SET auth_status = '%s',
                                    auth_date = NOW(),
                                    auth_contact = '%s',
                                    last_modified_time = NOW()
                                WHERE id = %d";
                        $dbresult = $wpdb->query($wpdb->prepare($sql, $_GET['subaction'], $authorizer, $_GET['vlr_id']));
                        if ((0 === $dbresult) || (false === $dbresult)) {
                            echo '<h1>You should never see this... VLR not found on update.</h1>';
                        } else {
                            echo '<h1>Great, all done now.</h1>';

                            //Email the requestor
                            //wp_schedule_single_event( time() + 10, 'en_mail_queue', array(
                            en_mail(
                                time(), $driver,'VLRRequestCopy',$param,
                                'VLR Authorization: '.$_GET['subaction'], '', 'noreply.cecet@intertek.com', 'Electron VLR'
                            //   )
                            );
                        }

                    } catch (Exception $e) {
                        echo '<h1>Database error: '.$e->getMessage().'</h1>';
                    }

                } else {
                    echo '<h1>Thanks, nothing to do. It looks like this request already was taken care of via the website directly.</h1>';
                }
            }

        } catch (Exception $e) {
            echo '<h1>Database error while looking up VLR: '.$e->getMessage().'</h1>';
        }

    } else {
        echo '<h1>Thanks, nothing to do. It looks like this request already was taken care of.</h1>';
    }
    wp_die();
}

function ajax_en_avte_vlr_vlrlookup(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        try {
            $wpdb->flush();
            $sql = "SELECT
                      en_avte_cars.year,
                      en_avte_cars.make,
                      en_avte_cars.model,
                      en_avte_cars.color,
                      LatestDamage.damage,
                      IsCarReady.outstanding_vlrs,
                      LatestDamage.id,
                      en_avte_cars.vehicle_id,
                      LatestDamage.display_name
                    FROM en_avte_cars
                      LEFT OUTER JOIN (SELECT
                        en_avte_vlr.car_id,
                        en_avte_vlr.damage,
                        en_avte_vlr.id,
                        wp_users.display_name
                      FROM (SELECT
                        en_avte_vlr.car_id,
                        MAX(en_avte_vlr.id) AS ID
                      FROM en_avte_vlr
                      WHERE COALESCE(en_avte_vlr.auth_status, '') <> 'denied'
                      GROUP BY en_avte_vlr.car_id) LatestPerCar
                        INNER JOIN en_avte_vlr
                          ON LatestPerCar.ID = en_avte_vlr.id
                        INNER JOIN wp_users
                          ON en_avte_vlr.driver_id = wp_users.ID) LatestDamage
                        ON en_avte_cars.vehicle_id = LatestDamage.car_id
                      LEFT OUTER JOIN (SELECT
                        COUNT(en_avte_vlr.id) AS outstanding_vlrs,
                        en_avte_vlr.car_id AS car_id
                      FROM en_avte_vlr
                      WHERE en_avte_vlr.date_returned IS NULL AND COALESCE(en_avte_vlr.auth_status, '') = 'approved'
                      GROUP BY en_avte_vlr.car_id) IsCarReady
                        ON en_avte_cars.vehicle_id = IsCarReady.car_id
                    WHERE LatestDamage.id = %d";
            $dbresult = $wpdb->get_row($wpdb->prepare($sql, $_REQUEST['vlr_id']));
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][0] = 'No vlr found';
            }
        } catch (Exception $e) {
            $json[errors][0] = $e->getMessage();
        }
    }

    //Also include the server's current date
    $curtime =current_time( 'mysql' );
    $cur_date = date_format(date_create_from_format('Y-m-d H:i:s', $curtime ), 'Y-m-d');
    $json['currentdate'] =  $cur_date;

    $json['allcols'] =$dbresult;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_vlr_carlookup(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        try {
            $wpdb->flush();
            $sql = "SELECT
                      en_avte_cars.year,
                      en_avte_cars.make,
                      en_avte_cars.model,
                      en_avte_cars.color,
                      LatestDamage.damage,
                      IsCarReady.outstanding_vlrs
                    FROM en_avte_cars
                      LEFT OUTER JOIN (SELECT
                        en_avte_vlr.car_id,
                        COALESCE(en_avte_vlr.damage_in, en_avte_vlr.damage, NULL) AS damage
                      FROM (SELECT
                        en_avte_vlr.car_id,
                        MAX(en_avte_vlr.id) AS ID
                      FROM en_avte_vlr
                      WHERE COALESCE(en_avte_vlr.auth_status, '') <> 'denied'
                      GROUP BY en_avte_vlr.car_id) LatestPerCar
                        INNER JOIN en_avte_vlr
                          ON LatestPerCar.ID = en_avte_vlr.id) LatestDamage
                        ON en_avte_cars.vehicle_id = LatestDamage.car_id
                      LEFT OUTER JOIN (SELECT
                        COUNT(en_avte_vlr.id) AS outstanding_vlrs,
                        en_avte_vlr.car_id AS car_id
                      FROM en_avte_vlr
                      WHERE en_avte_vlr.date_returned IS NULL AND COALESCE(en_avte_vlr.auth_status, '') <> 'denied'
                      GROUP BY en_avte_vlr.car_id) IsCarReady
                        ON en_avte_cars.vehicle_id = IsCarReady.car_id
                      WHERE en_avte_cars.vehicle_id = '%s'";
            $dbresult = $wpdb->get_row($wpdb->prepare($sql, $_REQUEST['car']));
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][0] = 'No car_id found';
            }
        } catch (Exception $e) {
            $json[errors][0] = $e->getMessage();
        }
    }

    //Also include the server's current date
    $curtime =current_time( 'mysql' );
    $cur_date = date_format(date_create_from_format('Y-m-d H:i:s', $curtime ), 'Y-m-d');
    $json['currentdate'] =  $cur_date;

    $json['allcols'] =$dbresult;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_vlr_checkout(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    global $current_user;

    if ( is_user_logged_in() ) {
        try {
            $wpdb->flush();
            $sql = 'call vlr_checkout ("%s", %s, "%s", "%s", %d, "%s", "%s", "%s")';
            $prep_sql = $wpdb->prepare($sql, $_REQUEST['car'], $current_user->id, $_REQUEST['date_required'],
                $_REQUEST['date_to_return'], $_REQUEST['mileage_out'], $_REQUEST['fuel_out'], $_REQUEST['purpose'], $_REQUEST['damage']);
            $dbresult = $wpdb->get_var($prep_sql);
            if ((0 === $dbresult) || (false === $dbresult) || "0" === $dbresult) {
                $json[errors][0] = 'Car is not ready to checkout';
            } else {
                $damage_pic = prep_damage($_REQUEST['damage']);

                //Email the requestor
                //wp_schedule_single_event( time() + 10, 'en_mail_queue', array(
                en_mail(
                    time(), $current_user->user_email,'VLRRequestCopy',
                    array('%Car ID%' => $_REQUEST['car'],
                        '%Color%' => $_REQUEST['color'],
                        '%Year%' => $_REQUEST['year'],
                        '%Make%' => $_REQUEST['make'],
                        '%Model%' => $_REQUEST['model'],
                        '%Date Required%' => $_REQUEST['date_required'],
                        '%Date to be returned%' => $_REQUEST['date_to_return'],
                        '%Current Mileage%' => $_REQUEST['mileage_out'],
                        '%Current Fuel Level%' => $_REQUEST['fuel_out'],
                        '%Reason for Vehicle Loan Request%' => $_REQUEST['purpose'],
                        '%vlr_id%' => $dbresult,
                        '%action_message%' => 'Here is a copy of your VLR:',
                        '%damage%' => '<img style="display:block;width:350px;height:285px;" alt="damage" src="cid:image-1@GUID"><br>'
                    ),
                    '', '', 'noreply.cecet@intertek.com', 'Electron VLR',  array($damage_pic)
                 //   )
                );

                //Email the managers
                $nonce = WPSimpleNonce::createNonce('auth'.$dbresult);
                $options = get_option('en_vlr_settings');
                //wp_schedule_single_event( time() + 10, 'en_mail_queue', array(
                en_mail(
                    time(), $options['auth_email'],'VLRAdminAuthorization',
                        array('%username%' => $current_user->display_name,
                            '%useremail%' => $current_user->user_email,
                            '%Car ID%' => $_REQUEST['car'],
                            '%Color%' => $_REQUEST['color'],
                            '%Year%' => $_REQUEST['year'],
                            '%Make%' => $_REQUEST['make'],
                            '%Model%' => $_REQUEST['model'],
                            '%Date Required%' => $_REQUEST['date_required'],
                            '%Date to be returned%' => $_REQUEST['date_to_return'],
                            '%Current Mileage%' => $_REQUEST['mileage_out'],
                            '%Current Fuel Level%' => $_REQUEST['fuel_out'],
                            '%Reason for Vehicle Loan Request%' => $_REQUEST['purpose'],
                            '%vlr_id%' => $dbresult,
                            '%approve_link%' => admin_url( 'admin-ajax.php' ) . '?action=ajaxen_avte_vlr_auth&nonce_name='.$nonce['name'].'&nonce_val='.$nonce['value'].'&vlr_id='.$dbresult.'&subaction=approved',
                            '%deny_link%' => admin_url( 'admin-ajax.php' ) . '?action=ajaxen_avte_vlr_auth&nonce_name='.$nonce['name'].'&nonce_val='.$nonce['value'].'&vlr_id='.$dbresult.'&subaction=denied',
                            '%action_message%' => 'You have a new Vehicle Loan Request to authorize:',
                            '%damage%' => '<img style="display:block;width:350px;height:285px;" alt="damage" src="cid:image-1@GUID"><br>'
                        ),
                        'Authorization for VLR - '.$current_user->display_name, '', 'noreply.cecet@intertek.com', 'Electron VLR', array($damage_pic)
                 //   )
                );
            }
        } catch (Exception $e) {
            $json[errors][0] = $e->getMessage();
        }
    }

    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_vlr_checkin(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    global $current_user;

    if ( is_user_logged_in() ) {
        try {
            $curtime =current_time( 'mysql' );
            $wpdb->flush();
            $sql = 'call vlr_checkin (%d, "%s", %d, "%s", "%s", %d, %d, %d, %d, %d, %d, %d, %d, %d, 
            "%s", %d, "%s", %d, "%s", "%s", "%s", %d, "%s", "%s", "%s", "%s", "%s")';
            $prep_sql = $wpdb->prepare($sql, $_REQUEST['vlr_id'], $curtime, $_REQUEST['mileage_in'], $_REQUEST['fuel_in'],
                $_REQUEST['damage_in'], getTrueFalse($_REQUEST['interior_clean']), getTrueFalse($_REQUEST['gauge_full']), getTrueFalse($_REQUEST['exterior_clean']),
                getTrueFalse($_REQUEST['charge_started']), getTrueFalse($_REQUEST['damage_inspected']), getTrueFalse($_REQUEST['driver_log_complete']), getTrueFalse($_REQUEST['fuel_log_complete']),
                getTrueFalse($_REQUEST['issues_reported']), getTrueFalse($_REQUEST['handling_issues']), $_REQUEST['handling_detail'], getTrueFalse($_REQUEST['acceleration_issues']),
                $_REQUEST['acceleration_detail'], getTrueFalse($_REQUEST['braking_issues']), $_REQUEST['braking_detail'], $_REQUEST['range_accuracy'],
                $_REQUEST['range_detail'], getTrueFalse($_REQUEST['console_issues']), $_REQUEST['console_detail'], $_REQUEST['road_noise'],
                $_REQUEST['motor_whine'], $_REQUEST['noise_detail'], $_REQUEST['observations']
            );
            $dbresult = $wpdb->get_var($prep_sql);
            if ((0 === $dbresult) || (false === $dbresult) || "0" === $dbresult) {
                $json[errors][0] = 'Car is not ready to checkout';
            } else {
                $damage_pic = prep_damage($_REQUEST['damage_in']);

                //Email a copy to the manager and user
                $options = get_option('en_vlr_settings');
                //wp_schedule_single_event( time() + 10, 'en_mail_queue', array(
                en_mail(
                    time(), $options['auth_email'].','.$current_user->user_email,'VLRReturn',
                    array('%username%' => $current_user->display_name,
                        '%useremail%' => $current_user->user_email,
                        '%Car ID%' => $_REQUEST['car_id'],
                        '%Color%' => $_REQUEST['color'],
                        '%Year%' => $_REQUEST['year'],
                        '%Make%' => $_REQUEST['make'],
                        '%Model%' => $_REQUEST['model'],
                        '%Current Mileage%' => $_REQUEST['mileage_in'],
                        '%Current Fuel Level%' => $_REQUEST['fuel_in'],
                        '%vlr_id%' => $_REQUEST['vlr_id'],
                        '%action_message%' => 'This car has been returned:',
                        '%Interior is clean & trash removed%' => $_REQUEST['interior_clean'],
                        '%Vehicle fuel gauge reads full%' => $_REQUEST['gauge_full'],
                        '%Exterior is clean & free of mud%' => $_REQUEST['exterior_clean'],
                        '%Charge session started for BEV/PHEV%' => $_REQUEST['charge_started'],
                        '%Vehicle is inspected for any damage%' => $_REQUEST['damage_inspected'],
                        '%Driver log is filled out completely%' => $_REQUEST['driver_log_complete'],
                        '%Fuel log is filled out correctly%' => $_REQUEST['fuel_log_complete'],
                        '%Reported performance issues to manager%' => $_REQUEST['issues_reported'],
                        '%Experienced an issue regarding ride & handling%' => $_REQUEST['handling_issues'].' - '. $_REQUEST['handling_detail'],
                        '%Experienced any issues with vehicle acceleration%' => $_REQUEST['acceleration_issues'].' - '. $_REQUEST['acceleration_detail'],
                        '%Experienced any issues with braking%' => $_REQUEST['braking_issues'].' - '. $_REQUEST['braking_detail'],
                        '%Rate the accuracy of the electric range estimator%' => $_REQUEST['range_accuracy'].' - '. $_REQUEST['range_detail'],
                        '%Experienced issues with the console instrumentation and/or climate control%' => $_REQUEST['console_issues'].' - '. $_REQUEST['console_detail'],
                        '%Experienced road noise%' => $_REQUEST['road_noise'],
                        '%Experienced motor whine%' => $_REQUEST['motor_whine'],
                        '%Road noise & motor whine detail%' => $_REQUEST['noise_detail'],
                        '%Other comments or observations%' => $_REQUEST['observations'],
                        '%damage%' => '<img style="display:block;width:350px;height:285px;" alt="damage" src="cid:image-1@GUID"><br>'
                    ),
                    'VLR Return - '.$current_user->display_name, '', 'noreply.cecet@intertek.com', 'Electron VLR',  array($damage_pic)
                //   )
                );
            }
        } catch (Exception $e) {
            $json[errors][0] = $e->getMessage();
        }
    }

    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

//Auxiliary functions
function prep_damage($damage){
    //prepare Damage picture to be emailed out. Email has a hard time showing background pictures or one image on top of the other,
    //so we are combining them here before sending it out.
    define("WIDTH", 350);
    define("HEIGHT", 285);
    $damage_header_pos= strpos($damage,",",0);
    $damage_image_str=substr($damage,$damage_header_pos+1);
    $vehicle_image_str = file_get_contents(plugins_url( 'img/vehicle-condition-report.gif', __FILE__ ), false);
    $vehicle_image = imagescale (imagecreatefromstring($vehicle_image_str),WIDTH,HEIGHT);
    $damage_image = imagecreatefromstring(base64_decode ($damage_image_str));

    $dest_image = imagecreatetruecolor(WIDTH, HEIGHT);

    //make sure the transparency information is saved
    imagesavealpha($dest_image, true);

    //create a fully transparent background (127 means fully transparent)
    $trans_background = imagecolorallocatealpha($dest_image, 0, 0, 0, 127);

    //fill the image with a transparent background
    imagefill($dest_image, 0, 0, $trans_background);

    //copy each png file on top of the destination (result) png
    imagecopy($dest_image, $vehicle_image, 0, 0, 0, 0, WIDTH, HEIGHT);
    imagecopy($dest_image, $damage_image, 0, 0, 0, 0, WIDTH, HEIGHT);

    ob_start();
    imagepng($dest_image); //This will normally output the image, but because of ob_start(), it won't.
    $damage_pic = ob_get_contents(); //Instead, output above is saved to $contents
    ob_end_clean(); //End the output buffer.

    //destroy all the image resources to free up memory
    imagedestroy($vehicle_image);
    imagedestroy($damage_image);
    imagedestroy($dest_image);

    return $damage_pic;
}

function getTrueFalse($str){
    if ($str == 'true') {
        return 1;
    } else if ($str == 'false'){
        return 0;
    }
    return null;
}

?>
