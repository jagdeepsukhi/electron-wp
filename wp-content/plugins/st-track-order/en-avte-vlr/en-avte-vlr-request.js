// before form is load in FormMaker. FM calls this.
function vlr_before_load() {

    //Get car info from server by sending car_id, and put it into hidden fields, so email and DB can see it
    jQuery.post(
        ajax_sttrackorder_object.ajaxurl,
        {
            'action': 'ajaxen_avte_vlr_carlookup',
            car: urlParam('mycar')
        }
        ,
        function (res) {
            //alert('The server responded: ' + res);
            var data = JSON.parse(res);

            if (data.error == true) {
                alert('Error looking up car_id');
                return false;
            }

            //double check to see if car is still available
            if (data.allcols.outstanding_vlrs) {
                if (data.allcols.outstanding_vlrs > 0){
                    alert ("Sorry, the car appears to be no longer available.");
                    window.location = "/vehicle-loan-car-list/";
                    return false;
                }
            }

            //Set the fields from values in DB response
            var mycar = jQuery("input[etl='car_id']");
            mycar.attr("title", urlParam("mycar"));
            mycar.attr("value", urlParam("mycar"));
            mycar.val(urlParam("mycar"));
            mycar.prop('readonly', true);

            var myyear = jQuery("input[etl='year']");
            myyear.attr("title", data.allcols.year);
            myyear.attr("value", data.allcols.year);
            myyear.val(data.allcols.year);
            myyear.prop('readonly', true);

            var mymake = jQuery("input[etl='make']");
            mymake.attr("title", data.allcols.make);
            mymake.attr("value", data.allcols.make);
            mymake.val(data.allcols.make);
            mymake.prop('readonly', true);

            var mymodel = jQuery("input[etl='model']");
            mymodel.attr("title", data.allcols.model);
            mymodel.attr("value", data.allcols.model);
            mymodel.val(data.allcols.model);
            mymodel.prop('readonly', true);

            var mycolor = jQuery("input[etl='color']");
            mycolor.attr("title", data.allcols.color);
            mycolor.attr("value", data.allcols.color);
            mycolor.val(data.allcols.color);
            mycolor.prop('readonly', true);

            var myrequired_date = jQuery("div.etl_daterequired > input[class='wdform-date']");
            myrequired_date.val(data.currentdate);

            //Set the damage image, if any
            if (data.allcols.damage) {
                sigImage.src = data.allcols.damage;
                sigImage.onload = function() {
                    clearCanvas();
                    ctx.drawImage(sigImage,0,0);
                    sigText.val(data.allcols.damage);
                    previous_sigText.val(data.allcols.damage);
                }
            }
        }
    );

}

// before form submit in FormMaker. FM calls this.
function vlr_before_submit() {
    //I am not sure, but it looks like the dents info is too large to send via the regular mySQL mapping (it just was not working, with no indication of where the error was), so sending it this way.

    //figure out what to say about purpose
    var purpose_dropbox = jQuery("select[etl='purpose']").val(),
        purpose_detail = jQuery("textarea[etl='purpose_detail']").val(),
        purpose = '';
    if (purpose_dropbox == 'Special Other'){
        purpose = purpose_dropbox + " - " + purpose_detail;
    } else {
        purpose = purpose_dropbox;
    }


    if (this[ajax_sttrackorder_object.request_checker](1)) {
        //also not sure why, but the regular way (jQuery.post) was not working on iOS, even if it worked just above, but this way works
        jQuery.ajax({
            url: ajax_sttrackorder_object.ajaxurl,
            dataType: 'json',
            async: false,
            type: 'post',
            data: {
                'action': 'ajaxen_avte_vlr_checkout',
                car: urlParam('mycar'),
                date_required: jQuery("div.etl_daterequired > input[class='wdform-date']").val(),
                date_to_return: jQuery("div.etl_date_to_return > input[class='wdform-date']").val(),
                mileage_out: jQuery("input[etl='mileage_out']").val(),
                fuel_out: jQuery("select[etl='fuel_out']").val(),
                purpose: purpose,
                damage: jQuery("input[etl='drawingURL']").val(),
                year: jQuery("input[etl='year']").val(),
                make: jQuery("input[etl='make']").val(),
                model: jQuery("input[etl='model']").val(),
                color: jQuery("input[etl='color']").val()
            },
            success: function (data) {
                //alert('The server responded: ' + JSON.stringify(data));
                if (data.error == true) {
                    alert('Sorry, we were not able to checkout the car due to the following error: ' + data.errors[0]);
                } else {
                    alert('Thanks, the request has been received and managers have been requested to give authorization. You will receive an email when the request has been reviewed.');
                }
            }
        });
    }

}
