// before form is load in FormMaker. FM calls this.
function before_load() {
    //Get car info from server by sending car_id, and put it into hidden fields, so email and DB can see it
    jQuery.post(
        ajax_sttrackorder_object.ajaxurl,
        {
            'action': 'ajaxen_avte_vlr_vlrlookup',
            vlr_id: urlParam('vlr_id')
        }
        ,
        function (res) {
            //alert('The server responded: ' + res);
            var data = JSON.parse(res);

            if (data.error == true) {
                alert('Error looking up vlr');
                return false;
            }

            //double check to see if car is still ready for check-in
            var check_in_ready = false;
            if (data.allcols) {
                if (data.allcols.outstanding_vlrs){
                    check_in_ready = true;
                }
            }
            if (!(check_in_ready)) {
                    alert ("Sorry, the car appears to have been checked in after the latest screen refresh.");
                    window.location = "/vehicle-loan-car-list/";
                    return false;
            }

            //Set the fields from values in DB response
            var mydriver = jQuery("input[etl='driver']");
            mydriver.attr("title", data.allcols.display_name);
            mydriver.attr("value", data.allcols.display_name);
            mydriver.val(data.allcols.display_name);
            mydriver.prop('readonly', true);

            var mycar = jQuery("input[etl='car_id']");
            mycar.attr("title", data.allcols.vehicle_id);
            mycar.attr("value", data.allcols.vehicle_id);
            mycar.val(data.allcols.vehicle_id);
            mycar.prop('readonly', true);

            var myyear = jQuery("input[etl='year']");
            myyear.attr("title", data.allcols.year);
            myyear.attr("value", data.allcols.year);
            myyear.val(data.allcols.year);
            myyear.prop('readonly', true);

            var mymake = jQuery("input[etl='make']");
            mymake.attr("title", data.allcols.make);
            mymake.attr("value", data.allcols.make);
            mymake.val(data.allcols.make);
            mymake.prop('readonly', true);

            var mymodel = jQuery("input[etl='model']");
            mymodel.attr("title", data.allcols.model);
            mymodel.attr("value", data.allcols.model);
            mymodel.val(data.allcols.model);
            mymodel.prop('readonly', true);

            var mycolor = jQuery("input[etl='color']");
            mycolor.attr("title", data.allcols.color);
            mycolor.attr("value", data.allcols.color);
            mycolor.val(data.allcols.color);
            mycolor.prop('readonly', true);

            //Set the damage image, if any
            if (data.allcols.damage) {
                sigImage.src = data.allcols.damage;
                sigImage.onload = function() {
                    clearCanvas();
                    ctx.drawImage(sigImage,0,0);
                    sigText.val(data.allcols.damage);
                    previous_sigText.val(data.allcols.damage);
                }
            }
        }
    );

}

// before form submit in FormMaker. FM calls this.
function before_submit() {
    //I am not sure, but it looks like the dents info is too large to send via the regular mySQL mapping (it just was not working, with no indication of where the error was), so sending it this way.
    //also not sure why, but the regular way (jQuery.post) was not working on iOS, even if it worked just above, but this way works

    if (this[ajax_sttrackorder_object.return_checker](1)) {
        var handling_issues_y = jQuery("div.etl_handling_issues input[value='yes']").prop("checked"),
            handling_issues_n = jQuery("div.etl_handling_issues input[value='no']").prop("checked"),
            acceleration_issues_y = jQuery("div.etl_acceleration_issues input[value='yes']").prop("checked"),
            acceleration_issues_n = jQuery("div.etl_acceleration_issues input[value='no']").prop("checked"),
            braking_issues_y = jQuery("div.etl_braking_issues input[value='yes']").prop("checked"),
            braking_issues_n = jQuery("div.etl_braking_issues input[value='no']").prop("checked"),
            console_issues_y = jQuery("div.etl_console_issues input[value='yes']").prop("checked"),
            console_issues_n = jQuery("div.etl_console_issues input[value='no']").prop("checked");

        jQuery.ajax({
            url: ajax_sttrackorder_object.ajaxurl,
            dataType: 'json',
            async: false,
            type: 'post',
            data: {
                'action': 'ajaxen_avte_vlr_checkin',
                vlr_id: urlParam('vlr_id'),
                mileage_in: jQuery("input[etl='mileage_in']").val(),
                fuel_in: jQuery("select[etl='fuel_in']").val(),
                damage_in: jQuery("input[etl='drawingURL']").val(),
                interior_clean: jQuery("div.etl_interior_clean input[type='checkbox']").prop("checked"),
                gauge_full: jQuery("div.etl_gauge_full input[type='checkbox']").prop("checked"),
                exterior_clean: jQuery("div.etl_exterior_clean input[type='checkbox']").prop("checked"),
                charge_started: jQuery("div.etl_charge_started input[type='checkbox']").prop("checked"),
                damage_inspected: jQuery("div.etl_damage_inspected input[type='checkbox']").prop("checked"),
                driver_log_complete: jQuery("div.etl_driver_log_complete input[type='checkbox']").prop("checked"),
                fuel_log_complete: jQuery("div.etl_fuel_log_complete input[type='checkbox']").prop("checked"),
                issues_reported: jQuery("div.etl_issues_reported input[type='checkbox']").prop("checked"),
                handling_issues: getYesNo(handling_issues_y, handling_issues_n),
                handling_detail: jQuery("textarea[etl='handling_detail']").val(),
                acceleration_issues: getYesNo(acceleration_issues_y, acceleration_issues_n),
                acceleration_detail: jQuery("textarea[etl='acceleration_detail']").val(),
                braking_issues: getYesNo(braking_issues_y, braking_issues_n),
                braking_detail: jQuery("textarea[etl='braking_detail']").val(),
                range_accuracy: getRating(jQuery("div[etl='range_accuracy'] input[type='radio']")),
                range_detail: jQuery("textarea[etl='range_detail']").val(),
                console_issues: getYesNo(console_issues_y, console_issues_n),
                console_detail: jQuery("textarea[etl='console_detail']").val(),
                road_noise: getRating(jQuery("div[etl='road_noise'] input[type='radio']")),
                motor_whine: getRating(jQuery("div[etl='motor_whine'] input[type='radio']")),
                noise_detail: jQuery("textarea[etl='noise_detail']").val(),
                observations: jQuery("textarea[etl='observations']").val(),
                year: jQuery("input[etl='year']").val(),
                make: jQuery("input[etl='make']").val(),
                model: jQuery("input[etl='model']").val(),
                color: jQuery("input[etl='color']").val(),
                car_id: jQuery("input[etl='car_id']").val()
            },
            success: function (data) {
                //alert('The server responded: ' + JSON.stringify(data));
                if (data.error == true) {
                    alert('Sorry, we were not able to check-in the car due to the following error: ' + data.errors[0]);
                } else {
                    alert('Thanks, the car has been checked-in.');
                }
            }
        });
    }

}

// before form reset in FormMaker. FM calls this.
function before_reset() {

}