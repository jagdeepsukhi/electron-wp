<?php

function en_vlr_admin_form()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    ?>
    <div>
        <h1>Electron - VLR Settings</h1>
        <!-- notification on success -->
        <?php if ( isset($_GET['settings-updated']) ) { ?>
            <div class="notice notice-success is-dismissible">
                <p>Settings saved!</p>
            </div>
        <?php } ?>
        <form action="options.php" method="post">
            <?php settings_fields('en_vlr_settings'); ?>
            <?php do_settings_sections('en-vlr'); ?>
            <input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>"/>
        </form>
    </div>
    <?php
}

function en_vlr_admin_section_cb($arg) {
    switch ($arg['id']) {
        case 'en_vlr_main':
            echo '<p>These are settings that are general to all of VLR.</p>';
            break;
        case 'en_vlr_request':
            echo '<p>These are settings that are specific for the REQUEST portion of the VLR.</p>';
            break;
        case 'en_vlr_return':
            echo '<p>These are settings that are specific for the RETURN portion of the VLR.</p>';
            break;
    }
}

function en_vlr_admin_field_cb($arg) {
    $options = get_option('en_vlr_settings');

    switch ($arg[0]) {
        case 'en_vlr_auth_email':
            echo en_text_admin_field($arg[0], 'en_vlr_settings[auth_email]', '100',
                'john.smith@intertek.com,jane.smith@intertek.com', $options['auth_email'],
                'These are the people that get emailed with new authorization requests, and also when a car gets returned. Use comma (no spaces) to include multiple people.');
            break;
        case 'en_vlr_request_form':
            echo en_text_admin_field($arg[0], 'en_vlr_settings[request_form]', '100',
                'e.g. 21', $options['request_form'],
                'Enter here the ID of the FormMaker form used for VLR Requests.');
            break;
        case 'en_vlr_return_form':
            echo en_text_admin_field($arg[0], 'en_vlr_settings[return_form]', '100',
                'e.g. 21', $options['return_form'],
                'Enter here the ID of the FormMaker form used for VLR Returns.');
            break;
    }
}

function en_avte_vlr_return_form()
{
    global $current_user;
    $vlr_id = $_GET['vlr_id'];
    $options = get_option('en_vlr_settings');

    ob_start();

    if (is_user_logged_in()) {
        if ($vlr_id != null) {
            $user = $current_user;
            if (isset($user->roles) && is_array($user->roles)) {
                $ADorg = get_user_option('ad_integration_account_suffix');
                if (
                    (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) &&
                    (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
                ) {
                    wd_form_maker($options['return_form']);
                } else {
                    ?>
                    <div class="en_avte_vlr_form">
                        Sorry, you don't have the right roles assigned to return a car.
                    </div> <!--en_avte_vlr_form-->
                    <?php
                }
            }
        } else {
            ?>
            <br>
            <div class="en_avte_vlr_form">
                Hmm, you can't get to this page this way. Perhaps you meant to come <a href="/vehicle-loan-car-list/">HERE</a> instead?
            </div> <!--en_avte_vlr_form-->
            <br>
            <?php
        }
    } else {
        ?>
        <br>
        <div class="en_avte_vlr_form">
            Sorry, you need to be logged in. You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_vlr_form-->
        <br>
        <?php
    }

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function en_avte_vlr_form()
{
    global $current_user;
    $mycar = $_GET['mycar'];
    $options = get_option('en_vlr_settings');

    ob_start();

        if (is_user_logged_in()) {
            if ($mycar != null) {
                $user = $current_user;
                if (isset($user->roles) && is_array($user->roles)) {
                    $ADorg = get_user_option('ad_integration_account_suffix');
                    if (
                        (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) &&
                        (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
                    ) {
                        wd_form_maker($options['request_form']);
                    } else {
                        ?>
                        <div class="en_avte_vlr_form">
                            Sorry, you don't have the right roles assigned to request a car.
                        </div> <!--en_avte_vlr_form-->
                        <?php
                    }
                }
            } else {
                ?>
                <br>
                <div class="en_avte_vlr_form">
                    Hmm, you can't get to this page this way. Perhaps you meant to come <a href="/vehicle-loan-car-list/">HERE</a> instead?
                </div> <!--en_avte_vlr_form-->
                <br>
                <?php
            }
        } else {
            ?>
            <br>
            <div class="en_avte_vlr_form">
                Sorry, you need to be logged in. You can do that <a href="/logmein/">HERE</a>
            </div> <!--en_avte_vlr_form-->
            <br>
            <?php
        }

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function en_avte_vlr_list_form()
{
    global $current_user;
    ob_start();

    if (is_user_logged_in()) {
            $user = $current_user;
            if (isset($user->roles) && is_array($user->roles)) {
                $ADorg = get_user_option('ad_integration_account_suffix');
                if (
                    (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) &&
                    (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
                ) {
                    ?>
                    <br>
                    [wpdatatable id=1]
                    <br>
                    <?php
                } else {
                    ?>
                    <br>
                    <div class="en_avte_vlr_list_form">
                        Sorry, you don't have the right roles assigned to perform this action.
                    </div> <!--en_avte_vlr_form-->
                    <br>
                    <?php
                }
            }
    } else {
        ?>
        <br>
        <div class="en_avte_vlr_list_form">
            Sorry, you need to be logged in. You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_vlr_form-->
        <br>
        <?php
    }

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function en_avte_vlr_auth_form()
{
    global $current_user;
    global $wpdb;
    $myvlr = $_GET['vlr_id'];

    ob_start();

    if (is_user_logged_in()) {
        if ($myvlr != null) {
            $user = $current_user;
            if (isset($user->roles) && is_array($user->roles)) {
                $ADorg = get_user_option('ad_integration_account_suffix');
                if (
                    (doesUserHavePermissionToEnterPage($user, array('administrator', 'fm'))) &&
                    (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
                ) {
                    try {
                        $wpdb->flush();
                        $sql = "SELECT
                      en_avte_vlr.auth_status,
                      wp_users.user_email,
                      en_avte_cars.year,
                      en_avte_vlr.car_id,
                      en_avte_cars.make,
                      en_avte_cars.model,
                      en_avte_cars.color,
                      wp_users.display_name,
                      en_avte_vlr.date_required,
                      en_avte_vlr.date_to_return,
                      en_avte_vlr.mileage_out,
                      en_avte_vlr.fuel_out,
                      en_avte_vlr.purpose,
                      en_avte_vlr.damage
                    FROM en_avte_vlr
                      LEFT OUTER JOIN en_avte_cars
                        ON en_avte_vlr.car_id = en_avte_cars.vehicle_id
                      LEFT OUTER JOIN wp_users
                        ON en_avte_vlr.driver_id = wp_users.ID
                    WHERE en_avte_vlr.id = %d";
                        $dbresult = $wpdb->get_row($wpdb->prepare($sql, $myvlr));
                        if ((0 === $dbresult) || (false === $dbresult)) {
                            echo 'You should never see this... VLR not found on lookup.';
                        } else {
                            if ($dbresult->auth_status === null) {
                                $page = get_page_by_title( 'Email-VLRAdminAuthorization' );
                                $message = do_shortcode(get_post_field( 'post_content', $page ));
                                $param = array('%username%' => $dbresult->display_name,
                                    '%useremail%' => '<a href="mailto:'.$dbresult->user_email.'?Subject=Regarding VLR Request '.$myvlr.'">Send Mail</a>',
                                    '%Car ID%' => $dbresult->car_id,
                                    '%Color%' => $dbresult->color,
                                    '%Year%' => $dbresult->year,
                                    '%Make%' => $dbresult->make,
                                    '%Model%' => $dbresult->model,
                                    '%Date Required%' => $dbresult->date_required,
                                    '%Date to be returned%' => $dbresult->date_to_return,
                                    '%Current Mileage%' => $dbresult->mileage_out,
                                    '%Current Fuel Level%' => $dbresult->fuel_out,
                                    '%Reason for Vehicle Loan Request%' => $dbresult->purpose,
                                    '%vlr_id%' => $myvlr,
                                    '%approve_link%' => admin_url( 'admin-ajax.php' ) . '?action=ajaxen_avte_vlr_auth&vlr_id='.$myvlr.'&subaction=approved',
                                    '%deny_link%' => admin_url( 'admin-ajax.php' ) . '?action=ajaxen_avte_vlr_auth&&vlr_id='.$myvlr.'&subaction=denied',
                                    '%action_message%' => '<br>',
                                    '%damage%' => '<div style="border: 2px dotted #CCCCCC; border-radius: 5px; 
background-image: url(' . plugins_url( 'img/vehicle-condition-report.gif', __FILE__ ) . '); background-size: 350px;background-repeat: no-repeat; width: 350px;height: 285px;">	 	 
<img style="display:block; width:350px;height:285px;" id="base64image" src="'.$dbresult->damage.'"></div>'
                                );
                                foreach ($param as $key => $subarr) {
                                    $message = str_replace($key,$subarr,$message);
                                }
                                echo $message;
                            } else {
                                echo 'Thanks, nothing to do. It looks like this request was taken care of since the screen refreshed.';
                            }
                        }

                    } catch (Exception $e) {
                        echo 'Database error while looking up VLR: ' . $e->getMessage() . '';
                    }
                } else {
                    ?>
                    <br>
                    <div class="en_avte_vlr_auth_form">
                        Sorry, you don't have the right roles assigned to authorize VLRs.
                    </div> <!--en_avte_vlr_form-->
                    <br>
                    <?php
                }
            }
        } else {
            ?>
            <br>
            <div class="en_avte_vlr_auth_form">
                Hmm, you can't get to this page this way. Perhaps you meant to come <a href="/vehicle-loan-car-list/">HERE</a> instead?
            </div> <!--en_avte_vlr_form-->
            <br>
            <?php
        }
    } else {
        ?>
        <br>
        <div class="en_avte_vlr_auth_form">
            Sorry, you need to be logged in. You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_vlr_form-->
        <br>
        <?php
    }

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>