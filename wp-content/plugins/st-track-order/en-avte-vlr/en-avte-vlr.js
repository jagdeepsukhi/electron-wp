//gives ability to get parameters at the URL
function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}

function getYesNo(y,n){
    var myret = null;
    if ((y==true) && (n==false)) {
        myret = true;
    } else if ((y==false) && (n==true)) {
        myret = false;
    }
    return myret;
}

function getRating(rates){
    var myret = null;

    rates.each(function( index ) {
        if (this.checked == true) {
            myret = index+1;
            return false; //this is just so it exits the each loop... requirement
        }
    });
    return myret;
}

//Setup the functions and elements for the car drawing
// Get a regular interval for drawing to the screen
window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function (callback) {
                window.setTimeout(callback, 1000/60);
            }
            ;
    }
)();

// Set up the canvas
var canvas = document.getElementById("sig-canvas");
var ctx = canvas.getContext("2d");
ctx.strokeStyle = "#ff0000";
ctx.lineWith = 2;

// Set up the UI
var sigText = jQuery("input[etl='drawingURL']");
var previous_sigText = jQuery("input[etl='previous_drawingURL']");
var sigImage = document.getElementById("sig-image");
var clearBtn = document.getElementById("sig-clearBtn");
var submitBtn = document.getElementById("sig-submitBtn");
clearBtn.addEventListener("click", function (e) {
        clearCanvas();
        sigText.val("blank");
        //sigImage.setAttribute("src", "");
    }
    , false);
submitBtn.addEventListener("click", function (e) {
        sigImage.src = previous_sigText.val();
        sigImage.onload = function() {
            clearCanvas();
            ctx.drawImage(sigImage,0,0);
            sigText.val(previous_sigText.val());
        }

    }
    , false);

// Set up mouse events for drawing
var drawing = false;
var mousePos = {
        x:0, y:0 }
    ;
var lastPos = mousePos;
canvas.addEventListener("mousedown", function (e) {
        drawing = true;
        lastPos = getMousePos(canvas, e);
    }
    , false);
canvas.addEventListener("mouseup", function (e) {
        drawing = false;
        previous_sigText.val(sigText.val());
        var dataUrl = canvas.toDataURL();
        sigText.val(dataUrl);
    }
    , false);
canvas.addEventListener("mousemove", function (e) {
        mousePos = getMousePos(canvas, e);
    }
    , false);

// Set up touch events for mobile, etc
canvas.addEventListener("touchstart", function (e) {
        mousePos = getTouchPos(canvas, e);
        var touch = e.touches[0];
        var mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            }
        );
        canvas.dispatchEvent(mouseEvent);
    }
    , false);
canvas.addEventListener("touchend", function (e) {
        var mouseEvent = new MouseEvent("mouseup", {
            }
        );
        canvas.dispatchEvent(mouseEvent);
    }
    , false);
canvas.addEventListener("touchmove", function (e) {
        var touch = e.touches[0];
        var mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            }
        );
        canvas.dispatchEvent(mouseEvent);
    }
    , false);

// Prevent scrolling when touching the canvas
document.body.addEventListener("touchstart", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }
    , false);
document.body.addEventListener("touchend", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }
    , false);
document.body.addEventListener("touchmove", function (e) {
        if (e.target == canvas) {
            e.preventDefault();
        }
    }
    , false);

// Get the position of the mouse relative to the canvas
function getMousePos(canvasDom, mouseEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
    }
        ;
}

// Get the position of a touch relative to the canvas
function getTouchPos(canvasDom, touchEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
    }
        ;
}

// Draw to the canvas
function renderCanvas() {
    if (drawing) {
        ctx.moveTo(lastPos.x, lastPos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        lastPos = mousePos;
    }
}

function clearCanvas() {
    canvas.width = canvas.width;
    ctx.strokeStyle = "#ff0000";
    ctx.lineWith = 2;
}

// Allow for animation
(function drawLoop () {
        requestAnimFrame(drawLoop);
        renderCanvas();
    }
)();

