<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-maintenancedetail-form.php";
    wp_register_script('en-avte-maintenancedetail-js', ISTO_URL . "/en-avte-maintenancedetail/en-avte-maintenancedetail.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-maintenancedetail/en-avte-maintenancedetail.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_avte_maintenancedetail_new', 'ajax_en_avte_maintenancedetail_new');
    add_action('wp_ajax_ajaxen_avte_maintenancedetail_change', 'ajax_en_avte_maintenancedetail_change');
    add_action('wp_ajax_ajaxen_avte_maintenancedetail', 'ajax_en_avte_maintenancedetail');
    add_action('wp_ajax_ajaxen_avte_maintenancedetail_deleterecord', 'ajax_en_avte_maintenancedetail_deleterecord');
    add_action('wp_ajax_ajaxen_avte_maintenance_items_select_new_onTheFly', 'ajax_en_avte_maintenance_items_select_new_onTheFly');

    add_shortcode('en_avte_maintenancedetail', 'en_avte_maintenancedetail');
    function en_avte_maintenancedetail($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');

        $output = en_avte_maintenancedetail_form();
        return $output;
    }

function ajax_en_avte_maintenancedetail(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_maintenancedetail'
                    ORDER BY st_default_cols.`order`";
            }
         }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT
                  en_avte_maintenancedetail.id,
                  en_avte_maintenancedetail.item_cost,
                  en_avte_maintenancedetail.maintenance_items_performed_ID,
                  en_avte_maintenancedetail.maintenance_record_id,
                  en_avte_maintenancedetail.notes,
                  en_avte_maintenance_items_select.maintenance_item AS maintenance_items_performed
                FROM en_avte_maintenancedetail
                  LEFT OUTER JOIN en_avte_maintenance_items_select
                    ON en_avte_maintenancedetail.maintenance_items_performed_ID = en_avte_maintenance_items_select.id
                WHERE en_avte_maintenancedetail.maintenance_record_id = '%s'
                ORDER BY en_avte_maintenancedetail.id";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['maintenancerecord']), OBJECT );
    $dbdata = make_date($dbdata);
    $col_plus_data = array_merge ($transposed, $dbdata);


    // query selectable items list for maintenance detail dropdown menu
    $sqlSelectMaintenanceItems = "SELECT
                                  en_avte_maintenance_items_select.maintenance_item
                                  FROM en_avte_maintenance_items_select
                                  WHERE en_avte_maintenance_items_select.show_in_list = 'true'
                                  ORDER BY en_avte_maintenance_items_select.id, en_avte_maintenance_items_select.show_in_list DESC";

    $maintenance_items_selection = $wpdb->get_results( $sqlSelectMaintenanceItems, OBJECT );
    $maintenance_items_selection = make_date($maintenance_items_selection);


    //convert $maintenance_items_selection into an array form that can be used in the cell render on the javasctipt side
    $itemrow = 0;
    $maintenance_items_selection_array = [];
    foreach($maintenance_items_selection as $item){
        $maintenance_items_selection_array[$itemrow] = $maintenance_items_selection[$itemrow]->maintenance_item;
        $itemrow++;
    }

    //Send Response
    $json['allcols'] =$col_plus_data;
    $json['maintenance_items_selection'] = $maintenance_items_selection_array;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_maintenancedetail_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

    //Handle each change
    $cur_time = current_time( 'mysql' );

    for ($i = 1; $i <= $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
	        if ($_POST['addType'] != "UndoRedo.undo") {
		        $dbresult = $wpdb->insert('en_avte_maintenancedetail',
			        array(
				        'maintenance_record_id' => $_POST['maintenancerecord'],
			        )
		        );
	        } else{
		        $dbresult = $wpdb->insert('en_avte_maintenancedetail',
			        array(
				        'id' => $_POST['idsToAddBackIn'][$i],
				        'maintenance_record_id' => $_POST['maintenancerecord']
			        )
		        );
	        }

            $new_id[] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][$i] = true;
            }
            else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => 'NEW',
                    'table_id' => $_POST['maintenancerecord'],
                    'car_id' => $_POST['car']
                );
                $wpdb->insert( 'en_avte_maintenance_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['newid'] = $new_id;
    $json['user'] = $current_user;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_maintenancedetail_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_maintenancedetail'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    $dbindex = 0;
//See if they are allowed to delete this record
////Make the deletion
                foreach ($_POST['id'] as $id) {
                    if (allowed_to_change('id', $rolemap, $user->roles)) {
                    try {
                        $wpdb->flush();
                        $sql = "DELETE FROM en_avte_maintenancedetail
	            WHERE id = '%s'";
                        $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
                        if ((0 === $dbresult) || (false === $dbresult)) {

                            $json[errors][$dbindex] = true;

                        }
                    } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                    }

//                //Log the deletion
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => 'DETAIL RECORD DELETED',
                        'before' => 'Detail Record Active',
                        'after' => 'Detail Record Deleted',
                        'source' => 'Delete',
                        'table_id' => $_POST['maintenancerecord'],
                        'car_id' => $_POST['car']
                    );
                    $wpdb->insert('en_avte_maintenance_history', $dbdata);
                }

             else {
                //user did not have permissiong to make deletion
                $json[errors][$dbindex] = 'role';
            }

    $dbindex++;
}

 //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['user'] = $current_user;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_maintenancedetail_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_maintenancedetail'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

    	$saveChangedToText = $change[3];

        if($change[1] == "maintenance_items_performed") {
        // query selectable items list for maintenance detail dropdown menu
        $sqlSelectMaintenanceItemsID = "SELECT
                                        en_avte_maintenance_items_select.id
                                        FROM en_avte_maintenance_items_select
                                        WHERE en_avte_maintenance_items_select.maintenance_item = '%s'";

        $maintenance_items_selection_ID = $wpdb->get_results( $wpdb->prepare( $sqlSelectMaintenanceItemsID, $change[3]), object);
        $change[1] = "maintenance_items_performed_ID";
        $change[3] = $maintenance_items_selection_ID[0]->id;
        }

        $logit = true;
        //skip ID change notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles))) {
                //Make the change
                try {
                    $wpdb->flush();
                    //wpdb->prepare does not accept nulls, so we can't use it here.
                    $table = "en_avte_maintenancedetail";
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]) );
                    if ( false === $dbresult ) {

                            $json[errors][$dbindex] = true;

                    }
                }
                catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }

                //Log the change
                if ($logit) {
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => $change[1],
                        'before' => $change[2],
                        'after' => $saveChangedToText,
                        'source' => $_POST['source'],
                        'table_id' => $_POST['maintenancerecord'],
                        'car_id' => $_POST['car']
                    );
                    $wpdb->insert('en_avte_maintenance_history', $dbdata);
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = 'role';
            }
        }
        //user tried to change record "id" which is NEVER permissible
       else{ $json[errors][$dbindex] = true;}
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['maintenance_items_selection_ID'] = $maintenance_items_selection_ID;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_maintenance_items_select_new_onTheFly(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

    $cur_time = current_time( 'mysql' );

        //Now, let's insert a new record
        try {
            $wpdb->flush();
            $dbresult = $wpdb->insert('en_avte_maintenance_items_select',
                array(
                    // 'create_time' => $cur_time,
                    'maintenance_item' => $_POST['description_item'][0][3],
                    'show_in_list' => $_POST['add_to_dropdown'],
                    //'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                )
            );
            $new_id[] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][1] = true;
            }
            else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => $_POST['description_item'][0][3],
                    'source' => 'NEW',
                    'table_id' => "Edit Maintenance Title Options: {$new_id[1-1]}",
                    'car_id' => 'N/A'
                );
                $wpdb->insert( 'en_avte_maintenance_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][1] = $e->getMessage();
        }

    //Send Response
    $json['newid'] = $new_id;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>