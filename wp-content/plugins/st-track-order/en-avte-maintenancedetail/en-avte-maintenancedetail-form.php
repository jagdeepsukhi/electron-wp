<?php


function en_avte_maintenancedetail_form()
{
    global $current_user, $wpdb;
    $maintenanceRecord = $_GET['mymaintrec'];

    ob_start();
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'fm'))) {
                    wp_enqueue_script('en-avte-maintenancedetail-js');
                    ?>
                    <h3>Maintenance Record:  <?php echo $maintenanceRecord; ?></h3>
                    <A HREF="#BOTTOM">Go to bottom of page</A><BR>
                    <div class="en_avte_maintenancedetail_form">
                      <!--  Search: <input id="en_avte_maintenancedetail_searchgrid" type="text" />  -->
                        <div id="en_avte_maintenancedetail_grid" class="dataTable"></div>
                       <!-- <div id="en_avte_maintenancedetail_paginggrid" class="pagination"></div> -->
                    </div> <!--en_avte_maintenancedetail_form-->
                    <button onclick="window.history.back()">Return to Maintenance</button>
                    <h5></h5>

                    <i>In the event that no changes were made the "Return to Maintenance" button can still be used to navigate back to the Maintenance Table.</i></string>
                    <br/>
                    <br/>
                    <a name="BOTTOM"></a>

                <!--"Tool for Finding An Obscure vehicle maintenance Item that was entered previously"-->
                    <?php
                    // query selectable items list for maintenance detail dropdown menu
                    $sqlAllMaintenanceItems = "SELECT
                                                  en_avte_maintenance_items_select.maintenance_item
                                                  FROM en_avte_maintenance_items_select
                                                  ORDER BY en_avte_maintenance_items_select.id, en_avte_maintenance_items_select.show_in_list DESC";

                    $maintenance_items_all = $wpdb->get_results( $sqlAllMaintenanceItems, OBJECT );

                    // convert $maintenance_items_all to string for use in html list
                    $i=0;
                    $maintenance_items_all_string = "";
                    foreach($maintenance_items_all AS $maintencanceItem) {

                        // finds special characters (') within maintenance item string to add special character indicator "\"
                        if ((strpos($maintencanceItem->maintenance_item, '\'') == true) && (strpos($maintencanceItem->maintenance_item, '\\\'') != true)) {
                            $pos = strpos($maintencanceItem->maintenance_item,"'",0);
                            $maintencanceItem->maintenance_item = substr_replace($maintencanceItem->maintenance_item,"\\",$pos,0);
                            }

                        if ($i != 0){$maintenance_items_all_string= $maintenance_items_all_string . ", ";
                        }
                        $maintenance_items_all_string = $maintenance_items_all_string . "'" . $maintencanceItem->maintenance_item . "'";
                        $i++;
                    }
                     ?>

    <!--            Text box for entering search keywords of past entry-->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>jQuery UI Autocomplete - Default functionality</title>
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
                    <!--                      <link rel="stylesheet" href="/resources/demos/style.css">-->
                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
                    <script>
                        $( function() {
                            var availableTags =

                                [ <?php echo $maintenance_items_all_string; ?> ]

                            $( "#tags" ).autocomplete({
                                source: availableTags
                            });
                        } );
                    </script>

                    <div class="ui-widget">
                        <label for="tags">Search Tool for obscure maintenance items: </label>
                        <input id="tags" onKeyDown="if(event.keyCode==13);" value = "" placeholder = "*****Type Key Words Here*****" size ="100">
                    </div>

                        <?php

            } else {
                ?>
                <div class="en_avte_maintenancedetail_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_maintenancedetail_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_maintenancedetail_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_maintenancedetail_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>