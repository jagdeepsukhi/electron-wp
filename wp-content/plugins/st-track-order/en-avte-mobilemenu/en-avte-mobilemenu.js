
jQuery(document).ready(function($) {
    if (ajax_sttrackorder_object.IsIntertek == false)
    {
        jQuery("button[etl='vlr']").hide();
    }
});

// The Functions below allow the mobile user to navigate from the mobile menu page by pressing a button that links to
// the urls below.

function fuel(){
    window.location.assign("/avte-vehicle-log/fuel-entry/" + urlParam('mycar'));
}

function charge(){
    window.location.assign("/avte-vehicle-log/charge-entry/" + urlParam('mycar'));
}

function vlr(){
    window.location.assign(ajax_sttrackorder_object.ajaxurl + "?action=ajaxen_avte_vlr_donext&car=" + urlParam2('mycar'));
}

// Pulls specific vehicle identifying information from the url that is read from the QR code on the vehicle
function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[0] || 0;
    }
}

//Get parameters from url
function urlParam2 (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        //Remove %20 space indicator from url parameters
        while (results[1].indexOf("%20") !=- 1) {
            results[1] = results[1].replace("%20", " ");
        }
        return results[1] || 0;
    }
}