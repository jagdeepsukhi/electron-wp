<?php


function en_avte_mobilemenu_form()
{
    global $current_user;
    $car = $_GET['mycar'];
    ob_start();
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) {
                if ( wp_is_mobile() == true ){
                    wp_enqueue_script('en-avte-mobilemenu-js');
                    ?>
                    <h5>Vehicle ID: <?php echo $car; ?></h5>
                    <?php
                    wd_form_maker(12);
                }

            } else {
                ?>
                <div class="en_avte_mobilemenu_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_mobilemenu_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_mobilemenu_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_mobilemenu_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>