<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-mobilemenu-form.php";
    wp_register_script('en-avte-mobilemenu-js', ISTO_URL . "/en-avte-mobilemenu/en-avte-mobilemenu.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-mobilemenu/en-avte-mobilemenu.js'));

    add_shortcode('en_avte_mobilemenu', 'en_avte_mobilemenu');
    function en_avte_mobilemenu($atts, $content = '')
    {

        //Set the variables between php and js
        //must be done inside this function, because get_user_option is not callable from outside
        global $current_user;
        $user = $current_user;
        $ADorg = get_user_option('ad_integration_account_suffix');
        if (
            (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) &&
            (($ADorg == '@intertek.com') || (in_array($user->roles[0], array('administrator'))))
        ) {
            $IsIntertek = true;
        } else {
            $IsIntertek = false;
        }

        wp_localize_script( 'en-avte-mobilemenu-js', 'ajax_sttrackorder_object', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'IsIntertek' => $IsIntertek
        ));

        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');

        $output = en_avte_mobilemenu_form();
        return $output;
    }


?>