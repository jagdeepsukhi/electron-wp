<?php


function en_avte_electric_history_form()
{
    global $current_user;
    ob_start();
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'dr', 'fm'))) {
                ?>
                <div class="en_avte_electric_history_form">
                    Search: <input id="en_avte_electric_history_searchgrid" type="text" />
                    <div id="en_avte_electric_history_master_grid" class="dataTable"></div>
                    <pre id="en_avte_electric_history_master_console" class="console"></pre>
                    <div id="en_avte_electric_history_gridpage" class="pagination"></div>
                </div> <!--en_avte_electric_history_form-->
            <?php
            } else {
                ?>
                <div class="en_avte_electric_history_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_coc_main_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_electric_history_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_coc_main_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

?>