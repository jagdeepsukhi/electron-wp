var gas_range_low;
var e_range_low;

(function($) {
var user;
var

    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_electric_mobile_grid'),
    en_avte_electric_searchgrid = $$('en_avte_electric_searchgrid'),
    en_avte_electric_paginggrid = $$('en_avte_electric_paginggrid'),
    hot,
    lowCoveredMiles, highCoveredMiles;
    myData = [],
    noOfRowstoShow = 5

jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_electric_mobile',
        'car': urlParam('mycar')
    },
    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        myData = ObjToArr(data.allcols);
        lowCoveredMiles = Number(data.lowCoveredMiles);
        highCoveredMiles = Number(data.highCoveredMiles);
        lastodo = data.lastodo;
        lastDateTime = data.lastDateTime;
        currenttime = data.currenttime;
        currentdate = data.currentdate;
        current_time = data.current_time;
        lastChargeEvent = data.lastChargeEvent;
        vehicleFuelClass = data.vehicleClass;
        charge_event_colNum = Number(data.charge_event_colNum);
        buildHOT(data.allcols);
        getgridData(data.allcols, "1", 7);



        /* The jQuery below triggers upon completion of the mobile web page load. "myselect" is populated with the object in the
         * vehicleID field.  myselect.value is then populated with the vehicle's id number. This populates vehicle id and
         * record id in the mobile app so it can be recorded upon uploading event data*/

        jQuery(document).ready(function($) {
            var myselectVehicleId = document.getElementsByName('vehicleID')[0];
            if(typeof myselectVehicleId !== 'undefined') {

                myselectVehicleId.value = urlParam('mycar');
                    var myselectCurrentDate =jQuery("input[electronid='current_date']").val();
                    jQuery("input[electronid='current_date']").val(currentdate);
                    var myselectChargeDate = new Date(jQuery("input[class='wdform-date']").val());
                    jQuery("input[class='wdform-date']").val(currentdate);
                    var myselectChargeTime =jQuery("input[electronid='time']").val();
                    jQuery("input[electronid='time']").val(current_time);
                    var myselectverificationtype = document.getElementsByName('verification_type')[0];
                    myselectverificationtype.value = 'charge';
                    var myselectLastOdo = document.getElementsByName('last_odometer_for_mobile_functionality')[0];
                    myselectLastOdo.value = lastodo;
                    var myselecthigh_CoveredMiles = document.getElementsByName('highCoveredMiles')[0];
                    myselecthigh_CoveredMiles.value = highCoveredMiles;
                    var myselectlow_CoveredMiles = document.getElementsByName('lowCoveredMiles')[0];
                    myselectlow_CoveredMiles.value = lowCoveredMiles;
                    var myselectlastdatetime = document.getElementsByName('lastDateTime')[0];
                    myselectlastdatetime.value = lastDateTime;
                    var myselectlastchargeEvent = document.getElementsByName('lastChargeEvent')[0];
                    myselectlastchargeEvent.value = lastChargeEvent;




            //filter fields that are not necessary based on vehicle make/mode/year
                if ((myselectVehicleId.value.indexOf("T3P")!= -1 ) || (myselectVehicleId.value.indexOf("M2I")!= -1 )|| (myselectVehicleId.value.indexOf("C1V")!= -1 )
                    || (myselectVehicleId.value.indexOf("C3V")!= -1 )|| (myselectVehicleId.value.indexOf("C6V")!= -1 )){
                    jQuery("div[electronwdid='soc']").remove();

                }
                if ((myselectVehicleId.value.indexOf("M2I") == -1) && (myselectVehicleId.value.indexOf("C3V") == -1) && (myselectVehicleId.value.indexOf("C6V") == -1) && (myselectVehicleId.value.indexOf("C1V") == -1)){
                    jQuery("div[electronwdid='bars']").remove();

                }

                //filter fields that are not necessary based on vehicle fuel type
                switch(vehicleFuelClass) {
                    case "Gas":
                    case "Gasoline/CNG":
                    case "Diesel":
                    case "CNG":
                    case "HEV":
                        jQuery("div[class='wdform_section']").hide();
                        alert("The Charging form is not applicable to this vechicle.")
                    case "EV":

                        jQuery("div[electronwdid='gasolinerange']").remove();
                        jQuery("div[electronwdid='gas_range_low_button']").remove();
                }
            }
        });
    }
);

e_range_low = function e_range_low() {
    if (jQuery("input[electronid='electric_range']").attr('type') == 'text') {
        jQuery("input[electronid='electric_range']").get(0).setAttribute('type', 'number');
        jQuery("input[electronid='electric_range']").attr('readonly', false);
    }
    else    {
        jQuery("input[electronid='electric_range']").get(0).setAttribute('type', 'text');
        var myElectricRange = jQuery("input[electronid='electric_range']").val();
        jQuery("input[electronid='electric_range']").val("---");
        jQuery("input[electronid='electric_range']").attr('readonly', true);

    }
}

gas_range_low = function gas_range_low() {
    if (jQuery("input[electronid='gasoline_range']").attr('type') == 'text') {
        jQuery("input[electronid='gasoline_range']").get(0).setAttribute('type', 'number');
        jQuery("input[electronid='gasoline_range']").attr('readonly', false);
    }
    else    {
        jQuery("input[electronid='gasoline_range']").get(0).setAttribute('type', 'text');
        var myGasolineRange = jQuery("input[electronid='gasoline_range']").val();
        jQuery("input[electronid='gasoline_range']").val("Low");
        jQuery("input[electronid='gasoline_range']").attr('readonly', true);

    }
}


function buildHOT(idata) {
    hot = new Handsontable(container, {
        columns: GetHeaders(idata),
        startRows: 0,
        startCols: 0,
        fixedRowsTop: 2,
        readOnly: true,
        cells: function (row, col, prop) {
            return en_avte_electric_cellRender(row, col, prop, this.instance);
        },
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_electric_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_electric_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_electric_cellRender(row, col, prop, myhot) {
    var cellProperties = {};

    //hide columns
    if ((myhot.getDataAtCell(1, col) == "id") ||
        (myhot.getDataAtCell(1, col) == "Vehicle ID"))
    { cellProperties.type = {renderer: hiddenRowRender} }

    //hide group row.  Used for coloring and permissions.
    if (row === 0) {cellProperties.type = { renderer: hiddenRowRender }; }

    //color the second row, which is the friendly field name
    else if ((row == 1) && !((myhot.getDataAtCell(1, col) == "id") ||
        (myhot.getDataAtCell(1, col) == "Vehicle ID"))) {
        if (myhot.getDataAtCell(0, col) == "dr"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }

    else {
        switch(prop) {
            case "charge_type":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Level 1','Level 2','DCFC',''];
                break;
            case "driver_id":
                cellProperties.stretch = true;
                break;

            case "soc":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '0';
                cellProperties.validator =  /(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/;
                cellProperties.allowInvalid = false;
                break;
            case "charge_event":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Plug in','Unplug'];

                cellProperties.allowInvalid = false;
                break;
            case "distance_covered":
//              cellProperties.readOnly = true;
//                 cellProperties.validator = distanceCoveredValidator;
                cellProperties.renderer = distanceCoveredRenderer;
                cellProperties.allowInvalid = false;

            case "odometer":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/; ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;
            case "gas_range":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/; ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;
            case "electric_range":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0.0';

                break;


            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY h:mma';
                cellProperties.correctFormat = true;

            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = false;
            }
        }
        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }
    }

    return cellProperties;
}


function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}

// distanceCoveredValidator = function (value, callback) {
//     setTimeout(function(){
//         //change value to lowmpg and highmpg
//         if ((Number(value) >= (lowCoveredMiles)) && (Number(value) <= (highCoveredMiles))) {
//             callback(true);
//         }
//         else {
//
//             callback(false);
//         }
//     }, 1000);
// }

//render the cell contents for MPG.  If the mpg is out of range the cell contents are red and bold.  If within range the contents are black and non-bold
function distanceCoveredRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    var cellRenderOutput
    charge_event_value = hot.getSourceDataAtCell(row, charge_event_colNum);
    if( (value !== null)) {
        if (((Number(value) >= (lowCoveredMiles)) && (Number(value) <= (highCoveredMiles))) && !(Number(value) < 0) && (charge_event_value == 'Plug in') ) {

            td.style.fontWeight = 'none';
            td.style.color = 'black';
            td.style.background = 'white';
            cellRenderOutput = "noFlag"
        }
        else if ((Number(value) == 0) && (charge_event_value == 'Unplug')) {

            td.style.fontWeight = 'none';
            td.style.color = 'black';
            td.style.background = 'white';
            cellRenderOutput = "noFlag"
        }

        else {

            td.style.fontWeight = 'bold';
            td.style.color = 'blue';
            td.style.background = 'Red';
            cellRenderOutput = "flagged"
        }
    }

}

})( jQuery );


