var delete_record_call;

(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_electric_grid'),
    en_avte_electric_searchgrid = $$('en_avte_electric_searchgrid'),
    en_avte_electric_paginggrid = $$('en_avte_electric_paginggrid'),
    hot,
    firstInsert= 0,
    filtering = 0,
    lowCoveredMiles, highCoveredMiles;
    myData = [],
    noOfRowstoShow = 10000,
    myData_charge_event_col_num = 0,
    recordnum = [];
    newrowArray = [],
    idsToAddBackIn = []

jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_electric',
        'car': urlParam('mycar')
    },

    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. Error: " + JSON.stringify(data.errors));
        } else {
            var data = JSON.parse(res);
            myData = ObjToArr(data.allcols);
            lowCoveredMiles = Number(data.lowCoveredMiles);
            highCoveredMiles = Number(data.highCoveredMiles);
            currentUser = data.currentUser;
            charge_event_colNum = Number(data.charge_event_colNum);
            odometer_colNum = Number(data.odometer_colNum);
            buildHOT(data.allcols);
            getgridData(data.allcols, "1", 10000);
        }
    }
);

function buildHOT(idata) {
    hot = new Handsontable(container, {
        columns: GetHeaders(idata),
        startRows: 0,
        startCols: 0,
        fixedRowsTop: 2,
        autoRowSize: false,
        manualColumnResize: true,
        manualRowResize: true,
        wordWrap: false,
        colHeaders: false,
        contextMenu: ['remove_row', 'undo', 'redo', 'row_above', 'row_below'],
        minSpareRows: 1,
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        wordWrap: false,

        cells: function (row, col, prop) {
            return en_avte_electric_cellRender({row: row, col: col, prop: prop, myhot: this.instance});
        },

        beforeUndo:function(action){

            idsToAddBackIn = getIdsToUndo(action, hot);

        },

        afterUndo:function(action){
            // reinitialize "idsToAddBackIn " array so that we can track whether to make changes to calculated fields int the "afterchange" functionality
            idsToAddBackIn = [];

        },

        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 2) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                jQuery.ajax({
                    url: ajax_sttrackorder_object.ajaxurl,
                    dataType: 'json',
                    async: false,
                    type: 'post',
                    data: {
                        'action': "ajaxen_avte_electric_new",
                        'amount': amount,
                        'car': urlParam('mycar'),
                        'addType':action,
                        'idsToAddBackIn': idsToAddBackIn
                    },
                    success: function (data) {
                        if (data.error == true) {
                            alert("Sorry, we can't add a new row. Error: " + JSON.stringify(data.errors));
                            hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                        } else {
                            for (var i = 0; i < data.newid.length; i++) {
                                //set email to R/W
                                hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                var newrow = {id: data.newid[i]};
                                myData.splice(myData.length, 0, newrow);

                                if (action != 'UndoRedo.undo'){
                                    hot.setDataAtRowProp(index + i - row_offset, 'create_time', data.cur_time, 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'plugin_unplug_dat_time', data.cur_time, 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'driver_id', data.user, 'auto_edit');
                                    hot.setDataAtRowProp(index + i - row_offset, 'car_id', urlParam('mycar'), 'auto_edit')
                                }else {
                                    // I pass with this method rather than letting change do it for anything I want ot make read only.  The change functionality does not
                                    //seem to pick up values for cells that are read only
                                    hot.setDataAtRowProp(index + i - row_offset, 'car_id', urlParam('mycar'), 'UndoRedo.undo')
                                }
                            }
                            hot_console.innerText = 'Row Inserted in DB';
                        }
                    }
                });
            }
            firstInsert++;
        },

        beforeChange: function (changes, source) {
            // if user makes a gange that is equal to the current value do not bother making change... does NOT work for bulk changes
            if ((changes[0][2] == changes[0][3]) && (changes.length == 1)) {
                return false;
            }
            //we were getting an error when a blank was entered into the sample recipt dat time field. so if the entry is blank do not make change
            if (((changes[0][1].indexOf('time') >= 0) || (changes[0][1].indexOf('date') >= 0)) && (changes[0][1].indexOf('verify_date') == -1) && (changes[0][3] == "")) {
                return false;
            }

            currentDate = new Date();
            currentMonth = currentDate.getMonth() + 1;
            currentYear = currentDate.getFullYear();
            authorizedRoles = ['fm', 'administrator'];

            // find out if user has proper role to move records into previously reported months
            userHasAuthority = arrayIntersect(currentUser, authorizedRoles);

            // Do not let users change regords that have already been verified or move records into months that have been reported
            for (i = 0; i < changes.length; i++) {
                if (((hot.getDataAtRowProp(changes[i][0], "verify_date")) != "") && (hot.getDataAtRowProp(changes[i][0], "verify_date") != "11-30--0001") &&
                    ((hot.getDataAtRowProp(changes[i][0], "verify_date")) != null) && ((changes[i][1]) != "verify_date") && ((changes[i][1]) != "notes") &&
                    ((changes[i][1]) != "distance_covered" ) && ((changes[i][1]) != "charge_duration" )) {
                    alert("ERROR!!! Change NOT Saved!!!  You cannot make a change to a record that has already been verified");
                    return false;
                }
                // Check to see if date is being changed.  if so make sure it is not being moved into a past month unless FM is the user
                if (changes[i][1] == 'plugin_unplug_dat_time') {

                    changeDate = new Date(changes[0][3]);
                    changeMonth = changeDate.getMonth() + 1;
                    changeYear = changeDate.getFullYear();

                    if ((currentMonth != changeMonth) || (currentYear != changeYear)) {
                        if (userHasAuthority != true) {
                            alert("ERROR!!! \n\nChange NOT Saved!!!  \nYou cannot move a record into a month that has already been reported");
                            return false;
                        }
                    }
                }
            }
        },

        afterChange: function (change, source) {
            if ((source === 'loadData') || (change[0][1] == "distance_covered") || (change[0][1] == "charge_duration") ||
                //dont change when change to "plugin_unplug_dat_time" is comming from  "dateValidator" because this is a repetitive change coming from the javascript that has
                // aready been changed by 'edit'.  We only want the dateValidaor to make the change for the users visual convenience
                ((change[0][1] == 'plugin_unplug_dat_time') && (source == 'dateValidator'))) {
                return; //don't save this change
            }

            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }

            // // do not allow columns that are only caclulation that are not actually saved in the DB to be sent to the DB
            if ((change[0][1] == 'distance_covered') || (change[0][1] == 'charge_duration')) {
                return false;
            }

            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=hot.getDataAtRowProp(change[i][0],'id');
                // change[i][5]=en_coc_main_hot.getDataAtRowProp(change[i][0],'user_id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_avte_electric_change',
                    'data': change,
                    'source': source,
                    'car': urlParam('mycar')
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the following changes. Error: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';

                        }
                    }
                }
            });

            //Now go see if I need to do further actions
            /*only if idsToAddBackIn is null because this indicates that the change is not being triggered by undo.  Undo already has all the pertinent information for each column.
             * allowing it to make this change also confilcts with undo because this action will clear the previously undone actions in the hansdaontable functionality*/
            if (idsToAddBackIn.length == 0) {
                var key;
                for (key in change) {
                    //if there was a time entry change... recalculate affected rows so the user does not have to refresh the page to see the updated charge duration
                    if (change[key][1] == 'plugin_unplug_dat_time') {
                        if ((hot.getDataAtRowProp(change[key][0], 'charge_event') == 'Unplug' ) && ((change[key][0]) != 2 )) {
                            plugin_unplug_dat_time_String = hot.getDataAtRowProp(change[key][0], 'plugin_unplug_dat_time');
                            last_plugin_unplug_dat_time_String = hot.getDataAtRowProp((change[key][0]) - 1, 'plugin_unplug_dat_time');
                            plugin_unplug_dat_time = convert_datetimestring_to_datetime(plugin_unplug_dat_time_String);
                            last_plugin_unplug_dat_time = convert_datetimestring_to_datetime(last_plugin_unplug_dat_time_String);
                            duration = plugin_unplug_dat_time - last_plugin_unplug_dat_time;
                            chargeduration = convert_miliseconds_to_HH_MM_SS(duration);
                            hot.setDataAtRowProp((change[key][0]), 'charge_duration', chargeduration, 'change_' + change[key][1]);

                        }
                        if ((hot.getDataAtRowProp(change[key][0], 'charge_event') == 'Plug in' ) && (hot.getDataAtRowProp((change[key][0]) + 1, 'plugin_unplug_dat_time') != null )) {
                            plugin_unplug_dat_time_String = hot.getDataAtRowProp(change[key][0], 'plugin_unplug_dat_time');
                            next_plugin_unplug_dat_time_String = hot.getDataAtRowProp((change[key][0]) + 1, 'plugin_unplug_dat_time');
                            plugin_unplug_dat_time = convert_datetimestring_to_datetime(plugin_unplug_dat_time_String);
                            next_plugin_unplug_dat_time = convert_datetimestring_to_datetime(next_plugin_unplug_dat_time_String);
                            duration = next_plugin_unplug_dat_time - plugin_unplug_dat_time;
                            chargeduration = convert_miliseconds_to_HH_MM_SS(duration);
                            hot.setDataAtRowProp((change[key][0]) + 1, 'charge_duration', chargeduration, 'change_' + change[key][1]);
                        }
                    }

                    if (change[key][1] == 'odometer') {
                        if ((hot.getDataAtRowProp(change[key][0], 'charge_event') == 'Plug in') && (hot.getDataAtRowProp((change[key][0]) - 1, 'charge_event') == 'Unplug')) {
                            odometer_at_Plug_in = hot.getDataAtRowProp(change[key][0], 'odometer');
                            odometer_at_last_Unplug = hot.getDataAtRowProp((change[key][0]) - 1, 'odometer');
                            odometer_at_next_Unplug = hot.getDataAtRowProp((change[key][0]) + 1, 'odometer');
                            milesCovered = odometer_at_Plug_in - odometer_at_last_Unplug;
                            milesCoveredSinceNextUnplug = odometer_at_next_Unplug - odometer_at_Plug_in;
                            hot.setDataAtRowProp((change[key][0]), 'distance_covered', milesCovered, 'change_' + change[key][1]);
                            if (hot.getDataAtRowProp((change[key][0]) + 1, 'id') != null) {
                                hot.setDataAtRowProp((change[key][0]) + 1, 'distance_covered', milesCoveredSinceNextUnplug, 'change_' + change[key][1]);
                            }
                        }

                        if ((hot.getDataAtRowProp(change[key][0], 'charge_event') == 'Unplug') && (hot.getDataAtRowProp((change[key][0]) - 1, 'charge_event') == 'Plug in')) {

                            odometer_at_Unplug = hot.getDataAtRowProp(change[key][0], 'odometer');
                            odometer_at_next_plug_in = hot.getDataAtRowProp((change[key][0]) + 1, 'odometer');
                            odometer_at_last_Plug_in = hot.getDataAtRowProp((change[key][0]) - 1, 'odometer');
                            milesCoveredSincePlug_in = odometer_at_Unplug - odometer_at_last_Plug_in;
                            milesCovered = odometer_at_next_plug_in - odometer_at_Unplug;
                            if (hot.getDataAtRowProp((change[key][0]) + 1, 'id') != null) {
                                hot.setDataAtRowProp((change[key][0]) + 1, 'distance_covered', milesCovered, 'change_' + change[key][1]);
                            }
                            hot.setDataAtRowProp((change[key][0]), 'distance_covered', milesCoveredSincePlug_in, 'change_' + change[key][1]);
                        }
                    }
                }
            }
        },

        beforeRemoveRow: function (index, amount) {

            //Check to make sure we are not trying to delete the blank row at the bottom
            lastRow = hot.countRows() -1;
            if (((index == lastRow) || (index + amount -1 == lastRow)) && (hot.getDataAtRowProp(lastRow, 'id') == null)) {
                alert("You CANNOT delete the blank entry row (last row)."); return false;}

            // index the row(s) to be deleted with the record ID
            recordnum = [];
            if (index > 1) {
                i = 0;
                for (row = index; row <= index + (amount - 1); row++) {
                    recordnum[i] = hot.getDataAtRowProp(row, 'id');
                    //if a record in the selection has already been verified then cancel deletion
                    if ((hot.getDataAtRowProp(row, 'verify_date') != "11-30--0001") && (hot.getDataAtRowProp(row, 'verify_date') != "")
                        && (hot.getDataAtRowProp(row, 'verify_date') != null)) {
                        alert("ERROR!!! Record(s) NOT Deleted!!! One or more of the records selected for deletion has already been verified.");
                        return false;
                    }
                    i++;
                }
                delete_record(index, amount, recordnum);
            }
        }
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_electric_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_electric_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_electric_cellRender(parameters) {
    var row = parameters.row;
    var col = parameters.col;
    var prop = parameters.prop;
    var myhot = parameters.myhot;
    var cellProperties = {};

    //hide group row.  Used for coloring and permissions.
    if (row === 0) {cellProperties.type = { renderer: hiddenRowRender }; }

    //color the second row, which is the friendly field name
    // (myhot.getDataAtCell(1, col) == "id") ||
    else if ((row == 1) && !((myhot.getDataAtCell(1, col) == "Vehicle ID"))) {
        if (myhot.getDataAtCell(0, col) == "dr"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }

    else {
        switch(prop) {
            case 'car_id':
                cellProperties.readOnly = true;
                cellProperties.renderer = whiteCellRenderer;
                break;
            case "id":
                cellProperties.readOnly = true;
                break;
            case "charge_type":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Level 1','Level 2','DC Fast Charge',''];
                break;
            case "driver_id":
                cellProperties.stretch = true;
                break;

            case "soc":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '0';
                cellProperties.validator =  /(^100(\.0{1,1})?$)|(^([1-9]([0-9])?|0?$)(\.[0-9]{1,1})?$)/;
                cellProperties.allowInvalid = false;
                break;
            case "bars":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'numeric';
                cellProperties.format = '0';
                cellProperties.validator =  /^(\s*|\d+)$/;
                cellProperties.allowInvalid = false;
                break;
            case "charge_event":
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Plug in','Unplug'];
                cellProperties.allowInvalid = false;
                break;
            case "distance_covered":
                cellProperties.readOnly = true;
                // cellProperties.validator = distanceCoveredValidator;
                cellProperties.renderer = distanceCoveredRenderer;
                cellProperties.allowInvalid = false;
                break;
            case "create_time":
                // cellProperties.readOnly = true;
                break;
            case "odometer":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /^(\s*|\d+)$/; ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;
            case "gas_range":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0';
                cellProperties.validator = /(^(\s*|\d+)$|^Low$)/; ///^\d+$/;  for no blank allowed
                cellProperties.allowInvalid = false;
                break;
            case "electric_range":
                cellProperties.type = 'numeric';
                cellProperties.format = '0,0.0';
                cellProperties.validator = /^\d{1,3}(?:\.\d)?$|^---$/;
                cellProperties.allowInvalid = false;
                break;
            case "charge_duration":
                cellProperties.readOnly = true;
                break;

            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'M-D-YYYY h:mm a';
                cellProperties.correctFormat = true;
                cellProperties.allowInvalid = false;
            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = true;
            }
        }

        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }
    }

    return cellProperties;
}

distanceCoveredValidator = function (value, callback) {
    setTimeout(function(){
        //change value to lowmpg and highmpg
        if ((Number(value) >= (lowCoveredMiles)) && (Number(value) <= (highCoveredMiles))) {
            callback(true);
            }
        else {

           callback(false);
            }
        }, 1000);
    }

//render the cell contents for Electric Range.  If the Electric Range is out of range the cell contents are red and bold.  If within range the contents are black and non-bold
function distanceCoveredRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    var cellRenderOutput
    charge_event_colValue = hot.getSourceDataAtCell(row, charge_event_colNum);
    odometer_colValue = hot.getSourceDataAtCell(row, odometer_colNum);
    if (odometer_colValue != null) {
                lastOdometer_colValue = hot.getSourceDataAtCell(row - 1, odometer_colNum);
                nextChargeEvent_colValue = hot.getSourceDataAtCell(row + 1, charge_event_colNum);
                lastChargeEvent_colValue = hot.getSourceDataAtCell(row - 1, charge_event_colNum);
                lastlastOdometer_colValue = hot.getSourceDataAtCell(row - 2, odometer_colNum);

                if (((Number(value) >= (lowCoveredMiles)) && (Number(value) <= (highCoveredMiles))) && !(Number(value) < 0) && (charge_event_colValue == 'Plug in')) {

                    td.style.fontWeight = 'none';
                    td.style.color = 'black';
                    td.style.background = 'white';

                    if ((charge_event_colValue == nextChargeEvent_colValue) || (charge_event_colValue == lastChargeEvent_colValue)) {

                            td.style.fontWeight = 'bold';
                            td.style.color = 'blue';
                            td.style.background = 'Red';
                        }
                }
                else if ((Number(value) == 0) && (charge_event_colValue == 'Unplug') && (odometer_colValue == lastOdometer_colValue)) {

                    td.style.fontWeight = 'none';
                    td.style.color = 'black';
                    td.style.background = 'white';

                    if ((charge_event_colValue == nextChargeEvent_colValue) || (charge_event_colValue == lastChargeEvent_colValue)) {

                            td.style.fontWeight = 'bold';
                            td.style.color = 'blue';
                            td.style.background = 'Red';
                    }
                }

                else if ((Number(value) == 0) && (charge_event_colValue == 'Unplug') && (odometer_colValue != lastOdometer_colValue)){
                    if (odometer_colValue == lastlastOdometer_colValue){
                        td.style.fontWeight = 'none';
                        td.style.color = 'black';
                        td.style.background = 'white';
                    }
                }
                else if (charge_event_colValue == null){

                    td.style.fontWeight = 'none';
                    td.style.color = 'black';
                    td.style.background = 'white';

                }

                else {

                    td.style.fontWeight = 'bold';
                    td.style.color = 'blue';
                    td.style.background = 'Red';
                }

    }
    else{
        td.style.fontWeight = 'bold';
        td.style.color = 'blue';
        td.style.background = 'Red';
    }
}

// convert datetime string into datetime format
function convert_datetimestring_to_datetime(datetime_string){
    dateArray = datetime_string.split(' ');
    year = dateArray[0].split('-');
    timeString = dateArray[1] + ' ' + dateArray[2]
    time= convert_to_24h(timeString);
    finishDate = new Date(year[2], year[0]-1, year[1], time[0], time[1]);
    return finishDate;
}

//Convert 12 hour time to 24 hour time
function convert_to_24h(time_str) {
    // Convert a string like 10:05:23 PM to 24h format, returns like [22,5,23]
    var time = time_str.match(/(\d+):(\d+) (\w)/);
    var hours = Number(time[1]);
    var minutes = Number(time[2]);
  //  var seconds = Number(time[3]);
    var meridian = time[3].toLowerCase();

    if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
    }
    return [hours, minutes];
}

//converts miliseconds to HH:MM:SS... The SS in this case in hard coded as :00 since we do not use seconds when entering "plug in_unplug_dat_time via the desktop browser.
function convert_miliseconds_to_HH_MM_SS(miliseconds) {
totmins = miliseconds/60000;
hours = two_dig_num(Math.floor(totmins/60));
mins = two_dig_num(totmins - (hours *60));
chargeDuration = hours +':'+ mins + ':00';
    return chargeDuration;
}

// ensure any single digit number has a "0" in front of it
function two_dig_num(num) {
    return num > 9 ? "" + num : "0" + num;
}

//Ajax call to server for record delete
delete_record_call = function delete_record_call(index, amount, recordnum) {
    return jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        type: "POST",
        async: false,
        data: {
            action: "ajaxen_avte_electric_deleterecord",
            amount: amount,
            index: index,
            id: recordnum,
            car: urlParam('mycar')
        }
    }).responseText;
}

//Pull up another vehicle using the "Find Another Vehicle" text box
// pulls up new vehicle information upon pressing enter

document.getElementById('tags').onkeydown = function(event){

    document.getElementById('tags').style.color = "green";
    var e = event || window.event;

    if(e.keyCode == 13){
        myselectCars = jQuery ("input[id='tags']").val();
        window.location.assign("?mycar=" + myselectCars);
    }
}

})( jQuery );