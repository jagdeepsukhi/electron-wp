<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-avte-electric-form.php";
    wp_register_script('en-avte-electric-js', ISTO_URL . "/en-avte-electric/en-avte-electric.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-electric/en-avte-electric.js'));
    wp_register_script('en-avte-electric-mobile-js', ISTO_URL . "/en-avte-electric/en-avte-electric-mobile.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-avte-electric/en-avte-electric-mobile.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_avte_electric_new', 'ajax_en_avte_electric_new');
    add_action('wp_ajax_ajaxen_avte_electric_change', 'ajax_en_avte_electric_change');
    add_action('wp_ajax_ajaxen_avte_electric_saveall', 'ajax_en_avte_electric_saveall');
    add_action('wp_ajax_ajaxen_avte_electric', 'ajax_en_avte_electric');
    add_action('wp_ajax_ajaxen_avte_electric_mobile', 'ajax_en_avte_electric_mobile');
    add_action('wp_ajax_ajaxen_avte_electric_deleterecord', 'ajax_en_avte_electric_deleterecord');
    add_action('wp_ajax_ajaxen_avte_electric_undoDeleteRows', 'ajax_en_avte_electric_undoDeleteRows');


    add_shortcode('en_avte_electric', 'en_avte_electric');
    function en_avte_electric($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');


        $output = en_avte_electric_form();
        return $output;
    }


function ajax_en_avte_electric(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) {

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_electric'
                    ORDER BY st_default_cols.`order`";
            }

            else {
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_electric' AND st_default_cols.group = 'DR'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }
    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //find the column number for charge_event and odometer for use in the "distanceCoveredRender" function in the javascript.
    $col_count = 0;
    $charge_event_colNum = 0;
    $odometer_colNum = 0;
    foreach($default_cols as $fieldName){

        if ($fieldName->field_name == "charge_event"){
            $charge_event_colNum = $col_count;
        }
        if ($fieldName->field_name == "odometer"){
            $odometer_colNum = $col_count;
        }
        $col_count++;
    }


    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT

                en_avte_electric.id,
                en_avte_electric.car_id,
                en_avte_electric.plugin_unplug_dat_time,
                en_avte_electric.charge_event,
                en_avte_electric.electric_range,
                en_avte_electric.gas_range,
                en_avte_electric.soc,
                en_avte_electric.bars,
                en_avte_electric.charge_type,
                en_avte_electric.odometer,
                IF(en_avte_electric.charge_event = 'Plug in',NULL,IF(en_avte_electric.driver_id = 'Admin', NULL,TRIM(TRAILING '.' FROM (TRIM(TRAILING '0' FROM (SEC_TO_TIME(TO_SECONDS(en_avte_electric.plugin_unplug_dat_time)-@lastTime))))))) AS charge_duration,
                IF(en_avte_electric.driver_id = 'Admin', NULL,CAST(en_avte_electric.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED)) AS distance_covered,
                IF(en_avte_electric.driver_id = 'Admin',@lastOdo = @lastOdo,@lastOdo:= en_avte_electric.odometer),
                IF(en_avte_electric.driver_id = 'Admin', @lastTime:= @lastTime, @lastTime:= TO_SECONDS(en_avte_electric.plugin_unplug_dat_time)),
                @lastOdo AS Last_Odo,
                @lastTime AS Last_time,
                en_avte_electric.create_time,
                en_avte_electric.driver_id,
                en_avte_electric.verify_date,
                en_avte_electric.notes
        
          FROM en_avte_electric,
            ( select @lastOdo := 0, @lastTime := 0) SQLVar
          WHERE en_avte_electric.car_id = '%s'
          ORDER BY en_avte_electric.plugin_unplug_dat_time, en_avte_electric.odometer";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );
    $dbdata = make_date($dbdata);
    $col_plus_data = array_merge ($transposed, $dbdata);
// retrieve the column number for charge_event for use in the JavaScrip Cell Render Function


    // Get acceptable odo range for vehicle w/in +/-2 stddev
    $sql = "SELECT
              AVG(STDCalc.distance_covered) AS Average,
              AVG(STDCalc.distance_covered) + 2 * STDDEV(STDCalc.distance_covered) as high_elec_ran_std,
              AVG(STDCalc.distance_covered) - 2 * STDDEV(STDCalc.distance_covered) as low_elec_ran_std
              FROM(SELECT
                        en_avte_electric.plugin_unplug_dat_time,
                        en_avte_electric.odometer,
                        IF(en_avte_electric.driver_id = 'Admin', NULL,CAST(en_avte_electric.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED)) AS distance_covered,
                        IF(en_avte_electric.driver_id = 'Admin',@lastOdo = @lastOdo,@lastOdo:= en_avte_electric.odometer),
                        @lastOdo AS Last_Odo
                      FROM en_avte_electric,
                        ( select @lastOdo := 0) SQLVar
                      WHERE en_avte_electric.car_id = '%s'
                      ORDER BY en_avte_electric.plugin_unplug_dat_time, en_avte_electric.odometer) STDCalc
              WHERE STDCalc.plugin_unplug_dat_time >= '2016/03/02'
              AND STDCalc.distance_covered <> '0'";

    $odoRange = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );

    //Send Response
    $json['lowCoveredMiles'] =  $odoRange[0]->low_elec_ran_std;
    $json['highCoveredMiles'] = $odoRange[0]->high_elec_ran_std;
    $json['allcols'] =$col_plus_data;
    $json['currentUser'] =  $user->roles;
    $json['charge_event_colNum'] =$charge_event_colNum;
    $json['odometer_colNum'] =$odometer_colNum;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_electric_mobile(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    global $current_user;
    $lastOdo=0;


    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("fm", "administrator"), $user->roles)) > 0){

                //Go get the columns that should be there for all SO
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_electric'
                    ORDER BY st_default_cols.`order`";
            }

            else {
                $sql = "SELECT
                      st_default_cols.field_name,
                      st_default_cols.`group`,
                      st_default_cols.name
                    FROM st_default_cols
                    WHERE st_default_cols.table_name = 'en_avte_electric' AND st_default_cols.group = 'DR'
                    ORDER BY st_default_cols.`order`";
            }
        }
    }

    $default_cols = $wpdb->get_results( $sql, OBJECT );

    //find the column number for charge_event for use in the "distanceCoveredRender" function in the javascript.
    $col_count = 0;
    $charge_event_colNum = 0;
    foreach($default_cols as $fieldName){

        if ($fieldName->field_name == "charge_event"){
            $charge_event_colNum = $col_count;
        }
        $col_count++;
    }


    //Transpose the sql response into top columns, and then add the data
    $transposed = populate_cols($default_cols);
    $sql = "SELECT * FROM (
        SELECT  *
        FROM    en_avte_electric
        WHERE   car_id = '%s'
        ORDER BY plugin_unplug_dat_time DESC,odometer DESC LIMIT 5) as allFuelEntries
        ORDER BY plugin_unplug_dat_time ASC, odometer ASC";
    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );
    $dbdata = make_date($dbdata);
    $lastDateTime = end($dbdata)->plugin_unplug_dat_time;
    $lastOdo = end($dbdata)->odometer;
    $lastChargeEvent = end($dbdata)->charge_event;
    $col_plus_data = array_merge ($transposed, $dbdata);


    // Get acceptable odo range for vehicle w/in +/-2 stddev
    $sql = "SELECT
              AVG(STDCalc.distance_covered) AS Average,
              AVG(STDCalc.distance_covered) + 2 * STDDEV(STDCalc.distance_covered) as high_elec_ran_std,
              AVG(STDCalc.distance_covered) - 2 * STDDEV(STDCalc.distance_covered) as low_elec_ran_std
              FROM(SELECT
                        en_avte_electric.plugin_unplug_dat_time,
                        en_avte_electric.odometer,
                        IF(en_avte_electric.driver_id = 'Admin', NULL,CAST(en_avte_electric.odometer AS SIGNED) - CAST(@lastOdo AS SIGNED)) AS distance_covered,
                        IF(en_avte_electric.driver_id = 'Admin',@lastOdo = @lastOdo,@lastOdo:= en_avte_electric.odometer),
                        @lastOdo AS Last_Odo
                      FROM en_avte_electric,
                        ( select @lastOdo := 0) SQLVar
                      WHERE en_avte_electric.car_id = '%s'
                      ORDER BY en_avte_electric.plugin_unplug_dat_time, en_avte_electric.odometer) STDCalc
              WHERE STDCalc.plugin_unplug_dat_time >= '2016/03/02'
              AND STDCalc.distance_covered <> '0'";

   $odoRange = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );


    // Get vehicle Fuel Class
    $sql = "SELECT
              en_avte_cars.TYPE
            FROM en_avte_cars
            WHERE en_avte_cars.vehicle_id = '%s'";

    $vehicleClass = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['car']), OBJECT );


     // finalize data points that need to be sent to the mobile app for processing
    $curdatetime =current_time( 'mysql' );
    $curtime = date_format(date_create_from_format('Y-m-d H:i:s', $curdatetime), 'H:i');
    $curdate = date_format(date_create_from_format('Y-m-d H:i:s', $curdatetime), 'Y-m-d');

    //Send Response
    $json['lastDateTime'] = $lastDateTime;
    $json['vehicleClass'] = $vehicleClass[0]->TYPE;
    $json['lastChargeEvent'] = $lastChargeEvent;
    $json['currenttime'] = $curdatetime;
    $json['currentdate'] = $curdate;
    $json['current_time'] = $curtime;
    $json['lastodo'] =  $lastOdo;
    $json['lowCoveredMiles'] =  $odoRange[0]->low_elec_ran_std;
    $json['highCoveredMiles'] = $odoRange[0]->high_elec_ran_std;
    $json['allcols'] =$col_plus_data;
    $json['charge_event_colNum'] =$charge_event_colNum;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}




function ajax_en_avte_electric_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );

    //Handle each change
    $cur_time = current_time( 'mysql' );

    for ($i = 0; $i < $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
            if ($_POST['addType'] != "UndoRedo.undo") {
                $dbresult = $wpdb->insert('en_avte_electric',
                    array(
                        'create_time' => $cur_time,
                        'car_id' => $_POST['car'],
                    )
                );
            } else{
                $dbresult = $wpdb->insert('en_avte_electric',
                    array(
                        'id' => $_POST['idsToAddBackIn'][$i],
                        'car_id' => $_POST['car']
                    )
                );
            }

            $new_id[] = $wpdb->insert_id;
            if ((0 === $dbresult) || (false === $dbresult)) {
                $json[errors][$i] = true;
            } else {
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $new_id[$i],
                    'car_id' => $_POST['car']
                );
                $wpdb->insert( 'en_avte_electric_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['newid'] = $new_id;
    $json['user'] = $current_user->data->display_name;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_avte_electric_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );
    $table = "electric";

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_electric'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    $dbindex = 0;

    //See if they are allowed to delete this record
////Make the deletion
    foreach ($_POST['id'] as $id) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified($table, $id);

        if ((allowed_to_change('odometer', $rolemap, $user->roles)) && ((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")))) {
            try {
                $wpdb->flush();
                $sql = "DELETE FROM en_avte_electric
	            WHERE id = '%s'";
                $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
                if ((0 === $dbresult) || (false === $dbresult)) {

                    $json[errors][$dbindex] = true;

                }

//                //Log the deletion
            $dbdata = array(
                'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                'field' => 'RECORD DELETED',
                'before' => 'Active',
                'after' => 'Deleted',
                'source' => 'Delete',
                'table_id' => $id,
                'car_id' => $_POST['car']
            );
            $wpdb->insert('en_avte_electric_history', $dbdata);
            } catch (Exception $e) {
                $json[errors][$dbindex] = $e->getMessage();
            }
        } else {
            //user did not have permissiong to make deletion
            $json[errors][$dbindex] = 'role';
        }

        $dbindex++;
    }

    //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}
function ajax_en_avte_electric_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );
    $table = 'electric';

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_avte_electric'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

        //has record already been verified.  if so it will not allow the change to be made in the conditinal statements below
        $recordDateVerification = isRecordVerified ($table, $change[4]);

        // has the time period the user is trying to enter a record into already been reported on.  If so ONLY allow "fm" users to make the change
        $timeFrameAlreadyVerified = hasTimePeriodAlreadyBeenReported($table, $change, $current_user, $cur_time);

        $logit = true;
        //skip ID change column notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if ((allowed_to_change($change[1], $rolemap, $user->roles)) && (((!isset($recordDateVerification) || ($recordDateVerification == "0000-00-00")) && $timeFrameAlreadyVerified == false)
                    || ((count(array_intersect(array("fm", "administrator"), $user->roles)) > 0) && (($change[1] == "verify_date")||($change[1] == "notes"))))){
                //Make the change
                try {
                    $wpdb->flush();
                    //wpdb->prepare does not accept nulls, so we can't use it here.
                    $table = "en_avte_electric";

                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]) );
                    if ( false === $dbresult ) {

                        $json[errors][$dbindex] = true;
                    }


                //Log the change
                if ($logit) {
                    $dbdata = array(
                        'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                        'field' => $change[1],
                        'before' => $change[2],
                        'after' => $change[3],
                        'source' => $_POST['source'],
                        'table_id' => $change[4],
                        'car_id' => $_POST['car']
                    );
                    $wpdb->insert('en_avte_electric_history', $dbdata);
                }

                } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }

            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = 'role';
            }
        }
        //user tried to change record "id" which is NEVER permissible
        else{ $json[errors][$dbindex] = true;}
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>
