<?php
/**
 * Plugin Name: Intertek Electron
 * Plugin URI: http://www.intertek.com/energy/renewable/cecet/
 * Description: Provides multiple online tracking functionality.  
 * Version: 1.0.0
 * Author: Marco Gaertner and Jon Mackie
 * Author URI: http://www.intertek.com/energy/renewable/cecet/
 * License: proprietary.
 */
/* Copyright 2016 Intertek */


define("ISTO_BASENAME", plugin_basename(dirname(__FILE__)));
define("ISTO_DIR", WP_PLUGIN_DIR . '/' . ISTO_BASENAME);
define("ISTO_URL", WP_PLUGIN_URL . '/' . ISTO_BASENAME);
//require_once realpath( dirname( __file__ ) ) . "/create-view/st-create-view-order.php";
//require_once realpath( dirname( __file__ ) ) . "/edit/st-edit-order.php";
//require_once realpath( dirname( __file__ ) ) . "/change-history/st-change-history.php";
//require_once realpath( dirname( __file__ ) ) . "/edit-order-details/st-edit-order-details.php";
//require_once realpath( dirname( __file__ ) ) . "/health-details/st-health-details.php";
require_once realpath( dirname( __file__ ) ) . "/en-coc-main/en-coc-main.php";
require_once realpath( dirname( __file__ ) ) . "/en-coc-history/en-coc-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-cars/en-avte-cars.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-cars-history/en-avte-cars-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-fuel/en-avte-fuel.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-fuel-history/en-avte-fuel-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-electric/en-avte-electric.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-electric-history/en-avte-electric-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-mobilemenu/en-avte-mobilemenu.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-maintenance/en-avte-maintenance.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-maintenance-history/en-avte-maintenance-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-maintenancedetail/en-avte-maintenancedetail.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-monthend/en-avte-monthend.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-monthend-history/en-avte-monthend-history.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-maintenance_items_select/en-avte-maintenance_items_select.php";
require_once realpath( dirname( __file__ ) ) . "/en-coc-unique/en-coc-unique.php";
require_once realpath( dirname( __file__ ) ) . "/en-avte-vlr/en-avte-vlr.php";

isto_ajax_init();


//Add Admin menu
function electron_admin_menu()
{
        add_menu_page('Electron', 'Electron', 'manage_options', 'en-admin');
        add_submenu_page('en-admin', 'Electron - VLR Settings', 'VLR', 'manage_options', 'en-vlr','en_vlr_admin_form');
}
if ( is_admin() ) {
    add_action('admin_menu', 'electron_admin_menu');
}

function isto_ajax_init(){
    //get the JS script and CSS ready to be sent to web client
    wp_register_script('st-hot-js', ISTO_URL."/handsontable/dist/handsontable.full.js", array('jquery'), filemtime(ISTO_DIR.'/handsontable/dist/handsontable.full.js') );
    wp_register_style('st-hot-css', ISTO_URL."/handsontable/dist/handsontable.full.css", '', filemtime(ISTO_DIR.'/handsontable/dist/handsontable.full.css') );
    wp_register_script('st-track-order-js', ISTO_URL."/st-track-order.js", array('jquery'), filemtime(ISTO_DIR.'/st-track-order.js'));
    wp_register_style('st-track-order-css', ISTO_URL."/st-track-order.css", '', filemtime(ISTO_DIR.'/st-track-order.css') );

    //Set the variables between php and js
    wp_localize_script( 'st-track-order-js', 'ajax_sttrackorder_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));

    // It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
    add_action( 'wp_ajax_nopriv_ajaxsttrackorder', 'ajax_sttrackorder' );
    add_action( 'wp_ajax_ajaxsttrackorder', 'ajax_sttrackorderr' );
    add_action( 'wp_ajax_nopriv_ajaxsttrackorderchange', 'ajax_sttrackorder_change' );
    add_action( 'wp_ajax_ajaxsttrackorderchange', 'ajax_sttrackorder_change' );
    add_action( 'wp_ajax_nopriv_ajaxsttrackordersaveall', 'ajax_sttrackorder_saveall' );
    add_action( 'wp_ajax_ajaxsttrackordersaveall', 'ajax_sttrackorder_saveall' );
    add_action('login_head', 'my_loginlogo');
    add_filter('login_headerurl', 'my_loginURL');
    add_filter('login_headertitle', 'my_loginURLtext');
}

function add_roles_on_plugin_activation() {
    add_role(
        'cc',
        __( 'Chain Of Custody' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => false,
            'delete_posts' => false, // Use false to explicitly deny
            'edit_pages' => false, // Allows user to edit pages
            'edit_others_posts' => false, // Allows user to edit others posts not just their own
            'create_posts' => false, // Allows user to create new posts
            'manage_categories' => false, // Allows user to manage post categories
            'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
            'edit_themes' => false, // false denies this capability. User can’t edit your theme
            'install_plugins' => false, // User cant add new plugins
            'update_plugin' => false, // User can’t update any plugins
            'update_core' => false // user cant perform core updates
        )
    );

    add_role(
        'fm',
        __( 'Fleet Manager' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => false,
            'delete_posts' => false, // Use false to explicitly deny
            'edit_pages' => false, // Allows user to edit pages
            'edit_others_posts' => false, // Allows user to edit others posts not just their own
            'create_posts' => false, // Allows user to create new posts
            'manage_categories' => false, // Allows user to manage post categories
            'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
            'edit_themes' => false, // false denies this capability. User can’t edit your theme
            'install_plugins' => false, // User cant add new plugins
            'update_plugin' => false, // User can’t update any plugins
            'update_core' => false // user cant perform core updates
        )
    );

    add_role(
        'dr',
        __( 'Driver' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => false,
            'delete_posts' => false, // Use false to explicitly deny
            'edit_pages' => false, // Allows user to edit pages
            'edit_others_posts' => false, // Allows user to edit others posts not just their own
            'create_posts' => false, // Allows user to create new posts
            'manage_categories' => false, // Allows user to manage post categories
            'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
            'edit_themes' => false, // false denies this capability. User can’t edit your theme
            'install_plugins' => false, // User cant add new plugins
            'update_plugin' => false, // User can’t update any plugins
            'update_core' => false // user cant perform core updates
        )
    );

}
register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );

function my_loginlogo() {
    echo '<style type="text/css">
    h1 a {
      background-image: url(' . get_template_directory_uri() . '/login/logo.png) !important;
    }
  </style>';
}

function my_loginURL() {
    return 'http://';
}

function my_loginURLtext() {
    return 'Intertek';
}

function my_loginredrect( $redirect_to, $request, $user ) {
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        if( in_array('administrator', $user->roles)) {
            //return admin_url();
            return '/';
        } else {
            return '/';
        }
    } else {
        return '/';
    }
}
add_filter('login_redirect', 'my_loginredrect', 10, 3);

//Set cookie to never expire
add_filter( 'auth_cookie_expiration', 'wpy_custom_login_cookie' );
function wpy_custom_login_cookie() {
    return 31536000; // one year in seconds: 60 * 60 * 24 * 365 * 1
}

/* make the default for new users not to see the top admin bar when they are logged in */
add_action("user_register", "set_user_admin_bar_false_by_default", 10, 1);
function set_user_admin_bar_false_by_default($user_id) {
    update_user_meta( $user_id, 'show_admin_bar_front', 'false' );
    update_user_meta( $user_id, 'show_admin_bar_admin', 'false' );
}

//* Setup the footer text in the admin dashboard
function change_footer_admin () { echo 'Electron Admin Dashboard'; }
add_filter('admin_footer_text', 'change_footer_admin');

// Add Login/Logout menu button
add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'primary-menu') {
        $items .= '<li><a href="'. wp_logout_url() .'">Log Out</a></li>';
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'primary-menu') {
        $items .= '<li><a href="'. site_url('wp-login.php') .'">Log In</a></li>';
    }
    return $items;
}

function populate_cols($arr) {
    $out = array();
    if (!isset($out[0])) $out[0] = new stdClass();
    if (!isset($out[1])) $out[1] = new stdClass();

    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        $myfieldname ="";
        $mygroup ="";
        foreach ($subarr as $subkey => $subvalue) {
            if ($subkey == 'field_name') {$myfieldname = $subvalue;}
            elseif ($subkey == 'group') {$mygroup = $subvalue;}
        }
        $out[0]->$myfieldname = $mygroup;
    }
    //Now we look again to find the Friendly Field Names.  Non-optimal, but it's a short term solution
    foreach ($arr as $key => $subarr) {
        $myname = "";
        foreach ($subarr as $subkey => $subvalue) {
            if ($subkey == 'field_name') {$myfieldname = $subvalue;}
            elseif ($subkey == 'name') {$myname = $subvalue;}
        }
        $out[1]->$myfieldname = $myname;
    }
    return $out;
}

function make_so_link($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'so_number') {
                $subarr->$subkey = '<a href="/edit-order-details/?action=viewso&so='.$subvalue.'">' . $subvalue  . '</a>';
            }
      }
    }
    return $arr;
}

function make_date($arr) {
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if (strpos(strtolower($subkey),'date') != false) {
                if ($subvalue != null) {
                    //$subarr->$subkey = date_format(date_create_from_format('Y-m-d', $subvalue), 'm-d-Y');
                    $subarr->$subkey = date('m-d-Y', strtotime($subvalue));
                }
            } else if ((strpos(strtolower($subkey),'time') != false)) {
                if ($subvalue != null) {
                    //$subarr->$subkey = date_format(date_create_from_format('Y-m-d H:i:s', $subvalue), 'm-d-Y h:i a');
                    $subarr->$subkey = date('m-d-Y h:i a', strtotime($subvalue));
                }
            }
        }
    }
    return $arr;
}

function make_MySQLdate($arr) {
    foreach ($arr as $key => $value) {
        if ((strpos(strtolower($value[1]),'date') != false)  || (strpos(strtolower($value[1]),'time') != false)){
            if ($value[3] == "") {
                $arr[$key][3] = NULL;
            } else {
                $arr[$key][3] = date('Y-m-d H:i:s', strtotime(str_replace(['-','*','.','_','?','\\','+','`','~','<','>',';','\'','"','=',')','(','&','^','%','$','#','@','!','|'], '/', $value[3])));
            }
        }
    }
    return $arr;
}

function make_MySQLdate2($arr) {
    foreach ($arr as $key => $value) {
        if (strpos($key, '_time') !== false) {
            if($value != '') {
                $arr[$key] = date('Y-m-d H:i:s', strtotime(str_replace(['-', '*', '.', '_', '?', '\\', '+', '`', '~', '<', '>', ';', '\'', '"', '=', ')', '(', '&', '^', '%', '$', '#', '@', '!', '|'], '/', $arr[$key])));
            }else{$arr[$key] = NULL;}
        }
        elseif(strpos($key, '_date') !== false){
            if($value != '') {
                $arr[$key] = date('Y-m-d', strtotime(str_replace(['-', '*', '.', '_', '?', '\\', '+', '`', '~', '<', '>', ';', '\'', '"', '=', ')', '(', '&', '^', '%', '$', '#', '@', '!', '|'], '/', $arr[$key])));
            }else{$arr[$key] = NULL;}
        }
    }

    return $arr;
}

function array_value_recursive($key, array $arr){
    $val = array();
    array_walk_recursive($arr, function($v, $k) use($key, &$val){
        if(($k == $key)&&($v != null)) array_push($val, $v);
    });
    return  $val;
}

function allowed_to_change($field_changed, $rolemap, $userrole) {
    $colroleidx = array_search($field_changed, array_column($rolemap, 'field_name'));
    if ((in_array('administrator',$userrole)!= false)
        || in_array(($rolemap[$colroleidx]['group']), $userrole)
        || (in_array('fm',$userrole)!= false)) {
        return true;
    } else {
        return false;
    }
}


function findkey($arr, $ikey) {
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            $foundat = strpos(strtolower($subkey),$ikey);
            if ($foundat === false) {
            } else {
                if ($foundat >= 0) {
                    return true;
                }
            }
        }
    }
    return false;
}

function make_vehicle_id_fuel_link($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'vehicle_id') {
                $subarr->$subkey = '<A HREF="/avte-vehicle-log/fuel-entry/?mycar='.$subvalue.'">' . $subvalue  . '</a>';
               // <a href="pagename.hml#anchor">link text</a>
                http://70.42.103.170/avte-vehicle-log/

            }
        }
    }
    return $arr;
}

function make_maintenance_record_link($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'id') {
                $maintenance_record_val = $subvalue;
            }
            if ($subkey == 'explanation_of_service') {
                $explanation_of_service_val = $subvalue;
            }
            $subarr->explanation_of_service = '<A HREF="/vehicle-log/maintenance-entry/maintenance-detail/?mymaintrec='.$maintenance_record_val.'&mycar='.$subarr->car_id.'">' . $explanation_of_service_val  . '</a>';
            // <a href="pagename.hml#anchor">link text</a>

        }
    }
    return $arr;
}

function navigation_links($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'vehicle_id') {
                $vehicleID = $subvalue;

            }
            if ($subarr->TYPE == 'EV') {
                $subarr->navigation = '<A HREF="/vehicle-log/charge-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Charge" src="/wp-content/uploads/2015/10/Charge.png" title="Charge Log" width="20" height="20"> </a> | <A HREF="/vehicle-log/maintenance-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Maintenance" src="/wp-content/uploads/2015/10/Maintenance.jpg" title="Maintenance Log"  width="20" height="20"> </a>';
            } elseif ($subarr->TYPE == 'PHEV') {
                $subarr->navigation = '<A HREF="/vehicle-log/fuel-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Fuel" src="/wp-content/uploads/2015/10/Fuel.jpg" title="Fuel Log" width="20" height="20"> </a> | <A HREF="/vehicle-log/charge-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Charge" src="/wp-content/uploads/2015/10/Charge.png" title="Charge Log" width="20" height="20"> </a> | <A HREF="/vehicle-log/maintenance-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Maintenance" src="/wp-content/uploads/2015/10/Maintenance.jpg" title="Maintenance Log" width="20" height="20"> </a>';
            } else {
                $subarr->navigation = '<A HREF="/vehicle-log/fuel-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Fuel" src="/wp-content/uploads/2015/10/Fuel.jpg" title="Fuel Log" width="20" height="20"> </a> | <A HREF="/vehicle-log/maintenance-entry/?mycar=' . $vehicleID . '"> <img border="0" alt="Maintenance" src="/wp-content/uploads/2015/10/Maintenance.jpg" title="Maintenance Log" width="20" height="20"> </a>';
            }

            // <a href="pagename.hml#anchor">link text</a>

        }
    }
    return $arr;
}

function kWh_links ($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'vehicle_id') {
                $vehicleID = $subvalue;
            }
            if ($subkey == 'cumulative_kWh') {
                $cumulative_kWh = $subvalue;
            }
        }
        $subarr->cumulative_kWh = '<A HREF="/vehicle-log/month-end/?mycar=' . $vehicleID . '">' . $cumulative_kWh  . '</a>';
        // <a href="pagename.hml#anchor">link text</a>
    }
    return $arr;
}

function sample_links ($arr) {
    //Get the Grouping fields setup for later lookup
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'id') {
                $sampleGroupID = $subvalue;
            }
            if ($subkey == 'quantity') {
                $uniqueSampleQuantity = $subvalue;
            }
        }
        $subarr->quantity = '<A HREF="/chain-of-custody/unique-sample/?mysamplegroup=' . $sampleGroupID .'&projectID='.$subarr->project_id.'">' . $uniqueSampleQuantity  . '</a>';
        // <a href="pagename.hml#anchor">link text</a>
    }
    return $arr;
}

function client_links ($arr) {
    //Get the Grouping fields setup for later lookup

    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'client_name') {
                if ($subvalue == null){
                    $clientLabel = 'No Client Assigned';
                    $client = null;
                } else {
                    $clientLabel = $subvalue;
                    $client = $subvalue;
                }
            }
        }

            $subarr->client_name = '<A HREF="/chain-of-custody/?myclient=' . $client . '&view=ProjectID">' . $clientLabel . '</a>';
            $subarr->id = '';
        // <a href="pagename.hml#anchor">link text</a>
    }
    return $arr;
}

function projectid_links ($arr) {
    //Get the Grouping fields setup for later lookup

    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'project_id') {
                $projectid = $subvalue;
            }
            if ($subkey == 'client_name') {
                $client = $subvalue;
            }
        }
        if($projectid == NULL) {
            $subarr->project_id = NULL;
        }else{
        $subarr->project_id = '<A HREF="/chain-of-custody/?projectid=' . $projectid .'&myclient='.$client.'&view=Main">' . $projectid  . '</a>';
        }
        // <a href="pagename.hml#anchor">link text</a>
    }
    return $arr;
}

//function infinitaz_solr_facet_filter_admin_menu() {
//    add_options_page('SOLR Facet Filter', 'SOLR Facet Filter', 'manage_options', 'solr-facet-filter/solr-facet-filter-admin-settings.php');
//}

//function infinitaz_solr_facet_filter_register_options() {
//    add_option( "isff_title", "Browse by attribute" );
//    add_option( "isff_facets");
//    add_option( "isff_sortby");
//}

//if ( is_admin() ) {
//    add_action( "admin_menu", "infinitaz_solr_facet_filter_admin_menu" );
//    add_action( "admin_init", "infinitaz_solr_facet_filter_register_options" );
//}

function make_documentation($arr) {
    //Get the Grouping fields setup for later lookup
    global $current_user;
    $myid = 1;
    $rowid = "";
    $vehicleID = "";
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => & $subvalue) {
            if ($subkey == 'id') {
                $rowid = $subvalue;
            }
            if ($subkey == 'car_id') {
                $vehicleID = $subvalue;
            }

            if ($_POST['uploaddoc'] == "true"){
            	if ($myid ==1){
		            $subarr->service_documentation =  uploader_form();
	            }
            //$subarr->service_documentation =  uploader_form();
            //$subarr->service_documentation =  "<div class='right'><input id='fileupload' type='file' name='files[]' data-url='uploads/Maintenance/" . $vehicleID ."/" . $rowid . "/ multiple></div>";
            //$subarr->service_documentation =  "<div class='right'>" . do_shortcode('[wordpress_file_upload uploadid="' . $myid . '" uploadpath="uploads/Maintenance/' . $vehicleID .'/' . $rowid . '" createpath="true" notifyrecipients="gavinrob92@gmail.com"  notify="true" duplicatespolicy="reject" selectbutton="Select File ..." uploadtitle=""]' .  "</div>" );
            $subarr->service_documentation .=  "<div class='left' >" . do_shortcode('[MMFileList folder = "/Maintenance/' . $vehicleID .'/' . $rowid . '"]') .  "</div>" ;
            } else {
                $subarr->service_documentation = "<div class='right'></div><div class='left' >" . do_shortcode('[MMFileList folder = "/Maintenance/' . $vehicleID . '/' . $rowid . '"]') . "</div>";
            }

            $myid += 1;
        }
    }
    return $arr;
}

// Get record verification date
function isRecordVerified($table, $recordID){
    global $wpdb;
    switch ($table) {
        case fuel:
            $sql = "SELECT
              en_avte_fuel.verify_date
            FROM en_avte_fuel
            WHERE en_avte_fuel.id = '%s'";
            break;
        case electric:
            $sql = "SELECT
              en_avte_electric.verify_date
            FROM en_avte_electric
            WHERE en_avte_electric.id = '%s'";
            break;
        case maintenance:
            $sql = "SELECT
              en_avte_maintenance.verify_date
            FROM en_avte_maintenance
            WHERE en_avte_maintenance.id = '%s'";
            break;
        case monthend:
            $sql = "SELECT
              en_avte_monthend.verify_date
            FROM en_avte_monthend
            WHERE en_avte_monthend.id = '%s'";
            break;

        default:
    }

    $recordDateVerification = $wpdb->get_results($wpdb->prepare($sql, $recordID), OBJECT);
    $recordDateVerification = $recordDateVerification[0]->verify_date;

    return $recordDateVerification;
}

// has the time period the user is trying to enter a fuel or charge record into already been reported on.  If so ONLY allow "fm" users to make the change
function hasTimePeriodAlreadyBeenReported($table, $change, $current_user, $cur_time){

    //has the timeframe that the record is being change into already been reported on.  If so only allow fleet managers to make this change
    switch ($table) {
        case fuel:
            $eventDateName = 'fuel_date';
            break;
        case electric:
            $eventDateName = 'plugin_unplug_dat_time';
            break;

        default:
    }
    if ($change[1] == $eventDateName){
        if (((date("m",strtotime($change[3]))== date("m",strtotime($cur_time))) && (date("Y",strtotime($change[3]))== date("Y",strtotime($cur_time))))  ||
            (count(array_intersect(array("fm", "administrator"), $current_user->roles)) > 0)){
            $timeFrameAlreadyVerified = false;
        }else{$timeFrameAlreadyVerified = true;}

    }else{$timeFrameAlreadyVerified = false;} // this will allow any user changing any field other than the event date to make a change to any record.  Thus, we rely on the "isRecordVerified"
    //function above to gard against changes being made to already verified/reported time periods
    return $timeFrameAlreadyVerified;
}

//get user permissions and place them in $current_user->roles object to later check whether they have permission to enter a page

//function placeUserPermissionInsideRolesArray($current_user)
//{
//    if (($current_user->roles[0] != 'administrator') && (!(empty($current_user->roles)))) {
//        $i = 0;
//        foreach ($current_user->roles as $value) {
//            $current_user->roles[$i] = strtoupper($value);
//            $i++;
//        }
//    } elseif (empty($current_user->roles)){
//        $i = 0;
//        foreach (array_keys($current_user->caps) as $key =>$value) {
//            $current_user->roles[$i] = $value;
//            $i++;
//        }
//    }
//    return $current_user;
//}

// check to see if user has permission to enter the page

function doesUserHavePermissionToEnterPage($user, $permissibleUserArray)
{
    if (count(array_intersect($permissibleUserArray, $user->roles)) > 0){
        return true;
    } else{ return false;}


}

function en_mail( $ramdomizer=0, $to='', $template='', $param='', $subject='', $message='', $from='', $from_friendly ='', $image='') {
    //randomizer is here so that I can send a different param so that it can trigger the cron job without waiting 10min.
    if($to == '') $to=get_option('admin_email');
    if($from == '') $from = get_option('admin_email');
    if($message == '') $message = $subject;
    if($template != '')
    {
        $page = get_page_by_title( 'Email-'.$template );
        $message = do_shortcode(get_post_field( 'post_content', $page ));
        foreach ($param as $key => $subarr) {
            $message = str_replace($key,$subarr,$message);
        }
        //$message = str_replace('%first_name%',$param['first_name'],$message);
        if($subject == '') $subject = get_post_field( 'post_excerpt', $page );
    }
    $headers[] = 'MIME-Version: 1.0' . "\r\n";
    $headers[] = 'Content-Type: text/html; charset="UTF-8"' . "\r\n";
    $headers[] = 'From: ' . $from_friendly . ' <' . $from . '>' . "\r\n";

    if ($image){
        global $phpmailer;
        $email_guid = uniqid(); //Necessary as some email clients will show old images if not done.

        add_action( 'phpmailer_init', function(&$phpmailer)use($image,$email_guid){
            $phpmailer->SMTPKeepAlive = true;
            $i=0;
            foreach ($image as $key => $subarr) {
                $i++;
                $phpmailer->AddStringEmbeddedImage($subarr, 'image-'.$i.'@'.$email_guid,'image'.$email_guid.'.png');
            }
        });
        $message = str_replace('GUID',$email_guid,$message);
    }

    $mail = wp_mail($to, $subject, $message, $headers);
    if ($mail) {
        error_log("Email OK ".$ramdomizer."-From: ".$from.", To: ".$to.", Subject:".$subject);
        return true;
    } else {
        global $ts_mail_errors;
        global $phpmailer;
        if (!isset($ts_mail_errors)) $ts_mail_errors = array();
        $mailerror ='';
        if (isset($phpmailer)) {
            $mailerror = $phpmailer->ErrorInfo;
        }
        error_log("Email FAIL-Error: ".$mailerror.",From: ".$from.", To: ".$to.", Subject:".$subject);
        return false;
    }
}
add_action( 'en_mail_queue', 'en_mail', 10, 9 ); //priority 10, takes 9 parameters

function en_text_admin_field($id, $setting, $size, $placeholder, $value, $explanation)
{
    return "<input id='{$id}' name='{$setting}' size='{$size}' type='text' placeholder='{$placeholder}' value='{$value}' />
                <p style=\"color: #777;\">{$explanation}</p>";
}