<?php

//Go make the files and variables be available
    require_once realpath(dirname(__file__)) . "/en-coc-main-form.php";
    wp_register_script('en-coc-main-js', ISTO_URL . "/en-coc-main/en-coc-main.js", array('jquery'),
        filemtime(ISTO_DIR . '/en-coc-main/en-coc-main.js'));
//    wp_register_script('en-avte-electric-mobile-js', ISTO_URL . "/en-avte-electric/en-coc-main-mobile.js", array('jquery'),
//        filemtime(ISTO_DIR . '/en-avte-electric/en-coc-main-mobile.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxsteditorder', 'ajax_steditorder' );
    add_action('wp_ajax_ajaxen_coc_main_new', 'ajax_en_coc_main_new');
    add_action('wp_ajax_ajaxen_coc_main_change', 'ajax_en_coc_main_change');
    add_action('wp_ajax_ajaxen_coc_main', 'ajax_en_coc_main');
    add_action('wp_ajax_ajaxen_coc_main_deleterecord', 'ajax_en_coc_main_deleterecord');


    add_shortcode('en_coc_main', 'en_coc_main');
    function en_coc_main($atts, $content = '')
    {
        wp_enqueue_script('st-hot-js');
        wp_enqueue_style('st-hot-css');
        wp_enqueue_script('st-track-order-js');
        wp_enqueue_style('st-track-order-css');


        $output = en_coc_main_form();
        return $output;
    }


function ajax_en_coc_main(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );
    $view = $_POST['view'];
    global $current_user;
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if (isset($user->roles) && is_array($user->roles)) {
            if (count(array_intersect(array("cc", "administrator"), $user->roles)) > 0) {

                switch ($view) {
                    case "":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_coc_main-client'
                                OR st_default_cols.table_name = 'en_coc_main-client.id'
                                ORDER BY st_default_cols.`order`";
                        break;
                    case "ProjectID":
                            $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_coc_main-projectid'
                                OR st_default_cols.table_name = 'en_coc_main-client.id'
                                ORDER BY st_default_cols.`order`";
                        break;
                    case "Main":
                        $sql = "SELECT
                                  st_default_cols.field_name,
                                  st_default_cols.`group`,
                                  st_default_cols.name
                                FROM st_default_cols
                                WHERE st_default_cols.table_name = 'en_coc_main'
                                OR st_default_cols.table_name = 'en_coc_main-client'
                                OR st_default_cols.table_name = 'en_coc_main-projectid'
                                ORDER BY st_default_cols.`order`";
                        break;

                    default:
                }
            }
        }
    }
    try {
        $wpdb->flush();
        $default_cols = $wpdb->get_results($sql, OBJECT);
        if (false === $wpdb->result) {
            $json['errors']['lookup'] = "Default Columns:  ".$wpdb->last_error;
        } else {

//          Get Table Data
            switch ($view) {
                case "":
                    $sql = "SELECT
                      en_coc_main.client_name, 
                      en_coc_main.id
                      FROM en_coc_main
                    GROUP BY en_coc_main.client_name";
                    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['myclient']), OBJECT );
                    Break;

                case "ProjectID":
                    if($_POST['showactive'] == 'false') {
                        $sql = "SELECT
                              en_coc_main.project_id,
                              en_coc_main.id,
                              en_coc_main.client_name
                            FROM en_coc_main
                            WHERE en_coc_main.client_name = '%s'
                            GROUP BY en_coc_main.project_id
                            ORDER BY en_coc_main.project_id";
                        $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['myclient']), OBJECT );
                    } else {
                        $sql = "SELECT
                              en_coc_main.project_id,
                              en_coc_main.id,
                              en_coc_main.client_name
                            FROM en_coc_main
                            WHERE en_coc_main.client_name = '%s'
                            AND en_coc_main.active_project = 'true'
                            GROUP BY en_coc_main.project_id
                            ORDER BY en_coc_main.project_id";
                        $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['myclient']), OBJECT );
                    }
                    Break;

                case "Main":
                    $sql = "SELECT
                              en_coc_main.*,
                              COUNT(en_coc_unique.sample_group_id) AS quantity
                            FROM en_coc_main
                              LEFT OUTER JOIN en_coc_unique
                                ON en_coc_main.id = en_coc_unique.sample_group_id
                            WHERE en_coc_main.project_id = '%s'
                            AND en_coc_main.client_name = '%s'
                            GROUP BY en_coc_main.id,
                                     en_coc_main.client_name,
                                     en_coc_main.project_id,
                                     en_coc_main.client_name,
                                     en_coc_main.description,
                                     en_coc_main.type,
                                     en_coc_main.msds,
                                     en_coc_main.hazardous_materials,
                                     en_coc_main.disposition_method,
                                     en_coc_main.recieved_by,
                                     en_coc_main.origin,
                                     en_coc_main.handling_instructions,
                                     en_coc_main.recieved_dat_time";
                    $dbdata = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['projectid'], $_POST['myclient']), OBJECT );
                    Break;
            }
	        if (false === $wpdb->result) {$json['errors']['lookup'] = "Dbdata Table Query Problem:  ".$wpdb->last_error;}

            $dbdata = make_date($dbdata);
            switch ($view) {
                case "":
                    $linked_dbdata = client_links ($dbdata);
                    Break;
                case "ProjectID":
                    $linked_dbdata = projectid_links ($dbdata);
                    Break;
                case "Main":
                    $linked_dbdata = sample_links ($dbdata);
                    Break;
            }
        }
    } catch (Exception $e) {
        $json['errors']['initial'] = $e->getMessage();
    }

    //Send Response
    $json['colInfo'] = $default_cols;
    $json['allcols'] =$linked_dbdata;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    die();
}

function ajax_en_coc_main_new(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'newid' => '' );
    //Handle each change
    $cur_time = current_time( 'mysql' );

	//As a security measure make sure someone is NOT trying to crash the DB with bulk entries
	$sqlEntryRate = "SELECT
                COUNT(en_coc_history.field) AS entryRate
                FROM en_coc_history
                WHERE en_coc_history.date >= DATE_SUB(NOW(), INTERVAL 60 SECOND)
                AND en_coc_history.after = 'NEW'
                AND (en_coc_history.source = 'auto'
                OR en_coc_history.source = 'Autofill.fill')";
	$entryRate = $wpdb->get_results( $sqlEntryRate, OBJECT );
//	check query result
	if ($wpdb->result === false) {
		$json['errors']['lookup'] = "Entry Rate Checker:  ".$wpdb->last_error;
	}

    for ($i = 0; $i < $_POST['amount']; $i++) {
        //Now, let's insert a new record
        try {
            $wpdb->flush();
	        // Limit number of rows entered within a give time period to protect against maliscious activity.  Also make sure there are no DB errors to this point
	        if ($entryRate[0]->entryRate > 25 || count($json['errors']) > 0 ) {
		        $dbresult = false;
	        }else {
		        if ( $_POST['addType'] != "UndoRedo.undo" ) {
			        $dbresult = $wpdb->insert( 'en_coc_main',
				        array(
					        'project_id' => $_POST['projectid']
				        )
			        );
		        } else {
			        $dbresult = $wpdb->insert( 'en_coc_main',
				        array(
					        'id' => $_POST['idsToAddBackIn'][ $i ]
				        )
			        );
		        }

		        $json['newid'][ $i ] = $wpdb->insert_id;

		        //	        check insert result
		        if ((0 === $dbresult) || (false === $dbresult) ||  $wpdb->result === false) {
			        $json['errors']['lookup'] = "Insert Row: ".$wpdb->last_error;
			        $json[errors][$i] = true;
		        }
	        }

            if (count($json['errors']) == 0){
                //Log the insert
                $dbdata = array(
                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
                    'field' => 'NEW',
                    'before' => 'NEW',
                    'after' => 'NEW',
                    'source' => $_POST['addType'],
                    'table_id' => $json['newid'][$i],
                    'source_table'  => 'Main',
                );
                $wpdb->insert( 'en_coc_history', $dbdata );
            }
        } catch (Exception $e) {
            $json[errors][$i] = $e->getMessage();
        }
    }

    //Send Response
    $json['user'] = $current_user->data->display_name;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:i a');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

function ajax_en_coc_main_deleterecord(){
    global $wpdb, $current_user;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'deletedid' => '' );

//    // First check the nonce, if it fails the function will break
//    check_ajax_referer( 'ajax-steditorderdetails-nonce', 'security' );

    $cur_time = current_time( 'mysql' );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_coc_main'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );

// determine whether there are any unique items recorded under this group before deleting
    $placeHolder = implode(', ', array_fill(0, count($_POST['groupID']), '%s'));

    $sql = "SELECT
                COUNT(en_coc_unique.sample_group_id) AS unique_items
                FROM en_coc_unique
                WHERE en_coc_unique.sample_group_id IN ($placeHolder)";
    $unique_items = $wpdb->get_results( $wpdb->prepare( $sql, $_POST['groupID']), OBJECT );

//	check result
	if ($wpdb->result === false) {
		$json['errors']['lookup'] = "Unique_Items Query Checker:  ".$wpdb->last_error;
	}

    $user = $current_user;

    $dbindex = 0;

////Make the deletion
    foreach ($_POST['id'] as $id) {

//    	see if user has correct role, that there are no errors up to this point and there are no unique items.  If false in any of these cases the deletion should not be made
        if ((allowed_to_change('id', $rolemap, $user->roles))&&($unique_items[0]->unique_items == 0) && count($json['errors']) == 0){
            try {
                $wpdb->flush();
                $sql = "DELETE FROM en_coc_main
	            WHERE id = '%s'";
                $dbresult = $wpdb->query($wpdb->prepare($sql, $id));
                if ((0 === $dbresult) || (false === $dbresult) ||  $wpdb->result === false ) {

                    $json['errors']['lookup'] = "DbResult query had an issue:  ".$wpdb->last_error;
                    $json[errors][$dbindex] = true;
                }

	            if (count($json['errors']) == 0){
	                //Log the deletion
	                $dbdata = array(
		                'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
		                'field' => 'RECORD DELETED',
		                'before' => 'Active',
		                'after' => 'Deleted',
		                'source' => 'Delete',
		                'table_id' => $id,
		                'source_table'  => 'Main',

	                );
	                $wpdb->insert('en_coc_history', $dbdata);
                }
            } catch (Exception $e) {
                $json[errors][$dbindex] = $e->getMessage();
            }
        }
        elseif(($unique_items > 0) and count($unique_items) != 0){
            $json[errors][$dbindex] = "This record has subrecords associated with it.  You must first delete the subrecords to delete this record";
            break;
        }
        elseif(count($json['errors']) > 0){
	        break;
        }else {
            //user did not have permissiong to make deletion
            $json[errors][$dbindex] = 'role';
        }
        $dbindex++;
    }

    //Send Response
//http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['deletedid'] = $_POST['id'];
    $json['unique_items'] = $unique_items;
    $json['cur_time'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y h:ia');
    $json['cur_date'] = date_format(date_create_from_format('Y-m-d H:i:s', $cur_time), 'm-j-Y');
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}


function ajax_en_coc_main_change(){
    global $wpdb, $current_user;
    $json = array( 'error' => true, 'success' => false, 'errors' => array() );

    //Grab the column to role security mapping and current user's role
    $sql = "SELECT
              st_default_cols.field_name,
              st_default_cols.`group`
            FROM st_default_cols
            WHERE st_default_cols.table_name = 'en_coc_main'";
    $rolemap = $wpdb->get_results( $sql, ARRAY_A );
    $user = $current_user;

    //Handle each change
    $dbindex = 0;
    $changes = make_MySQLdate($_POST['data']);
    foreach ($changes as &$change) {

        $logit = true;
        //skip ID change column notifications
        if (!( 'id' == $change[1] )) {
            //See if they are allowed to make this change
            if (allowed_to_change($change[1], $rolemap, $user->roles)){
                //Make the change
                try {
                    $wpdb->flush();
                    $table = "en_coc_main";
                    $dbresult = $wpdb->update( $table, array($change[1] => $change[3]), array('id' => $change[4]));

                    if ( ( false === $dbresult ) || (false === $wpdb->result)) {
                        $json[errors][$dbindex] = "DbResult Update Problem:  ".$wpdb->last_error;
                    }else{
	                    //Log the change
	                    if ($logit) {
		                    $dbdata = array(
			                    'user' => $current_user->user_firstname . ' ' . $current_user->user_lastname,
			                    'field' => $change[1],
			                    'before' => $change[2],
			                    'after' => $change[3],
			                    'source' => $_POST['source'],
			                    'table_id' => $change[4],
			                    'source_table' => 'Main'
		                    );
		                    $wpdb->insert('en_coc_history', $dbdata);
	                    }
                    }
                } catch (Exception $e) {
                    $json[errors][$dbindex] = $e->getMessage();
                }
            } else {
                //user did not have permissiong to make change
                $json[errors][$dbindex] = "ERROR SAVING! You don't have permission.";
            }
        }
        //user tried to change record "id" which is NEVER permissible
        else{
//        	undo needs to be able to add the deleated ID back in and therefore needs access to change it
            if ($_POST['source'] != 'UndoRedo.undo'){
                $json[errors][$dbindex] = true;}
        }
        $dbindex++;
    }

    //Send Response
    //http://wordpress.stackexchange.com/questions/16382/showing-errors-with-wpdb-update
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>
