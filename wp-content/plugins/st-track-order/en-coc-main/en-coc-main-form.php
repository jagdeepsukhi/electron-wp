<?php


function en_coc_main_form()
{
    global $current_user;
    $view = $_GET["view"];
    $myclient = $_GET["myclient"];
    $projectid = $_GET["projectid"];
    $showactive = $_GET["showactive"];
    ob_start();

    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'cc'))) {
              //  if ( wp_is_mobile() == false ){
                    wp_enqueue_script('en-coc-main-js');

                    if ($view == "Main"){
                    ?>
                    <h5></h5>
                    <h3>Client: <?php echo $myclient; ?></h3>
                    <h5>Project ID: <?php echo $projectid; ?> </h5>
                    <?php }elseif($view=="ProjectID"){
                    ?>
                    <h5></h5>
                    <h3>Client: <?php echo $myclient; ?></h3>
                    <h5>Client Projects Numbers <button type="button" name="show_all_or_active_projects_button" id = "show_all_or_active_projects_button" value = "true" onclick="showAllOrActiveProjects()">
                            <?php if ($showactive == 'false') { ?> Click to Show Active Projects ONLY <?php } else{ ?> Click to Show All Projects <?php } ?> </button><BR></h5>

                    <?php } else{ ?>
                    <h5></h5> <h5>Select Client </h5> <?php
                    } ?>
                    <div class="en_coc_main_form">
                      <!--  Search: <input id="en_coc_main_searchgrid" type="text" />  -->
                        <div id="en_coc_main_grid" class="dataTable"></div>
                       <!-- <div id="en_coc_main_paginggrid" class="pagination"></div> -->
                    </div> <!--en_coc_main_form-->

                    <a name="BOTTOM"></a>
                    <?php

                //}
//                else {
//                    ?>
<!--                    --><?php
//                    wp_enqueue_script('en-coc-main-mobile-js');
//                        wd_form_maker(11);
//                    ?>
<!---->
<!---->
<!--                    <div id="en_coc_main_mobile_grid" class="dataTable"></div>-->
<!---->
<!--                    --><?php
//                }
            }
            else {
                ?>
                <div class="en_coc_main_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_coc_main_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_coc_main_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_coc_main_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

?>