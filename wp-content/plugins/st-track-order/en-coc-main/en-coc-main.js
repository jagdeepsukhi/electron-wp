var en_coc_main_cellRender;
var delete_record_call;
var showAllOrActiveProjects;


(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_coc_main_grid'),
    en_coc_main_searchgrid = $$('en_coc_main_searchgrid'),
    en_coc_main_paginggrid = $$('en_coc_main_paginggrid'),
    en_coc_main_hot,
    firstInsert= 0,
    filtering=0,
    myData = [],
    noOfRowstoShow = 10000,
    myData_charge_event_col_num = 0,
    recordnum = [],
    en_coc_main_hot;
    idsToAddBackIn = [];

// if there is an instance where a client was created and not named.  Change myclient to '' so it can be found in the database
if (urlParam ('myclient') == 0){
            client = '';
    }else{
        client = urlParam ('myclient');
    }

jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_coc_main',
        'view': urlParam ('view'),
        'myclient': client,
        'projectid': urlParam ('projectid'),
        'showactive': urlParam ('showactive')

    },
    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. \n\nError: " + JSON.stringify(data.errors));
        } else {
            myData = data.allcols;
            buildHOT(data.colInfo);
            getgridData(myData, "1", 1000);
        }
    }
);

function buildHOT(icols) {
    en_coc_main_hot = new Handsontable(container, {
        startRows: 0,
        startCols: 0,
        autoRowSize: true,
        manualColumnResize: true,
        columns: GetColumnMeta(icols),
        colHeaders: GetHeaderNames(icols),
        manualRowResize: true,
        contextMenu: ['remove_row', 'undo', 'redo', 'row_above', 'row_below'],
        currentRowClassName: 'currentRow',
        currentColClassName: 'currentCol',
        className: "htCenter",
        fillHandle: true,
        // minSpareRows: 1,

        cells: function (row, col, prop) {
            return en_coc_main_cellRender({row: row, col: col, prop: prop, myhot: this.instance});
        },
        beforeUndo:function(action){

            idsToAddBackIn = getIdsToUndo(action, en_coc_main_hot);
        },
        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 0) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(en_coc_main_hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                // if there is an instance where a client was created and not named.  Change myclient to '' so it can be found in the database
                if (urlParam ('myclient') == 0){
                    client = '';
                }else{
                    client = urlParam ('myclient');
                }
                    jQuery.ajax({
                        url: ajax_sttrackorder_object.ajaxurl,
                        dataType: 'json',
                        async: false,
                        type: 'post',
                        data: {
                            'action': "ajaxen_coc_main_new",
                            'amount': amount,
                            'myclient': client,
                            'projectid': urlParam('projectid'),
                            'addType':action,
                            'idsToAddBackIn': idsToAddBackIn,
                         },
                        success: function (data) {
                            if (data.error == true) {
                                alert("Sorry, we can't add a new row.  Upon refeshing the added record will NOT be present. \n\nError: " + JSON.stringify(data.errors));
                                hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                            } else {
                                for (var i = 0; i < data.newid.length; i++) {
                                    //set email to R/W
                                    //hot.setCellMeta(index + i - row_offset, 'user_email', 'valid', 'false');
                                    en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                    var newrow = {id: data.newid[i]};
                                    myData.splice(myData.length, 0, newrow);

                                    if (action != 'UndoRedo.undo'){
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'active_project', 'true', 'auto_edit');
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'recieved_dat_time', data.cur_time, 'auto_edit');
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'client_name', urlParam('myclient'), 'auto_edit');
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'recieved_by', data.user, 'auto_edit');
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'project_id', urlParam('projectid'), 'auto_edit');
                                        en_coc_main_hot.setDataAtRowProp(index + i - row_offset, 'quantity', 0, 'loadData');
                                    }
                                }
                                hot_console.innerText = 'Row Inserted in DB';
                            }
                        }
                    });
            }
            firstInsert++;
        },

        beforeChange: function (changes, source) {
            // if user makes a gange that is equal to the current value do not bother making change... does NOT work for bulk changes
            if ((changes[0][2] == changes[0][3]) && (changes.length == 1)) {
                return false;
            }
            // do not allow changes to the client_name or project id column unless there is nothing in it or unless it is in the main table view where all changes can be made
            if (((changes[0][1] == 'client_name') || (changes[0][1] == 'project_id')) && (changes[0][2] != null) && ((urlParam('view') == null) || (urlParam('view') == 'ProjectID'))) {
                return false; //don't save this change
            }
            // if the user completely deletes the client name or project id do not save change
            if (urlParam ('view') !== "Main" && (((changes[0][1] == 'client_name') || (changes[0][1] == 'project_id'))) && (changes[0][3] == '')) {
                return false; //don't save this change
            }
            //we were getting an error when a blank was entered into the sample recipt dat time field. so if the entry is blank do not make change
            if (((changes[0][1].indexOf('time') >= 0) || (changes[0][1].indexOf('date') >= 0)) && (changes[0][3] == "")) {
                return false;
            }
        },

        afterChange: function (change, source) {
            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }
            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=en_coc_main_hot.getDataAtRowProp(change[i][0],'id');
                // change[i][5]=en_coc_main_hot.getDataAtRowProp(change[i][0],'user_id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_coc_main_change',
                    'data':change,
                    'source': source,
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the requested change. Upon refeshing the change you made will NOT be present. \n\nError: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';

                        }
                    }
                }
            });

        },

        beforeRemoveRow: function (index, amount) {


            //do not allow user to delete client name or project id unless they are in the Main table view where all changes are allowed to be made
            selected_column_number = en_coc_main_hot.getSelected();
            column_prop = en_coc_main_hot.colToProp(selected_column_number[1]);
            if (((column_prop == 'client_name') || (column_prop == 'project_id')) && ((urlParam('view') == null) || (urlParam('view') == 'ProjectID'))) {
                return false; //don't save this change
            }

            //Check to make sure we are not trying to delete the blank row at the bottom
            // lastRow = en_coc_main_hot.countRows() -1;
            // if (((index == lastRow) || (index + amount -1 == lastRow)) && (en_coc_main_hot.getDataAtRowProp(lastRow, 'id') == null)) {
            //     alert("You CANNOT delete the blank entry row (last row)."); return false;}

            // index the row(s) to be deleted with the record ID
            recordnum = [];

                i = 0;
                for (row = index; row <= index + (amount - 1); row++) {
                    recordnum[i] = en_coc_main_hot.getDataAtRowProp(row, 'id');
                    i++;
                }
                shouldWeDeleteTheRow = delete_record(index, amount, recordnum);

            //if there are subrecords associated with this record.  Advise the user they must delete the subrecords first
            if (shouldWeDeleteTheRow == false) {
                return false;
            }
        }
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_coc_main_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            en_coc_main_hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_coc_main_paginggrid).append(element);
    }
    en_coc_main_hot.loadData(part);
    return part;
}

en_coc_main_cellRender = function en_coc_main_cellRender(parameters) {
    var row = parameters.row;
    var col = parameters.col;
    var prop = parameters.prop;
    var myhot = parameters.myhot;
    var cellProperties = {};

        switch(prop) {
            case "id":
                cellProperties.readOnly = true;
                break;
            case "recieved_by":
                // cellProperties.readOnly = true;
                break;
            case "description":
            case "handling_instructions":
                cellProperties.type = 'text';
                cellProperties.renderer = customCellRenderer;
                break;
            case "msds":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Yes','No'];
                break;
            case "client_name":
                    // cellProperties.type = 'string';
                    cellProperties.renderer = customCellRenderer;
                break;
            case "project_id":
                // cellProperties.readOnly = true;
                cellProperties.renderer = customCellRenderer;
                break;
            case "quantity":
                cellProperties.renderer = customCellRenderer;
                cellProperties.readOnly = true;
                break;
            case "type":
                //http://utilitymill.com/utility/Regex_For_Range
                cellProperties.type = 'autocomplete';
                cellProperties.allowInvalid = false;
                cellProperties.strict = true;
                cellProperties.filter = true;
                cellProperties.source =  ['Accessory','Sample', 'Other'];
                break;
            case "active_project":
                cellProperties.type = 'checkbox';
                // cellProperties.renderer = idRowRenderer;
                break;

            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'M-D-YYYY h:mm a';
                cellProperties.correctFormat = true;
                cellProperties.allowInvalid = false;
            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = true;
            }
        }

        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }

    return cellProperties;
}

//Ajax call to server for record delete
delete_record_call = function delete_record_call(index, amount, recordnum) {
    groupID = [];
    //get group ID so we can verify whether a group within a project ID has items in the en_coc_unique table
    // this is only implemented on chain of custody "main".  When it is implemented on all tables this can be moved to
    //the 'before remove' hook (with a peice similar functionality) and passed through the function
    selected_row_number = en_coc_main_hot.getSelected();
    if(selected_row_number[0] <= selected_row_number[2]) {
        for (i = 0; i <= (selected_row_number[2] - selected_row_number[0]); i++) {
            groupID[i] = en_coc_main_hot.getDataAtRowProp(selected_row_number[0] + i, 'id');
        }
    }else{
        for (i = 0; i <= (selected_row_number[0] - selected_row_number[2]); i++) {
            groupID[i] = en_coc_main_hot.getDataAtRowProp(selected_row_number[2] + i, 'id');
        }
    }

    return jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        type: "POST",
        async: false,
        data: {
            action: "ajaxen_coc_main_deleterecord",
            amount: amount,
            index: index,
            id: recordnum,
            groupID: groupID,
        }
    }).responseText;
}

showAllOrActiveProjects = function showAllOrActiveProjects (){

    if ((urlParam ('showactive') =='false')) {
        location.href = URL_add_parameter(location.href, 'showactive', 'true');
    } else {
        location.href = URL_add_parameter(location.href, 'showactive', 'false');
    }
}

// This takes a passed parameter through the URL and can handle parameters with white spaces
function URL_add_parameter(url, param, value){
    var hash       = {};
    var parser     = document.createElement('a');
    parser.href    = url;
    var parameters = parser.search.split(/\?|&/);

    for(var i=0; i < parameters.length; i++) {
        if(!parameters[i])
            continue;
        var ary      = parameters[i].split('=');
        hash[ary[0]] = ary[1];
    }

    hash[param] = value;

    var list = [];
    Object.keys(hash).forEach(function (key) {
        list.push(key + '=' + hash[key]);
    });

    parser.search = '?' + list.join('&');
    return parser.href;
}

})( jQuery );


