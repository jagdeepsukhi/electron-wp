<?php

//Go make the files and variables be available
require_once realpath( dirname( __file__ ) ) . "/en-coc-history-form.php";
wp_register_script('en-coc-history-js', ISTO_URL."/en-coc-history/en-coc-history.js", array('jquery'),
    filemtime(ISTO_DIR.'/en-coc-history/en-coc-history.js'));

// Set Ajax callbacks. It needs both lines to allow people logged in or NOT logged in to run ajax, with the function specific below.
//add_action( 'wp_ajax_nopriv_ajaxstchangehistory', 'ajax_stchangehistory' );
add_action( 'wp_ajax_ajaxen_coc_history', 'ajax_en_coc_history' );


add_shortcode( 'en_coc_history', 'en_coc_history' );
function en_coc_history( $atts, $content = '' )
{
    wp_enqueue_script('st-hot-js');
    wp_enqueue_style('st-hot-css');
    wp_enqueue_script('st-track-order-js');
    wp_enqueue_style('st-track-order-css');
    wp_enqueue_script('en-coc-history-js');

    $output = en_coc_history_form();
    return $output;
}

function ajax_en_coc_history(){
    global $wpdb;
    $json = array( 'error' => false, 'success' => true, 'errors' => array(), 'allcols' => '' );

    //Go get the columns that should be there for all SO
    $sql = "SELECT
              date,
              source_table,
              table_id,
              user,
              field,
              `before`,
              after,
              source, 
              group_id

            FROM en_coc_history
            ORDER BY id DESC";
    $myres = $wpdb->get_results( $sql, OBJECT );

    //Send Response
    $json['allcols'] = $myres;
    $json['error'] = count($json['errors']) > 0;
    echo json_encode( $json );
    wp_die();
}

?>