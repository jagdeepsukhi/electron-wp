var delete_record_call;

(function($) {
var user;
var
    $$ = function(id) {
        return document.getElementById(id);
    },
    container = $$('en_avte_maintenance_items_select_grid'),
    en_avte_maintenance_items_select_searchgrid = $$('en_avte_maintenance_items_select_searchgrid'),
    en_avte_maintenance_items_select_paginggrid = $$('en_avte_maintenance_items_select_paginggrid'),

    en_avte_maintenance_items_select_autosaveNotification,
    hot,
    wait_for_data,
    rowmap ={},
    firstInsert= 0,
    filtering = 0,
    myData = [],
    noOfRowstoShow = 10000,
    firsttime = true,
    recordnum = [];
    idsToAddBackIn = [];



jQuery.post(
    ajax_sttrackorder_object.ajaxurl,
    {
        'action': 'ajaxen_avte_maintenance_items_select',
        'car': urlParam('mycar'),
        'maintenancerecord': urlParam('mymaintrec')
    },
    function (res) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(res);
        if (data.error == true) {
            alert("Sorry, there were errors loading the table. Error: " + JSON.stringify(data.errors));
        } else {
            var data = JSON.parse(res);
            myData = ObjToArr(data.allcols);
            myMaintenanceSelection = data.maintenance_items_selection;
            buildHOT(data.allcols);
            getgridData(data.allcols, "1", 1000);
        }
    }
);

function buildHOT(idata) {
    hot = new Handsontable(container, {
        columns: GetHeaders(idata),
        startRows: 0,
        startCols: 0,
        fixedRowsTop: 2,
        autoColumnSize: true,
        contextMenu: ['remove_row', 'undo', 'redo'],
        minSpareRows: 1,

        cells: function (row, col, prop) {
            return en_avte_maintenance_items_select_cellRender({row: row, col: col, prop: prop, myhot: this.instance});
        },

        beforeUndo:function(action){

            idsToAddBackIn = getIdsToUndo(action, hot);
        },

        afterCreateRow: function (index, amount, action) {
            //we don't want to create a new detail ID when the form first opens and automatically creates a blank row, or when filtering
            if ((firstInsert >= 2) && (filtering ==0)) { //Needs to be the number of tables in the page times two (first NEW, and then loaddata), zero based.
                //Detect if this is a new row add by typying or by other method (i.e.undo)
                var row_offset = (index==0)?0:(hot.getDataAtRowProp(index-1,'id') == null ) ? 1:0;
                //Go add the record to the table and return the ID
                // if there is an instance where a client was created and not named.  Change myclient to '' so it can be found in the database

                jQuery.ajax({
                    url: ajax_sttrackorder_object.ajaxurl,
                    dataType: 'json',
                    async: false,
                    type: 'post',
                    data: {
                        'action': "ajaxen_avte_maintenance_items_select_new",
                        'amount': amount,
                        'addType':action,
                        'idsToAddBackIn': idsToAddBackIn
                    },
                    success: function (data) {
                        if (data.error == true) {
                            alert("Sorry, we can't add a new row. Error: " + JSON.stringify(data.errors));
                            hot_console.innerText = 'ERROR saving the added row! Refresh the table and try again';
                        } else {
                            for (var i = 0; i < data.newid.length; i++) {
                                //set email to R/W
                                //hot.setCellMeta(index + i - row_offset, 'user_email', 'valid', 'false');
                                hot.setDataAtRowProp(index + i - row_offset, 'id', data.newid[i], 'new_row')
                                var newrow = {id: data.newid[i]};
                                myData.splice(myData.length, 0, newrow);

                                if (action != 'UndoRedo.undo'){
                                    hot.setDataAtRowProp(index + i - row_offset, 'show_in_list', 'false', 'auto_edit')
                                }

                            }
                            hot_console.innerText = 'Row Inserted in DB';
                        }
                    }
                });
            }
            firstInsert++;
        },

        afterChange: function (change, source) {

            //change: 0-record#, 1-prop name, 2-before, 3-after, 4-DB_ID, 5-display_name
            if ((source === 'loadData') || (source === 'new_row')|| (source === 'lookup')) {
                return; //don't save this change
            }

            if (change[0][1] == 'maintenance_item') {
                serviceTitleChangeConfirmation = confirm("******Warning****** \n\n Changing the \"Service Title Description\" at the " +
                    "adminitrative level will change all service titles fitting this description from their " +
                    "original entry to the updated entry.\n Please confirm this is what you would like to do or press \"Cancel\"");
                if (!(serviceTitleChangeConfirmation)) {
                    hot.setDataAtRowProp(change[0][0], change[0][1], change[0][2], 'loadData');
                    return;
                }
            }

            //Let's add the actual SQL record number and user name to the *change* variable
            //Also, get of rid invalid changes. Stupid HOT was inserting bad (hidden) columns when undoing a removed row
            for(var i = 0; i < change.length; i++){
                change[i][4]=hot.getDataAtRowProp(change[i][0],'id');
                // change[i][5]=hot.getDataAtRowProp(change[i][0],'user_id');
                if (isNumber(change[i][1])) {
                    change.splice(i, 1);
                    i--;
                }
            }

            //Go change the record in the DB
            jQuery.ajax({
                url: ajax_sttrackorder_object.ajaxurl,
                dataType: 'json',
                async: true,
                type: 'post',
                data: {
                    'action': 'ajaxen_avte_maintenance_items_select_change',
                    'data': change,
                    'source': source
                },
                success: function (data) {
                    if (data.error == true) {
                        alert("Sorry, we can't make the following changes. Error: " + JSON.stringify(data.errors));
                        if (data.errors[0] == "ERROR SAVING! You don't have permission.") {
                            hot_console.innerText =
                                "ERROR SAVING! You don't have permission.";
                        } else {
                            hot_console.innerText = 'NOT Saved';
                        }
                    } else {
                        //Now that DB is successful, let's change local data var
                        for(var i = 0; i < change.length; i++){
                            //get the internal index from iid, the right prop, and set with new value
                            myData[change[i][0]][change[i][1]] = change[i][3];
                            hot_console.innerText = 'Changes saved';
                        }
                    }
                }
            });
        },

        beforeRemoveRow: function (index, amount) {

            //Check to make sure we are not trying to delete the blank row at the bottom
            lastRow = hot.countRows() -1;
            if (((index == lastRow) || (index + amount -1 == lastRow)) && (hot.getDataAtRowProp(lastRow, 'id') == null)) {
                alert("You CANNOT delete the blank entry row (last row)."); return false;}

            // index the row(s) to be deleted with the record ID
            recordnum = [];
            if (index > 1) {
                i = 0;
                for (row = index; row <= index + (amount - 1); row++) {
                    recordnum[i] = hot.getDataAtRowProp(row, 'id');

                    i++;
                }
                delete_record(index, amount, recordnum);
            }
        }
    });
}

function getgridData(res, hash, noOfRowstoShow) {

    var page = parseInt(hash.replace('#', ''), 10) || 1, limit = noOfRowstoShow, row = (page - 1) * limit,
        count = page * limit, part = [];

    for (; row < count; row++) {
        if (res[row] != null) {
            part.push(res[row]);
        }
    }

    var pages = Math.ceil(res.length / noOfRowstoShow);
    jQuery(en_avte_maintenance_items_select_paginggrid).empty();
    for (var i = 1; i <= pages; i++) {
        var element = jQuery("<a href='#" + i + "'>" + i + "</a>");
        element.bind('click', function (e) {
            var hash = e.currentTarget.attributes[0].nodeValue;
            hot.loadData(getgridData(res, hash, noOfRowstoShow));
        });
        jQuery(en_avte_maintenance_items_select_paginggrid).append(element);
    }
    hot.loadData(part);
    return part;
}

function en_avte_maintenance_items_select_cellRender(parameters) {
    var row = parameters.row;
    var col = parameters.col;
    var prop = parameters.prop;
    var myhot = parameters.myhot;
    var cellProperties = {};

    //hide group row.  Used for coloring and permissions.
    if (row === 0) {cellProperties.type = { renderer: hiddenRowRender }; }

    //color the second row, which is the friendly field name
   // (myhot.getDataAtCell(1, col) == "id") ||
    else if ((row == 1) && (!(myhot.getDataAtCell(1, col) == "Vehicle ID"))) {
        if (myhot.getDataAtCell(0, col) == "dr"){ cellProperties.renderer = SCRowRenderer;}
        else if (myhot.getDataAtCell(0, col) == "manager"){ cellProperties.renderer = BlackCellRenderer;}
        cellProperties.readOnly = true;
    }

    else {
        switch(prop) {
            case "id":
                cellProperties.readOnly = true;
                break;
            case "show_in_list":
                cellProperties.type = 'checkbox';
                // cellProperties.renderer = idRowRenderer;
                break;

            default:
        }
        //Take care of Date columns
        if (prop != null) {
            if (prop.indexOf("time") > -1) {
                //cellProperties.type = 'date';
                //cellProperties.dateFormat = 'MM-DD-YYYY HH:MM:SS';
                //cellProperties.correctFormat = false;
                //cellProperties.defaultDate = '01/18/2020';
            } else if (prop.indexOf("date") > -1) {
                cellProperties.type = 'date';
                cellProperties.dateFormat = 'MM-DD-YYYY';
                cellProperties.correctFormat = true;

            }
        }

        if (prop != null) {
            if ((prop.indexOf("collection") > -1) ||
                (prop.indexOf("accession") > -1)){
                cellProperties.readOnly = true;
            }
        }
    }

    return cellProperties;
}

//Ajax call to server for record delete
delete_record_call = function delete_record_call(index, amount, recordnum) {
    return jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        type: "POST",
        async: false,
        data: {
            action: "ajaxen_avte_maintenance_items_select_deleterecord",
            amount: amount,
            index: index,
            id: recordnum,

        }
    }).responseText;
}

})( jQuery );




