<?php


function en_avte_maintenance_items_select_form()
{
    global $current_user, $wpdb;
    $maintenanceRecord = $_GET['mymaintrec'];
    ob_start();
    if ( is_user_logged_in() ) {
        $user = $current_user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            if (doesUserHavePermissionToEnterPage($user, array('administrator', 'fm'))) {
                    wp_enqueue_script('en-avte-maintenance_items_select-js');
                    ?>

                    <BR><A HREF="#BOTTOM">Go to bottom of page</A><BR>
                    <div class="en_avte_maintenance_items_select_form">
                      <!--  Search: <input id="en_avte_maintenance_items_select_searchgrid" type="text" />  -->
                        <div id="en_avte_maintenance_items_select_grid" class="dataTable"></div>
                       <!-- <div id="en_avte_maintenance_items_select_paginggrid" class="pagination"></div> -->
                    </div> <!--en_avte_maintenance_items_select_form-->
                    <h5></h5>
                    <string name="button_description"<br/>
                    <br/>
                    <a name="BOTTOM"></a>
                    <?php

            } else {
                ?>
                <div class="en_avte_maintenance_items_select_form">
                    Sorry, you don't have the right roles assigned to perform this action.
                </div> <!--en_avte_maintenance_items_select_form-->
            <?php
            }
        }
    } else {
        ?>
        <div class="en_avte_maintenance_items_select_form">
            Sorry, you need to be logged in.  You can do that <a href="/logmein/">HERE</a>
        </div> <!--en_avte_maintenance_items_select_form-->
    <?php
    }
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}


?>