var urlParam;
var GetColumnMeta;
var hiddenRowRender;
var SCRowRenderer;
var BlackCellRenderer;
var whiteCellRenderer;
var customCellRenderer;
var delete_record;
var hot_console;
var innerText;
var undo_delete_record;
var isNumber;
var ObjToArr;
var arrayIntersect;
var getIdsToUndo;
var GetHeaderNames;
var GetHeaders;


(function($) {
    hot_console = jQuery('.hot_console.menu-item')[0],
    hot_save = jQuery('.hot_save.menu-item')[0],
    lookup_cache =[];



function lookup_list (query, process, action){
    //alert('here');
    //debugger;
    if (lookup_cache[action] == null) {
        //alert("get");
        jQuery.ajax({
            url: ajax_sttrackorder_object.ajaxurl,
            dataType: 'json',
            data: {
                action: action,
                data: query
            },
            success: function (response) {
                //console.log(response);
                //console.log("response", response.data);
                //process(JSON.parse(response.data)); // JSON.parse takes string as a argument
                set_cache(response.data, action)
                process(lookup_cache[action]);
            }
        });
    } else {
        //alert("here");
        process(lookup_cache[action]);
    }
}

function get_all_codes() {
    jQuery.ajax({
        url: ajax_sttrackorder_object.ajaxurl,
        dataType: 'json',
        data: {
            action: 'ajaxsteditorderdetailscodelookup'
        },
        success: function (response) {
            set_cache(response.data, 'ajaxsteditorderdetailscodelookup')
        }
    });
}

function set_cache (data, action) {
    if (data.length != 0) {
        lookup_cache[action] = data;
    }
}

function SRRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'blue';
    td.style.background = '#C7CEFF';
}

SCRowRenderer = function SCRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'green';
    td.style.background = '#CEC';
}

function idRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'white';
    td.style.background = 'white';
    td.style.border = 'white';
}

whiteCellRenderer = function whiteCellRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    // td.style.fontWeight = 'bold';
    td.style.color = 'grey';
    td.style.background = 'white';
    td.style.border = 'grey';
}

function HCRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'grey';
    td.style.background = '#CBCBC6';
}

BlackCellRenderer = function BlackCellRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'white';
    td.style.background = 'black';
}

hiddenRowRender = function hiddenRowRender(instance, td, row, col, prop, value, cellProperties) {
    //debugger;
    td.style.display = "none";
}

function readOnlyRendererForVerifiedRecords(row, cellProperties, myhot, prop) {
    if (((myhot.getDataAtRowProp(row,"verify_date")) != null)&& (myhot.getDataAtRowProp(row,"verify_date")!= "11-30--0001") && (prop != "verify_date"))
    {cellProperties.readOnly = true; }
}

function safeHtmlRenderer(instance, td, row, col, prop, value, cellProperties) {
    var escaped = Handsontable.helper.stringify(value);
    escaped = strip_tags(escaped, '<em><b><strong><a><big>'); //be sure you only allow certain HTML tags to avoid XSS threats (you should also remove unwanted HTML attributes)
    td.innerHTML = escaped;
    return td;
}
function strip_tags(input, allowed) {
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

    // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');

    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

GetHeaders = function GetHeaders(arr){
        var array_to_return = [{}];
        var x = 0;

        jQuery.each(arr[0], function (i, l) {
            if (!array_to_return[x]) {
                array_to_return[x] = {};
            }
            array_to_return[x] = {data: i};
            x++;
        })
        return  array_to_return;
        debugger;
    }

GetColumnMeta = function GetColumnMeta(arr){
        var array_to_return = [{}];
        var x = 0;

        jQuery.each(arr, function (i, l) {
            if (!array_to_return[x]) {
                array_to_return[x] = {};
            }
            array_to_return[x] = {data: arr[x].field_name};
            x++;
        })
        return  array_to_return;
    }

GetHeaderNames = function GetHeaderNames(arr){
        var array_to_return = [];
        var x = 0;
        // debugger;

        jQuery.each(arr, function (i, l) {
            if (!array_to_return[x]) {
                array_to_return[x] = [];
            }
            array_to_return[x] = arr[x].name;
            x++;
        })
        return  array_to_return;
    }

ObjToArr = function ObjToArr(arr){

    var j = arr.length;
    var array_to_return = [[]];
    while(j--) {
        var x = 0;
        jQuery.each(arr[j], function (i, l) {
            //alert( "Index #" + x + ": " + l );
            if (!array_to_return[j]) {
                array_to_return[j] = [];
            }
            array_to_return[j][x] = l;
            x++;
        })
    }
    return  array_to_return;
}


// update the 'rowmap" with the new row id when a new row is added
function ObjSplice(obj, new_obj, pos){
    var j = Object.keys(obj).length+1;
    while(j--) {
        if (j == pos) {
            obj[j] = new_obj;
            break;
        }
        else {
            obj[j] = obj[j - 1];
        }
    }
}

function keepByAttr(arr, attr1, attr2){
    var i = arr.length;
    while(i--){
        if( arr[i]
            && !(arr[i].hasOwnProperty(attr1) || arr[i].hasOwnProperty(attr2)) ){
            arr.splice(i,1);
        }
    }
    return arr;
}

function returnMap(arr, accepted) {
    //debugger;
    var i = arr.length;
    while(i--) {
        arr[i] = filterKeys (arr[i], accepted);
    }
    return arr;
}

function filterKeys(arr, accepted) {
    var result = {};
    for (var type in arr)
        if (accepted.indexOf(type) > -1)
            result[type] = arr[type];
    return result;
}

// Handsontable.Dom.addEvent(hot_save, 'click', function() {
//     //order_save_all();
//     //order_details_save_all();
//     en_coc_main_save_all();
// });


// deleate function called by beforeRemoveRow hook
delete_record = function delete_record (index, amount, recordnum) {
    var aa = delete_record_call(index, amount, recordnum);
    deleteNotifier(aa);
    function deleteNotifier (aa) {
        //alert('The server responded: ' + res);  //for debug - only works in google chrome
        var data = JSON.parse(aa);
        executeLineDeletion = true;

        // return result to user based on whether the deletion was able to be made in the database
        if (data.error == false) {
            hot_console.innerText = 'Record/s Deleted';
        } else {
            if (data.errors[0] == 'role') {
                hot_console.innerText =
                    "CANNOT DELETE! You don't have permission.";
                executeLineDeletion = false;
            } else {
                hot_console.innerText = 'Not Deleted';
                alert("Sorry, we can't delete the requested record. \n\nError: " + JSON.stringify(data.errors));
                executeLineDeletion = false;
            }
        }
        en_avte_maintenance_autosaveNotification = setTimeout(function () {
            hot_console.innerText = 'Changes will be autosaved';
        }, 2000);
    }
    return executeLineDeletion;
}

//Render cells to have specific characteristics Sunch as html links and specified widths
customCellRenderer = function customCellRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    switch (prop) {

        case "notes":
        case "disposition_details":
        case "description":
        case "handling_instructions":
           td.style.width = '300px';
          break;
        case "year":
            td.style.width = '50px';
            break;
        case "condition":
           td.style.width = '150px';
           break;
        case "explanation_of_service":
           td.style.color = 'blue';
           td.innerHTML = value;
           td.style.width = '300px';
           break;
        case "quantity":
        // case "client_name":
        case "service_documentation":
            td.style.color = 'blue';
            td.innerHTML = value;
            break;
        case "project_id":
        case "client_name":
            td.style.color = 'blue';
            td.innerHTML = value;
             td.style.width = '100px';
            break;

        default:
    }
}



//Get parameters from url
urlParam = function urlParam (name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        //Remove %20 space indicator from url parameters
        while (results[1].indexOf("%20") !=- 1) {
            results[1] = results[1].replace("%20", " ");
        }
        return results[1] || 0;
    }
}

// intersect arrays
arrayIntersect = function arrayIntersect (array1, array2) {
    hasIntersect = false;
    for (key1 in array1) {
        if (key1 == 'each') {
            break;
        }
        x = array1[key1];
        for (key2 in array2) {
            if (key2 == 'each') {
                break;
            }
            y = array2[key2];
            if (x == y) {
                hasIntersect = true;
                break;
            }
        }
    }
    return hasIntersect;
}

isNumber = function isNumber(obj) { return !isNaN(parseFloat(obj)) }

getIdsToUndo = function getIdsToUndo(action, hot){
    if (action.actionType == "remove_row") {
        idsToAddBackIn = action.data;
        for (undoNum = 0; undoNum < action.data.length; undoNum++) {
            idsToAddBackIn[undoNum] = action.data[undoNum][hot.propToCol('id')]
        }
        return idsToAddBackIn;
    }

}

})( jQuery );



