<?php
// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

$items = get_posts( array( 'post_type' => 'qsm_popups', 'post_status' => 'any', 'numberposts' => -1, 'fields' => 'ids' ) );
if ( $items ) {
	foreach ( $items as $item ) {
		wp_delete_post( $item, true);
	}
}

delete_option( 'qsm_addon_simple_popups_settings' );
?>