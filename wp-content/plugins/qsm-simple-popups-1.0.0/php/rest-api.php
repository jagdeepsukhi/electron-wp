<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Returns all active popups
 * 
 * @since 1.0.0
 * @return array An array of popups
 */
function qsm_addon_simple_popups_get_popups() {

	$popups = array();

	// Makes sure user is logged in
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( 0 !== $current_user ) {
			$my_query = new WP_Query( array( 'post_type' => 'qsm_popups', 'posts_per_page' => -1, 'post_status' => 'publish' ) );
			if ( $my_query->have_posts() ) {
				while ( $my_query->have_posts() ) {
					$my_query->the_post();
					$popups[] = array(
						'id' => get_the_ID(),
						'headline' => get_the_title(),
						'quiz' => get_post_meta( get_the_ID(), 'quiz_id', true )
					);
				}
			}
			wp_reset_postdata();
		}
	}
	return $popups;
}

/**
 * Creates a new popup
 * 
 * @since 1.0.0
 * @param object The parameters sent from the Rest API
 * @return array An array that includes new id as value for key 'id'
 */
function qsm_addon_simple_popups_create_popup( WP_REST_Request $request ) {

	// Makes sure user is logged in
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( 0 !== $current_user ) {
			$popups_headline = htmlspecialchars( stripslashes( $request["headline"] ), ENT_QUOTES );
			$quiz = intval( $request["quiz"] );
			$args = array(
				'post_type'    => 'qsm_popups',
				'post_status'  => 'publish',
				'post_parent'  => 0,
				'post_title' => $popups_headline,
				'post_content' => "[qsm quiz=$quiz]"
			);
			$popup_id = wp_insert_post( $args );
			update_post_meta( $popup_id, 'quiz_id', $quiz );
			return array(
				'status' => 'success',
				'id' => $popup_id
			);
		}
	}
	return array(
		'status' => 'error',
		'msg' => 'User not logged in'
	);
}

/**
 * Saves an existing popup
 * 
 * @since 1.0.0
 * @param object The parameters sent from the Rest API
 * @return array An array that says success or error
 */
function qsm_addon_simple_popups_save_popup( WP_REST_Request $request ) {
	// Makes sure user is logged in
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( 0 !== $current_user ) {
			$id = intval( $request["id"] );
			$popups_headline = htmlspecialchars( stripslashes( $request["headline"] ), ENT_QUOTES );
			$quiz = intval( $request["quiz"] );
			$my_post = array(
				'ID'           => $id,
				'post_title' => $popups_headline,
				'post_content' => "[qsm quiz=$quiz]"
			);
			wp_update_post( $my_post );
			update_post_meta( $id, 'quiz_id', $quiz );
			return array(
				'status' => 'success'
			);
		}
	}
	return array(
		'status' => 'error',
		'msg' => 'User not logged in'
	);
}

/**
 * Deletes a popup
 * 
 * @since 1.0.0
 * @param object The parameters sent from the Rest API
 * @return array An array that says success or error
 */
function qsm_addon_simple_popups_delete_popup( WP_REST_Request $request ) {
	// Makes sure user is logged in
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( 0 !== $current_user ) {
			$id = intval( $request["id"] );
			$my_post = array(
				'ID'          => $id,
				'post_status' => 'trash'
			);
			wp_update_post( $my_post );
			return array(
				'status' => 'success'
			);
		}
	}
	return array(
		'status' => 'error',
		'msg' => 'User not logged in'
	);
}
?>