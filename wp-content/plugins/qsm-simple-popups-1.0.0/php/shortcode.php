<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Returns the HTML and enqueues the JS/CSS for the shortcode [qsm_popup]
 * 
 * @since 1.0.0
 * @param array $atts The attributes passed to the shortcode
 * @return string The HTML for the shortcode
 */
function qsm_addon_simple_popups_shortcode( $atts, $content = '' ) {
	extract(shortcode_atts(array(
		'id' => 0,
		'class' => ''
	), $atts));

	// Enqueue your scripts and styles
	wp_enqueue_script( 'micromodal_script', plugins_url( '../js/micromodal.min.js' , __FILE__ ) );
	wp_enqueue_script( 'qsm_simple_popups_script', plugins_url( '../js/qsm-simple-popups.js' , __FILE__ ), array( 'jquery', 'micromodal_script' ) );
	wp_enqueue_style( 'qsm_simple_popups_style', plugins_url( '../css/qsm-simple-popups.css' , __FILE__ ) );  

	$id = intval( $id );
	$classes = esc_attr( apply_filters( 'qsm_simple_popups_classes', $class ) );
	$content = esc_html( apply_filters( 'qsm_simple_popups_content', $content ) );
	$post = get_post( $id );
	
	if ( 'publish' != $post->post_status ) {
		return '';
	}

	ob_start();
	?>
	<a href='#' class='qsm-popup-link <?php echo $classes; ?>' data-popup-id='<?php echo $id; ?>'><?php echo $content; ?></a>
	<div class="qsm-popup qsm-popup-slide" id="modal-<?php echo $id; ?>" aria-hidden="true">
			<div class="qsm-popup__overlay" tabindex="-1" data-micromodal-close>
				<div class="qsm-popup__container" role="dialog" aria-modal="true" aria-labelledby="modal-<?php echo $id; ?>-title">
					<header class="qsm-popup__header">
						<h2 class="qsm-popup__title" id="modal-<?php echo $id; ?>-title"><?php echo $post->post_title; ?></h2>
						<a class="qsm-popup__close" aria-label="Close modal" data-micromodal-close></a>
					</header>
					<main class="qsm-popup__content" id="modal-<?php echo $id; ?>-content">
						<?php echo do_shortcode( $post->post_content ); ?>
					</main>
				</div>
			</div>
		</div>
	<?php
	$display = ob_get_contents();
	ob_end_clean();
	return $display;
}
?>