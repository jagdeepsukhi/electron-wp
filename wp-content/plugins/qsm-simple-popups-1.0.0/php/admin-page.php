<?php
if ( ! defined( 'ABSPATH' ) ) exit;

function qsm_simple_popups_generate_admin_page() {
	global $mlwQuizMasterNext;

	// Only let admins and editors see this page
	if ( ! current_user_can( 'moderate_comments' ) ) {
		return;
	}

	// Enqueue your scripts and styles
	wp_enqueue_script( 'micromodal_script', plugins_url( '../js/micromodal.min.js' , __FILE__ ) );
	wp_enqueue_script( 'qsm_simple_popups_admin_script', plugins_url( '../js/qsm-simple-popups-admin.js' , __FILE__ ), array( 'jquery', 'backbone', 'underscore', 'micromodal_script' ) );
	wp_enqueue_style( 'qsm_simple_popups_admin_style', plugins_url( '../css/qsm-simple-popups.css' , __FILE__ ) );  

	// Load our quizzes
	$quizzes = $mlwQuizMasterNext->pluginHelper->get_quizzes();

	wp_localize_script( 'qsm_simple_popups_admin_script', 'qsmSimplePopupSettings', array(
		'nonce' => wp_create_nonce( 'wp_rest' ),
		'quizzes' => $quizzes
	));
	?>
	<div class="wrap">
		<h2>Simple Popups<a id="new-popup" href="#" class="add-new-h2">Add New</a></h2>
		<div class="alert-messages"></div>
		<?php
		// Show any alerts from saving
		$mlwQuizMasterNext->alertManager->showAlerts();
		?>
		<table class="widefat">
			<thead>
				<tr>
					<th>Popup</th>
					<th>Attached to</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="the-list" class="popup-table-body"></tbody>
			<tfoot>
				<tr>
					<th>Popup</th>
					<th>Attached to</th>
					<th></th>
				</tr>
			</tfoot>
		</table>

		<script type="text/template" id="popup-table-row-tmpl">
			<tr class="popup-row" data-popup-id="<%= id %>">
				<td class="popup-row-edit"><%= headline %><span class="dashicons dashicons-edit"></span></td>
				<td><%= quizName %></td>
				<td class="popup-row-delete"><span class="dashicons dashicons-trash"></span></td>
			</tr>
		</script>

		<script type="text/template" id="alert-tmpl">
			<div class="notice notice-<%= type %>"><p><%= message %></p></div>
		</script>

		<div class="qsm-popupmodal qsm-popup-slide" id="modal-1" aria-hidden="true">
			<div class="qsm-popup__overlay" tabindex="-1" data-micromodal-close>
				<div class="qsm-popup__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
					<header class="qsm-popup__header">
						<h2 class="qsm-popup__title" id="modal-1-title">Edit Popup</h2>
						<a class="qsm-popup__close" aria-label="Close modal" data-micromodal-close></a>
					</header>
					<main class="qsm-popup__content" id="modal-1-content">
						<input type="hidden" id="popup-id" value="0">
						<div class="qsm-row">
							<label for="popup-headline">Headline</label>
							<input type="text" id="popup-headline">
						</div>
						<div class="qsm-row">
							<label for="popup-quiz">Quiz or survey to show in popup</label>
							<select id="popup-quiz">
								<?php
								foreach ($quizzes as $quiz ) {
									echo "<option value='{$quiz->quiz_id}'>";
									echo esc_html( $quiz->quiz_name );
									echo "</option>";
								}
								?>
							</select>
						</div>
						<div class="qsm-row">
							<label for="popup-when">Open this popup when... (more options coming soon)</label>
							<select id="popup-when">
								<option value="link">...a user clicks on a link</option>
							</select>
						</div>
						<div class="qsm-row">
							<pre id="popup-shortcode"></pre>
						</div>
					</main>
					<footer class="qsm-popup__footer">
						<button id="save-popup-button" class="qsm-popup__btn qsm-popup__btn-primary">Save Popup</button>
						<button class="qsm-popup__btn" data-micromodal-close aria-label="Close this dialog window">Cancel</button>
					</footer>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>