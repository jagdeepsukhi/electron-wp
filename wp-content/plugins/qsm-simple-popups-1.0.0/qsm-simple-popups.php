<?php
/**
 * Plugin Name: QSM - Simple Popups
 * Plugin URI: https://quizandsurveymaster.com
 * Description: Easily create popups that have your quizzes or surveys in them.
 * Author: Frank Corso
 * Author URI: https://quizandsurveymaster.com
 * Version: 1.0.0
 *
 * @author
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;


/**
  * This class is the main class of the plugin
  *
  * When loaded, it loads the included plugin files and add functions to hooks or filters. The class also handles the admin menu
  *
  * @since 1.0.0
  */
class QSM_Simple_Popups {

    /**
  	 * Version Number
  	 *
  	 * @var string
  	 * @since 1.0.0
  	 */
  	public $version = '1.0.0';

    /**
  	  * Main Construct Function
  	  *
  	  * Call functions within class
  	  *
  	  * @since 1.0.0
  	  * @uses QSM_Simple_Popups::load_dependencies() Loads required filed
  	  * @uses QSM_Simple_Popups::add_hooks() Adds actions to hooks and filters
  	  * @return void
  	  */
    function __construct() {
		$this->load_dependencies();
		$this->add_hooks();
		$this->check_license();
    }

    /**
  	  * Load File Dependencies
  	  *
  	  * @since 1.0.0
  	  * @return void
  	  */
    public function load_dependencies() {
		include( "php/addon-settings-tab-content.php" );
		include( "php/admin-page.php" );
		include( "php/rest-api.php" );
		include( "php/shortcode.php" );
    }

    /**
  	  * Add Hooks
  	  *
  	  * Adds functions to relavent hooks and filters
  	  *
  	  * @since 1.0.0
  	  * @return void
  	  */
    public function add_hooks() {
		add_shortcode( 'qsm_popup', 'qsm_addon_simple_popups_shortcode' );
		add_action( 'admin_init', 'qsm_addon_simple_popups_register_addon_settings_tabs' );
		add_action( 'admin_menu', array( $this, 'setup_admin_menu' ) );
		add_action( 'init', array( $this, 'register_post_type' ) );
		add_action( 'rest_api_init', function () {
			register_rest_route( 'qsm-simple-popups/v1', '/popups/', array(
				'methods' => 'GET',
				'callback' => 'qsm_addon_simple_popups_get_popups',
			) );
		} );
		add_action( 'rest_api_init', function () {
			register_rest_route( 'qsm-simple-popups/v1', '/popups/', array(
				'methods' => 'POST',
				'callback' => 'qsm_addon_simple_popups_create_popup',
			) );
		} );
		add_action( 'rest_api_init', function () {
			register_rest_route( 'qsm-simple-popups/v1', '/popups/(?P<id>\d+)', array(
				'methods' => 'PUT',
				'callback' => 'qsm_addon_simple_popups_save_popup',
			) );
		} );
		add_action( 'rest_api_init', function () {
			register_rest_route( 'qsm-simple-popups/v1', '/popups/(?P<id>\d+)', array(
				'methods' => 'DELETE',
				'callback' => 'qsm_addon_simple_popups_delete_popup',
			) );
		} );
	}

	/**
	 * Registers the post type in WordPress
	 *
	 * @since 1.0.0
	 */
	public function register_post_type() {
		/* popups post type */
		$popups_args = array(
			'labels'          => array( 'name' => 'Popups' ),
			'public'          => false,
			'query_var'       => false,
			'rewrite'         => false,
			'capability_type' => 'post',
			'supports'        => array( 'title', 'editor' ),
			'can_export'      => false
		);

		// Registers QSM logs post type with filtered $args
		register_post_type( 'qsm_popups', apply_filters( 'qsm_popups_post_type_args', $popups_args ) );
	}
	
	/**
	 * Adds the popups page to the menu
	 * 
	 * @since 1.0.0
	 */
	public function setup_admin_menu() {
		if ( function_exists( 'add_menu_page' ) ) {
			add_menu_page( 'Popups', 'Popups', 'moderate_comments', 'qsm_simple_popups', 'qsm_simple_popups_generate_admin_page', 'dashicons-clipboard' );
		}
	}

	/**
	 * Checks license
	 *
	 * Checks to see if license is active and, if so, checks for updates
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function check_license() {
		
		if( ! class_exists( 'EDD_SL_Plugin_Updater' ) ) {
			// load our custom updater
			include( 'php/EDD_SL_Plugin_Updater.php' );
		}

		// retrieve our license key from the DB
		$simple_popups_data = get_option( 'qsm_addon_simple_popups_settings', '' );
		if ( isset( $simple_popups_data["license_key"] ) ) {
			$license_key = trim( $simple_popups_data["license_key"] );
		} else {
			$license_key = '';
		}

		// setup the updater
		$edd_updater = new EDD_SL_Plugin_Updater( 'https://quizandsurveymaster.com', __FILE__, array(
			'version' 	=> $this->version, 				// current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => 'Simple Popups', 	// name of this plugin
			'author' 	=> 'Frank Corso'  // author of this plugin
		));
	}
}

/**
 * Loads the addon if QSM is installed and activated
 *
 * @since 1.0.0
 * @return void
 */
function qsm_addon_simple_popups_load() {
	// Make sure QSM is active
	if ( class_exists( 'MLWQuizMasterNext' ) ) {
		$qsm_simple_popups = new QSM_Simple_Popups();
	} else {
		add_action( 'admin_notices', 'qsm_addon_simple_popups_missing_qsm' );
	}
}
add_action( 'plugins_loaded', 'qsm_addon_simple_popups_load' );

/**
 * Display notice if Quiz And Survey Master isn't installed
 *
 * @since       1.0.0
 * @return      string The notice to display
 */
function qsm_addon_simple_popups_missing_qsm() {
  echo '<div class="error"><p>QSM - Simple Popups requires Quiz And Survey Master. Please install and activate the Quiz And Survey Master plugin.</p></div>';
}
?>
