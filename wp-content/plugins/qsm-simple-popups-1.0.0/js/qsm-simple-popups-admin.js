/**
 * QSM - Simple Popups v1.0.0
 */

var QSMSimplePopups;
(function ($) {
	QSMSimplePopups = {
		popups: null,
		popup: Backbone.Model.extend({
			defaults: {
				id: null,
				headline: 'My Popup',
				quiz: 1
			}
		}),
		popupCollection: null,
		loadPopups: function() {
			QSMSimplePopups.displayAlert( 'Loading popups...', 'info' );
			QSMSimplePopups.popups.fetch({ 
				headers: { 'X-WP-Nonce': qsmSimplePopupSettings.nonce },
				success: QSMSimplePopups.loadSuccess,
				error: QSMSimplePopups.displayError
			});
		},
		loadSuccess: function() {
			QSMSimplePopups.clearAlerts();
			QSMSimplePopups.loadPopupTable();
		},
		createSuccess: function() {
			QSMSimplePopups.clearAlerts();
			QSMSimplePopups.displayAlert( 'Popup was created!', 'success' );
			QSMSimplePopups.loadPopupTable();
		},
		saveSuccess: function() {
			QSMSimplePopups.clearAlerts();
			QSMSimplePopups.displayAlert( 'Popup was saved!', 'success' );
			QSMSimplePopups.loadPopupTable();
		},
		deleteSuccess: function() {
			QSMSimplePopups.clearAlerts();
			QSMSimplePopups.displayAlert( 'Popup was deleted!', 'success' );
			QSMSimplePopups.loadPopupTable();
		},
		loadPopupTable: function() {
			var template = _.template( $( '#popup-table-row-tmpl' ).html() );
			$( '.popup-table-body' ).empty();
			QSMSimplePopups.popups.each( function( model ) {
				var data = model.toJSON();
				data.quizName = '';
				_.each( qsmSimplePopupSettings.quizzes, function( quiz, index, quizzes) {
					if ( quiz.quiz_id == data.quiz ) {
						data.quizName = quiz.quiz_name;
					}
				});
				$( '.popup-table-body' ).append( template( data ) );
			});
			$( '.popup-row-edit' ).on( 'click', function( event ) {
				event.preventDefault();
				QSMSimplePopups.openEditPopup( $( this ).parents( '.popup-row' ).data( 'popup-id' ) );
			});
			$( '.popup-row-delete' ).on( 'click', function( event ) {
				event.preventDefault();
				QSMSimplePopups.deletePopup( $( this ).parents( '.popup-row' ).data( 'popup-id' ) );
			});
			$( '#the-list tr' ).filter( ':even' ).addClass( 'alternate' );
		},
		openNewPopup: function() {
			QSMSimplePopups.openPopup( 0, '', '', 'modal-1' );
		},
		openEditPopup: function( id ) {
			var popup = QSMSimplePopups.popups.get( id );
			QSMSimplePopups.openPopup( id, popup.get( 'headline' ), popup.get( 'quiz' ), 'modal-1' );
		},
		openPopup: function( id, headline, quiz, modalID ) {
			$( '#popup-id' ).val( id );
			$( '#popup-headline' ).val( headline );
			$( '#popup-quiz' ).val( quiz );
			if ( quiz ) {
				$( '#popup-shortcode' ).html( '[qsm_popup id=' + id + ']Click here[/qsm_popup]' );
			} else {
				$( '#popup-shortcode' ).html( 'Shortcode will appear here after saving...' );
			}
			MicroModal.show( modalID );
		},
		savePopup: function() {
			var id = $( '#popup-id' ).val();
			var headline = $( '#popup-headline' ).val();
			var quiz = $( '#popup-quiz' ).val() ;
			if ( 0 == id ) {
				QSMSimplePopups.createPopup( headline, quiz );
			} else {
				QSMSimplePopups.displayAlert( 'Saving popup...', 'info' );
				var model = QSMSimplePopups.popups.get( id );
				model.save( { headline: headline, quiz: quiz }, 
					{ 
						headers: { 'X-WP-Nonce': qsmSimplePopupSettings.nonce },
						success: QSMSimplePopups.saveSuccess,
						error: QSMSimplePopups.displayError
					} 
				);
			}
			MicroModal.close('modal-1');
		},
		createPopup: function( headline, quiz ) {
			QSMSimplePopups.displayAlert( 'Creating popup...', 'info' );
			QSMSimplePopups.popups.create( 
				{ headline: headline, quiz: quiz, opens: 0 }, 
				{ 
					headers: { 'X-WP-Nonce': qsmSimplePopupSettings.nonce },
					success: QSMSimplePopups.createSuccess,
					error: QSMSimplePopups.displayError
				}
			);
		},
		deletePopup: function( id ) {
			QSMSimplePopups.displayAlert( 'Deleting popup...', 'info' );
			var popup = QSMSimplePopups.popups.get( id );
			popup.destroy({ 
				headers: { 'X-WP-Nonce': qsmSimplePopupSettings.nonce },
				success: QSMSimplePopups.deleteSuccess,
				error: QSMSimplePopups.displayError
			});
		},
		displayError: function( jqXHR, textStatus, errorThrown ) {
			QSMSimplePopups.clearAlerts();
			QSMSimplePopups.displayAlert( 'Error: ' + errorThrown.errorThrown + '! Please try again.', 'error' );
		},
		displayAlert: function( message, type ) {
			QSMSimplePopups.clearAlerts();
			var template = _.template( $( '#alert-tmpl' ).html() );
			var data = {
				message: message,
				type: type
			};
			$( '.alert-messages' ).append( template( data ) );
		},
		clearAlerts: function() {
			$( '.alert-messages' ).empty();
		}
	};
	$(function() {
		// Prepares collection
		QSMSimplePopups.popupCollection = Backbone.Collection.extend({
			url: '/wp-json/qsm-simple-popups/v1/popups',
			model: QSMSimplePopups.popup
		});
		QSMSimplePopups.popups = new QSMSimplePopups.popupCollection();

		// Sets up handlers
		$( '#new-popup' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMSimplePopups.openNewPopup();
		});
		$( '#save-popup-button' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMSimplePopups.savePopup();
		});

		// Begins loading
		QSMSimplePopups.loadPopups();
	});
}(jQuery));
