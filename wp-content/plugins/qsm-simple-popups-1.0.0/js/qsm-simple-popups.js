/**
 * QSM - Simple Popups v1.0.0
 */

var QSMSimplePopups;
(function ($) {
	QSMSimplePopups = {
		openPopup: function( id ) {
			MicroModal.show( 'modal-' + id );
		}
	};
	$(function() {
		$( '.qsm-popup-link' ).on( 'click', function( event ) {
			event.preventDefault();
			QSMSimplePopups.openPopup( $( this ).data( 'popup-id') );
		});
	});
}(jQuery));
