<?php
/*
This function creates the leaderboard from the shortcode.
*/
function mlw_qmn_al_leaderboard_shortcode($atts)
{
	extract(shortcode_atts(array(
		'mlw_quiz' => 1,
		'ranks' => 0,
		'name' => 'none',
		'order' => 'score'
	), $atts));
	$mlw_quiz_id = $mlw_quiz;
	$mlw_quiz_leaderboard_display = "";
	$mlw_limit_sql = '';
	
	if ($ranks != 0)
	{
		$mlw_limit_sql = " LIMIT $ranks";
	}
	
	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_quizzes" . " WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
	$mlw_quiz_options = $wpdb->get_results($sql);
	foreach($mlw_quiz_options as $mlw_eaches) {
		$mlw_quiz_options = $mlw_eaches;
		break;
	}
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
	if ($order != 'latest')
	{
		if ($mlw_quiz_options->system == 0)
		{
			$sql .= " ORDER BY correct_score DESC";
		}
		if ($mlw_quiz_options->system == 1)
		{
			$sql .= " ORDER BY point_score DESC";
		}
	}
	else
	{
		$sql .= " ORDER BY time_taken_real DESC";
	}
	$sql = $sql.$mlw_limit_sql;
	$mlw_result_data = $wpdb->get_results($sql);
	
	$mlw_quiz_leaderboard_display = "";
	if ($name != 'none')
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$name."</h2>";
	}
	else
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$mlw_quiz_options->quiz_name."</h2>";
	}
	$mlw_quiz_leaderboard_display .= "<ol>";
	
	$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
	foreach($mlw_result_data as $mlw_eaches) {
		$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
		$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
		
		$mlw_quiz_leaderboard_display .= "<li>".$mlw_quiz_leaderboard_each."</li>";
	}
	$mlw_quiz_leaderboard_display .= "</ol>";
	return $mlw_quiz_leaderboard_display;
}

function mlw_qmn_al_all_leaderboard_shortcode($atts)
{
	extract(shortcode_atts(array(
		'ranks' => 0,
		'name' => 'none',
		'order' => 'score'
	), $atts));
	$mlw_quiz_leaderboard_display = "";
	$mlw_limit_sql = '';
	$mlw_order_sql = '';
	
	if ($ranks != 0)
	{
		$mlw_limit_sql = " LIMIT $ranks";
	}
	if ($order != 'latest')
	{
		if ($mlw_quiz_options->system == 0)
		{
			$mlw_order_sql = " ORDER BY correct_score DESC";
		}
		if ($mlw_quiz_options->system == 1)
		{
			$mlw_order_sql = " ORDER BY point_score DESC";
		}
	}
	else
	{
		$mlw_order_sql = " ORDER BY time_taken_real DESC";
	}
	
	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0'".$mlw_order_sql.$mlw_limit_sql;
	
	$mlw_result_data = $wpdb->get_results($sql);
	
	$mlw_quiz_leaderboard_display = "";
	if ($name != 'none')
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$name."</h2>";
	}
	else
	{
		$mlw_quiz_leaderboard_display .= "<h2>Quiz Scores</h2>";
	}
	$mlw_quiz_leaderboard_display .= "<ol>";
	
	$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
	foreach($mlw_result_data as $mlw_eaches) {
		$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
		$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
		
		$mlw_quiz_leaderboard_display .= "<li>".$mlw_quiz_leaderboard_each."</li>";
	}
	$mlw_quiz_leaderboard_display .= "</ol>";
	return $mlw_quiz_leaderboard_display;
}

function mlw_qmn_al_all_graph_shortcode($atts)
{
	extract(shortcode_atts(array(
		'ranks' => 0,
		'name' => 'none',
		'max_points' => 500,
		'order' => 'score'
	), $atts));
	$mlw_quiz_leaderboard_display = "";
	$mlw_limit_sql = '';
	$mlw_order_sql = '';
	$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
	
	if ($ranks != 0)
	{
		$mlw_limit_sql = " LIMIT $ranks";
	}
	
	if ($order != 'latest')
	{
		if ($mlw_quiz_options->system == 0)
		{
			$mlw_order_sql = " ORDER BY correct_score DESC";
		}
		if ($mlw_quiz_options->system == 1)
		{
			$mlw_order_sql = " ORDER BY point_score DESC";
		}
	}
	else
	{
		$mlw_order_sql = " ORDER BY time_taken_real DESC";
	}

	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0'".$mlw_order_sql.$mlw_limit_sql;
	$mlw_result_data = $wpdb->get_results($sql);
	
	$mlw_quiz_leaderboard_display = "";
	$mlw_quiz_leaderboard_display .= "
	<style>
	.mlw_shortcode_bargraph 
	{
		list-style: none;
		padding-top: 20px;
		width:560px;
    }

	ul.mlw_shortcode_bargraph li 
	{
		color: white;
		text-align: left;
		font-style: italic;
		font-weight:bolder;
		font-size: 14px;
		line-height: 35px;
		padding: 0px 20px;
		margin-bottom: 5px;
		background: ".$mlw_qmn_al_options['graph_color'].";
	}
	</style>";
	if ($name != 'none')
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$name."</h2>";
	}
	else
	{
		$mlw_quiz_leaderboard_display .= "<h2>Quiz Scores</h2>";
	}
	$mlw_quiz_leaderboard_display .= "<ul class=\"mlw_shortcode_bargraph\">";
	
	
	foreach($mlw_result_data as $mlw_eaches) {
		$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
		$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
		if ($mlw_eaches->quiz_system == 0)
		{
			if ($mlw_eaches->correct_score < 15)
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
			else
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_eaches->correct_score."%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
		}
		if ($mlw_eaches->quiz_system == 1)
		{
			$mlw_style_width = ($mlw_eaches->point_score/$max_points) * 100;
			if ($mlw_style_width < 15)
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
			else
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_style_width."%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
		}
	}
	$mlw_quiz_leaderboard_display .= "</ul>";
	return $mlw_quiz_leaderboard_display;
}

function mlw_qmn_al_single_graph_shortcode($atts)
{
	extract(shortcode_atts(array(
		'mlw_quiz' => 1,
		'ranks' => 0,
		'name' => 'none',
		'max_points' => 500,
		'order' => 'score'
	), $atts));
	$mlw_quiz_id = $mlw_quiz;
	$mlw_quiz_leaderboard_display = "";
	$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
	$mlw_quiz_leaderboard_display .= "
	<style>
	.mlw_shortcode_bargraph 
	{
		list-style: none;
		padding-top: 20px;
		width:560px;
    }

	ul.mlw_shortcode_bargraph li 
	{
		color: white;
		text-align: left;
		font-style: italic;
		font-weight:bolder;
		font-size: 14px;
		line-height: 35px;
		padding: 0px 20px;
		margin-bottom: 5px;
		background: ".$mlw_qmn_al_options['graph_color'].";
	}
	</style>";
	$mlw_limit_sql = '';
	
	if ($ranks != 0)
	{
		$mlw_limit_sql = " LIMIT $ranks";
	}
	
	global $wpdb;
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_quizzes" . " WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
	$mlw_quiz_options = $wpdb->get_results($sql);
	foreach($mlw_quiz_options as $mlw_eaches) {
		$mlw_quiz_options = $mlw_eaches;
		break;
	}
	$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
	if ($order != 'latest')
	{
		if ($mlw_quiz_options->system == 0)
		{
			$sql .= " ORDER BY correct_score DESC";
		}
		if ($mlw_quiz_options->system == 1)
		{
			$sql .= " ORDER BY point_score DESC";
		}
	}
	else
	{
		$sql .= " ORDER BY time_taken_real DESC";
	}
	$sql = $sql.$mlw_limit_sql;
	$mlw_result_data = $wpdb->get_results($sql);
	
	if ($name != 'none')
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$name."</h2>";
	}
	else
	{
		$mlw_quiz_leaderboard_display .= "<h2>".$mlw_quiz_options->quiz_name."</h2>";
	}
	$mlw_quiz_leaderboard_display .= "<ul class=\"mlw_shortcode_bargraph\">";
	
	foreach($mlw_result_data as $mlw_eaches) {
		$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
		$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
		$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
		if ($mlw_eaches->quiz_system == 0)
		{
			if ($mlw_eaches->correct_score < 15)
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
			else
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_eaches->correct_score."%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
		}
		if ($mlw_eaches->quiz_system == 1)
		{
			$mlw_style_width = ($mlw_eaches->point_score/$max_points) * 100;
			if ($mlw_style_width < 15)
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
			else
			{
				$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_style_width."%;\">".$mlw_quiz_leaderboard_each."</li>";
			}
		}
	}
	$mlw_quiz_leaderboard_display .= "</ul>";
	return $mlw_quiz_leaderboard_display;
}
?>