<?php
/*
This is the file that contains all the widgets for the plugin
*/

class Mlw_Qmn_Al_Widget extends WP_Widget {
   	
   	// constructor
    function __construct() {
        parent::__construct(false, 'Advanced Leaderboard For QMN');
    }
    
    // widget form creation
    function form($instance) { 
	    // Check values
		if( $instance) {
	     	$quiz = esc_attr($instance['quiz']);
			$ranks = esc_attr($instance['ranks']);
			$title = esc_attr($instance['title']);
		} else {
			$quiz = '';
			$ranks = '';
			$title = '';
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'mlw_qmn_text_domain'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id('quiz'); ?>"><?php _e('Quiz ID (leave 0 for all quizzes)', 'mlw_qmn_text_domain'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('quiz'); ?>" name="<?php echo $this->get_field_name('quiz'); ?>" type="text" value="<?php echo $quiz; ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id('ranks'); ?>"><?php _e('Amount Of Results To Show (Leave 0 for all results'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('ranks'); ?>" name="<?php echo $this->get_field_name('ranks'); ?>" type="text" value="<?php echo $ranks; ?>" />
		</p>
		<?php
	}
	
    // widget update
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
      	// Fields
      	$instance['quiz'] = strip_tags($new_instance['quiz']);
		$instance['ranks'] = strip_tags($new_instance['ranks']);
		$instance['title'] = strip_tags($new_instance['title']);
     	return $instance;
    }
    
    // widget display
    function widget($args, $instance) {
        extract( $args );
   		// these are the widget options
   		$quiz = $instance['quiz'];
		$ranks = $instance['ranks'];
		$title = $instance['title'];
		$mlw_rank_sql = '';
		if ($ranks != 0)
		{
			$mlw_rank_sql = " LIMIT $ranks";
		}
    	echo $before_widget;
   		// Display the widget
   		echo '<div class="widget-text wp_widget_plugin_box">';
   		// Check if title is set
   		if ( $title ) {
      		echo $before_title . $title . $after_title;
   		}
		$mlw_quiz_id = $quiz;
		$mlw_quiz_leaderboard_display = "";
		
		
		global $wpdb;
		if ($mlw_quiz_id != 0)
		{
			$order = 'score';
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_quizzes" . " WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
			$mlw_quiz_options = $wpdb->get_results($sql);
			foreach($mlw_quiz_options as $mlw_eaches) {
				$mlw_quiz_options = $mlw_eaches;
				break;
			}
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
			if ($order != 'latest')
			{
				if ($mlw_quiz_options->system == 0)
				{
					$sql .= " ORDER BY correct_score DESC";
				}
				if ($mlw_quiz_options->system == 1)
				{
					$sql .= " ORDER BY point_score DESC";
				}
			}
			else
			{
				$sql .= " ORDER BY time_taken_real DESC";
			}
			$sql = $sql.$mlw_rank_sql;
			$mlw_result_data = $wpdb->get_results($sql);
			
			$mlw_quiz_leaderboard_display = "";
			$mlw_quiz_leaderboard_display .= "<ol>";
			
			$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
			foreach($mlw_result_data as $mlw_eaches) {
				$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
				$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
				
				$mlw_quiz_leaderboard_display .= "<li>".$mlw_quiz_leaderboard_each."</li>";
			}
			$mlw_quiz_leaderboard_display .= "</ol>";
		}
		else
		{
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0' ORDER BY time_taken_real DESC".$mlw_rank_sql;
			$mlw_result_data = $wpdb->get_results($sql);
			
			$mlw_quiz_leaderboard_display = "";
			$mlw_quiz_leaderboard_display .= "<ol>";
			
			$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
			foreach($mlw_result_data as $mlw_eaches) {
				$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
				$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
				
				$mlw_quiz_leaderboard_display .= "<li>".$mlw_quiz_leaderboard_each."</li>";
			}
			$mlw_quiz_leaderboard_display .= "</ol>";
		}
		echo $mlw_quiz_leaderboard_display;
   		echo '</div>';
   		echo $after_widget;
    }
}

class Mlw_Qmn_Al_Graph_Widget extends WP_Widget {
   	
   	// constructor
    function __construct() {
        parent::__construct(false, 'Advanced Leaderboard Graph For QMN');
    }
    
    // widget form creation
    function form($instance) { 
	    // Check values
		if( $instance) {
	     	$quiz = esc_attr($instance['quiz']);
			$ranks = esc_attr($instance['ranks']);
			$title = esc_attr($instance['title']);
		} else {
			$quiz = '';
			$ranks = '';
			$title = '';
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'mlw_qmn_text_domain'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id('quiz'); ?>"><?php _e('Quiz ID (leave 0 for all quizzes)', 'mlw_qmn_text_domain'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('quiz'); ?>" name="<?php echo $this->get_field_name('quiz'); ?>" type="text" value="<?php echo $quiz; ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id('ranks'); ?>"><?php _e('Amount Of Results To Show (Leave 0 for all results'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('ranks'); ?>" name="<?php echo $this->get_field_name('ranks'); ?>" type="text" value="<?php echo $ranks; ?>" />
		</p>
		<?php
	}
	
    // widget update
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
      	// Fields
      	$instance['quiz'] = strip_tags($new_instance['quiz']);
		$instance['ranks'] = strip_tags($new_instance['ranks']);
		$instance['title'] = strip_tags($new_instance['title']);
     	return $instance;
    }
    
    // widget display
    function widget($args, $instance) {
        extract( $args );
   		// these are the widget options
   		$quiz = $instance['quiz'];
		$ranks = $instance['ranks'];
		$title = $instance['title'];
		$mlw_rank_sql = '';
		if ($ranks != 0)
		{
			$mlw_rank_sql = " LIMIT $ranks";
		}
    	echo $before_widget;
   		// Display the widget
   		echo '<div class="widget-text wp_widget_plugin_box">';
   		// Check if title is set
   		if ( $title ) {
      		echo $before_title . $title . $after_title;
   		}
		$mlw_quiz_id = $quiz;
		$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
		$mlw_quiz_leaderboard_display = "";
		$mlw_quiz_leaderboard_display .= "
		<style>
		.mlw_widget_bargraph 
		{
			list-style: none;
			padding-top: 20px;
			width:100%;
		}

		ul.mlw_widget_bargraph li 
		{
			color: white;
			text-align: left;
			font-style: italic;
			font-weight:bolder;
			font-size: 14px;
			line-height: 35px;
			padding: 0px 20px;
			margin-bottom: 5px;
			background: ".$mlw_qmn_al_options['graph_color'].";
		}
		</style>";
		
		
		global $wpdb;
		if ($mlw_quiz_id != 0)
		{
			$order = 'score';
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_quizzes" . " WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
			$mlw_quiz_options = $wpdb->get_results($sql);
			foreach($mlw_quiz_options as $mlw_eaches) {
				$mlw_quiz_options = $mlw_eaches;
				break;
			}
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE quiz_id=".$mlw_quiz_id." AND deleted='0'";
			if ($order != 'latest')
			{
				if ($mlw_quiz_options->system == 0)
				{
					$sql .= " ORDER BY correct_score DESC";
				}
				if ($mlw_quiz_options->system == 1)
				{
					$sql .= " ORDER BY point_score DESC";
				}
			}
			else
			{
				$sql .= " ORDER BY time_taken_real DESC";
			}
			$sql = $sql.$mlw_rank_sql;
			$mlw_result_data = $wpdb->get_results($sql);
			
			$mlw_quiz_leaderboard_display .= "<ul class=\"mlw_widget_bargraph\">";
			
			
			foreach($mlw_result_data as $mlw_eaches) {
				$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
				$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
				if ($mlw_eaches->quiz_system == 0)
				{
					if ($mlw_eaches->correct_score < 15)
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
					else
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_eaches->correct_score."%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
				}
				if ($mlw_eaches->quiz_system == 1)
				{
					$mlw_style_width = ($mlw_eaches->point_score/500) * 100;
					if ($mlw_style_width < 15)
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
					else
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_style_width."%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
				}
			}
			$mlw_quiz_leaderboard_display .= "</ul>";
		}
		else
		{
			$sql = "SELECT * FROM " . $wpdb->prefix . "mlw_results WHERE deleted='0' ORDER BY time_taken_real DESC".$mlw_rank_sql;
			$mlw_result_data = $wpdb->get_results($sql);
			
			$mlw_quiz_leaderboard_display = "";
			$mlw_quiz_leaderboard_display .= "<ul class=\"bargraph\">";
			
			$mlw_qmn_al_options = get_option('mlw_qmn_al_option');
			foreach($mlw_result_data as $mlw_eaches) {
				$mlw_quiz_leaderboard_each = $mlw_qmn_al_options['template'];
				$mlw_quiz_leaderboard_each = str_replace( "%POINT_SCORE%" , $mlw_eaches->point_score, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%CORRECT_SCORE%" , round($mlw_eaches->correct_score, 2), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%QUIZ_NAME%" , $mlw_eaches->quiz_name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%USER_NAME%" , $mlw_eaches->name, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TIME_TAKEN%" , date("Y-m-d H:i", strtotime($mlw_eaches->time_taken_real)), $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%AMOUNT_CORRECT%" , $mlw_eaches->correct, $mlw_quiz_leaderboard_each);
				$mlw_quiz_leaderboard_each = str_replace( "%TOTAL_QUESTIONS%" , $mlw_eaches->total, $mlw_quiz_leaderboard_each);
				
				if ($mlw_eaches->quiz_system == 0)
				{
					if ($mlw_eaches->correct_score < 15)
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
					else
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_eaches->correct_score."%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
				}
				if ($mlw_eaches->quiz_system == 1)
				{
					$mlw_style_width = ($mlw_eaches->point_score/500) * 100;
					if ($mlw_style_width < 15)
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: 15%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
					else
					{
						$mlw_quiz_leaderboard_display .= "<li style=\"width: ".$mlw_style_width."%;\">".$mlw_quiz_leaderboard_each."</li>";
					}
				}
			}
			$mlw_quiz_leaderboard_display .= "</ul>";
		}
		echo $mlw_quiz_leaderboard_display;
   		echo '</div>';
   		echo $after_widget;
    }
}
?>