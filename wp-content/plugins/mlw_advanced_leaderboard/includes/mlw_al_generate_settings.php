<?php
function mlw_qmn_generate_leaderboard_settings()
{
	?>
	<style type="text/css">
		div.mlw_quiz_options input[type='text'], div.mlw_quiz_options textarea 
		{
			border-color:#000000;
			color:#3300CC; 
			cursor:hand;
		}
	</style>
	<div class="wrap">
	<div class='mlw_quiz_options'>
	<h2>QMN Advanced Leaderboards</h2>
	<form method="post" action="options.php">
		<?php settings_fields('mlw_qmn_al_settings'); ?>
		<?php $mlw_qmn_al_options = get_option('mlw_qmn_al_option'); ?>
		<p>There are 4 shortcodes with this add-on. Ranks is the amount of results you want in the leaderboard. Mlw_quiz is the quiz number of the quiz you want in the leaderboard. 
		Name is the title of the leaderboard. For those using the points system, you must include max_points=0 in your shortcode where 0 is the maximum amount of points a user can 
		receive in the quiz. Lastly, set order to score to order the leaderboard by highest score first or set it to latest to order the leaderboard by most recent first.</p>
		<p>The first shortcode is to show a leaderboard for all quizzes: <code>[mlw_qmn_all_leaderboard ranks=4 name='Title' order='latest']</code></p>
		<p>The next shortcode is to show a leaderboard for one quizzes: <code>[mlw_qmn_single_leaderboard mlw_quiz=1 ranks=4 name='Title' order='score']</code></p>
		<p>The next shortcode is to show a graph for all quizzes: <code>[mlw_qmn_all_graph ranks=4 name='Title']</code></p>
		<p>The last shortcode is to show a graph for one quizzes: <code>[mlw_qmn_single_graph mlw_quiz=1 ranks=4 name='Title']</code></p>
		<table class="form-table">
			<tr>
				<td width="30%">
					<strong>Leaderboard Template</strong>
					<br />
					<p>Allowed Variables: </p>
					<p style="margin: 2px 0">- %POINT_SCORE%</p>
					<p style="margin: 2px 0">- %CORRECT_SCORE%</p>
					<p style="margin: 2px 0">- %QUIZ_NAME%</p>
					<p style="margin: 2px 0">- %USER_NAME%</p>
					<p style="margin: 2px 0">- %TIME_TAKEN%</p>
					<p style="margin: 2px 0">- %AMOUNT_CORRECT%</p>
					<p style="margin: 2px 0">- %TOTAL_QUESTIONS%</p>
				</td>
				<td><textarea cols="80" rows="3" id="mlw_qmn_al_option[template]" name="mlw_qmn_al_option[template]"><?php echo $mlw_qmn_al_options['template']; ?></textarea>
				</td>
			</tr>
			<tr>
				<td width="30%">
					<strong>Graph Color</strong>
					<br />
				</td>
				<td><input type="text" id="mlw_qmn_al_option[graph_color]" name="mlw_qmn_al_option[graph_color]" value="<?php echo $mlw_qmn_al_options['graph_color']; ?>" />
				</td>
			</tr>
        </table>
        <p class="submit">
        	<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>
    </form>
	</div>
	</div>	
	<?php
}

?>