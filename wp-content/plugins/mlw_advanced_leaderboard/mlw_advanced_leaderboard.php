<?php

/*
Plugin Name: QMN Advanced Leaderboard
Description: Enhance the leaderboard system
Version: 1.0
Author: Frank Corso
Author URI: http://www.mylocalwebstop.com/
Plugin URI: http://www.mylocalwebstop.com/
*/

/* 
Copyright 2014, My Local Webstop (email : fpcorso@mylocalwebstop.com)

Disclaimer of Warranties. 

The plugin is provided "as is". My Local Webstop and its suppliers and licensors hereby disclaim all warranties of any kind, 
express or implied, including, without limitation, the warranties of merchantability, fitness for a particular purpose and non-infringement. 
Neither My Local Webstop nor its suppliers and licensors, makes any warranty that the plugin will be error free or that access thereto will be continuous or uninterrupted.
You understand that you install, operate, and unistall the plugin at your own discretion and risk.
*/
include("includes/mlw_al_shortcode.php");
include("includes/mlw_al_widgets.php");
include("includes/mlw_al_generate_settings.php");

add_shortcode('mlw_qmn_all_leaderboard', 'mlw_qmn_al_all_leaderboard_shortcode');
add_shortcode('mlw_qmn_single_leaderboard', 'mlw_qmn_al_leaderboard_shortcode');
add_shortcode('mlw_qmn_all_graph', 'mlw_qmn_al_all_graph_shortcode');
add_shortcode('mlw_qmn_single_graph', 'mlw_qmn_al_single_graph_shortcode');
add_action('widgets_init', create_function('', 'return register_widget("Mlw_Qmn_Al_Widget");'));
add_action('widgets_init', create_function('', 'return register_widget("Mlw_Qmn_Al_Graph_Widget");'));
add_action('admin_menu', 'mlw_qmn_al_add_menu', 11);
add_action('admin_init', 'mlw_qmn_al_register_settings' );
register_activation_hook( __FILE__, 'mlw_qmn_al_activate');


function mlw_qmn_al_register_settings(){
    register_setting( 'mlw_qmn_al_settings', 'mlw_qmn_al_option' );
}

function mlw_qmn_al_add_menu()
{
	if (function_exists('add_submenu_page'))
	{
		add_submenu_page('quiz-master-next/mlw_quizmaster2.php', 'Advanced Leaderboard', 'Advanced Leaderboard', 'moderate_comments', 'mlw_qmn_al', 'mlw_qmn_generate_leaderboard_settings');
	}
}

function mlw_qmn_al_activate()
{
	if ( ! get_option('mlw_qmn_al_option'))
	{
		$mlw_al_option = array();
		$mlw_al_option["template"] = "%USER_NAME% - %CORRECT_SCORE%%";
		$mlw_al_option["graph_color"] = "#00AEEF";
		add_option('mlw_qmn_al_option' , $mlw_al_option);
	}
}
?>